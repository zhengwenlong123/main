## 后台系统aaa

### 安装
```bash
npm install
```

### 运行
```bash
npm start         # visit http://localhost:22300
```

### 发布
```bash
npm run build     # 注意发布时没有mock
```


## 目录结构

```bash
├── config/                 # umi配置文件夹
│ ├─ config.js              # umi打包发布用配置
│ └─ router.config.js       # 路由配置(被config.js引用)
├── dist/                   # (暂时不管)默认的 build 输出目录
├── doc/                    # 开发相关说明文档
├── e2e/                    # (暂时不管)自动化测试相关
├── mock/                   # 开发时模拟返回Mock相关文件
├── public/                 # 静态文件夹
├── script/                 # 构建前代码美化脚本
├── src/                    # 源码目录
│ ├── asset/                # 静态资源
│ ├── component/            # 组件目录
│ ├── layouts/              # 公共模板layout
│ │ └ BasicLayout.js        # 基本的含菜单布局, 大部分页面应该基于此布局
│ ├── locales/              # 多语系文件
│ ├── models/               # 全局公共Models
│ ├── pages/                # 真正需要开发的页面   <------ 需开发 
│ │ └ document.ejs          # 最初的文件
│ ├── services/             # 调用服务器相关       <------ 需开发
│ ├── util/                 # 公共方法
│ ├── defaultSetting.js     # 全局默认配置
│ └── global.less           # 公共样式
├── test/                   # (暂时不管)测试用
├── .editorconfig           # (暂时不管)编辑器配置
├── .eslinigore.js          # (暂时不管)ES6语法检查忽略配置
├── .eslintrc.js            # (暂时不管)ES6语法检查配置
├── .gitignore.js           # (暂时不管)git忽略配置
├── .prettierignore.js      # (暂时不管)美化忽略配置
├── .prettierrc.js          # (暂时不管)美化配置
├── .stylelintrc.js         # (暂时不管)CSS美化配置
├──  jtest.config.js        # (暂时不管)自动化测试相关配置
├──  jsconfig.json          # (暂时不管)编辑器语法检查配置
├──  package.json           # 项目信息
├──  jsconfig.json          # (暂时不管)编辑器语法检查配置
└──  jsconfig.json          # (暂时不管)TS语法检查配置
```



参考
- Preview: http://preview.pro.ant.design
- Home Page: http://pro.ant.design
- Documentation: http://pro.ant.design/docs/getting-started
- ChangeLog: http://pro.ant.design/docs/changelog
- FAQ: http://pro.ant.design/docs/faq
- Mirror Site in China: http://ant-design-pro.gitee.io



