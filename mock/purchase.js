// 获得供应商列表
function getSupplierList(req, res, u) {
  res.json([
    {
      code: '中邮普泰',
      companyTid: '251514855322927104',
      desc: '中邮普泰',
      enable: true,
      id: '276897398728278016',
      name: '中邮普泰',
      shortName: '中邮普泰',
    },
  ]);
}

// 取得产品列表
function getProducts(req, res, u) {
  res.json([
    {
      name: 'NOVA系列',
      code: 'N0',
      children: [
        { name: 'NOVA4', code: '275014616842747904' },
        { name: 'NOVA3', code: 'N3' },
        { name: 'NOVA2', code: 'N2' },
      ],
    },
    {
      name: 'Mate系列',
      code: 'M0',
      children: [
        { name: 'Mate20', code: 'M20' },
        { name: 'Mate19', code: 'M19' },
        { name: 'Mate18', code: 'M18' },
      ],
    },
  ]);
}

// 通过产品ID 取得产品明细
function getProductDetail(req, res, u) {
  return res.json({
    id: '5AD54E8FBC3C47BB9768A6DB12C722E5',
    name: 'nova 4 自拍极点全面屏超广角三摄正品智能手机',
    code: '2611200995',
    bandName: '华为',
    bandCode: 'HUAWEI',
    bandId: '767D2CA3857C41BAB8D9FE7DB2F0B53D',
    supplierName: '中邮普泰通信服务股份有限公司',
    supplierId: '1207',
    oldPrice: 3000,
    salePrice: 2000,
    imgs: [
      {
        id: 1,
        image:
          'https://res.vmallres.com/pimages//product/6901443278107/800_800_1544693112699mp.png',
        title: '图片1',
      },
      {
        id: 2,
        image:
          'https://res.vmallres.com/pimages//product/6901443278121/800_800_1544693497685mp.png',
        title: '图片2',
      },
      {
        id: 3,
        image:
          'https://res.vmallres.com/pimages//product/6901443278114/800_800_1544693347383mp.png',
        title: '图片3',
      },
      {
        id: 4,
        image:
          'https://img.alicdn.com/imgextra/i1/2838892713/O1CN011VuazcxnwTuUHoA_!!2838892713.jpg_430x430q90.jpg',
        title: '图片4',
      },
    ],
    goods: [
      {
        id: '274966521032589312',
        name: 'NOVA3 8G 64G',
        spu: '274966269573709824',
        code: 'NOVA3',
        enable: true,
        skuInfoVOList: [
          {
            id: '275019233645805568',
            skuStatus: '',
            skuGroup: '274966521032589312',
            skuGroupVO: null,
            skuDesc: 'NOVA3 8G 64G 红色',
            skuShortName: 'NOVA3 8G 64G 红色',
            skuName: 'NOVA3 8G 64G 红色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: 'NOVA3 8G 64G 红色',
            price: '806400',
          },
          {
            id: '275019391435522048',
            skuStatus: '',
            skuGroup: '274966521032589312',
            skuGroupVO: null,
            skuDesc: 'NOVA3 8G 64G 黄色',
            skuShortName: 'NOVA3 8G 64G 黄色',
            skuName: 'NOVA3 8G 64G 黄色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: '',
            price: '808400',
          },
          {
            id: '275019465402073088',
            skuStatus: '',
            skuGroup: '274966521032589312',
            skuGroupVO: null,
            skuDesc: 'NOVA3 8G 64G 蓝色',
            skuShortName: 'NOVA3 8G 64G 蓝色',
            skuName: 'NOVA3 8G 64G 蓝色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: '',
            price: '806400',
          },
          {
            id: '275019675788361728',
            skuStatus: '',
            skuGroup: '274966521032589312',
            skuGroupVO: null,
            skuDesc: 'NOVA3 8G 128G 蓝色',
            skuShortName: 'NOVA3 8G 128G 蓝色',
            skuName: 'NOVA3 8G 128G 蓝色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: '',
            price: '812800',
          },
        ],
      },
      {
        id: '274966669083131904',
        name: 'NOVA3 8G 128G',
        spu: '274966269573709824',
        code: 'NOVA3',
        enable: false,
        skuInfoVOList: [
          {
            id: '275019566715486208',
            skuStatus: '',
            skuGroup: '274966669083131904',
            skuGroupVO: null,
            skuDesc: 'NOVA3 8G 128G 红色',
            skuShortName: 'NOVA3 8G 128G 红色',
            skuName: 'NOVA3 8G 128G 红色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: '',
            price: '812800',
          },
          {
            id: '275019616208273408',
            skuStatus: '',
            skuGroup: '274966669083131904',
            skuGroupVO: null,
            skuDesc: 'NOVA3 8G 128G 黄色',
            skuShortName: 'NOVA3 8G 128G 黄色',
            skuName: 'NOVA3 8G 128G 黄色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: '',
            price: '812800',
          },
        ],
      },
      {
        id: '274966726608011264',
        name: 'NOVA3 6G 64G',
        spu: '274966269573709824',
        code: '',
        enable: true,
        skuInfoVOList: [
          {
            id: '275019880294236160',
            skuStatus: '',
            skuGroup: '274966726608011264',
            skuGroupVO: null,
            skuDesc: 'NOVA3 6G 64G 黑色',
            skuShortName: 'NOVA3 6G 64G 黑色',
            skuName: 'NOVA3 6G 64G 黑色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: '',
            price: '606400',
          },
          {
            id: '275019952583065600',
            skuStatus: '',
            skuGroup: '274966726608011264',
            skuGroupVO: null,
            skuDesc: 'NOVA3 6G 64G 白色',
            skuShortName: 'NOVA3 6G 64G 白色',
            skuName: 'NOVA3 6G 64G 白色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: '',
            price: '606400',
          },
        ],
      },
      {
        id: '274966767590555648',
        name: 'NOVA3 6G 128G',
        spu: '274966269573709824',
        code: '',
        enable: false,
        skuInfoVOList: [
          {
            id: '275020082589712384',
            skuStatus: '',
            skuGroup: '274966767590555648',
            skuGroupVO: null,
            skuDesc: 'NOVA3 6G 128G 白色',
            skuShortName: 'NOVA3 6G 128G 白色',
            skuName: 'NOVA3 6G 128G 白色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: '',
            price: '612800',
          },
          {
            id: '275020154039681024',
            skuStatus: '',
            skuGroup: '274966767590555648',
            skuGroupVO: null,
            skuDesc: 'NOVA3 6G 128G 黑色',
            skuShortName: 'NOVA3 6G 128G 黑色',
            skuName: 'NOVA3 6G 128G 白色',
            spu: '274966269573709824',
            spuVO: null,
            seq: null,
            skuCode: '',
            price: '612800',
          },
        ],
      },
    ],
  });
}

// 取得当前账号地址列表
function getAddress(req, res, u) {
  return res.json([
    {
      id: 1,
      city: '湖北武汉',
      address: '武汉市武昌区中南路1号',
      contact: [{ name: '刘德华', tel: '18011111111' }, { name: '张学友', tel: '18011112222' }],
    },
    {
      id: 2,
      city: '湖北武汉',
      address: '武汉市江汉区江汉路佳丽广场4楼',
      contact: [{ name: '林志玲', tel: '19922221111' }, { name: '林心如', tel: '19922222222' }],
    },
    {
      id: 3,
      city: '湖北武汉',
      address: '武汉市洪山区洪山广场',
      contact: [{ name: '李一一', tel: '19933331111' }, { name: '李一二', tel: '19944442222' }],
    },
    {
      id: 4,
      city: '湖北武汉',
      address: '武汉市硚口区仁寿路123号',
      contact: [{ name: '王二一', tel: '19933331111' }],
    },
  ]);
}

// 取得当前区域经理列表
function getAreaManager(req, res, u) {
  return res.json([
    { name: '赵经理', code: '111' },
    { name: '钱经理', code: '222' },
    { name: '孙经理', code: '333' },
    { name: '付经理', code: '444' },
    { name: '贾经理', code: '555' },
  ]);
}

// 暂存
function Save(req, res, u) {
  return res.json({
    code: 200,
    message: '',
    data: {},
  });
}

// 提交
function Submit(req, res, u) {
  return res.json({
    code: 200,
    message: '',
    data: {},
  });
}

function UploadFile(req, res, u) {
  setTimeout(() => {
    return res.json({
      code: 200,
      message: 'success',
      data: req.files[0].originalname,
    });
  }, 3000);
}

function GetOrderBaseMsg(req, res, u) {
  return res.json({
    suplier: '供应商',
    code: '供应商单号',
    orderCreateTime: '采购时间',
    orderType: '订单类型',
    orderMoney: 2000,
    orderStatus: '订单状态',
    discount: 45656,
    status: '审核状态',
    shouldPay: 2335,
    orderPayStatus: '支付状态',
    readyPay: 5655,
    orderShippingStatus: '发运状态',
    payType: '付款方式',
    ebsStatus: 'EBS状态',
    manager: '区域经理',
    oa: 'https://res.vmallres.com/pimages//product/6901443278107/800_800_1544693112699mp.png',
    desc: '订单附言',
    agree: '购销协议',
  });
}

function GetOrderDetail(req, res, u) {
  return res.json({
    salesOrderDetailVOList: [
      {
        id: 'a1',
        name: 'NOVA3 8G 64G 红色',
        number: '7',
      },
      {
        id: 'a2',
        name: 'NOVA3 8G 128G 蓝色',
        number: '18',
      },
    ],
    addressSplitCargoVOList: [
      {
        id: 'a3',
        province: '湖北',
        city: '武汉',
        area: '江岸区',
        addressDetail: '张自忠路2号',
        receiverName: '姓名201, 姓名202',
        receiverTel: '电话201, 电话202',
        splitCargoList: [
          {
            id: 'a4',
            name: 'NOVA3 8G 64G 红色',
            number: '2',
          },
          {
            id: 'a5',
            name: 'NOVA3 8G 128G 蓝色',
            number: '10',
          },
        ],
      },
      {
        id: 'a6',
        province: '湖北省',
        city: '武汉市',
        area: '江汉区',
        addressDetail: '张自忠路1号',
        receiverName: '姓名101, 姓名102',
        receiverTel: '电话101, 电话102',
        splitCargoList: [
          {
            id: 'a7',
            name: 'NOVA3 8G 64G 红色',
            number: '5',
          },
          {
            id: 'a8',
            name: 'NOVA3 8G 128G 蓝色',
            number: '8',
          },
        ],
      },
    ],
  });
}

function GetPayDetail(req, res, u) {
  return res.json([
    {
      id: 11,
      payType: '返利抵扣',
      time: '2019-02-10 10:12:23',
      type: '支付',
      money: '21400',
      desc: '23435456567567867',
    },
    {
      id: 22,
      payType: '钱包',
      time: '2019-02-10 10:12:23',
      type: '退款',
      money: '500',
      desc: '改退单：234253453',
    },
  ]);
}

function GetLogisticsDetail(req, res, u) {
  return res.json({
    salesOrderLogistics: [
      {
        id: 'a',
        name: 'NOVA3 8G 64G 红色',
        orderNum: '7',
        splitcargoNum: '12',
        waitNum: '3',
        DeliverNum: '6',
        signingNum: '9',
        cancelNum: '1',
      },
      {
        id: 'b',
        name: 'NOVA3 8G 128G 蓝色',
        orderNum: '5',
        splitcargoNum: '1',
        waitNum: '3',
        DeliverNum: '6',
        signingNum: '9',
        cancelNum: '1',
      },
    ],
    salesAddressLogistics: [
      {
        id: 1,
        province: '湖北',
        city: '武汉',
        area: '江岸区',
        addressDetail: '张自忠路2号',
        receiverName: '姓名201, 姓名202',
        receiverTel: '电话201, 电话202',
        splitCargoList: [
          {
            id: 'aa',
            name: 'NOVA3 8G 64G 红色',
            number: '2',
          },
          {
            id: 'bb',
            name: 'NOVA3 8G 128G 蓝色',
            number: '10',
          },
        ],
        timeLineData: [
          {
            time: '2019-09-02',
            status: 1,
            person: '张三',
            logistical: '顺丰',
            code: '20000000000000002',
            img:
              'https://res.vmallres.com/pimages//product/6901443278121/800_800_1544693497685mp.png',
            detail: [{ desc: '华为NOVA4-AL00(4+64G)蓝楹紫全网通手机20台' }],
          },
          {
            time: '2019-09-03',
            status: 2,
            person: '张三',
            logistical: '顺丰',
            code: '2000000000000000',
          },
        ],
      },
      {
        id: 2,
        province: '湖北省',
        city: '武汉市',
        area: '江汉区',
        addressDetail: '张自忠路1号',
        receiverName: '姓名101, 姓名102',
        receiverTel: '电话101, 电话102',
        splitCargoList: [
          {
            id: 'cc',
            name: 'NOVA3 8G 64G 红色',
            number: '5',
          },
          {
            id: 'dd',
            name: 'NOVA3 8G 128G 蓝色',
            number: '8',
          },
        ],
        timeLineData: [
          {
            time: '2019-09-01',
            status: 0,
            person: '张三',
            logistical: '顺丰',
            code: '20000000000000003',
            img:
              'https://res.vmallres.com/pimages//product/6901443278107/800_800_1544693112699mp.png',
            detail: [
              { desc: '华为NOVA4-AL00(4+64G)蓝楹紫全网通手机20台' },
              { desc: '华为NOVA4-AL00(4+64G)相思红全网通手机30台' },
            ],
          },
          {
            time: '2019-09-03',
            status: 2,
            person: '张三',
            logistical: '顺丰',
            code: '20000000000000006',
          },
        ],
      },
    ],
  });
}

function GetEditLogDetail(req, res, u) {
  return res.json([
    {
      id: 'b1',
      type: '财务审核',
      person: 'ella',
      result: 0,
      result1: 0,
      time: '1',
      description: 'My name is Jim Green, ',
    },
    {
      id: 'b2',
      type: '商务审核',
      person: 'selina',
      result: 1,
      result1: 1,
      time: '1',
      description: '',
    },
    {
      id: 'b3',
      type: '业务审核',
      person: 'hebe',
      result: 2,
      result1: 2,
      time: '1',
      description: 'I am 42 years old, living in London No. 1 Lake Park.',
    },
    {
      id: 'b4',
      type: '创建订单',
      person: 'jonlin',
      result: 1,
      result1: 1,
      time: '1',
      description: '',
    },
  ]);
}

function GetPayTypes(req, res, u) {
  return res.json([
    { name: '返利抵扣', number: 0, code: 'discount' },
    { name: '红票', number: 2000, code: 'redTicket' },
    { name: '钱包', number: 30000, code: 'wallet' },
    { name: '账期', number: 0, code: 'accountPeriod' },
  ]);
}

function GetPurchaseInventory(req, res, u) {
  return res.json({
    totalList: [
      {
        id: 'a4',
        price: 2000,
        skuShortName: 'NOVA3 8G 64G 红色',
        number: 2,
        divideNum: 1,
      },
      {
        id: 'a5',
        price: 3000,
        skuShortName: 'NOVA3 8G 128G 蓝色',
        number: 10,
        divideNum: 4,
      },
    ],
    addressSplitCargoVOList: [
      {
        id: 'a3',
        province: '湖北',
        city: '武汉',
        area: '江岸区',
        addressDetail: '张自忠路2号',
        receiverName: '姓名201, 姓名202',
        receiverTel: '电话201, 电话202',
        splitCargoList: [
          {
            id: 'a4',
            name: 'NOVA3 8G 64G 红色',
            number: 2,
            divideNum: 1,
          },
          {
            id: 'a5',
            name: 'NOVA3 8G 128G 蓝色',
            number: 10,
            divideNum: 4,
          },
        ],
      },
      {
        id: 'a6',
        province: '湖北省',
        city: '武汉市',
        area: '江汉区',
        addressDetail: '张自忠路1号',
        receiverName: '姓名101, 姓名102',
        receiverTel: '电话101, 电话102',
        splitCargoList: [
          {
            id: 'a7',
            name: 'NOVA3 8G 64G 红色',
            number: 5,
            divideNum: 6,
          },
          {
            id: 'a8',
            name: 'NOVA3 8G 128G 蓝色',
            number: 8,
            divideNum: 3,
          },
        ],
      },
    ],
  });
}

export default {
  // 取得应商列表
  'GET /API/SupplierList': getSupplierList,

  // 取得产品列表
  'GET /API/Product': getProducts,
  // 通过产品ID 取得产品明细
  'GET /API/Product/:id': getProductDetail,
  // 取得当前账号地址列表
  'GET /API/Address': getAddress,
  // 取得当前区域经理列表
  'GET /API/AreaManager': getAreaManager,
  // 暂存
  'POST /API/Save': Save,
  // 提交
  'POST /API/Submit': Submit,
  'POST /API/UploadFile': UploadFile,

  // 获取订单基本信息
  'GET /API/OrderBaseMsg': GetOrderBaseMsg,
  // 获取订单详情
  'GET /API/OrderDetail': GetOrderDetail,
  // 获取支付详情
  'GET /API/PayDetail': GetPayDetail,
  // 获取订单物流详情
  'GET /API/LogisticsDetail': GetLogisticsDetail,
  // 获取操作日志详情
  'GET /API/EditLogDetail': GetEditLogDetail,

  'GET /API/PayTypes': GetPayTypes,
  'GET /API/PurchaseInventory': GetPurchaseInventory,
};
