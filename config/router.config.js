import AUTHORITY from '../src/utils/authorities';

export default [
  // 正常登入页
  {
    path: '/login',
    component: './Login/Login',
  },
  // 从其他系统跳转登入
  {
    path: '/jumplogin',
    component: './Login/JumpLogin',
  },
  // 忘记密码页
  {
    path: '/forgetpassword',
    component: './Login/forgetPassword',
  },
  // 登陆跳转修改密码
  {
    path: '/changepassword',
    component: './Login/changePassword',
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    routes: [
      // 根重定向
      {
        path: '/',
        redirect: '/welcome',
      },
      // 欢迎页
      {
        path: '/welcome',
        name: 'welcome',
        icon: 'home',
        component: './Welcome/Welcome',
      },
      // 商品主数据管理
      {
        path: '/goodsmanage',
        name: 'goodsmanage',
        icon: 'folder',
        authority: [AUTHORITY.goods_manage],
        routes: [
          // 品牌管理
          {
            path: '/goodsmanage/goodsbrandmanage',
            name: 'goodsbrandmanage',
            icon: 'file',
            component: './goodsmanage/goodsbrandmanage/GoodsBrandManage',
            authority: [AUTHORITY.goods_brand_manage],
          },
          // 产品大类管理
          {
            path: '/goodsmanage/goodsclassificationmanage',
            name: 'goodsclassificationmanage',
            icon: 'file',
            component: './goodsmanage/goodsclassificationmanage/GoodsClassificationManage',
            authority: [AUTHORITY.goods_classification_manage],
          },
          // 产品类目管理
          {
            path: '/goodsmanage/goodscatagorymanage',
            name: 'goodscatagorymanage',
            icon: 'file',
            component: './goodsmanage/goodscatagorymanage/GoodsCatagoryManage',
            authority: [AUTHORITY.goods_category_manage],
          },
          // ebs分类
          {
            path: '/goodsmanage/goodsebscatagorymanage',
            name: 'goodsebscatagorymanage',
            icon: 'file',
            component: './goodsmanage/goodsebscatagorymanage/GoodsEbsCatagoryManage',
            authority: [AUTHORITY.goods_ebs_category],
          },
          // 属性集管理
          // {
          //   path: '/goodsmanage/goodsattrcollectmanage',
          //   name: 'goodsattrcollectmanage',
          //   icon: 'file',
          //   component: './goodsmanage/goodsattrcollectmanage/GoodsAttrCollectManage',
          // },
          // 属性名称管理
          {
            path: '/goodsmanage/goodsattrnamemanage',
            name: 'goodsattrnamemanage',
            icon: 'file',
            component: './goodsmanage/goodsattrnamemanage/GoodsAttrNameManage',
            authority: [AUTHORITY.goods_attr_name_manage],
          },
          // 产品信息管理
          {
            path: '/goodsmanage/goodsspuinfomanage',
            name: 'goodsspuinfomanage',
            icon: 'file',
            component: './goodsmanage/goodsspuinfomanage/GoodsSpuInfoManage',
            authority: [AUTHORITY.goods_spu_info_manage],
          },
          // 商品信息管理
          {
            path: '/goodsmanage/goodsskuinfomanage',
            name: 'goodsskuinfomanage',
            icon: 'file',
            component: './goodsmanage/goodsskuinfomanage/GoodsSkuInfoManage',
            authority: [AUTHORITY.goods_sku_info_manage],
          },
          // 物料信息查询
          // {
          //   path: '/goodsmanage/goodsmaterielmanage',
          //   name: 'goodsmaterielmanage',
          //   icon: 'file',
          //   component: './goodsmanage/goodsmaterielmanage/GoodsMaterielManage',
          // },
          // 产品申请单管理
          {
            path: '/goodsmanage/goodsapplybillmanage',
            name: 'goodsapplybillmanage',
            icon: 'file',
            component: './goodsmanage/goodsapplybillmanage/GoodsApplyBillManage',
            authority: [AUTHORITY.goods_apply_bill_manage],
          },
          {
            path: '/goodsmanage/goodsapplybillmanage/detail',
            name: 'goodsapplybillmanage.detail',
            icon: 'file',
            component: './goodsmanage/goodsapplybillmanage/GoodsApplyBillMain',
            authority: [AUTHORITY.goods_apply_bill_manage],
            hideInMenu: true,
          },
          // 产品申请单审核
          {
            path: '/goodsmanage/goodsapplybillaudit',
            name: 'goodsapplybillaudit',
            icon: 'file',
            component: './goodsmanage/goodsapplybillaudit/GoodsApplyBillAudit',
            authority: [AUTHORITY.goods_apply_bill_audit],
          },
          {
            path: '/goodsmanage/goodsapplybillaudit/applyAudit',
            name: 'goodsapplybillaudit.applyAudit',
            icon: 'file',
            component: './goodsmanage/goodsapplybillaudit/GoodsApplyBillMain',
            authority: [AUTHORITY.goods_apply_bill_audit],
            hideInMenu: true,
          },
          // 交易产品信息管理
          {
            path: '/goodsmanage/goodstradespuinfomanage',
            name: 'goodstradespuinfomanage',
            icon: 'file',
            component: './goodsmanage/goodstradespuinfomanage/GoodsTradeSpuInfoManage',
            authority: [AUTHORITY.goods_trade_info_manage],
          },
          {
            path: '/goodsmanage/goodstradespuinfomanage/detail',
            name: 'goodstradespuinfomanage.detail',
            icon: 'file',
            component: './goodsmanage/goodstradespuinfomanage/GoodsTradeSpuInfoDetail',
            authority: [AUTHORITY.goods_trade_info_manage],
            hideInMenu: true,
          },
          // 标签管理
          {
            path: '/goodsmanage/goodstagmanage',
            name: 'goodstagmanage',
            icon: 'file',
            component: './goodsmanage/goodstagmanage/GoodsTagManage',
            authority: [AUTHORITY.goods_tag_manage],
          },
          // 物料序号信息
          {
            path: '/goodsmanage/goodslastnomanage',
            name: 'goodslastnomanage',
            icon: 'file',
            component: './goodsmanage/goodslastnomanage/goodsLastNoManage',
            authority: [AUTHORITY.goods_last_no_manage],
          },
        ],
      },
      // 客户主数据管理
      {
        path: '/custmanage',
        name: 'custmanage',
        icon: 'folder',
        authority: [AUTHORITY.cust_manage],
        routes: [
          // 企业信息管理
          {
            path: '/custmanage/custcompanyinfomanage',
            name: 'custcompanyinfomanage',
            icon: 'file',
            component: './custmanage/custcompanyinfomanage/CustCompanyInfoManage',
            authority: [AUTHORITY.cust_company_info_manage],
          },
          // 企业标签管理
          {
            path: '/custmanage/custcompanytagmanage',
            name: 'custcompanytagmanage',
            icon: 'file',
            component: './custmanage/custcompanytagmanage/CustCompanyTagManage',
            authority: [AUTHORITY.cust_company_tag_manage],
          },
          // 企业编码对照管理
          {
            path: '/custmanage/custcompanymappingmanage',
            name: 'custcompanymappingmanage',
            icon: 'file',
            component: './custmanage/custcompanymappingmanage/CustCompanyMappingManage',
            authority: [AUTHORITY.cust_company_mapping_manage],
          },
          // 经营单位类型定义管理
          {
            path: '/custmanage/custoperunittypemanage',
            name: 'custoperunittypemanage',
            icon: 'file',
            component: './custmanage/custoperunittypemanage/CustOperUnitTypeManage',
            authority: [AUTHORITY.cust_oper_unit_type_manage],
          },
          // 客户CA记录
          // {
          //   path: '/custmanage/custcarecordmanage',
          //   name: 'custcarecordmanage',
          //   icon: 'file',
          //   component: './custmanage/custcarecordmanage/CustCaRecordmanage'
          // }
          // 客户申请单
          {
            path: '/custmanage/custapplymanage',
            name: 'custapplymanage',
            icon: 'file',
            component: './custmanage/custapplymanage/CustApplyManage',
            authority: [AUTHORITY.cust_apply_bill_manage],
          },
          // 客户申请单维护
          {
            path: '/custmanage/custapplymanage/info',
            name: 'custapplymanage',
            icon: 'file',
            component: './custmanage/custapplymanage/CustApplyInfoById',
            hideInMenu: true,
            authority: [AUTHORITY.cust_apply_bill_manage],
          },

          // 客户申请单详情
          {
            path: '/custmanage/custapplymanage/detail',
            name: 'custapplymanage',
            icon: 'file',
            component: './custmanage/custapplymanage/DetailInfoById',
            hideInMenu: true,
            authority: [AUTHORITY.cust_apply_bill_manage],
          },
          // 客户申请单审核
          {
            path: '/custmanage/custapplyauditmanage',
            name: 'custapplyauditmanage',
            icon: 'file',
            component: './custmanage/custapplyauditmanage/CustApplyAuditManage',
            authority: [AUTHORITY.cust_apply_bill_audit],
          },
          // 客户申请单审核 详情
          {
            path: '/custmanage/custapplyauditmanage/detail',
            name: 'custapplyauditmanage',
            icon: 'file',
            component: './custmanage/custapplymanage/DetailInfoById',
            hideInMenu: true,
            authority: [AUTHORITY.cust_apply_bill_audit],
          },
          // 客户申请单批量审核
          {
            path: '/custmanage/custapplyauditmanage/batchaudit',
            name: 'custapplyauditmanage',
            icon: 'file',
            component: './custmanage/custapplyauditmanage/CustApplyBatchAudit',
            hideInMenu: true,
            authority: [AUTHORITY.cust_apply_bill_audit],
          },
          // 客户信息管理
          {
            path: '/custmanage/custinfomanage',
            name: 'custinfomanage',
            icon: 'file',
            component: './custmanage/custinfomanage/CustInfoManage',
            authority: [AUTHORITY.cust_info_manage],
          },
          // 客户信息详情
          {
            path: '/custmanage/custinfomanage/detail',
            name: 'custinfomanage',
            icon: 'file',
            component: './custmanage/custinfomanage/CustInfoById',
            hideInMenu: true,
            authority: [AUTHORITY.cust_info_manage],
          },
        ],
      },
      // 参数主数据管理
      {
        path: '/parmmanage',
        name: 'parmmanage',
        icon: 'folder',
        authority: [AUTHORITY.parm_manage],
        routes: [
          // 公用参数管理
          {
            path: '/parmmanage/parmpublicparametermanage',
            name: 'parmpublicparametermanage',
            icon: 'file',
            component: './parmmanage/parmpublicparametermanage/ParmPublicParameterManage',
            authority: [AUTHORITY.parm_public_parameter_manage],
          },
          // 国别定义管理
          {
            path: '/parmmanage/parmcountrymanage',
            name: 'parmcountrymanage',
            icon: 'file',
            component: './parmmanage/parmcountrymanage/ParmCountryManage',
            authority: [AUTHORITY.parm_country_manage],
          },
          // 行政区划定义管理
          {
            path: '/parmmanage/parmregionmanage',
            name: 'parmregionmanage',
            icon: 'file',
            component: './parmmanage/parmregionmanage/ParmRegionManage',
            authority: [AUTHORITY.parm_region_manage],
          },
          // 币别定义管理
          {
            path: '/parmmanage/parmcurrencymanage',
            name: 'parmcurrencymanage',
            icon: 'file',
            component: './parmmanage/parmcurrencymanage/ParmCurrencyManage',
            authority: [AUTHORITY.parm_currency_manage],
          },
        ],
      },
      // 系统配置管理
      /* {
        path: '/sysconfmanage',
        name: 'sysconfmanage',
        icon: 'folder',
        routes: [
          // 系统角色管理
          {
            path: '/sysconfmanage/sysrolemanage',
            name: 'sysrolemanage',
            icon: 'file',
            component: './sysconfmanage/sysrolemanage/SysRoleManage',
          },
          // 系统帐号管理
          {
            path: '/sysconfmanage/sysaccountmanage',
            name: 'sysaccountmanage',
            icon: 'file',
            component: './sysconfmanage/sysaccountmanage/SysAccountManage',
          },
        ],
      }, */
      // 账户信息
      {
        name: 'account',
        icon: 'user',
        path: '/account',
        hideInMenu: true,
        routes: [
          // 个人设置
          {
            path: '/account/setting/base',
            name: 'settings',
            component: './account/setting/Base',
          },
        ],
      },
      // 异常页
      {
        name: 'exception',
        icon: 'warning',
        path: '/exception',
        hideInMenu: true,
        routes: [
          // exception
          {
            path: '/exception/403',
            name: 'not-permission',
            component: './Exception/403',
          },
          {
            path: '/exception/404',
            name: 'not-find',
            component: './Exception/404',
          },
          {
            path: '/exception/500',
            name: 'server-error',
            component: './Exception/500',
          },
          {
            path: '/exception/trigger',
            name: 'trigger',
            hideInMenu: true,
            component: './Exception/TriggerException',
          },
        ],
      },
    ],
  },
];
