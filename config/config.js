// ref: https://umijs.org/config/
import { primaryColor, systemName } from '../src/defaultSettings';
import pageRoutes from './router.config';

export default {
  plugins: [
    [
      'umi-plugin-react',
      {
        // 是否启用antd
        antd: true,
        // 是否启用dva
        dva: { hmr: true },
        // 目标版本
        targets: { ie: 11 },
        // 是否本地化多语言
        locale: {
          enable: true,
          // default false
          default: 'zh-CN',
          // default zh-CN
          baseNavigator: true,
        },
        // default true, when it is true, will use `navigator.language` overwrite default
        dynamicImport: {
          loadingComponent: './layouts/components/PageLoading/index',
          webpackChunkName: true,
          level: 3,
        },
        // 是否启动静态化.以便二次加速
        // dll: true,
        // 默认标题
        title: { defaultTitle: systemName },
      },
    ],
    [
      'umi-plugin-pro-block',
      {
        // block载入时是否集中
        moveMock: false,
        // block载入时是否service集中
        moveService: false,
        //
        modifyRequest: true,
        // 是否自动加入菜单
        autoAddMenu: true,
      },
    ],
  ],
  targets: { ie: 11 },
  /**
   * 路由相关配置
   */
  routes: pageRoutes,
  disableRedirectHoist: true,
  /**
   * 指定 history 类型，可选 browser、hash 和 memory,
   * browser 预设, 但是刷新页面会404,无法直接访问某个具体页面
   * hash - 中间会带#, 不美观
   * memory - url根本不会变,无法直接访问某个具体页面
   */
  // history: 'hash',

  /**
   * 这句静态化,比上面更加完美解决刷新404的问题
   * 但是会在build后生成很多多余的文件夹以避免404问题
   * 最后不在路由列表里的依然不会被看到
   */
  exportStatic: true,
  /**
   * webpack 相关配置
   */
  define: { APP_TYPE: process.env.APP_TYPE || '' },
  // 减小无效文件
  treeShaking: true,
  // 是否后面加上随机文字
  hash: true,
  // Theme for antd
  // https://ant.design/docs/react/customize-theme-cn
  theme: { 'primary-color': primaryColor },
  externals: { '@antv/data-set': 'DataSet' },
  ignoreMomentLocale: true,
  lessLoaderOptions: { javascriptEnabled: true },

  proxy: {
    '/serviceApi': {
      // target: 'http://47.107.155.39:22000/mdc-server',
      target: 'http://47.107.155.39:21119',
      // target: 'http://10.10.40.26:21119',
      // target: 'http://192.168.1.101:21119',
      changeOrigin: true,
      pathRewrite: { '^/serviceApi': '' },
    },
    '/appApi': {
      target: 'http://47.107.155.39:22000/',
      changeOrigin: true,
      pathRewrite: { '^/appApi': '' },
    },
    '/attachment': {
      target: 'http://47.106.247.208:21020/',
      changeOrigin: true,
      pathRewrite: { '^/attachment': '' },
    },
    // '/server/api': {
    //   target: 'http://47.107.155.39:21016/',
    //   changeOrigin: true,
    //   pathRewrite: { '^/server/api': '' },
    // },
  },
};
