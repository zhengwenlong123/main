/**
 * [国别定义管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-国别定义
 */
export async function getParmCountry(params) {
  return request.post(`${serverUrl}/mdc/parm/parmcountry/list`, { data: params });
}

/**
 * 获取-国别定义(所有)
 */
export async function getParmCountryAll(params) {
  return request.post(`${serverUrl}/mdc/parm/parmcountry/getParmCountryAll`, { data: params });
}

/**
 * 新增-国别定义
 */
export async function addParmCountry(params) {
  return request.post(`${serverUrl}/mdc/parm/parmcountry`, { data: params });
}

/**
 * 更新-国别定义
 */
export async function updateParmCountry(params) {
  return request.put(`${serverUrl}/mdc/parm/parmcountry/${params.id}`, { data: params });
}

/**
 * 更新-国别定义状态
 */
export async function updateParmCountryEnable(params) {
  return request.put(`${serverUrl}/mdc/parm/parmcountry/${params.id}`, { data: params });
}

/**
 * 分页-国别定义
 * @param {*} parms
 */
export async function paginationParmCountry(params) {
  return request.post(`${serverUrl}/mdc/parm/parmcountry/list`, { data: params });
}
