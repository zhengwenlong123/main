/**
 * [公用参数管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-公用参数类型定义
 */
export async function getParmPublicParameterType(params) {
  return request.post(`${serverUrl}/mdc/parm/parmpublicparametertype/list`, { data: params });
}

/**
 * 新增-公用参数类型定义
 */
export async function addParmPublicParameterType(params) {
  return request.post(`${serverUrl}/mdc/parm/parmpublicparametertype`, { data: params });
}

/**
 * 更新-公用参数类型定义
 */
export async function updateParmPublicParameterType(params) {
  return request.put(`${serverUrl}/mdc/parm/parmpublicparametertype/${params.id}`, {
    data: params,
  });
}

/**
 * 删除-公用参数类型定义
 */
export async function deleteParmPublicParameterType(idArray) {
  return request.delete(`${serverUrl}/mdc/parm/parmpublicparametertype/${idArray}`);
}

/**
 * 分页-公用参数类型定义
 * @param {*} parms
 */
export async function paginationParmPublicParameterType(params) {
  return request.post(`${serverUrl}/mdc/parm/parmpublicparametertype/list`, { data: params });
}

/**
 * 分页-公用参数定义
 * @param {*} parms
 */
export async function paginationParmPublicParameter(params) {
  return request.post(`${serverUrl}/mdc/parm/parmpublicparameter/list`, { data: params });
}

/**
 * 获取-公用参数定义(通过公用参数类型定义查询)
 */
export async function getParmPublicParameterAllByParmPublicParameterType(params) {
  return request.post(
    `${serverUrl}/mdc/parm/parmpublicparameter/getParmPublicParameterAllByParmPublicParameterType`,
    { data: params }
  );
}

/**
 * 新增-公用参数定义
 */
export async function addParmPublicParameter(params) {
  return request.post(`${serverUrl}/mdc/parm/parmpublicparameter`, { data: params });
}

/**
 * 更新-公用参数定义
 */
export async function updateParmPublicParameter(params) {
  return request.put(`${serverUrl}/mdc/parm/parmpublicparameter/${params.id}`, { data: params });
}

/**
 * 删除-公用参数定义
 */
export async function deleteParmPublicParameter(idArray) {
  return request.delete(`${serverUrl}/mdc/parm/parmpublicparameter/${idArray}`);
}

/**
 * 根据公用参数类型定义查询公用参数定义所有数据
 */
export async function findByParamType(params) {
  const { parmTypeCode, ...other } = params;
  return request.post(
    `${serverUrl}/mdc/parm/parmpublicparameter/findByParamType/${params.parmTypeCode}`,
    { data: other }
  );
}
