/**
 * [行政区划定义管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-行政区划定义
 * @param {object} params
 */
export async function getParmRegion(params) {
  return request.post(`${serverUrl}/mdc/parm/parmregion/list`, { data: params });
}

/**
 * 获取-行政区划定义数据(所有,列表结构)
 * @param {object} params
 */
export async function getParmRegionAllByParmRegion(params) {
  return request.post(`${serverUrl}/mdc/parm/parmregion/getParmRegionAllByParmRegion`, {
    data: params,
  });
}

/**
 * 新增-行政区划定义
 */
export async function addParmRegion(params) {
  return request.post(`${serverUrl}/mdc/parm/parmregion`, { data: params });
}

/**
 * 更新-行政区划定义
 */
export async function updateParmRegion(params) {
  return request.put(`${serverUrl}/mdc/parm/parmregion/${params.id}`, { data: params });
}

/**
 * 更新-行政区划定义状态
 */
export async function updateParmRegionEnable(params) {
  return request.put(`${serverUrl}/mdc/parm/parmregion/${params.id}`, { data: params });
}

/**
 * 分页-行政区划定义
 * @param {object} params
 */
export async function paginationParmRegion(params) {
  return request.post(`${serverUrl}/mdc/parm/parmregion/list`, { data: params });
}

/**
 * 获取-根据tid获取行政区划内按国别代码regionLevel为1所有数据
 * @param {string} params
 */
export async function getParmRegionOfCountryCodeAll(params) {
  return request.post(
    `${serverUrl}/mdc/parm/parmregion/getParmRegionOfCountryCodeAll/${params.tid}`
  );
}
