/**
 * [币别定义管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-币别定义
 */
export async function getParmCurrency(params) {
  return request.post(`${serverUrl}/mdc/parm/parmcurrency/list`, { data: params });
}

/**
 * 获取-币别定义(所有)
 */
export async function getParmCurrencyAll(params) {
  return request.post(`${serverUrl}/mdc/parm/parmcurrency/getParmCurrencyAll`, { data: params });
}

/**
 * 新增-币别定义
 */
export async function addParmCurrency(params) {
  return request.post(`${serverUrl}/mdc/parm/parmcurrency`, { data: params });
}

/**
 * 更新-币别定义
 */
export async function updateParmCurrency(params) {
  return request.put(`${serverUrl}/mdc/parm/parmcurrency/${params.id}`, { data: params });
}

/**
 * 分页-币别定义
 * @param {*} parms
 */
export async function paginationParmCurrency(params) {
  return request.post(`${serverUrl}/mdc/parm/parmcurrency/list`, { data: params });
}
