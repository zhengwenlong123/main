/**
 * [产品申请单审核] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-产品申请单审核列表(分页/列表)
 */
export async function getGoodsApplyBillAuditList(params) {
  return request.post(`${serverUrl}/mdc/goods_rf/goodsrfinfo/listReview`, { data: params });
}

/**
 * 新增-申请单审核记录
 */
export async function addGoodsApplyAudit(params) {
  return request.post(`${serverUrl}/mdc/goods_rf/goodsrfverifyinfo`, { data: params });
}

/**
 * 获取-获取申请单审核记录
 */
export async function getGoodsApplyAuditList(params) {
  return request.post(`${serverUrl}/mdc/goods_rf/goodsrfverifyinfo/list`, {
    data: params,
  });
}
