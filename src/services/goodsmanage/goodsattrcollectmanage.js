/**
 * [属性集管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-属性集(分页)
 */
export async function getGoodsAttrCollect(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsattrcollection/list`, { data: params });
}

/**
 * 获取-属性集所有
 */
export async function getGoodsAttrCollectAll(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsattrcollection/listAll`, { data: params });
}

/**
 * 新增-属性集
 */
export async function addGoodsAttrCollect(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsattrcollection`, { data: params });
}

/**
 * 更新-属性集
 */
export async function updateGoodsAttrCollect(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsattrcollection/${params.id}`, { data: params });
}

/**
 * 更新-属性集状态(启用/停用)
 */
export async function updateGoodsAttrCollectEnable(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsattrcollection/${params.id}`, { data: params });
}

/**
 * 分页-属性集
 * @param {*} parms
 */
export async function paginationGoodsAttrCollect(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsattrcollection/list`, { data: params });
}
