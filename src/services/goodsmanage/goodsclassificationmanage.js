/**
 * [产品大类管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-产品大类(所有)
 */
export async function getGoodsClassificationAll(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsclassification/listAll`, { data: params });
}

/**
 * 获取-产品大类(列表/分页)
 * @param {*} params
 */
export async function paginationGoodsClassification(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsclassification/list`, { data: params });
}

/**
 * 新增-产品大类
 */
export async function addGoodsClassification(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsclassification`, { data: params });
}

/**
 * 更新-产品大类
 */
export async function updateGoodsClassification(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsclassification/${params.id}`, { data: params });
}

// 更新-产品大类状态
export async function updatedGoodsClassificationStatus(parmas) {
  return request.get(`${serverUrl}/mdc/goods/goodsclassification/${parmas.id}/${parmas.enable}`);
}
