/**
 * [标签管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-标签类型
 */
export async function getGoodsTagType(params) {
  return request.post(`${serverUrl}/mdc/goods/goodstagtype/list`, { data: params });
}

/**
 * 新增-标签类型
 */
export async function addGoodsTagType(params) {
  return request.post(`${serverUrl}/mdc/goods/goodstagtype`, { data: params });
}

/**
 * 更新-标签类型
 */
export async function updateGoodsTagType(params) {
  return request.put(`${serverUrl}/mdc/goods/goodstagtype/${params.id}`, { data: params });
}

/**
 * 更新-标签状态
 */
export async function updateGoodsTagTypeEnable(params) {
  return request.put(`${serverUrl}/mdc/goods/goodstagtype/${params.id}`, { data: params });
}

/**
 * 删除-标签状态
 */
export async function deleteGoodsTagType(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodstagtype/${idArray}`);
}

/**
 * 分页-标签类型
 * @param {*} params
 */
export async function paginationGoodsTagType(params) {
  return request.post(`${serverUrl}/mdc/goods/goodstagtype/list`, { data: params });
}

/**
 * 获取-当前选择-标签类型下标签值定义数据(所有)
 */
export async function getCurrentSelectGoodsTagDefineAll(params) {
  return request.post(`${serverUrl}/mdc/goods/goodstagdefine/getCurrentSelectGoodsTagDefineAll`, {
    data: params,
  });
}

/**
 * 新增-标签值定义
 * @param {params} params
 */
export async function addGoodsTagDefine(params) {
  return request.post(`${serverUrl}/mdc/goods/goodstagdefine`, { data: params });
}

/**
 * 更新-标签值定义
 * @param {params} params
 */
export async function updateGoodsTagDefine(params) {
  return request.put(`${serverUrl}/mdc/goods/goodstagdefine/${params.id}`, { data: params });
}

/**
 * 删除-标签值定义
 */
export async function deleteGoodsTagDefine(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodstagdefine/${idArray}`);
}
