/**
 * [产品信息管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-该产品是否已被商品引用状态
 */
export async function getSpuBeAdoptedStatus(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuinfo/verifySpuBeAdopted`, { data: params });
}

/**
 * 获取-产品信息(列表/分页)
 * @param {*} params
 */
export async function paginationGoodsSpuInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuinfo/list`, { data: params });
}

/**
 * 新增-产品信息
 */
export async function addGoodsSpuInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuinfo`, { data: params });
}

/**
 * 更新-产品信息
 */
export async function updateGoodsSpuInfo(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsspuinfo/${params.id}`, { data: params });
}

/**
 * 获取-产品普通属性(列表/分页)
 * @param {*} params
 */
export async function paginationGoodsSpuAttrNormal(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrnormal/list`, { data: params });
}

/**
 * 新增-产品普通属性
 * @param {*} params
 */
export async function addGoodsSpuAttrNormal(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrnormal`, { data: params });
}

/**
 * 更新-产品普通属性
 * @param {*} params
 */
export async function updateGoodsSpuAttrNormal(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsspuattrnormal/${params.id}`, { data: params });
}

/**
 * 删除-产品普通属性
 * @param {*} idArray
 */
export async function deleteGoodsSpuAttrNormal(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsspuattrnormal/${idArray}`);
}

/**
 * 获取-产品SKU属性(列表/分页)
 */
export async function paginationGoodsSpuAttrSku(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrsku/list`, { data: params });
}

/**
 * 获取-产品下产品SKU属性(列表/所有)
 */
export async function getSpuCondWithGoodsSpuAttrSkuList(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrsku/getGoodsSpuAttrSkuAll`, {
    data: params,
  });
}

/**
 * 新增-产品SKU属性
 * @param {*} params
 */
export async function addGoodsSpuAttrSku(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrsku`, { data: params });
}

/**
 * 更新-产品SKU属性
 * @param {*} params
 */
export async function updateGoodsSpuAttrSku(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsspuattrsku/${params.id}`, { data: params });
}

/**
 * 删除-产品SKU属性
 * @param {*} idArray
 */
export async function deleteGoodsSpuAttrSku(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsspuattrsku/${idArray}`);
}

/**
 * 获取-产品SKU属性项值(列表/分页)
 */
export async function paginationGoodsSpuAttrSkuItem(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrskuitem/list`, { data: params });
}

/**
 * 新增-产品SKU属性项值
 * @param {*} params
 */
export async function addGoodsSpuAttrSkuItem(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrskuitem`, { data: params });
}

/**
 * 更新-产品SKU属性项值
 * @param {*} params
 */
export async function updateGoodsSpuAttrSkuItem(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsspuattrskuitem/${params.id}`, { data: params });
}

/**
 * 删除-产品SKU属性项值
 * @param {*} idArray
 */
export async function deleteGoodsSpuAttrSkuItem(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsspuattrskuitem/${idArray}`);
}

/**
 * 获取-产品描述(列表,分页)
 * @param {*} params
 */
export async function paginationGoodsSpuDesc(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspudesc/list`, { data: params });
}

/**
 * 新增-产品描述
 * @param {*} params
 */
export async function addGoodsSpuDesc(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspudesc`, { data: params });
}

/**
 * 更新-产品描述
 * @param {*} params
 */
export async function updateGoodsSpuDesc(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsspudesc/${params.id}`, { data: params });
}

/**
 * 删除-产品描述
 * @param {*} idArray
 */
export async function deleteGoodsSpuDesc(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsspudesc/${idArray}`);
}

/**
 * 获取-产品型号定义(列表/分页)
 * @param {*} params
 */
export async function paginationGoodsSpuModel(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspumodel/list`, { data: params });
}

/**
 * 新增-产品型号定义
 * @param {*} params
 */
export async function addGoodsSpuModel(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspumodel`, { data: params });
}

/**
 * 更新-产品型号定义
 * @param {*} params
 */
export async function updateGoodsSpuModel(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsspumodel/${params.id}`, { data: params });
}

/**
 * 删除-产品型号定义
 * @param {*} idArray
 */
export async function deleteGoodsSpuModel(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsspumodel/${idArray}`);
}

/**
 * 获取-商品分组(列表/分页)
 * @param {*} params
 */
export async function paginationGoodsSkuGroup(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskugroup/list`, { data: params });
}

/**
 * 新增-商品分组
 */
export async function addGoodsSkuGroup(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskugroup`, { data: params });
}

/**
 * 更新-商品分组
 */
export async function updateGoodsSkuGroup(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsskugroup/${params.id}`, { data: params });
}
