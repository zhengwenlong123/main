/**
 * [产品类目管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-当前产品类目父级产品类目数据对象
 */
export async function getCurrentSelectCatagoryParentNode(params) {
  return request.post(`${serverUrl}/mdc/goods/goodscatagory/getCurrentSelectCatagoryParentNode`, {
    data: params,
  });
}

/**
 * 获取-当前选择-节点下产品类目数据所有
 */
export async function getCurrentSelectCatagoryChildNode(params) {
  return request.post(`${serverUrl}/mdc/goods/goodscatagory/getCurrentSelectCatagoryChildNode`, {
    data: params,
  });
}

/**
 * 获取-产品类目1级、2级、3级数据(树结构/所有)
 */
export async function getGoodsCatagoryWith3LevelAll(params) {
  return request.get(
    `${serverUrl}/mdc/goods/goodscatagory/getGoodsCatagoryWith3LevelAll/${params.tid}`
  );
}

/**
 * 新增-产品类目
 */
export async function addGoodsCatagory(params) {
  return request.post(`${serverUrl}/mdc/goods/goodscatagory`, { data: params });
}

/**
 * 更新-产品类目
 */
export async function updateGoodsCatagory(params) {
  return request.put(`${serverUrl}/mdc/goods/goodscatagory/${params.id}`, { data: params });
}

/**
 * 更新-产品类目状态
 */
export async function updateGoodsCatagoryEnable(enable) {
  return request.put(`${serverUrl}/`, { params: enable });
}

/**
 * 删除-产品类目
 */
export async function deleteGoodsCatagory(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodscatagory/${idArray}`);
}

/**
 * 获取-产品类目内部属性集
 */
export async function getGoodsCatagoryAttrCollectionByGoodsCategory(goodsCatagory) {
  return request.post(
    `${serverUrl}/mdc/goods/goodscatagoryattrcollection/getGoodsCatagoryAttrCollectionByGoodsCategory`,
    { data: goodsCatagory }
  );
}

/**
 * 新增-产品类目内部属性集
 */
export async function addGoodsCatagoryAttrCollectionByGoodsCategory(params) {
  return request.post(`${serverUrl}/mdc/goods/goodscatagoryattrcollection`, { data: params });
}

/**
 * 更新-产品类目内部属性集
 */
export async function updateGoodsCatagoryAttrCollectionByGoodsCategory(params) {
  return request.put(`${serverUrl}/mdc/goods/goodscatagoryattrcollection/${params.id}`, {
    data: params,
  });
}

/**
 * 删除-产品类目内部属性集
 */
export async function deleteGoodsCatagoryAttrCollectionByGoodsCategory(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodscatagoryattrcollection/${idArray}`);
}
