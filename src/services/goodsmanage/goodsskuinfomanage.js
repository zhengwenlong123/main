/**
 * [商品信息管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-根据产品信息条件模糊查询产品信息（列表）
 */
export async function getSpuListBySpu(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuinfo/getGoodsSpuInfoAll`, { data: params });
}

/**
 * 获取-根据产品代码查询商品分组数据(列表)
 */
export async function getCurrentSpuCondOfGoodsSkuGroupList(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskugroup/getGoodsSkuGroupAll`, { data: params });
}

/**
 * 获取-根据产品代码查询产品型号数据(列表)
 */
export async function getCurrentSpuCondOfGoodsSpuModelList(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspumodel/getGoodsSpuModelAll`, { data: params });
}

/**
 * 获取-根据产品代码查询产品普通属性数据(列表)
 */
export async function getCurrentSpuCondOfGoodsSpuAttrNormalList(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrnormal/getGoodsSpuAttrNormalAll`, {
    data: params,
  });
}

/**
 * 获取-根据产品代码查询产品sku属性数据(列表)以及产品sku属性项值数据(列表)
 */
export async function getCurrentSpuCondOfGoodsSpuAttrSkuListWithChildAttrSkuItemList(params) {
  return request.post(
    `${serverUrl}/mdc/goods/goodsspuattrsku/getGoodsSpuAttrSkuWithChildAttrSkuItemAll`,
    { data: params }
  );
}

/**
 * 获取-根据产品sku属性条件获取到产品sku属性项值数据(列表)
 */
export async function getGoodsSpuAttrSkuItemListByGoodsSpuAttrSku(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrskuitem/getGoodsSpuAttrSkuItemAll`, {
    data: params,
  });
}

/**
 * 获取-根据时间获取最新商品信息1条数据对象
 */
export async function getLatestGoodsSkuInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskuinfo/getLatestGoodsSkuInfo`, {
    data: params,
  });
}

/**
 * 新增-商品信息
 */
export async function addGoodsSkuInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskuinfo`, { data: params });
}
// 附加sku属性及普通属性新增
export async function addGoodsSkuAllInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskuinfo/addGoodsSkuInfo`, { data: params });
}

/**
 * 更新-商品信息
 */
export async function updateGoodsSkuInfo(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsskuinfo/${params.id}`, { data: params });
}

/**
 * 更新-商品信息状态
 */
export async function updateGoodsSkuInfoEnable(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsskuinfo/${params.id}`, { data: params });
}

/**
 * 分页-商品信息
 * @param {*} params
 */
export async function paginationGoodsSkuInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskuinfo/list`, { data: params });
}

/**
 * 当前选择-商品(单品)商品普通属性(列表)
 * @param {object} params
 */
export async function getCurrentSelectGoodsSkuAttrNormal(params) {
  return request.post(
    `${serverUrl}/mdc/goods/goodsskuattrnormal/getCurrentSelectGoodsSkuAttrNormal`,
    { data: params }
  );
}

/**
 * 新增-商品(单品)商品普通属性
 * @param {object} params
 */
export async function addGoodsSkuAttrNormal(params) {
  if (params instanceof Array) {
    params.forEach(item => {
      request.post(`${serverUrl}/mdc/goods/goodsskuattrnormal`, { data: item });
    });
    return {};
  }
  return request.post(`${serverUrl}/mdc/goods/goodsskuattrnormal`, { data: params });
}

/**
 * 更新-商品(单品)商品普通属性
 * @param {object} params
 */
export async function updateGoodsSkuAttrNormal(params) {
  if (params instanceof Array) {
    params.forEach(item => {
      request.put(`${serverUrl}/mdc/goods/goodsskuattrnormal/${item.id}`, { data: item });
    });
    return {};
  }
  return request.put(`${serverUrl}/mdc/goods/goodsskuattrnormal/${params.id}`, { data: params });
}

/**
 * 删除-当前选择-商品(单品)商品普通属性
 * @param {*} idArray
 */
export async function deleteGoodsSkuAttrNormal(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsskuattrnormal/${idArray}`);
}

/**
 * 获取-当前选择-商品(单品)商品SKU属性
 * @param {object} params
 */
export async function getCurrentSelectGoodsSkuAttrSku(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskuattrsku/getCurrentSelectGoodsSkuAttrSku`, {
    data: params,
  });
}

/**
 * 新增-当前选择-商品(单品)商品SKU属性
 */
export async function addGoodsSkuAttrSku(params) {
  if (params instanceof Array) {
    params.forEach(item => {
      request.post(`${serverUrl}/mdc/goods/goodsskuattrsku`, { data: item });
    });
    return {};
  }
  return request.post(`${serverUrl}/mdc/goods/goodsskuattrsku`, { data: params });
}

/**
 * 更新-当前选择-商品(单品)商品SKU属性
 */
export async function updateGoodsSkuAttrSku(params) {
  if (params instanceof Array) {
    params.forEach(item => {
      request.put(`${serverUrl}/mdc/goods/goodsskuattrsku/${item.id}`, { data: item });
    });
    return {};
  }
  return request.put(`${serverUrl}/mdc/goods/goodsskuattrsku/${params.id}`, { data: params });
}

/**
 * 删除-当前选择-商品(单品)商品SKU属性
 */
export async function deleteGoodsSkuAttrSku(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsskuattrsku/${idArray}`);
}

/**
 * 获取-当前选择-商品(单品)商品描述
 * @param {object} params
 */
export async function paginationGoodsSkuDesc(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskudesc/list`, { data: params });
}

/**
 * 新增-当前选择-商品(单品)商品描述
 * @param {object} params
 */
export async function addGoodsSkuDesc(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskudesc`, { data: params });
}

/**
 * 更新-当前选择-商品(单品)商品描述
 * @param {object} params
 */
export async function updateGoodsSkuDesc(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsskudesc/${params.id}`, { data: params });
}

/**
 * 删除-当前选择-商品(单品)商品描述
 * @param {array} idArray
 */
export async function deleteGoodsSkuDesc(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsskudesc/${idArray}`);
}

/**
 * 获取-当前选择-商品(单品)商品物料信息
 */
export async function paginationGoodsMaterielInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsmaterielinfo/list`, { data: params });
}

/**
 * 新增-当前选择-商品(单品)商品物料信息
 */
export async function addGoodsMaterielInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsmaterielinfo`, { data: params });
}

/**
 * 更新-当前选择-商品(单品)商品物料信息
 */
export async function updateGoodsMaterielInfo(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsmaterielinfo/${params.id}`, { data: params });
}

/**
 * 删除-当前选择-商品(单品)商品物料信息
 */
export async function deleteGoodsMaterielInfo(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsmaterielinfo/${idArray}`);
}
