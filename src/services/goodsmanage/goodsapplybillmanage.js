/**
 * [产品申请单] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-产品申请单列表(分页/列表)
 */
export async function getGoodsApplyBillList(params) {
  return request.post(`${serverUrl}/mdc/goods_rf/goodsrfinfo/list`, { data: params });
}

/**
 * 新增-产品申请单
 */
export async function addGoodsApplyBill(params) {
  return request.post(`${serverUrl}/mdc/goods_rf/goodsrfoperate2`, { data: params });
}

/**
 * 获取-产品申请单
 */
export async function getGoodsApplyBill(params) {
  return request.post(`${serverUrl}/mdc/goods_rf/goodsrfoperate2/getGoodsRfOperate`, {
    data: params,
  });
}

/**
 * 修改-产品申请单
 */
export async function editGoodsApplyBill(params) {
  return request.put(`${serverUrl}/mdc/goods_rf/goodsrfoperate2`, { data: params });
}

/**
 * 获取-正式系统产品信息列表(所有)
 */
export async function getGoodsSpuInfoAll(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuinfo/getGoodsSpuInfoAll`, { data: params });
}

/**
 * 验证产品名称数据唯一性
 */
export async function verifyUniqueBySpuName(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuinfo/verifyUniqueBySpuName`, {
    data: params,
  });
}

/**
 * 获取-产品类目所有数据（树形）
 */
export async function getGoodsCatagoryListAll(params) {
  return request.post(`${serverUrl}/mdc/goods/goodscatagory/listAll`, { data: params });
}

/**
 * 获取-产品大类（所有）
 */
export async function getGoodsClassificationListAll(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsclassification/listAll`, { data: params });
}

/**
 * 获取-产品品牌（所有）
 */
export async function getGoodsbrandListAll(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsbrand/listAll`, { data: params });
}

/**
 * 获取-属性名称（所有）
 */
export async function getGoodsAttrNameAll(tip) {
  return request.get(`${serverUrl}/mdc/goods/goodsattrname/getGoodsAttrNameAll/${tip}`);
}

/**
 * 获取ebs分类（所有）
 * @param {*} params
 */
export async function getGoodsebscatagoryListAll(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsebscatagory/listAll`, { data: params });
}

/**
 * 修改产产品申请单信息
 * @param {*} params
 */
export async function modifyGoodsRfInfo(params) {
  const { id } = params;
  return request.put(`${serverUrl}/mdc/goods_rf/goodsrfinfo/${id}`, { data: params });
}

/**
 * 删除产品申请单
 * @param {*} params
 */
export async function deleteGoodsRfInfo(idArray) {
  const ids = idArray.join(',');
  return request.delete(`${serverUrl}/mdc/goods_rf/goodsrfinfo/${ids}`);
}
