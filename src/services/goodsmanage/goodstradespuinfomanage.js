/**
 * [交易产品信息管理] service层
 */
import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-产品信息(列表/分页)
 * @param {*} params
 */
export async function paginationGoodsTradeSpuInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuinfo/list`, { data: params });
}

/**
 * 更新-产品信息
 */
export async function updateGoodsTradeSpuInfo(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsspuinfo/${params.id}`, { data: params });
}

/**
 * 获取-产品信息
 */
export async function getGoodsTradeSpuInfo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsinfomanage/getGoodsOperate`, {
    data: params,
    requestType: 'form',
  });
}

/**
 * 新增-普通属性(产品)
 */
export async function addGoodsTradeSpuCommonAttr(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspuattrnormal`, { data: params });
}

/**
 * 修改-普通属性(产品)
 */
export async function modifyGoodsTradeSpuCommonAttr(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsspuattrnormal/${params.id}`, { data: params });
}

/**
 * 删除-普通属性(产品)
 */
export async function deleteGoodsTradeSpuCommonAttr(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsspuattrnormal/${idArray}`);
}

/**
 * 新增-描述(产品)
 */
export async function addGoodsTradeSpuDesc(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsspudesc`, { data: params });
}

/**
 * 修改-描述(产品)
 */
export async function modifyGoodsTradeSpuDesc(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsspudesc/${params.id}`, { data: params });
}

/**
 * 删除-描述(产品)
 */
export async function deleteGoodsTradeSpuDesc(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsspudesc/${idArray}`);
}

/**
 * 根据产品代码、商品代码查询商品相关信息
 */
export async function getGoodsSkuInfoEnhance(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsinfomanage/getGoodsSkuInfoEnhance`, {
    data: params,
    requestType: 'form',
  });
}
/**
 * 更新-商品信息
 */
export async function updateGoodsTradeSkuInfo(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsskuinfo/${params.id}`, { data: params });
}

/**
 * 新增-普通属性(商品)
 */
export async function addGoodsTradeSkuCommonAttr(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskuattrnormal`, { data: params });
}

/**
 * 修改-普通属性(商品)
 */
export async function modifyGoodsTradeSkuCommonAttr(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsskuattrnormal/${params.id}`, { data: params });
}

/**
 * 删除-普通属性(商品)
 */
export async function deleteGoodsTradeSkuCommonAttr(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsskuattrnormal/${idArray}`);
}
/**
 * 新增-描述(商品)
 */
export async function addGoodsTradeSkuDesc(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsskudesc`, { data: params });
}

/**
 * 修改-描述(商品)
 */
export async function modifyGoodsTradeSkuDesc(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsskudesc/${params.id}`, { data: params });
}

/**
 * 删除-描述(商品)
 */
export async function deleteGoodsTradeSkuDesc(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsskudesc/${idArray}`);
}

/**
 * 修改-物料
 */
export async function modifyGoodsMaterielInfo(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsmaterielinfo/${params.id}`, { data: params });
}
