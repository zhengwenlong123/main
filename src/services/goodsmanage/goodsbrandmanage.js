/**
 * [品牌管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-产品品牌(所有)
 */
export async function getGoodsBrandAll(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsbrand/listAll`, { data: params });
}

/**
 * 新增-产品品牌
 */
export async function addGoodsBrand(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsbrand`, { data: params });
}

/**
 * 更新-产品品牌
 */
export async function updateGoodsBrand(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsbrand/${params.id}`, { data: params });
}

/**
 * 更新-产品品牌状态
 */
export async function updateGoodsBrandEnable(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsbrand/${params.id}`, { data: params });
}

/**
 * 分页-产品品牌(列表)
 * @param {*} params
 */
export async function paginationGoodsBrand(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsbrand/list`, { data: params });
}

/**
 * 分页-当前选择-产品品牌下产品品牌描述数据(所有,分页)
 */
export async function getCurrentSelectGoodsBrandDescByBrandCode(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsbranddesc/list`, { data: params });
}

/**
 * 新增-产品品牌描述数据
 */
export async function addGoodsBrandDesc(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsbranddesc`, { data: params });
}

/**
 * 更新-产品品牌描述数据
 */
export async function updateGoodsBrandDesc(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsbranddesc/${params.id}`, { data: params });
}

/**
 * 删除-产品品牌描述数据
 */
export async function deleteGoodsBrandDesc(idArray) {
  return request.delete(`${serverUrl}/mdc/goods/goodsbranddesc/${idArray}`);
}
