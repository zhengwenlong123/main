/**
 * [属性名称管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-属性名称(列表/分页)
 */
export async function paginationGoodsAttrName(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsattrname/list`, { data: params });
}

/**
 * 新增-属性名称
 */
export async function addGoodsAttrName(params) {
  return request.post(`${serverUrl}/mdc/goods/goodsattrname`, { data: params });
}

/**
 * 更新-属性名称
 */
export async function updateGoodsAttrName(params) {
  return request.put(`${serverUrl}/mdc/goods/goodsattrname/${params.id}`, { data: params });
}

/**
 * 获取-属性名称(列表/所有)(产品普通属性/产品SKU属性 数据都来源于属性名称表进行选择)
 * @param {object} params
 */
export async function getGoodsAttrNameAll(params) {
  return request.get(`${serverUrl}/mdc/goods/goodsattrname/getGoodsAttrNameAll/${params.tid}`);
}
