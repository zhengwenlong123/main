/**
 * [物料序号信息] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取序号控制列表 - 分页
 */
export default async function paginationGoodsLastNo(params) {
  return request.post(`${serverUrl}/mdc/goods/goodslastno/list`, { data: params });
}
