/**
 * [系统帐户管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取系统帐号数据(支持分页操作)
 */
export async function getSysAccount(params) {
  return request.post(`${serverUrl}/application/security/account/list`, { data: params });
}

/**
 * 添加-系统帐号
 */
export async function addSysAccount(params) {
  return request.post(`${serverUrl}/application/security/account`, { data: params });
}

/**
 * 更新-系统帐号
 */
export async function updateSysAccount(params) {
  return request.put(`${serverUrl}/application/security/account/${params.id}`, { data: params });
}

/**
 * 删除-系统帐号
 * @param {array} idArray
 */
export async function deleteSysAccount(idArray) {
  return request.delete(`${serverUrl}/application/security/account/${idArray}`);
}
