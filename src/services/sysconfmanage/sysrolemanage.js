/**
 * [系统角色管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 根据租户ID-获取系统角色数据(支持分页操作)
 */
export async function getSysRole(params) {
  return request.post(`${serverUrl}/application/security/role/list`, { data: params });
}

/**
 * 添加-系统角色
 */
export async function addSysRole(params) {
  return request.post(`${serverUrl}/application/security/role`, { data: params });
}

/**
 * 更新-系统角色
 */
export async function updateSysRole(params) {
  return request.put(`${serverUrl}/application/security/role/${params.id}`, { data: params });
}

/**
 * 删除-系统角色
 * @param {array} idArray
 */
export async function deleteSysRole(idArray) {
  return request.delete(`${serverUrl}/application/security/role/${idArray}`);
}

/**
 * 获取应用所有(角色-所属应用列表)
 */
export async function getAppAll(params) {
  return request.post(`${serverUrl}/platform/platform/application/app/list`, { data: params });
}

/**
 * 获取分配权限数据所有(树形结构)
 */
export async function getAllocateResource(params) {
  return request.post(`${serverUrl}/application/security/roleResourceRelation`, { data: params });
}

/**
 * 获取分配权限-子节点数据所有(树形结构)
 */
export async function getAllocateResourceChild(params) {
  return request.post(`${serverUrl}/application/security/roleResourceRelation/childNodes`, {
    data: params,
  });
}
