/* eslint-disable */
/**
 * [个人设置] service层
 */

import request from '@/utils/request';
import { appUrl } from '@/defaultSettings';

/**
 * 更新-账户密码
 */
export async function updateAccountPassword2(params) {
  return request.post(`${appUrl}/security/login/updatePassword`, { data: params });
}
