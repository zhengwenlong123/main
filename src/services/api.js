export default {
  // 系统登入
  AccountLogin: { method: 'post', url: '/security/login/authenticate', requestType: 'form' },
  // 系统登出
  AccountLogout: { method: 'post', url: '/logout', requestType: 'form' },
  // 获取忘记密码手机验证码
  GetPhoneCode: {
    method: 'post',
    url: '/_public/application/security/passwordReset/getVerificationCode',
    requestType: 'form',
  },
  // 重新获取忘记密码手机验证码
  GetPhoneCodeAgain: {
    method: 'post',
    url: '/_public/application/security/passwordReset/repeatSendVerificationCode',
    requestType: 'form',
  },
  // 验证验证码是否有效
  VerificationPhoneCode: {
    method: 'post',
    url: '/_public/application/security/passwordReset/verifyVerificationCode',
    requestType: 'form',
  },
  // 重置密码
  ModifyPassword: {
    method: 'put',
    url: '/_public/application/security/passwordReset/modifyPassword',
    requestType: 'form',
  },
  // 修改密码
  UpdateAccountPassword: { method: 'post', url: '/security/login/updatePassword' },

  // 取得当前登入账号的信息
  CurrentAccount: { method: 'post', url: '/current/account' },
  // 取得当前App信息
  CurrentApp: { method: 'post', url: '/current/app' },
  // 取得当前人的菜单权限
  AccountResource: { method: 'post', url: '/security/login/getUserResource' },
  // 取得当前人能使用的APP列表
  CanUseApps: { method: 'get', url: '/application/personal/platform/config' },
  // 通过指定AppCode得到token
  GetAppToken: { method: 'post', url: '/security/jump/:appCode' },
  // 通过token获取跳转session
  TokenLogin: { method: 'post', url: '/security/login/jump', requestType: 'form' },

  // 表格下载
  DownloadExcel: { method: 'post', url: '/order/split_cargo/excel/make', responseType: 'blob' },

  // 表格上传
  UploadExcel: { method: 'post', url: '/order/split_cargo/excel/parse', requestType: 'form' },

  // // 图片上传
  // UploadImg: { method: 'post', url: '/order/split_cargo/excel/parse', requestType: 'form' },

  // 获取供应商列表
  GetSupplierList: {
    method: 'get',
    url: '/provincepurchase/api/supplier/supplierList',
    isMock: true,
  },

  // 获取供应商列表下的销售目录
  GetSupplierCatalog: {
    method: 'get',
    url: '/provincepurchase/api/supplier/:selectSupplierId/catalog',
    isMock: true,
  },

  // 获取商品明细
  GetGoodsDetail: {
    method: 'get',
    url: '/provincepurchase/api/supplier/:selectSupplierId/productDetail/:selectProductId',
    isMock: true,
  },

  // 取得指定供应商下的收货地址列表
  GetSupplierAddresses: {
    method: 'get',
    url: '/provincepurchase/api/supplier/:supplierid/registeredAddress',
  },

  // 取得指定供应商下的区域经理列表
  GetSupplierManagers: {
    method: 'get',
    url: '/provincepurchase/api/supplier/:supplierid/areaManage',
  },

  // 提交省包下单信息
  SubmitlPurchase: {
    method: 'post',
    url: '/provincepurchase/api/supplier/:selectSupplierId/createOrder',
  },

  // 获取支付初始化页面数据
  GetPayInitialization: {
    method: 'get',
    url: '/provincepurchase/api/payment/paymentPageInit/:id',
    isMock: true,
  },

  // 提交订单支付数据
  SubmitOrderPay: {
    method: 'post',
    url: '/provincepurchase/api/payment/payment',
    isMock: true,
  },

  // 获取订单详情基本信息
  GetOrderBaseMsg: {
    method: 'get',
    url: '/provincepurchase/api/po/getOrderMainDetail/:id',
    isMock: true,
  },
  // 获取订单详情下的tab订单详情
  GetOrderDetail: {
    method: 'get',
    url: '/provincepurchase/api/po/getOrderTabDetail/:id',
    isMock: true,
  },
  // 获取订单详情下的tab付款详情
  GetPayDetail: {
    method: 'get',
    url: '/provincepurchase/api/po/getPayTabDetail/:id',
    isMock: true,
  },
  // 获取订单详情下的tab操作日志详情
  GetEditLogDetail: {
    method: 'get',
    url: '/provincepurchase/api/po/getOperateLogTabDetail/:id',
    isMock: true,
  },
  // 获取订单详情下的tab物流信息详情
  GetLogisticsDetail: {
    method: 'get',
    url: '/provincepurchase/api/po/getShipTabDetail/:id',
    isMock: true,
  },

  // 获取订单商品汇总列表
  GetGoodsList: {
    method: 'get',
    url: '/provincepurchase/api/po/getOrderGoodsList/:id',
    isMock: true,
  },

  // 获取收货地址商品明细列表
  GetOrderAddrReceiveDetail: {
    method: 'get',
    url: '/provincepurchase/api/po/getOrderAddrReceiveDetail/:id',
    isMock: true,
  },

  // 提交退货
  SubmitReturn: {
    method: 'post',
    url: '/provincepurchase/api/reversionOrder/createReversionOrder',
    isMock: true,
  },

  // 取得参数(订单状态,发运状态等)
  GetParamsByParamType: {
    method: 'get',
    url: '/provincepurchase/api/common/getParamsByParamType/:paramTypeCode',
    isMock: true,
  },
  // 取得查询搜索的状态
  GetPoSearchStatus: {
    method: 'post',
    url: '/provincepurchase/api/po/getPoSearchStatus',
    isMock: true,
  },

  // 取得市区树
  GetProvinceCityRegion: {
    method: 'get',
    url: '/provincepurchase/api/common/getProvinceCityRegion/:countryCode',
    isMock: true,
  },

  // 取得采购单列表
  GetPoList: {
    method: 'post',
    url: '/provincepurchase/api/po/poList',
    isMock: true,
  },
};
