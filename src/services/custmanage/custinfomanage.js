/**
 * [客户申请单管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-客户信息列表
 */
export async function getCustInfoList(params) {
  return request.post(`${serverUrl}/mdc/cust/custrfnfoentiremanage/companyInfolist`, {
    data: params,
  });
}

/**
 * 获取-客户信息详情
 */
export async function getCustInfoById(id) {
  return request.post(`${serverUrl}/mdc/cust/custrfnfoentiremanage/getCustCompanyInfo?id=${id}`);
}

/**
 * 更新-客户信息状态
 */
export async function updateCustInfoRfStatus(params) {
  return request.put(`${serverUrl}/mdc/cust/custrfnfoentiremanage/companyInfo/${params.id}`, {
    data: params,
  });
}

/**
 * 新增-客户实体ID
 */
export async function addCustOuId(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanyextinfo`, { data: params });
}

/**
 * 更新-客户信息状态
 */
export async function updateCustOuId(params) {
  return request.put(`${serverUrl}/mdc/cust/custcompanyextinfo/update/${params.custCode}`, {
    data: params,
  });
}
