/**
 * [客户CA记录管理组件] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-客户CA记录
 */
export async function getCustCaRecord(params) {
  return request.post(`${serverUrl}/mdc/cust/custcarecord/list`, { data: params });
}

/**
 * 新增-客户CA记录
 */
export async function addCustCaRecord(params) {
  return request.post(`${serverUrl}/mdc/cust/custcarecord`, { data: params });
}

/**
 * 更新-客户CA记录
 */
export async function updateCustCaRecord(params) {
  return request.put(`${serverUrl}/mdc/cust/custcarecord/${params.id}`, { data: params });
}

/**
 * 更新-客户CA记录状态
 */
export async function updateCustCaRecordEnable(params) {
  return request.put(`${serverUrl}/mdc/cust/custcarecord/${params.id}`, { data: params });
}

/**
 * 分页-客户CA记录
 * @param {*} params
 */
export async function paginationCustCaRecird(params) {
  return request.post(`${serverUrl}/mdc/cust/custcarecord/list`, { data: params });
}
