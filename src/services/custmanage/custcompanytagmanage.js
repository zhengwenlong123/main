/**
 * [企业标签管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-企业标签类型
 */
export async function getCustCompanyTagType(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanytagtype/list`, { data: params });
}

/**
 * 新增-企业标签类型
 */
export async function addCustCompanyTagType(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanytagtype`, { data: params });
}

/**
 * 更新-企业标签类型
 */
export async function updateCustCompanyTagType(params) {
  return request.put(`${serverUrl}/mdc/cust/custcompanytagtype/${params.id}`, { data: params });
}

/**
 * 更新-企业标签类型状态
 */
export async function updateCustCompanyTagTypeEnable(params) {
  return request.put(`${serverUrl}/mdc/cust/custcompanytagtype/${params.id}`, { data: params });
}

/**
 * 删除-企业标签类型
 */
export async function deleteCustCompanyTagType(idArray) {
  return request.delete(`${serverUrl}/mdc/cust/custcompanytagtype/${{ idArray }}`);
}

/**
 * 分页-企业标签类型
 * @param {*} params
 */
export async function paginationCustCompanyTagType(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanytagtype/list`, { data: params });
}

/**
 * 获取-企业标签值定义(所有)
 * @param {object} params
 */
export async function getCustCompanyTagDefineAll(params) {
  return request.post(
    `${serverUrl}/mdc/cust/custcompanytagdefine/getCustCompanyTagDefineByCustCompanyTagType`,
    { data: params }
  );
}

/**
 * 新增-企业标签值定义
 */
export async function addCustCompanyTagDefine(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanytagdefine`, { data: params });
}

/**
 * 更新-企业标签值定义
 */
export async function updateCustCompanyTagDefine(params) {
  return request.put(`${serverUrl}/mdc/cust/custcompanytagdefine/${params.id}`, { data: params });
}

/**
 * 更新-企业标签值定义状态
 */
export async function updateCustCompanyTagDefineEnable(params) {
  return request.put(`${serverUrl}/mdc/cust/custcompanytagdefine/${params.id}`, { data: params });
}

/**
 * 删除-企业标签值定义
 */
export async function deleteCustCompanyTagDefine(idArray) {
  return request.delete(`${serverUrl}/mdc/cust/custcompanytagdefine/${idArray}`);
}
