/**
 * [企业编码对照管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-企业编码对照
 */
export async function getCustCompanyMapping(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanymapping/list`, { data: params });
}

/**
 * 新增-企业编码对照
 */
export async function addCustCompanyMapping(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanymapping`, { data: params });
}

/**
 * 更新-企业编码对照
 */
export async function updateCustCompanyMapping(params) {
  return request.put(`${serverUrl}/mdc/cust/custcompanymapping/${params.id}`, { data: params });
}

/**
 * 更新-企业编码对照状态
 */
export async function updateCustCompanyMappingEnable(params) {
  return request.put(`${serverUrl}/mdc/cust/custcompanymapping/${params.id}`, { data: params });
}

/**
 * 分页-企业编码对照
 * @param {*} params
 */
export async function paginationCustCompanyMapping(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanymapping/list`, { data: params });
}
