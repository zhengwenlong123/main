/**
 * [客户申请单管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-客户申请单信息列表
 */
export async function getCustApplyList(params) {
  return request.post(`${serverUrl}/mdc/cust/custrfnfoentiremanage/list`, { data: params });
}

/**
 * 获取-客户申请单信息详情
 */
export async function getCustApplyInfoByRfId(params) {
  return request.post(
    `${serverUrl}/mdc/cust/custrfnfoentiremanage/getCustRfInfo?tid=${params.tid}&rfId=${params.id}`
  );
}

/**
 * 新增-客户申请单信息
 */
export async function addCustApplyInfo(params) {
  return request.post(`${serverUrl}/mdc/cust/custrfnfoentiremanage`, { data: params });
}

/**
 * 新增-客户申请单信息
 */
export async function deleteCustApplyInfo(id) {
  return request.delete(`${serverUrl}/mdc/cust/custrequestforminfo/${id}`);
}

/**
 * 更新-客户申请单信息
 */
export async function updateCustApplyInfo(params) {
  return request.put(`${serverUrl}/mdc/cust/custrfnfoentiremanage/updateCustRfInfo`, {
    data: params,
  });
}

/**
 * 更新-客户申请单状态
 */
export async function updateCustApplyInfoRfStatus(params) {
  return request.put(`${serverUrl}/mdc/cust/custrfnfoentiremanage/${params.id}`, { data: params });
}

/**
 * 获取-通过社会信用码查询客户申请单天眼查信息
 */
export async function getTianYCInfoByCompanyCode(param) {
  return request.get(
    `${serverUrl}/mdc/cust/tianyancha/companyinfo/${param.code}?sourceType=${param.sourceType}`
  );
}

/**
 * 获取-根据企业id条件查询经营异常列表
 */
export async function getTianYCAbnormalList(param) {
  return request.post(
    `${serverUrl}/mdc/cust/tianyancha/companyabnormal/list?id=${param.id}&pageNum=${param.pageNum}&pageSize=${param.pageSize}&sourceType=${param.sourceType}`
  );
}

/**
 * 获取-根据企业id条件查询企业风险
 */
export async function getTianYCRisk(param) {
  return request.get(
    `${serverUrl}/mdc/cust/tianyancha/companyrisk/${param.id}?sourceType=${param.sourceType}`
  );
}

/**
 * 审核-客户申请单信息--审核
 */
export async function auditCustApplyInfo(params) {
  return request.post(`${serverUrl}/mdc/cust/custrfverifyinfo`, { data: params });
}

/**
 * 获取-客户申请单审核信息列表
 */
export async function getCustApplyAuditList(params) {
  return request.post(`${serverUrl}/mdc/cust/custrfnfoentiremanage/verifylist`, { data: params });
}

/**
 * 审核-获取-某个客户申请单审核历史列表
 */
export async function getCustApplyAuditLogs(params) {
  return request.post(`${serverUrl}/mdc/cust/custrfverifyinfo/list`, { data: params });
}

/**
 * 获取-根据行政区划定义tid,countryCode,regionCode,parentRegionCode查询行政区划定义所有
 */
export async function getAddressByRegionCode(params) {
  return request.post(`${serverUrl}/mdc/parm/parmregion/getParmRegionByCode`, { data: params });
}

