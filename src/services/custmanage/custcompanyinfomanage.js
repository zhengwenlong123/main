/**
 * [企业信息管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-企业信息
 */
export async function getCustCompanyInfo(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanyinfo/list`, { data: params });
}

/**
 * 获取-企业信息(所有)
 */
export async function getCustCompanyInfoAll(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanyinfo/getCustCompanyInfoAll`, {
    data: params,
  });
}

/**
 * 新增-企业信息
 */
export async function addCustCompanyInfo(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanyinfo`, { data: params });
}

/**
 * 更新-企业信息
 */
export async function updateCustCompanyInfo(params) {
  return request.put(`${serverUrl}/mdc/cust/custcompanyinfo/${params.id}`, { data: params });
}

/**
 * 更新-企业信息状态
 */
export async function updateCustCompanyInfoEnable(params) {
  return request.put(`${serverUrl}/mdc/cust/custcompanyinfo/${params.id}`, { data: params });
}

/**
 * 分页-企业信息
 * @param {*} params
 */
export async function paginationCustCompanyInfo(params) {
  return request.post(`${serverUrl}/mdc/cust/custcompanyinfo/list`, { data: params });
}

/**
 * 获取行政区号
 */
export async function getParmRegionByReginCodesUsing(params) {
  return request.post(`${serverUrl}/mdc/parm/parmregion/getParmRegionByReginCodeList`, {
    data: params,
  });
}
