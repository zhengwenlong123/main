/**
 * [经营单位类型定义管理] service层
 */

import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/**
 * 获取-经营单位类型定义
 */
export async function getCustOperUnitType(params) {
  return request.post(`${serverUrl}/mdc/cust/custoperunittype/list`, { data: params });
}

/**
 * 获取-经营单位类型定义(所有)
 */
export async function getCustOperUnitTypeAll(params) {
  return request.post(`${serverUrl}/mdc/cust/custoperunittype/getCustOperUnitTypeAll`, {
    data: params,
  });
}

/**
 * 新增-经营单位类型定义
 */
export async function addCustOperUnitType(params) {
  return request.post(`${serverUrl}/mdc/cust/custoperunittype`, { data: params });
}

/**
 * 更新-经营单位类型定义
 */
export async function updateCustOperUnitType(params) {
  return request.put(`${serverUrl}/mdc/cust/custoperunittype/${params.id}`, { data: params });
}

/**
 * 更新-经营单位类型定义状态
 */
export async function updateCustOperUnitTypeEnable(params) {
  return request.put(`${serverUrl}/mdc/cust/custoperunittype/${params.id}`, { data: params });
}

/**
 * 分页-经营单位类型定义
 * @param {*} params
 */
export async function paginationCustOperUnitType(params) {
  return request.post(`${serverUrl}/mdc/cust/custoperunittype/list`, { data: params });
}

/**
 * 删除-经营单位类型定义
 */
export async function deleteCustOperUnitType(idArray) {
  return request.delete(`${serverUrl}/mdc/cust/custoperunittype/${idArray}`);
}
