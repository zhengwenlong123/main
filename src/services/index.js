/* eslint-disable no-restricted-syntax */
import request from '@/utils/request';
import api from './api';
import pathToRegexp from 'path-to-regexp';
import { cloneDeep, merge, has } from 'lodash';
import { notification } from 'antd';
import { appUrl } from '@/defaultSettings';

// 这里双剪头的意思是针对每一个ApiSetting 返回一个function, 而data是返回的funcion的参数
const gen = apiSetting => data => {
  try {
    const oneApi = cloneDeep(apiSetting);
    let { url } = oneApi;
    const { method } = oneApi;
    // 这里是fetch API原因 url中的参数在params, post body在data
    // 我们兼容在payload中直接写data或param, 或者不带
    // 如果既没有data也没有param, 要么data就是参数本身,要么是空
    let cloneData;
    if (!has(data, 'data') && !has(data, 'params')) {
      cloneData = method.toLocaleLowerCase() === 'get' ? { params: data } : { data };
    } else {
      cloneData = data;
    }
    const match = pathToRegexp.parse(url);
    // 删除掉匹配的参数
    if (cloneData.params) {
      url = pathToRegexp.compile(url)(cloneData.params);
      for (const item of match) {
        if (cloneData.params && item instanceof Object && item.name in cloneData.params) {
          delete cloneData.params[item.name];
        }
      }
    }
    if (cloneData.data) {
      url = pathToRegexp.compile(url)(cloneData.data);
      for (const item of match) {
        if (cloneData.data && item instanceof Object && item.name in cloneData.data) {
          delete cloneData.data[item.name];
        }
      }
    }

    delete oneApi.url;
    // 这里是fetch API原因 url中的参数在params, post body在data
    const finalParams = merge(cloneData, oneApi);

    // url = (isMock ? mockUrl : serverUrl) + url;

    url = appUrl + url; // 重新赋login地址

    return request(url, cloneDeep(finalParams));
  } catch (e) {
    notification.error({ message: e.message });
    return false;
  }
};

const APIFunction = {};
Object.keys(api).forEach(key => {
  APIFunction[key] = gen(api[key]);
});

export default APIFunction;
