import request from '@/utils/request';

export async function getProducts() {
  return request(`/API/Product`, {
    method: 'get',
  });
}

export async function getProductDetail({ id }) {
  return request(`/API/Product/${id}`, {
    method: 'get',
  });
}

export async function handelUploadFile(file) {
  const filedata = new FormData();
  filedata.append('file', file);
  return request(`/API/UploadFile`, {
    method: 'post',
    body: filedata,
  });
}

// 取得地址列表
export async function getAddress() {
  return request.get(`/API/Address`);
}
export async function getAreaManager() {
  return request.get(`/API/AreaManager`);
}

export async function getOrderDetail() {
  return request.get(`/API/OrderDetail`);
}

export async function getPayDetail() {
  return request.get(`/API/PayDetail`);
}

export async function getLogisticsDetail() {
  return request.get(`/API/LogisticsDetail`);
}

export async function getEditLogDetail() {
  return request.get(`/API/EditLogDetail`);
}

export async function getOrderBaseMsg() {
  return request.get(`/API/OrderBaseMsg`);
}

export async function getpayTypes() {
  return request.get(`/API/PayTypes`);
}

export async function getPurchaseInventory() {
  return request.get(`/API/PurchaseInventory`);
}
