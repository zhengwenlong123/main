/* eslint-disable import/prefer-default-export */
/**
 * [附件管理] service层
 */

import request from '@/utils/request';
import { fileUrl } from '@/defaultSettings';
import { fileRelationPathReg } from '@/utils/mdcutil';

/**
 * 附件上传
 * @param {File} 附件
 * @returns 返回Promise对象, 使用then/catch接收结果数据, 结果数据为附件上传后的相对路径, 上传失败返回null
 */
export function enclosureUploadPromise(enclosure) {
  return new Promise((resolve, reject) => {
    // console.log(`准备上传附件：${enclosure}`);

    let enclosureRelationUrl = null; // 附件相对路径
    if (enclosure instanceof File) {
      // 是文件
      const formData = new FormData();
      formData.append('file', enclosure);
      const reqPromise = request.post(`${fileUrl}/bootstrap/fileobject/upload/single`, {
        data: formData,
      });
      reqPromise
        .then(res => {
          if (res) {
            // console.log(`附件上传成功：${res}`);
            enclosureRelationUrl = res.url;
            resolve(enclosureRelationUrl); // 正常
          } else {
            reject(enclosureRelationUrl); // 异常
          }
        })
        .catch(() => {
          reject(enclosureRelationUrl); // 异常
        });
    } else {
      // 是地址
      enclosureRelationUrl = enclosure.replace(fileRelationPathReg, '');
      if (enclosureRelationUrl === '') enclosureRelationUrl = null;
      if (enclosureRelationUrl) {
        resolve(enclosureRelationUrl); // 正常
      } else {
        reject(enclosureRelationUrl); // 异常
      }
    }
  });
}
