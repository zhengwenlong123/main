module.exports = {
  navTheme: 'dark', // theme for nav menu
  primaryColor: '#1890FF', // primary color of ant design
  layout: 'sidemenu', // nav menu position: sidemenu or topmenu
  contentWidth: 'Fluid', // layout of content: Fluid or Fixed, only works when layout is topmenu
  fixedHeader: false, // sticky header
  autoHideHeader: false, // auto hide header
  fixSiderbar: false, // sticky siderbar

  // 下面开始是自定义新系统信息
  systemName: '主数据', // 系统的名字
  mockUrl: '', // http://120.77.246.176:21001/mock/165

  // serverUrl: 'http://127.0.0.1:21119', // 数据服务地址
  // loginUrl: 'http://47.107.155.39:22000', // 登录地址
  // fileUrl: 'http://47.107.155.39:22000', // 附件上传地址
  // accessFileUrl: 'http://47.106.247.208:21020', // 已上传成功附件直接访问地址

  serverUrl: '/serviceApi', // 数据服务地址
  appUrl: '/appApi', // 登录地址(原来loginUrl)
  fileUrl: '/appApi', // 附件上传地址
  accessFileUrl: '/attachment', // 已上传成功附件直接访问地址

  defaultPage: '/welcome', // 系统登入后的默认地址
  notAuthPages: ['/login', '/jumplogin', '/forgetPassword'], // 不受权限控制的页面列表
  alibabaFontIcont: '//at.alicdn.com/t/font_1143078_lmi8q5vvjhr.js',
  useAliCaptcha: true,
  disableSubLogin: false, // 是否关闭子站的登入. 如果为true则只会跳到loginUrl的地址
  loginUrl: '/login', // 子站登入地址是/login, 如果关闭子站登入必须http开头地址
};
