/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
// import moment from 'moment';
import { message, Button, Form, Input, Checkbox } from 'antd';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

/**
 * 标签类型-维护组件
 */
@ValidationFormHoc
class GoodsTagTypeUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      form: { getFieldsValue },
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
      modalSearchCondition,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.tagTypeEnable ? (values.tagTypeEnable = 1) : (values.tagTypeEnable = 0);

    // 维护
    if (mut === 2) {
      values.id = mue.id;
      dispatch({
        type: 'goodstagmanage/updateGoodsTagType',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    // 新增
    if (mut === 10) {
      dispatch({
        type: 'goodstagmanage/addGoodsTagType',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
    } = this.props;

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form {...formItemLayout}>
            {mut === 10 ? null : (
              <Form.Item label={formatMessage({ id: 'form.common.code' })}>
                {getFieldDecorator('tagTypeCode', { rules: [{ required: true }] })(
                  <Input disabled />
                )}
              </Form.Item>
            )}
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('tagTypeName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.alias' })}>
              {getFieldDecorator('tagTypeAlias', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.alias.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.description' })}>
              {getFieldDecorator('tagTypeDesc', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.description.placeholder' }),
                  },
                ],
              })(<Input.TextArea disabled={mut === 1} rows={3} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('tagTypeEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        tagTypeCode: Form.createFormField({ value: mue ? mue.tagTypeCode : '' }),
        tagTypeName: Form.createFormField({ value: mue ? mue.tagTypeName : '' }),
        tagTypeAlias: Form.createFormField({ value: mue ? mue.tagTypeAlias : '' }),
        tagTypeDesc: Form.createFormField({ value: mue ? mue.tagTypeDesc : '' }),
        tagTypeEnable: Form.createFormField({ value: mue ? mue.tagTypeEnable === 1 : true }),
      };
    },
  })(GoodsTagTypeUpkeep)
);
