/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Button, Input, Checkbox, message } from 'antd';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

/**
 * 商品标签值定义列表项-维护组件
 */
@ValidationFormHoc
class GoodsTagTableItemUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      validationForm,
      dispatch,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.tagEnable ? (values.tagEnable = 1) : (values.tagEnable = 0);

    // 维护
    if (mut === 2) {
      values.id = mue.id;
      dispatch({
        type: 'goodstagmanage/updateGoodsTagDefine',
        payload: values,
        callback: this.onCallback,
      });
    }
    // 新增
    if (mut === 10) {
      dispatch({
        type: 'goodstagmanage/addGoodsTagDefine',
        payload: values,
        callback: this.onCallback,
      });
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalUpkeepType: mut,
      modalVisibleOnChange,
      form: { getFieldDecorator },
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyTagTableItemUpkeep.companyTagTypeCode' })}
            >
              {getFieldDecorator('tagTypeCode', { rules: [{ required: true }] })(
                <Input disabled />
              )}
            </Form.Item>
            {mut === 10 ? null : (
              <Form.Item label={formatMessage({ id: 'form.common.code' })}>
                {getFieldDecorator('tagCode', { rules: [{ required: true }] })(<Input disabled />)}
              </Form.Item>
            )}
            <Form.Item label={formatMessage({ id: 'form.common.value' })}>
              {getFieldDecorator('tagValue', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.value.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsTagManage.tagShortDesc' })}>
              {getFieldDecorator('tagShortDesc')(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsTagManage.tagDetailDesc' })}>
              {getFieldDecorator('tagDetailDesc')(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('tagEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" htmlType="button" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button
                type="primary"
                htmlType="button"
                onClick={this.onClickSubmit}
                loading={confirmLoading}
              >
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue, tagTypeCode } = props;
      return {
        tagTypeCode: Form.createFormField({ value: mue ? mue.tagTypeCode : tagTypeCode || '' }),
        tagCode: Form.createFormField({ value: mue ? mue.tagCode : '' }),
        tagValue: Form.createFormField({ value: mue ? mue.tagValue : '' }),
        tagShortDesc: Form.createFormField({ value: mue ? mue.tagShortDesc : '' }),
        tagDetailDesc: Form.createFormField({ value: mue ? mue.tagDetailDesc : '' }),
        tagEnable: Form.createFormField({ value: mue ? mue.tagEnable === 1 : true }),
      };
    },
  })(GoodsTagTableItemUpkeep)
);
