/* eslint-disable no-nested-ternary */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import { formatMessage } from 'umi/locale';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Divider, message } from 'antd';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import GoodsTagTableItemUpkeep from './GoodsTagTableItemUpkeep';

const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 商品标签值定义列表维护
 */
class GoodsTagTableUpkoop extends MdcManageBasic {
  constructor(props) {
    super(props);
    // state
    this.state = {
      // Table行勾选数据对象
      selectedRowKeys: [],
      tableRowSelectionEntity: null,
    };
    // Table-表格行勾选处理
    // this.tableRowSelection = {
    //   type: 'radio',
    //   columnWidth: 30,
    //   columnTitle: '<>',
    //   selectedRowKeys: [],
    //   onChange: (selectedRowKeys, selectedRows) => {
    //     this.tableRowSelection.selectedRowKeys = selectedRowKeys;
    //     this.setState({ tableRowSelectionEntity: selectedRows[0] });
    //   }, // Table勾选回调事件
    // };
    // 表格列表字段定义
    this.tableColumns = [
      // { title: '标签类型代码', dataIndex: 'tagTypeCode', key: 'tagTypeCode', align: 'center'},
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'tagCode',
        key: 'tagCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.value' }),
        dataIndex: 'tagValue',
        key: 'tagValue',
        align: 'center',
      },
      // { title: '内容简略', dataIndex: 'tagShortDesc', key: 'tagShortDesc', align: 'center'},
      // { title: '内容详情', dataIndex: 'tagDetailDesc', key: 'tagDetailDesc', align: 'center'},
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'tagEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.tagEnable]}</span>,
      },
    ];
  }

  /**
   * 渲染组件-公共参数定义
   */
  render() {
    const { modalVisible, modalUpkeepType, modalUpkeepEntity, selectedRowKeys } = this.state;
    const {
      tagTypeCode, // 当前选择-商品标签类型代码
      currentSelectGoodsTagDefineAll, // 当前选择[商品标签类型]下[商品标签值定义]列表
    } = this.props;
    const dispatchType = 'goodstagmanage/updateGoodsTagDefine'; // 停用dispatchType
    return (
      <MdcManageBlock
        tableColumns={this.tableColumns}
        tableDataSource={currentSelectGoodsTagDefineAll}
        tableOperateAreaCmp={this._tableOperateAreaCmp(dispatchType)}
        tableRowSelection={{
          columnWidth: 30,
          columnTitle: '<>',
          selectedRowKeys,
          onSelect: (record, selected, selectedRows) => {
            if (selected) {
              this.setState({ tableRowSelectionEntity: record });
            } else if (selectedRows.length === 0) {
              this.setState({ tableRowSelectionEntity: null });
            }
          },
          onChange: selectedRowKeysArr => {
            this.setState({
              selectedRowKeys:
                selectedRowKeysArr.length > 1 ? [selectedRowKeysArr[1]] : selectedRowKeysArr,
            });
          }, // Table勾选回调事件
        }}
        tableTitle={formatMessage({ id: 'form.goodsTagTableUpkoop.goodsTagMaintain' })}
        modalVisible={modalVisible}
        modalTitle={formatMessage({ id: 'form.goodsTagTableUpkoop.goodsTagValue' })}
        modalContentCmp={
          <GoodsTagTableItemUpkeep
            tagTypeCode={tagTypeCode} // 在新增时使用(商品标签类型代码)
            modalUpkeepType={modalUpkeepType}
            modalUpkeepEntity={modalUpkeepEntity}
            modalVisibleOnChange={this.modalVisibleOnChange}
          />
        }
        modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
      />
    );
  }

  // Table-表格头部动作处理组件
  _tableOperateAreaCmp = (dispatchType = null) => {
    const { tableRowSelectionEntity: trse } = this.state;
    const { actionType } = this.props; // 商品标签类型的动作类型值,1为详情;2为维护;10为新增

    // 校验Table勾选tableRowSelectionEntity值是否存在
    const checkTableRowSelectionEntity = (flag = -1) => {
      if (trse == null || trse === undefined) {
        const tips =
          flag === 2
            ? formatMessage({ id: 'button.common.modify' })
            : flag === 20
            ? formatMessage({ id: 'button.common.delete' })
            : flag === 3
            ? formatMessage({ id: 'form.common.disabled' })
            : formatMessage({ id: 'button.common.handle' });
        message.warn(
          `${formatMessage({ id: 'form.goodsTagTableUpkoop.pleaseSelectData' })}[${tips}]`
        );
        return;
      }
      if (flag === 1) {
        // 详情动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 2) {
        // 维护动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 20) {
        // 删除动作
        this._deleteTableRowSelection(trse);
      }
      if (flag === 3) {
        // 停用动作
        // 设置值为0,表示停用
        Object.keys(trse)
          .filter(item => ['tagEnable'].includes(item))
          .forEach(item => {
            trse[item] = trse[item] === 0 ? 1 : 0;
          });
        this.onClickActionExecuteEvent(flag, trse, null, dispatchType);
      }
    };

    return actionType === 1 ? (
      <a type="primary" onClick={() => checkTableRowSelectionEntity(1)}>
        {formatMessage({ id: 'button.common.details' })}
      </a>
    ) : (
      <div>
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(1);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.details' })}
        </a>
        <Divider type="vertical" />
        <a type="primary" onClick={() => this.onClickActionExecuteEvent(10)}>
          {formatMessage({ id: 'button.common.add' })}
        </a>
        <Divider type="vertical" />
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(2);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.modify' })}
        </a>
        <Divider type="vertical" />
        {/* <a type="primary" onClick={() => checkTableRowSelectionEntity(20)}>删除</a> */}
        <a type="primary" onClick={() => checkTableRowSelectionEntity(3)}>
          {formatMessage({ id: 'form.common.enabledAndDisabled' })}
        </a>
      </div>
    );
  };

  /**
   * Table-删除行勾选数据
   * @param {object} item 处理数据对象
   * 删除1条商品标签值定义数据
   */
  _deleteTableRowSelection = item => {
    const { dispatch } = this.props;
    dispatch({ type: 'custcompanytagmanage/deleteCustCompanyTagDefine', payload: item });
  };
}

GoodsTagTableUpkoop.propTypes = {
  tagTypeCode: PropTypes.string.isRequired, // 商品标签类型代码,为新增商品标签值定义时使用
  actionType: PropTypes.number.isRequired, // 商品标签类型动作类型(同modalUpkeepType绑定一致)
};

export default connect(({ goodstagmanage }) => ({
  currentSelectGoodsTagDefineAll: goodstagmanage.currentSelectGoodsTagDefineAll || [], // 当前选择-商品标签值定义数据(所有)
}))(GoodsTagTableUpkoop);
