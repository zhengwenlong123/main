import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Tabs } from 'antd';
import GoodsTagTableUpkeep from './GoodsTagTableUpkeep';
import GoodsTagTypeUpkeep from './GoodsTagTypeUpkeep';

/**
 * 标签管理维护Modal对话框内容部分组件
 */
class GoodsTagUpkeep extends React.PureComponent {
  render() {
    const { modalUpkeepType: mut, modalUpkeepEntity: mue } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        {mut === 10 ? (
          <GoodsTagTypeUpkeep {...this.props} />
        ) : (
          <Tabs tabPosition="left">
            <Tabs.TabPane
              key="0"
              tab={formatMessage({ id: 'menu.goodsmanage.goodstagmanage.tagType' })}
            >
              <GoodsTagTypeUpkeep {...this.props} />
            </Tabs.TabPane>
            <Tabs.TabPane
              key="1"
              tab={formatMessage({ id: 'menu.goodsmanage.goodstagmanage.TagValues' })}
            >
              <GoodsTagTableUpkeep tagTypeCode={mue.tagTypeCode} actionType={mut} />
            </Tabs.TabPane>
          </Tabs>
        )}
      </div>
    );
  }
}

export default connect()(GoodsTagUpkeep);
