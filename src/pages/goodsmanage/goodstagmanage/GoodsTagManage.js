/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Select, Input, Card } from 'antd';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import GoodsTagUpkeep from './GoodsTagUpkeep';
// import { serverUrl } from '@/defaultSettings';
import styles from './index.less';

const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 标签管理组件
 */
class GoodsTagManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'tagTypeCode',
        key: 'tagTypeCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'tagTypeName',
        key: 'tagTypeName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.alias' }),
        dataIndex: 'tagTypeAlias',
        key: 'tagTypeAlias',
        align: 'center',
      },
      // { title: '描述', dataIndex: 'tagTypeDesc', key: 'tagTypeDesc', align: 'center' },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'tagTypeEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.tagTypeEnable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.goodsTagManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.goodsTagManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'goodstagmanage/updateGoodsTagTypeEnable',
                  this.searchContent
                )
              }
            >
              {record.tagTypeEnable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {};
  }

  /**
   * 响应点击处理
   */
  hookProcess = (type, record) => {
    const { dispatch } = this.props;
    if (type === 3) {
      record.tagTypeEnable = record.tagTypeEnable === 1 ? 0 : 1; // 设置商品标签类型状态为无效
    } else {
      // 获取-当前选择-标签类型下标签值定义数据(所有)
      dispatch({ type: 'goodstagmanage/getCurrentSelectGoodsTagDefineAll', payload: record });
    }
    return record;
  };

  /**
   * 默认加载标签数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-标签类型数据
    dispatch({ type: 'goodstagmanage/getGoodsTagType' });
  }

  /**
   * 根据条件搜索品牌
   */
  onFuzzyGoodsTagHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'goodstagmanage/paginationGoodsTagType',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
    // 清空搜素框
    // setFieldsValue({
    // });
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
      formItemLayout,
      tailFormItemLayout,
    } = this.state;
    const { goodsTagTypeList, goodsTagTypeListPageSize } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.goodsTagManage.tagTypeCode' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('tagTypeCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsTagManage.tagTypeName' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('tagTypeName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsTagManage.tagTypeEnable' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('tagTypeEnable')(
                <Select style={{ width: 80 }} allowClear>
                  {/* <Select.Option value="">全部</Select.Option> */}
                  <Select.Option value="1">
                    {formatMessage({ id: 'form.common.enabled' })}
                  </Select.Option>
                  <Select.Option value="0">
                    {formatMessage({ id: 'form.common.disabled' })}
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyGoodsTagHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            style={{ marginTop: 4, marginRight: 10 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.goodsTagManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };
    const tablePaginationOnChangeEventDispatchType = 'goodstagmanage/paginationGoodsTagType';
    return (
      <Card bordered={false}>
        <div>
          <MdcManageBlock
            tableTitle={formatMessage({ id: 'menu.goodsmanage.goodstagmanage' })}
            tableColumns={tableColumns}
            tableDataSource={goodsTagTypeList}
            tableOperateAreaCmp={tableOperateAreaCmp()}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            tableTotalSize={goodsTagTypeListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
            modalTitle={modalTitle}
            modalVisible={modalVisible}
            modalContentCmp={
              <GoodsTagUpkeep
                modalUpkeepType={modalUpkeepType}
                modalUpkeepEntity={modalUpkeepEntity}
                modalVisibleOnChange={this.modalVisibleOnChange}
                modalOnChangePageCurrent={this.changeCurrentPage}
                modalSearchCondition={this.searchContent}
              />
            }
            modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ goodstagmanage }) => ({
  goodsTagTypeList: goodstagmanage.goodsTagTypeList || [],
  goodsTagTypeListPageSize: goodstagmanage.goodsTagTypeListPageSize || 0,
}))(Form.create()(GoodsTagManage));
