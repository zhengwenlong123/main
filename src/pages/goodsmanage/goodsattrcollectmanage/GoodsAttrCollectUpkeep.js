/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Input, Checkbox, Button, message } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 属性集管理维护Modal对话框内容部分组件
 */
@ValidationFormHoc
class GoodsAttrCollectUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    tailFormItemLayout: {
      wrapperCol: { xs: { span: 24, offset: 0 }, sm: { span: 24, offset: 0 } },
    },
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      validationForm,
      dispatch,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    values.attrCollectionEnable
      ? (values.attrCollectionEnable = 1)
      : (values.attrCollectionEnable = 0);

    switch (mut) {
      case 2: // 维护
        values.id = mue.id;
        dispatch({
          type: 'goodsattrcollectmanage/updateGoodsAttrCollect',
          payload: values,
          callback: this.onCallback,
        });
        break;
      case 10: // 新增
        dispatch({
          type: 'goodsattrcollectmanage/addGoodsAttrCollect',
          payload: values,
          callback: this.onCallback,
        });
        break;
      default:
        break;
    }
  };

  render() {
    const { formItemLayout, tailFormItemLayout } = this.state;
    const {
      modalVisibleOnChange,
      modalUpkeepType: mut,
      form: { getFieldDecorator },
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div className={`${styles.modalHandleFormScopeArea}`}>
        <Form {...formItemLayout}>
          {mut === 10 ? null : (
            <Form.Item label={formatMessage({ id: 'form.common.code' })}>
              {getFieldDecorator('attrCollectionCode')(<Input disabled />)}
            </Form.Item>
          )}
          <Form.Item label={formatMessage({ id: 'form.common.name' })}>
            {getFieldDecorator('attrCollectionName')(<Input disabled={mut === 1} />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.common.alias' })}>
            {getFieldDecorator('attrCollectionAlias')(<Input disabled={mut === 1} />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.common.description' })}>
            {getFieldDecorator('attrCollectionDesc')(
              <Input.TextArea disabled={mut === 1} rows={3} />
            )}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
            {getFieldDecorator('attrCollectionEnable', { valuePropName: 'checked' })(
              <Checkbox disabled={mut === 1}>
                {formatMessage({ id: 'form.common.whetherEnabled' })}
              </Checkbox>
            )}
          </Form.Item>
          {mut === 1 ? null : (
            <Form.Item {...tailFormItemLayout}>
              <div className={styles.modalHandleBtnArea}>
                <Button
                  type="default"
                  htmlType="button"
                  onClick={() => modalVisibleOnChange(false)}
                >
                  {formatMessage({ id: 'button.common.cancel' })}
                </Button>
                <Button type="primary" htmlType="button" onClick={this.onClickSubmit}>
                  {formatMessage({ id: 'button.common.submit' })}
                </Button>
              </div>
            </Form.Item>
          )}
        </Form>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        attrCollectionCode: Form.createFormField({ value: mue ? mue.attrCollectionCode : '' }),
        attrCollectionName: Form.createFormField({ value: mue ? mue.attrCollectionName : '' }),
        attrCollectionAlias: Form.createFormField({ value: mue ? mue.attrCollectionAlias : '' }),
        attrCollectionDesc: Form.createFormField({ value: mue ? mue.attrCollectionDesc : '' }),
        attrCollectionEnable: Form.createFormField({
          value: mue ? mue.attrCollectionEnable === 1 : false,
        }),
      };
    },
  })(GoodsAttrCollectUpkeep)
);
