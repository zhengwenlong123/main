/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
/* eslint-disable no-param-reassign */
/* eslint-disable no-nested-ternary */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Card } from 'antd';
import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import GoodsAttrCollectUpkeep from './GoodsAttrCollectUpkeep';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 属性集管理组件
 */
class GoodsAttrCollectManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'attrCollectionCode',
        key: 'attrCollectionCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'attrCollectionName',
        key: 'attrCollectionName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.alias' }),
        dataIndex: 'attrCollectionAlias',
        key: 'attrCollectionAlias',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.description' }),
        dataIndex: 'attrCollectionDesc',
        key: 'attrCollectionDesc',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'attrCollectionEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.attrCollectionEnable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.goodsAttrCollectManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.goodsAttrCollectManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'goodsattrcollectmanage/updateGoodsAttrCollectEnable'
                )
              }
            >
              {record.attrCollectionEnable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    super(props, tableColumns);
  }

  /**
   * 修改attrCollectionEnable为0
   */
  hookProcess = (type, record) => {
    // const {dispatch} = this.props;
    if (type === 3) {
      record.attrCollectionEnable = record.attrCollectionEnable === 1 ? 0 : 1;
    } else {
      // 暂未处理
    }
    return record;
  };

  /**
   * 默认加载属性集数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({ type: 'goodsattrcollectmanage/getGoodsAttrCollect' });
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
    } = this.state;
    const { goodsAttrCollectList, goodsAttrCollectListPageSize } = this.props;

    const tableOperateAreaCmp = (
      <Button
        type="primary"
        onClick={() =>
          this.onClickActionExecuteEvent(
            10,
            null,
            formatMessage({ id: 'button.goodsAttrCollectManage.add' })
          )
        }
      >
        {formatMessage({ id: 'button.common.add' })}
      </Button>
    );
    const tablePaginationOnChangeEventDispatchType =
      'goodsattrcollectmanage/paginationGoodsAttrCollect';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'menu.goodsmanage.goodsattrcollectmanage' })}
          tableColumns={tableColumns}
          tableDataSource={goodsAttrCollectList}
          tableOperateAreaCmp={tableOperateAreaCmp}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType
            )
          }
          tableTotalSize={goodsAttrCollectListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <GoodsAttrCollectUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ goodsattrcollectmanage }) => ({
  goodsAttrCollectList: goodsattrcollectmanage.goodsAttrCollectList || [],
  goodsAttrCollectListPageSize: goodsattrcollectmanage.goodsAttrCollectListPageSize || 0,
}))(GoodsAttrCollectManage);
