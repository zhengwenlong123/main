/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Input, Select, Form, Card } from 'antd';
import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import GoodsClassificationUpkeep from './GoodsClassificationUpkeep';
import { serverUrl } from '@/defaultSettings';
import styles from './index.less';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 产品大类管理组件
 */
class GoodsClassificationManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'mainClassCode',
        key: 'mainClassCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'mainClassName',
        key: 'mainClassName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.alias' }),
        dataIndex: 'mainClassAlias',
        key: 'mainClassAlias',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.description' }),
        dataIndex: 'mainClassDesc',
        key: 'mainClassDesc',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'enable',
        align: 'center',
        render: (text, record) => <span>{status[record.enable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.goodsClassificationManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.goodsClassificationManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'goodsclassificationmanage/updatedGoodsClassificationStatus',
                  this.searchContent
                )
              }
            >
              {record.enable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {
      mainClassCode: '',
      mainClassName: '',
      enable: '',
    };
  }

  /**
   * 响应点击处理
   */
  hookProcess = (type, record) => {
    // const {dispatch} = this.props;
    if (type === 3) {
      const enable = record.enable === 1 ? 0 : 1;
      return { id: record.id, enable };
    }
    // else {
    // FIXME
    // }
    return record;
  };

  /**
   * 加载默认产品大类数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 加载产品大类数据
    dispatch({
      type: 'goodsclassificationmanage/paginationGoodsClassification',
      payload: { page: 1, pageSize: 10 },
    });
  }

  /**
   * 根据条件搜索产品大类
   */
  onFuzzyGoodsClassificationHandle() {
    const {
      dispatch,
      form: { getFieldsValue },
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'goodsclassificationmanage/paginationGoodsClassification',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
    // 清空搜素框
    // setFieldsValue({
    //   mainClassCode: '',
    //   mainClassName: '',
    //   enable: '',
    // });
  }

  /**
   * 导出文件请求
   */
  onExportGoodsClassificationFile() {
    const values = this.searchContent;
    const params = {
      ...values,
    };
    const formElement = document.createElement('form');
    formElement.style.display = 'display:none;';
    formElement.method = 'post';
    formElement.action = `${serverUrl}/mdc/goods/goodsclassification/export`;
    formElement.target = 'callBackTarget';
    Object.keys(params).map(item => {
      const inputElement = document.createElement('input');
      inputElement.type = 'hidden';
      inputElement.name = item;
      inputElement.value = params[item] || '';
      formElement.appendChild(inputElement);
      return item;
    });
    document.body.appendChild(formElement);
    formElement.submit();
    document.body.removeChild(formElement);
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      formItemLayout,
      tailFormItemLayout,
      tableColumns,
      tableCurrentPage,
    } = this.state;
    const { goodsClassificationList, goodsClassificationListPageSize } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.common.code' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('mainClassCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.name' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('mainClassName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.enable' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('enable')(
                <Select style={{ width: 80 }} allowClear>
                  {/* <Select.Option value="">全部</Select.Option> */}
                  <Select.Option value="1">
                    {formatMessage({ id: 'form.common.enabled' })}
                  </Select.Option>
                  <Select.Option value="0">
                    {formatMessage({ id: 'form.common.disabled' })}
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyGoodsClassificationHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            icon="download"
            style={{ marginTop: 4, marginRight: 10 }}
            onClick={() => this.onExportGoodsClassificationFile()}
          >
            {formatMessage({ id: 'button.common.export' })}
          </Button>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.goodsClassificationManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };
    const tablePaginationOnChangeEventDispatchType =
      'goodsclassificationmanage/paginationGoodsClassification';
    return (
      <Card bordered={false}>
        <div>
          <MdcManageBlock
            tableTitle={formatMessage({ id: 'menu.goodsmanage.goodsclassificationmanage' })}
            tableColumns={tableColumns}
            tableDataSource={goodsClassificationList}
            tableOperateAreaCmp={tableOperateAreaCmp()}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            tableTotalSize={goodsClassificationListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
            modalTitle={modalTitle}
            modalVisible={modalVisible}
            modalContentCmp={
              <GoodsClassificationUpkeep
                modalUpkeepType={modalUpkeepType}
                modalUpkeepEntity={modalUpkeepEntity}
                modalVisibleOnChange={this.modalVisibleOnChange}
                modalOnChangePageCurrent={this.changeCurrentPage}
                modalSearchCondition={this.searchContent}
              />
            }
            modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ goodsclassificationmanage }) => ({
  goodsClassificationList: goodsclassificationmanage.goodsClassificationList || [],
  goodsClassificationListPageSize: goodsclassificationmanage.goodsClassificationListPageSize || 0,
}))(Form.create()(GoodsClassificationManage));
