/* eslint-disable no-return-assign */
/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Button, Input, Checkbox, message } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
// import { verifyUnique, verifyInputTypeNumber } from '@/utils/mdcutil';
import { verifyInputTypeNumber } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品大类管理维护组件
 */
@ValidationFormHoc
class GoodsClassificationUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      modalSearchCondition,
      validationForm,
      dispatch,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.enable ? (values.enable = 1) : (values.enable = 0);

    if (mut === 2) {
      // 维护
      values.id = mue.id;
      values.enable = mue.enable;
      dispatch({
        type: 'goodsclassificationmanage/updateGoodsClassification',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    if (mut === 10) {
      // 新增
      // const params = { mainClassCode: values.mainClassCode };
      // const vu = await verifyUnique('/mdc/goods/goodsclassification/verifyUnique', params);
      // if (!vu) {
      //   message.warn(
      //     formatMessage({ id: 'form.goodsClassificationUpkeep.mainClassCode-conformity' })
      //   );
      //   return;
      // }
      dispatch({
        type: 'goodsclassificationmanage/addGoodsClassification',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalUpkeepType: mut,
      modalVisibleOnChange,
      form: { getFieldDecorator },
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.common.code' })}>
              {getFieldDecorator('mainClassCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsClassificationUpkeep.mainClassCode.placeholder',
                    }),
                  },
                ],
              })(
                <Input
                  ref={ref => (this.mainClassCodeInput = ref)}
                  disabled={mut === 1 || mut === 2}
                  maxLength={2}
                  onChange={() => {
                    verifyInputTypeNumber(this.mainClassCodeInput);
                  }}
                />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('mainClassName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.alias' })}>
              {getFieldDecorator('mainClassAlias')(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.description' })}>
              {getFieldDecorator('mainClassDesc')(
                <Input.TextArea disabled={mut === 1} rows={3} maxLength={250} />
              )}
            </Form.Item>
            {mut === 1 || mut === 10 ? (
              <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
                {getFieldDecorator('enable', { valuePropName: 'checked' })(
                  <Checkbox disabled={mut === 1}>
                    {formatMessage({ id: 'form.common.whetherEnabled' })}
                  </Checkbox>
                )}
              </Form.Item>
            ) : null}
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" htmlType="button" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button
                type="primary"
                htmlType="button"
                onClick={this.onClickSubmit}
                loading={confirmLoading}
              >
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        mainClassCode: Form.createFormField({ value: mue ? mue.mainClassCode : '' }),
        mainClassName: Form.createFormField({ value: mue ? mue.mainClassName : '' }),
        mainClassAlias: Form.createFormField({ value: mue ? mue.mainClassAlias : '' }),
        mainClassDesc: Form.createFormField({ value: mue ? mue.mainClassDesc : '' }),
        enable: Form.createFormField({ value: mue && mue.enable === 1 ? mue.enable === 1 : false }),
      };
    },
  })(GoodsClassificationUpkeep)
);
