/* eslint-disable no-constant-condition */
/* eslint-disable jsx-a11y/alt-text */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import moment from 'moment';
import EnclosureTableBasic from '@/components/Enclosure/EnclosureTableBasic'; // 附件描述维护统一使用此组件封装好了

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 品牌描述列表-列表维护
 */
class GoodsBrandDescUpkeep extends React.PureComponent {
  constructor(props) {
    super(props);
    // 表格列表字段定义
    this.tableColumns_GoodsBrandDesc = [
      {
        title: formatMessage({ id: 'form.common.rowno' }),
        dataIndex: 'seq',
        key: 'seq',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandName' }),
        dataIndex: 'brandName',
        key: 'brandName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.descriptionType' }),
        dataIndex: 'descTypeValue',
        key: 'descTypeValue',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.file' }),
        key: 'address',
        align: 'center',
        render: (text, record) => record.address.split(',').length,
      },
      {
        title: formatMessage({ id: 'form.common.descriptionName' }),
        dataIndex: 'descName',
        key: 'descName',
        align: 'center',
      },
      // { title: '描述备注', dataIndex: 'descNotes', key: 'descNotes', align: 'center' },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'descEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.descEnable]}</span>,
      },
    ];
  }

  /**
   * 渲染组件-品牌描述(列表)
   */
  render() {
    const {
      currentSelectGoodsBrandDesc: csgbd, // 当前选择[产品品牌]下[品牌描述]列表
      currentSelectGoodsBrandDescPageSize: csgbdps, // 当前选择[产品品牌]下[品牌描述]列表-总数量
      parmPublicParameterOfGoodsDescTypeAll: pppogdta, // 描述类型数据(所有)

      modalUpkeepType: mut, // 当前组件操作的类型
      modalUpkeepEntity: mue, // 当前组件操作的实体对象
    } = this.props;

    const enclosureOnDeleteEventDispatchType = 'goodsbrandmanage/deleteGoodsBrandDesc'; // 附件删除dispathc的type值
    const enclosureOnAddEventDispatchType = 'goodsbrandmanage/addGoodsBrandDesc'; // 附件添加dispatch的type值
    const enclosureOnUpdateEventDispatchType = 'goodsbrandmanage/updateGoodsBrandDesc'; // 附件更新dispatch的type值
    const enclosureTablePaginationOnChangeEventDispatchType =
      'goodsbrandmanage/paginationGoodsBrandDescByGoodsBrand'; // 品牌描述列表分页dispatch的type值
    const enclosureTablePaginationOnChangeEventCondition = {
      brandCode: mue.brandCode,
    }; // 附件列表分页附带的参数,是object类型

    return (
      <EnclosureTableBasic
        actionType={mut} // 附件列表-当前操作类型,来源于父级组件的定义值, 1为详情;2为修改;3为停用;10为新增;20为删除
        enclosureOperateSign="BrandDesc" // 附件当前操作标识
        enclosureTableTitle={formatMessage({ id: 'menu.goodsmanage.goodsbrandmanage.brandDesc' })} // 附件列表标题
        enclosureAssociatedObject={mue} // 附件关联的对象(当前操作父级对象)
        enclosureTableColumnsField={this.tableColumns_GoodsBrandDesc} // 附件列表字段定义
        enclosureTablePaginationOnChangeEventDispatchType={
          enclosureTablePaginationOnChangeEventDispatchType
        } // 附件列表分页对应dispatch的type
        enclosureTablePaginationOnChangeEventCondition={
          enclosureTablePaginationOnChangeEventCondition
        } // 附件列表分页附带的参数,是object类型
        enclosureTableDataSource={csgbd} // 附件列表源数据
        enclosureTableTotalSize={csgbdps} // 附件列表总数据量
        enclosureOnDeleteEventDispatchType={enclosureOnDeleteEventDispatchType} // 附件删除dispathc的type值
        enclosureOnAddEventDispatchType={enclosureOnAddEventDispatchType} // 附件添加dispatch的type值
        enclosureOnUpdateEventDispatchType={enclosureOnUpdateEventDispatchType} // 附件更新dispatch的type值
        enclosureDescTypeAll={pppogdta} // 附件描述类型数据所有
      />
    );
  }
}

export default connect(({ goodsbrandmanage, parmpublicparametermanage }) => ({
  currentSelectGoodsBrandDesc: goodsbrandmanage.currentSelectGoodsBrandDesc || [], // 品牌描述列表
  currentSelectGoodsBrandDescPageSize: goodsbrandmanage.currentSelectGoodsBrandDescPageSize, //  品牌描述列表-总数量
  parmPublicParameterOfGoodsDescTypeAll:
    parmpublicparametermanage.parmPublicParameterOfGoodsDescTypeAll || [], // 描述类型数据所有
}))(GoodsBrandDescUpkeep);
