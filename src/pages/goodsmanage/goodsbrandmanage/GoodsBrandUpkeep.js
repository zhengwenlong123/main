import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Tabs } from 'antd';
import moment from 'moment';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import GoodsBrandBasicUpkeep from './GoodsBrandBasicUpkeep';
import GoodsBrandDescUpkeep from './GoodsBrandDescUpkeep';

moment.locale('zh-cn');
/**
 * 品牌管理维护Modal对话框内容部分组件
 */
class GoodsBrandUpkeep extends MdcManageBasic {
  render() {
    const { modalUpkeepType: mut } = this.props;

    return (
      <div>
        {mut === 10 ? (
          <GoodsBrandBasicUpkeep {...this.props} />
        ) : (
          <Tabs tabPosition="left">
            <Tabs.TabPane
              key="1"
              tab={formatMessage({ id: 'menu.goodsmanage.goodsbrandmanage.brandBasic' })}
            >
              <GoodsBrandBasicUpkeep {...this.props} />
            </Tabs.TabPane>
            <Tabs.TabPane
              key="2"
              tab={formatMessage({ id: 'menu.goodsmanage.goodsbrandmanage.brandDesc' })}
            >
              <GoodsBrandDescUpkeep {...this.props} />
            </Tabs.TabPane>
          </Tabs>
        )}
      </div>
    );
  }
}

export default connect()(GoodsBrandUpkeep);
