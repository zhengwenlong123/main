/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Input, Select, Card } from 'antd';
import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import GoodsBrandUpkeep from './GoodsBrandUpkeep';
import { serverUrl } from '@/defaultSettings';
import styles from './index.less';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 品牌管理组件
 */
class GoodsBrandManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'brandCode',
        key: 'brandCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'brandName',
        key: 'brandName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.shortName' }),
        dataIndex: 'brandShortName',
        key: 'brandShortName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.rowno' }),
        dataIndex: 'seq',
        key: 'seq',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.website' }),
        dataIndex: 'brandWebsite',
        key: 'brandWebsite',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'brandEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.brandEnable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              size="small"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.goodsBrandManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              size="small"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.goodsBrandManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              size="small"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'goodsbrandmanage/updateGoodsBrandEnable',
                  this.searchContent
                )
              }
            >
              {record.brandEnable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {
      brandCode: '',
      brandName: '',
      brandEnable: '',
    };
  }

  /**
   * 响应点击处理
   * 进入产品品牌之前获取产品品牌相关联的数据
   */
  hookProcess = (type, record) => {
    const { dispatch } = this.props;
    const values = this.searchContent;
    if (type === 3) {
      const beforrecord = { ...record };
      beforrecord.brandEnable = record.brandEnable === 1 ? 0 : 1; // 如果是启用,则改成停用,如果是停用则改成启用
      return beforrecord;
    }
    // else {
    // 获取-当前选择-品牌描述数据(列表)
    dispatch({
      type: 'goodsbrandmanage/paginationGoodsBrandDescByGoodsBrand',
      payload: { brandCode: record.brandCode, page: 1, pageSize: 10, searchCondition: values },
    });
    // }
    return record;
  };

  /**
   * 默认加载品牌数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-产品品牌数据(列表,分页)
    dispatch({ type: 'goodsbrandmanage/paginationGoodsBrand', payload: { page: 1, pageSize: 10 } });
    // 获取-根据公用参数类型定义代码[goods_desc_type]获取公用参数定义数据所有
    dispatch({
      type:
        'parmpublicparametermanage/getParmPublicParameterAllByParmPublicParameterTypeOfGoodsDescType',
      payload: { parmTypeCode: 'goods_desc_type' },
    });
  }

  /**
   * 根据条件搜索品牌
   */
  onFuzzyGoodsBrandhHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'goodsbrandmanage/paginationGoodsBrand',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
    // 清空搜素框
    // setFieldsValue({
    //   brandCode: '',
    //   brandName: '',
    //   brandEnable: '',
    // });
  }

  /**
   * 导出文件请求
   */
  onExportGoodsBrandFile() {
    const values = this.searchContent;
    const params = {
      ...values,
    };
    const formElement = document.createElement('form');
    formElement.style.display = 'display:none;';
    formElement.method = 'post';
    formElement.action = `${serverUrl}/mdc/goods/goodsbrand/export`;
    formElement.target = 'callBackTarget';
    Object.keys(params).map(item => {
      const inputElement = document.createElement('input');
      inputElement.type = 'hidden';
      inputElement.name = item;
      inputElement.value = params[item] || '';
      formElement.appendChild(inputElement);
      return item;
    });
    document.body.appendChild(formElement);
    formElement.submit();
    document.body.removeChild(formElement);
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      formItemLayout,
      tailFormItemLayout,
      tableColumns,
      tableCurrentPage,
    } = this.state;
    const { goodsBrandList, goodsBrandListPageSize } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandCode' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('brandCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandName' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('brandName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandEnable' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('brandEnable')(
                <Select style={{ width: 80 }} allowClear>
                  {/* <Select.Option value="">全部</Select.Option> */}
                  <Select.Option value="1">
                    {formatMessage({ id: 'form.common.enabled' })}
                  </Select.Option>
                  <Select.Option value="0">
                    {formatMessage({ id: 'form.common.disabled' })}
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyGoodsBrandhHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            icon="download"
            style={{ marginTop: 4, marginRight: 10 }}
            onClick={() => this.onExportGoodsBrandFile()}
          >
            {formatMessage({ id: 'button.common.export' })}
          </Button>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.goodsBrandManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };
    const tablePaginationOnChangeEventDispatchType = 'goodsbrandmanage/paginationGoodsBrand';
    return (
      <Card bordered={false}>
        <div>
          <MdcManageBlock
            tableTitle={formatMessage({ id: 'menu.goodsmanage.goodsbrandmanage' })}
            tableColumns={tableColumns}
            tableDataSource={goodsBrandList === undefined ? [] : goodsBrandList}
            tableOperateAreaCmp={tableOperateAreaCmp()}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            tableTotalSize={goodsBrandListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
            modalTitle={modalTitle}
            modalVisible={modalVisible}
            modalContentCmp={
              <GoodsBrandUpkeep
                modalUpkeepType={modalUpkeepType}
                modalUpkeepEntity={modalUpkeepEntity}
                modalVisibleOnChange={this.modalVisibleOnChange}
                modalOnChangePageCurrent={this.changeCurrentPage}
                modalSearchCondition={this.searchContent}
              />
            }
            modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ goodsbrandmanage }) => ({
  goodsBrandList: goodsbrandmanage.goodsBrandList || [],
  goodsBrandListPageSize: goodsbrandmanage.goodsBrandListPageSize || 0,
}))(Form.create()(GoodsBrandManage));
