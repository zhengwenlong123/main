/* eslint-disable react/no-string-refs */
/* eslint-disable no-return-assign */
/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
// import moment from 'moment';
import { message, Button, Form, Input, InputNumber, Checkbox } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
// import { verifyUnique, verifyInputTypeNumber } from '@/utils/mdcutil';
import { verifyInputTypeNumber } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品品牌基本信息维护组件
 */
@ValidationFormHoc
class GoodsBrandBasicUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      modalSearchCondition,
      validationForm,
      dispatch,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.brandEnable ? (values.brandEnable = 1) : (values.brandEnable = 0);

    // 维护
    if (mut === 2) {
      values.id = mue.id;
      dispatch({
        type: 'goodsbrandmanage/updateGoodsBrand',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    // 新增
    if (mut === 10) {
      // const params = { brandCode: values.brandCode };
      // const vu = await verifyUnique('/mdc/goods/goodsbrand/verifyUnique', params);
      // if (!vu) {
      //   message.warn(formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandCodeMust' }));
      //   return;
      // }
      dispatch({
        type: 'goodsbrandmanage/addGoodsBrand',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
    } = this.props;

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form layout="horizontal" labelAlign="right" {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.common.code' })}>
              {getFieldDecorator('brandCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsBrandBasicUpkeep.brandCode.placeholder',
                    }),
                  },
                ],
              })(
                <Input
                  ref={ref => (this.brandCodeInput = ref)}
                  maxLength={3}
                  disabled={mut === 1 || mut === 2}
                  onChange={() => {
                    verifyInputTypeNumber(this.brandCodeInput);
                  }}
                />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('brandName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsBrandBasicUpkeep.brandName.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.shortName' })}>
              {getFieldDecorator('brandShortName')(<Input disabled={mut === 1} maxLength={50} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.website' })}>
              {getFieldDecorator('brandWebsite')(<Input disabled={mut === 1} maxLength={50} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.rowno' })}>
              {getFieldDecorator('seq')(
                <InputNumber min={1} max={1000000} defaultChecked={1} disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.description' })}>
              {getFieldDecorator('brandDesc')(
                <Input.TextArea rows={5} disabled={mut === 1} maxLength={250} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('brandEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        brandCode: Form.createFormField({ value: mue ? mue.brandCode : '' }),
        brandName: Form.createFormField({ value: mue ? mue.brandName : '' }),
        brandShortName: Form.createFormField({ value: mue ? mue.brandShortName : '' }),
        brandWebsite: Form.createFormField({ value: mue ? mue.brandWebsite : '' }),
        seq: Form.createFormField({ value: mue ? mue.seq : 1 }),
        brandDesc: Form.createFormField({ value: mue ? mue.brandDesc : '' }),
        brandEnable: Form.createFormField({ value: mue ? mue.brandEnable === 1 : true }),
      };
    },
  })(GoodsBrandBasicUpkeep)
);
