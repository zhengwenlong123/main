/* eslint-disable react/sort-comp */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { connect } from 'dva';
import { Row, Col, Card } from 'antd';
import GoodsCatagoryUpkeep from './GoodsCatagoryUpkeep';
import GoodsCatagoryTree from './GoodsCatagoryTree';
import styles from './index.less';

/**
 * 产品类目管理组件
 */

class GoodsCatagoryManage extends React.PureComponent {
  /**
   * 页面退出清理数据
   */
  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({ type: 'goodscatagorymanage/changeGoodsCatagoryRootNode', payload: [] });
    dispatch({
      type: 'goodscatagorymanage/changeCurrentSelectCatagory',
      payload: { payload: null, parentPayload: null },
    });
  }

  /**
   * 默认获取产品类目数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取类目根节点数据
    dispatch({ type: 'goodscatagorymanage/getGoodsCatagoryRootNode' });
  }

  /**
   * 获取tree组件更改的值去调用子组件的方法
   */
  changeUpkeep = flag => {
    this.child.disableUpkeepType(flag);
  };

  onRef = ref => {
    this.child = ref;
  };

  render() {
    const { goodsCatagoryRootNode: gcrn } = this.props;
    return (
      <Card bordered={false}>
        <div className={styles.gridContainer}>
          {gcrn.length > 0 ? (
            <Row
              className={styles.gridRow}
              gutter={{ xs: 8, sm: 16, md: 24 }}
              justify="space-between"
              type="flex"
            >
              <Col className={styles.gridColSider} span={4}>
                <GoodsCatagoryTree changeSelected={this.changeUpkeep} />
              </Col>
              <Col className={styles.gridColContent} span={20}>
                <GoodsCatagoryUpkeep onRef={this.onRef} />
              </Col>
            </Row>
          ) : (
            ''
          )}
        </div>
      </Card>
    );
  }
}

export default connect(({ goodscatagorymanage }) => ({
  goodsCatagoryRootNode: goodscatagorymanage.goodsCatagoryRootNode || [], // 产品类目-根节点数据(列表所有)
}))(GoodsCatagoryManage);
