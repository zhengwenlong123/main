/* eslint-disable prefer-destructuring */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable no-restricted-syntax */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Input, InputNumber, Modal, message, Checkbox } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
// import { verifyUnique } from '@/utils/mdcutil';
// import { cloneDeep } from 'lodash';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品类目[维护]组件
 */
@ValidationFormHoc
class GoodsCatagoryUpkeep extends React.PureComponent {
  state = {
    upkeepType: 0, // 当前维护类型, 默认展示为0, 维护节点为1, 添加子节点为2
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 8 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 16 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 留给父组件接口当tree选中更换后禁用内容修改
   */
  componentDidMount() {
    const { onRef } = this.props;
    onRef(this);
  }

  disableUpkeepType = () => {
    this.setState({ upkeepType: 0 });
  };

  /**
   * 接收产品类目返回的数据并查找显示
   * 递归查找父级tree数据
   */
  findTreeParent = (arr, selected) => {
    let selectedParent;
    for (const item of arr) {
      if (item.catagoryCode === selected.parentCode) {
        selectedParent = item;
        break;
      } else if (item.childrens && item.childrens.length > 0) {
        selectedParent = this.findTreeParent(item.childrens, selected);
        if (selectedParent) {
          break;
        }
      }
    }
    return selectedParent;
  };

  /**
   * 更新state中upkeepType值
   */
  changeUpkeepType = typeFlag => {
    const {
      dispatch,
      currentSelectCatagory: csc,
      currentSelectParentCatagory: cspc,
      goodsCatagoryRootNode,
    } = this.props;
    // 2-添加子节点-设置state当中currentSelectCatagory值为null
    if (typeFlag === 2) {
      if (csc.catagoryLevel === 3)
        return message.warn(
          formatMessage({ id: 'form.goodsSpuInfoManage.catagoryCode.add.choice' })
        );
      if (!csc) return null;
      // 因添加子节点-设置-当前选择类目对象及父级类目对象值,且所以parentPayload设置为csc
      dispatch({
        type: 'goodscatagorymanage/changeCurrentSelectCatagory',
        payload: { payload: null, parentPayload: csc },
      });
      // 因添加子节点-设置-当前选择类目属性集对象为null
      // dispatch({type:'goodscatagorymanage/changeCurrentSelectCatagoryAttrCollect', payload: null});
    } else if (typeFlag === -1) {
      if (csc === null) {
        dispatch({
          type: 'goodscatagorymanage/changeCurrentSelectCatagory',
          payload: {
            payload: cspc,
            parentPayload: this.findTreeParent(goodsCatagoryRootNode, cspc),
          },
        });
      }
      typeFlag = 0; // eslint-disable-line
    }
    return this.setState({ upkeepType: typeFlag });
  };

  /**
   * [删除-当前选择类目节点]-回调函数
   */
  onClickDeleteNodeHandle = () => {
    const {
      dispatch,
      currentSelectCatagory: csc, // 当前选择类目对象
      // currentSelectCatagoryAttrCollect: cscac, // 当前选择类目-对应属性集对象
    } = this.props;
    const { childrens } = csc;

    // if(cscac){ // 类目对应属性集存在
    //   csc.goodsCatagoryAttrCollectionObj = cscac;
    // }else{ // 类目对应属性集不存在
    //   csc.goodsCatagoryAttrCollectionObj = null;
    // }

    if (childrens && childrens.length > 0) {
      Modal.warn({
        title: formatMessage({ id: 'form.goodsCatagoryUpkeep.deletePrompt' }),
        content: (
          <div className={styles.deleteChlidNodeText}>
            {formatMessage({ id: 'form.goodsCatagoryUpkeep.deletePromptMsg' })}
          </div>
        ),
        okText: formatMessage({ id: 'form.common.sure' }),
      });
    } else {
      // 执行删除
      Modal.confirm({
        title: formatMessage({ id: 'form.common.prompt' }),
        content: formatMessage({ id: 'message.delete.it' }),
        okText: formatMessage({ id: 'form.common.sure' }),
        cancelText: formatMessage({ id: 'form.common.cancel' }),
        onOk: () => {
          // 直接调用后台接口处理
          dispatch({
            type: 'goodscatagorymanage/deleteGoodsCatagory',
            payload: { params: csc, callback: this.onCallback },
          });
        },
        onCancel: () => {
          // 无处理
        },
      });
    }
  };

  /**
   * 1.表单提交请求回调
   * 2.恢复默认值状态
   * @param {number} type 默认展示为0, 维护节点为1, 添加子节点为2, 删除节点为3,
   * @param {boolean} result 请求接口处理状态,true为成功,false为失败
   */
  onCallback = (type, result) => {
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      if (type === 3) {
        // 删除节点
        // 清空当前选择类目数据的值
        const { dispatch, currentSelectParentCatagory: cspc, goodsCatagoryRootNode } = this.props;
        dispatch({
          type: 'goodscatagorymanage/changeCurrentSelectCatagory',
          payload: {
            payload: cspc,
            parentPayload: this.findTreeParent(goodsCatagoryRootNode, cspc),
          },
        });
        // dispatch({
        //   type: 'goodscatagorymanage/changeCurrentSelectCatagory',
        //   payload: { payload: null, parentPayload: null },
        // });
      }
      // 恢复默认值状态
      this.changeUpkeepType(0);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 当前维护节点类型处理,含[维护节点,添加子节点]操作
   * 默认展示为0, 维护节点为1, 添加子节点为2
   */
  onClickUpkeepTypeHandle = async () => {
    const {
      dispatch,
      validationForm,
      form: { getFieldsValue },
      currentSelectCatagory: csc, // 当前选择类目对象
      currentSelectParentCatagory: cspc, // 当前选择类目-父级类目对象
      // currentSelectCatagoryAttrCollect: cscac, // 当前选择类目-对应属性集对象
    } = this.props;
    const status = validationForm(); // 验证必填完整性
    if (!status) return;
    this.setState({ confirmLoading: true });
    const { upkeepType } = this.state;
    const result = getFieldsValue();
    result.catagoryEnable = result.catagoryEnable ? 1 : 0;

    // 1-维护节点
    if (upkeepType === 1) {
      // if (result.catagoryCode !== csc.catagoryCode) {
      //   // 检验类目代码唯一性
      //   const params = { catagoryCode: result.catagoryCode };
      //   const vu = await verifyUnique('/mdc/goods/goodscatagory/verifyUnique', params);
      //   if (!vu) {
      //     message.warn(formatMessage({ id: 'form.goodsCatagoryUpkeep.catagoryCode-conformity' }));
      //     return;
      //   }
      // }
      // 检验级别
      if (csc.catagoryLevel !== result.catagoryLevel) {
        message.error(formatMessage({ id: 'form.goodsCatagoryUpkeep.catagoryLevel-conformity' }));
        return;
      }
      // 类目属性集对象
      // if(cscac && cscac.attrCollectionCode === result.attrCollectionCode){ // 旧值与新值无变化
      //   result.goodsCatagoryAttrCollectionObj = null;
      // }else{
      //   const temp = cloneDeep(cscac || {});
      //   temp.catagoryCode = result.catagoryCode; // 更新类目代码
      //   temp.attrCollectionCode = result.attrCollectionCode; // 更换新的attrCollectionCode值
      //   result.goodsCatagoryAttrCollectionObj = temp;
      // }
      result.id = csc.id;
      dispatch({
        type: 'goodscatagorymanage/updateGoodsCatagory',
        payload: { params: result, callback: this.onCallback },
      });
    }

    // 2-添加节点
    if (upkeepType === 2) {
      // 验证数据唯一性
      // const params = { catagoryCode: result.catagoryCode };
      // const vu = await verifyUnique('/mdc/goods/goodscatagory/verifyUnique', params);
      // if (!vu) {
      //   message.warn(formatMessage({ id: 'form.goodsCatagoryUpkeep.catagoryCode-conformity' }));
      //   return;
      // }
      // 检验级别
      if (cspc.catagoryLevel !== Number(result.catagoryLevel - 1)) {
        message.error(formatMessage({ id: 'form.goodsCatagoryUpkeep.catagoryLevel-conformity' }));
        return;
      }
      // 类目属性集对象
      // if(result.attrCollectionCode){ // 有选择类目属性集则新增
      //   result.goodsCatagoryAttrCollectionObj = {catagoryCode: null, attrCollectionCode: result.attrCollectionCode};
      // }else{ // 无选择类目属性集则不处理
      //   result.goodsCatagoryAttrCollectionObj = null;
      // }
      dispatch({
        type: 'goodscatagorymanage/addGoodsCatagory',
        payload: { params: result, parentCatagory: cspc, callback: this.onCallback },
      });
    }
  };

  /**
   * upkeepType：默认展示为0, 维护节点为1, 添加子节点为2,
   */
  render() {
    const { upkeepType, formItemLayout, confirmLoading } = this.state; // 当前维护类型, 默认展示为0, 维护节点为1, 添加子节点为2
    const {
      form: { getFieldDecorator },
      currentSelectCatagory: csc, // 当前选择类目对象
      currentSelectParentCatagory: cspc, // 当前选择类目-父级类目对象
      // goodsAttrCollectAll: gaca, // 属性集(所有)数组
    } = this.props;

    if (!csc && !cspc && upkeepType === 0) return null; // 当前操作对象为空且upkeepType为0时
    const isEnable = upkeepType === 0; // 是否可编辑

    return (
      <div>
        <div className={styles.contentHeader}>
          <span className={styles.contentHeaderTitle}>
            {csc
              ? `${formatMessage({ id: 'form.goodsCatagoryUpkeep.currentOperationCategory' })}：${
                  csc.catagoryName
                }`
              : cspc
              ? `${formatMessage({ id: 'form.goodsCatagoryUpkeep.currentOperationCategory' })}：${
                  cspc.catagoryName
                }`
              : ''}
          </span>
          <div>
            {(csc && csc.catagoryLevel !== 0) || (cspc && cspc.catagoryLevel !== 0) ? ( // cspc为当前操作对象的父级对象
              <Button.Group>
                <Button type="default" onClick={() => this.changeUpkeepType(1)}>
                  {formatMessage({ id: 'form.goodsCatagoryUpkeep.maintenanceNode' })}
                </Button>
                <Button type="default" onClick={() => this.changeUpkeepType(2)}>
                  {formatMessage({ id: 'form.goodsCatagoryUpkeep.addChildNodes' })}
                </Button>
                <Button type="default" onClick={() => this.onClickDeleteNodeHandle()}>
                  {formatMessage({ id: 'form.goodsCatagoryUpkeep.deleteNode' })}
                </Button>
              </Button.Group>
            ) : (
              <Button type="default" onClick={() => this.changeUpkeepType(2)}>
                {formatMessage({ id: 'form.goodsCatagoryUpkeep.addChildNodes' })}
              </Button>
            )}
          </div>
        </div>
        <Divider type="horizontal" />
        <div>
          <Form {...formItemLayout}>
            <Form.Item
              label={formatMessage({ id: 'form.goodsCatagoryUpkeep.parentCode' })}
              style={{ display: 'none' }}
            >
              {getFieldDecorator('parentCode', { rules: [{ required: true }] })(<Input disabled />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsCatagoryUpkeep.parentName' })}>
              {getFieldDecorator('parentName', { rules: [{ required: true }] })(<Input disabled />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.code' })}>
              {getFieldDecorator('catagoryCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.code.placeholder' }),
                  },
                ],
              })(<Input disabled={upkeepType !== 2} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('catagoryName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={isEnable} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.alias' })}>
              {getFieldDecorator('catagoryAlias')(<Input disabled={isEnable} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.level' })}>
              {getFieldDecorator('catagoryLevel', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.level.placeholder' }),
                  },
                ],
              })(<InputNumber disabled />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.rowno' })}>
              {getFieldDecorator('seq')(<InputNumber min={1} max={10000} disabled={isEnable} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.description' })}>
              {getFieldDecorator('catagoryDesc')(<Input.TextArea rows={3} disabled={isEnable} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('catagoryEnable', { valuePropName: 'checked' })(
                <Checkbox disabled={isEnable}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
            {/* csc && csc.catagoryLevel === 0 ? (null) : ( // 如果是根节点则不显示类目属性集
              <Form.Item label="类目属性集">
                {getFieldDecorator('attrCollectionCode')(
                  <Select disabled={isEnable} allowClear>
                    {gaca.map(item => (
                      <Select.Option key={item.attrCollectionCode} value={item.attrCollectionCode}>{item.attrCollectionName}</Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
              ) */}
          </Form>
          {!isEnable ? (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => this.changeUpkeepType(-1)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button
                type="primary"
                onClick={this.onClickUpkeepTypeHandle}
                loading={confirmLoading}
              >
                {formatMessage({ id: 'button.common.save' })}
              </Button>
            </div>
          ) : null}
        </div>
      </div>
    );
  }
}

export default connect(({ goodscatagorymanage /* goodsattrcollectmanage */ }) => ({
  currentSelectCatagory: goodscatagorymanage.currentSelectCatagory, // 当前选择类目对象
  currentSelectParentCatagory: goodscatagorymanage.currentSelectParentCatagory, // 当前选择类目父对象
  goodsCatagoryRootNode: goodscatagorymanage.goodsCatagoryRootNode || [], // 产品类目-根节点数据(列表所有)
  // currentSelectCatagoryAttrCollect: goodscatagorymanage.currentSelectCatagoryAttrCollect, // 当前选择类目-对应的属性集对象, 需求：每个类目只可以有一个属性集
  // goodsAttrCollectAll: goodsattrcollectmanage.goodsAttrCollectAll || [], // 属性集所有数据
}))(
  Form.create({
    mapPropsToFields(props) {
      const {
        currentSelectCatagory: csc, // 当前选择类目对象
        currentSelectParentCatagory: cspc, // 当前选择类目-父级类目对象
        // currentSelectCatagoryAttrCollect: cscac, // 当前选择类目-对应属性集对象
      } = props;
      return {
        parentCode: Form.createFormField({ value: cspc ? cspc.catagoryCode : '' }),
        parentName: Form.createFormField({ value: cspc ? cspc.catagoryName : '' }),
        catagoryCode: Form.createFormField({ value: csc ? csc.catagoryCode : '' }),
        catagoryName: Form.createFormField({ value: csc ? csc.catagoryName : '' }),
        catagoryAlias: Form.createFormField({ value: csc ? csc.catagoryAlias : '' }),
        catagoryLevel: Form.createFormField({
          value: csc
            ? parseInt(csc.catagoryLevel, 10)
            : cspc
            ? parseInt(cspc.catagoryLevel, 10) + 1
            : 1,
        }),
        seq: Form.createFormField({ value: csc ? parseInt(csc.seq || 1, 10) : 1 }),
        catagoryDesc: Form.createFormField({ value: csc ? csc.catagoryDesc : '' }),
        catagoryEnable: Form.createFormField({ value: csc ? csc.catagoryEnable === 1 : true }),
        // attrCollectionCode: Form.createFormField({ value: cscac ? cscac.attrCollectionCode : ''}), // 类目属性集, 需求：每个类目只可以有一个属性集
      };
    },
  })(GoodsCatagoryUpkeep)
);
