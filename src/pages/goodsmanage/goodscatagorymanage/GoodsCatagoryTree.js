/* eslint-disable no-param-reassign */
/* eslint-disable no-console */
import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Tree } from 'antd';
import { getCurrentSelectCatagoryChildNode } from '@/services/goodsmanage/goodscatagorymanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

/**
 * 产品类目[树]组件
 */
class GoodsCatagoryTree extends PureComponent {
  /**
   * 产品类目[树节点选择]处理
   */
  onSelectTreeHandle = (selectedKeys, e) => {
    const { node } = e;
    const {
      props: { dataRef, dataParentRef },
    } = node; // 避免空值的问题
    if (dataRef) {
      const { dispatch, changeSelected } = this.props;
      changeSelected(true);
      // 设置state当前选择类目数据及父级类目数据
      dispatch({
        type: 'goodscatagorymanage/changeCurrentSelectCatagory',
        payload: { payload: dataRef, parentPayload: dataParentRef },
      });
      // 获取当前类目下属性集数据对象, 需求:每个类目只可以有一个属性集
      // if(dataRef.catagoryLevel > 0){ // 非根节点才获取当前类目-对应属性集对象
      //   dispatch({type: 'goodscatagorymanage/getGoodsCatagoryAttrCollectionByGoodsCategory', payload: {catagoryCode: dataRef.catagoryCode}});
      // }
    }
  };

  onExpandTreeHandle = async (expandedKeys, { expanded: bool, node }) => {
    // console.log(bool);
    if (!bool) return;

    // 加载当前选择节点-子节点数据
    const cItem = node.props.dataRef;
    const res = await getCurrentSelectCatagoryChildNode(
      wrapTid({
        parentCode: cItem.catagoryCode,
        catagoryLevel: Number(cItem.catagoryLevel + 1),
        sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
      })
    );
    const results = requestResult(res);
    node.props.dataRef.childrens = results || [];
    const { goodsCatagoryRootNode: gcrn, dispatch } = this.props;
    dispatch({ type: 'goodscatagorymanage/changeGoodsCatagoryRootNode', payload: [...gcrn] });
  };

  /**
   * 渲染子节点
   * @param data 当前节点对象
   * @param parentData 当前节点父级对象
   */
  renderTreeNodes = (data, parentData) =>
    data.map(item => {
      if (item.childrens && item.childrens.length > 0) {
        return (
          <Tree.TreeNode
            key={item.id}
            title={item.catagoryName}
            dataRef={item}
            dataParentRef={parentData}
          >
            {this.renderTreeNodes(item.childrens || [], item)}
          </Tree.TreeNode>
        );
      }
      return (
        <Tree.TreeNode
          key={item.id}
          title={item.catagoryName}
          dataRef={item}
          dataParentRef={parentData}
        />
      );
    });

  render() {
    const { goodsCatagoryRootNode: gcrn } = this.props;
    return (
      <div>
        <Tree.DirectoryTree
          // loadData={this.onLoadData}
          onSelect={this.onSelectTreeHandle}
          onExpand={this.onExpandTreeHandle}
          defaultExpandedKeys={gcrn.length > 0 ? [`${gcrn[0].id}`] : []}
          defaultSelectedKeys={gcrn.length > 0 ? [`${gcrn[0].id}`] : []}
          // defaultExpandAll
        >
          {this.renderTreeNodes(gcrn || [], null)}
        </Tree.DirectoryTree>
      </div>
    );
  }
}

export default connect(({ goodscatagorymanage }) => ({
  goodsCatagoryRootNode: goodscatagorymanage.goodsCatagoryRootNode || [], // 产品类目-根节点数据(列表所有)
}))(GoodsCatagoryTree);

// /**
//  * 加载节点数据
//  * 函数返回的是Promise对象
//  */
// onLoadData = treeNode =>
//   new Promise(resolve => {
//     if (treeNode.props.children && treeNode.props.children.length > 0) {
//       resolve();
//       return;
//     }

//     // 执行请求获取数据
//     setTimeout(async () => {
//       const cItem = treeNode.props.dataRef;
//       const res = await getCurrentSelectCatagoryChildNode(wrapTid({
//         parentCode: cItem.catagoryCode,
//         catagoryLevel: Number(cItem.catagoryLevel + 1),
//         catagoryEnable: 1,
//       }));
//       const results = requestResult(res);
//       treeNode.props.dataRef.childrens = results || [];
//       const {goodsCatagoryRootNode:gcrn, dispatch} = this.props;
//       dispatch({type: 'goodscatagorymanage/changeGoodsCatagoryRootNode', payload: [...gcrn]});
//       resolve();
//     }, 100);
//   });

// /**
//  * 树视图[组装]
//  */
// treeHandleView = () => {
//   const { goodsCatagoryAll } = this.props;

//   // 有数据
//   if (goodsCatagoryAll instanceof Array && goodsCatagoryAll.length > 0) {
//     // 定义Tree组装函数, data为当前节点, parentData为父级节点
//     const getTreeComponent = (data, parentData) =>
//       data.map(item => {
//         const { childrens } = item;
//         if (childrens.length > 0) {
//           // 返回子节点
//           return (
//             <Tree.TreeNode
//               key={item.id}
//               title={item.catagoryName}
//               payload={item}
//               parentPayload={parentData}
//             >
//               {getTreeComponent(childrens, item)}
//             </Tree.TreeNode>
//           );
//         }
//         // 返回叶子节点
//         return (
//           <Tree.TreeNode
//             key={item.id}
//             isLeaf
//             title={item.catagoryName}
//             payload={item}
//             parentPayload={parentData}
//           />
//         );
//       });

//     // 返回Tree
//     return (
//       <Tree.DirectoryTree onSelect={this.onSelectTreeHandle}>
//         {getTreeComponent(goodsCatagoryAll, null)}
//       </Tree.DirectoryTree>
//     );
//   }

//   // 无数据
//   return (
//     <Button type="primary" icon="add" size="small">
//       添加根节点
//     </Button>
//   );
// };
