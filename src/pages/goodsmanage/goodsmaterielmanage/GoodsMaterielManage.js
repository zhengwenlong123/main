import React from 'react';
import { formatMessage } from 'umi/locale';
import moment from 'moment';

moment.locale('zh-cn');
/**
 * 商品（单品）物料管理-提供查询/导出功能
 */
class GoodsMaterielManage extends React.PureComponent {
  render() {
    return <div>{formatMessage({ id: 'form.goodsMaterielManage.export' })}</div>;
  }
}

export default GoodsMaterielManage;
