import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import router from 'umi/router'; // eslint-disable-line
import { message, Button, Form, Input, Select, Row, Col } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import { GOODS_APPLY_TYPE } from '@/utils/const';
import { wrapTid } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单名称、类型、产品选择的弹窗组件
 */
@ValidationFormHoc
class GoodsApplyBillAddType extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 18 } },
    },
    messageItemLayout: {
      xs: { span: 24 },
      sm: { span: 18, offset: 4 },
    },
    productItemShow: true,
  };

  /**
   * 默认加载产品数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 产品下拉列表的查询
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsSpuInfoAll' });
  }

  /**
   * 创建类型改变回调
   */
  onChangeUploadFile = val => {
    if (val !== 'product') {
      // 创建类型parmCode的值“product”即为产品类型
      // 当创建类型为非产品时，即商品&物料时，才允许【选择产品】中录入产品。否则不允许录入值。
      this.setState({ productItemShow: true });
    } else {
      this.setState({ productItemShow: false });
    }
  };

  /**
   * 点击下一步
   */
  onClickNext = e => {
    const {
      form: { validateFields },
      currentUser,
      dispatch,
    } = this.props;
    e.preventDefault();

    validateFields((err, values) => {
      if (!err) {
        const payload = {
          rfRequestUserName: currentUser.name, // 申请人姓名
          rfRequestUserOrgId: currentUser.name, // 申请人所属机构编码
          rfRequestUserOrgName: currentUser.name, // 申请人所属机构名称
          rfStatus: 'verify_waiting', // 申请单状态
          spuCode: '',
          tid: wrapTid().tid,
          ...values,
        };
        if (payload.spuCode) {
          // 产品不为空，选择创建类型为商品、物料，可以带出已生效的产品信息
          dispatch({
            type: 'goodsapplybillmanage/fetchApplyBillInfo',
            payload: {
              spuCode: payload.spuCode,
              rfId: '',
            },
          });
        }
        dispatch({
          type: 'goodsapplybillmanage/changeGoodsRFInfo',
          payload,
        });
        router.push({
          pathname: '/goodsmanage/goodsapplybillmanage/detail',
          query: {
            type: 'add',
          },
        });
      } else {
        message.error('请填写必填项');
      }
    });
  };

  render() {
    const { formItemLayout, messageItemLayout, productItemShow } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      parmPublicParameterApplyBillTypeAll = [],
      validProductAll,
    } = this.props;

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form layout="horizontal" labelAlign="right" {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.goodsApplyBillManage.applyName' })}>
              {getFieldDecorator('rfName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsApplyBillManage.applyName.placeholder',
                    }),
                  },
                ],
              })(<Input />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsApplyBillManage.createType' })}>
              {getFieldDecorator('rfTypeCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsApplyBillManage.createType.placeholder',
                    }),
                  },
                ],
              })(
                <Select onSelect={this.onChangeUploadFile}>
                  {parmPublicParameterApplyBillTypeAll.map(v => (
                    <Select.Option value={v.parmCode} key={v.id}>
                      {v.parmShowValue}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Row className={styles.messageBottom}>
              {Object.keys(GOODS_APPLY_TYPE).map(key => (
                <Col key={key} {...messageItemLayout}>
                  <span className={styles.messageTitle}>
                    {formatMessage({ id: `form.common.apply.${key}` })}:
                  </span>
                  <span>{formatMessage({ id: `form.goodsApplyBillManage.message.${key}` })}</span>
                </Col>
              ))}
            </Row>
            {productItemShow && (
              <span>
                <Form.Item label={formatMessage({ id: 'form.goodsApplyBillManage.selectProduct' })}>
                  {getFieldDecorator('spuCode', {
                    rules: [
                      {
                        required: true,
                        message: formatMessage({
                          id: 'form.goodsApplyBillManage.selectProduct.placeholder',
                        }),
                      },
                    ],
                  })(
                    <Select
                      showSearch
                      allowClear
                      placeholder=""
                      optionFilterProp="children"
                      filterOption={(input, option) => option.props.children.indexOf(input) >= 0}
                    >
                      {validProductAll.map(v => (
                        <Select.Option value={v.spuCode} key={v.spuCode}>
                          {`${v.spuCode} ${v.spuName}`}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
                <Row>
                  <Col {...messageItemLayout}>
                    <span>
                      {formatMessage({ id: `form.goodsApplyBillManage.message.spuMust` })}
                    </span>
                  </Col>
                </Row>
              </span>
            )}
          </Form>
        </div>
        <div>
          <div className={styles.modalHandleBtnArea}>
            <Button type="default" onClick={() => modalVisibleOnChange(false)}>
              {formatMessage({ id: 'button.common.cancel' })}
            </Button>
            <Button type="primary" onClick={this.onClickNext}>
              {formatMessage({ id: 'button.common.next' })}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ goodsapplybillmanage, user }) => ({
  validProductAll: goodsapplybillmanage.validProductAll || [],
  currentUser: user.currentUser || {},
}))(
  Form.create({
    // onValuesChange: (props, changedValues, allValues) => {
    //   const { dispatch } = props;
    //   dispatch({
    //     type: 'goodsapplybillmanage/changeGoodsRFInfo',
    //     payload: { ...allValues },
    //   });
    // },
  })(GoodsApplyBillAddType)
);
