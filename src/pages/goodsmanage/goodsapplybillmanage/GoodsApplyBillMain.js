import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { message } from 'antd';
import router from 'umi/router'; // eslint-disable-line
import { formatMessage } from 'umi/locale';
import GoodsApplyBillContent from './GoodsApplyBillContent';
import AuditTab from '../goodsapplybillaudit/goodsapplybilltab/auditTab';

moment.locale('zh-cn');
/**
 * 产品申请单新增、维护的界面
 */
class GoodsApplyBillMain extends React.Component {
  state = {};

  handleToList = () => {
    router.push('/goodsmanage/goodsapplybillmanage');
  };

  submit = (dataSource, submitType) => {
    const {
      dispatch,
      location: {
        query: { id = '', type = 'add' },
      },
    } = this.props;
    if (id && type === 'edit') {
      // 修改
      const { goodsRfInfo } = dataSource;
      if (goodsRfInfo && submitType === 'save') {
        // 保存按钮触发下，状态改成待提交
        goodsRfInfo.rfStatus = 'not_submitted';
      } else if (goodsRfInfo) {
        // 状态改成待审核
        goodsRfInfo.rfStatus = 'verify_waiting';
      }
      dispatch({
        type: 'goodsapplybillmanage/editApplyBill',
        payload: { ...dataSource, goodsRfInfo },
        callback: () => {
          message.success(formatMessage({ id: 'form.common.optionSuccess' }));
          router.push('/goodsmanage/goodsapplybillmanage');
        },
      });
    }
    if (!id && type === 'add') {
      // 新建
      const { goodsRfInfo } = dataSource;
      if (goodsRfInfo && submitType === 'save') {
        // 保存按钮触发下，状态改成待提交
        goodsRfInfo.rfStatus = 'not_submitted';
      } else if (goodsRfInfo) {
        // 状态改成待审核
        goodsRfInfo.rfStatus = 'verify_waiting';
      }
      dispatch({
        type: 'goodsapplybillmanage/addApplyBill',
        payload: dataSource,
        callback: () => {
          message.success(formatMessage({ id: 'form.common.optionSuccess' }));
          router.push('/goodsmanage/goodsapplybillmanage');
        },
      });
    }
  };

  render() {
    const {
      goodsRfInfo,
      parmPublicParameterApplyBillTypeAll,
      location: {
        query: { id = '', type = 'add' },
      },
    } = this.props;
    const titleObj = {
      add: formatMessage({ id: 'button.common.build' }),
      edit: formatMessage({ id: 'button.common.edit' }),
      see: formatMessage({ id: 'button.common.see' }),
    };
    const titleType = titleObj[type] ? titleObj[type] : '';
    const findVal = parmPublicParameterApplyBillTypeAll.find(
      v => v.parmCode === goodsRfInfo.rfTypeCode
    );
    const titleDesc = findVal ? findVal.parmValue : '';
    const title = type === 'see' ? `${titleType}申请单详情` : `${titleType}${titleDesc}`;
    const buttonList =
      type === 'see'
        ? []
        : [
            { type: 'save', label: formatMessage({ id: 'button.common.save' }) },
            { type: 'submit', label: formatMessage({ id: 'button.goodsApplyBillManage.commit' }) },
          ];
    const logTab =
      type === 'see'
        ? [
            {
              key: 'auditTab',
              title: '历史审核记录',
              render: () => <AuditTab />,
            },
          ]
        : [];
    return (
      <GoodsApplyBillContent
        id={id} // 申请单id
        operationType={type}
        title={<h3>{title}</h3>} // 申请单顶部标题
        isFilter // 是否过滤rfStage为1的数据，默认值false，不过滤
        buttonList={buttonList} // 右上角按钮名称，为空的情况下代表没有右上角按钮
        onToList={this.handleToList} // 当刷新后没有申请id的时候跳转回页面函数
        onSubmit={this.submit} // 提交按钮返回所有数据(已处理符合接口的数据格式)
        defineTabList={logTab} // 审核记录
      />
    );
  }
}

export default connect(({ goodsapplybillmanage, parmpublicparametermanage }) => ({
  goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 申请单信息
  parmPublicParameterApplyBillTypeAll:
    parmpublicparametermanage.parmPublicParameterApplyBillTypeAll || [], // 申请单创建类型
}))(GoodsApplyBillMain);
