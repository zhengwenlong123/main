import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Input, Select, Card, Modal, message } from 'antd';
import moment from 'moment';
// eslint-disable-next-line import/no-extraneous-dependencies
import router from 'umi/router';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import GoodsApplyBillAddType from './GoodsApplyBillAddType';
import { serverUrl } from '@/defaultSettings';
import { wrapTid } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
// const status = [
//   formatMessage({ id: 'form.common.disabled' }),
//   formatMessage({ id: 'form.common.enabled' }),
// ];
/**
 * 产品申请单管理组件
 */
class GoodsApplyBillManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'rfName',
        key: 'rfName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.type' }),
        dataIndex: 'rfTypeCode',
        key: 'rfTypeCode',
        align: 'center',
        render: (text, record) => <span>{this.applyBillField(record, 'rfTypeCode')}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.apply.people' }),
        dataIndex: 'rfRequestUserName',
        key: 'rfRequestUserName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.apply.organization' }),
        dataIndex: 'rfRequestUserOrgName',
        key: 'rfRequestUserOrgName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.apply.status' }),
        key: 'rfStatus',
        align: 'center',
        render: (text, record) => <span>{this.applyBillField(record, 'rfStatus')}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.apply.time' }),
        key: 'rfCreateTime',
        dataIndex: 'rfCreateTime',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'left',
        onCell: () => ({ style: { minWidth: 50, maxWidth: 140 } }),
        render: (text, record) => (
          <div>
            <a
              type="primary"
              size="small"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.goodsApplyBillManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            {/* verify_reject审核驳回、not_submitted待提交状态开放维护功能 */}
            {['not_submitted', 'verify_reject'].includes(record.rfStatus) && (
              <span>
                <Divider type="vertical" />
                <a
                  type="primary"
                  size="small"
                  onClick={() =>
                    this.onClickActionExecuteEvent(
                      2,
                      this.hookProcess(2, record),
                      formatMessage({ id: 'button.goodsApplyBillManage.mod' })
                    )
                  }
                >
                  {formatMessage({ id: 'button.common.modify' })}
                </a>
              </span>
            )}
            {/* verify_waiting待审核状态开放撤销功能，撤销后变成驳回状态、审核说明为‘自行撤销’、并做操作记录 */}
            {['verify_waiting'].includes(record.rfStatus) && (
              <span>
                <Divider type="vertical" />
                <a type="primary" size="small" onClick={() => this.hookProcess(4, record)}>
                  {formatMessage({ id: 'button.common.revoke' })}
                </a>
              </span>
            )}
            {/* verify_reject审批驳回状态开放作废功能，单据作废需要做操作记录 */}
            {['verify_reject'].includes(record.rfStatus) && (
              <span>
                <Divider type="vertical" />
                <a type="primary" size="small" onClick={() => this.hookProcess(5, record)}>
                  {formatMessage({ id: 'button.common.nullify' })}
                </a>
              </span>
            )}
            {/* not_submitted待提交状态开放删除功能，将数据从数据库删除 */}
            {['not_submitted'].includes(record.rfStatus) && (
              <span>
                <Divider type="vertical" />
                <a type="primary" size="small" onClick={() => this.hookProcess(6, record)}>
                  {formatMessage({ id: 'button.common.delete' })}
                </a>
              </span>
            )}
          </div>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {
      brandCode: '',
      brandName: '',
      brandEnable: '',
    };
  }

  applyBillField = (record, field) => {
    const {
      parmpublicparametermanage: {
        parmPublicParameterGoodsRfStatus,
        parmPublicParameterApplyBillTypeAll,
      },
    } = this.props;
    const obj = {
      rfStatus: parmPublicParameterGoodsRfStatus,
      rfTypeCode: parmPublicParameterApplyBillTypeAll,
    };
    const findVal = obj[field].find(v => v.parmCode === record[field]);
    return findVal ? findVal.parmValue : '';
  };

  /**
   * 响应点击处理
   */
  hookProcess = (type, record) => {
    const val = { ...record };
    if (type === 1) {
      // 详情
      router.push({
        pathname: '/goodsmanage/goodsapplybillmanage/detail',
        query: {
          id: record.rfId,
          type: 'see',
        },
      });
    }
    if (type === 2) {
      // 维护
      router.push({
        pathname: '/goodsmanage/goodsapplybillmanage/detail',
        query: {
          id: record.rfId,
          type: 'edit',
        },
      });
    }
    if (type === 4) {
      // 撤销
      const params = {
        verifyDesc: formatMessage({ id: 'form.goodsApplyBillManage.verifyDesc.revoke' }),
        verifyResult: 'verify_reject',
        modalCont: {
          title: formatMessage({ id: 'form.goodsApplyBillManage.verifyRevoke.sure' }),
          content: `${formatMessage({ id: 'form.common.name' })}: ${record.rfName}`,
          okText: formatMessage({ id: 'button.common.ok' }),
          cancelText: formatMessage({ id: 'button.common.cancel' }),
          success: () => {
            message.success(
              formatMessage({ id: 'form.goodsApplyBillManage.verifyRevoke.success' })
            );
          },
        },
      };
      this.handleNullify(record, params);
    }
    if (type === 5) {
      // 单据作废
      const params = {
        verifyDesc: formatMessage({ id: 'form.goodsApplyBillManage.verifyDesc' }),
        verifyResult: 'bill_invalid',
        modalCont: {
          title: formatMessage({ id: 'form.goodsApplyBillManage.verifyNullify.sure' }),
          content: `${formatMessage({ id: 'form.common.name' })}: ${record.rfName}`,
          okText: formatMessage({ id: 'button.common.ok' }),
          cancelText: formatMessage({ id: 'button.common.cancel' }),
          success: () => {
            message.success(
              formatMessage({ id: 'form.goodsApplyBillManage.verifyNullify.success' })
            );
          },
        },
      };
      this.handleNullify(record, params);
    }
    if (type === 6) {
      // 删除
      this.handleDelete(record);
    }
    return val;
  };

  /**
   * 默认加载品牌数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-产品品牌数据(列表,分页)
    dispatch({
      type: 'goodsapplybillmanage/paginationGoodsApplyBill',
      payload: { page: 1, pageSize: 10 },
    });
    // 获取-根据公用参数类型定义代码[goods_rf_type]获取公用参数定义数据所有(产品申请单类型)
    dispatch({
      type: 'parmpublicparametermanage/getApplyBillType',
      payload: { parmTypeCode: 'goods_rf_type' },
    });
    // 获取-根据公用参数类型定义代码[goods_rf_type]获取公用参数定义数据所有(产品申请单状态数据)
    dispatch({
      type: 'parmpublicparametermanage/getGoodsRfStatus',
      payload: { parmTypeCode: 'goods_rf_status' },
    });
  }

  /**
   *作废处理／撤销处理
   */
  handleNullify = (record, { verifyDesc = '', verifyResult = '', modalCont = {} }) => {
    const {
      form: { getFieldsValue },
      dispatch,
      currentUser,
    } = this.props;
    const { tableCurrentPage } = this.state;
    this.searchContent = getFieldsValue();
    const searchCondition = this.searchContent;
    const params = {
      rfId: record.rfId,
      tid: wrapTid().tid,
      verifyDesc,
      verifyId: currentUser.id,
      verifyResult,
      verifyUserOrgId: wrapTid().tid,
      verifyUserOrgName: currentUser.name,
    };
    const { success, ...other } = modalCont;
    Modal.confirm({
      ...other,
      onOk() {
        dispatch({
          type: 'goodsapplybillmanage/applyBillNullify',
          payload: params,
          callback: res => {
            if (res) {
              success();
              dispatch({
                type: 'goodsapplybillmanage/paginationGoodsApplyBill',
                payload: { page: tableCurrentPage || 1, pageSize: 10, searchCondition },
              });
            }
          },
        });
      },
      onCancel() {},
    });
  };

  /** 删除申请单 */
  handleDelete = record => {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    const { tableCurrentPage } = this.state;
    this.searchContent = getFieldsValue();
    const searchCondition = this.searchContent;
    Modal.confirm({
      title: formatMessage({ id: 'form.goodsApplyBillManage.delete.sure' }),
      content: `${formatMessage({ id: 'form.common.name' })}: ${record.rfName}`,
      okText: formatMessage({ id: 'button.common.ok' }),
      cancelText: formatMessage({ id: 'button.common.cancel' }),
      onOk() {
        dispatch({
          type: 'goodsapplybillmanage/deleteApplyBill',
          payload: [record.id],
          callback: res => {
            if (res) {
              message.success(formatMessage({ id: 'form.goodsApplyBillManage.delete.success' }));
              dispatch({
                type: 'goodsapplybillmanage/paginationGoodsApplyBill',
                payload: { page: tableCurrentPage || 1, pageSize: 10, searchCondition },
              });
            }
          },
        });
      },
      onCancel() {},
    });
  };

  /**
   * 根据条件搜索
   */
  onFuzzyGoodsBrandhHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'goodsapplybillmanage/paginationGoodsApplyBill',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
    // 清空搜素框
    // setFieldsValue({
    //   brandCode: '',
    //   brandName: '',
    //   brandEnable: '',
    // });
  }

  /**
   * 导出文件请求
   */
  onExportGoodsBrandFile() {
    const values = this.searchContent;
    const params = {
      ...values,
    };
    const formElement = document.createElement('form');
    formElement.style.display = 'display:none;';
    formElement.method = 'post';
    formElement.action = `${serverUrl}/mdc/goods/goodsbrand/export`;
    formElement.target = 'callBackTarget';
    Object.keys(params).map(item => {
      const inputElement = document.createElement('input');
      inputElement.type = 'hidden';
      inputElement.name = item;
      inputElement.value = params[item] || '';
      formElement.appendChild(inputElement);
      return item;
    });
    document.body.appendChild(formElement);
    formElement.submit();
    document.body.removeChild(formElement);
  }

  tableOperateAreaCmp = () => {
    const {
      form: { getFieldDecorator },
      parmpublicparametermanage: { parmPublicParameterApplyBillTypeAll = [] },
    } = this.props;
    const { formItemLayout, tailFormItemLayout } = this.state;
    return (
      <div className={styles.dropdownOverlayArea}>
        <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
          <Form.Item
            label={formatMessage({ id: 'form.common.name' })}
            className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
          >
            {getFieldDecorator('rfName')(
              <Input
                style={{ width: 120 }}
                allowClear
                onPressEnter={() => this.onFuzzyGoodsBrandhHandle()}
              />
            )}
          </Form.Item>
          <Form.Item
            label={formatMessage({ id: 'form.common.type' })}
            className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
          >
            {getFieldDecorator('rfTypeCode')(
              <Select style={{ width: 80 }} allowClear>
                {parmPublicParameterApplyBillTypeAll.map(v => (
                  <Select.Option value={v.parmCode} key={v.id}>
                    {v.parmShowValue}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item
            {...tailFormItemLayout}
            className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
          >
            <Button onClick={() => this.onFuzzyGoodsBrandhHandle()}>
              {formatMessage({ id: 'button.common.query' })}
            </Button>
          </Form.Item>
        </Form>
        <Button
          type="primary"
          style={{ marginTop: 4 }}
          onClick={() =>
            this.onClickActionExecuteEvent(
              10,
              null,
              formatMessage({ id: 'button.goodsApplyBillManage.add' })
            )
          }
        >
          {formatMessage({ id: 'button.common.add' })}
        </Button>
      </div>
    );
  };

  render() {
    const {
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
    } = this.state;
    const {
      goodsApplyBillList,
      // goodsBrandList,
      goodsApplyBillListPageSize,
      parmpublicparametermanage: { parmPublicParameterApplyBillTypeAll = [] },
    } = this.props;

    const tablePaginationOnChangeEventDispatchType =
      'goodsapplybillmanage/paginationGoodsApplyBill';
    const modalContentCmp =
      modalUpkeepType === 10 ? (
        <GoodsApplyBillAddType
          modalUpkeepType={modalUpkeepType}
          modalUpkeepEntity={modalUpkeepEntity}
          modalVisibleOnChange={this.modalVisibleOnChange}
          modalOnChangePageCurrent={this.changeCurrentPage}
          parmPublicParameterApplyBillTypeAll={parmPublicParameterApplyBillTypeAll}
        />
      ) : (
        <span>{}</span>
      );
    return (
      <Card bordered={false}>
        <div>
          <MdcManageBlock
            tableTitle={formatMessage({ id: 'menu.goodsmanage.goodsapplybillmanage' })}
            tableColumns={tableColumns}
            tableDataSource={goodsApplyBillList === undefined ? [] : goodsApplyBillList}
            tableOperateAreaCmp={this.tableOperateAreaCmp()}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            tableTotalSize={goodsApplyBillListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
            modalTitle={formatMessage({ id: 'menu.goodsmanage.goodsapplybillmanage.add' })}
            modalWidth={800}
            modalVisible={modalVisible}
            modalBodyStyle={{ minHeight: 500 }}
            modalContentCmp={modalContentCmp}
            modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ parmpublicparametermanage, goodsapplybillmanage, user }) => ({
  goodsApplyBillListPageSize: goodsapplybillmanage.goodsApplyBillListPageSize || 0,
  parmpublicparametermanage,
  goodsApplyBillList: goodsapplybillmanage.goodsApplyBillList || [],
  currentUser: user.currentUser || {},
}))(Form.create()(GoodsApplyBillManage));
