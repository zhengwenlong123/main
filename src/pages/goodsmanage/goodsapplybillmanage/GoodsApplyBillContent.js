import React from 'react';
import { connect } from 'dva';
import { Card, Button, Spin, message } from 'antd';
import moment from 'moment';
import router from 'umi/router'; // eslint-disable-line
import uuid from 'uuid';
import { formatMessage } from 'umi/locale';
import SpuTab from './goodsapplybilltab/spuTab';
import GroupTab from './goodsapplybilltab/groupTab';
import SkuTab from './goodsapplybilltab/skuTab';
import MaterielTab from './goodsapplybilltab/materielTab';
import { wrapTid } from '@/utils/mdcutil';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增、维护的界面部分组件
 */
class GoodsApplyBillContent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeKey: 'spuTab',
    };
    this.spuTabRef = null;
    const tabList = [];
    const tabContent = {};
    const { defineTabList = [] } = props;
    defineTabList.forEach(v => {
      const { key, title, render } = v;
      if (key && title && render) {
        tabList.push({ key, tab: title });
        tabContent[key] = render();
      }
    });
    this.contentList = {
      spuTab: (
        <SpuTab
          onRef={val => {
            this.spuTabRef = val;
          }}
          operationType={props.operationType}
        />
      ),
      groupTab: <GroupTab operationType={props.operationType} />,
      skuTab: <SkuTab operationType={props.operationType} />,
      materielTab: <MaterielTab operationType={props.operationType} />,
      ...tabContent,
    };
    this.tabList = [
      {
        key: 'spuTab',
        tab: '产品',
      },
      {
        key: 'groupTab',
        tab: '商品分组',
      },
      {
        key: 'skuTab',
        tab: '商品',
      },
      {
        key: 'materielTab',
        tab: '物料',
      },
      ...tabList,
    ];
  }

  /**
   * 默认加载数据
   */
  componentDidMount() {
    const { dispatch, goodsRfInfo, id = '', onToList } = this.props;
    // dispatch({
    //   type: 'goodsapplybillmanage/clearModel',
    // });
    if (!goodsRfInfo.rfTypeCode && !id) {
      // 申请单类型代码没有选择的话退出到列表页
      onToList();
    } else if (id) {
      // 申请id存在、维护阶段，根据申请id取回申请单详细信息
      dispatch({
        type: 'goodsapplybillmanage/fetchApplyBillInfo',
        payload: {
          rfId: id,
        },
      });
    }
    // 产品类目下拉列表的查询
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsCatagoryListAll' });
    // 产品大类下拉列表的查询
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsClassificationListAll' });
    // 产品品牌下拉列表的查询
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsbrandListAll' });
    // ebs分类下拉列表的查询
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsEbsCatagoryListAll' });
    // 属性名称（所有）
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsAttrNameAll' });
    // 获取-根据公用参数类型定义代码[goods_spu_type]获取公用参数定义数据所有(产品类型)
    dispatch({
      type: 'parmpublicparametermanage/getProductType',
      payload: { parmTypeCode: 'goods_spu_type' },
    });
    // 获取-根据公用参数类型定义代码[goods_desc_type]获取公用参数定义数据所有(产品描述类型)
    dispatch({
      type: 'parmpublicparametermanage/getProductDesc',
      payload: { parmTypeCode: 'goods_desc_type' },
    });
    // 获取-根据公用参数类型定义代码[goods_unit_of_quantity]获取公用参数定义数据所有(商品数量单位类型数据)
    dispatch({
      type: 'parmpublicparametermanage/getSpuQuantityUnit',
      payload: { parmTypeCode: 'goods_unit_of_quantity' },
    });
    // 获取-根据公用参数类型定义代码[good_ebs_sub_attribute]获取公用参数定义数据所有(子分类数据)
    dispatch({
      type: 'parmpublicparametermanage/getGoodsEbsSubAttr',
      payload: { parmTypeCode: 'good_ebs_sub_attribute' },
    });
  }

  componentWillReceiveProps(nextProps) {
    const { id } = this.props;
    if (id !== nextProps.id) {
      this.refreshApplyBill(nextProps.id);
    }
  }

  /** 组件即将卸载 */
  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'goodsapplybillmanage/clearModel',
    });
  }

  /** 更换申请id刷新数据 */
  refreshApplyBill = id => {
    const { dispatch } = this.props;
    // dispatch({
    //   type: 'goodsapplybillmanage/clearModel',
    // });
    // 申请id存在、维护阶段，根据申请id取回申请单详细信息
    dispatch({
      type: 'goodsapplybillmanage/fetchApplyBillInfo',
      payload: {
        rfId: id,
      },
    });
  };

  /**
   * tab切换回调
   */
  tabChange = key => {
    // const { dispatch } = this.props;
    // 当切换到物料的时候，做如下处理(计算处理代码在reducers中)：
    // 1、如果当前物料已有值，则保持当前值渲染
    // 2、如果当前物料为空数组，则进行计算匹配出物料默认值
    // if (key === 'materielTab') {
    //   dispatch({
    //     type: 'goodsapplybillmanage/toMaterielTab',
    //   });
    // }
    this.setState({
      activeKey: key,
    });
  };

  errorMessage = () => {
    message.error(formatMessage({ id: 'form.common.form.error' }));
  };

  /**
   * 提交申请单
   */
  submit = btnType => {
    // 提交校验各表单是否有错误
    this.spuTabRef
      .commit()
      .then(() => {
        this.submitData(btnType);
      })
      .catch(() => this.errorMessage());
  };

  submitData = btnType => {
    const { goodsRfInfo: rfInfo, id, onSubmit, isFilter = false } = this.props; // isFilter过滤掉rfStage中的为1的数据
    const spuCode = rfInfo.spuCode || uuid.v4();
    const defaultRfStage = 0; // 数据阶段， 0：申请单信息， 1：正式生效信息
    const defaultId = id
      ? {
          id: rfInfo.id || '', // 记录id
          rfId: rfInfo.rfId || '', // 申请单id
          spuId: rfInfo.spuId || '', // 产品id
          tid: wrapTid().tid, // 租户id
        }
      : {
          tid: wrapTid().tid, // 租户id
          rfId: rfInfo.rfId || '', // 申请单id
        }; // 编辑状态下拥有默认值
    const goodsRfInfo = this.getGoodsRfInfo(spuCode, defaultRfStage, defaultId, isFilter); // 申请单信息
    const goodsRfSpuInfo = this.getGoodsRfSpuInfo(spuCode, defaultRfStage, defaultId, isFilter); // 申请单-产品信息
    const goodsRfSkuGroupList = this.getGoodsRfSkuGroupList(
      spuCode,
      defaultRfStage,
      defaultId,
      isFilter
    ); // 申请单-商品（单品）分组
    const goodsRfSkuInfoList = this.getGoodsRfSkuInfoList(
      spuCode,
      defaultRfStage,
      defaultId,
      isFilter
    ); // 申请单-商品（单品）信息
    const goodsRfMaterielInfoList = this.getGoodsRfMaterielInfoList(
      spuCode,
      defaultRfStage,
      defaultId,
      isFilter
    ); // 申请单-商品（单品）物料信息
    onSubmit(
      {
        goodsRfInfo,
        goodsRfSpuInfo,
        goodsRfSkuGroupList,
        goodsRfSkuInfoList,
        goodsRfMaterielInfoList,
      },
      btnType
    );
  };

  /**
   * 申请单信息
   */
  getGoodsRfInfo = (spuCode, defaultRfStage, defaultId) => {
    const { goodsRfInfo } = this.props;
    return { ...goodsRfInfo, spuCode, ...defaultId };
  };

  /**
   * 申请单-产品信息
   */
  getGoodsRfSpuInfo = (spuCode, defaultRfStage, defaultId, isFilter) => {
    const {
      goodsRfSpuInfo, // 基本信息（dva）
      goodsRfSpuAttrNormalList: commonAttr, // 普通属性（dva）
      goodsSkuAttrUsed, // sku属性信息（dva）
      goodsRfSpuDescList: descInfo, // 描述信息 （dva）
      goodsSpuModellist: modelList, // 产品型号 （dva）
      goodsRfSpuSubAttrInfo: spuSubAttr, // 商品分组从属属性设定(dva)
    } = this.props;
    const basicInfo =
      isFilter && goodsRfSpuInfo.rfStage === 1
        ? {}
        : {
            // 基本信息
            ...goodsRfSpuInfo,
            catagoryCode: goodsRfSpuInfo.catagoryCode[goodsRfSpuInfo.catagoryCode.length - 1], // 产品类目只取数组中最后一位数
            spuTimeToMarket: goodsRfSpuInfo.spuTimeToMarket
              ? moment(goodsRfSpuInfo.spuTimeToMarket).format('YYYY-MM-DD')
              : '', // 时间处理成时间字符串
            spuEnable: 1, // 产品状态，0：无效， 1：有效 默认值1
            spuCode, // 产品代码
            productLine: '', // 产品系列
            seq: 0, // 排列序号
            rfStage: goodsRfSpuInfo.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
            ...defaultId,
          };
    const commonAttrInvalid = isFilter ? commonAttr.filter(v => v.rfStage !== 1) : [...commonAttr]; // 过滤rfStage为1
    const goodsRfSpuAttrNormalList = commonAttrInvalid.map(v => ({
      // 普通属性
      attrCode: v.attr.key, // 属性代码
      attrName: v.attr.label, // 属性名称
      attrValue: v.attrValue, // 设定值
      rfStage: v.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
      attrEnable: 1, // 产品状态，0：无效， 1：有效 默认值1
      attrDesc: '', // 属性描述
      spuCode, // 产品代码
      ...defaultId,
    }));
    const goodsRfSpuAttrSkuList = goodsSkuAttrUsed.map((v, ind) => {
      // 产品sku属性集合
      const invalid = isFilter ? v.tags.filter(i => i.rfStage !== 1) : [...v.tags]; // 过滤rfStage为1
      const goodsRfSpuAttrSkuItemList = invalid.map((t, index) => ({
        skuAttrItemCode: t.code, // SKU属性项代码
        skuAttrItemValue: t.name, // SKU属性项值
        skuAttrItemDesc: t.name, // SKU属性项描述
        skuAttrItemEnable: 1, // SKU属性项状态，0：无效， 1：有效 默认值1
        skuAttrCode: v.attr.key, // SKU属性代码
        skuAttrName: v.attr.label, // SKU属性名称
        rfStage: t.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
        spuCode, // 产品代码
        seq: t.seq || index, // 排列序号
        ...defaultId,
      }));
      return isFilter && v.attr.rfStage === 1
        ? { goodsRfSpuAttrSkuItemList }
        : {
            skuAttrCode: v.attr.key, // SKU属性代码
            skuAttrName: v.attr.label, // SKU属性名称
            goodsRfSpuAttrSkuItemList, // 产品SKU属性项值使用的DTO
            skuAttrNameEnable: 1, // 状态，0：无效， 1：有效 默认值1
            rfStage: v.attr.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
            spuCode, // 产品代码
            seq: ind + 1, // 排列序号
            ...defaultId,
          };
    });

    // 商品分组从属属性设定
    const spuGroupAttrSku = {
      skuAttrCode: spuSubAttr.key || '', // SKU属性代码
      skuAttrName: spuSubAttr.label || '', // SKU属性名称
      rfStage: spuSubAttr.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
      spuCode, // 产品代码
      ...defaultId,
    };
    const goodsRfSpuGroupAttrSku =
      isFilter && spuSubAttr.rfStage === 1 ? null : { ...spuGroupAttrSku };

    // 产品描述集合计算
    const descInfoInvalid = isFilter ? descInfo.filter(v => v.rfStage !== 1) : [...descInfo]; // 过滤rfStage为1
    const goodsRfSpuDescList = descInfoInvalid.map((v, index) => ({
      // 产品描述集合
      descTypeCode: v.descTypeCode, // 描述类型代码
      descName: v.descName, // 描述名称
      descNotes: v.descNotes, // 描述备注
      address: v.address, // 存放地址
      descEnable: 1, // 描述状态
      rfStage: v.rfStage || defaultRfStage, // 数据阶段
      seq: index, // 排列序号
      rid: v.rid || 0, // 记录编号
      spuCode, // 产品代码
      ...defaultId,
    }));
    const modelListInvalid = isFilter ? modelList.filter(v => v.rfStage !== 1) : [...modelList]; // 过滤rfStage为1
    const goodsRfSpuModelList = modelListInvalid.map(v => ({
      // 产品型号集合
      modelCode: v.modelCode, // 产品型号
      modelDesc: v.modelCode, // 型号描述
      modelName: v.modelCode, // 型号名称
      modelShortName: v.modelCode, // 型号简称
      modelEnable: 1, // 型号状态
      rfStage: v.rfStage || defaultRfStage, // 数据阶段
      spuCode, // 产品代码
      ...defaultId,
    }));

    return {
      ...basicInfo, // 基本信息
      goodsRfSpuAttrNormalList, // 普通属性
      goodsRfSpuAttrSkuList: goodsRfSpuAttrSkuList.filter(v => {
        // 过滤空值goodsRfSpuAttrSkuItemList
        if (
          !v.skuAttrCode &&
          v.goodsRfSpuAttrSkuItemList &&
          v.goodsRfSpuAttrSkuItemList.length === 0
        ) {
          return false;
        }
        return true;
      }), // 产品sku属性集合
      goodsRfSpuDescList, // 产品描述集合
      goodsRfSpuModelList, // 产品型号集合
      goodsRfSpuGroupAttrSku, // 商品分组从属属性名
    };
  };

  /**
   * 申请单-商品（单品）分组
   */
  getGoodsRfSkuGroupList = (spuCode, defaultRfStage, defaultId, isFilter) => {
    const {
      goodsRfGroupList = [], // 商品分组列表数据(新)
      goodsRfGroupListValid = [], // 商品分组列表数据(已生效)
      goodsSkuAttrUsed = [], // sku属性信息（dva）
      goodsRfSpuSubAttrInfo: spuSubAttr, // 商品分组从属属性设定(dva)
    } = this.props;
    const noSubAttr = goodsSkuAttrUsed.filter(v => v.attr.key !== spuSubAttr.key).map(i => i.attr); // 非从属属性的sku属性集合数据
    const invalidList = isFilter
      ? [...goodsRfGroupList]
      : [...goodsRfGroupListValid, ...goodsRfGroupList];
    return invalidList.map((item, index) => ({
      goodsRfSkuGroupSkuItemList: noSubAttr.map((v, ind) => ({
        // 商品（单品）分组属性值
        groupCode: item.groupCode, // 商品分组代码
        skuAttrCode: v.key, // SKU属性代码
        skuAttrName: v.label, // SKU属性名称
        skuAttrItemCode: item[v.key].code, // SKU属性项代码
        skuAttrItemName: item[v.key].name, // SKU属性项名称
        rfStage: item.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
        spuCode, // 产品代码
        seq: ind, // 序号
        ...defaultId,
      })),
      groupCode: item.groupCode, // 商品分组代码
      groupName: item.groupName, // 商品分组名称
      groupShortName: item.groupShortName || item.groupName, // 商品分组简称
      subAttrCode1: item.subAttrCode, // 从属的属性代码1
      subAttrName1: item.subAttrName, // 从属的属性名称1
      groupDesc: item.groupName, // 商品分组描述
      groupId: item.groupId || '', // 商品分组id
      groupEnable: 1, // 商品分组状态 0：无效， 1：有效 默认值1
      rfStage: item.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
      spuCode, // 产品代码
      seq: index, // 排列序号`
      ...defaultId,
    }));
  };

  /**
   * 申请单-商品（单品）信息
   */
  getGoodsRfSkuInfoList = (spuCode, defaultRfStage, defaultId, isFilter) => {
    const {
      goodsSkulist, // 商品数据（新建）
      goodsSkulistValid, // 商品数据（已建）
    } = this.props;
    const invalidList = isFilter ? [...goodsSkulist] : [...goodsSkulistValid, ...goodsSkulist]; // 过滤
    return invalidList.map((item, index) => ({
      goodsRfSkuAttrSkuList: this.HandleGoodsRfSkuAttrSkuList(
        item,
        spuCode,
        item.skuCode || item.id,
        defaultRfStage,
        defaultId
      ),
      skuName: item.skuName, // 商品名称
      skuShortName: item.skuShortName, // 商品简称
      groupCode: item.groupCode, // 商品分组代码
      groupId: item.groupId || '', // 商品分组id
      defaultModelCode: item.defaultModelCode, // 缺省产品型号代码
      unitOfQuantityCode: item.unitOfQuantity && item.unitOfQuantity.key, // 数量单位代码
      unitOfQuantityName:
        item.unitOfQuantityName ||
        this.findUnitOfQuantityName(item.unitOfQuantity && item.unitOfQuantity.key), // 数量单位名称
      skuDesc: item.skuDesc || item.skuName, // 商品简介
      skuCode: item.skuCode || item.id, // 商品代码
      skuEnable: 1, // 商品状态 0：无效， 1：有效 默认值1
      rfStage: item.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
      spuCode, // 产品代码
      seq: index, // 排列序号
      skuId: '', // 商品id
      ...defaultId,
      goodsRfSkuEbs: {
        // ebs相关信息
        ebsCatagoryCode: item.ebsCatagoryCode[item.ebsCatagoryCode.length - 1], // ebs分类代码只取数组中最后一位数
        ebsSubCatagory: (item.ebsSubCatagory && item.ebsSubCatagory.key) || '', // ebs子分类代码
        rfStage: item.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
        skuCode: item.skuCode || item.id, // 商品代码
        spuCode, // 产品代码
        ...defaultId,
      },
    }));
  };

  /**
   *申请单-商品（单品）物料信息
   */
  getGoodsRfMaterielInfoList = (spuCode, defaultRfStage, defaultId, isFilter) => {
    const { goodsMaterielList } = this.props;
    const invalidList = isFilter
      ? goodsMaterielList.filter(v => v.rfStage !== 1)
      : [...goodsMaterielList]; // 过滤rfStage !== 1
    return invalidList.map((item, index) => ({
      goodsRfMaterielEbs: {
        // ebs对接
        customizedMachineType: item.customizedMachineType, // 定制机类型
        manufactureMaterielCode: item.manufactureMaterielCode, // 厂家物料编码
        materielCode: item.materielCode, // 物料编码
        rfStage: item.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
        skuCode: item.skuCode, // 商品代码
        spuCode, // 产品代码
        ...defaultId,
      },
      groupCode: item.groupCode || '', // 商品分组代码
      materielCode: item.materielCode, // 物料编码
      materielDesc: item.materielName, // 物料简介
      materielEnable: 1, // 物料状态 0：无效， 1：有效 默认值1
      materielName: item.materielName, // 物料名称
      materielShortName: item.materielShortName || item.materielName, // 物料简称
      modelCode: item.modelCode, // 产品型号代码
      rfStage: item.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
      series: item.series, // 系列
      skuCode: item.skuCode, // 商品代码
      spuCode, // 产品代码
      seq: index, // 排列序号
      ...defaultId,
    }));
  };

  /**
   * 处理数量单位label
   * @param {string} code  当前的数量单位代码
   */
  findUnitOfQuantityName = code => {
    const { parmPublicParameterQuantityUnit } = this.props;
    if (!code) return '';
    const unitOfQuantity = parmPublicParameterQuantityUnit.find(v => v.parmCode === code);
    return unitOfQuantity ? unitOfQuantity.parmValue : '';
  };

  // 处理商品列表中的sku属性数据
  HandleGoodsRfSkuAttrSkuList = (obj, spuCode, skuCode, defaultRfStage, defaultId) => {
    const { goodsSkuAttrUsed } = this.props;
    const arr = [];
    goodsSkuAttrUsed.forEach(element => {
      if (element.attr && element.attr.key) {
        const fieldVal = obj[element.attr.key];
        if (fieldVal) {
          const tag = element.tags.find(v => v.code === fieldVal);
          arr.push({
            skuAttrCode: element.attr.key, // SKU属性代码
            skuAttrName: element.attr.label, // SKU属性名称
            skuAttrDesc: element.attr.label, // SKU属性描述
            skuAttrItemCode: tag.code, // SKU属性代码
            skuAttrItemValue: tag.name, // SKU属性项值
            skuAttrEnable: 1, // 状态 0：无效， 1：有效 默认值1
            rfStage: element.rfStage || defaultRfStage, // 数据阶段， 0：申请单信息， 1：正式生效信息
            skuCode, // 商品代码
            spuCode, // 产品代码
            skuId: '', // 商品id
            ...defaultId,
          });
        }
      }
    });
    return arr;
  };

  render() {
    const { activeKey } = this.state;
    const { title, goodsRfInfo, buttonList = [], loading, parmpublicparametermanage } = this.props;
    const getRfTypeName = code => {
      // 获取申请单类型
      if (!code) return '';
      const { parmPublicParameterApplyBillTypeAll = [] } = parmpublicparametermanage || {};
      const rfType = parmPublicParameterApplyBillTypeAll.find(v => v.parmCode === code);
      return rfType ? rfType.parmValue : '';
    };
    const extra = (
      <span>
        {buttonList.map(v => (
          <Button
            key={v.type}
            style={{ marginLeft: 8 }}
            type="primary"
            onClick={() => this.submit(v.type)}
          >
            {v.label}
          </Button>
        ))}
      </span>
    );
    const headerTitle = (
      <span>
        <div>{title}</div>
        <div style={{ fontSize: 12, color: '#6F6A6A', marginTop: 8 }}>
          <span style={{ marginRight: 30 }}>申请单名称：{goodsRfInfo.rfName || ''}</span>
          <span>申请单类型：{getRfTypeName(goodsRfInfo.rfTypeCode)}</span>
        </div>
      </span>
    );
    return (
      <Card
        style={{ width: '100%' }}
        title={headerTitle}
        extra={extra}
        tabList={this.tabList}
        activeTabKey={activeKey}
        onTabChange={this.tabChange}
      >
        {loading && <Spin />}
        {!loading &&
          goodsRfInfo.rfTypeCode &&
          this.tabList.map(item => (
            <div key={item.key} style={{ display: activeKey === item.key ? 'block' : 'none' }}>
              {this.contentList[item.key]}
            </div>
          ))}
      </Card>
    );
  }
}

export default connect(({ goodsapplybillmanage, parmpublicparametermanage, loading }) => ({
  goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 申请单信息
  goodsRfSpuInfo: goodsapplybillmanage.goodsRfSpuInfo || {}, // 产品基本信息
  goodsSpuModellist: goodsapplybillmanage.goodsSpuModellist || [], // 产品型号
  goodsRfSpuDescList: goodsapplybillmanage.goodsRfSpuDescList || [], // 产品描述
  goodsSkuAttrUsed: goodsapplybillmanage.goodsSkuAttrUsed || [], // sku属性(产品tab中sku信息已设置好的属性内容)
  goodsRfSpuAttrNormalList: goodsapplybillmanage.goodsRfSpuAttrNormalList || [], // 产品普通属性
  goodsSkulist: goodsapplybillmanage.goodsSkulist || [], // 商品列表数据(新)
  goodsSkulistValid: goodsapplybillmanage.goodsSkulistValid || [], // 商品列表数据(已生效))
  goodsMaterielList: goodsapplybillmanage.goodsMaterielList || [], // 物料数据
  parmPublicParameterQuantityUnit: parmpublicparametermanage.parmPublicParameterQuantityUnit || [], // 数量单位类型数据
  goodsRfSpuSubAttrInfo: goodsapplybillmanage.goodsRfSpuSubAttrInfo || {}, // 商品分组从属属性设定值
  goodsRfGroupList: goodsapplybillmanage.goodsRfGroupList || [], // 商品分组列表数据(新)
  goodsRfGroupListValid: goodsapplybillmanage.goodsRfGroupListValid || [], // 商品分组列表数据(已生效)
  parmpublicparametermanage,
  loading: loading.effects['goodsapplybillmanage/fetchApplyBillInfo'],
}))(GoodsApplyBillContent);
