import React from 'react';
import { Button, Form, Icon } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import styles from './DynamicFormSet.less';

moment.locale('zh-cn');

const formItemLayoutWithOutLabel = {
  wrapperCol: {
    xs: { span: 24, offset: 0 },
    sm: { span: 20, offset: 4 },
    xl: { span: 19, offset: 2 },
  },
};

/**
 * 产品申请单新增：产品型号部分组件
 */
export default function DynamicFormSet(props) {
  const {
    form,
    formItem,
    initialValue = [],
    registerField = {},
    fieldName = '',
    onRemove,
    onAdd,
    beforeRemove, // 类型：function，移除之前的操作回调函数
    beforeAdd, // 类型：function，添加之前的操作回调函数
    addDisabled = false,
    formItemLayoutAddBtn = { ...formItemLayoutWithOutLabel },
  } = props;
  const { getFieldDecorator, getFieldValue } = form;

  const registerObj = () => ({
    id: String(new Date().getTime()),
    ...registerField,
  });

  const remove = (k, record, index) => {
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    const arr = form.getFieldValue(fieldName);
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }
    const setValue = {};
    if (fieldName) {
      arr.splice(index, 1);
      setValue[fieldName] = arr;
    }
    // can use data-binding to set
    form.setFieldsValue(
      {
        keys: keys.filter(key => key.id !== k),
        ...setValue,
      },
      () => {
        if (onRemove) onRemove(k, record, index);
      }
    );
  };

  const handleRemove = (k, record, index) => {
    // beforeRemove移除数据之前的回调函数，返回值isRemove为false代表终止移除数据的操作，true代表继续移除
    const isRemove = beforeRemove
      ? beforeRemove(k, record, index, flag => {
          if (flag) remove(k, record, index);
        })
      : true;
    if (!isRemove) return;
    remove(k, record, index);
  };

  const add = () => {
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(registerObj());
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue(
      {
        keys: nextKeys,
      },
      () => {
        if (onAdd) onAdd();
      }
    );
  };

  const handleAdd = () => {
    // beforeAdd添加一行数据之前的回调函数，返回值isAdd为false代表终止添加数据的操作，true代表继续添加
    const isAdd = beforeAdd
      ? beforeAdd(flag => {
          if (flag) add();
        })
      : true;
    if (!isAdd) return;
    add();
  };

  getFieldDecorator('keys', { initialValue });
  const keys = getFieldValue('keys');
  const formItems = keys.map((k, index) =>
    formItem(k, index, () => {
      return keys.length > 1 ? (
        <Icon
          key={k.id}
          className="dynamic-delete-button"
          type="minus-circle-o"
          onClick={() => handleRemove(k.id, k, index)}
        />
      ) : null;
    })
  );
  return (
    <div className={styles.modalHandleFormScopeArea}>
      <Form>
        {formItems}
        {!addDisabled && (
          <Form.Item {...formItemLayoutAddBtn}>
            <Button type="dashed" onClick={handleAdd} style={{ width: '120px' }}>
              <Icon type="plus" /> {formatMessage({ id: 'button.common.add' })}
            </Button>
          </Form.Item>
        )}
      </Form>
    </div>
  );
}
