/**
 * 产品模糊搜索
 * labelInValue: 同时返回label和value
 * onChange：值发生改变时回调函数
 */
import React, { PureComponent } from 'react';
import { Select, Spin } from 'antd';
import { connect } from 'dva';
import debounce from 'lodash/debounce';

const { Option } = Select;

@connect(({ productBusiness, loading }) => ({
  productBusiness,
  loading: loading.effects['productBusiness/productSearch'],
}))
class ProductVagueSearch extends PureComponent {
  constructor(props) {
    super(props);
    this.fetchProduct = debounce(this.fetchProduct, 500);
    this.state = {
      fetchProductList: [],
    };
  }

  componentDidMount() {
    this.fetchProduct();
  }

  fetchProduct = value => {
    const { dispatch } = this.props;
    const payload = { pageNo: 1, pageSize: 10, genericNameChs: value };
    // 采用的是药品标品库通用名搜索
    dispatch({
      type: 'productBusiness/productSearch',
      payload,
      errText: '搜索产品失败',
    });
  };

  onChange = val => {
    const { fetchProductList } = this.state;
    const { onChange } = this.props;
    if (!val) {
      onChange(val, {});
      return;
    }
    const obj = fetchProductList.find(item => item.code === val.key);
    onChange(val, obj);
  };

  render() {
    const {
      labelInValue,
      value,
      productBusiness: { spuList },
      loading,
      width = '100%',
    } = this.props;
    return (
      <Select
        value={value}
        filterOption={false}
        showSearch
        allowClear
        loading={loading}
        labelInValue={labelInValue || false}
        showArrow={false}
        notFoundContent={loading ? <Spin size="small" /> : <div>暂无数据</div>}
        onSearch={this.fetchProduct}
        onChange={this.onChange}
        placeholder="输入通用名关键字搜索"
        style={{ width }}
      >
        {spuList.map(item => (
          <Option key={item.code} value={item.code}>
            {item.code} {item.genericNameChs}
          </Option>
        ))}
      </Select>
    );
  }
}

export default ProductVagueSearch;
