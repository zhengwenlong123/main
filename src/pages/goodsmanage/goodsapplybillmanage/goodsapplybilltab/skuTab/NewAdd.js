/*
 * @Author: mikey.zhaopeng
 * @Date: 2019-10-11 22:39:04
 * @Last Modified by: dana_chen@sina.cn
 * @Last Modified time: 2019-12-12 19:16:11
 */
import React from 'react';
import { Select, Input, Button, Card, message, Cascader, Modal } from 'antd';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import EditableTableBasic from '@/components/EditableTable/EditableTableBasic';
import EditableTableBlock from '@/components/EditableTable/EditableTableBlock';
import { getTreeParent } from '@/utils/utils';
import styles from './index.less';

/**
 * 必填项标题处理
 * @param {*} label 标题
 */
const headerColumntitle = id => (
  <span>
    <span style={{ color: 'red' }}>*</span>
    {formatMessage({ id })}
  </span>
);

class NewAdd extends EditableTableBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: headerColumntitle('form.common.name'),
        dataIndex: 'skuName',
        // width: 200,
        editable: true,
        rules: [
          { required: true, message: formatMessage({ id: 'form.common.name.placeholder' }) },
          { validator: (rule, value, callback) => this.validatorSkuName(rule, value, callback) },
        ],
        renderInput: form => (
          <Input style={{ minWidth: 100 }} onBlur={e => this.skuNameBlur(e, form)} allowClear />
        ),
      },
      {
        title: headerColumntitle('form.common.shortName'),
        dataIndex: 'skuShortName',
        // width: 140,
        editable: true,
        rules: [
          { required: true, message: formatMessage({ id: 'form.common.shortName.placeholder' }) },
        ],
        renderInput: () => <Input style={{ minWidth: 100 }} allowClear />,
      },
      {
        title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode' }),
        dataIndex: 'groupName',
        onCell: () => ({ style: { minWidth: 100 } }),
        render: (text, record) => <span>{text && `${record.groupName}`}</span>,
      },
      {
        title: formatMessage({ id: 'form.goodsApplyBillManage.title.defaultModel' }),
        dataIndex: 'defaultModelCode',
        editable: true,
        // onCell: () => ({ style: { minWidth: 80 } }),
        rules: [
          { validator: (rule, value, callback) => this.validatorModelCode(rule, value, callback) },
        ],
        render: text => <span>{text}</span>,
        renderInput: () => this.spuModelSelect(),
      },
      {
        title: headerColumntitle('form.goodsSkuBasicUpkeep.unitOfQuantity'),
        dataIndex: 'unitOfQuantity',
        editable: true,
        onCell: () => ({ style: { minWidth: 80 } }),
        rules: [
          {
            required: true,
            message: formatMessage({ id: 'form.goodsSkuBasicUpkeep.unitOfQuantity.placeholder' }),
          },
        ],
        render: text => <span>{text && text.label}</span>,
        renderInput: () => this.paramsSelect('parmPublicParameterQuantityUnit'),
      },
      {
        title: headerColumntitle('form.goodsApplyBillManage.title.ebscategory'),
        dataIndex: 'ebsCatagoryCode',
        editable: true,
        onCell: () => ({ style: { minWidth: 80 } }),
        rules: [
          {
            required: true,
            message: formatMessage({
              id: 'form.goodsApplyBillManage.title.ebscategory.placeholder',
            }),
          },
        ],
        render: text => this.ebsRender(text),
        renderInput: () => this.ebsSelect(),
      },
      {
        title: formatMessage({ id: 'form.goodsApplyBillManage.title.subcategory' }),
        dataIndex: 'ebsSubCatagory',
        editable: true,
        onCell: () => ({ style: { minWidth: 80 } }),
        render: text => <span>{text && text.label}</span>,
        renderInput: () => this.paramsSelect('parmPublicParameterGoodsEbsSubAttr'),
      },
    ];
    const validatorDispatch = list => {
      // 筛选出已生效数据
      const validList = list.filter(v => v.skuName);
      // const validSku = props.goodsSkulist.filter(v => v.rfStage === 1);
      const payload = [...validList].map(v => ({ ...v, skuCode: v.skuCode || v.id }));
      return payload;
    }; // 校验是有效才保存到redux
    super(props, {
      tableColumns,
      validatorDispatch,
      rowKey: 'id',
      dispatchType: 'goodsapplybillmanage/changeGoodsSkulist',
    });
    this.handled = false;
  }

  componentDidMount() {
    const {
      goodsSkuAttrUsed,
      goodsApplyBillDetailInfo: { goodsRfSkuInfoList = [] },
    } = this.props;
    this.handleColumns(goodsSkuAttrUsed);
    this.initGoodsApplyBillDetailInfo(goodsRfSkuInfoList);
  }

  componentWillReceiveProps(nextProps) {
    const {
      goodsSkuAttrUsedChangeId,
      goodsSkuAttrUsedChangeClear,
      goodsApplyBillDetailInfo,
      goodsSpuModelChangeClear,
      goodsSkulist,
      dispatch,
    } = this.props;
    if (goodsSkuAttrUsedChangeId !== nextProps.goodsSkuAttrUsedChangeId) {
      // goodsSkuAttrUsedChangeId的值不一样代表goodsSkuAttrUsed已发生改变
      this.handleColumns(nextProps.goodsSkuAttrUsed);
    }
    if (goodsSkuAttrUsedChangeClear !== nextProps.goodsSkuAttrUsedChangeClear) {
      // sku属性发生改变
      this.initGoodsApplyBillDetailInfo([]); // 清空商品数据
      this.clearMaterielRemote(); // 清空物料数据
    }
    if (goodsSpuModelChangeClear !== nextProps.goodsSpuModelChangeClear) {
      // 产品型号发生改变
      const modelArr = nextProps.goodsSpuModellist.map(v => v.modelCode);
      const arr = goodsSkulist.filter(i => modelArr.includes(i.defaultModelCode));
      this.setState({
        nativeData: arr,
      });
      dispatch({
        // 只保存新建部分
        type: 'goodsapplybillmanage/changeGoodsSkulist',
        payload: arr,
      });
      if (Array.isArray(arr) && arr.length === 0) {
        this.clearMaterielRemote(); // 清空物料数据
      }
    }
    if (goodsApplyBillDetailInfo !== nextProps.goodsApplyBillDetailInfo) {
      const {
        goodsApplyBillDetailInfo: { goodsRfSkuInfoList = [] },
      } = nextProps;
      this.initGoodsApplyBillDetailInfo(goodsRfSkuInfoList);
    }
  }

  /** 初始化来自goodsApplyBillDetailInfo的数据 */
  initGoodsApplyBillDetailInfo = (goodsRfSkuInfoList = []) => {
    const { dispatch } = this.props;
    let payload = [];
    if (goodsRfSkuInfoList && goodsRfSkuInfoList.length > 0) {
      const arr = goodsRfSkuInfoList.map(item => ({ ...this.handleAttrToTableField(item) }));
      payload = arr.filter(v => v.rfStage !== 1);
    }
    this.setState({
      nativeData: payload,
    });
    dispatch({
      // 只保存新建部分
      type: 'goodsapplybillmanage/changeGoodsSkulist',
      payload,
    });
  };

  /**
   * 清除redux中的物料
   * 原因： 当商品清空包括有效商品也没有的时候，物料界面已不再加载，所以只能这里添加远程清除数据
   */
  clearMaterielRemote = () => {
    const { dispatch, goodsRfGroupListValid = [] } = this.props;
    if (goodsRfGroupListValid && goodsRfGroupListValid.length > 0) return;
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsMaterielList',
      payload: [],
    });
  };

  handleAttrToTableField = val => {
    const {
      parmPublicParameterGoodsEbsSubAttr,
      goodsEbsCatagoryListAll = [],
      goodsApplyBillDetailInfo: { goodsRfSkuGroupList = [] }, // 商品分组
    } = this.props;
    const {
      goodsRfSkuAttrSkuList = [],
      unitOfQuantityCode = '',
      unitOfQuantityName = '',
      goodsRfSkuEbs = {},
      ...other
    } = val;

    let catagoryCodeArr = [];
    if (goodsRfSkuEbs && goodsRfSkuEbs.ebsCatagoryCode) {
      catagoryCodeArr =
        getTreeParent(
          goodsEbsCatagoryListAll,
          goodsRfSkuEbs.ebsCatagoryCode,
          'ebsCatagoryCode',
          'ebsParentCode'
        ) || [];
    }
    const ebsCatagoryCode = catagoryCodeArr.map(v => v.ebsCatagoryCode) || [];

    const obj = {};
    goodsRfSkuAttrSkuList.forEach(v => {
      obj[v.skuAttrCode] = v.skuAttrItemCode;
    });
    const findGroup = goodsRfSkuGroupList.find(v => v.groupCode === val.groupCode);
    const groupName = findGroup ? findGroup.groupName : '';
    const itemObj = {
      ...other,
      ...obj,
      groupName,
      unitOfQuantity: { key: unitOfQuantityCode, label: unitOfQuantityName },
      ebsSubCatagory: {
        key: (goodsRfSkuEbs && goodsRfSkuEbs.ebsSubCatagory) || '',
        label: this.getLabel(
          parmPublicParameterGoodsEbsSubAttr,
          goodsRfSkuEbs && goodsRfSkuEbs.ebsSubCatagory
        ),
      },
      ebsCatagoryCode,
    };
    return itemObj;
  };

  /** 公共参数根据key获取label */
  getLabel = (list = [], key = '') => {
    if (!key) return '';
    const findVal = list.find(v => v.parmCode === key);
    return findVal ? findVal.parmValue : '';
  };

  /**
   * 动态处理sku属性字段展示函数
   * @param {array} arr  已配置了的所有sku属性数据
   * @param {array} disabledArr  需要处理的所有sku属性字段的集合
   */
  handleColumns = (arr, disabledArr = []) => {
    const second = arr.map(v => ({
      title: (
        <span>
          <span style={{ color: 'red' }}>*</span>
          {v.attr.label}
        </span>
      ),
      dataIndex: v.attr.key,
      editable: true,
      onCell: () => ({ style: { minWidth: 100 } }),
      rules: [
        {
          required: true,
          message: `${v.attr.label}${formatMessage({ id: 'form.common.notNull' })}`,
        },
      ],
      render: text => this.skuTextRender(v.attr.key, text),
      renderInput: form =>
        this.customizeSkuSelect(
          form,
          v.tags || [],
          disabledArr.includes(v.attr.key),
          v.attr.key,
          disabledArr
        ),
    }));
    const { tableColumns } = this.state;
    const first = tableColumns.slice(0, 3); // 取前三位数值
    const third = tableColumns.slice(-4); // 取后俩位数值
    // 列表前三位和后俩位固定，中间根据sku属性而定制
    this.setState({
      tableColumns: [...first, ...second, ...third],
    });
  };

  skuNameBlur = (e, form) => {
    const skuName = e.target.value;
    if (!form.getFieldsValue(['skuShortName']).skuShortName && skuName) {
      form.setFieldsValue({ skuShortName: skuName });
    }
  };

  /** sku属性下拉框 */
  customizeSkuSelect = (form, arr, disabled, key) => {
    return (
      <Select
        style={{ width: '100%', minWidth: 80 }}
        disabled={disabled}
        onChange={val => this.attrChange(form, { [key]: val })}
      >
        {arr.map(v => (
          <Select.Option value={v.code} key={v.code}>
            {v.name}
          </Select.Option>
        ))}
      </Select>
    );
  };

  /** sku属性的勾选自动带出商品分组 */
  attrChange = (form, currentVal) => {
    const {
      goodsSkuAttrUsed,
      goodsRfSpuSubAttrInfo,
      goodsRfGroupListValid,
      goodsRfGroupList,
    } = this.props;
    this.skuAtrrToSkuName(form, currentVal); // 由sku属性带出产品名称
    const { editingKey, rowKey, nativeData } = this.state;
    const attrValidList = goodsSkuAttrUsed.filter(v => v.attr.key !== goodsRfSpuSubAttrInfo.key); // 过滤掉商品从属属性
    const totalGoodsGroupList = [...goodsRfGroupListValid, ...goodsRfGroupList]; // 总共所有商品分组数据
    const attrValidKeys = attrValidList.map(v => v.attr.key);
    const getAttrValidVal = { ...form.getFieldsValue(attrValidKeys), ...currentVal };
    const findGroup = totalGoodsGroupList.find(v => {
      let flag = true;
      attrValidKeys.forEach(key => {
        const { code = '' } = v[key] || {};
        if (code !== getAttrValidVal[key]) {
          flag = false;
        }
      });
      return flag;
    });
    console.log('找到分组', findGroup);
    if (!findGroup) return;
    const arr = nativeData.map(v => {
      if (v[rowKey] === editingKey) {
        return { ...v, groupCode: findGroup.groupCode, groupName: findGroup.groupName };
      }
      return { ...v };
    });
    this.updateData(arr);
  };

  /**
   * 当产品名称不存在的情况下，sku属性的全部勾选后自动产生产品名称
   *
   */
  skuAtrrToSkuName = (form, currentVal) => {
    const getVal = form.getFieldsValue(['skuName']);
    if (getVal && getVal.skuName) return; // 产品名称已存在
    const { goodsSkuAttrUsed, goodsRfSpuSubAttrInfo, goodsRfSpuInfo } = this.props;
    const keys = goodsSkuAttrUsed.map(v => v.attr.key);
    const currentSkuKeys = { ...form.getFieldsValue(keys), ...currentVal };
    if (Object.keys(currentSkuKeys).filter(v => !currentSkuKeys[v]).length === 0) {
      // 全部sku属性都有值
      const skuObj = {};
      let skuLabel = `${(goodsRfSpuInfo && goodsRfSpuInfo.spuName) || ''}`; // 首位产品名称
      goodsSkuAttrUsed.forEach(item => {
        const tag = item.tags.find(i => i.code === currentSkuKeys[item.attr.key]);
        skuObj[item.attr.key] = { ...tag };
      });
      skuLabel = `${skuLabel}${skuObj[goodsRfSpuSubAttrInfo.key].name || ''}`; // 第二位从属属性项值
      Object.keys(skuObj).forEach(key => {
        // sku其它属性项值
        if (goodsRfSpuSubAttrInfo.key !== key) {
          skuLabel = `${skuLabel}${skuObj[key].name || ''}`;
        }
      });
      form.setFieldsValue({ skuName: skuLabel });
      if (!form.getFieldsValue(['skuShortName']).skuShortName) {
        // 产品简称不存在的话，也被默认初始
        form.setFieldsValue({ skuShortName: skuLabel });
      }
    }
  };

  /**
   * 非编辑状态下sku属性显示样式
   */
  skuTextRender = (skuKey, text) => {
    const { goodsSkuAttrUsed } = this.props;
    const findVal = goodsSkuAttrUsed.find(v => v.attr.key === skuKey);
    const tags = findVal ? findVal.tags : [];
    const findTag = tags.find(v => v.code === text);
    return <span>{findTag ? `${findTag.name}` : `${text}`}</span>;
  };

  /** 默认型号下拉框 */
  spuModelSelect = () => {
    const { goodsSpuModellist } = this.props;
    return (
      <Select style={{ width: '100%', minWidth: 70 }}>
        {goodsSpuModellist.map(v => (
          <Select.Option value={v.modelCode} key={v.modelCode}>
            {v.modelCode}
          </Select.Option>
        ))}
      </Select>
    );
  };

  /** 公共参数下拉框（数量单位、子分类） */
  paramsSelect = str => {
    return (
      <Select style={{ width: '100%', minWidth: 80 }} labelInValue>
        {this.props[str].map(v => (
          <Select.Option value={v.parmCode} key={v.id}>
            {v.parmValue}
          </Select.Option>
        ))}
      </Select>
    );
  };

  /** ebs分类 */
  ebsSelect = () => {
    const { goodsEbsCatagoryListAll = [] } = this.props;

    return (
      <Cascader
        fieldNames={{ label: 'ebsCatagoryName', value: 'ebsCatagoryCode', children: 'childrens' }}
        options={goodsEbsCatagoryListAll}
        popupClassName={styles.catagoryCascader}
        placeholder="ebs分类"
        style={{ width: '100%', minWidth: 80 }}
        showSearch={(inputValue, path) =>
          path.some(option => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1)
        }
      />
    );
  };

  ebsRender = (text = []) => {
    const { goodsEbsCatagoryListAll } = this.props;
    if (text.length === 0) return '';
    const code = text[text.length - 1]; // 取最后一位
    const ebsCatagoryCodeArr =
      getTreeParent(goodsEbsCatagoryListAll, code, 'ebsCatagoryCode', 'ebsParentCode') || [];
    return ebsCatagoryCodeArr.map(v => v.ebsCatagoryName).join('/');
  };

  /**
   * 商品名称唯一性校验
   */
  validatorSkuName = (rule, value, callback) => {
    const { editingKey, rowKey } = this.state;
    const { goodsSkulist, goodsSkulistValid } = this.props;
    if (value) {
      const skuNameDiff = goodsSkulist.find(v => v.skuName === value && v[rowKey] !== editingKey);
      const skuNameDiffValid = goodsSkulistValid.find(v => v.skuName === value);
      if (skuNameDiff || skuNameDiffValid) {
        callback(formatMessage({ id: 'form.goodsApplyBillManage.skuName.repeat' }));
      } else {
        callback();
      }
    } else {
      callback();
    }
  };

  /**
   * 默认型号是否存在校验
   * 若存在产品型号数据，则做默认型号字段存在性校验，否则不做检验
   */
  validatorModelCode = (rule, value, callback) => {
    const { goodsSpuModellist } = this.props;
    if (goodsSpuModellist.length > 0 && !value) {
      callback(formatMessage({ id: 'form.goodsApplyBillManage.title.defaultModel.placeholder' }));
    } else {
      callback();
    }
  };

  /**
   * 保存的时候校验商品组合：
   * 1、不同的商品间，SKU属性值组合不能完全相同
   */
  handleSave = (index, row, list) => {
    const { goodsSkuAttrUsed, goodsSkulistValid } = this.props;
    const val = [...goodsSkulistValid, ...list].filter(v => {
      const differ = goodsSkuAttrUsed.find(item => v[item.attr.key] !== row[item.attr.key]);
      // 如果遍历所有sku属性中没有找到list和row有不相等的sku属性项值，即俩者所有sku属性项值一样，返回true，否则返回false
      if (!differ) {
        return true;
      }
      return false;
    });
    // 当所有数据list过滤完，数值超过一个长度以上，说明已存在有相同组合sku属性值的商品
    if (val.length > 1) {
      message.error(
        formatMessage({ id: 'form.goodsApplyBillManage.message.skuAttrCombination.repeat' })
      );
    } else {
      if (this.handled) {
        this.handleColumns(goodsSkuAttrUsed);
        this.handled = false;
      }
      this.onSave(index, row, list);
    }
  };

  /** sku商品删除时确认提醒 */
  handleDelete = key => {
    const selectYesMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.select.yes' });
    const selectNoMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.select.no' });
    const deleteProductMessage = formatMessage({
      id: 'form.goodsApplyBillManage.message.delete.sku.relation',
    });
    const keepStatusMessage = formatMessage({
      id: 'form.goodsApplyBillManage.message.keep.status',
    });
    const content = (
      <span>
        <p>
          -{selectYesMessage}: {deleteProductMessage}
        </p>
        <p>
          -{selectNoMessage}: {keepStatusMessage}
        </p>
      </span>
    );
    Modal.confirm({
      title: formatMessage({ id: 'form.goodsApplyBillManage.message.delete.sku' }),
      content,
      width: 440,
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk: () => {
        this.onDelete(key);
      },
      onCancel() {},
    });
  };

  handleAddRow = () => {
    const { nativeData } = this.state;
    const { goodsSkulist, goodsSkulistValid } = this.props;
    const totalSku = [...goodsSkulist, ...goodsSkulistValid];
    if (totalSku.length > 0) {
      // 数量单位默认与已有商品的数量单位相同
      this.newAddRow({ unitOfQuantity: totalSku[0].unitOfQuantity }, nativeData);
    } else {
      this.newAddRow({}, nativeData);
    }
  };

  render() {
    const { editingKey, tableColumns, rowKey, nativeData } = this.state;
    const otherButton = {
      value: 'remove',
      label: formatMessage({ id: 'button.common.delete' }),
      onClick: this.handleDelete,
    };
    const extra = (
      <Button icon="plus" type="primary" onClick={() => this.handleAddRow()}>
        {formatMessage({ id: 'button.common.build' })}
      </Button>
    );
    return (
      <Card
        title={formatMessage({ id: 'form.common.building.product' })}
        bordered={false}
        extra={extra}
        className={styles.skuTabCard}
        headStyle={{ paddingLeft: 0, minHeight: 30, borderBottom: 0 }}
        bodyStyle={{ padding: 0 }}
      >
        <EditableTableBlock
          rowKey={rowKey}
          columns={tableColumns}
          dataList={nativeData}
          otherButton={otherButton}
          onSave={this.handleSave}
          onCanCel={this.onCanCel}
          editingKey={editingKey}
          editingKeyChange={this.editingKeyChange}
          scroll={{ x: true }}
        />
      </Card>
    );
  }
}

export default connect(({ goodsapplybillmanage, parmpublicparametermanage }) => ({
  goodsSpuModelChangeClear: goodsapplybillmanage.goodsSpuModelChangeClear || '', // 产品型号发生改变，物料、商品删除的标志
  goodsSkuAttrUsedChangeClear: goodsapplybillmanage.goodsSkuAttrUsedChangeClear || '', // sku属性发生改变，物料、商品清空的标志
  goodsSkuAttrUsedChangeId: goodsapplybillmanage.goodsSkuAttrUsedChangeId, // sku属性值goodsSkuAttrUsed发生改变都会赋予一个不一样的值
  goodsSkuAttrUsed: goodsapplybillmanage.goodsSkuAttrUsed || [], // sku属性(产品tab中sku信息已设置好的属性内容)
  goodsSkulist: goodsapplybillmanage.goodsSkulist || [], // 商品列表数据 （新建）
  goodsSkulistValid: goodsapplybillmanage.goodsSkulistValid || [], // 商品列表数据（已生效）
  goodsSpuModellist: goodsapplybillmanage.goodsSpuModellist || [], // 产品型号总数据
  goodsRfSpuInfo: goodsapplybillmanage.goodsRfSpuInfo || {}, // 产品基本信息
  parmPublicParameterQuantityUnit: parmpublicparametermanage.parmPublicParameterQuantityUnit || [], // 数量单位类型数据
  goodsApplyBillDetailInfo: goodsapplybillmanage.goodsApplyBillDetailInfo || {},
  goodsRfGroupListValid: goodsapplybillmanage.goodsRfGroupListValid || [], // 商品分组数据（已生效）
  goodsRfGroupList: goodsapplybillmanage.goodsRfGroupList || [], // 商品分组数据（新）
  goodsRfSpuSubAttrInfo: goodsapplybillmanage.goodsRfSpuSubAttrInfo || {}, // 商品分组从属属性设定值
  goodsEbsCatagoryListAll: goodsapplybillmanage.goodsEbsCatagoryListAll || [], // ebs分类
  parmPublicParameterGoodsEbsSubAttr:
    parmpublicparametermanage.parmPublicParameterGoodsEbsSubAttr || [], // 子分类数据
}))(NewAdd);
