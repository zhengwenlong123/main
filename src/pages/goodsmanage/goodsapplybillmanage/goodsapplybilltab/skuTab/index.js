import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import NewAdd from './NewAdd';
import SkuValid from './SkuValid';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增、维护的界面商品部分组件
 */
class SkuTab extends React.PureComponent {
  render() {
    const {
      operationType,
      goodsRfInfo,
      goodsApplyBillDetailInfo: { goodsRfSkuInfoList = [] },
    } = this.props;
    let dataRender = '';
    if (operationType === 'see') {
      // 查询详情
      dataRender = !goodsRfSkuInfoList.length ? (
        <div>
          <h3 className={styles.txtCenter}>
            {formatMessage({
              id: 'message.datasource.null',
            })}
          </h3>
        </div>
      ) : (
        <div>
          <SkuValid isSeeNewAdd />
          <SkuValid />
        </div>
      );
    } else if (goodsRfInfo && goodsRfInfo.rfTypeCode === 'materiel') {
      // 物料：只允许展示商品数据
      dataRender = (
        <div>
          <SkuValid />
        </div>
      );
    } else {
      dataRender = (
        <div>
          <NewAdd />
          <SkuValid />
        </div>
      );
    }
    return <div className={styles.skuTab}>{dataRender}</div>;
  }
}

export default connect(({ goodsapplybillmanage }) => ({
  goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 申请单信息
  goodsSkuAttrUsed: goodsapplybillmanage.goodsSkuAttrUsed || [], // sku属性(产品tab中sku信息已设置好的属性内容)
  goodsSkulist: goodsapplybillmanage.goodsSkulist || [], // 商品列表数据
  goodsApplyBillDetailInfo: goodsapplybillmanage.goodsApplyBillDetailInfo || {},
}))(SkuTab);
