/*
 * @Author: mikey.zhaopeng
 * @Date: 2019-10-11 22:39:04
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2019-11-03 10:25:46
 */
import React from 'react';
import { Card, Table } from 'antd';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import { getTreeParent } from '@/utils/utils';
import styles from './index.less';

class NewAdd extends React.Component {
  constructor(props) {
    super(props);
    this.fixedColumns = () => [
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'skuName',
      },
      {
        title: formatMessage({ id: 'form.common.shortName' }),
        dataIndex: 'skuShortName',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode' }),
        dataIndex: 'groupCode',
        editable: true,
        render: text => this.groupRender(text),
      },
      {
        title: formatMessage({ id: 'form.goodsApplyBillManage.title.defaultModel' }),
        dataIndex: 'defaultModelCode',
        render: text => <span>{text}</span>,
      },
      {
        title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.unitOfQuantity' }),
        dataIndex: 'unitOfQuantity',
        render: text => <span>{text && text.label}</span>,
      },
      {
        title: formatMessage({ id: 'form.goodsApplyBillManage.title.ebscategory' }),
        dataIndex: 'ebsCatagoryCode',
        // render: text => this.ebsRender(text),
      },
      {
        title: formatMessage({ id: 'form.goodsApplyBillManage.title.subcategory' }),
        dataIndex: 'ebsSubCatagory',
        render: text => <span>{text && text.label}</span>,
      },
    ];
    this.state = {
      tableColumns: [...this.fixedColumns()],
      nativeData: [],
    };
  }

  componentDidMount() {
    const {
      goodsSkuAttrUsed,
      goodsApplyBillDetailInfo: { goodsRfSkuInfoList = [] },
    } = this.props;
    this.handleColumns(goodsSkuAttrUsed);
    this.handleSetData(goodsRfSkuInfoList);
  }

  componentWillReceiveProps(nextProps) {
    const {
      goodsSkuAttrUsedChangeId,
      goodsSkuAttrUsedChangeClear,
      goodsApplyBillDetailInfo,
    } = this.props;
    if (goodsSkuAttrUsedChangeId !== nextProps.goodsSkuAttrUsedChangeId) {
      // goodsSkuAttrUsedChangeId的值不一样代表goodsSkuAttrUsed已发生改变
      this.handleColumns(nextProps.goodsSkuAttrUsed);
    }
    if (goodsSkuAttrUsedChangeClear !== nextProps.goodsSkuAttrUsedChangeClear) {
      this.handleSetData([]); // 清空商品数据
    }
    if (goodsApplyBillDetailInfo !== nextProps.goodsApplyBillDetailInfo) {
      const {
        goodsApplyBillDetailInfo: { goodsRfSkuInfoList = [] },
      } = nextProps;
      this.handleSetData(goodsRfSkuInfoList);
    }
  }

  // 处理商品数据格式
  handleSetData = (goodsRfSkuInfoList = []) => {
    const { isSeeNewAdd, dispatch } = this.props;
    let arr = [];
    if (goodsRfSkuInfoList && goodsRfSkuInfoList.length > 0) {
      arr = goodsRfSkuInfoList
        .filter(this.fliterRf)
        .map(item => ({ ...this.handleAttrToTableField(item) }));
    }
    this.setState({
      nativeData: arr,
    });
    if (!isSeeNewAdd) {
      // 已建界面下保存到redux
      dispatch({
        type: 'goodsapplybillmanage/changeGoodsSkulistValid',
        payload: arr,
      });
    }
  };

  fliterRf = v => {
    const { isSeeNewAdd } = this.props;
    if (isSeeNewAdd) {
      // 查看详情
      return v.rfStage !== 1; // 新建部分过滤
    }
    return v.rfStage === 1;
  };

  handleAttrToTableField = val => {
    const { parmPublicParameterGoodsEbsSubAttr, goodsEbsCatagoryListAll } = this.props;
    const {
      goodsRfSkuAttrSkuList = [],
      unitOfQuantityCode = '',
      unitOfQuantityName = '',
      goodsRfSkuEbs = {},
      ...other
    } = val;
    const obj = {};
    goodsRfSkuAttrSkuList.forEach(v => {
      obj[v.skuAttrCode] = v.skuAttrItemCode;
    });
    let ebsCatagoryCode = '';
    if (goodsRfSkuEbs && goodsRfSkuEbs.ebsCatagoryCode) {
      const ebsCatagoryCodeArr =
        getTreeParent(
          goodsEbsCatagoryListAll,
          goodsRfSkuEbs.ebsCatagoryCode,
          'ebsCatagoryCode',
          'ebsParentCode'
        ) || [];
      ebsCatagoryCode = ebsCatagoryCodeArr.map(v => v.ebsCatagoryName).join('/');
    }

    return {
      ...other,
      ...obj,
      unitOfQuantity: { key: unitOfQuantityCode, label: unitOfQuantityName },
      ebsCatagoryCode,
      ebsSubCatagory: {
        key: (goodsRfSkuEbs && goodsRfSkuEbs.ebsSubCatagory) || '',
        label: this.getLabel(
          parmPublicParameterGoodsEbsSubAttr,
          goodsRfSkuEbs && goodsRfSkuEbs.ebsSubCatagory
        ),
      },
    };
  };

  /** 公共参数根据key获取label */
  getLabel = (list, key) => {
    if (!key) return '';
    const findVal = list.find(v => v.parmCode === key);
    return findVal ? findVal.parmValue : '';
  };

  /**
   * 动态处理sku属性字段展示函数
   * @param {array} arr  已配置了的所有sku属性数据
   * @param {array} disabledArr  需要处理的所有sku属性字段的集合
   */
  handleColumns = (arr, disabledArr = []) => {
    const second = arr.map(v => ({
      title: v.attr.label,
      dataIndex: v.attr.key,
      editable: true,
      render: text => this.skuTextRender(v.attr.key, text),
      renderInput: () =>
        this.customizeSkuSelect(
          v.tags || [],
          disabledArr.includes(v.attr.key),
          v.attr.key,
          disabledArr
        ),
    }));
    // 列表前三位和后四位固定，中间根据sku属性而定制
    // const { tableColumns } = this.state;
    const { isSeeNewAdd, goodsRfInfo = {} } = this.props;
    const fixedColumnsList = [...this.fixedColumns()];
    const first = fixedColumnsList.slice(0, 3); // 取前三位数值
    const third = fixedColumnsList.slice(-4); // 取后俩四数值
    const skuCodeColumn = {
      // 商品编码列
      title: formatMessage({ id: 'form.common.codeVal' }),
      dataIndex: 'skuCode',
    };
    if (isSeeNewAdd && goodsRfInfo.rfStatus !== 'verify_adopt') {
      // 如果显示的是新建部分即isSeeNewAdd为true和非审核通过的，则没有商品编码
      this.setState({
        tableColumns: [...first, ...second, ...third],
      });
    } else {
      this.setState({
        tableColumns: [{ ...skuCodeColumn }, ...first, ...second, ...third],
      });
    }
  };

  /**
   * 非编辑状态下sku属性显示样式
   */

  skuTextRender = (skuKey, text) => {
    const { goodsSkuAttrUsed } = this.props;
    const findVal = goodsSkuAttrUsed.find(v => v.attr.key === skuKey);
    const tags = findVal ? findVal.tags : [];
    const findTag = tags.find(v => v.code === text);
    return <span>{findTag ? `${findTag.name}` : `${text}`}</span>;
  };

  /**
   * 非编辑状态下商品分组显示内容：分组编码 + 分组名称
   */
  groupRender = groupCode => {
    const { goodsRfGroupListValid, goodsRfGroupList } = this.props;
    const findVal = [...goodsRfGroupListValid, ...goodsRfGroupList].find(
      v => v.groupCode === groupCode
    );
    const groupName = findVal ? `${findVal.groupName}` : groupCode;
    return <span>{groupName}</span>;
  };

  render() {
    const { isSeeNewAdd } = this.props;
    const { tableColumns, nativeData = [] } = this.state;
    if (nativeData.length === 0) {
      return '';
    }
    const title = isSeeNewAdd
      ? formatMessage({ id: 'form.common.building.product' })
      : formatMessage({ id: 'form.common.built.product' });
    return (
      <Card
        title={title}
        bordered={false}
        className={styles.skuTabCard}
        headStyle={{ paddingLeft: 0, minHeight: 30, borderBottom: 0 }}
        bodyStyle={{ padding: 0 }}
      >
        <Table
          rowKey="skuCode"
          bordered
          size="small"
          dataSource={nativeData}
          columns={tableColumns}
          pagination={false}
          scroll={{ x: true }}
        />
      </Card>
    );
  }
}

export default connect(({ goodsapplybillmanage, parmpublicparametermanage }) => ({
  goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 申请单信息
  goodsSkuAttrUsedChangeId: goodsapplybillmanage.goodsSkuAttrUsedChangeId, // sku属性值goodsSkuAttrUsed发生改变都会赋予一个不一样的值
  goodsSkuAttrUsed: goodsapplybillmanage.goodsSkuAttrUsed || [], // sku属性(产品tab中sku信息已设置好的属性内容)
  goodsRfGroupListValid: goodsapplybillmanage.goodsRfGroupListValid || [], // 商品分组数据（已生效）
  goodsRfGroupList: goodsapplybillmanage.goodsRfGroupList || [], // 商品分组数据（新）
  goodsSkulist: goodsapplybillmanage.goodsSkulist || [], // 商品列表数据
  goodsSpuModellist: goodsapplybillmanage.goodsSpuModellist || [], // 产品型号总数据
  parmPublicParameterQuantityUnit: parmpublicparametermanage.parmPublicParameterQuantityUnit || [], // 数量单位类型数据
  goodsApplyBillDetailInfo: goodsapplybillmanage.goodsApplyBillDetailInfo || {},
  goodsEbsCatagoryListAll: goodsapplybillmanage.goodsEbsCatagoryListAll || [], // ebs分类
  parmPublicParameterGoodsEbsSubAttr:
    parmpublicparametermanage.parmPublicParameterGoodsEbsSubAttr || [], // 子分类数据
}))(NewAdd);
