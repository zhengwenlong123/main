import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Form, Input, Row, Col, Select, Cascader, DatePicker, Descriptions } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import { getTreeParent } from '@/utils/utils';
import styles from './index.less';

moment.locale('zh-cn');

/**
 * 产品申请单新增：产品基本信息部分组件
 */
function BasicInfo(props, ref) {
  const {
    form,
    dispatch,
    goodsCatagoryListAll,
    goodsClassificationListAll,
    goodsbrandListAll = [],
    parmPublicParameterProductType,
    goodsRfSpuInfo,
    operationType, // 操作类型
    goodsApplyBillDetailInfo,
    disabled, // 是否可修改
  } = props;

  useImperativeHandle(ref, () => ({
    form,
  }));

  const [catagoryList, setCatagoryList] = useState(goodsCatagoryListAll);
  const [classificationList, setClassificationList] = useState(goodsClassificationListAll);
  const [goodsbrandList, setGoodsbrandList] = useState([...goodsbrandListAll]);
  const [goodsTypeList, setGoodsTypeList] = useState(parmPublicParameterProductType);

  // 监听并更新产品类目变化
  useEffect(() => {
    setCatagoryList([...goodsCatagoryListAll]);
  }, [goodsCatagoryListAll]);
  // 监听并更新产品大类变化
  useEffect(() => {
    setClassificationList([...goodsClassificationListAll]);
  }, [goodsClassificationListAll]);
  // 监听并更新产品品牌变化
  useEffect(() => {
    setGoodsbrandList([...goodsbrandListAll]);
  }, [goodsbrandListAll]);
  // 监听并更新产品类型变化
  useEffect(() => {
    setGoodsTypeList([...parmPublicParameterProductType]);
  }, [parmPublicParameterProductType]);

  const formItemLayout =
    operationType !== 'audit'
      ? {
          labelCol: { xs: { span: 24 }, sm: { span: 4 }, xl: { span: 2 } },
          wrapperCol: { xs: { span: 24 }, sm: { span: 20 }, xl: { span: 21 } },
        }
      : {
          labelCol: { xs: { span: 24 }, sm: { span: 6 }, xl: { span: 4 }, xxl: { span: 2 } },
          wrapperCol: { xs: { span: 24 }, sm: { span: 18 }, xl: { span: 19 }, xxl: { span: 21 } },
        };
  const colLayout =
    operationType !== 'audit'
      ? {
          xs: 24,
          sm: 12,
          xl: 8,
        }
      : {
          xs: 24,
          sm: 12,
          xll: 8,
        };
  const formItemLayoutSome =
    operationType !== 'audit'
      ? {
          labelCol: { xs: { span: 24 }, sm: { span: 8 }, xl: { span: 6 } },
          wrapperCol: { xs: { span: 24 }, sm: { span: 16 }, xl: { span: 15 } },
        }
      : {
          labelCol: { xs: { span: 24 }, sm: { span: 6 }, xl: { span: 8 }, xxl: { span: 6 } },
          wrapperCol: { xs: { span: 24 }, sm: { span: 14 }, xl: { span: 14 }, xxl: { span: 15 } },
        };

  const { getFieldDecorator } = form;

  const brandLabel = brandCode => {
    if (!brandCode) return '';
    const findVal = goodsbrandList.find(v => v.brandCode === brandCode);
    return findVal ? findVal.brandName : '';
  };
  const mainClassLabel = mainClassCode => {
    if (!mainClassCode) return '';
    const findVal = classificationList.find(v => v.mainClassCode === mainClassCode);
    return findVal ? findVal.mainClassName : '';
  };
  const typeLabel = spuTypeCode => {
    if (!spuTypeCode) return '';
    const findVal = goodsTypeList.find(v => v.parmCode === spuTypeCode);
    return findVal ? findVal.parmValue : '';
  };
  const catagoryLabel = (catagoryCode = []) => {
    if (catagoryCode.length === 0) return '';
    const code = catagoryCode[catagoryCode.length - 1]; // 取最后一位
    const catagoryCodeArr =
      getTreeParent(goodsCatagoryListAll, code, 'catagoryCode', 'parentCode') || [];
    return catagoryCodeArr.map(v => v.catagoryName).join('/');
  };
  // 产品名称唯一性校验
  const validatorSkuName = async (rules, value, callback) => {
    if (value && value.length > 100) {
      const wordVal = formatMessage({ id: 'form.common.word.length' }).replace('00', '100');
      callback(wordVal);
    } else if (value) {
      await dispatch({
        type: 'goodsapplybillmanage/testUniqueBySpuName',
        payload: { spuName: value },
        callback: res => {
          const { code, data = {} } = res;
          const { goodsRfSpuInfo: info } = goodsApplyBillDetailInfo;
          const { spuCode = '' } = info || {};
          if (code === 0 && data && data.spuCode !== spuCode) {
            callback('产品名称已经存在');
          } else {
            callback();
            if (goodsRfSpuInfo && !goodsRfSpuInfo.spuShortName) {
              // 当手机简称没有值自动默认产品名称当值
              dispatch({
                type: 'goodsapplybillmanage/changeGoodsRfSpuInfo',
                payload: {
                  ...goodsRfSpuInfo,
                  spuShortName: value,
                },
              });
            }
          }
        },
      });
    } else {
      callback();
    }
  };

  const spanRender = () => (
    <Descriptions column={3}>
      <Descriptions.Item
        span={3}
        label={formatMessage({ id: 'form.goodsApplyBillManage.product.name' })}
      >
        {goodsRfSpuInfo.spuName}
      </Descriptions.Item>
      <Descriptions.Item
        label={formatMessage({ id: 'form.goodsApplyBillManage.product.shortName' })}
      >
        {goodsRfSpuInfo.spuShortName}
      </Descriptions.Item>
      <Descriptions.Item label={formatMessage({ id: 'form.goodsApplyBillManage.product.brand' })}>
        {brandLabel(goodsRfSpuInfo.brandCode)}
      </Descriptions.Item>
      <Descriptions.Item label={formatMessage({ id: 'form.goodsApplyBillManage.product.type' })}>
        {typeLabel(goodsRfSpuInfo.spuTypeCode)}
      </Descriptions.Item>
      <Descriptions.Item
        label={formatMessage({ id: 'form.goodsApplyBillManage.product.marketTime' })}
      >
        {goodsRfSpuInfo.spuTimeToMarket
          ? moment(goodsRfSpuInfo.spuTimeToMarket).format('YYYY-MM-DD')
          : ''}
      </Descriptions.Item>
      <Descriptions.Item label={formatMessage({ id: 'form.goodsApplyBillManage.product.tegory' })}>
        {catagoryLabel(goodsRfSpuInfo.catagoryCode)}
      </Descriptions.Item>
      <Descriptions.Item
        label={formatMessage({ id: 'form.goodsApplyBillManage.product.mainClassCode' })}
      >
        {mainClassLabel(goodsRfSpuInfo.mainClassCode)}
      </Descriptions.Item>
      <Descriptions.Item
        label={formatMessage({ id: 'form.goodsApplyBillManage.product.synopsis' })}
      >
        {goodsRfSpuInfo.spuDesc}
      </Descriptions.Item>
    </Descriptions>
  );

  const formRender = () => (
    <Form layout="horizontal" labelAlign="right" {...formItemLayout}>
      <Row>
        <Form.Item label={formatMessage({ id: 'form.goodsApplyBillManage.product.tegory' })}>
          {getFieldDecorator('catagoryCode', {
            rules: [
              {
                required: true,
                message: formatMessage({
                  id: 'form.goodsApplyBillManage.product.tegory.placeholder',
                }),
              },
            ],
          })(
            <Cascader
              fieldNames={{ label: 'catagoryName', value: 'catagoryCode', children: 'childrens' }}
              options={catagoryList}
              popupClassName={styles.catagoryCascader}
              placeholder=""
              showSearch={(inputValue, path) =>
                path.some(
                  option => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1
                )
              }
            />
          )}
        </Form.Item>
      </Row>
      <Row>
        <Form.Item label={formatMessage({ id: 'form.goodsApplyBillManage.product.name' })}>
          {getFieldDecorator('spuName', {
            validateTrigger: ['onBlur'],
            rules: [
              {
                required: true,
                message: formatMessage({
                  id: 'form.goodsApplyBillManage.product.name.placeholder',
                }),
              },
              {
                validator: validatorSkuName,
              },
            ],
          })(<Input />)}
        </Form.Item>
      </Row>
      <Row>
        <Col {...colLayout}>
          <Form.Item
            {...formItemLayoutSome}
            label={formatMessage({ id: 'form.goodsApplyBillManage.product.mainClassCode' })}
          >
            {getFieldDecorator('mainClassCode', {
              rules: [
                {
                  required: true,
                  message: formatMessage({
                    id: 'form.goodsApplyBillManage.product.mainClassCode.choice',
                  }),
                },
              ],
            })(
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.indexOf(input) >= 0}
              >
                {classificationList.map(v => (
                  <Select.Option value={v.mainClassCode} key={v.id}>
                    {`${v.mainClassCode} ${v.mainClassName}`}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item
            {...formItemLayoutSome}
            label={formatMessage({ id: 'form.goodsApplyBillManage.product.brand' })}
          >
            {getFieldDecorator('brandCode', {
              rules: [
                {
                  required: true,
                  message: formatMessage({
                    id: 'form.goodsApplyBillManage.product.brand.choice',
                  }),
                },
              ],
            })(
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.indexOf(input) >= 0}
              >
                {goodsbrandList.map(v => (
                  <Select.Option value={v.brandCode} key={v.id}>
                    {`${v.brandCode} ${v.brandName}`}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item
            {...formItemLayoutSome}
            label={formatMessage({ id: 'form.goodsApplyBillManage.product.shortName' })}
          >
            {getFieldDecorator('spuShortName', {
              rules: [
                {
                  required: true,
                  message: formatMessage({
                    id: 'form.goodsApplyBillManage.product.shortName.placeholder',
                  }),
                },
              ],
            })(<Input />)}
          </Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item
            {...formItemLayoutSome}
            label={formatMessage({ id: 'form.goodsApplyBillManage.product.marketTime' })}
          >
            {getFieldDecorator('spuTimeToMarket')(<DatePicker style={{ width: '100%' }} />)}
          </Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item
            {...formItemLayoutSome}
            label={formatMessage({ id: 'form.goodsApplyBillManage.product.type' })}
          >
            {getFieldDecorator('spuTypeCode', {
              rules: [
                {
                  required: true,
                  message: formatMessage({
                    id: 'form.goodsApplyBillManage.product.type.placeholder',
                  }),
                },
              ],
            })(
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(input, option) => option.props.children.indexOf(input) >= 0}
              >
                {goodsTypeList.map(v => (
                  <Select.Option value={v.parmCode} key={v.id}>
                    {`${v.parmCode} ${v.parmValue}`}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Form.Item label={formatMessage({ id: 'form.goodsApplyBillManage.product.synopsis' })}>
          {getFieldDecorator('spuDesc', {
            validateTrigger: ['onChange', 'onBlur'],
            rules: [
              {
                max: 250,
                message: formatMessage({ id: 'form.goodsApplyBillManage.value.word' }),
              },
            ],
          })(<Input.TextArea />)}
        </Form.Item>
      </Row>
    </Form>
  );

  const formItemRender = () => {
    if (operationType === 'see') {
      // 查看详情（不可修改）
      return spanRender();
    }
    /* rfStage 1：生效数据 2：申请单数据（可操作修改）  */
    return goodsRfSpuInfo.rfStage === 1 || disabled ? spanRender() : formRender();
  };

  return <div className={styles.goodsapplybilltabBasicInfo}>{formItemRender()}</div>;
}

export default Form.create({
  onValuesChange({ dispatch, goodsRfSpuInfo }, changedValues) {
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsRfSpuInfo',
      payload: {
        ...goodsRfSpuInfo,
        ...changedValues,
      },
    });
  },
  mapPropsToFields(props) {
    const { goodsRfSpuInfo } = props;
    return {
      catagoryCode: Form.createFormField({
        value: goodsRfSpuInfo ? goodsRfSpuInfo.catagoryCode : [],
      }),
      spuName: Form.createFormField({ value: goodsRfSpuInfo ? goodsRfSpuInfo.spuName : '' }),
      mainClassCode: Form.createFormField({
        value: goodsRfSpuInfo ? goodsRfSpuInfo.mainClassCode : '',
      }),
      brandCode: Form.createFormField({ value: goodsRfSpuInfo ? goodsRfSpuInfo.brandCode : '' }),
      spuShortName: Form.createFormField({
        value: goodsRfSpuInfo ? goodsRfSpuInfo.spuShortName : '',
      }),
      spuTimeToMarket: Form.createFormField({
        value: goodsRfSpuInfo ? goodsRfSpuInfo.spuTimeToMarket : '',
      }),
      spuTypeCode: Form.createFormField({
        value: goodsRfSpuInfo ? goodsRfSpuInfo.spuTypeCode : '',
      }),
      spuDesc: Form.createFormField({ value: goodsRfSpuInfo ? goodsRfSpuInfo.spuDesc : '' }),
    };
  },
})(React.forwardRef(BasicInfo));
