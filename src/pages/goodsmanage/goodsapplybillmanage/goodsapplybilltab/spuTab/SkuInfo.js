import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Form, Select, Modal, message } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import uuid from 'uuid';
import { compare } from '@/utils/utils';
import EditableTagGroupModal from '@/components/EditableTagGroup/EditableTagGroupModal';
import DynamicFormSet from '../../components/DynamicFormSet';
import SpanFormShow from '../../components/SpanFormShow';
// import styles from './index.less';

moment.locale('zh-cn');
const registerField = flag => {
  if (flag) {
    return {
      attr: { key: 'color', label: '颜色' },
      tags: [],
    };
  }
  return {
    attr: { key: '', label: '' },
    tags: [],
  };
};
/**
 * 产品申请单新增：产品sku信息部分组件
 */
function SkuInfo(props, ref) {
  const {
    form,
    goodsAttrNameAble,
    goodsApplyBillDetailInfo,
    dispatch,
    disabled,
    itemDisabled,
    operationType, // 操作类型
    goodsSkulist = [],
    goodsRfInfo = {}, // 申请单信息
    goodsAttrNameAll,
  } = props;
  const seeFlag = operationType === 'see'; // 查看详情
  useImperativeHandle(ref, () => ({
    form,
  }));
  const [attrListAll, setAttrListAll] = useState(goodsAttrNameAble);
  const [initList, setInitList] = useState([{ id: uuid.v4(), ...registerField() }]);

  // 监听并更新属性集变化
  useEffect(() => {
    setAttrListAll([...goodsAttrNameAble]);
  }, [goodsAttrNameAble]);

  useEffect(() => {
    const usedSkuAttr = [];
    if (operationType === 'add' && goodsRfInfo && !goodsRfInfo.spuCode) {
      // 新增产品类型
      // 默认sku属性是颜色color，过滤可用sku属性保存起来
      usedSkuAttr.push('color');
    } else {
      // 其它情况初加载使用详情中信息处理
      const { goodsRfSpuInfo } = goodsApplyBillDetailInfo || {};
      const { goodsRfSpuAttrSkuList, goodsRfSpuAttrNormalList } = goodsRfSpuInfo || {};
      if (goodsRfSpuAttrNormalList && goodsRfSpuAttrNormalList.attrCode) {
        // 普通属性已用的属性
        usedSkuAttr.push(goodsRfSpuAttrNormalList.attrCode);
      }
      if (Array.isArray(goodsRfSpuAttrSkuList) && goodsRfSpuAttrSkuList.length > 0) {
        goodsRfSpuAttrSkuList.forEach(v => {
          usedSkuAttr.push(v.skuAttrCode);
        });
      } else {
        // 当sku属性值为空时，sku信息中的属性默认值是颜色color
        usedSkuAttr.push('color');
      }
    }
    dispatch({
      // 默认sku属性是颜色color，过滤可用sku属性保存起来
      type: 'goodsapplybillmanage/changeGoodsAttrNameAble',
      payload: [...usedSkuAttr],
    });
  }, [operationType, goodsAttrNameAll, goodsRfInfo, goodsApplyBillDetailInfo]);

  useEffect(() => {
    const { goodsRfSpuInfo } = goodsApplyBillDetailInfo;
    const { goodsRfSpuAttrSkuList = [], goodsRfSpuSubAttrInfoObj = {} } = goodsRfSpuInfo || {};
    let arr = goodsRfSpuAttrSkuList.map(item => {
      const attr = {
        key: item.skuAttrCode,
        label: item.skuAttrName,
        rfStage: item.rfStage,
        rfId: item.rfId,
        spuCode: item.spuCode,
        spuId: item.spuId,
      };
      const { goodsRfSpuAttrSkuItemList = [] } = item;
      const tags = goodsRfSpuAttrSkuItemList.map(v => ({
        ...v,
        name: v.skuAttrItemValue,
        code: v.skuAttrItemCode,
      }));
      return { attr, tags: tags.sort(compare('seq')), id: uuid.v4() };
    });
    if (arr && arr.length === 0) {
      arr = [{ id: uuid.v4(), ...registerField(true) }];
    }
    setInitList(arr);
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsSkuAttrUsed',
      payload: [...arr],
    });
    // 商品从属属性设定: 当从属属性为空的时候，取用sku第一个属性作为从属属性
    const { key = arr[0].attr.key, label = arr[0].attr.label } = goodsRfSpuSubAttrInfoObj;
    const obj = { key, label };
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsRfSpuSubAttrInfo',
      payload: { ...obj },
    });
  }, [goodsApplyBillDetailInfo]);

  const { getFieldDecorator } = form;
  const formItemLayout =
    operationType !== 'audit'
      ? {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
            xl: { span: 2 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 20 },
            xl: { span: 19 },
          },
        }
      : {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 6 },
            xl: { span: 4 },
            xll: { span: 2 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 },
            xl: { span: 17 },
            xll: { span: 2 },
          },
        };
  const tagLayout =
    operationType !== 'audit'
      ? {
          wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 20, offset: 4 },
            xl: { span: 19, offset: 2 },
          },
        }
      : {
          wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 18, offset: 6 },
            xl: { span: 17, offset: 4 },
            xll: { span: 19, offset: 2 },
          },
        };
  const addBtnLayout =
    operationType === 'audit'
      ? {
          wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 18, offset: 6 },
            xl: { span: 19, offset: 4 },
            xll: { span: 19, offset: 2 },
          },
        }
      : undefined;
  const validatorAttr = (rule, value, callback) => {
    if (value && !value.key) {
      callback(formatMessage({ id: 'form.common.apply.attr.placeholder' }));
    } else {
      callback();
    }
  };

  // 相关确认款提示文字
  const modifyMessage = formatMessage({ id: 'button.common.edit' });
  const deleteMessage = formatMessage({ id: 'button.common.delete' });
  // const addMessage = formatMessage({id: 'button.common.addition'});
  const skuAttrMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.skuAtrr' });
  const skuAtrrItemMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.skuAtrrItem' });
  const quetoMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.skuName.queto' });
  const quetoAttrMessage = formatMessage({
    id: 'form.goodsApplyBillManage.message.skuAttrName.queto',
  });
  const selectYesMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.select.yes' });
  const selectNoMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.select.no' });
  const deleteProductMessage = formatMessage({
    id: 'form.goodsApplyBillManage.message.delete.product',
  });
  const swapAttrMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.swap.attr' });
  const keepStatusMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.keep.status' });
  // sku属性相关值发生了改变, 清空商品、物料数据
  const skuContentChange = () => {
    dispatch({ type: 'goodsapplybillmanage/changeGoodsSkuAttrUsedChangeClear' });
  };

  const selectChange = (k, index, val) => {
    if (goodsSkulist.length === 0) return;
    const content = (
      <span>
        <p>
          -{selectYesMessage}:<br /> 1&gt;{deleteProductMessage}
          <br />
          2&gt;{swapAttrMessage}
        </p>
        <p>
          -{selectNoMessage}:{keepStatusMessage}
        </p>
      </span>
    );
    if (val && val.key) {
      const skuInfo = form.getFieldValue('skuInfo');
      Modal.confirm({
        title: `${skuAttrMessage}${modifyMessage}: ${quetoMessage}`,
        content,
        width: 420,
        okText: '是',
        okType: 'danger',
        cancelText: '否',
        onOk() {
          // const skuInfo = form.getFieldValue('skuInfo');
          // const keys = form.getFieldValue('keys');
          skuInfo.splice(index, 1, { attr: { ...val }, tags: [] });
          // const findIndex = keys.findIndex(v => v.id === k.id);
          // keys.splice(findIndex, 1, { ...keys[findIndex], attr: { ...val }, tags: [], });
          form.setFieldsValue({ skuInfo });
          // form.setFieldsValue({keys});
          skuContentChange();
        },
        onCancel() {
          // const skuInfo = form.getFieldValue('skuInfo');
          form.setFieldsValue({ skuInfo });
        },
      });
    }
  };
  const beforeRemove = (k, record, index, callback) => {
    // 移除之前的操作回调
    const skuInfoArr = form.getFieldValue('skuInfo');
    if (skuInfoArr[index].attr && !skuInfoArr[index].attr.key) return true; // 移除空数据行不需提示
    if (goodsSkulist.length === 0) return true;
    const content = (
      <span>
        <p>
          -{selectYesMessage}:{deleteProductMessage}
        </p>
        <p>
          -{selectNoMessage}:{keepStatusMessage}
        </p>
      </span>
    );
    Modal.confirm({
      title: `${skuAttrMessage}${deleteMessage}: ${quetoMessage}`,
      content,
      width: 420,
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk() {
        callback(true);
        skuContentChange();
      },
      onCancel() {},
    });
    return false;
  };
  const beforeAdd = () => {
    // 判断是否已存在一行sku，是否选择值，如果没有值不允许再次添加空值框
    const skuInfoArr = form.getFieldValue('skuInfo');
    const findVal = skuInfoArr.find(v => v.attr && !v.attr.key);
    if (findVal) {
      message.error(formatMessage({ id: 'form.goodsApplyBillManage.select.attr' }));
      return false;
    }
    return true;
  };
  const tagsAddAble = () => {
    // sku属性项值添加的确认操作: 暂不加确认框提示
    return true;
  };
  const tagsCloseAble = callback => {
    // sku属性项值删除的确认操作
    if (goodsSkulist.length === 0) return true;
    const content = (
      <span>
        <p>
          -{selectYesMessage}:{deleteProductMessage}
        </p>
        <p>
          -{selectNoMessage}:{keepStatusMessage}
        </p>
      </span>
    );
    Modal.confirm({
      title: `${skuAtrrItemMessage}${deleteMessage}: ${quetoAttrMessage}`,
      content,
      width: 420,
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk() {
        callback(true);
        skuContentChange();
      },
      onCancel() {},
    });
    return false;
  };
  const formItem = (k, index, removeRender) => {
    return (
      <div key={k.id}>
        {seeFlag || k.attr.rfStage === 1 ? (
          <div>
            <Form.Item
              {...formItemLayout}
              label={`${formatMessage({ id: 'form.common.apply.attr' })}${index + 1}`}
            >
              {getFieldDecorator(`skuInfo[${index}].attr`, {
                validateTrigger: ['onChange', 'onBlur'],
                initialValue: { key: k.attr.key, label: k.attr.label },
                rules: [
                  {
                    required: false,
                    type: 'object',
                    message: formatMessage({ id: 'form.common.apply.attr.placeholder' }),
                  },
                ],
              })(<SpanFormShow.SpanSelectShow />)}
            </Form.Item>
            <Form.Item {...tagLayout}>
              {getFieldDecorator(`skuInfo[${index}].tags`, {
                initialValue: [...k.tags],
              })(
                <EditableTagGroupModal
                  addDisabled={itemDisabled || seeFlag}
                  // tagColor: SKU属性项值新增项值颜色为blue
                  tagColor={tag =>
                    (tag.rfStage !== 1 && goodsRfInfo.rfTypeCode !== 'product' && 'blue') || ''
                  }
                  filterClosable={tag => tag.rfStage !== 1 && !seeFlag}
                  modalTitle={formatMessage({ id: 'form.goodsApplyBillManage.title.skuAttr' })}
                />
              )}
            </Form.Item>
          </div>
        ) : (
          <div>
            <Form.Item
              {...formItemLayout}
              label={`${formatMessage({ id: 'form.common.apply.attr' })}${index + 1}`}
            >
              {getFieldDecorator(`skuInfo[${index}].attr`, {
                validateTrigger: ['onChange', 'onBlur'],
                initialValue: { key: k.attr.key, label: k.attr.label },
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.apply.attr.placeholder' }),
                  },
                  { validator: validatorAttr },
                ],
              })(
                <Select
                  style={{ width: '60%', marginRight: 8 }}
                  labelInValue
                  onChange={val => selectChange(k, index, val)}
                >
                  {attrListAll.map(v => (
                    <Select.Option value={v.attrCode} key={v.id}>
                      {v.attrName}
                    </Select.Option>
                  ))}
                </Select>
              )}
              {removeRender(k.id)}
            </Form.Item>
            {form.getFieldValue('skuInfo')[index].attr.key && (
              <Form.Item {...tagLayout}>
                {getFieldDecorator(`skuInfo[${index}].tags`, {
                  initialValue: [...k.tags],
                })(
                  <EditableTagGroupModal
                    filterClosable={tag => tag.rfStage !== 1}
                    addDisabled={itemDisabled}
                    // onChange={(val) => tagChange(k, index, val)}
                    onAddable={tagsAddAble}
                    onCloseAble={tagsCloseAble}
                    modalTitle={formatMessage({ id: 'form.goodsApplyBillManage.title.skuAttr' })}
                  />
                )}
              </Form.Item>
            )}
          </div>
        )}
      </div>
    );
  };

  return (
    <DynamicFormSet
      fieldName="skuInfo"
      initialValue={initList}
      registerField={registerField()}
      formItem={formItem}
      addDisabled={disabled || seeFlag}
      formItemLayoutAddBtn={addBtnLayout}
      beforeRemove={beforeRemove}
      beforeAdd={beforeAdd}
      {...props}
    />
  );
}
export default Form.create({
  onValuesChange: (props, changedValues, allValues) => {
    const { keys, skuInfo } = allValues;
    const { dispatch, goodsSkuAttrUsed, goodsRfSpuAttrNormalList, onSkuInfoChange } = props;
    let arr = [...goodsSkuAttrUsed];
    if (changedValues.skuInfo && skuInfo.length - keys.length === 0) {
      // 说明当前是item节点数据改变
      changedValues.skuInfo.forEach((item, index) => {
        if ((item.attr && item.attr.key) || item.tags) {
          arr[index] = { ...registerField(), ...goodsSkuAttrUsed[index], ...item };
        }
      });
    }
    if (changedValues.skuInfo && skuInfo.length - keys.length > 0) {
      // 说明当前是row行减少（即删除）
      const current = changedValues.skuInfo.filter(v => v.attr && v.attr.key);
      arr = [...goodsSkuAttrUsed].filter(x => [...current].some(y => y.attr.key === x.attr.key));
    }
    dispatch({
      // 属性发生改变后，过滤可用sku属性保存起来
      type: 'goodsapplybillmanage/changeGoodsAttrNameAble',
      payload: [...arr.map(v => v.attr.key), ...goodsRfSpuAttrNormalList.map(v => v.attr.key)],
    });
    dispatch({
      // 保存已配置好的sku属性
      type: 'goodsapplybillmanage/changeGoodsSkuAttrUsed',
      payload: [...arr],
    });
    onSkuInfoChange(arr);
  },
})(React.forwardRef(SkuInfo));
