/* eslint-disable no-case-declarations */
import React from 'react';
import { connect } from 'dva';
import { Card } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import BasicInfo from './BasicInfo';
import SkuInfo from './SkuInfo';
import CommonAttr from './CommonAttr';
import ProductModel from './ProductModel';
import Describe from './Describe';
import DependenceAttr from './DependenceAttr';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增、维护的界面部分组件
 */
class GoodsApplyBillSpuTab extends React.PureComponent {
  state = {};

  /**
   * 产品tab下所有form表单对象
   */
  formObj = {
    basicInfo: null,
    skuInfo: null,
    commonAttr: null,
    productModel: null,
    describe: null,
  };

  /**
   * 产品所有表单属性列表
   */
  cardList = [
    {
      key: 'basicInfo',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.basicInfo' }),
      cardRender: ({ disabled = false }) => (
        <BasicInfo
          disabled={disabled}
          wrappedComponentRef={form => this.getForm('basicInfo', form)}
          {...this.props}
        />
      ),
    },
    {
      key: 'skuInfo',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.skuInfo' }),
      cardRender: ({ disabled = false, itemDisabled = false }) => (
        <SkuInfo
          wrappedComponentRef={form => this.getForm('skuInfo', form)}
          onSkuAttrChange={this.skuAttrChange}
          disabled={disabled}
          itemDisabled={itemDisabled}
          onSkuInfoChange={arr => this.onSkuInfoChange(arr)}
          {...this.props}
        />
      ),
    },
    {
      key: 'dependenceAttr',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.dependenceAttr' }),
      cardRender: ({ disabled = false, itemDisabled = false }) => (
        <DependenceAttr
          wrappedComponentRef={form => this.getForm('dependenceAttr', form)}
          disabled={disabled}
          itemDisabled={itemDisabled}
          onDependenceAttrChange={obj => this.onDependenceAttrChange(obj)}
          {...this.props}
        />
      ),
    },
    {
      key: 'commonAttr',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.commonAttr' }),
      cardRender: ({ disabled = false }) => (
        <CommonAttr
          {...this.props}
          disabled={disabled}
          onSkuAttrChange={this.skuAttrChange}
          wrappedComponentRef={form => this.getForm('commonAttr', form)}
        />
      ),
    },
    {
      key: 'productModel',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.productModel' }),
      cardRender: ({ disabled = false }) => (
        <ProductModel
          {...this.props}
          disabled={disabled}
          wrappedComponentRef={form => this.getForm('productModel', form)}
        />
      ),
    },
    {
      key: 'describe',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.describe' }),
      cardRender: ({ disabled = false }) => (
        <Describe
          disabled={disabled}
          {...this.props}
          wrappedComponentRef={form => this.getForm('describe', form)}
        />
      ),
    },
  ];

  /**
   * 产品tab卡片标题提醒以及是否可修改列表
   * message：表示当前卡片内容可操作的提示文字
   * disabled：是否可进行新增操作， true： 无法操作，false： 可操作
   * 另外：某项可否进行修改，看字段rfStage是否是1,1代表已生效数据，无法操作
   */
  cardTitleMessage = {
    basicInfo: {
      // 基本信息
      product: { message: formatMessage({ id: 'form.common.notNull' }), disabled: false }, // 必录
      goods: { message: formatMessage({ id: 'form.common.notAllow' }), disabled: true }, // 不允许修改
      materiel: { message: formatMessage({ id: 'form.common.notAllow' }), disabled: true }, // 不允许修改
    },
    skuInfo: {
      // sku信息
      product: { message: formatMessage({ id: 'form.common.notNull' }), disabled: false }, // 必录
      goods: { message: formatMessage({ id: 'form.common.allow.addAttrItem' }), disabled: true }, // 允许新增SKU属性项值
      materiel: {
        message: formatMessage({ id: 'form.common.notAllow' }),
        disabled: true,
        itemDisabled: true,
      }, // 不允许修改
    },
    dependenceAttr: {
      // sku信息
      product: { message: formatMessage({ id: 'form.common.notNull' }), disabled: false }, // 必录
      goods: { message: formatMessage({ id: 'form.common.notAllow' }), disabled: true }, // 不允许修改
      materiel: {
        message: formatMessage({ id: 'form.common.notAllow' }),
        disabled: true,
      }, // 不允许修改
    },
    commonAttr: {
      // 普通属性
      product: { message: formatMessage({ id: 'form.common.option' }), disabled: false }, // 可选项
      goods: { message: formatMessage({ id: 'form.common.notAllow' }), disabled: true }, // 不允许修改
      materiel: { message: formatMessage({ id: 'form.common.notAllow' }), disabled: true }, // 不允许修改
    },
    productModel: {
      // 产品型号
      product: { message: formatMessage({ id: 'form.common.option' }), disabled: false }, // 可选项
      goods: {
        message: formatMessage({ id: 'form.common.allow.addProductModel' }),
        disabled: false,
      }, // 允许新增产品型号
      materiel: {
        message: formatMessage({ id: 'form.common.allow.addProductModel' }),
        disabled: false,
      }, // 允许新增产品型号
    },
    describe: {
      // 产品描述
      product: { message: formatMessage({ id: 'form.common.option' }), disabled: false }, // 可选项
      goods: { message: formatMessage({ id: 'form.common.notAllow' }), disabled: true }, // 不允许修改
      materiel: { message: formatMessage({ id: 'form.common.notAllow' }), disabled: true }, // 不允许修改
    },
  };

  componentDidMount() {
    const { onRef } = this.props;
    onRef(this);
  }

  getForm = (key, form) => {
    this.formObj[key] = form;
  };

  // 属性发生改变后，存放到skuAttrIdUsed存放起来
  skuAttrChange = () => {
    const { dispatch } = this.props;
    const skuAttrIdUsed = [];
    this.formObj.skuInfo.form.getFieldValue('skuInfo').forEach(v => {
      if (v.attr && v.attr.key) {
        skuAttrIdUsed.push(v.attr.key);
      }
    });
    this.formObj.commonAttr.form.getFieldValue('commonAttr').forEach(v => {
      if (v.attr && v.attr.key) {
        skuAttrIdUsed.push(v.attr.key);
      }
    });
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsAttrNameAble',
      payload: skuAttrIdUsed,
    });
  };

  // 产品sku信息发生改变的回调(只回调有效值)
  onSkuInfoChange = arr => {
    const { dispatch, goodsRfSpuSubAttrInfo } = this.props;
    const findSku = arr.find(v => v.attr.key === goodsRfSpuSubAttrInfo.key);
    const subAttr = findSku
      ? { ...goodsRfSpuSubAttrInfo }
      : { key: arr[0].attr.key, label: arr[0].attr.label }; // 从属属性对象
    if (!findSku) {
      // 当发生sku属性改变，导致从属属性下拉值不存在，从属属性key将从sku已有数据中取第一个属性
      dispatch({
        type: 'goodsapplybillmanage/changeGoodsRfSpuSubAttrInfo',
        payload: { ...subAttr },
      });
    }
    this.groupListComputed(arr, subAttr);
  };

  // 商品从属属性发生改变的回调
  onDependenceAttrChange = obj => {
    const { goodsSkuAttrUsed } = this.props;
    this.groupListComputed(goodsSkuAttrUsed, { ...obj });
  };

  /**
   *sku和从属属性发生改变，商品分组的排列组合数
   * @param {array} arr  sku属性数组
   * @param {object} subAttr 从属属性对象值
   */
  groupListComputed = (arr, subAttr) => {
    const {
      dispatch,
      goodsRfSpuInfo: { spuName },
      goodsRfGroupListValid,
    } = this.props;
    let groupList = [];
    const combinationSkuAttr = arr.filter(v => v.attr.key !== subAttr.key);
    const subAttrObj = {
      subAttrCode: subAttr.key,
      subAttrName: subAttr.label,
    };
    const combinationSkuAttrKeys = combinationSkuAttr.map(v => v.attr.key); // 将非从属属性的sku属性编码集合
    if (combinationSkuAttr.length === 0) {
      // 当产品只有一个SKU属性名时，商品分组名称即:"商品分组_"+产品名称
      groupList.push({
        groupName: `商品分组_${spuName}`,
        groupCode: subAttr.key,
        ...subAttrObj,
      });
    } else {
      const result = combinationSkuAttr.reduce((ary, { attr, tags }) => {
        // 属性项值排列组合
        return ary.length
          ? [].concat(
              ...tags.map(v =>
                ary.map(res => ({
                  ...res,
                  [attr.key]: { ...v },
                  groupName: `${res.groupName}_${v.name}`,
                  groupCode: `${res.groupCode}_${v.code}`,
                }))
              )
            )
          : tags.map(v => ({
              [attr.key]: { ...v },
              ...subAttrObj,
              groupName: v.name,
              groupCode: v.code,
            }));
      }, []);
      groupList = result.filter(v => {
        // 过滤掉已生效的商品分组
        const findIndex = goodsRfGroupListValid.findIndex(item => {
          let flag = true;
          combinationSkuAttrKeys.forEach(key => {
            const { code: vCode } = v[key] || {};
            const { code: itemCode } = item[key] || {};
            if (vCode !== itemCode) {
              flag = false;
            }
          });
          return flag;
        });
        return findIndex === -1;
      });
    }
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsRfGroupList',
      payload: [...groupList],
    });
  };

  /** 产品信息提交触发 */
  commit = () => {
    const promiseALl = [];
    this.cardList.forEach(item => {
      promiseALl.push(
        new Promise((resolve, reject) => {
          this.formObj[item.key].form.validateFields(err => {
            if (err) {
              reject();
            } else {
              resolve();
            }
          });
        })
      );
    });
    return Promise.all([...promiseALl]);
  };

  render() {
    const { goodsRfInfo } = this.props;
    const titleFnc = item => {
      const val = this.cardTitleMessage[item.key][goodsRfInfo.rfTypeCode].message;
      return `${item.title}(${val})`;
    };
    return (
      <div className={styles.productCont}>
        {this.cardList.map(item => (
          <Card
            title={titleFnc(item)}
            key={item.key}
            bordered={false}
            headStyle={{ paddingLeft: 0, minHeight: 30 }}
          >
            {item.cardRender(this.cardTitleMessage[item.key][goodsRfInfo.rfTypeCode])}
          </Card>
        ))}
      </div>
    );
  }
}

export default connect(({ goodsapplybillmanage, parmpublicparametermanage }) => ({
  goodsCatagoryListAll: goodsapplybillmanage.goodsCatagoryListAll || [], // 产品类目
  goodsClassificationListAll: goodsapplybillmanage.goodsClassificationListAll || [], // 产品大类
  goodsbrandListAll: goodsapplybillmanage.goodsbrandListAll || [], // 产品品牌
  parmPublicParameterProductType: parmpublicparametermanage.parmPublicParameterProductType || [], // 产品类别
  goodsAttrNameAll: goodsapplybillmanage.goodsAttrNameAll || [], // 属性名称(所有)
  goodsAttrNameAble: goodsapplybillmanage.goodsAttrNameAble || [], // 属性名称(可用)
  parmPublicParameterProductDesc: parmpublicparametermanage.parmPublicParameterProductDesc || [], // 产品描述类型
  goodsApplyBillDetailInfo: goodsapplybillmanage.goodsApplyBillDetailInfo || {},
  goodsRfSpuInfo: goodsapplybillmanage.goodsRfSpuInfo || {}, // 产品基本信息
  goodsSkuAttrUsed: goodsapplybillmanage.goodsSkuAttrUsed || [], // sku属性(产品tab中sku信息已设置好的属性内容)
  goodsRfSpuAttrNormalList: goodsapplybillmanage.goodsRfSpuAttrNormalList || [], // 产品普通属性
  goodsSpuModellist: goodsapplybillmanage.goodsSpuModellist || [], // 产品型号
  goodsRfSpuDescList: goodsapplybillmanage.goodsRfSpuDescList || [], // 产品描述
  goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 产品申请单信息
  goodsRfSpuSubAttrInfo: goodsapplybillmanage.goodsRfSpuSubAttrInfo || {}, // 商品分组从属属性设定值
  goodsRfGroupListValid: goodsapplybillmanage.goodsRfGroupListValid || [], // 商品分组数据（已生效
  goodsSkulist: goodsapplybillmanage.goodsSkulist || [], // 商品列表数据(新)
  goodsMaterielList: goodsapplybillmanage.goodsMaterielList || [], // 物料总数据
}))(GoodsApplyBillSpuTab);
