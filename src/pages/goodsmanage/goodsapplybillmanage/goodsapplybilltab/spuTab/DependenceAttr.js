import React, { useImperativeHandle, useState, useEffect } from 'react';
import { Form, Select } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import SpanFormShow from '../../components/SpanFormShow';
// import styles from './index.less';

moment.locale('zh-cn');
// const registerField = () => ({
//   attr: { key: '', label: '' },
//   attrValue: '',
// });
/**
 * 产品申请单新增：产品普通属性部分组件
 */

function DependenceAttr(props, ref) {
  const {
    form,
    dispatch,
    disabled,
    goodsSkuAttrUsed,
    goodsApplyBillDetailInfo,
    operationType,
  } = props;
  const { getFieldDecorator } = form;
  const seeFlag = operationType === 'see'; // 查看详情
  useImperativeHandle(ref, () => ({
    form,
  }));

  const [attrListAll, setAttrListAll] = useState(goodsSkuAttrUsed);
  const [isRfStage, setIsRfStage] = useState(false);

  // 监听并更新属性集变化
  useEffect(() => {
    setAttrListAll([...goodsSkuAttrUsed]);
  }, [goodsSkuAttrUsed]);

  useEffect(() => {
    const { goodsRfSpuInfo } = goodsApplyBillDetailInfo;
    const { goodsRfSpuGroupAttrSku = {} } = goodsRfSpuInfo || {};
    setIsRfStage(goodsRfSpuGroupAttrSku && goodsRfSpuGroupAttrSku.rfStage === 1);
    if (goodsRfSpuGroupAttrSku && goodsRfSpuGroupAttrSku.skuAttrCode) {
      dispatch({
        type: 'goodsapplybillmanage/changeGoodsRfSpuSubAttrInfo',
        payload: {
          ...goodsRfSpuGroupAttrSku,
          key: goodsRfSpuGroupAttrSku.skuAttrCode,
          label: goodsRfSpuGroupAttrSku.skuAttrName,
        },
      });
    }
  }, [goodsApplyBillDetailInfo]);

  const formItemLayout =
    operationType !== 'audit'
      ? {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
            xl: { span: 2 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 20 },
            xl: { span: 19 },
          },
        }
      : {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 6 },
            xl: { span: 4 },
            xll: { span: 2 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 },
            xl: { span: 17 },
            xll: { span: 19 },
          },
        };

  const validatorAttr = (rule, value, callback) => {
    if (value && !value.key) {
      callback(formatMessage({ id: 'form.common.apply.attr.placeholder' }));
    } else {
      callback();
    }
  };

  return (
    <Form>
      {seeFlag || disabled || isRfStage ? (
        <Form.Item {...formItemLayout} label={`${formatMessage({ id: 'form.common.apply.attr' })}`}>
          {getFieldDecorator('attr', {
            validateTrigger: ['onChange', 'onBlur'],
            rules: [
              {
                required: true,
                message: formatMessage({ id: 'form.common.apply.attr.placeholder' }),
              },
              { validator: validatorAttr },
            ],
          })(<SpanFormShow.SpanSelectShow />)}
        </Form.Item>
      ) : (
        <Form.Item {...formItemLayout} label={`${formatMessage({ id: 'form.common.apply.attr' })}`}>
          {getFieldDecorator('attr', {
            validateTrigger: ['onChange', 'onBlur'],
            rules: [
              {
                required: true,
                message: formatMessage({ id: 'form.common.apply.attr.placeholder' }),
              },
              { validator: validatorAttr },
            ],
          })(
            <Select style={{ width: '60%', marginRight: 8 }} labelInValue>
              {attrListAll.map(v => (
                <Select.Option value={v.attr.key} key={v.attr.key}>
                  {v.attr.label}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
      )}
    </Form>
  );
}
export default Form.create({
  onValuesChange: (props, changedValues, allValues) => {
    const { dispatch, onDependenceAttrChange } = props;
    const { attr } = allValues;
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsRfSpuSubAttrInfo',
      payload: { ...attr },
    });
    onDependenceAttrChange({ ...attr });
  },
  mapPropsToFields(props) {
    const {
      goodsRfSpuSubAttrInfo: { key = '', label = '' },
    } = props;
    return {
      attr: Form.createFormField({
        value: { key, label },
      }),
    };
  },
})(React.forwardRef(DependenceAttr));
