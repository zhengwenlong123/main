import React, { useImperativeHandle, useState, useEffect } from 'react';
import { Form, Input, Modal } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import uuid from 'uuid';
import DynamicFormSet from '../../components/DynamicFormSet';
import SpanFormShow from '../../components/SpanFormShow';
import styles from './index.less';

moment.locale('zh-cn');
const registerField = () => ({
  modelCode: '',
});
/**
 * 产品申请单新增：产品型号部分组件
 */
function ProductModel(props, ref) {
  const {
    form,
    goodsApplyBillDetailInfo,
    dispatch,
    disabled,
    operationType,
    goodsRfInfo = {},
    goodsSkulist, // 商品数据（新）
    goodsMaterielList, // 物料数据
  } = props;
  // let currentInputOldVal = ''; // 计算当前输入框的原始值
  const seeFlag = operationType === 'see'; // 查看详情
  useImperativeHandle(ref, () => ({
    form,
  }));
  const [initList, setInitList] = useState([{ id: uuid.v4(), ...registerField() }]);
  const [currentInputOldVal, setCurrentInputOldVal] = useState('');

  useEffect(() => {
    const { goodsRfSpuInfo } = goodsApplyBillDetailInfo;
    const { goodsRfSpuModelList = [] } = goodsRfSpuInfo || {};
    const arr = goodsRfSpuModelList.map(item => ({
      ...item,
      id: uuid.v4(),
    }));
    if (arr && arr.length > 0) {
      setInitList(arr);
      dispatch({
        type: 'goodsapplybillmanage/changeGoodsSpuModellist',
        payload: [...arr],
      });
    }
  }, [goodsApplyBillDetailInfo]);

  const { getFieldDecorator } = form;
  const formItemLayout =
    operationType !== 'audit'
      ? {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 4 },
            xl: { span: 2 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 20 },
            xl: { span: 19 },
          },
        }
      : {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 6 },
            xl: { span: 4 },
            xll: { span: 2 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 18 },
            xl: { span: 17 },
            xll: { span: 19 },
          },
        };
  const addBtnLayout =
    operationType === 'audit'
      ? {
          wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 18, offset: 6 },
            xl: { span: 19, offset: 4 },
            xll: { span: 19, offset: 2 },
          },
        }
      : undefined;
  const selectYesMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.select.yes' });
  const selectNoMessage = formatMessage({ id: 'form.goodsApplyBillManage.message.select.no' });
  const deleteMessage = formatMessage({ id: 'form.goodsApplyBillManage.productModel.delete' });
  const keepStatusMessage = formatMessage({
    id: 'form.goodsApplyBillManage.message.keep.status',
  });
  const content = (
    <span>
      <p>
        -{selectYesMessage}:{deleteMessage}
      </p>
      <p>
        -{selectNoMessage}:{keepStatusMessage}
      </p>
    </span>
  );
  const productModelChange = () => {
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsSpuModelChangeClear',
    });
  };
  const isChange = val => {
    const modelArr = val.filter(v => v.modelCode).map(i => i.modelCode);
    const findSkuNo = goodsSkulist.find(v => !modelArr.includes(v.defaultModelCode));
    const findMaterielNo = goodsMaterielList.find(
      v => !modelArr.includes(v.modelCode) && v.rfStage !== 1
    );
    return findSkuNo || findMaterielNo;
  };
  const handleBlur = (e, index) => {
    // 光标失焦
    console.log('productModel', form.getFieldValue('productModel'));
    console.log('handleBlur', e.target.value);
    const productModelFormVal = form.getFieldValue('productModel');
    const current = e.target.value || '';
    if (currentInputOldVal === current) return; // 输入框的值没有发生改变
    if (!isChange(productModelFormVal)) return; // 没有商品和物料的影响

    Modal.confirm({
      title: formatMessage({ id: 'form.goodsApplyBillManage.productModel.title' }),
      content,
      width: 420,
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk() {
        // callback(true);
        productModelChange();
      },
      onCancel() {
        productModelFormVal.splice(index, 1, { modelCode: currentInputOldVal });
        form.setFieldsValue({ productModel: productModelFormVal });
        setCurrentInputOldVal('');
      },
    });
  };
  const handleFocus = e => {
    // 光标聚焦
    console.log('handleFocus', e.target.value);
    setCurrentInputOldVal(e.target.value || '');
  };
  const beforeRemove = (k, record, index, callback) => {
    // 移除之前的操作回调
    const productModelArr = form.getFieldValue('productModel');
    const currentModel = productModelArr[index] && productModelArr[index].modelCode; // 当前要删除的产品型号
    if (!currentModel) return true; // 移除空数据行不需提示
    const findSku = goodsSkulist.find(v => v.defaultModelCode === currentModel);
    const findMateriel = goodsMaterielList.find(v => v.modelCode === currentModel);
    if (!(findSku || findMateriel)) return true; // 没有商品和物料的影响

    Modal.confirm({
      title: formatMessage({ id: 'form.goodsApplyBillManage.productModel.delete.title' }),
      content,
      width: 420,
      okText: '是',
      okType: 'danger',
      cancelText: '否',
      onOk() {
        callback(true);
        productModelChange();
      },
      onCancel() {},
    });
    return false;
  };
  const modelCodeValidator = (rule, value, callback) => {
    if (value) {
      const productModelAll = form.getFieldValue('productModel');
      const filters = productModelAll.filter(v => v.modelCode === value);
      if (filters.length > 1) {
        callback(formatMessage({ id: 'form.goodsApplyBillManage.message.modelCode.repeat' }));
      } else {
        callback();
      }
    }
    callback();
  };
  const formItemInvalid = (k, index, removeRender) => (
    <Form.Item
      {...formItemLayout}
      label={`${formatMessage({ id: 'form.goodsSpuInfoUpkeepBasic.modelCode' })}${index + 1}`}
      key={k.id}
    >
      {getFieldDecorator(`productModel[${index}].modelCode`, {
        validateTrigger: ['onChange'],
        initialValue: k.modelCode || '',
        rules: [
          {
            required: false,
          },
          { validator: modelCodeValidator },
        ],
      })(
        <Input
          style={{ width: '60%', marginRight: 8 }}
          onFocus={handleFocus}
          onBlur={e => handleBlur(e, index)}
        />
      )}
      {removeRender(k.id)}
    </Form.Item>
  );
  const formItemValid = (k, index) => (
    <Form.Item
      {...formItemLayout}
      label={`${formatMessage({ id: 'form.goodsSpuInfoUpkeepBasic.modelCode' })}${index + 1}`}
      key={k.id}
    >
      {getFieldDecorator(`productModel[${index}].modelCode`, {
        validateTrigger: ['onChange'],
        initialValue: k.modelCode || '',
        rules: [
          {
            required: false,
          },
          { validator: modelCodeValidator },
        ],
      })(
        <SpanFormShow.SpanInputShow
          className={k.rfStage !== 1 && goodsRfInfo.rfTypeCode !== 'product' && styles.modalNewAdd}
        />
      )}
    </Form.Item>
  );
  const formItem = (k, index, removeRender) => {
    const valid = k.rfStage === 1;
    return seeFlag || valid ? formItemValid(k, index) : formItemInvalid(k, index, removeRender);
  };
  return (
    <DynamicFormSet
      fieldName="productModel"
      initialValue={initList}
      registerField={registerField()}
      formItem={formItem}
      addDisabled={disabled || seeFlag}
      formItemLayoutAddBtn={addBtnLayout}
      beforeRemove={beforeRemove}
      {...props}
    />
  );
}

export default Form.create({
  onValuesChange: (props, changedValues, allValues) => {
    const { dispatch } = props;
    const { keys, productModel = [] } = allValues;
    const modelValid = keys.filter(v => v.rfStage === 1); // 过滤出生效的数据,因为这些数据不可操作
    const modelCodeValidArr = modelValid.map(v => v.modelCode);
    const codeArr = [];
    const arr = [];
    if (keys.length < productModel.length) {
      // 此次删除最后一个
      const lastIndex = productModel.length - 1;
      productModel.splice(lastIndex, 1);
    }
    productModel.forEach(v => {
      if (v.modelCode && codeArr.indexOf(v.modelCode) === -1) {
        // 只保留有效的： 不为空、已存在的只记录其中一个
        codeArr.push(v.modelCode);
        if (modelCodeValidArr.indexOf(v.modelCode) === -1) {
          // 并且不存在已生效的产品型号，才可以保存处理
          arr.push({
            modelCode: v.modelCode || '',
            modelName: v.modelCode || '',
            modelShortName: v.modelCode || '',
          });
        }
      }
    });
    dispatch({
      // 保存的都是有效数据
      type: 'goodsapplybillmanage/changeGoodsSpuModellist',
      payload: [...modelValid, ...arr],
    });
  },
})(React.forwardRef(ProductModel));
