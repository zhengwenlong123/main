import React, { useImperativeHandle, useState, useEffect } from 'react';
import { Form, Input, Row, Col, Select } from 'antd';
import moment from 'moment';
import uuid from 'uuid';
import { formatMessage } from 'umi/locale';
import DynamicFormSet from '../../components/DynamicFormSet';
import SpanFormShow from '../../components/SpanFormShow';
// import styles from './index.less';

moment.locale('zh-cn');
const registerField = () => ({
  attr: { key: '', label: '' },
  attrValue: '',
});
/**
 * 产品申请单新增：产品普通属性部分组件
 */

function CommonAttr(props, ref) {
  const {
    dispatch,
    form,
    goodsAttrNameAble,
    goodsApplyBillDetailInfo,
    disabled,
    operationType,
  } = props;
  const { getFieldDecorator } = form;
  const seeFlag = operationType === 'see'; // 查看详情
  useImperativeHandle(ref, () => ({
    form,
  }));

  const [attrListAll, setAttrListAll] = useState(goodsAttrNameAble);
  const [initList, setInitList] = useState([{ id: uuid.v4(), ...registerField() }]);

  // 监听并更新属性集变化
  useEffect(() => {
    setAttrListAll([...goodsAttrNameAble]);
  }, [goodsAttrNameAble]);

  useEffect(() => {
    const { goodsRfSpuInfo } = goodsApplyBillDetailInfo;
    const { goodsRfSpuAttrNormalList = [] } = goodsRfSpuInfo || {};
    const arr = goodsRfSpuAttrNormalList.map(item => {
      const attr = {
        key: item.attrCode,
        label: item.attrName,
      };
      return { attr, attrValue: item.attrValue, id: uuid.v4(), ...item };
    });
    if (arr && arr.length > 0) {
      setInitList(arr);
      dispatch({
        type: 'goodsapplybillmanage/changeGoodsRfSpuAttrNormalList',
        payload: [...arr],
      });
    }
  }, [goodsApplyBillDetailInfo]);

  const formItemLayout =
    operationType !== 'audit'
      ? {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
            xl: { span: 6 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
            xl: { span: 18 },
          },
        }
      : {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 10 },
            xl: { span: 8 },
            xll: { span: 6 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 14 },
            xl: { span: 16 },
            xll: { span: 18 },
          },
        };
  const colLayout =
    operationType !== 'audit'
      ? {
          xs: 24,
          sm: 12,
          xl: 8,
        }
      : {
          xs: 24,
          sm: 12,
          xll: 8,
        };
  const addBtnLayout =
    operationType === 'audit'
      ? {
          wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 18, offset: 6 },
            xl: { span: 19, offset: 4 },
            xll: { span: 19, offset: 2 },
          },
        }
      : undefined;
  // 判断是否校验设定值必填： 属性不为空必填
  const validatorSetVal = index => {
    const { attr } = form.getFieldValue('commonAttr')[index];
    if (attr && attr.key) {
      return true;
    }
    return false;
  };
  const selectChange = (index, val = {}) => {
    const commonAttr = form.getFieldValue('commonAttr');
    commonAttr.splice(index, 1, {
      attr: { key: (val && val.key) || '', label: (val && val.label) || '' },
      attrValue: '',
    });
    form.setFieldsValue({
      commonAttr,
    });
  };
  const formItemValid = (
    k,
    index // 已生效
  ) => (
    <Row key={k.id}>
      <Col {...colLayout}>
        <Form.Item
          {...formItemLayout}
          label={`${formatMessage({ id: 'form.common.apply.attr' })}${index + 1}`}
        >
          {getFieldDecorator(`commonAttr[${index}].attr`, {
            validateTrigger: ['onChange', 'onBlur'],
            initialValue: { ...k.attr },
            rules: [
              {
                required: false,
              },
            ],
          })(<SpanFormShow.SpanSelectShow />)}
        </Form.Item>
      </Col>
      <Col {...colLayout}>
        <Form.Item
          {...formItemLayout}
          label={`${formatMessage({ id: 'form.common.apply.setValue' })}${index + 1}`}
        >
          {getFieldDecorator(`commonAttr[${index}].attrValue`, {
            validateTrigger: ['onChange', 'onBlur'],
            initialValue: k.attrValue || '',
            rules: [
              {
                required: validatorSetVal(index),
                message: formatMessage({ id: 'form.goodsApplyBillManage.message.attrValue' }),
              },
            ],
          })(<SpanFormShow.SpanInputShow />)}
        </Form.Item>
      </Col>
    </Row>
  );
  const formItemInValid = (
    k,
    index,
    removeRender // 申请单
  ) => (
    <Row key={k.id}>
      <Col {...colLayout}>
        <Form.Item
          {...formItemLayout}
          label={`${formatMessage({ id: 'form.common.apply.attr' })}${index + 1}`}
        >
          {getFieldDecorator(`commonAttr[${index}].attr`, {
            validateTrigger: ['onChange', 'onBlur'],
            initialValue: { ...k.attr },
            rules: [
              {
                required: false,
              },
            ],
          })(
            <Select
              style={{ width: '80%', marginRight: 8 }}
              allowClear
              labelInValue
              onChange={val => selectChange(index, val)}
            >
              {attrListAll.map(v => (
                <Select.Option value={v.attrCode} key={v.id}>
                  {v.attrName}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col {...colLayout}>
        <Form.Item
          {...formItemLayout}
          label={`${formatMessage({ id: 'form.common.apply.setValue' })}${index + 1}`}
        >
          {getFieldDecorator(`commonAttr[${index}].attrValue`, {
            validateTrigger: ['onChange', 'onBlur'],
            initialValue: k.attrValue || '',
            rules: [
              {
                required: validatorSetVal(index),
                message: formatMessage({ id: 'form.goodsApplyBillManage.message.attrValue' }),
              },
              {
                max: 250,
                message: formatMessage({ id: 'form.goodsApplyBillManage.value.word' }),
              },
            ],
          })(<Input style={{ width: '80%', marginRight: 8 }} />)}
          {removeRender()}
        </Form.Item>
      </Col>
    </Row>
  );
  const formItem = (k, index, removeRender) => {
    const valid = k.rfStage === 1; // 已生效的
    return seeFlag || disabled || valid
      ? formItemValid(k, index)
      : formItemInValid(k, index, removeRender);
  };
  return (
    <DynamicFormSet
      fieldName="commonAttr"
      initialValue={initList}
      registerField={registerField()}
      formItem={formItem}
      addDisabled={disabled || seeFlag}
      formItemLayoutAddBtn={addBtnLayout}
      {...props}
    />
  );
}
export default Form.create({
  onValuesChange: (props, changedValues, allValues) => {
    const { dispatch, goodsRfSpuAttrNormalList, goodsSkuAttrUsed } = props;
    const { keys, commonAttr } = allValues;
    let arr = [...goodsRfSpuAttrNormalList];
    if (changedValues.commonAttr && commonAttr.length - keys.length === 0) {
      // 说明当前是item节点数据改变
      changedValues.commonAttr.forEach((item, index) => {
        arr[index] = { ...registerField(), ...goodsRfSpuAttrNormalList[index], ...item };
      });
    }
    if (changedValues.commonAttr && commonAttr.length - keys.length > 0) {
      // 说明当前是row行减少（即删除）
      arr = changedValues.commonAttr.filter(v => v.attr && v.attr.key);
    }
    dispatch({
      // 保存的都是有效数据
      type: 'goodsapplybillmanage/changeGoodsRfSpuAttrNormalList',
      payload: [...arr],
    });
    dispatch({
      // 属性发生改变后，过滤可用sku属性保存起来
      type: 'goodsapplybillmanage/changeGoodsAttrNameAble',
      payload: [...arr.map(v => v.attr.key), ...goodsSkuAttrUsed.map(v => v.attr.key)],
    });
  },
})(React.forwardRef(CommonAttr));
