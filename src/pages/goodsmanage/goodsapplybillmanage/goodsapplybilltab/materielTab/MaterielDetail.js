import React, { useState, useEffect } from 'react';
import { Table, Card, Badge } from 'antd';
import { formatMessage } from 'umi/locale';
import styles from './index.less';

/**
 * 物料详情
 */
function MaterielDetail(props) {
  const { goodsApplyBillDetailInfo = {}, goodsRfInfo = {} } = props;
  const { goodsRfMaterielInfoWithSkuNameGroupList = [] } = goodsApplyBillDetailInfo;

  const columnsArr = [
    {
      title: '状态',
      dataIndex: 'rfStage',
      editable: false,
      width: 65,
      render: text => (
        <Badge color={text === 1 ? 'green' : 'blue'} text={text === 1 ? '已有' : '新增'} />
      ),
    },
    {
      title: '物料编号',
      dataIndex: 'materielCode',
      editable: false,
      onCell: () => ({ style: { minWidth: 60, width: 100 } }),
      render: (text, record) => (
        <span>{record.rfStage === 1 || goodsRfInfo.rfStatus === 'verify_adopt' ? text : ''}</span>
      ),
    },
    {
      title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.modelCode' }),
      dataIndex: 'modelCode',
      width: 80,
    },
    {
      title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.materielName' }),
      dataIndex: 'materielName',
    },
    {
      title: formatMessage({ id: 'form.common.series' }),
      dataIndex: 'series',
      width: 100,
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillManage.title.manufactureMateriel' }),
      dataIndex: 'manufactureMaterielCode',
      width: 120,
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillManage.title.customize' }),
      dataIndex: 'customizedMachineType',
      width: 120,
    },
  ];

  const [materielInfo, setMaterielInfo] = useState([...goodsRfMaterielInfoWithSkuNameGroupList]);
  const [columns, setColumns] = useState([...columnsArr]);

  useEffect(() => {
    setMaterielInfo([...goodsRfMaterielInfoWithSkuNameGroupList]);
  }, [goodsApplyBillDetailInfo]);

  useEffect(() => {
    const { rfTypeCode, rfStatus } = goodsRfInfo || {};
    if (rfTypeCode === 'product' && rfStatus !== 'verify_adopt') {
      // 产品、除审核通过的物料都不显示物料编码表头列
      const [first, , ...other] = columnsArr;
      setColumns([{ ...first }, ...other]);
    }
  }, [goodsRfInfo]);

  const preMateriel = () => {
    if (materielInfo.length === 0) {
      return (
        <div>
          <h3 className={styles.txtCenter}>
            {formatMessage({
              id: 'message.datasource.null',
            })}
          </h3>
        </div>
      );
    }
    return materielInfo.map(item => {
      const { goodsRfSkuInfo = {}, goodsRfMaterielInfoList = [] } = item;
      const { skuName = '' } = goodsRfSkuInfo || {};
      const arr = goodsRfMaterielInfoList.map(v => {
        const { goodsRfMaterielEbs, ...other } = v;
        const { manufactureMaterielCode = '', customizedMachineType = '' } =
          goodsRfMaterielEbs || {};
        return { ...other, manufactureMaterielCode, customizedMachineType, skuName };
      });
      return (
        <Card
          key={goodsRfSkuInfo.skuCode}
          title={`商品：${goodsRfSkuInfo.skuName}`}
          bordered={false}
          className={styles.skuTabCard}
          headStyle={{ paddingLeft: 0, minHeight: 30, borderBottom: 0 }}
          bodyStyle={{ padding: 0 }}
        >
          <Table
            size="small"
            bordered={false}
            rowKey="materielCode"
            dataSource={arr || []}
            columns={columns}
            pagination={false}
          />
        </Card>
      );
    });
  };
  return preMateriel();
}

export default MaterielDetail;
