import React from 'react';
import { Select, Input, Button, Card, Message, Badge } from 'antd';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import moment from 'moment';
import uuid from 'uuid';
import EditableTableBasic from '@/components/EditableTable/EditableTableBasic';
import AddAbleTableBlock from './AddAbleTableBlock';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 必填项标题处理
 * @param {*} label 标题
 */
const headerColumntitle = label => (
  <span>
    <span style={{ color: 'red' }}>*</span>
    {label}
  </span>
);

// 编辑时，不能编辑的列的dataIndex
const myNotEditAbleColList = ['modelCode'];

class MaterielItem extends EditableTableBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: '状态',
        dataIndex: 'rfStage',
        editable: false,
        onCell: () => ({ style: { width: 70 } }),
        render: text => (
          <Badge color={text === 1 ? 'green' : 'blue'} text={text === 1 ? '已有' : '新增'} />
        ),
      },
      {
        title: '物料编号',
        dataIndex: 'materielCode',
        editable: false,
        width: 110,
        render: (text, record) => <span>{record.rfStage === 1 ? text : ''}</span>,
      },
      {
        title: headerColumntitle(
          formatMessage({
            id: 'form.goodsMaterielInfoUpkeep.modelCode',
          })
        ),
        dataIndex: 'modelCode',
        editable: true,
        width: 90,
        rules: [
          { validator: (rule, value, callback) => this.validatorModelCode(rule, value, callback) },
        ],
        render: text => <span>{text}</span>,
        renderInput: () => this.spuModelSelect(),
      },
      {
        title: headerColumntitle(
          formatMessage({
            id: 'form.goodsMaterielInfoUpkeep.materielName',
          })
        ),
        dataIndex: 'materielName',
        onCell: () => ({ style: { minWidth: 160 } }),
        editable: true,
        rules: [
          {
            required: true,
            message: formatMessage({
              id: 'form.goodsMaterielInfoUpkeep.materielName.placeholder',
            }),
          },
        ],
        renderInput: () => <Input style={{ minWidth: 160 }} />,
      },
      {
        title: formatMessage({
          id: 'form.common.series',
        }),
        dataIndex: 'series',
        width: 100,
        editable: true,
        rules: [
          {
            required: false,
            message: formatMessage({
              id: 'form.goodsApplyBillManage.materiel.series.placeholder',
            }),
          },
        ],
        renderInput: () => <Input />,
      },
      {
        title: formatMessage({
          id: 'form.goodsApplyBillManage.title.manufactureMateriel',
        }),
        dataIndex: 'manufactureMaterielCode',
        width: 120,
        editable: true,
        rules: [
          {
            validator: (rule, value, callback) =>
              this.validatorManufactureMaterielCode(rule, value, callback),
          },
        ],
        renderInput: () => <Input />,
      },
      {
        title: formatMessage({
          id: 'form.goodsApplyBillManage.title.customize',
        }),
        dataIndex: 'customizedMachineType',
        width: 120,
        editable: true,
        rules: [
          {
            required: false,
            message: formatMessage({
              id: 'form.goodsApplyBillManage.title.customize',
            }),
          },
        ],
        renderInput: () => <Input />,
      },
    ];
    // const validatorDispatch = list => {
    //   // 筛选出已生效数据
    //   const { goodsMaterielList = [], good } = props;
    //   const arr = goodsMaterielList.filter(v => v.skuCode !== good.skuCode);
    //   return [...arr, ...list];
    // };
    super(props, {
      tableColumns,
      validatorDispatch: list => this.materielToredux(list),
      rowKey: 'materielCode',
      dispatchType: 'goodsapplybillmanage/changeGoodsMaterielList',
    });
    this.handled = false;
  }

  componentDidMount() {
    const { goodsMaterielList } = this.props;
    this.handleMateriellist(goodsMaterielList);
  }

  componentWillReceiveProps(nextProps) {
    const { goodsMaterielList, goodsSpuModelChangeClear } = this.props;
    if (goodsMaterielList !== nextProps.goodsMaterielList) {
      this.handleMateriellist(nextProps.goodsMaterielList);
    }
    if (goodsSpuModelChangeClear !== nextProps.goodsSpuModelChangeClear) {
      // 产品型号发生改变,清除相关不符合规则的物料数据
      const modelArr = nextProps.goodsSpuModellist.map(v => v.modelCode) || [];
      let arr = [];
      if (modelArr.length === 0) {
        arr = goodsMaterielList.filter(i => !i.modelCode);
      } else {
        arr = goodsMaterielList.filter(i => {
          if (!modelArr.includes(i.modelCode) && i.rfStage !== 1) {
            return false;
          }
          return true;
        });
      }
      this.clearMateriellist(arr);
    }
  }

  clearMateriellist = arr => {
    const { dispatch } = this.props;
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsMaterielList',
      payload: arr,
    });
    this.setState({
      goodMateries: [...arr],
    });
  };

  // 格式化物料数据
  // formatMateriel = list => {
  //   let materielList = [];
  //   list.forEach(v => {
  //     const { skuName = '' } = v.goodsRfSkuInfo || {};
  //     const arr = v.goodsRfMaterielInfoList.map(item => {
  //       const { goodsRfMaterielEbs, ...other } = item;
  //       const { manufactureMaterielCode = '', customizedMachineType = '' } =
  //         goodsRfMaterielEbs || {};
  //       return { ...other, manufactureMaterielCode, customizedMachineType, skuName };
  //     });
  //     materielList = materielList.concat(arr);
  //   });
  //   return materielList;
  // };

  // 校验是有效才保存到redux
  materielToredux = list => {
    const { goodsMaterielList = [], good } = this.props;
    const arr = goodsMaterielList.filter(v => v.skuCode !== good.skuCode);
    return [...arr, ...list];
  };

  // 获取当前商品下的物料总数据
  handleMateriellist = goodsMaterielList => {
    const goodMateries = []; // 当前商品下的物料总数据
    const { good, goodsSpuModellist, dispatch } = this.props;
    goodsMaterielList.forEach(v => {
      if (v.skuCode === good.skuCode) {
        goodMateries.push({ ...good, ...v });
      }
    });
    // 商品下如无物料，则新增一个默认物料
    if (goodMateries.length === 0) {
      const defaultMaterie = {
        materielCode: uuid(),
        materielName: '',
        series: '',
        manufactureMaterielCode: '',
        customizedMachineType: '',
        // 默认数据
        groupCode: good.groupCode,
        spuCode: good.spuCode,
        skuCode: good.skuCode,
        skuId: good.id,
        materielEnable: 1, // 1可用，0不可用
      };
      if (goodsSpuModellist.length === 0) {
        defaultMaterie.materielName = good.skuName;
      } else {
        defaultMaterie.materielName = `${good.skuName} ${goodsSpuModellist[0].modelName}`;
        defaultMaterie.modelCode = goodsSpuModellist[0].modelCode;
      }
      goodMateries.push(defaultMaterie);
      goodsMaterielList.push(defaultMaterie);
      dispatch({
        type: 'goodsapplybillmanage/changeGoodsMaterielList',
        payload: goodsMaterielList,
      });
    }
    this.setState({
      goodMateries,
    });
  };

  /** 型号下拉框 */
  spuModelSelect = () => {
    const { goodsSpuModellist } = this.props;
    const { goodMateries = [] } = this.state;
    const modellist = [];
    goodsSpuModellist.forEach(v => {
      let flag = false;
      goodMateries.forEach(val => {
        if (val.modelCode === v.modelCode) {
          flag = true;
        }
      });
      if (!flag) modellist.push(v);
    });
    return (
      <Select style={{ width: '100%', minWidth: 80 }}>
        {modellist.map(v => (
          <Select.Option value={v.modelCode} key={v.modelCode}>
            {v.modelCode}
          </Select.Option>
        ))}
      </Select>
    );
  };

  /**
   * 产品型号选中后自动带出物料名称： 商品名称 + 空格 + 产品型号
   */
  toMaterielName = val => {
    const { editingKey, rowKey, goodMateries = [] } = this.state;
    if (!val) return;
    const findIndex = goodMateries.findIndex(v => v[rowKey] === editingKey);
    if (findIndex !== -1) {
      const materielName = `${goodMateries[findIndex].skuName} ${val}`;
      goodMateries.splice(findIndex, 1, { ...goodMateries[findIndex], materielName });
      this.updateData([...goodMateries]);
    }
  };

  /**
   * 产品型号是否存在校验
   * 若存在产品型号数据，则做默认型号字段存在性校验，否则不做检验
   */
  validatorModelCode = (rule, value, callback) => {
    const { goodsSpuModellist } = this.props;
    if (goodsSpuModellist.length > 0 && !value) {
      callback(formatMessage({ id: 'form.goodsApplyBillManage.materiel.modelCode.placeholder' }));
    } else {
      callback();
      this.toMaterielName(value);
    }
  };

  /**
   * 厂家物料编码校验
   * 1. 校验 规则：在同一一个品牌下，保持唯一性 数据范围：已生效的物料及当前申请单的物料
   * 2. 此值可以录入，也支持不录入，一旦录入，则必须做数据校验
   */
  validatorManufactureMaterielCode = (rule, value, callback) => {
    const { goodsMaterielList } = this.props;
    const { editingKey, rowKey } = this.state;
    if (value) {
      const arr = goodsMaterielList.filter(v => v.manufactureMaterielCode === value);
      if (arr && arr.length === 2) {
        callback(
          formatMessage({ id: 'form.goodsApplyBillManage.title.manufactureMateriel.repeat' })
        );
      } else if (arr && arr.length === 1 && arr[0][rowKey] !== editingKey) {
        callback(
          formatMessage({ id: 'form.goodsApplyBillManage.title.manufactureMateriel.repeat' })
        );
      } else {
        callback();
      }
    } else {
      callback();
    }
  };

  /**
   * 删除函数
   * @param {string} key 即将删除的行标识key
   * @param {array} dataList 列表所有数据
   */
  myDelete = (key, dataList) => {
    const list = [];
    const { rowKey } = this.state;
    if (dataList.length === 1) {
      Message.warning('商品至少有一个对应物料');
    } else {
      dataList.forEach(v => {
        if (v[`${rowKey}`] !== key) {
          list.push(v);
        }
      });
      this.updateData(list);
    }
  };

  // otherButton 展示条件：删除按钮
  otherButtonShow = (record, index) => index === 0 || record.rfStage === 1; // 不显示的条件

  // otherButton 展示条件：编辑按钮
  editBtnShow = record => record.rfStage === 1; // 不显示的条件

  /**
   * 保存函数
   * @param {number} index  当前操作的行位置
   * @param {array} row 当前操作的行数据
   * @param {array} list 即将保存更新的所有列表数据
   */
  // mySave = (index, row, list) => {
  //   console.log('onSave', list);
  //   this.setState({ editingKey: '' }, () => {
  //     this.updateData(list);
  //   });
  //   // 单个商品的物料出现变化的时候，整体的物料列表变化
  //   const { goodsMaterielList, dispatch } = this.props;
  //   let myList = [];
  //   goodsMaterielList.forEach(v => {
  //     let flag = false;
  //     try {
  //       list.forEach(m => {
  //         if (v.materielCode === m.materielCode) {
  //           flag = true;
  //           throw new Error('找到相同物料');
  //         }
  //       });
  //     } catch (error) {
  //       console.log(error);
  //     }

  //     if (!flag) myList.push(v);
  //   });
  //   myList = myList.concat(list);
  //   dispatch({
  //     type: 'goodsapplybillmanage/changeGoodsMaterielList',
  //     payload: myList,
  //   });
  // };

  render() {
    const { good, goodsSpuModellist } = this.props;
    const { editingKey, tableColumns, rowKey, goodMateries = [] } = this.state;
    const otherButton = {
      value: 'remove',
      label: formatMessage({ id: 'button.common.delete' }),
      onClick: this.myDelete,
    };
    const payload = () => ({
      materielCode: uuid(),
      materielName: '',
      series: '',
      manufactureMaterielCode: '',
      customizedMachineType: '',
      // 默认数据
      spuCode: good.spuCode,
      skuCode: good.skuCode,
      skuId: good.id,
      materielEnable: 1, // 1可用，0不可用
      rfStage: 0,
    });
    const extra = (() => {
      let disable = false;
      if (goodsSpuModellist.length === 0) {
        // 产品不存在型号时，一个商品只能新增一个物料
        disable = goodMateries.length === 1;
      } else {
        // 产品型号存在是，一个商品的物料要小于或者等于产品型号数量
        disable = goodMateries.length === goodsSpuModellist.length;
      }
      return (
        <Button
          disabled={disable}
          icon="plus"
          type="primary"
          onClick={() => this.newAddRow({ ...payload() }, goodMateries)}
        >
          新建
        </Button>
      );
    })();
    return (
      <Card
        title={`商品：${good.skuName}`}
        bordered={false}
        extra={extra}
        className={styles.skuTabCard}
        headStyle={{ paddingLeft: 0, minHeight: 30, borderBottom: 0 }}
        bodyStyle={{ padding: 0 }}
      >
        <AddAbleTableBlock
          rowKey={rowKey}
          columns={tableColumns}
          dataList={goodMateries}
          otherButton={otherButton}
          otherButtonShow={this.otherButtonShow}
          onSave={this.onSave}
          onCanCel={this.onCanCel}
          editingKey={editingKey}
          editingKeyChange={this.editingKeyChange}
          editBtnShow={this.editBtnShow}
          notEditAbleColList={myNotEditAbleColList}
          // scroll={{ x: true }}
        />
      </Card>
    );
  }
}

export default connect(({ goodsapplybillmanage }) => ({
  goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 申请单信息
  goodsSkuAttrUsed: goodsapplybillmanage.goodsSkuAttrUsed || [], // sku属性(产品tab中sku信息已设置好的属性内容)
  goodsSkulist: goodsapplybillmanage.goodsSkulist || [], // 商品列表数据
  goodsSpuModellist: goodsapplybillmanage.goodsSpuModellist || [], // 产品型号总数据
  goodsMaterielList: goodsapplybillmanage.goodsMaterielList || [], // 物料总数据
  goodsApplyBillDetailInfo: goodsapplybillmanage.goodsApplyBillDetailInfo || {}, // 维护时查询出来的总数据
}))(MaterielItem);
