import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import moment from 'moment';
import MaterielItem from './MaterielItem';
import MaterielDetail from './MaterielDetail';
import styles from './index.less';

moment.locale('zh-cn');

class MaterielList extends PureComponent {
  render() {
    const { goodsSkulist, goodsSkulistValid, operationType } = this.props;
    const skuList = [...goodsSkulistValid, ...goodsSkulist];
    let data = {};
    if (operationType === 'see') {
      // 查看详情下
      return <MaterielDetail {...this.props} />;
    }
    if (skuList.length > 0 && skuList[0].skuName) {
      data = (
        <div>
          {skuList.map(v => (
            <MaterielItem good={v} key={v.id} operationType={operationType} />
          ))}
        </div>
      );
    } else {
      data = (
        <div>
          <h3 className={styles.txtCenter}>
            {formatMessage({
              id: 'form.goodsApplyBillManage.materiel.noMateriel',
            })}
          </h3>
        </div>
      );
    }
    return data;
  }
}

export default connect(({ goodsapplybillmanage }) => ({
  goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 申请单信息
  goodsSkulist: goodsapplybillmanage.goodsSkulist || [], // 商品列表数据(新)
  goodsSkulistValid: goodsapplybillmanage.goodsSkulistValid || [], // 商品列表数据（已生效）
  goodsApplyBillDetailInfo: goodsapplybillmanage.goodsApplyBillDetailInfo || {}, // 申请单详情
}))(MaterielList);
