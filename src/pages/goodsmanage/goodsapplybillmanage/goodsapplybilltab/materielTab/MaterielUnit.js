import React, { useState, useEffect } from 'react';
import { Card, Popconfirm, Button, Input } from 'antd';
import moment from 'moment';
import EditTableCellStandard from '@/components/EditableTableCell/EditableCellStandard';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增：物料单个商品单元组件
 */
function MaterielUnit(props) {
  const { unitValue = [] } = props;

  const [dataSource, setDataSource] = useState(unitValue);
  const [count, setCount] = useState(1);

  useEffect(() => {
    setDataSource(unitValue);
  }, [unitValue]);

  const handleDelete = key => {
    setDataSource(dataSource.filter(item => item.key !== key));
  };

  const handleAdd = () => {
    const newData = {
      key: count,
      modelCode: ``,
      materielName: '',
      series: ``,
    };
    setDataSource([...dataSource, newData]);
    setCount(count + 1);
  };

  const onSave = (newData, error, currentError) => {
    console.log('保存', newData, error, currentError);
    setDataSource(newData);
  };

  const columns = [
    {
      title: '产品型号',
      dataIndex: 'modelCode',
      width: '30%',
      editable: true,
      formItem: (callbackNode, callbackEvent) => (
        <Input
          ref={node => callbackNode(node)}
          onPressEnter={callbackEvent}
          onBlur={callbackEvent}
        />
      ),
    },
    {
      title: '物料名称',
      dataIndex: 'materielName',
      editable: true,
      formItem: (callbackNode, callbackEvent) => (
        <Input
          ref={node => callbackNode(node)}
          onPressEnter={callbackEvent}
          onBlur={callbackEvent}
        />
      ),
    },
    {
      title: '系列',
      dataIndex: 'series',
      editable: true,
      formItem: (callbackNode, callbackEvent) => (
        <Input
          ref={node => callbackNode(node)}
          onPressEnter={callbackEvent}
          onBlur={callbackEvent}
        />
      ),
    },
    {
      title: '操作',
      dataIndex: 'operation',
      render: (text, record) => (
        <Popconfirm title="确定移除" onConfirm={() => handleDelete(record.key)}>
          <a>移除</a>
        </Popconfirm>
      ),
    },
  ];

  const extra = (
    <Button size="small" icon="plus" onClick={() => handleAdd()} type="primary">
      添加物料
    </Button>
  );
  const title = `商品：${dataSource.skuName}`;
  return (
    <Card
      title={title}
      bordered={false}
      extra={extra}
      className={styles.skuTabCard}
      headStyle={{ paddingLeft: 0, minHeight: 30 }}
      bodyStyle={{ padding: '0px 24px' }}
    >
      <EditTableCellStandard
        bordered={false}
        columns={columns}
        dataSource={dataSource.list}
        pagination={false}
        onSave={onSave}
        className={styles.EditTableCellStandard}
      />
    </Card>
  );
}

export default MaterielUnit;
