import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
// import NewAdd from './NewAdd';
import MaterielList from './MaterielList';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增、维护的界面《物料》部分组件
 */
class MaterielTab extends React.PureComponent {
  render() {
    const { operationType } = this.props;
    return (
      <div className={styles.skuTab}>
        <div style={{ marginBottom: 24 }}>
          <MaterielList operationType={operationType} />
        </div>
      </div>
    );
  }
}

export default connect(({ goodsbrandmanage }) => ({
  goodsSpuModellist: goodsbrandmanage.goodsSpuModellist || [],
  goodsBrandListPageSize: goodsbrandmanage.goodsBrandListPageSize || 0,
}))(MaterielTab);
