import React, { Fragment } from 'react';
import { Table, Form, Divider } from 'antd';

const FormItem = Form.Item;
const EditableContext = React.createContext();

const EditableCell = props => {
  const {
    editing,
    dataIndex,
    title,
    // inputType,
    record,
    index,
    renderInput,
    validate,
    validateTitle,
    initialValueType,
    init,
    rulesVal,
    notEditAbleColList = [],
    ...restProps
  } = props;

  /** 表单检验可有俩种办法定义
   * 一、自定义，采用rules字段，格式完全依照form的rules格式
   * 二、默认定义，{required， message},通过字段validate（boolean）,
   *    validateTitle(string/字段中文名称)
   */
  const rules = rulesVal || [
    {
      required: !!validate,
      message: `${validateTitle}不能为空`,
    },
  ];

  /** 判断编辑时哪些列不能编辑
   *  根据notEditAbleColList设置的dataIndex判断，编辑时，哪些列不能编辑
   */
  const isEditAbleCol = (ind, editAbleColList = []) => {
    let flag = true;
    try {
      editAbleColList.forEach(element => {
        if (element === ind) {
          flag = false;
          // throw new Error('找到编辑时不能编辑的列');
        }
      });
    } catch (error) {
      console.log(error);
    }
    return flag;
  };

  const tableCell = (getFieldDecorator, form) => {
    let res = null;
    if (editing) {
      // 可编辑状态
      // 新增数据时不需要控制
      if (record.add) {
        res = (
          <FormItem style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules,
              initialValue: initialValueType ? init(record) : record[dataIndex],
            })(renderInput(form))}
          </FormItem>
        );
      } else {
        // 编辑时，判断
        res = isEditAbleCol(dataIndex, notEditAbleColList) ? (
          <FormItem style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules,
              initialValue: initialValueType ? init(record) : record[dataIndex],
            })(renderInput())}
          </FormItem>
        ) : (
          restProps.children
        );
      }
    } else {
      res = restProps.children;
    }
    return res;
  };

  return (
    <EditableContext.Consumer>
      {form => {
        const { getFieldDecorator } = form;
        return <td {...restProps}>{tableCell(getFieldDecorator, form)}</td>;
      }}
    </EditableContext.Consumer>
  );
};

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [...props.columns, this.operationColumn(props)],
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      columns: [...nextProps.columns, this.operationColumn(nextProps)],
    });
  }

  operation = (form, record) => {
    const { rowKey } = this.props;
    if (record.add) {
      return (
        <a style={{ marginRight: 8 }} onClick={() => this.save(form, record[rowKey], 'add')}>
          添加
        </a>
      );
    }
    return (
      <a style={{ marginRight: 8 }} onClick={() => this.save(form, record[rowKey], 'edit')}>
        保存
      </a>
    );
  };

  operationColumn = myProps => {
    const {
      rowKey,
      editingKey,
      otherButton = null,
      otherButtonShow = null,
      editBtnShow = null,
      otherBtnRender = null,
      dataList,
    } = myProps;
    return {
      title: '操作',
      width: 120,
      // dataIndex: rowKey,
      render: (text, record, index) => {
        const editable = this.isEditing(record);
        return (
          <div>
            {editable ? (
              <span>
                <EditableContext.Consumer>
                  {form => this.operation(form, record)}
                </EditableContext.Consumer>
                <a onClick={() => this.cancel(record[rowKey], record.add)}>取消</a>
              </span>
            ) : (
              <Fragment>
                {editBtnShow && editBtnShow(record, index) ? (
                  <span>&nbsp;</span>
                ) : (
                  <a disabled={editingKey !== ''} onClick={() => this.edit(record[rowKey])}>
                    编辑
                  </a>
                )}
                {/* {(otherButton || otherBtnRender) && <Divider type="vertical" />} */}
                {otherButton &&
                  (otherButtonShow && otherButtonShow(record, index) ? (
                    <span>&nbsp;</span>
                  ) : (
                    <Fragment>
                      <Divider type="vertical" />
                      <a
                        disabled={editingKey !== ''}
                        onClick={() =>
                          otherButton.onClick(record[rowKey], dataList, otherButton.value)
                        }
                      >
                        {otherButton && otherButton.label}
                      </a>
                    </Fragment>
                  ))}
                {otherBtnRender && otherBtnRender(record)}
              </Fragment>
            )}
          </div>
        );
      },
    };
  };

  editingKeySet = (val = '') => {
    const { editingKeyChange } = this.props;
    editingKeyChange(val);
  };

  isEditing = record => {
    const { rowKey, editingKey } = this.props;
    return record[rowKey] === editingKey;
  };

  cancel = (key, add) => {
    const { dataList, onCanCel, rowKey } = this.props;
    const newData = [...dataList];
    const index = newData.findIndex(item => key === item[rowKey]);
    if (index > -1 && add) {
      newData.splice(index, 1);
      onCanCel(newData);
    } else {
      onCanCel(dataList);
    }
  };

  save(form, key, type) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const { dataList, onSave, rowKey } = this.props;
      const newData = [...dataList];
      const index = newData.findIndex(item => key === item[rowKey]);
      if (type === 'add') {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        delete newData[index].add;
        onSave(index, row, newData, type);
      }
      if (index > -1 && type === 'edit') {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        onSave(index, newData[index], newData, type);
      } else if (type === 'edit') {
        newData.push(row);
        onSave(index, row, newData, type);
      }
    });
  }

  edit(key) {
    this.editingKeySet(key);
  }

  render() {
    const components = {
      body: {
        cell: EditableCell,
      },
    };
    const { columns = [] } = this.state;
    const { notEditAbleColList = [] } = this.props;

    const columnsVal = columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          renderInput: col.renderInput,
          initialValueType: col.initialValueType,
          init: col.init,
          rulesVal: col.rules,
          validate: col.validate,
          validateTitle: col.validateTitle,
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
          notEditAbleColList,
        }),
      };
    });

    const {
      form,
      headTitle,
      dataList,
      rowKey,
      loading = false,
      bordered = false,
      scroll,
    } = this.props;
    return (
      <EditableContext.Provider value={form}>
        <Table
          rowKey={rowKey || 'key'}
          title={headTitle}
          bordered={bordered}
          loading={loading}
          components={components}
          dataSource={dataList}
          columns={columnsVal}
          rowClassName="editable-row"
          className="ediable-table-block small-margin-common-table"
          pagination={false}
          scroll={scroll}
          // {...other}
          // footer={() => this.footerRender()}
        />
      </EditableContext.Provider>
    );
  }
}

const EditableTableBlock = Form.create()(EditableTable);

export default EditableTableBlock;
