import React from 'react';
import { connect } from 'dva';
import { Card, Empty } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import SubgroupValid from './SubgroupValid';
import Subgroup from './Subgroup';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增、维护的界面商品分组部分组件
 */
class GroupTab extends React.PureComponent {
  groupForm = null;

  getForm = form => {
    this.groupForm = form;
  };

  render() {
    const groupTitle = formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode' });
    const built = formatMessage({ id: 'form.common.apply.built' });
    const building = formatMessage({ id: 'form.common.apply.building' });
    const {
      goodsApplyBillDetailInfo: { goodsRfSkuGroupList = [] },
      operationType, // 操作类型
    } = this.props;
    const arr = goodsRfSkuGroupList.filter(item => item.rfStage === 1);
    /**
     * 查看详情，判断数值为空，渲染空界面图案
     */
    const seeMain =
      goodsRfSkuGroupList.length > 0 ? (
        <Card
          bordered={false}
          title={`${groupTitle}(${building})`}
          headStyle={{ paddingLeft: 0, minHeight: 30 }}
        >
          <SubgroupValid isSee {...this.props} />
        </Card>
      ) : (
        <Empty />
      );
    return (
      <div className={styles.productCont}>
        {arr.length > 0 && (
          <Card
            title={`${groupTitle}(${built})`}
            bordered={false}
            headStyle={{ paddingLeft: 0, minHeight: 30 }}
          >
            <SubgroupValid {...this.props} />
          </Card>
        )}
        {operationType === 'see' ? (
          seeMain
        ) : (
          <Card
            title={arr.length > 0 ? `${groupTitle}(${building})` : `${groupTitle}`}
            bordered={false}
            headStyle={{ paddingLeft: 0, minHeight: 30 }}
          >
            <Subgroup {...this.props} wrappedComponentRef={this.getForm} />
          </Card>
        )}
      </div>
    );
  }
}

export default connect(({ goodsapplybillmanage }) => ({
  goodsSkuAttrUsed: goodsapplybillmanage.goodsSkuAttrUsed || [], // sku属性(产品tab中sku信息已设置好的属性内容)
  goodsGrouplist: goodsapplybillmanage.goodsGrouplist || [], // 商品分组列表总数据
  goodsApplyBillDetailInfo: goodsapplybillmanage.goodsApplyBillDetailInfo || {},
}))(GroupTab);
