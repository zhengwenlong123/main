import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Row, Col, Form } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import DynamicFormSet from '../../components/DynamicFormSet';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增：商品分组部分组件
 */
function SubgroupValid(props, ref) {
  const registerField = () => ({
    subAttr: { key: '', label: '' },
    groupCode: '',
    groupName: '',
  });

  const { form, goodsApplyBillDetailInfo, isSee } = props;
  useImperativeHandle(ref, () => ({
    form,
  }));
  const [initList, setInitList] = useState([]);

  useEffect(() => {
    const { goodsRfSkuGroupList = [] } = goodsApplyBillDetailInfo;
    if (isSee) {
      setInitList(goodsRfSkuGroupList);
    } else {
      const arr = goodsRfSkuGroupList.filter(item => item.rfStage === 1);
      if (arr && arr.length > 0) {
        setInitList(arr);
      }
    }
  }, [goodsApplyBillDetailInfo]);

  const colLayout = {
    xs: 24,
    sm: 7,
    xl: 7,
    xll: 6,
  };
  const removeLayout = {
    wrapperCol: {
      xs: { span: 24, offset: 0 },
      sm: { span: 21, offset: 3 },
      xl: { span: 22, offset: 2 },
    },
  };
  const formItem = (k, index) => {
    return (
      <Row key={k.id}>
        <Col xs={24} sm={3} xl={2}>
          <Form.Item>
            {`${formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode' })}${index + 1}`}
          </Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item key={k.id}>{k.groupName || ''}</Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item key={k.id}>{k.groupCode || ''}</Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item key={k.id}>{`${k.subAttrName1}(${k.subAttrCode1})`}</Form.Item>
        </Col>
      </Row>
    );
  };
  return (
    <DynamicFormSet
      initialValue={initList}
      registerField={registerField()}
      formItem={formItem}
      formItemLayoutAddBtn={removeLayout}
      addDisabled
      {...props}
    />
  );
}

export default Form.create()(React.forwardRef(SubgroupValid));
