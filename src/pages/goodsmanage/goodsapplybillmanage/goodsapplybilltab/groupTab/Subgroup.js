import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Row, Col, Form, Input, Select } from 'antd';
import moment from 'moment';
import uuid from 'uuid';
import { formatMessage } from 'umi/locale';
import DynamicFormSet from '../../components/DynamicFormSet';
// import styles from './index.less';

moment.locale('zh-cn');
const registerField = () => ({
  subAttr: { key: '', label: '' },
  groupCode: '',
  groupName: '',
});
/**
 * 产品申请单新增：商品分组部分组件
 */
function Subgroup(props, ref) {
  /** 处理出分组属性列表 */
  const handleSkuAttr = arr => {
    if (arr.length === 0) return [];
    const attrArr = [];
    arr.forEach(v => {
      if (v.attr && v.attr.key) {
        attrArr.push({ ...v.attr });
      }
    });
    return attrArr;
  };

  const { form, goodsSkuAttrUsed, goodsApplyBillDetailInfo, dispatch, goodsGrouplist } = props;
  useImperativeHandle(ref, () => ({
    form,
  }));
  const [attrList, setAttrList] = useState(handleSkuAttr(goodsSkuAttrUsed));
  const [initList, setInitList] = useState([{ id: uuid.v4(), ...registerField() }]);

  // 监听并更新属性集变化
  useEffect(() => {
    setAttrList(handleSkuAttr(goodsSkuAttrUsed));
  }, [goodsSkuAttrUsed]);

  useEffect(() => {
    const { goodsRfSkuGroupList = [] } = goodsApplyBillDetailInfo;
    const arr = goodsRfSkuGroupList.map(item => ({
      ...item,
      id: uuid.v4(),
      subAttr: { key: item.subAttrCode1, label: item.subAttrName1 },
    }));
    if (arr && arr.length > 0) {
      setInitList(arr.filter(v => v.rfStage !== 1));
      dispatch({
        type: 'goodsapplybillmanage/changeGoodsGrouplist',
        payload: [...arr],
      });
    }
  }, [goodsApplyBillDetailInfo]);

  const { getFieldDecorator } = form;
  const colLayout = {
    xs: 24,
    sm: 7,
    xl: 7,
    xll: 6,
  };
  const removeLayout = {
    wrapperCol: {
      xs: { span: 24, offset: 0 },
      sm: { span: 21, offset: 3 },
      xl: { span: 22, offset: 2 },
    },
  };
  // 移除分组
  const removeGroup = () => {
    const validData = goodsGrouplist.filter(v => v.rfStage === 1);
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsGrouplist',
      payload: [...validData, ...form.getFieldValue('groups')],
    });
  };
  /**
   * 商品分组名称和分组编码唯一性校验
   * @param {string} value  检验的当前数值
   * @param {function} callback  校验回调函数
   * @param {string} filed  正在校验的字段名称
   */
  const groupNameOrCodeValidator = (value, callback, filed) => {
    if (value) {
      const groupsNameAll = form.getFieldValue('groups');
      const filters = groupsNameAll.filter(v => v[filed] === value);
      if (filters.length > 1) {
        callback(formatMessage({ id: `form.goodsApplyBillManage.message.${filed}.repeat` }));
      } else {
        callback();
      }
    }
    callback();
  };
  const formItem = (k, index, removeRender) => {
    return (
      <Row key={k.id}>
        <Col xs={24} sm={3} xl={2}>
          <Form.Item>
            {`${formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode' })}${index + 1}`}
          </Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item key={k.id}>
            {getFieldDecorator(`groups[${index}].groupName`, {
              validateTrigger: ['onChange', 'onBlur'],
              initialValue: k.groupName || '',
              rules: [
                {
                  required: false,
                  message: formatMessage({ id: 'form.common.apply.groupName' }),
                },
                {
                  validator: (rule, value, callback) =>
                    groupNameOrCodeValidator(value, callback, 'groupName'),
                },
              ],
            })(
              <Input
                placeholder={formatMessage({ id: 'form.common.apply.groupName' })}
                style={{ width: '90%', marginRight: 8 }}
              />
            )}
          </Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item key={k.id}>
            {getFieldDecorator(`groups[${index}].groupCode`, {
              validateTrigger: ['onChange', 'onBlur'],
              initialValue: k.groupCode || '',
              rules: [
                {
                  required: form.getFieldValue('groups')[index].groupName,
                  message: formatMessage({ id: 'form.common.apply.groupCode.placeholder' }),
                },
                {
                  validator: (rule, value, callback) =>
                    groupNameOrCodeValidator(value, callback, 'groupCode'),
                },
              ],
            })(
              <Input
                placeholder={formatMessage({ id: 'form.common.apply.groupCode' })}
                style={{ width: '90%', marginRight: 8 }}
              />
            )}
          </Form.Item>
        </Col>
        <Col {...colLayout}>
          <Form.Item key={k.id}>
            {getFieldDecorator(`groups[${index}].subAttr`, {
              validateTrigger: ['onChange', 'onBlur'],
              initialValue: k.subAttr,
              rules: [
                {
                  required: form.getFieldValue('groups')[index].groupName,
                  // whitespace: true,
                  message: formatMessage({ id: 'form.common.apply.groupAttr.placeholder' }),
                },
              ],
            })(
              <Select
                labelInValue
                style={{ width: '80%', marginRight: 8 }}
                placeholder={formatMessage({ id: 'form.common.apply.groupAttr' })}
              >
                {attrList.map(v => (
                  <Select.Option value={v.key} key={v.key}>
                    {v.label}
                  </Select.Option>
                ))}
              </Select>
            )}
            {removeRender()}
          </Form.Item>
        </Col>
      </Row>
    );
  };
  return (
    <DynamicFormSet
      fieldName="groups"
      initialValue={initList}
      registerField={registerField()}
      formItem={formItem}
      formItemLayoutAddBtn={removeLayout}
      onRemove={removeGroup}
      {...props}
    />
  );
}

export default Form.create({
  onValuesChange: (props, changedValues, allValues) => {
    const { dispatch, goodsGrouplist } = props;
    const { keys, groups } = allValues;
    const validData = goodsGrouplist.filter(v => v.rfStage === 1);
    let invalidData = goodsGrouplist.filter(v => v.rfStage !== 1);

    if (changedValues.groups && groups.length - keys.length === 0) {
      // 说明当前是item节点数据改变
      changedValues.groups.forEach((item, index) => {
        invalidData[index] = { ...registerField(), ...invalidData[index], ...item };
      });
    }
    if (changedValues.groups && groups.length - keys.length > 0) {
      // 说明当前是row行减少（即删除）
      invalidData = changedValues.groups.filter(v => v.groupCode && v.groupName);
    }

    // invalidData = invalidData.filter(item => item.groupCode && item.groupName);
    dispatch({
      type: 'goodsapplybillmanage/changeGoodsGrouplist',
      payload: [...validData, ...invalidData],
    });
  },
})(React.forwardRef(Subgroup));
