import React, { useState, useEffect } from 'react';
import { Table, Card } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增：商品分组部分组件
 */
function groupList(props) {
  // 默认表格列数据
  const defaultColumns = [
    {
      title: formatMessage({ id: 'form.common.apply.groupName' }),
      dataIndex: 'groupName',
    },
    {
      title: formatMessage({ id: 'form.common.apply.groupCode' }),
      dataIndex: 'groupCode',
    },
    {
      title: formatMessage({ id: 'form.common.apply.attr' }),
      dataIndex: 'subAttrName',
    },
  ];

  const { list, goodsSkuAttrUsed, goodsRfSpuSubAttrInfo, extraDesc } = props;
  const [dataSource, setDataSource] = useState([...list]);
  const [columns, setColumns] = useState([...defaultColumns]);
  const [extraTitle, setExtraTitle] = useState('');

  // 监听并更新属性集变化
  useEffect(() => {
    if (extraDesc) {
      setExtraTitle(`(${extraDesc})`);
    } else {
      setExtraTitle('');
    }
  }, [extraDesc]);
  useEffect(() => {
    setDataSource([...list]);
  }, [list]);
  useEffect(() => {
    const arr = goodsSkuAttrUsed.filter(v => v.attr.key !== goodsRfSpuSubAttrInfo.key);
    const columnsAtrr = arr.map(v => ({
      title: v.attr.label,
      dataIndex: v.attr.key,
      render: text => <span>{text && `${text.name}(${text.code})`}</span>,
    }));
    setColumns([...defaultColumns, ...columnsAtrr]);
  }, [goodsSkuAttrUsed, goodsRfSpuSubAttrInfo]);

  return (
    <Card
      title={`${formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode' })}${extraTitle}`}
      bordered={false}
      headStyle={{ paddingLeft: 0, minHeight: 30 }}
    >
      <Table rowKey="groupCode" pagination={false} columns={columns} dataSource={dataSource} />
    </Card>
  );
}

export default groupList;
