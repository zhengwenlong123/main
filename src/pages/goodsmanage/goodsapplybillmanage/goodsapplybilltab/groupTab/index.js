import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import GroupList from './GroupList';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增、维护的界面商品分组部分组件
 */
class GroupTab extends React.PureComponent {
  groupForm = null;

  getForm = form => {
    this.groupForm = form;
  };

  render() {
    const built = formatMessage({ id: 'form.common.apply.built' });
    const building = formatMessage({ id: 'form.common.apply.building' });
    const {
      goodsRfGroupList, // 新建的商品分组
      goodsRfGroupListValid, // 已生效的商品分组
      // operationType, // 操作类型
      goodsRfInfo, // 申请单信息
    } = this.props;
    const isValid = goodsRfGroupListValid.length > 0;
    const newAddHide = goodsRfInfo && goodsRfInfo.rfTypeCode === 'materiel'; // 物料类型：商品没有新建部分
    return (
      <div className={styles.productCont}>
        {!newAddHide && (
          <GroupList {...this.props} list={goodsRfGroupList} extraDesc={isValid && building} />
        )}
        {isValid && (
          <GroupList {...this.props} list={goodsRfGroupListValid} extraDesc={isValid && built} />
        )}
      </div>
    );
  }
}

export default connect(({ goodsapplybillmanage }) => ({
  goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 申请单信息
  goodsSkuAttrUsed: goodsapplybillmanage.goodsSkuAttrUsed || [], // sku属性(产品tab中sku信息已设置好的属性内容)
  goodsRfSpuSubAttrInfo: goodsapplybillmanage.goodsRfSpuSubAttrInfo || {}, // 商品分组从属属性设定值
  goodsRfGroupList: goodsapplybillmanage.goodsRfGroupList || [], // 商品分组列表数据(新)
  goodsRfGroupListValid: goodsapplybillmanage.goodsRfGroupListValid || [], // 商品分组列表数据(已生效)
}))(GroupTab);
