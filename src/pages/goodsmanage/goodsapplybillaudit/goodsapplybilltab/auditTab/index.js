import React from 'react';
import { connect } from 'dva';
import { Card, Table } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单审核记录部分组件
 */

class AuditTab extends React.PureComponent {
  tableColumns = [
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.account' }),
      dataIndex: 'verifyUserOrgName',
      key: 'verifyUserOrgName',
      align: 'center',
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.company' }),
      dataIndex: 'verifyUserOrgId',
      key: 'verifyUserOrgId',
      align: 'center',
      render: () => <span>主数据</span>,
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.optionTime' }),
      dataIndex: 'verifyCreateTime',
      key: 'verifyCreateTime',
      align: 'center',
      // render: (text, record) => <span>{status[record.brandEnable]}</span>,
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.optionResult' }),
      dataIndex: 'verifyResult',
      key: 'verifyResult',
      align: 'center',
      render: (text, record) => <span>{this.renderStatus(record.verifyResult)}</span>,
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.optionDesc' }),
      dataIndex: 'verifyDesc',
      key: 'verifyDesc',
      align: 'center',
    },
  ];

  /**
   * 默认加载审核历史数据
   */
  componentDidMount() {
    const { goodsRfInfo, dispatch } = this.props;
    // 获取-根据公用参数类型定义代码[goods_rf_type]获取公用参数定义数据所有(产品申请单状态数据)
    dispatch({
      type: 'parmpublicparametermanage/getGoodsRfStatus',
      payload: { parmTypeCode: 'goods_rf_status' },
    });
    if (goodsRfInfo && goodsRfInfo.rfId) {
      dispatch({
        type: 'goodsapplybillaudit/fetchGoodsApplyAuditList',
        payload: { searchCondition: { rfId: goodsRfInfo.rfId }, page: 0, pageSize: 100 },
      });
    }
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log('nextProps', nextProps);
  //   const { goodsApplyBillDetailInfo, dispatch } = this.props;
  //   if (goodsApplyBillDetailInfo !== nextProps.goodsApplyBillDetailInfo) {
  //     const {
  //       goodsApplyBillDetailInfo: { goodsRfInfo = {} },
  //     } = nextProps;
  //     if (goodsRfInfo) {
  //       dispatch({
  //         type: 'goodsapplybillaudit/fetchGoodsApplyAuditList',
  //         payload: { searchCondition: { rfId: goodsRfInfo.rfId }, page: 0, pageSize: 100 },
  //       });
  //     }
  //   }
  // }

  // 渲染申请单的状态
  renderStatus(rfStatus) {
    const {
      parmpublicparametermanage: { parmPublicParameterGoodsRfStatus = [] },
    } = this.props;
    let result = '';
    parmPublicParameterGoodsRfStatus.forEach(val => {
      if (val.parmCode === rfStatus) result = val.parmShowValue;
    });
    return result;
  }

  render() {
    const { goodsApplyAuditList } = this.props;
    return (
      <div className={styles.productCont}>
        <Card
          title={formatMessage({ id: 'form.goodsApplyBillAudit.log' })}
          bordered={false}
          headStyle={{ paddingLeft: 0, minHeight: 30 }}
        >
          <Table
            rowKey={record => record.id}
            dataSource={goodsApplyAuditList.content}
            columns={this.tableColumns}
          />
          ;
        </Card>
      </div>
    );
  }
}

export default connect(
  ({ parmpublicparametermanage, goodsapplybillmanage, goodsapplybillaudit }) => ({
    parmpublicparametermanage,
    goodsApplyBillDetailInfo: goodsapplybillmanage.goodsApplyBillDetailInfo || {}, // 申请单所有信息
    goodsRfSpuInfo: goodsapplybillmanage.goodsRfSpuInfo || {}, // 申请单产品基本信息
    goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 申请单选择类型信息
    goodsApplyAuditList: goodsapplybillaudit.goodsApplyAuditList || {}, // 申请单审核记录
  })
)(AuditTab);
