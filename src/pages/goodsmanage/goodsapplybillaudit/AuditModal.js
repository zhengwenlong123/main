/* eslint-disable react/no-string-refs */
/* eslint-disable no-return-assign */
/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
// import moment from 'moment';
import { message, Button, Form, Input, Select } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import { wrapTid } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品品牌基本信息维护组件
 */
@ValidationFormHoc
class GoodsApplyBillAuditModal extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
  };

  /**
   * 表单提交请求回调
   */
  onCallback = result => {
    const { modalVisibleOnChange, commit, modalOnChangePageCurrent } = this.props;
    if (result) {
      if (modalOnChangePageCurrent) {
        modalOnChangePageCurrent(1);
      }
      if (commit) {
        commit();
      }
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      modalUpkeepEntity,
      currentUser,
      form: { getFieldsValue },
      modalSearchCondition,
      validationForm,
      dispatch,
      dataSource,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    const params = {
      ...values,
      verifyId: currentUser.id,
      rfId: modalUpkeepEntity.rfId,
      verifyUserOrgId: wrapTid().tid,
      verifyUserOrgName: currentUser.name,
    };
    dispatch({
      type: 'goodsapplybillmanage/editApplyBill',
      payload: dataSource,
      callback: () => {
        // message.success(formatMessage({ id: 'form.common.optionSuccess' }));
        // this.handleToList();
        if (modalSearchCondition) {
          dispatch({
            type: 'goodsapplybillaudit/addApplyAudit',
            payload: params,
            callback: this.onCallback,
            searchCondition: modalSearchCondition,
          });
        } else {
          dispatch({
            type: 'goodsapplybillaudit/addApplyAudit',
            payload: params,
            callback: this.onCallback,
          });
        }
      },
    });
  };

  render() {
    const { formItemLayout } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      parmPublicParameterGoodsRfStatus,
    } = this.props;
    const auditTypeList = parmPublicParameterGoodsRfStatus.filter(
      v => v.parmCode === 'verify_adopt' || v.parmCode === 'verify_reject'
    );

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form layout="horizontal" labelAlign="right" {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.goodsApplyBillAudit.result' })}>
              {getFieldDecorator('verifyResult', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsApplyBillAudit.result.placeholder',
                    }),
                  },
                ],
              })(
                // eslint-disable-next-line react/jsx-no-duplicate-props
                <Select style={{ width: 80 }} allowClear style={{ width: '50%' }}>
                  {auditTypeList.map(v => (
                    <Select.Option value={v.parmCode} key={v.id}>
                      {v.parmShowValue}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsApplyBillAudit.desc' })}>
              {getFieldDecorator('verifyDesc')(<Input.TextArea rows={5} />)}
            </Form.Item>
          </Form>
        </div>
        <div>
          <div className={styles.modalHandleBtnArea}>
            <Button type="default" onClick={() => modalVisibleOnChange(false)}>
              {formatMessage({ id: 'button.common.cancel' })}
            </Button>
            <Button type="primary" onClick={this.onClickSubmit}>
              {formatMessage({ id: 'form.goodsApplyBillAudit.audit' })}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ user }) => ({
  currentUser: user.currentUser || {},
}))(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        rfId: Form.createFormField({ value: mue ? mue.rfId : '' }),
        verifyDesc: Form.createFormField({ value: mue ? mue.verifyDesc : '' }),
        verifyResult: Form.createFormField({ value: mue ? mue.verifyResult : '' }),
      };
    },
  })(GoodsApplyBillAuditModal)
);
