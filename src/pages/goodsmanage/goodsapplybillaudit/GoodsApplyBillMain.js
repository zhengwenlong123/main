import React, { Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Row, Col, Affix } from 'antd';
import router from 'umi/router'; // eslint-disable-line
import { formatMessage } from 'umi/locale';
import GoodsApplyBillContent from '../goodsapplybillmanage/GoodsApplyBillContent';
import AuditTab from './goodsapplybilltab/auditTab';
import AuditModal from './AuditModal';
import GoodsApplyBillNoAudit from './GoodsApplyBillNoAudit';
import ModalBasic from '@/components/ModalBasic/ModalBasic';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';

moment.locale('zh-cn');
/**
 * 产品申请单新增、维护的界面
 */
class GoodsApplyBillMain extends MdcManageBasic {
  state = {
    dataSource: {}, // GoodsApplyBillContent返回的申请单数据
  };

  // 保存待审批列表组件的对象
  awaitAuditRef = null;

  /**
   * 默认加载品牌数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-根据公用参数类型定义代码[goods_rf_type]获取公用参数定义数据所有(产品申请单类型)
    dispatch({
      type: 'parmpublicparametermanage/getApplyBillType',
      payload: { parmTypeCode: 'goods_rf_type' },
    });
  }

  handleToList = () => {
    router.push('/goodsmanage/goodsapplybillaudit');
  };

  // 打开审核弹框
  openModel = dataSource => {
    this.modalVisibleOnChange(true);
    this.setState({
      dataSource,
    });
  };

  // 提交审核中，提交相关申请单信息的更新
  submit = () => {
    // const { dataSource } = this.state;
    // const { dispatch } = this.props;
    // // 更新申请单信息
    // dispatch({
    //   type: 'goodsapplybillmanage/editApplyBill',
    //   payload: dataSource,
    //   callback: () => {
    //     message.success(formatMessage({ id: 'form.common.optionSuccess' }));
    //     // this.handleToList();
    //     this.awaitAuditRef.nextData();
    //   },
    // });
    this.awaitAuditRef.nextData();
  };

  render() {
    const { modalVisible, dataSource } = this.state;
    const {
      goodsRfInfo,
      parmPublicParameterApplyBillTypeAll,
      parmPublicParameterGoodsRfStatus,
      location: {
        query: { id = '', type = 'see' },
      },
    } = this.props;
    let title = null;
    if (parmPublicParameterApplyBillTypeAll.length > 0) {
      const findVal = parmPublicParameterApplyBillTypeAll.find(
        v => v.parmCode === goodsRfInfo.rfTypeCode
      );
      const titleDesc = findVal ? findVal.parmValue : '';
      title = (
        <h3>
          {titleDesc}申请单-审核{type === 'see' && '详情'}
        </h3>
      );
    }
    const logTab = [
      {
        key: 'auditTab',
        title: '历史审核记录',
        render: () => <AuditTab />,
      },
    ];
    const modalContentCmp = (
      <AuditModal
        dataSource={dataSource}
        modalUpkeepEntity={goodsRfInfo}
        modalVisibleOnChange={this.modalVisibleOnChange}
        parmPublicParameterGoodsRfStatus={parmPublicParameterGoodsRfStatus}
        commit={this.submit}
      />
    );
    const buttonList =
      type === 'see'
        ? []
        : [{ type: 'submit', label: formatMessage({ id: 'form.goodsApplyBillAudit.audit' }) }];
    return (
      <Fragment>
        {type === 'audit' && (
          <Row gutter={16}>
            <Col xs={{ span: 24 }} md={{ span: 15 }}>
              <GoodsApplyBillContent
                id={id} // 申请单id
                title={title} // 申请单顶部标题
                isFilter
                operationType={type}
                buttonList={buttonList} // 审核
                onToList={this.handleToList} // 当刷新后没有申请id的时候跳转回页面函数
                onSubmit={this.openModel} // 提交按钮返回所有数据(已处理符合接口的数据格式)
                defineTabList={logTab}
              />
            </Col>
            <Col xs={{ span: 24 }} md={{ span: 9 }}>
              <Affix offsetTop={10}>
                <GoodsApplyBillNoAudit
                  id={id}
                  onRef={ref => {
                    this.awaitAuditRef = ref;
                  }}
                />
              </Affix>
            </Col>
          </Row>
        )}
        {type === 'see' && (
          <GoodsApplyBillContent
            id={id} // 申请单id
            title={title} // 申请单顶部标题
            isFilter
            operationType={type}
            buttonList={buttonList} // 审核
            onToList={this.handleToList} // 当刷新后没有申请id的时候跳转回页面函数
            onSubmit={this.openModel} // 提交按钮返回所有数据(已处理符合接口的数据格式)
            defineTabList={logTab}
          />
        )}
        <ModalBasic
          modalTitle={formatMessage({ id: 'form.goodsApplyBillAudit.title' })}
          modalContentCmp={modalContentCmp}
          modalWidth={800}
          modalVisible={modalVisible}
          modalBodyStyle={{ minHeight: 500 }}
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Fragment>
    );
  }
}

export default connect(({ goodsapplybillmanage, parmpublicparametermanage }) => ({
  goodsRfInfo: goodsapplybillmanage.goodsRfInfo || {}, // 申请单信息
  parmPublicParameterApplyBillTypeAll:
    parmpublicparametermanage.parmPublicParameterApplyBillTypeAll || [], // 申请单创建类型
  parmPublicParameterGoodsRfStatus:
    parmpublicparametermanage.parmPublicParameterGoodsRfStatus || [], // 申请单状态
}))(GoodsApplyBillMain);
