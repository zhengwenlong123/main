import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Input, Select, Card } from 'antd';
import moment from 'moment';
// eslint-disable-next-line import/no-extraneous-dependencies
import router from 'umi/router';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import AuditModal from './AuditModal';
import { serverUrl } from '@/defaultSettings';
import { formatDate } from '@/utils/utils';
import styles from './index.less';

moment.locale('zh-cn');
// const status = [
//   formatMessage({ id: 'form.common.disabled' }),
//   formatMessage({ id: 'form.common.enabled' }),
// ];
/**
 * 产品申请单管理组件
 */
class GoodsApplyBillManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'rfName',
        key: 'rfName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.type' }),
        dataIndex: 'rfTypeCode',
        key: 'rfTypeCode',
        align: 'center',
        render: (text, record) => <span>{this.applyBillField(record, 'rfTypeCode')}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.apply.people' }),
        dataIndex: 'rfRequestUserName',
        key: 'rfRequestUserName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.apply.organization' }),
        dataIndex: 'rfRequestUserOrgName',
        key: 'rfRequestUserOrgName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.apply.status' }),
        key: 'rfStatus',
        align: 'center',
        render: (text, record) => <span>{this.applyBillField(record, 'rfStatus')}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.apply.time' }),
        key: 'createAt',
        align: 'center',
        render: (text, record) => (
          <span>{formatDate(new Date(record.createAt), 'yyyy-MM-dd hh:mm:ss')}</span>
        ),
        // render: (text, record) => (
        //   <span>{formatDate(new Date(record.createAt), 'yyyy-MM-dd hh:mm:ss')}</span>
        // ),
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              size="small"
              onClick={() => {
                // 详情跳转至申请单类似tab页面
                const { dispatch } = this.props;
                const payload = {
                  ...record, //
                };
                dispatch({
                  type: 'goodsapplybillaudit/changeSelectGoodsApplyInfo',
                  payload,
                });
                router.push({
                  pathname: '/goodsmanage/goodsapplybillaudit/applyAudit',
                  query: {
                    id: record.rfId,
                    type: 'see',
                  },
                });
              }}
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            {record.rfStatus === 'verify_waiting' ? (
              <span>
                <Divider type="vertical" />
                <a
                  type="primary"
                  size="small"
                  onClick={
                    () => {
                      router.push({
                        pathname: '/goodsmanage/goodsapplybillaudit/applyAudit',
                        query: {
                          id: record.rfId,
                          type: 'audit',
                        },
                      });
                    }
                    // this.onClickActionExecuteEvent(
                    //   10,
                    //   this.hookProcess(1, record),
                    //   formatMessage({ id: 'form.goodsApplyBillAudit.title' })
                    // )
                  }
                >
                  {formatMessage({ id: 'form.goodsApplyBillAudit.audit' })}
                </a>
              </span>
            ) : (
              <span>&nbsp;</span>
            )}
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {
      rfStatus: '',
    };
  }

  applyBillField = (record, field) => {
    const {
      parmpublicparametermanage: {
        parmPublicParameterGoodsRfStatus,
        parmPublicParameterApplyBillTypeAll,
      },
    } = this.props;
    const obj = {
      rfStatus: parmPublicParameterGoodsRfStatus,
      rfTypeCode: parmPublicParameterApplyBillTypeAll,
    };
    const findVal = obj[field].find(v => v.parmCode === record[field]);
    return findVal ? findVal.parmValue : '';
  };

  /**
   * 响应点击处理
   * 进入产品品牌之前获取产品品牌相关联的数据
   */
  hookProcess = (type, record) => {
    // const { dispatch } = this.props;
    const val = { ...record };
    // const values = this.searchContent;
    if (type === 2) {
      router.push({
        pathname: '/goodsmanage/goodsapplybillaudit/applyAudit',
        query: {
          id: record.rfId,
          type: 'edit',
        },
      });
    }
    // if (type === 3) {
    //   val.brandEnable = record.brandEnable === 1 ? 0 : 1; // 如果是启用,则改成停用,如果是停用则改成启用
    // } else {
    //   // 获取-当前选择-品牌描述数据(列表)
    //   dispatch({
    //     type: 'goodsapplybillmanage/paginationGoodsApplyBill',
    //     payload: { brandCode: val.brandCode, page: 1, pageSize: 10, searchCondition: values },
    //   });
    // }
    return val;
  };

  /**
   * 默认加载品牌数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-产品品牌数据(列表,分页)
    dispatch({
      type: 'goodsapplybillaudit/paginationGoodsApplyBillAudit',
      payload: { page: 1, pageSize: 10, searchCondition: {} },
    });
    // 获取-根据公用参数类型定义代码[goods_rf_type]获取公用参数定义数据所有(产品申请单类型)
    dispatch({
      type: 'parmpublicparametermanage/getApplyBillType',
      payload: { parmTypeCode: 'goods_rf_type' },
    });
    // 获取-根据公用参数类型定义代码[goods_rf_type]获取公用参数定义数据所有(产品申请单状态数据)
    dispatch({
      type: 'parmpublicparametermanage/getGoodsRfStatus',
      payload: { parmTypeCode: 'goods_rf_status' },
    });
  }

  /**
   * 根据条件搜索品牌
   */
  onFuzzyGoodsBrandhHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'goodsapplybillaudit/paginationGoodsApplyBillAudit',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
    // 清空搜素框
    // setFieldsValue({
    //   brandCode: '',
    //   brandName: '',
    //   brandEnable: '',
    // });
  }

  /**
   * 导出文件请求
   */
  onExportGoodsBrandFile() {
    const values = this.searchContent;
    const params = {
      ...values,
    };
    const formElement = document.createElement('form');
    formElement.style.display = 'display:none;';
    formElement.method = 'post';
    formElement.action = `${serverUrl}/mdc/goods/goodsbrand/export`;
    formElement.target = 'callBackTarget';
    Object.keys(params).map(item => {
      const inputElement = document.createElement('input');
      inputElement.type = 'hidden';
      inputElement.name = item;
      inputElement.value = params[item] || '';
      formElement.appendChild(inputElement);
      return item;
    });
    document.body.appendChild(formElement);
    formElement.submit();
    document.body.removeChild(formElement);
  }

  tableOperateAreaCmp = () => {
    const {
      form: { getFieldDecorator },
      parmpublicparametermanage: { parmPublicParameterGoodsRfStatus = [] },
    } = this.props;
    const { formItemLayout, tailFormItemLayout } = this.state;
    const ableType = ['verify_adopt', 'verify_waiting', 'verify_reject']; // 可展示的类型: 审核通过、待审核、审核驳回
    return (
      <div className={styles.dropdownOverlayArea}>
        <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
          <Form.Item
            label={formatMessage({ id: 'form.common.name' })}
            className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
          >
            {getFieldDecorator('rfName')(<Input style={{ width: 120 }} allowClear />)}
          </Form.Item>
          <Form.Item
            label={formatMessage({ id: 'form.common.enable' })}
            className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
          >
            {getFieldDecorator('rfStatus')(
              <Select style={{ width: 120 }} allowClear>
                {parmPublicParameterGoodsRfStatus
                  .filter(i => ableType.includes(i.parmCode))
                  .map(v => (
                    <Select.Option value={v.parmCode} key={v.id}>
                      {v.parmShowValue}
                    </Select.Option>
                  ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item
            {...tailFormItemLayout}
            className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
          >
            <Button onClick={() => this.onFuzzyGoodsBrandhHandle()}>
              {formatMessage({ id: 'button.common.query' })}
            </Button>
          </Form.Item>
        </Form>
        {/* <Button
          type="primary"
          style={{ marginTop: 4 }}
          onClick={() =>
            this.onClickActionExecuteEvent(
              10,
              null,
              formatMessage({ id: 'button.goodsApplyBillManage.add' })
            )
          }
        >
          {formatMessage({ id: 'button.common.add' })}
        </Button> */}
      </div>
    );
  };

  render() {
    const {
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
    } = this.state;
    const {
      goodsApplyBillAuditList,
      // goodsBrandList,
      goodsApplyBillAuditListPageSize,
      parmpublicparametermanage: { parmPublicParameterGoodsRfStatus = [] },
    } = this.props;

    const tablePaginationOnChangeEventDispatchType =
      'goodsapplybillaudit/paginationGoodsApplyBillAudit';
    const modalContentCmp = (
      <AuditModal
        modalUpkeepType={modalUpkeepType}
        modalUpkeepEntity={modalUpkeepEntity}
        modalVisibleOnChange={this.modalVisibleOnChange}
        modalOnChangePageCurrent={this.changeCurrentPage}
        parmPublicParameterGoodsRfStatus={parmPublicParameterGoodsRfStatus}
        modalSearchCondition={this.searchContent}
      />
    );
    return (
      <Card bordered={false}>
        <div>
          <MdcManageBlock
            tableTitle={formatMessage({ id: 'menu.goodsmanage.goodsapplybillaudit' })}
            tableColumns={tableColumns}
            tableDataSource={goodsApplyBillAuditList === undefined ? [] : goodsApplyBillAuditList}
            tableOperateAreaCmp={this.tableOperateAreaCmp()}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            tableTotalSize={goodsApplyBillAuditListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
            modalTitle={formatMessage({ id: 'menu.goodsmanage.goodsapplybillmanage.add' })}
            modalWidth={800}
            modalVisible={modalVisible}
            modalBodyStyle={{ minHeight: 500 }}
            modalContentCmp={modalContentCmp}
            modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ parmpublicparametermanage, goodsapplybillaudit }) => ({
  goodsApplyBillAuditListPageSize: goodsapplybillaudit.goodsApplyBillAuditListPageSize || 0,
  parmpublicparametermanage,
  goodsApplyBillAuditList: goodsapplybillaudit.goodsApplyBillAuditList || [],
}))(Form.create()(GoodsApplyBillManage));
