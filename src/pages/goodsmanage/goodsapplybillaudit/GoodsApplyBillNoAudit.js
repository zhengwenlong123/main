import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Card, Modal } from 'antd';
// eslint-disable-next-line import/no-extraneous-dependencies
import router from 'umi/router';
import { formatMessage } from 'umi/locale';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单审核列表
 */
class GoodsApplyBillNoAudit extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'rfName',
        key: 'rfName',
        align: 'left',
      },
      {
        title: formatMessage({ id: 'form.common.type' }),
        dataIndex: 'rfTypeCode',
        key: 'rfTypeCode',
        align: 'left',
        render: (text, record) => <span>{this.applyBillField(record, 'rfTypeCode')}</span>,
      },
    ];
    super(props, tableColumns);
    this.searchContent = {
      rfStatus: 'verify_waiting', // 待审核
    };
  }

  componentDidMount() {
    const { dispatch, onRef } = this.props;
    onRef(this);
    this.refresh();
    // 获取-根据公用参数类型定义代码[goods_rf_type]获取公用参数定义数据所有(产品申请单类型)
    dispatch({
      type: 'parmpublicparametermanage/getApplyBillType',
      payload: { parmTypeCode: 'goods_rf_type' },
    });
  }

  applyBillField = (record, field) => {
    const {
      parmpublicparametermanage: {
        parmPublicParameterGoodsRfStatus,
        parmPublicParameterApplyBillTypeAll,
      },
    } = this.props;
    const obj = {
      rfStatus: parmPublicParameterGoodsRfStatus,
      rfTypeCode: parmPublicParameterApplyBillTypeAll,
    };
    const findVal = obj[field].find(v => v.parmCode === record[field]);
    return findVal ? findVal.parmValue : '';
  };

  // 刷新
  refresh = currentPage => {
    const { dispatch } = this.props;
    const page = currentPage || 1;
    // 获取-产品品牌数据(列表,分页)
    dispatch({
      type: 'goodsapplybillaudit/paginationGoodsApplyBillAwaitAudit',
      payload: { page, pageSize: 10, searchCondition: { ...this.searchContent } },
    });
  };

  countDown = (defaultSeconds, callback) => {
    let secondsToGo = defaultSeconds;
    const content = formatMessage({ id: 'form.goodsApplyBillManage.message.seconds.list' });
    const modal = Modal.success({
      title: formatMessage({ id: 'form.goodsApplyBillManage.message.handle.awaitAudit' }),
      content: `${secondsToGo}${content}.`,
    });
    const timer = setInterval(() => {
      secondsToGo -= 1;
      modal.update({
        content: `${secondsToGo}${content}.`,
      });
    }, 1000);
    setTimeout(() => {
      clearInterval(timer);
      modal.destroy();
      callback();
    }, secondsToGo * 1000);
  };

  // 审批通过后进行下个选中
  nextData = () => {
    const { tableCurrentPage } = this.state;
    const { goodsApplyBillAwaitAuditList, id, goodsApplyBillAwaitAuditListPageSize } = this.props;
    if (goodsApplyBillAwaitAuditListPageSize < 2) {
      // 只有一条待审数据，审核完3秒后就跳转回列表页
      this.countDown(3, () => {
        router.push('/goodsmanage/goodsapplybillaudit');
      });
      return;
    }
    const findIndex = goodsApplyBillAwaitAuditList.findIndex(v => v.rfId === id);
    const index = findIndex === -1 ? 0 : findIndex + 1;
    const record = goodsApplyBillAwaitAuditList[index];
    this.refresh(tableCurrentPage);
    router.push({
      pathname: '/goodsmanage/goodsapplybillaudit/applyAudit',
      query: {
        id: record.rfId,
        type: 'audit',
      },
    });
  };

  render() {
    const { goodsApplyBillAwaitAuditList, goodsApplyBillAwaitAuditListPageSize, id } = this.props;
    const { tableColumns, tableCurrentPage } = this.state;
    const tablePaginationOnChangeEventDispatchType =
      'goodsapplybillaudit/paginationGoodsApplyBillAwaitAudit';
    return (
      <Card
        title={formatMessage({ id: 'form.goodsApplyBillManage.awaitAudit.list' })}
        extra={
          <a onClick={() => this.refresh()}>{formatMessage({ id: 'button.common.refresh' })}</a>
        }
      >
        <div className={styles.goodsApplyBillNoAuditTable}>
          <MdcManageBlock
            hideHeader
            tableColumns={tableColumns}
            tableDataSource={
              goodsApplyBillAwaitAuditList === undefined ? [] : goodsApplyBillAwaitAuditList
            }
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            tableTotalSize={goodsApplyBillAwaitAuditListPageSize}
            rowClassName={record => {
              if (record.rfId === id) {
                return styles.rowActive;
              }
              return '';
            }}
            onRow={record => {
              return {
                onClick: () => {
                  router.push({
                    pathname: '/goodsmanage/goodsapplybillaudit/applyAudit',
                    query: {
                      id: record.rfId,
                      type: 'audit',
                    },
                  });
                },
              };
            }}
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ parmpublicparametermanage, goodsapplybillaudit }) => ({
  goodsApplyBillAwaitAuditListPageSize:
    goodsapplybillaudit.goodsApplyBillAwaitAuditListPageSize || 0,
  parmpublicparametermanage,
  goodsApplyBillAwaitAuditList: goodsapplybillaudit.goodsApplyBillAwaitAuditList || [],
}))(GoodsApplyBillNoAudit);
