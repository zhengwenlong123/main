/* eslint-disable no-nested-ternary */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Divider, message } from 'antd';
import moment from 'moment';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import GoodsMaterielInfoUpkeep from './GoodsMaterielInfoUpkeep';
// import GoodsSkuDescUpkeep from './GoodsSkuDescUpkeep';
import EnclosureTableBasic from '@/components/Enclosure/EnclosureTableBasic'; // 附件描述维护统一使用此组件封装好了

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 商品信息维护[抽象类]
 */
class GoodsSkuInfoUpkeepBasic extends MdcManageBasic {
  constructor(props) {
    super(props);
    // state
    this.state = {
      ...this.state,
      // Table行勾选数据对象
      selectedRowKeys: [],
      tableRowSelectionEntity: null,
    };
    // Table-表格行勾选处理
    // this.tableRowSelection = {
    //   type: 'radio',
    //   columnWidth: 30,
    //   columnTitle: '<>',
    //   selectedRowKeys: [],
    //   onChange: (selectedRowKeys, selectedRows) => {
    //     this.tableRowSelection.selectedRowKeys = selectedRowKeys;
    //     this.setState({ tableRowSelectionEntity: selectedRows[0] });
    //   }, // Table勾选回调事件
    // };
    // listType为列表类型,值仅为: 1为图片; 2为文件; 3为视频;
    // actionType为列表行操作类型; item为列表行操作数据
    // this.listRowSelectionEvent = (listType, actionType, item) => {
    //   // 详情
    //   if (actionType === 1) this.onClickActionExecuteEvent(10);
    //   // 维护
    //   if (actionType === 2) this.onClickActionExecuteEvent(2, item);
    //   // 新增
    //   if (actionType === 10) this.onClickActionExecuteEvent(10, null);
    //   // 删除
    //   if (actionType === 20) this._deleteListRowSelection(listType, item);
    // };

    // 商品(单品)商品物料信息-表格列表定义字段
    this.tableColumns_GoodsMaterielInfo = [
      {
        title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.materielCode' }),
        dataIndex: 'materielCode',
        key: 'materielCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.materielName' }),
        dataIndex: 'materielName',
        key: 'materielName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.materielShortName' }),
        dataIndex: 'materielShortName',
        key: 'materielShortName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.modelName' }),
        dataIndex: 'modelName',
        key: 'modelCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'materielEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.materielEnable]}</span>,
      },
      // { title: '序号', dataIndex: 'seq', key: 'seq', align: 'center' },
      // { title: '物料简介', dataIndex: 'materielDesc', key: 'materielDesc', align: 'center' },
      // { title: '产品代码', dataIndex: 'spuCode', key: 'spuCode', align: 'center' },
      // { title: '商品代码', dataIndex: 'skuCode', key: 'skuCode', align: 'center' },
      // { title: '商品分组代码', dataIndex: 'groupCode', key: 'groupCode', align: 'center' },
      // { title: '型号代码', dataIndex: 'modelCode', key: 'modelCode', align: 'center' },
    ];
    // 商品描述-表格列表字段定义
    this.tableColumns_GoodsSkuDesc = [
      {
        title: formatMessage({ id: 'form.common.rowno' }),
        dataIndex: 'seq',
        key: 'seq',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.spuName' }),
        dataIndex: 'spuName',
        key: 'spuName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuName' }),
        dataIndex: 'skuName',
        key: 'skuName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.descriptionType' }),
        dataIndex: 'descTypeValue',
        key: 'descTypeValue',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.descriptionName' }),
        dataIndex: 'descName',
        key: 'descName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'descEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.descEnable]}</span>,
      },
      // { title: '存放地址', dataIndex: 'address', key: 'address', align: 'center' },
      // { title: '描述备注', dataIndex: 'descNotes', key: 'descNotes', align: 'center' },
    ];
  }

  /**
   * 渲染Table组件-商品(单品)商品物料信息
   */
  renderGoodsMaterielInfoCmpTable = (mue, mut) => {
    const {
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableCurrentPage,
      selectedRowKeys,
    } = this.state;
    const { goodsMaterielInfoList: gmil, goodsMaterielInfoListPageSize: gmilps } = this.props;
    const dispatchType = 'goodsskuinfomanage/updateGoodsMaterielInfo'; // 停用dispatchType
    const tablePaginationOnChangeEventDispatchType =
      'goodsskuinfomanage/paginationGoodsMaterielInfo';
    const tablePaginationOnChangeEventCondition = { spuCode: mue.spuCode, skuCode: mue.skuCode };
    return (
      <MdcManageBlock
        tableTitle={`${formatMessage({ id: 'form.goodsSkuInfoUpkeepBasic.sku' })}：${mue.skuCode}+${
          mue.skuName
        }`}
        tableColumns={this.tableColumns_GoodsMaterielInfo}
        tableDataSource={gmil}
        tableTotalSize={gmilps}
        tableOperateAreaCmp={this._tableOperateAreaCmp(3, mut, dispatchType)}
        tableCurrentPage={tableCurrentPage}
        tablePaginationOnChangeEvent={(page, pageSize) =>
          this.tablePaginationOnChangeEvent(
            page,
            pageSize,
            tablePaginationOnChangeEventDispatchType,
            tablePaginationOnChangeEventCondition
          )
        }
        tableRowSelection={{
          columnWidth: 30,
          columnTitle: '<>',
          selectedRowKeys,
          onSelect: (record, selected, selectedRows) => {
            if (selected) {
              this.setState({ tableRowSelectionEntity: record });
            } else if (selectedRows.length === 0) {
              this.setState({ tableRowSelectionEntity: null });
            }
          },
          onChange: selectedRowKeysArr => {
            this.setState({
              selectedRowKeys:
                selectedRowKeysArr.length > 1 ? [selectedRowKeysArr[1]] : selectedRowKeysArr,
            });
          }, // Table勾选回调事件
        }}
        modalVisible={modalVisible}
        modalTitle={formatMessage({ id: 'menu.goodsmanage.goodsskuinfomanage.skuMateriel' })}
        modalContentCmp={
          <GoodsMaterielInfoUpkeep
            originGoodsSkuInfo={mue}
            modalUpkeepType={modalUpkeepType}
            modalUpkeepEntity={modalUpkeepEntity}
            modalVisibleOnChange={this.modalVisibleOnChange}
            modalOnChangePageCurrent={this.changeCurrentPage}
          />
        }
        modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
      />
    );
  };

  /**
   * 渲染Table组件-商品描述
   * @param {array} mue 商品描述资源数据
   * @returns 商品描述维护组件
   */
  renderGoodsSkuDescCmpTable = (mue, mut) => {
    const {
      goodsSkuDescList: gsdl, // 当前选择-商品描述(列表)
      goodsSkuDescListPageSize: gsdlps, // 当前选择-商品描述(列表)总数量
      parmPublicParameterOfGoodsDescTypeAll: pppogdta, // 描述类型数据(所有)
    } = this.props;

    const enclosureOnDeleteEventDispatchType = 'goodsskuinfomanage/deleteGoodsSkuDesc'; // 附件删除dispathc的type值
    const enclosureOnAddEventDispatchType = 'goodsskuinfomanage/addGoodsSkuDesc'; // 附件添加dispatch的type值
    const enclosureOnUpdateEventDispatchType = 'goodsskuinfomanage/updateGoodsSkuDesc'; // 附件更新dispatch的type值
    const enclosureTablePaginationOnChangeEventDispatchType =
      'goodsskuinfomanage/paginationGoodsSkuDesc'; // 产品描述列表分页dispatch的type值
    const enclosureTablePaginationOnChangeEventCondition = {
      spuCode: mue.spuCode,
      skuCode: mue.skuCode,
    }; // 附件列表分页附带的参数,是object类型

    return (
      <EnclosureTableBasic
        actionType={mut} // 附件列表-当前操作类型,来源于父级组件的定义值, 1为详情;2为修改;3为停用;10为新增;20为删除
        enclosureOperateSign="GoodsSkuDesc" // 附件当前操作标识
        enclosureTableTitle={`${formatMessage({ id: 'form.goodsSkuInfoUpkeepBasic.sku' })}：${
          mue.skuCode
        }+${mue.skuName}`} // 附件列表标题
        enclosureAssociatedObject={mue} // 附件关联的对象(当前操作父级对象)
        enclosureTableColumnsField={this.tableColumns_GoodsSkuDesc} // 附件列表字段定义
        enclosureTablePaginationOnChangeEventDispatchType={
          enclosureTablePaginationOnChangeEventDispatchType
        } // 附件列表分页对应dispatch的type
        enclosureTablePaginationOnChangeEventCondition={
          enclosureTablePaginationOnChangeEventCondition
        } // 附件列表分页附带的参数,是object类型
        enclosureTableDataSource={gsdl} // 附件列表源数据
        enclosureTableTotalSize={gsdlps} // 附件列表总数据量
        enclosureOnDeleteEventDispatchType={enclosureOnDeleteEventDispatchType} // 附件删除dispathc的type值
        enclosureOnAddEventDispatchType={enclosureOnAddEventDispatchType} // 附件添加dispatch的type值
        enclosureOnUpdateEventDispatchType={enclosureOnUpdateEventDispatchType} // 附件更新dispatch的type值
        enclosureDescTypeAll={pppogdta} // 附件描述类型数据所有
      />
    );
  };

  render() {
    const { upkeepBasicFlag, modalUpkeepType: mut, modalUpkeepEntity: mue } = this.props;
    let upkeepBasicCmp;

    switch (upkeepBasicFlag) {
      case 1: // 商品(单品)商品描述维护组件
        upkeepBasicCmp = this.renderGoodsSkuDescCmpTable(mue, mut);
        break;
      case 2: // 商品(单品)商品物料维护组件
        upkeepBasicCmp = this.renderGoodsMaterielInfoCmpTable(mue, mut);
        break;
      default:
        upkeepBasicCmp = null;
        break;
    }

    return upkeepBasicCmp;
  }

  /**
   * Table-表格头部动作处理组件
   * @param {number} typeFlag 标识码; 1为商品普通属性;2为商品SKU属性;3为商品物料
   * @param {number} actionType 动作类型
   * @param {string} dispatchType 动作对应dispatch的type值
   */
  _tableOperateAreaCmp = (typeFlag, actionType, dispatchType = null) => {
    const { tableRowSelectionEntity: trse } = this.state;

    // 校验Table勾选tableRowSelectionEntity值是否存在
    const checkTableRowSelectionEntity = (flag = -1) => {
      if (trse == null || trse === undefined) {
        const tips =
          flag === 2
            ? formatMessage({ id: 'button.common.modify' })
            : flag === 20
            ? formatMessage({ id: 'button.common.delete' })
            : formatMessage({ id: 'button.common.handle' });
        message.warn(
          `${formatMessage({ id: 'form.goodsSkuInfoUpkeepBasic.pleaseSelectData' })}[${tips}]`
        );
        return;
      }
      if (flag === 1) {
        // 详情动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 2) {
        // 维护动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 20) {
        // 删除动作
        this._deleteTableRowSelection(typeFlag, trse);
      }
      if (flag === 3) {
        // 停用动作
        // 设置值为0,表示停用
        Object.keys(trse)
          .filter(item => ['attrEnable', 'skuAttrEnable', 'materielEnable'].includes(item))
          .forEach(item => {
            trse[item] = 0;
          });
        this.onClickActionExecuteEvent(flag, trse, null, dispatchType);
      }
    };

    return actionType === 1 ? (
      <a type="primary" onClick={() => checkTableRowSelectionEntity(1)}>
        {formatMessage({ id: 'button.common.details' })}
      </a>
    ) : (
      <div>
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(1);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.details' })}
        </a>
        <Divider type="vertical" />
        <a type="primary" onClick={() => this.onClickActionExecuteEvent(10)}>
          {formatMessage({ id: 'button.common.add' })}
        </a>
        <Divider type="vertical" />
        {typeFlag === 2 ? null : (
          <span>
            <a
              type="primary"
              onClick={() => {
                checkTableRowSelectionEntity(2);
                this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
              }}
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            {/* <Divider type="vertical" /> */}
          </span>
        )}
        {/* <a type="primary" onClick={() => checkTableRowSelectionEntity(20)}>删除</a> */}
        {/* <a type="primary" onClick={() => checkTableRowSelectionEntity(3)}>停用</a> */}
      </div>
    );
  };

  /**
   * Table-删除行勾选数据
   * @param {number} type 当前操作Tabs类别
   * @param {object} item 处理数据对象
   */
  _deleteTableRowSelection = (type, item) => {
    const { dispatch } = this.props;
    switch (type) {
      case 3: // 商品(单品)商品物料信息
        dispatch({ type: 'goodsskuinfomanage/deleteGoodsMaterielInfo', payload: { id: item.id } });
        break;
      default:
        break;
    }
  };

  // /**
  //  * List-删除行勾选数据
  //  * @param {number} listType listType为列表类型,值仅为: 1为图片; 2为文件; 3为视频;
  //  * @param {object} item 处理数据对象
  //  */
  // _deleteListRowSelection = (listType, item) => {
  //   const { dispatch } = this.props;
  //   dispatch({ type: 'goodsskuinfomanage/deleteGoodsSkuDesc', payload: item });
  // };
}

export default connect(({ goodsskuinfomanage, parmpublicparametermanage }) => ({
  goodsSkuDescList: goodsskuinfomanage.goodsSkuDescList || [], // 当前选择-商品描述(列表)
  goodsSkuDescListPageSize: goodsskuinfomanage.goodsSkuDescListPageSize || 0, // 当前选择-商品描述(列表)总数量
  goodsMaterielInfoList: goodsskuinfomanage.goodsMaterielInfoList || [], // 当前选择-商品(单品)商品物料(列表)
  goodsMaterielInfoListPageSize: goodsskuinfomanage.goodsMaterielInfoListPageSize || 0, // 当前选择-商品(单品)商品物料(列表)总数量
  parmPublicParameterOfGoodsDescTypeAll:
    parmpublicparametermanage.parmPublicParameterOfGoodsDescTypeAll || [], // 描述类型数据所有
}))(GoodsSkuInfoUpkeepBasic);
