/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-expressions */
import React, { PureComponent } from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Form, Input, Checkbox, Select, message, InputNumber } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import { execOneRequest } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 商品(单品)物料信息维护组件
 */
@ValidationFormHoc
class GoodsMaterielInfoUpkeep extends PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      originGoodsSkuInfo: ogsi, // 原始商品信息数据
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    values.materielEnable ? (values.materielEnable = 1) : (values.materielEnable = 0);
    values.id = mue ? mue.id || '' : '';
    values.spuCode = ogsi.spuCode;
    values.skuCode = ogsi.skuCode;
    values.groupCode = ogsi.groupCode;

    // 在新增/维护时商品或产品型号有改变时需要验证商品物料数据具备唯一性
    if (mut === 10 || mue.skuCode !== values.skuCode || mue.modelCode !== values.modelCode) {
      const verifyResult = await execOneRequest(
        '/mdc/goods/goodsmaterielinfo/verifyGoodsMaterielInfoOnlyOne',
        { spuCode: values.spuCode, skuCode: values.skuCode, modelCode: values.modelCode || '' }
      );
      if (verifyResult) {
        if (verifyResult.status) {
          // 验证通过
        } else {
          message.error(verifyResult.message, 5);
          return;
        }
      } else {
        message.warn(formatMessage({ id: 'form.goodsMaterielInfoUpkeep.verifyResult-conformity' }));
        return;
      }
    }
    this.setState({ confirmLoading: true });

    // 维护
    if (mut === 2) {
      dispatch({
        type: 'goodsskuinfomanage/updateGoodsMaterielInfo',
        payload: values,
        callback: this.onCallback,
      });
    }
    // 新增
    if (mut === 10) {
      dispatch({
        type: 'goodsskuinfomanage/addGoodsMaterielInfo',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
      // currentSpuCondOfGoodsSkuGroupList: cscogsgl, // 当前产品条件-对应的商品分组数据(列表)
      currentSpuCondOfGoodsSpuModelList: cscogsml, // 当前产品条件-对应的产品型号数据(列表)
    } = this.props;

    return (
      <div>
        <div>
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.goodsMaterielInfoUpkeep.spuInfo' })}>
              {getFieldDecorator('spuInfo', { rules: [{ required: true }] })(
                <Input className={styles.inputTextStyle} disabled />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsMaterielInfoUpkeep.skuInfo' })}>
              {getFieldDecorator('skuInfo', { rules: [{ required: true }] })(
                <Input className={styles.inputTextStyle} disabled />
              )}
            </Form.Item>
            {mut === 10 ? null : (
              <Form.Item label={formatMessage({ id: 'form.goodsMaterielInfoUpkeep.materielCode' })}>
                {getFieldDecorator('materielCode', { rules: [{ required: true }] })(
                  <Input disabled />
                )}
              </Form.Item>
            )}
            <Form.Item label={formatMessage({ id: 'form.goodsMaterielInfoUpkeep.materielName' })}>
              {getFieldDecorator('materielName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsMaterielInfoUpkeep.materielName.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsMaterielInfoUpkeep.materielShortName' })}
            >
              {getFieldDecorator('materielShortName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsMaterielInfoUpkeep.materielShortName.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsMaterielInfoUpkeep.materielDesc' })}>
              {getFieldDecorator('materielDesc')(<Input.TextArea disabled={mut === 1} rows={3} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsMaterielInfoUpkeep.modelCode' })}>
              {getFieldDecorator('modelCode')(
                <Select allowClear disabled={mut === 1}>
                  {cscogsml.map(item => (
                    <Select.Option key={item.modelCode} value={item.modelCode}>
                      {item.modelName}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            {/* <Form.Item label="商品分组">
              {getFieldDecorator('groupCode')(
                <Select allowClear disabled={mut === 1}>
                  {cscogsgl.map(item => (<Select.Option key={item.groupCode} value={item.groupCode}>{item.groupName}</Select.Option>))}
                </Select>
              )}
              </Form.Item> */}
            <Form.Item label={formatMessage({ id: 'form.common.rowno' })}>
              {getFieldDecorator('seq')(
                <InputNumber min={1} max={10000} defaultChecked={1} disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.series' })}>
              {getFieldDecorator('series', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.common.series.placeholder',
                    }),
                  },
                ],
              })(<Input maxLength={50} disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('materielEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ goodsskuinfomanage }) => ({
  // currentSpuCondOfGoodsSkuGroupList: goodsskuinfomanage.currentSpuCondOfGoodsSkuGroupList || [], // 当前产品条件-对应的商品分组数据(列表)
  currentSpuCondOfGoodsSpuModelList: goodsskuinfomanage.currentSpuCondOfGoodsSpuModelList || [], // 当前产品条件-对应的产品型号数据(列表)
}))(
  Form.create({
    mapPropsToFields(props) {
      const {
        modalUpkeepEntity: mue,
        originGoodsSkuInfo: ogsi, // 当前操作的商品原始数据
      } = props;
      return {
        spuInfo: Form.createFormField({
          value: ogsi ? `${ogsi.spuCode}+${ogsi.spuCodeVO.spuName}` : '',
        }),
        skuInfo: Form.createFormField({ value: ogsi ? `${ogsi.skuCode}+${ogsi.skuName}` : '' }),
        modelCode: Form.createFormField({ value: mue ? mue.modelCode : '' }),
        // groupCode: Form.createFormField({ value: mue ? mue.groupCode : '' }),
        materielCode: Form.createFormField({ value: mue ? mue.materielCode : '' }),
        materielName: Form.createFormField({ value: mue ? mue.materielName : '' }),
        materielShortName: Form.createFormField({ value: mue ? mue.materielShortName : '' }),
        materielDesc: Form.createFormField({ value: mue ? mue.materielDesc : '' }),
        seq: Form.createFormField({ value: mue ? mue.seq : 1 }),
        series: Form.createFormField({ value: mue ? mue.series : '' }),
        materielEnable: Form.createFormField({ value: mue ? mue.materielEnable === 1 : true }),
      };
    },
  })(GoodsMaterielInfoUpkeep)
);
