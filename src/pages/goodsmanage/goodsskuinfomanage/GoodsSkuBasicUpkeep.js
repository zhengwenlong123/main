/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import moment from 'moment';
import { message, Button, Form, Select, Input, InputNumber, Checkbox } from 'antd';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import { execOneRequest, wrapTid } from '@/utils/mdcutil'; // eslint-disable-line
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 商品(单品)基本信息维护组件
 */
@ValidationFormHoc
class GoodsSkuBasicUpkeep extends React.PureComponent {
  constructor(props) {
    super(props);
    // form表单项布局
    this.formItemLayout = {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    };
    this.confirmLoading = false; // 提交等待
  }

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.confirmLoading = false; // 提交等待
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      dispatch,
      validationForm,
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      goodsSkuAttrSkuList: gsasl, // 商品sku属性数据(列表)
      goodsSkuAttrNormalList: gsanl, // 商品普通属性数据(列表)
      goodsSkuAttrSkuUnderSkuGroupList: gsasusgl, // 基于商品分组已关联商品的商品sku属性数据(列表)
      parmPublicParameterOfGoodsUnitOfQuantityAll: pppouogqa, // 商品(单品)数量单位数据(所有)(对应公用参数定义表数据)
      currentCondSelectEffectiveSpu: ccses, // 当前条件选择的有效产品信息数据(对象)
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.confirmLoading = true; // 提交等待
    const values = getFieldsValue();
    values.spuCode = ccses.spuCode;
    values.skuEnable ? (values.skuEnable = 1) : (values.skuEnable = 0);
    values.unitOfQuantityName = this.parseOnlyUnitOfQuantityName(
      pppouogqa,
      values.unitOfQuantityCode
    );

    // 验证商品sku属性数据
    const goodsSkuAttrSkuArrays = []; // 当前新增的商品sku属性及属性项数据
    if (mut === 10) {
      // 仅在新增时处理商品sku属性
      let skuAttrSkuData = []; // 空数组
      if (gsasusgl && gsasusgl.length > 0) {
        skuAttrSkuData = gsasusgl; // 有商品分组数据时执行if(商品分组对应的商品sku属性数据存在)
      } else {
        skuAttrSkuData = gsasl; // 无选择商品分组时执行else
      }
      skuAttrSkuData.forEach(item => {
        if (item.skuAttrItemList) {
          // 存在下拉选择项
          const skuItemCode = values[`${item.skuAttrCode}`]; // 当前选择的sku属性项代码
          const skuItemValueList = item.skuAttrItemList.filter(
            tmp => tmp.skuAttrItemCode === skuItemCode
          );
          if (skuItemCode === null || skuItemCode === undefined || skuItemValueList.length === 0) {
            message.error(`请选择商品sku属性项${item.skuAttrName}的值后再提交操作`, 8);
            return;
          }
          const skuItemValue = skuItemValueList[0].skuAttrItemValue; // sku属性项值
          goodsSkuAttrSkuArrays.push({
            id: item.id || '',
            spuCode: ccses.spuCode,
            skuCode: mue ? mue.skuCode : '',
            skuAttrCode: item.skuAttrCode,
            skuAttrName: item.skuAttrName,
            skuAttrDesc: item.skuAttrDesc,
            skuAttrEnable: 1, // 默认启用
            skuAttrItemCode: skuItemCode, // sku属性项代码
            skuAttrItemValue: skuItemValue, // sku属性项值
          });
        } else {
          // 无下拉选择
          goodsSkuAttrSkuArrays.push(item);
        }
      });
    }
    values.goodsSkuAttrSku = goodsSkuAttrSkuArrays;

    // 验证商品普通属性数据
    const goodsSkuAttrNormalArrays = []; // 当前新增的商品普通属性数据
    gsanl.forEach(item => {
      const newValue = values[`${item.attrCode}`];
      if (newValue === null || newValue === undefined || newValue === '') {
        message.error(`填写商品sku普通属性[${item.attrName}]的值后再提交操作`, 8);
        return;
      }
      goodsSkuAttrNormalArrays.push({
        id: item.id || '',
        spuCode: ccses.spuCode,
        skuCode: mue ? mue.skuCode : '',
        attrCode: item.attrCode,
        attrName: item.attrName,
        attrValue: newValue,
        attrDesc: item.attrDesc,
        attrEnable: 1, // 默认启用
        attrRefType: newValue === item.attrValue ? 0 : 1, // 属性引用类型,当新值与旧值不一致时为1,一致则为0
      });
    });
    values.goodsSkuAttrNormal = goodsSkuAttrNormalArrays;

    if (mut === 2) {
      // 维护
      values.id = mue.id;
      values.skuCode = mue.skuCode;
      dispatch({
        type: 'goodsskuinfomanage/updateGoodsSkuInfo',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (mut === 10) {
      // 新增
      // 验证同一款产品下, 商品的sku属性及属性项值数据具备唯一性,否则不能创建新商品
      // const verifyCond = values.goodsSkuAttrSku.map(item => {
      //   const obj = {};
      //   obj.spuCode = item.spuCode;
      //   obj.skuAttrCode = item.skuAttrCode;
      //   obj.skuAttrName = item.skuAttrName;
      //   obj.skuAttrItemCode = item.skuAttrItemCode;
      //   obj.skuAttrItemValue = item.skuAttrItemValue;
      //   return wrapTid(obj);
      // });
      // const verifyResult = await execOneRequest(
      //   '/mdc/goods/goodsskuattrsku/verifyGoodsSkuAttrSkuOnlyOne',
      //   verifyCond
      // );
      // if (verifyResult) {
      //   if (verifyResult.status) {
      //     dispatch({
      //       type: 'goodsskuinfomanage/addGoodsSkuInfo',
      //       payload: values,
      //       callback: this.onCallback,
      //     });
      //   } else {
      //     message.error(verifyResult.message, 5);
      //   }
      // } else {
      //   message.warn('新增商品时验证SKU属性及属性项值唯一性失败,请核实处理');
      // }
      dispatch({
        type: 'goodsskuinfomanage/addGoodsSkuAllInfo',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  /**
   * 根据数量单位代码查找数量单位名称值返回
   * @param {array} refer // 商品(单品)数量单位数据(所有)(对应公用参数定义表数据)
   * @param {string} code 待匹配数量单位代码
   */
  parseOnlyUnitOfQuantityName = (refer, code) => {
    if (refer === undefined || code === undefined) return '';
    const tempArr = refer.filter(item => item.parmCode === code);
    if (tempArr === null || tempArr.length === 0) return '';
    return tempArr[0].parmValue;
  };

  /**
   * 新增-渲染[商品sku属性]form组件,[属性项值下拉]选择
   */
  renderAddGoodsSkuAttrSkuFormCmp = () => {
    const {
      form: { getFieldDecorator },
      modalUpkeepType: mut, // 当前modal状态类型,1为详情、2为修改、10为新增、20为删除
      goodsSkuAttrSkuList: gsasl, // 商品sku属性数据(列表)
      goodsSkuAttrSkuUnderSkuGroupList: gsasusgl, // 基于商品分组已关联商品的商品sku属性数据(列表)
    } = this.props;

    // 如果gsasusgl有值则展示gsasusgl的数据,如果没有则再展示gsasl的数据
    // gsasusgl数据来源于当前选择的商品分组获取到的sku属性数据
    let skuAttrSkuData = [];
    if (mut === 10 && gsasusgl && gsasusgl.length > 0) {
      skuAttrSkuData = gsasusgl;
    } else {
      skuAttrSkuData = gsasl;
    }
    return (
      <Form {...this.formItemLayout}>
        {skuAttrSkuData.map(item =>
          item.skuAttrItemList ? ( // 有下拉值可选取
            <Form.Item key={item.skuAttrCode} label={item.skuAttrName}>
              {getFieldDecorator(`${item.skuAttrCode}`, {
                rules: [
                  {
                    required: true,
                    message: `${formatMessage({ id: 'form.goodsSkuBasicUpkeep.pleaseSelect' })}${
                      item.skuAttrName
                    }`,
                  },
                ],
              })(
                <Select disabled={mut !== 10}>
                  {item.skuAttrItemList.map(childItem => (
                    <Select.Option
                      key={childItem.skuAttrItemCode}
                      value={childItem.skuAttrItemCode}
                    >
                      {childItem.skuAttrItemValue}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          ) : (
            // 无下拉值,仅展示不可修改
            <Form.Item key={item.skuAttrCode} label={item.skuAttrName}>
              {getFieldDecorator(`${item.skuAttrCode}`, {
                rules: [{ required: true }],
                initialValue: item.skuAttrItemValue,
              })(<Input className={styles.inputTextStyle} disabled maxLength={50} />)}
            </Form.Item>
          )
        )}
      </Form>
    );
  };

  /**
   * 新增-渲染[商品普通属性]form组件,[属性]值可直接录入
   */
  renderAddGoodsSkuAttrNormalFormCmp = () => {
    const {
      form: { getFieldDecorator },
      modalUpkeepType: mut, // 当前modal状态类型,1为详情、2为修改、10为新增、20为删除
      goodsSkuAttrNormalList: gsanl, // 商品普通属性数据(列表)
    } = this.props;
    return (
      <Form {...this.formItemLayout}>
        {gsanl.map(item => (
          <Form.Item key={item.id} label={item.attrName}>
            {getFieldDecorator(`${item.attrCode}`, {
              rules: [
                {
                  required: true,
                  message: `${formatMessage({ id: 'form.goodsSkuBasicUpkeep.pleaseWrite' })}${
                    item.attrName
                  }`,
                },
              ],
              initialValue: item.attrValue,
            })(
              mut === 1 ? (
                <Input className={styles.inputTextStyle} disabled maxLength={50} />
              ) : (
                <Input maxLength={50} />
              )
            )}
          </Form.Item>
        ))}
      </Form>
    );
  };

  /**
   * 商品分组下拉选择改变时触发函数
   */
  onChangeGoodsSkuGroupHandle = (value, option) => {
    const { dispatch } = this.props;
    const {
      props: { skugroupref },
    } = option || { props: { skugroupref: {} } };
    if (Object.keys(skugroupref).length === 0) {
      // 设置当前选择商品分组数据(对象)为空
      dispatch({ type: 'goodsskuinfomanage/changeCurrentSelectGoodsSkuGroup', payload: null });
      // 设置当前商品分组下商品sku属性数据(列表)为空
      dispatch({ type: 'goodsskuinfomanage/changeGoodsSkuAttrSkuUnderSkuGroupList', payload: [] });
    } else {
      // 设置当前选择商品分组数据(对象)
      dispatch({
        type: 'goodsskuinfomanage/changeCurrentSelectGoodsSkuGroup',
        payload: skugroupref,
      });
      // 根据当前选择的商品分组-验证是否已存在关联的商品
      // 如果存在已关联商品,则获取第1条商品数据,再根据该商品获取对应的商品sku属性数据作为当前新增商品的默认sku属性数据,且不可修改
      // 如果未存在关联商品,则不作任何处理,使用默认加载的产品sku属性及关联属性项数据即可
      dispatch({
        type: 'goodsskuinfomanage/underSkuGroupVerifyAndProcess',
        payload: { skuGroup: skugroupref },
      });
    }
  };

  render() {
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut, // 当前modal状态类型,1为详情、2为修改、10为新增、20为删除
      currentSpuCondOfGoodsSpuModelList: cscogsml, // 当前产品条件-对应的产品型号数据(列表)
      currentSpuCondOfGoodsSkuGroupList: cscogsgl, // 当前产品条件-对应的商品分组数据(列表)
      parmPublicParameterOfGoodsUnitOfQuantityAll: pppouogqa, // 商品(单品)数量单位数据(所有)(对应公用参数定义表数据)
    } = this.props;

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form {...this.formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.spuInfo' })}>
              {getFieldDecorator('spuInfo', { rules: [{ required: true }] })(
                <Input className={styles.inputTextStyle} disabled />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode' })}>
              {getFieldDecorator('groupCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode.choice' }),
                  },
                ],
              })(
                <Select
                  allowClear
                  disabled={mut !== 10}
                  onChange={this.onChangeGoodsSkuGroupHandle}
                >
                  {cscogsgl.map(item =>
                    item.groupEnable ? (
                      <Select.Option key={item.groupCode} value={item.groupCode} skugroupref={item}>
                        {item.groupName}
                      </Select.Option>
                    ) : (
                      ''
                    )
                  )}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuName' })}>
              {getFieldDecorator('skuName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuName.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={200} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuShortName' })}>
              {getFieldDecorator('skuShortName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsSkuBasicUpkeep.skuShortName.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.defaultModelCode' })}>
              {getFieldDecorator('defaultModelCode')(
                <Select disabled={mut === 1} allowClear>
                  {cscogsml.map(item => (
                    <Select.Option key={item.modelCode} value={item.modelCode}>
                      {item.modelName}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            {mut !== 1 ? (
              <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.unitOfQuantity' })}>
                {getFieldDecorator('unitOfQuantityCode', {
                  rules: [
                    {
                      required: true,
                      message: formatMessage({
                        id: 'form.goodsSkuBasicUpkeep.unitOfQuantity.placeholder',
                      }),
                    },
                  ],
                })(
                  <Select disabled={mut === 1}>
                    {pppouogqa.map(item => (
                      <Select.Option key={item.parmCode} value={item.parmCode}>
                        {item.parmValue}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
            ) : (
              <div>
                <Form.Item
                  label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.unitOfQuantityCode' })}
                >
                  {getFieldDecorator('unitOfQuantityCode', { rules: [{ required: true }] })(
                    <Input disabled={mut === 1} />
                  )}
                </Form.Item>
                <Form.Item
                  label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.unitOfQuantityName' })}
                >
                  {getFieldDecorator('unitOfQuantityName', { rules: [{ required: true }] })(
                    <Input disabled={mut === 1} />
                  )}
                </Form.Item>
              </div>
            )}
            <Form.Item label={formatMessage({ id: 'form.common.rowno' })}>
              {getFieldDecorator('seq')(
                <InputNumber min={1} max={10000} defaultChecked={1} disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuDesc' })}>
              {getFieldDecorator('skuDesc')(
                <Input.TextArea
                  rows={5}
                  disabled={mut === 1}
                  placeholder={formatMessage({
                    id: 'form.goodsSkuBasicUpkeep.skuDesc.placeholder',
                  })}
                  maxLength={1000}
                />
              )}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.enable' })}
              style={{ display: 'none' }}
            >
              {getFieldDecorator('skuEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          <span>{formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuAttribute' })}</span>
          {this.renderAddGoodsSkuAttrSkuFormCmp()}
        </div>
        <div>
          <span>{formatMessage({ id: 'form.goodsSkuBasicUpkeep.commonAttribute' })}</span>
          {this.renderAddGoodsSkuAttrNormalFormCmp()}
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={this.confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ goodsskuinfomanage, parmpublicparametermanage }) => ({
  currentCondSelectEffectiveSpu: goodsskuinfomanage.currentCondSelectEffectiveSpu, // 当前条件选择的有效产品信息数据(对象)
  currentSpuCondOfGoodsSpuModelList: goodsskuinfomanage.currentSpuCondOfGoodsSpuModelList || [], // 当前产品条件-对应的产品型号数据(列表)
  currentSpuCondOfGoodsSkuGroupList: goodsskuinfomanage.currentSpuCondOfGoodsSkuGroupList || [], // 当前产品条件-对应的商品分组数据(列表)
  currentSelectGoodsSkuGroup: goodsskuinfomanage.currentSelectGoodsSkuGroup, // 当前选择的商品分组数据(对象)
  goodsSkuAttrSkuList: goodsskuinfomanage.goodsSkuAttrSkuList || [], // 商品sku属性数据(列表)
  goodsSkuAttrNormalList: goodsskuinfomanage.goodsSkuAttrNormalList || [], // 商品普通属性数据(列表)
  goodsSkuAttrSkuUnderSkuGroupList: goodsskuinfomanage.goodsSkuAttrSkuUnderSkuGroupList || [], // 基于商品分组已关联商品的商品sku属性数据(列表)
  parmPublicParameterOfGoodsUnitOfQuantityAll:
    parmpublicparametermanage.parmPublicParameterOfGoodsUnitOfQuantityAll || [], // 商品(单品)数量单位数据(所有)(对应公用参数定义表数据)
}))(
  Form.create({
    mapPropsToFields(props) {
      const {
        currentCondSelectEffectiveSpu: ccses, // 当前条件选择的有效产品信息数据(对象)
        currentSelectGoodsSkuGroup: csgsg, // 当前选择的商品分组数据(对象)
        modalUpkeepEntity: mue, // 当前选中的商品数据(对象)
      } = props;

      return {
        spuInfo: Form.createFormField({ value: ccses ? `${ccses.spuCode}+${ccses.spuName}` : '' }),
        skuName: Form.createFormField({ value: mue ? mue.skuName : '' }),
        skuShortName: Form.createFormField({ value: mue ? mue.skuShortName : '' }),
        groupCode: Form.createFormField({
          value: mue ? mue.groupCode : csgsg ? csgsg.groupCode : '',
        }), // 商品分组
        defaultModelCode: Form.createFormField({ value: mue ? mue.defaultModelCode : '' }), // 缺省产品型号代码
        unitOfQuantityCode: Form.createFormField({ value: mue ? mue.unitOfQuantityCode : '' }), // 数量单位代码
        unitOfQuantityName: Form.createFormField({ value: mue ? mue.unitOfQuantityName : '' }), // 数量单位名称
        seq: Form.createFormField({ value: mue ? mue.seq : 1 }),
        skuDesc: Form.createFormField({ value: mue ? mue.skuDesc : '' }),
        skuEnable: Form.createFormField({ value: mue ? mue.skuEnable === 1 : false }),
      };
    },
  })(GoodsSkuBasicUpkeep)
);

// <div>
//   <MdcManageBlock
//     tableTitle={<div>商品SKU属性</div>}
//     tableColumns={this.tableColumns_GoodsSkuAttrSku}
//     tableDataSource={gsasl}
//     hasTableFooter={false}
//     hasModal={false}
//   />
// </div>
// <div>
//   <MdcManageBlock
//     tableTitle={<div>商品普通属性</div>}
//     tableColumns={this.tableColumns_GoodsSkuAttrNormal}
//     tableDataSource={gsanl}
//     hasTableFooter={false}
//     hasModal={false}
//   />
// </div>

//  /**
//    * type为1：sku属性项值下拉选取-改变-回调函数
//    * type为2：普通属性-属性值录入-改变-回调函数
//    */
//   onChangeEventHandle = (type, record) => {
//     const {dispatch} = this.props;
//     if(type === 1){ // 更新-下拉选取的sku属性项值
//       dispatch({type: '', payload: {}});
//     }
//     if(type === 2){ // 更新-普通属性-属性值
//       dispatch({type: '', payload: {}});
//     }
//   }

//   /**
//    * 渲染商品sku属性-sku属性项值下拉选取列组件
//    * 下拉选取
//    */
//   renderGoodsSkuAttrSkuOfSkuAttrItemValueCmp = record => {
//     const {modalUpkeepType: mut} = this.props;
//     return (
//       <React.Fragment>
//         {mut === 1 ? (
//           <span>{record.skuAttrItemValue}</span>
//         ) : (
//           <Select
//             value={record.id}
//             onChange={value=>this.onChangeEventHandle(1, value)}
//             selectDataRef={record}
//           >
//             {pppouogqa.map(item => (
//               <Select.Option key={item.id} value={item.id}>
//                 {item.skuAttrItemValue}
//               </Select.Option>))}
//           </Select>
//         )}
//       </React.Fragment>
//     );
//   };

// /**
//  * 渲染商品普通属性-属性值录入组件
//  */
// renderGoodsSkuAttrNormalOfAttrValueCmp = record => {
//   const {modalUpkeepType: mut} = this.props;
//   return (
//     <React.Fragment>
//       {mut === 1 ? (
//         <span>{record.attrValue}</span>
//       ) : (
//         <Input value={record.attrValue} />
//       )}
//     </React.Fragment>
//   );
// }

/*
    if(gsasusgl && gsasusgl.length > 0){ // 有商品分组数据时执行if(商品分组对应的商品sku属性数据存在)
      gsasusgl.forEach(item=>{
        if(item.skuAttrItemList){ // 存在下拉选择项
          const skuItemCode = values[`${item.skuAttrCode}`]; // 当前选择的sku属性项代码
          if(skuItemCode === null || skuItemCode === undefined) {
            message.error(`请选择商品sku属性项${item.skuAttrName}的值后再提交操作`,8);
            return;
          }
          const skuItemValue = item.skuAttrItemList.filter(tmp=>tmp.skuAttrItemCode === skuItemCode)[0].skuAttrItemValue; // sku属性项值
          goodsSkuAttrSkuArrays.push({
            id: item.id || '',
            spuCode: ccses.spuCode,
            skuCode: (mue ? mue.skuCode : ''),
            skuAttrCode: item.skuAttrCode,
            skuAttrName: item.skuAttrName,
            skuAttrDesc: item.skuAttrDesc,
            skuAttrEnable: 1, // 默认启用
            skuAttrItemCode: skuItemCode, // sku属性项代码
            skuAttrItemValue: skuItemValue,// sku属性项值
          });
        }else{// 无下拉选择
          goodsSkuAttrSkuArrays.push(item);
        }
      });
    }else{// 无选择商品分组时执行else
      gsasl.forEach(item=>{
        const skuItemCode = values[`${item.skuAttrCode}`]; // 当前选择的sku属性项代码
        if(skuItemCode === null || skuItemCode === undefined) {
          message.error(`请选择商品sku属性项${item.skuAttrName}的值后再提交操作`,8);
          return;
        }
        const skuItemValue = item.skuAttrItemList.filter(tmp=>tmp.skuAttrItemCode === skuItemCode)[0].skuAttrItemValue; // sku属性项值
        goodsSkuAttrSkuArrays.push({
          id: item.id || '',
          spuCode: ccses.spuCode,
          skuCode: (mue ? mue.skuCode : ''),
          skuAttrCode: item.skuAttrCode,
          skuAttrName: item.skuAttrName,
          skuAttrDesc: item.skuAttrDesc,
          skuAttrEnable: 1, // 默认启用
          skuAttrItemCode: skuItemCode, // sku属性项代码
          skuAttrItemValue: skuItemValue,// sku属性项值
        });
      });
    }
    values.goodsSkuAttrSku = goodsSkuAttrSkuArrays;
*/

// if(gsasusgl && gsasusgl.length > 0){
//   return (
//     <Form {...this.formItemLayout}>
//       {gsasusgl.map(item=> (item.skuAttrItemList ? ( // 有下拉值可选取
//         <Form.Item label={item.skuAttrName}>
//           {getFieldDecorator(`${item.skuAttrCode}`,{rules:[{required: true, message: `请选择${item.skuAttrName}`}]})(
//             <Select disabled={mut !== 10}>
//               {item.skuAttrItemList.map(childItem => (<Select.Option key={childItem.skuAttrItemCode} value={childItem.skuAttrItemCode}>{childItem.skuAttrItemValue}</Select.Option>))}
//             </Select>
//           )}
//         </Form.Item>
//       ) : ( // 无下拉选取,仅展示不可修改
//         <Form.Item label={item.skuAttrName}>
//           {getFieldDecorator(`${item.skuAttrCode}`,{rules:[{required: true}], initialValue: item.skuAttrItemValue})(<Input disabled maxLength={50} />)}
//         </Form.Item>
//       )))}
//     </Form>
//   );
// }
