/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import moment from 'moment';
import { Button, Divider, Select, Dropdown, Form, Input, message, Icon, Card } from 'antd';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import GoodsSkuInfoUpkeep from './GoodsSkuInfoUpkeep';
import { execOneRequest } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 商品信息管理组件
 */
class GoodsSkuInfoManage extends MdcManageBasic {
  constructor(props) {
    super(props);
    this.state = {
      ...this.state,
      formItemLayout: {
        labelCol: { xs: { span: 24 }, sm: { span: 4 } },
        wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
      },
      tailFormItemLayout: {
        wrapperCol: { xs: { span: 24, offset: 0 }, sm: { span: 16, offset: 8 } },
      },
      overlayMenuVisible: false, // 下拉菜单显示/隐藏状态
      // groupCode: '', // 下拉分组列表选中值
      materielCode: '', // 物料编码搜索值 - 双向数据绑定
      // overLayValues: '', // 拓展搜索
      searchCondition: {},
    };
    // 商品信息展示列表字段定义
    this.tableColumns = [
      {
        title: formatMessage({ id: 'form.goodsSkuInfoManage.groupName' }),
        dataIndex: 'groupName',
        key: 'groupName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuInfoManage.groupSeq' }),
        dataIndex: 'groupSeq',
        key: 'groupSeq',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuCode' }),
        dataIndex: 'skuCode',
        key: 'skuCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuName' }),
        dataIndex: 'skuName',
        key: 'skuName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuShortName' }),
        dataIndex: 'skuShortName',
        key: 'skuShortName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.seq' }),
        dataIndex: 'seq',
        key: 'seq',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.defaultModelName' }),
        dataIndex: 'defaultModelName',
        key: 'defaultModelName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'skuEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.skuEnable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.goodsSkuInfoManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.goodsSkuInfoManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'goodsskuinfomanage/updateGoodsSkuInfoEnable'
                )
              }
            >
              {record.skuEnable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
  }

  /**
   * 响应点击处理
   * 进入商品详情之前获取商品相关联的数据
   */
  hookProcess = (type, record) => {
    const {
      dispatch,
      currentCondSelectEffectiveSpu: ccses, // 当前条件选择的有效产品信息数据(对象)
    } = this.props;

    if (type === 3) {
      // 停用/启用
      // record.skuEnable = record.skuEnable === 1 ? 0 : 1;
    } else if (type === 10) {
      // 新增
      // 首先清理上次新增的数据
      // 清理-当前商品分组下商品sku属性数据(列表)为空
      dispatch({ type: 'goodsskuinfomanage/changeGoodsSkuAttrSkuUnderSkuGroupList', payload: [] });
      // 清理-当前选择商品分组数据(对象)为空
      dispatch({ type: 'goodsskuinfomanage/changeCurrentSelectGoodsSkuGroup', payload: null });

      // 获取-当前选择的产品-对应[产品型号]数据(列表所有)
      dispatch({
        type: 'goodsskuinfomanage/getCurrentSpuCondOfGoodsSpuModelList',
        payload: { spuCode: ccses.spuCode },
      });
      // 获取-当前选择的产品-对应产品[普通属性定义]列表数据(列表所有),属性代码、属性名称、属性值,全部直接复制用于当前新增商品普通属性默认数据
      dispatch({
        type: 'goodsskuinfomanage/getCurrentSpuCondOfGoodsSpuAttrNormalList',
        payload: { spuCode: ccses.spuCode },
      });
    } else {
      // 详情/维护
      // 获取-当前选择的商品-对应产品代码内[商品分组]数据(列表所有)
      dispatch({
        type: 'goodsskuinfomanage/getCurrentSpuCondOfGoodsSkuGroupList',
        payload: { spuCode: record.spuCode },
      });
      // 获取-当前选择的商品-对应产品代码内[产品型号]数据(列表所有)
      dispatch({
        type: 'goodsskuinfomanage/getCurrentSpuCondOfGoodsSpuModelList',
        payload: { spuCode: record.spuCode },
      });
      // 获取-当前选择的商品-商品(单品)[普通属性]数据(列表所有)
      dispatch({ type: 'goodsskuinfomanage/getCurrentSelectGoodsSkuAttrNormal', payload: record });
      // 获取-当前选择的商品-商品(单品)[SKU属性]数据(列表所有)
      dispatch({ type: 'goodsskuinfomanage/getCurrentSelectGoodsSkuAttrSku', payload: record });
      // 获取-当前选择的商品-商品(单品)[描述]数据(列表所有/分页)
      dispatch({
        type: 'goodsskuinfomanage/paginationGoodsSkuDesc',
        payload: { page: 1, pageSize: 10, spuCode: record.spuCode, skuCode: record.skuCode },
      });
      // 获取-当前选择的商品-商品(单品)[物料信息]数据(列表所有)
      dispatch({
        type: 'goodsskuinfomanage/paginationGoodsMaterielInfo',
        payload: { page: 1, pageSize: 10, spuCode: record.spuCode, skuCode: record.skuCode },
      });
    }
    return record;
  };

  /**
   * 过滤查询分组不匹配的属性
   */
  filterGroupName = (input, option) =>
    option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;

  /**
   * 校验当前商品下商品分组是否已存在数据
   */
  checkGoodsSkuGroudExists = async spuCode => {
    if (spuCode === undefined) {
      message.warn(formatMessage({ id: 'form.goodsSkuBasicUpkeep.spuCode-conformity' }), 5);
      return;
    }

    // 校验当前产品下是否已存在[商品分组]数据
    const goodsSkuInfoAddCondVerifyRes = await execOneRequest(
      '/mdc/goods/goodsskuinfo/verifyAddSkuCondBySpuCode',
      { spuCode }
    );
    if (goodsSkuInfoAddCondVerifyRes && goodsSkuInfoAddCondVerifyRes.status) {
      // 验证通过
      // 设置新增状态
      this.onClickActionExecuteEvent(
        10,
        this.hookProcess(10, null),
        formatMessage({ id: 'button.goodsSkuInfoManage.add' })
      );
      return;
    }
    if (goodsSkuInfoAddCondVerifyRes && !goodsSkuInfoAddCondVerifyRes.status) {
      message.warn(`${goodsSkuInfoAddCondVerifyRes.message}`, 10);
      return;
    }
    message.warn(formatMessage({ id: 'form.goodsSkuInfoManage.callToVerifyInterfaceFailed' }), 10);
  };

  /**
   * 加载默认商品信息数据及公共数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-首次默认产品-对应的商品数据(列表/分页),条件：按照最新时间添加的商品找到产品,再查询该产品对应的商品进行列表展示
    dispatch({ type: 'goodsskuinfomanage/getDefaultFirstGoodsSkuInfo' });
    // 获取-商品数量单位类型数据(所有)-公用参数(所有)(条件为代码是goods_unit_of_quantity的商品(单品)数量单位数据)
    dispatch({
      type:
        'parmpublicparametermanage/getParmPublicParameterAllByParmPublicParameterTypeOfGoodsUnitOfQuantity',
      payload: { parmTypeCode: 'goods_unit_of_quantity' },
    });
    // 获取-描述类型数据(所有)-公用参数(所有)(条件为代码是goods_desc_type的产品描述数据)
    dispatch({
      type:
        'parmpublicparametermanage/getParmPublicParameterAllByParmPublicParameterTypeOfGoodsDescType',
      payload: { parmTypeCode: 'goods_desc_type' },
    });
  }

  /**
   * 物料编码填写时候绑定值
   */
  onChangeMeterilCode = e => {
    this.setState({ materielCode: e.target.value });
  };

  /**
   * 按照物料编码查询数据
   */
  onBlurGooodsSpuMaterielCodeHandle = e => {
    const { searchCondition, materielCode } = this.state;
    const {
      dispatch,
      // form: { getFieldsValue },
      currentCondSelectEffectiveSpu: ccses, // 当前条件选择的有效产品信息数据(对象)
    } = this.props;
    const { value } = e.target; // e.target.value
    // searchCondition.materielCode = value
    this.setState({ materielCode: value });
    // 有选择产品-更新
    // 查询-当前选择产品-对应商品信息数据列表(产品代码必须有值)
    dispatch({
      type: 'goodsskuinfomanage/paginationGoodsSkuInfo',
      payload: {
        page: 1,
        pageSize: 10,
        spuCode: ccses.spuCode,
        searchCondition: {
          ...searchCondition,
          materielCode,
        },
      },
    });
    // 返回第一页
    this.changeCurrentPage(1);
  };

  /**
   * 按[产品模糊搜索]查询数据
   */
  onFuzzyGoodsSpuInfohHandle = () => {
    const { searchCondition, materielCode } = this.state;
    const {
      dispatch,
      form: { getFieldsValue },
      currentCondSelectEffectiveSpu: ccses,
    } = this.props;
    const values = getFieldsValue();
    searchCondition.skuCode = values.skuCode;
    searchCondition.skuName = values.skuName;
    this.setState({ searchCondition });
    // 根据模糊条件搜索-产品信息数据
    dispatch({
      type: 'goodsskuinfomanage/paginationGoodsSkuInfo',
      payload: {
        page: 1,
        pageSize: 10,
        spuCode: ccses.spuCode,
        searchCondition: {
          ...searchCondition,
          materielCode,
        },
      },
    });
    this.setState({ overlayMenuVisible: false });
    // 返回第一页
    this.changeCurrentPage(1);
  };

  /**
   * 按[产品名称搜索]查询数据
   */
  onSearchGoodsSpuInfohHandle = value => {
    const { dispatch } = this.props;
    // 根据条件查询-产品信息数据-默认按名称查询(数据供下拉选择使用)
    dispatch({
      type: 'goodsskuinfomanage/getGoodsSpuInfoListByGoodsSpuInfo',
      payload: { spuName: value },
    });
  };

  /**
   * 商品列表-产品选择
   * 改变更新state数据
   */
  onChangeGoodsSpuInofHandle = (value, option) => {
    const {
      dispatch,
      form: { setFieldsValue, getFieldsValue },
    } = this.props;
    const { searchCondition, materielCode } = this.state;
    const {
      props: { spuref },
    } = option || { props: { spuref: {} } };

    // 更新-当前选择-有效操作的产品信息数据
    dispatch({
      type: 'goodsskuinfomanage/changeCurrentCondEffectiveSpuData',
      payload: { spu: spuref },
    });
    searchCondition.groupCode = '';
    searchCondition.skuCode = '';
    searchCondition.skuName = '';
    this.setState({ searchCondition });
    setFieldsValue({ skuCode: '', skuName: '' });

    if (!(spuref && spuref.spuCode)) {
      spuref.spuCode = '';
      // 如果当前未选择产品-更新对应[商品分组]为空
      dispatch({
        type: 'goodsskuinfomanage/changeCurrentSpuCondOfGoodsSkuGroupList',
      });
    } else {
      // 获取-当前选择的产品-对应[商品分组]数据(列表所有)
      dispatch({
        type: 'goodsskuinfomanage/getCurrentSpuCondOfGoodsSkuGroupList',
        payload: { spuCode: spuref.spuCode },
      });
    }
    const values = getFieldsValue();
    dispatch({
      type: 'goodsskuinfomanage/paginationGoodsSkuInfo',
      payload: {
        page: 1,
        pageSize: 10,
        spuCode: spuref.spuCode,
        searchCondition: {
          materielCode,
          ...searchCondition,
        },
      },
    });
    // if (spuref && spuref.spuCode) {
    //   // 有选择产品-更新
    //   // 查询-当前选择产品-对应商品信息数据列表(产品代码必须有值)
    //   dispatch({
    //     type: 'goodsskuinfomanage/paginationGoodsSkuInfo',
    //     payload: { page: 1, pageSize: 10, spuCode: spuref.spuCode },
    //   });
    // } else {
    //   // 未选择产品-更新
    //   // 查询所有产品数据供下拉选择产品使用
    //   dispatch({ type: 'goodsskuinfomanage/getGoodsSpuInfoListByGoodsSpuInfo', payload: {} });
    //   // 因未选择产品,则设置当前选择产品的商品信息列表及数量为空值
    //   // dispatch({
    //   //   type: 'goodsskuinfomanage/changeGoodsSkuInfoList',
    //   //   payload: { content: [], total: 0 },
    //   // });
    //   dispatch({
    //     type: 'goodsskuinfomanage/paginationGoodsSkuInfo',
    //     payload: { page: 1, pageSize: 10, spuCode: spuref.spuCode },
    //   });
    // }
    // 返回第一页
    this.changeCurrentPage(1);
  };

  /**
   * 商品分组列表
   */
  onChangeGoodsSpuGroupInofHandle = (value, option) => {
    const { searchCondition, materielCode } = this.state;
    const {
      dispatch,
      form: { getFieldsValue },
      currentCondSelectEffectiveSpu: ccses, // 当前条件选择的有效产品信息数据(对象)
    } = this.props;
    searchCondition.groupCode = value;
    this.setState({ searchCondition });
    // const values = getFieldsValue();
    // 有选择产品-更新
    // 查询-当前选择产品-对应商品信息数据列表(产品代码必须有值)
    dispatch({
      type: 'goodsskuinfomanage/paginationGoodsSkuInfo',
      payload: {
        page: 1,
        pageSize: 10,
        spuCode: ccses.spuCode,
        searchCondition: {
          searchCondition,
          materielCode,
        },
      },
    });
    // 返回第一页
    this.changeCurrentPage(1);
  };

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      formItemLayout,
      tailFormItemLayout,
      overlayMenuVisible,
      // groupCode,
      materielCode,
      // overLayValues,
      searchCondition,
      tableCurrentPage,
    } = this.state;
    const {
      goodsSkuInfoList,
      goodsSkuInfoListPageSize,
      tableLoadingStatus, // 商品列表数据加载状态
      currentCondSearchEffectiveSpuList: ccsesl, // 当前条件查询到的有效产品信息数据(列表)
      currentCondSelectEffectiveSpu: ccses, // 当前条件选择的有效产品信息数据(对象)
      currentSpuCondOfGoodsSkuGroupList: cscogsgl, // 当前条件选择的有效产品信息分组(列表)
      form: { getFieldsValue },
    } = this.props;
    /**
     * 下拉操作组件
     * 模糊搜索条件输入展示组件
     */
    const dropdownOverlay = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <div>
            <Form {...formItemLayout}>
              <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuCode' })}>
                {getFieldDecorator('skuCode')(<Input />)}
              </Form.Item>
              <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuName' })}>
                {getFieldDecorator('skuName')(<Input />)}
              </Form.Item>
              <Form.Item {...tailFormItemLayout}>
                <Button onClick={() => this.setState({ overlayMenuVisible: false })}>
                  {formatMessage({ id: 'button.common.cancel' })}
                </Button>
                <Button onClick={() => this.onFuzzyGoodsSpuInfohHandle()}>
                  {formatMessage({ id: 'button.common.query' })}
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      );
    };

    const tableOperateAreaCmp = (
      <div className={styles.searchCmpStyle}>
        <div style={{ marginRight: '20px' }}>
          <Input
            style={{ width: 200 }}
            placeholder={formatMessage({ id: 'form.goodsSkuInfoManage.materielCode.placeholder' })}
            value={materielCode}
            onBlur={this.onBlurGooodsSpuMaterielCodeHandle}
            onChange={this.onChangeMeterilCode}
          />
          &nbsp;
          <Select
            allowClear
            showSearch
            placeholder={formatMessage({ id: 'form.goodsSkuInfoManage.spuName.placeholder' })}
            filterOption={false}
            value={ccses.spuCode || ''}
            onSearch={this.onSearchGoodsSpuInfohHandle}
            onChange={this.onChangeGoodsSpuInofHandle}
            style={{ width: 248 }}
          >
            {ccsesl.map(item => (
              <Select.Option key={item.spuCode} value={item.spuCode} spuref={item}>
                {`${item.spuName}`}
              </Select.Option>
            ))}
          </Select>
          &nbsp;
          <Select
            allowClear
            showSearch
            placeholder={formatMessage({ id: 'form.goodsSkuInfoManage.groupName.placeholder' })}
            // filterOption={false}
            value={searchCondition.groupCode || ''}
            // onSearch={this.onSearchGoodsSpuInfohHandle}
            optionFilterProp="children"
            filterOption={this.filterGroupName}
            onChange={this.onChangeGoodsSpuGroupInofHandle}
            style={{ width: 240 }}
          >
            {cscogsgl.map(item => (
              <Select.Option key={item.groupCode} value={item.groupCode} spuref={item}>
                {`${item.groupName}`}
              </Select.Option>
            ))}
          </Select>
          &nbsp;
          <Dropdown
            overlay={dropdownOverlay()}
            trigger={['click']}
            onVisibleChange={flag => this.setState({ overlayMenuVisible: flag })}
            visible={overlayMenuVisible}
            overlayStyle={{ width: 445 }}
          >
            <Button>
              <Icon type="menu-fold" />
            </Button>
          </Dropdown>
        </div>
        <Button
          type="primary"
          onClick={() => {
            const { dispatch } = this.props;
            // 获取-当前选择的产品-对应产品[sku属性定义]列表数据(列表所有,含skuAttrItemList属性项值列表数据),属性代码、属性名称,全部直接复制用于当前新增商品sku属性默认数据,另属性项值下拉选取
            dispatch({
              type:
                'goodsskuinfomanage/getCurrentSpuCondOfGoodsSpuAttrSkuListWithChildAttrSkuItemList',
              payload: { spuCode: ccses.spuCode },
            });
            return ccses
              ? this.checkGoodsSkuGroudExists(ccses.spuCode)
              : message.warn(formatMessage({ id: 'form.goodsSkuInfoManage.spuName-conformity' }));
          }}
        >
          {formatMessage({ id: 'button.common.add' })}
        </Button>
      </div>
    );
    const tablePaginationOnChangeEventDispatchType = 'goodsskuinfomanage/paginationGoodsSkuInfo';
    return (
      <Card bordered={false}>
        <div>
          <MdcManageBlock
            tableTitle={
              ccses && ccses.spuCode
                ? `${formatMessage({ id: 'form.goodsSkuInfoManage.currentProduct' })}：${
                    ccses.spuCode
                  }   ${ccses.spuName}`
                : formatMessage({ id: 'menu.goodsmanage.goodsskuinfomanage' })
            }
            tableColumns={this.tableColumns}
            tableDataSource={goodsSkuInfoList}
            tableOperateAreaCmp={tableOperateAreaCmp}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                // { spuCode: ccses.spuCode, groupCode, materielCode, ...overLayValues }
                { spuCode: ccses.spuCode, searchCondition: { ...searchCondition, materielCode } }
              )
            }
            tableTotalSize={goodsSkuInfoListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
            tableLoading={tableLoadingStatus}
            modalTitle={modalTitle}
            modalVisible={modalVisible}
            modalContentCmp={
              <GoodsSkuInfoUpkeep
                modalUpkeepType={modalUpkeepType}
                modalUpkeepEntity={modalUpkeepEntity}
                modalVisibleOnChange={this.modalVisibleOnChange}
                modalOnChangePageCurrent={this.changeCurrentPage}
              />
            }
            modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ goodsskuinfomanage }) => ({
  tableLoadingStatus: goodsskuinfomanage.tableLoadingStatus || false, // 商品列表数据加载状态
  goodsSkuInfoList: goodsskuinfomanage.goodsSkuInfoList || [], // 商品信息列表
  goodsSkuInfoListPageSize: goodsskuinfomanage.goodsSkuInfoListPageSize || 0, // 商品信息列表总数量
  currentCondSearchEffectiveSpuList: goodsskuinfomanage.currentCondSearchEffectiveSpuList || [], // 当前条件查询到的有效产品信息数据(列表)
  currentCondSelectEffectiveSpu: goodsskuinfomanage.currentCondSelectEffectiveSpu || {}, // 当前条件选择的有效产品信息数据(对象)
  currentSpuCondOfGoodsSkuGroupList: goodsskuinfomanage.currentSpuCondOfGoodsSkuGroupList || [], // 当前条件查询到的有效产品信息数据(列表)
}))(Form.create()(GoodsSkuInfoManage));
