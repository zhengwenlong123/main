import React, { PureComponent } from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import moment from 'moment';
import { Button, Form, Input, Checkbox, Select, InputNumber, message } from 'antd';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 商品描述维护组件
 */
@ValidationFormHoc
class GoodsSkuDescUpkeep extends PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      originGoodsSkuInfo: ogsi, // 原始商品(单品)基本信息数据
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    values.spuCode = ogsi.spuCode;
    values.descEnable = values.descEnable ? 1 : 0;

    if (mut === 2) {
      // 维护
      values.id = mue.id;
      dispatch({
        type: 'goodsskuinfomanage/updateGoodsSkuDesc',
        payload: values,
        callback: this.onCallback,
      });
    }

    if (mut === 10) {
      // 新增
      values.spuCode = ogsi.spuCode;
      values.skuCode = ogsi.skuCode;
      dispatch({
        type: 'goodsskuinfomanage/addGoodsSkuDesc',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
      parmPublicParameterOfGoodsDescTypeAll: pppogdta, // 描述类型数据(所有)
    } = this.props;

    return (
      <div>
        <div>
          <Form {...formItemLayout}>
            {mut === 10 ? null : (
              <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.spuCode' })}>
                {getFieldDecorator('spuCode', { rules: [{ required: true }] })(<Input disabled />)}
              </Form.Item>
            )}
            {mut === 10 ? null : (
              <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuCode' })}>
                {getFieldDecorator('skuCode', { rules: [{ required: true }] })(<Input disabled />)}
              </Form.Item>
            )}
            <Form.Item label={formatMessage({ id: 'form.common.descriptionType' })}>
              {getFieldDecorator('descTypeCode', { rules: [{ required: true }] })(
                <Select disabled={mut === 1}>
                  {pppogdta.map(item => (
                    <Select.Option key={item.parmCode} value={item.parmCode}>
                      {item.parmValue}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.storageAddress' })}>
              {getFieldDecorator('address', { rules: [{ required: true }] })(
                <Input disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('descName')(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.rowno' })}>
              {getFieldDecorator('seq')(
                <InputNumber min={1} max={10000} defaultChecked={1} disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.remark' })}>
              {getFieldDecorator('descNotes')(<Input.TextArea rows={5} disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('descEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ parmpublicparametermanage }) => ({
  parmPublicParameterOfGoodsDescTypeAll:
    parmpublicparametermanage.parmPublicParameterOfGoodsDescTypeAll || [], // 描述类型数据(所有)
}))(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        spuCode: Form.createFormField({ value: mue ? mue.spuCode : '' }),
        skuCode: Form.createFormField({ value: mue ? mue.skuCode : '' }),
        descTypeCode: Form.createFormField({ value: mue ? mue.descTypeCode : '' }),
        address: Form.createFormField({ value: mue ? mue.address : '' }),
        descName: Form.createFormField({ value: mue ? mue.descName : '' }),
        seq: Form.createFormField({ value: mue ? mue.seq : 1 }),
        descNotes: Form.createFormField({ value: mue ? mue.descNotes : '' }),
        descEnable: Form.createFormField({ value: mue ? mue.descEnable === 1 : true }),
      };
    },
  })(GoodsSkuDescUpkeep)
);
