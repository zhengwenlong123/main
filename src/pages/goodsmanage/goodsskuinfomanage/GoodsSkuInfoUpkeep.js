import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Tabs } from 'antd';
import moment from 'moment';
import GoodsSkuInfoUpkeepBasic from './GoodsSkuInfoUpkeepBasic';
import GoodsSkuBasicUpkeep from './GoodsSkuBasicUpkeep';

moment.locale('zh-cn');
/**
 * 商品信息维护组件
 */
class GoodsSkuInfoUpkeep extends React.PureComponent {
  render() {
    const { modalUpkeepType: mut } = this.props;

    return (
      <div>
        {mut === 10 ? (
          <GoodsSkuBasicUpkeep {...this.props} />
        ) : (
          <div>
            <Tabs tabPosition="left">
              <Tabs.TabPane
                key="0"
                tab={formatMessage({ id: 'menu.goodsmanage.goodsskuinfomanage.skuBasic' })}
              >
                <GoodsSkuBasicUpkeep {...this.props} />
              </Tabs.TabPane>
              <Tabs.TabPane
                key="1"
                tab={formatMessage({ id: 'menu.goodsmanage.goodsskuinfomanage.skuInfo' })}
              >
                <GoodsSkuInfoUpkeepBasic {...this.props} upkeepBasicFlag={1} />
              </Tabs.TabPane>
              <Tabs.TabPane
                key="2"
                tab={formatMessage({ id: 'menu.goodsmanage.goodsskuinfomanage.skuMateriel' })}
              >
                <GoodsSkuInfoUpkeepBasic {...this.props} upkeepBasicFlag={2} />
              </Tabs.TabPane>
            </Tabs>
          </div>
        )}
      </div>
    );
  }
}

export default connect()(GoodsSkuInfoUpkeep);
