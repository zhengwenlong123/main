import React, { PureComponent } from 'react';

class SpanSelectShow extends PureComponent {
  render() {
    const {
      value: { key = '', label = '' },
    } = this.props;
    return (
      <div key={key}>
        <span>{label}</span>
      </div>
    );
  }
}

class SpanInputShow extends PureComponent {
  render() {
    const { value, className } = this.props;
    return (
      <div className={className || ''}>
        <span>{value}</span>
      </div>
    );
  }
}

export default { SpanSelectShow, SpanInputShow };
