import React, { useState, useEffect } from 'react';
import { Table, Card } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import { SPU_ENABLE } from '@/utils/const';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单新增：商品分组部分组件
 */
function MaterielTab(props) {
  const { dispatch, goodsTradeSpuMaterielList, goodsTradeSpuSkuList, spuType } = props;

  // 商品状态文字显示
  const materielEnableLabel = text => {
    let enablelabel = '';
    Object.keys(SPU_ENABLE).forEach(key => {
      if (SPU_ENABLE[key].value === text) {
        const { label } = SPU_ENABLE[key];
        enablelabel = label;
      }
    });
    return enablelabel;
  };
  // 请求操作
  const hookProcess = (materielEnable, record) => {
    dispatch({
      type: 'goodstradespuinfomanage/modifyGoodsMaterielInfo',
      payload: {
        ...record,
        materielEnable,
      },
    });
  };
  // 默认表格列数据
  const defaultColumns = [
    {
      title: '物料编号',
      dataIndex: 'materielCode',
      editable: false,
      onCell: () => ({ style: { minWidth: 60, width: 100 } }),
    },
    {
      title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.modelCode' }),
      dataIndex: 'modelCode',
      width: 80,
    },
    {
      title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.materielName' }),
      dataIndex: 'materielName',
    },
    {
      title: formatMessage({ id: 'form.common.series' }),
      dataIndex: 'series',
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillManage.title.manufactureMateriel' }),
      dataIndex: 'goodsMaterielEbs.manufactureMaterielCode',
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillManage.title.customize' }),
      dataIndex: 'goodsMaterielEbs.customizedMachineType',
    },
    {
      title: formatMessage({ id: 'form.common.enable' }), // 状态
      dataIndex: 'materielEnable',
      render: text => <span>{materielEnableLabel(text)}</span>,
    },
    {
      title: formatMessage({ id: 'form.common.options' }),
      key: 'action',
      align: 'center',
      render: (text, record) => (
        <>
          {SPU_ENABLE.enabled.value === record.materielEnable && (
            <a type="primary" onClick={() => hookProcess(SPU_ENABLE.disabled.value, record)}>
              {SPU_ENABLE.disabled.label}
            </a>
          )}
          {SPU_ENABLE.disabled.value === record.materielEnable && (
            <a type="primary" onClick={() => hookProcess(SPU_ENABLE.enabled.value, record)}>
              {SPU_ENABLE.enabled.label}
            </a>
          )}
        </>
      ),
    },
  ];

  const [skuList, setSkuList] = useState([]);
  const [materielList, setMaterielList] = useState([]);

  // 监听并更新属性集变化
  useEffect(() => {
    setSkuList([...goodsTradeSpuSkuList]);
  }, [goodsTradeSpuSkuList]);
  useEffect(() => {
    setMaterielList([...goodsTradeSpuMaterielList]);
  }, [goodsTradeSpuMaterielList]);

  const tableRender = skuCode => {
    const dataSource = materielList.filter(v => v.skuCode === skuCode);
    const columns =
      spuType === 'edit' ? defaultColumns : defaultColumns.filter(v => v.key !== 'action');
    return (
      <Table
        rowKey="materielCode"
        className="small-margin-common-table"
        pagination={false}
        columns={columns}
        dataSource={dataSource}
      />
    );
  };

  return (
    <>
      {skuList.map(v => (
        <Card
          key={v.id}
          title={v.skuName}
          bordered={false}
          headStyle={{ paddingLeft: 0, minHeight: 30 }}
        >
          {tableRender(v.skuCode)}
        </Card>
      ))}
    </>
  );
}
export default connect(({ goodstradespuinfomanage }) => ({
  goodsTradeSpuSkuList: goodstradespuinfomanage.goodsTradeSpuSkuList || [], // 商品列表数据
  goodsTradeSpuMaterielList: goodstradespuinfomanage.goodsTradeSpuMaterielList || [], // 物料数据
}))(MaterielTab);
