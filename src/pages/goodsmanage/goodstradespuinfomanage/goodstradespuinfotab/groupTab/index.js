import React, { useState, useEffect } from 'react';
import { Table, Card } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品信息：商品分组列表
 */
function GroupTab(props) {
  // 默认表格列数据
  const defaultColumns = [
    {
      title: formatMessage({ id: 'form.common.apply.groupName' }),
      dataIndex: 'groupName',
    },
    {
      title: formatMessage({ id: 'form.common.apply.groupCode' }),
      dataIndex: 'groupCode',
    },
    {
      title: formatMessage({ id: 'form.common.apply.attr' }),
      dataIndex: 'subAttrName',
    },
  ];

  const {
    goodsTradeSpuSkuGroupList,
    goodsTradeSpuSkuInfo,
    goodsTradeSpuSKuGroupAttr,
    extraDesc,
  } = props;
  const [dataSource, setDataSource] = useState([...goodsTradeSpuSkuGroupList]);
  const [columns, setColumns] = useState([...defaultColumns]);
  const [extraTitle, setExtraTitle] = useState('');

  // 监听并更新属性集变化
  useEffect(() => {
    if (extraDesc) {
      setExtraTitle(`(${extraDesc})`);
    } else {
      setExtraTitle('');
    }
  }, [extraDesc]);
  useEffect(() => {
    setDataSource([...goodsTradeSpuSkuGroupList]);
  }, [goodsTradeSpuSkuGroupList]);
  useEffect(() => {
    let arr = [...goodsTradeSpuSkuInfo];
    if (goodsTradeSpuSKuGroupAttr && goodsTradeSpuSKuGroupAttr.skuAttrCode) {
      arr = goodsTradeSpuSkuInfo.filter(v => v.attr.key !== goodsTradeSpuSKuGroupAttr.skuAttrCode);
    }
    const columnsAtrr = arr.map(v => ({
      title: v.attr.label,
      dataIndex: v.attr.key,
      render: text => <span key={text.code}>{text && `${text.name}(${text.code})`}</span>,
    }));
    setColumns([...defaultColumns, ...columnsAtrr]);
  }, [goodsTradeSpuSkuInfo, goodsTradeSpuSKuGroupAttr]);

  return (
    <Card
      title={`${formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode' })}${extraTitle}`}
      bordered={false}
      headStyle={{ paddingLeft: 0, minHeight: 30 }}
    >
      <Table
        rowKey="groupCode"
        className="small-margin-common-table"
        pagination={false}
        columns={columns}
        dataSource={dataSource}
      />
    </Card>
  );
}
export default connect(({ goodstradespuinfomanage }) => ({
  goodsTradeSpuSkuInfo: goodstradespuinfomanage.goodsTradeSpuSkuInfo || [], // 产品sku信息
  goodsTradeSpuSKuGroupAttr: goodstradespuinfomanage.goodsTradeSpuSKuGroupAttr || {}, // 产品商品分组从属属性
  goodsTradeSpuSkuGroupList: goodstradespuinfomanage.goodsTradeSpuSkuGroupList || [], // 商品分组列表数据
}))(GroupTab);
