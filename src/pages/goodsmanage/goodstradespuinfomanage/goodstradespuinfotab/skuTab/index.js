import React, { PureComponent } from 'react';
import { Table, Divider, Row, Col, Button } from 'antd';
import { connect } from 'dva';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import { SPU_ENABLE } from '@/utils/const';
import Goodstradeskuinfomanage from '../../goodstradeskuinfomanage';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品信息：商品信息列表
 */
class SkuTab extends PureComponent {
  // 默认表格列数据
  tableColumns = [
    {
      title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.groupCode' }), // 商品分组
      dataIndex: 'groupCode',
      render: text => <span>{this.groupLabel(text)}</span>,
    },
    {
      title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuCode' }), // 商品代码
      dataIndex: 'skuCode',
    },
    {
      title: formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuName' }), // 商品名称
      dataIndex: 'skuName',
    },
    {
      title: formatMessage({ id: 'form.goodsTradeSpuInfoManage.skuModel' }), // 缺省型号
      dataIndex: 'defaultModelCode',
    },
    {
      title: formatMessage({ id: 'form.common.enable' }), // 状态
      dataIndex: 'skuEnable',
      render: text => <span>{this.skuEnableLabel(text)}</span>,
    },
    {
      title: formatMessage({ id: 'form.common.options' }),
      key: 'action',
      align: 'center',
      render: (text, record) => (
        <>
          <a type="primary" onClick={() => this.hookProcess('see', record)}>
            {formatMessage({ id: 'button.common.details' })}
          </a>
          <Divider type="vertical" />
          <a type="primary" onClick={() => this.hookProcess('modify', record)}>
            {formatMessage({ id: 'button.common.modify' })}
          </a>
          <Divider type="vertical" />
          <a type="primary" onClick={() => this.hookProcess('status', record)}>
            {SPU_ENABLE.enabled.value === record.skuEnable && SPU_ENABLE.disabled.label}
            {SPU_ENABLE.disabled.value === record.skuEnable && SPU_ENABLE.enabled.label}
          </a>
        </>
      ),
    },
  ];

  state = {
    showInfo: false,
    operateType: '',
  };

  // 自动加载
  componentDidMount() {}

  // 根据商品分组编码或者分组名称
  groupLabel = groupCode => {
    const { goodsTradeSpuSkuGroupList } = this.props;
    const findVal = goodsTradeSpuSkuGroupList.find(v => v.groupCode === groupCode);
    return findVal ? findVal.groupName : '';
  };

  // 商品状态文字显示
  skuEnableLabel = text => {
    let enablelabel = '';
    Object.keys(SPU_ENABLE).forEach(key => {
      if (SPU_ENABLE[key].value === text) {
        const { label } = SPU_ENABLE[key];
        enablelabel = label;
      }
    });
    return enablelabel;
  };

  hookProcess = (type, record) => {
    const obj = {
      see: () => this.handleSee(record),
      modify: () => this.handleModify(record),
      status: () => this.handleStatus(record),
    };
    obj[type]();
  };

  // 详情
  handleSee = record => {
    const { dispatch } = this.props;
    const { spuCode, skuCode } = record;
    dispatch({
      type: 'goodstradespuinfomanage/getGoodsSkuInfoEnhance',
      payload: { spuCode, skuCode },
    });
    this.setState({
      showInfo: true,
      operateType: 'see',
    });
  };

  // 维护
  handleModify = record => {
    const { dispatch } = this.props;
    const { spuCode, skuCode } = record;
    dispatch({
      type: 'goodstradespuinfomanage/getGoodsSkuInfoEnhance',
      payload: { spuCode, skuCode },
    });
    this.setState({
      showInfo: true,
      operateType: 'edit',
    });
  };

  // 状态修改
  handleStatus = record => {
    const { dispatch } = this.props;
    // 如果是有效,则改成无效,如果是无效则改成有效
    record.skuEnable = record.skuEnable === 1 ? 0 : 1; // eslint-disable-line
    dispatch({
      type: 'goodstradespuinfomanage/modifyGoodsTradeSkuStatus',
      payload: record,
    });
  };

  // 返回列表
  handleReturn = () => {
    this.setState({
      showInfo: false,
      operateType: '',
    });
  };

  render() {
    const { goodsTradeSpuSkuList, goodsTradeSkuBasicInfo, spuType } = this.props;
    const { showInfo, operateType } = this.state;
    const columns =
      spuType === 'edit' ? this.tableColumns : this.tableColumns.filter(v => v.key !== 'action');
    return (
      <>
        {showInfo ? (
          <span>
            <Row>
              <Col xs={24} sm={24} md={18} lg={16} xl={16} xxl={16}>
                <h3>{goodsTradeSkuBasicInfo && goodsTradeSkuBasicInfo.skuName}</h3>
              </Col>
              <Col xs={24} sm={24} md={6} lg={8} xl={8} xxl={8} style={{ textAlign: 'right' }}>
                <Button type="primary" onClick={() => this.handleReturn()}>
                  返回商品列表
                </Button>
              </Col>
            </Row>
            <Goodstradeskuinfomanage operateType={operateType} />
          </span>
        ) : (
          <Table
            rowKey="skuCode"
            className="small-margin-common-table"
            pagination={false}
            columns={columns}
            dataSource={goodsTradeSpuSkuList}
          />
        )}
      </>
    );
  }
}
export default connect(({ goodstradespuinfomanage }) => ({
  goodsTradeSpuSkuInfo: goodstradespuinfomanage.goodsTradeSpuSkuInfo || [], // 产品sku信息
  goodsTradeSpuSKuGroupAttr: goodstradespuinfomanage.goodsTradeSpuSKuGroupAttr || {}, // 产品商品分组从属属性
  goodsTradeSpuSkuGroupList: goodstradespuinfomanage.goodsTradeSpuSkuGroupList || [], // 商品分组列表数据
  goodsTradeSpuSkuList: goodstradespuinfomanage.goodsTradeSpuSkuList || [], // 商品列表数据
  goodsTradeSkuBasicInfo: goodstradespuinfomanage.goodsTradeSkuBasicInfo || {}, // 商品基本信息
}))(SkuTab);
