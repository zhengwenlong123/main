import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Descriptions } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import styles from './index.less';

moment.locale('zh-cn');

/**
 * 产品信息：产品商品分组从属属性
 */
function DependenceAttrCard(props, ref) {
  const { form, goodsTradeSpuSKuGroupAttr } = props;

  useImperativeHandle(ref, () => ({
    form,
  }));

  const [attrVal, setAttrVal] = useState({ ...goodsTradeSpuSKuGroupAttr });

  useEffect(() => {
    setAttrVal({ ...goodsTradeSpuSKuGroupAttr });
  }, [goodsTradeSpuSKuGroupAttr]);

  return (
    <div className={styles.goodsapplybilltabBasicInfo}>
      <Descriptions>
        <Descriptions.Item span={3} label={`${formatMessage({ id: 'form.common.apply.attr' })}`}>
          {`${attrVal.skuAttrName || ''}(${attrVal.skuAttrCode})`}
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
}

export default React.forwardRef(DependenceAttrCard);
