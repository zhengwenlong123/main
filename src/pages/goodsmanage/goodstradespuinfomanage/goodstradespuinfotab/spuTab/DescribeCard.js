import React from 'react';
import { Select, Input, Button, message, Table } from 'antd';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import EditableTableBasic from '@/components/EditableTable/EditableTableBasic';
import EditableTableBlock from '@/components/EditableTable/EditableTableBlock';
import UploadFileDirect from '@/components/UploadBasic/UploadFileDirect';
import styles from './index.less';

/**
 * 必填项标题处理
 * @param {*} label 标题
 */
const headerColumntitle = id => (
  <span>
    <span style={{ color: 'red' }}>*</span>
    {formatMessage({ id })}
  </span>
);
// 初始列表行数据‘
const initRowData = {
  descTypeCode: '',
  descName: '',
  address: '',
};

class DescribeCard extends EditableTableBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: headerColumntitle('form.common.descriptionType'),
        dataIndex: 'descTypeCode',
        // width: 200,
        editable: true,
        rules: [
          {
            required: true,
            message: formatMessage({ id: 'form.common.descriptionType.placeholder' }),
          },
        ],
        render: text => <span>{this.descTypeLabel(text)}</span>,
        renderInput: () => this.typeSelect(),
      },
      {
        title: headerColumntitle('form.common.descriptionName'),
        dataIndex: 'descName',
        editable: true,
        // onCell: () => ({ style: { minWidth: 80 } }),
        rules: [
          {
            required: true,
            message: formatMessage({ id: 'form.common.descriptionName.placeholder' }),
          },
        ],
        renderInput: () => <Input style={{ minWidth: 100 }} allowClear />,
      },
      {
        title: headerColumntitle('form.common.attachment'),
        dataIndex: 'address',
        editable: true,
        // onCell: () => ({ style: { minWidth: 80 } }),
        rules: [
          {
            required: true,
            message: formatMessage({ id: 'form.common.attachment.placeholder' }),
          },
        ],
        render: text => <UploadFileDirect value={text} uploadAccept="image/*" uploadIsDisable />,
        renderInput: () => (
          <UploadFileDirect
            // onChange={uploadFileDirectChange}
            uploadAccept="image/*"
            // uploadSign={k.id}
          />
        ),
      },
    ];
    // const validatorDispatch = list => {
    //   // 筛选出已生效数据
    //   const validList = list.filter(v => v.attr && v.attr.key);
    //   // const validSku = props.goodsSkulist.filter(v => v.rfStage === 1);
    //   // const payload = [...validList].map(v => ({ ...v, skuCode: v.skuCode || v.id }));
    //   return validList;
    // }; // 校验是有效才保存到redux
    super(props, {
      tableColumns,
      // validatorDispatch,
      rowKey: 'id',
      dispatchType: 'goodstradespuinfomanage/changeGoodsTradeSpuDesc',
    });
    this.handled = false;
  }

  componentDidMount() {
    const { goodsTradeSpuDesc } = this.props;
    this.setState({
      nativeData: [...goodsTradeSpuDesc],
    });
  }

  componentWillReceiveProps(nextProps) {
    const { goodsTradeSpuDesc } = this.props;
    if (goodsTradeSpuDesc !== nextProps.goodsTradeSpuDesc) {
      this.setState({
        nativeData: [...nextProps.goodsTradeSpuDesc],
      });
    }
  }

  descTypeLabel = text => {
    if (!text) return '';
    const { parmPublicParameterProductDesc } = this.props;
    const findVal = parmPublicParameterProductDesc.find(v => v.parmCode === text);
    return findVal ? findVal.parmValue : '';
  };

  /** 类型下拉框 */
  typeSelect = () => {
    const { parmPublicParameterProductDesc } = this.props;
    return (
      <Select style={{ width: '100%', minWidth: 80 }}>
        {parmPublicParameterProductDesc.map(v => (
          <Select.Option value={v.parmCode} key={v.id}>
            {v.parmValue}
          </Select.Option>
        ))}
      </Select>
    );
  };

  /**
   * 删除
   */
  handleDelete = key => {
    console.log('删除', key);
    const { dispatch } = this.props;
    dispatch({
      type: 'goodstradespuinfomanage/deleteGoodsTradeSpuDesc',
      payload: [key],
      callback: () => {
        message.success(formatMessage({ id: 'form.common.delete.tips' }));
        this.onDelete(key);
      },
    });
  };

  handleSave = (index, row, list, type) => {
    const { dispatch } = this.props;
    const { descTypeCode, descName, address, seq, ...other } = list[index];
    const params = {
      descTypeCode,
      descName,
      address,
      seq,
    };
    if (type === 'add') {
      // 新增
      dispatch({
        type: 'goodstradespuinfomanage/addGoodsTradeSpuDesc',
        payload: { ...params },
        callback: res => {
          if (res !== undefined && res !== null && res.code === 0) {
            message.success(formatMessage({ id: 'form.common.save.tips' }));
            const { data } = res;
            list.splice(index, 1, { ...data });
            this.onSave(index, row, list);
          }
        },
      });
    }
    if (type === 'edit') {
      // 编辑
      dispatch({
        type: 'goodstradespuinfomanage/modifyGoodsTradeSpuDesc',
        payload: {
          ...other,
          ...params,
        },
        callback: res => {
          if (res !== undefined && res !== null && res.code === 0) {
            message.success(formatMessage({ id: 'form.common.update.tips' }));
            const { data } = res;
            list.splice(index, 1, { ...data });
            this.onSave(index, row, list);
          }
        },
      });
    }
  };

  handleAddRow = () => {
    const { nativeData } = this.state;
    let nextSeq = 0;
    if (nativeData.length > 0) {
      nextSeq = nativeData[nativeData.length - 1].seq || 0;
    }
    this.newAddRow({ ...initRowData, seq: nextSeq + 1 }, nativeData);
  };

  render() {
    const { spuType } = this.props;
    const { editingKey, tableColumns, rowKey, nativeData } = this.state;
    const otherButton = {
      value: 'remove',
      label: formatMessage({ id: 'button.common.delete' }),
      onClick: this.handleDelete,
    };
    return (
      <div style={{ maxWidth: 700 }}>
        {spuType === 'edit' ? (
          <span>
            <EditableTableBlock
              rowKey={rowKey}
              columns={tableColumns}
              dataList={nativeData}
              otherButton={otherButton}
              onSave={this.handleSave}
              onCanCel={this.onCanCel}
              editingKey={editingKey}
              editingKeyChange={this.editingKeyChange}
            />
            <Button
              icon="plus"
              className={styles.rowAdd}
              type="primary"
              block
              onClick={() => this.handleAddRow()}
            >
              {formatMessage({ id: 'button.common.add' })}
            </Button>
          </span>
        ) : (
          <span>
            <Table
              rowKey="id"
              className="small-margin-common-table"
              pagination={false}
              columns={tableColumns}
              dataSource={nativeData}
            />
          </span>
        )}
      </div>
    );
  }
}

export default connect(({ goodstradespuinfomanage, parmpublicparametermanage }) => ({
  parmPublicParameterProductDesc: parmpublicparametermanage.parmPublicParameterProductDesc || [], // 产品描述类型
  goodsTradeSpuDesc: goodstradespuinfomanage.goodsTradeSpuDesc || [], // 产品描述
}))(DescribeCard);
