import React from 'react';
import { connect } from 'dva';
import { Card } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import BasicInfoCard from './BasicInfoCard';
import SkuInfoCard from './SkuInfoCard';
import ProductModelCard from './ProductModelCard';
import CommonAttrCard from './CommonAttrCard';
import DescribeCard from './DescribeCard';
import DependenceAttr from './DependenceAttrCard';
import styles from './index.less';

moment.locale('zh-cn');

class SpuTab extends React.Component {
  formObj = {
    basicInfo: null,
    skuInfo: null,
    productModel: null,
  };

  /**
   * 产品所有表单属性列表
   */
  cardList = [
    {
      key: 'basicInfo',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.basicInfo' }),
      isEdit: true,
      cardRender: () => (
        <BasicInfoCard
          wrappedComponentRef={form => this.getForm('basicInfo', form)}
          {...this.props}
        />
      ),
    },
    {
      key: 'skuInfo',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.skuInfo' }),
      isEdit: false,
      cardRender: () => (
        <SkuInfoCard wrappedComponentRef={form => this.getForm('skuInfo', form)} {...this.props} />
      ),
    },
    {
      key: 'dependenceAttr',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.dependenceAttr' }),
      cardRender: () => (
        <DependenceAttr
          wrappedComponentRef={form => this.getForm('dependenceAttr', form)}
          {...this.props}
        />
      ),
    },
    {
      key: 'commonAttr',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.commonAttr' }),
      isEdit: true,
      cardRender: () => (
        <CommonAttrCard
          {...this.props}
          // disabled={false}
          wrappedComponentRef={form => this.getForm('commonAttr', form)}
        />
      ),
    },
    {
      key: 'productModel',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.productModel' }),
      isEdit: false,
      cardRender: () => (
        <ProductModelCard
          {...this.props}
          wrappedComponentRef={form => this.getForm('productModel', form)}
        />
      ),
    },
    {
      key: 'describe',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.describe' }),
      isEdit: true,
      cardRender: () => (
        <DescribeCard
          {...this.props}
          wrappedComponentRef={form => this.getForm('describe', form)}
        />
      ),
    },
  ];

  getForm = (key, form) => {
    this.formObj[key] = form;
  };

  render() {
    const { spuType } = this.props;
    const titleFnc = item => {
      const editPrompt = spuType === 'edit' ? '(可修改)' : '';
      if (item.isEdit && editPrompt) {
        return (
          <span className={styles.spuTypeTitle}>
            {item.title}
            {editPrompt}
          </span>
        );
      }
      return `${item.title}`;
    };
    return (
      <div className={styles.productCont}>
        {this.cardList.map(item => (
          <Card
            title={titleFnc(item)}
            key={item.key}
            bordered={false}
            headStyle={{ paddingLeft: 0, minHeight: 30 }}
          >
            {item.cardRender()}
          </Card>
        ))}
      </div>
    );
  }
}

export default connect(
  ({ goodsapplybillmanage, parmpublicparametermanage, goodstradespuinfomanage }) => ({
    goodsCatagoryListAll: goodsapplybillmanage.goodsCatagoryListAll || [], // 产品类目
    goodsClassificationListAll: goodsapplybillmanage.goodsClassificationListAll || [], // 产品大类
    goodsbrandListAll: goodsapplybillmanage.goodsbrandListAll || [], // 产品品牌
    parmPublicParameterProductType: parmpublicparametermanage.parmPublicParameterProductType || [], // 产品类别
    goodsTradeSpuBasicInfo: goodstradespuinfomanage.goodsTradeSpuBasicInfo || {}, // 产品基本信息
    goodsTradeSpuBasicInfoValid: goodstradespuinfomanage.goodsTradeSpuBasicInfoValid || {}, // 产品基本信息（正式生效的数据）
    goodsTradeSpuSkuInfo: goodstradespuinfomanage.goodsTradeSpuSkuInfo || [], // 产品sku信息
    goodsTradeSpuModel: goodstradespuinfomanage.goodsTradeSpuModel || [], // 产品型号
    goodsTradeSpuCommonAttr: goodstradespuinfomanage.goodsTradeSpuCommonAttr || [], // 产品普通属性
    goodsTradeSpuSkuAttrListAble: goodstradespuinfomanage.goodsTradeSpuSkuAttrListAble || [], // 产品sku属性下拉列表数据(可用)
    goodsTradeSpuDesc: goodstradespuinfomanage.goodsTradeSpuDesc || [], // 产品描述
    parmPublicParameterProductDesc: parmpublicparametermanage.parmPublicParameterProductDesc || [], // 产品描述类型
    goodsTradeSpuSKuGroupAttr: goodstradespuinfomanage.goodsTradeSpuSKuGroupAttr || {}, // 产品商品分组从属属性
  })
)(SpuTab);
