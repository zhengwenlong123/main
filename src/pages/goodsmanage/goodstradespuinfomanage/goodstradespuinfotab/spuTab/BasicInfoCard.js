import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Form, Input, Descriptions, message } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import { getTreeParent } from '@/utils/utils';
import styles from './index.less';

moment.locale('zh-cn');

/**
 * 产品申请单新增：产品基本信息部分组件
 */
function BasicInfoCard(props, ref) {
  const {
    form,
    dispatch,
    goodsCatagoryListAll,
    goodsClassificationListAll,
    goodsbrandListAll = [],
    parmPublicParameterProductType,
    goodsTradeSpuBasicInfo,
    goodsTradeSpuBasicInfoValid,
    spuType, // 操作类型
    // goodsApplyBillDetailInfo,
    // disabled, // 是否可修改
  } = props;

  useImperativeHandle(ref, () => ({
    form,
  }));

  const [descError, setDescError] = useState(false);
  const [catagoryList, setCatagoryList] = useState(goodsCatagoryListAll);
  const [classificationList, setClassificationList] = useState(goodsClassificationListAll);
  const [goodsbrandList, setGoodsbrandList] = useState([...goodsbrandListAll]);
  const [goodsTypeList, setGoodsTypeList] = useState(parmPublicParameterProductType);

  // 监听并更新产品类目变化
  useEffect(() => {
    setCatagoryList([...goodsCatagoryListAll]);
  }, [goodsCatagoryListAll]);
  // 监听并更新产品大类变化
  useEffect(() => {
    setClassificationList([...goodsClassificationListAll]);
  }, [goodsClassificationListAll]);
  // 监听并更新产品品牌变化
  useEffect(() => {
    setGoodsbrandList([...goodsbrandListAll]);
  }, [goodsbrandListAll]);
  // 监听并更新产品类型变化
  useEffect(() => {
    setGoodsTypeList([...parmPublicParameterProductType]);
  }, [parmPublicParameterProductType]);
  // 监听产品信息变化
  useEffect(() => {
    setDescError(false);
  }, [goodsTradeSpuBasicInfo]);

  const { getFieldDecorator } = form;

  const brandLabel = brandCode => {
    if (!brandCode) return '';
    const findVal = goodsbrandList.find(v => v.brandCode === brandCode);
    return findVal ? findVal.brandName : '';
  };
  const mainClassLabel = mainClassCode => {
    if (!mainClassCode) return '';
    const findVal = classificationList.find(v => v.mainClassCode === mainClassCode);
    return findVal ? findVal.mainClassName : '';
  };
  const typeLabel = spuTypeCode => {
    if (!spuTypeCode) return '';
    const findVal = goodsTypeList.find(v => v.parmCode === spuTypeCode);
    return findVal ? findVal.parmValue : '';
  };
  const catagoryLabel = catagoryCode => {
    if (!catagoryCode) return '';
    const catagoryCodeArr =
      getTreeParent(catagoryList, catagoryCode, 'catagoryCode', 'parentCode') || [];
    // goodsTradeSpuBasicInfo.catagoryCode = catagoryCodeArr.map(v => v.catagoryCode) || [];
    return catagoryCodeArr.map(v => v.catagoryName).join('/');
  };
  const handleReset = () => {
    dispatch({
      type: 'goodstradespuinfomanage/changeGoodsTradeSpuBasicInfo',
      payload: {
        ...goodsTradeSpuBasicInfoValid,
      },
    });
  };

  const handleBlur = () => {
    dispatch({
      type: 'goodstradespuinfomanage/modifyGoodsTradeSpuBasicInfo',
      callback: res => {
        if (res !== undefined && res !== null && res.code === 0) {
          message.success('保存成功');
          setDescError(false);
        } else {
          setDescError(true);
        }
      },
    });
  };

  const formRender = () => (
    <Form layout="horizontal" labelAlign="right">
      <Descriptions column={3}>
        <Descriptions.Item
          span={3}
          label={formatMessage({ id: 'form.goodsApplyBillManage.product.name' })}
        >
          {(goodsTradeSpuBasicInfo && goodsTradeSpuBasicInfo.spuName) || ''}
        </Descriptions.Item>
        <Descriptions.Item
          label={formatMessage({ id: 'form.goodsApplyBillManage.product.shortName' })}
        >
          {(goodsTradeSpuBasicInfo && goodsTradeSpuBasicInfo.spuShortName) || ''}
        </Descriptions.Item>
        <Descriptions.Item label={formatMessage({ id: 'form.goodsApplyBillManage.product.brand' })}>
          {brandLabel(goodsTradeSpuBasicInfo && goodsTradeSpuBasicInfo.brandCode)}
        </Descriptions.Item>
        <Descriptions.Item label={formatMessage({ id: 'form.goodsApplyBillManage.product.type' })}>
          {typeLabel(goodsTradeSpuBasicInfo && goodsTradeSpuBasicInfo.spuTypeCode)}
        </Descriptions.Item>
        <Descriptions.Item
          label={formatMessage({ id: 'form.goodsApplyBillManage.product.marketTime' })}
        >
          {goodsTradeSpuBasicInfo && goodsTradeSpuBasicInfo.spuTimeToMarket
            ? moment(goodsTradeSpuBasicInfo.spuTimeToMarket).format('YYYY-MM-DD')
            : ''}
        </Descriptions.Item>
        <Descriptions.Item
          label={formatMessage({ id: 'form.goodsApplyBillManage.product.tegory' })}
        >
          {(goodsTradeSpuBasicInfo && catagoryLabel(goodsTradeSpuBasicInfo.catagoryCode)) || ''}
        </Descriptions.Item>
        <Descriptions.Item
          label={formatMessage({ id: 'form.goodsApplyBillManage.product.mainClassCode' })}
        >
          {mainClassLabel(goodsTradeSpuBasicInfo && goodsTradeSpuBasicInfo.mainClassCode)}
        </Descriptions.Item>
        <Descriptions.Item
          span={3}
          label={formatMessage({ id: 'form.goodsApplyBillManage.product.synopsis' })}
          className={styles.synopsis}
        >
          {spuType === 'edit' ? (
            <Form.Item>
              {getFieldDecorator('spuDesc')(
                <Input.TextArea onBlur={handleBlur} style={{ width: '100%' }} />
              )}
              {descError ? (
                <span className={styles.errorPrompt}>
                  保存失败：是否继续保存，恢复即将恢复修改前数据？
                  <a onClick={handleBlur}>保存</a>
                  <a onClick={handleReset}>恢复</a>
                </span>
              ) : (
                <span className={styles.blurPrompt}>鼠标光标失焦将自动保存简介内容</span>
              )}
            </Form.Item>
          ) : (
            <span>{(goodsTradeSpuBasicInfo && goodsTradeSpuBasicInfo.spuDesc) || ''}</span>
          )}
        </Descriptions.Item>
      </Descriptions>
    </Form>
  );

  return <div className={styles.goodsapplybilltabBasicInfo}>{formRender()}</div>;
}

export default Form.create({
  onValuesChange({ dispatch, goodsTradeSpuBasicInfo }, changedValues) {
    dispatch({
      type: 'goodstradespuinfomanage/changeGoodsTradeSpuBasicInfo',
      payload: {
        ...goodsTradeSpuBasicInfo,
        ...changedValues,
      },
    });
  },
  mapPropsToFields(props) {
    const { goodsTradeSpuBasicInfo } = props;
    return {
      spuDesc: Form.createFormField({
        value: goodsTradeSpuBasicInfo ? goodsTradeSpuBasicInfo.spuDesc : '',
      }),
    };
  },
})(React.forwardRef(BasicInfoCard));
