import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Row, Col, Form, Input, Select } from 'antd';
import moment from 'moment';
import uuid from 'uuid';
import { formatMessage } from 'umi/locale';
import UploadFileDirect from '@/components/UploadBasic/UploadFileDirect';
import DynamicFormSet from '../../components/DynamicFormSet';
// import SpanFormShow from '../../components/SpanFormShow';
// import styles from './index.less';

moment.locale('zh-cn');
const registerField = () => ({
  descTypeCode: '',
  descName: '',
  address: '',
});
/**
 * 产品申请单新增：产品描述部分组件
 */
function DescribeCard(props, ref) {
  const {
    form,
    // dispatch,
    // disabled,
    goodsTradeSpuDesc,
    parmPublicParameterProductDesc,
    operationType,
  } = props;
  const seeFlag = operationType === 'see'; // 查看详情
  useImperativeHandle(ref, () => ({
    form,
  }));
  const [isShowSave, setIsShowSave] = useState(false);
  const [initList, setInitList] = useState([{ id: uuid.v4(), ...registerField() }]);

  useEffect(() => {
    const arr = [...goodsTradeSpuDesc].map(item => ({
      ...item,
      id: uuid.v4(),
    }));
    if (arr && arr.length > 0) {
      setInitList(arr);
    }
  }, [goodsTradeSpuDesc]);

  const { getFieldDecorator } = form;
  const formItemLayout =
    operationType !== 'audit'
      ? {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
            xl: { span: 6 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
            xl: { span: 18 },
          },
        }
      : {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 10 },
            xl: { span: 8 },
            xll: { span: 6 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 14 },
            xl: { span: 16 },
            xll: { span: 18 },
          },
        };
  const colLayout =
    operationType !== 'audit'
      ? {
          xs: 24,
          sm: 12,
          xl: 8,
        }
      : {
          xs: 24,
          sm: 12,
          xll: 8,
        };
  const attachLayout =
    operationType !== 'audit'
      ? {
          wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 20, offset: 4 },
            xl: { span: 19, offset: 2 },
          },
        }
      : {
          wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 18, offset: 6 },
            xl: { span: 19, offset: 4 },
            xll: { span: 19, offset: 2 },
          },
        };
  // 描述类型查找
  // const descTypeNameHandle = descTypeCode => {
  //   const findVal = parmPublicParameterProductDesc.find(v => v.parmCode === descTypeCode);
  //   const obj = {
  //     key: descTypeCode,
  //     label: (findVal && findVal.parmValue) || '',
  //   };
  //   return obj;
  // };
  // 普通属性发生改动后，触发显示保存按钮
  const handleSaveStatus = () => {
    if (!isShowSave) {
      setIsShowSave(true);
    }
  };
  const typeChange = val => {
    console.log('typeChange', val);
    handleSaveStatus();
  };
  const nameChange = val => {
    console.log('nameChange', val);
    handleSaveStatus();
  };
  /**
   * 文件上传UploadFile组件回调函数
   */
  const uploadFileDirectChange = (url, key) => {
    const keys = form.getFieldValue('keys');
    const index = keys.findIndex(item => item.id === key);
    if (index > -1) {
      keys[index].url = url;
    }
    form.setFieldsValue({
      keys,
    });
    console.log('uploadFileDirectChange', url);
    handleSaveStatus();
  };
  // 保存触发函数
  const handleSave = () => {
    console.log('保存');
    form.validateFields(err => {
      if (!err) {
        const arr = form.getFieldValue('describe');
        console.log('保存数据', arr);

        setIsShowSave(false);
      }
    });
  };
  const formItemInvalid = (k, index, removeRender) => (
    <Row key={k.id}>
      <Col {...colLayout}>
        <Form.Item
          {...formItemLayout}
          label={`${formatMessage({ id: 'form.common.description' })}${index + 1}`}
          key={k.id}
        >
          {getFieldDecorator(`describe[${index}].descTypeCode`, {
            validateTrigger: ['onChange', 'onBlur'],
            initialValue: k.descTypeCode || '',
            rules: [
              {
                required: false,
                message: formatMessage({ id: 'form.common.descriptionType.placeholder' }),
              },
            ],
          })(
            <Select style={{ width: '80%', marginRight: 8 }} onChange={typeChange}>
              {parmPublicParameterProductDesc.map(v => (
                <Select.Option value={v.parmCode} key={v.id}>
                  {v.parmValue}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col {...colLayout}>
        <Form.Item {...formItemLayout} key={k.id}>
          {getFieldDecorator(`describe[${index}].descName`, {
            validateTrigger: ['onChange', 'onBlur'],
            initialValue: k.descName || '',
            rules: [
              {
                required: false,
                message: formatMessage({ id: 'form.common.descriptionName.placeholder' }),
              },
            ],
          })(
            <Input
              placeholder={formatMessage({ id: 'form.common.name.placeholder' })}
              style={{ width: '80%', marginRight: 8 }}
              onChange={nameChange}
            />
          )}
          {removeRender()}
        </Form.Item>
      </Col>
      <Col span={24}>
        <Form.Item {...attachLayout}>
          {getFieldDecorator(`describe[${index}].address`, {
            initialValue: k.address || '',
            // validateTrigger: ['onChange', 'onBlur'],
            // rules: [
            //   {
            //     required: false,
            //     // whitespace: true,
            //     message: "Please input passenger's name or delete this field.",
            //   },
            // ],
          })(
            <UploadFileDirect
              onChange={uploadFileDirectChange}
              uploadAccept="image/*"
              uploadSign={k.id}
            />
          )}
        </Form.Item>
      </Col>
    </Row>
  );
  // const formItemValid = (k, index) => (
  //   <Row key={k.id}>
  //     <Col {...colLayout}>
  //       <Form.Item
  //         {...formItemLayout}
  //         label={`${formatMessage({ id: 'form.common.description' })}${index + 1}`}
  //         key={k.id}
  //       >
  //         {getFieldDecorator(`describe[${index}].descTypeCode`, {
  //           validateTrigger: ['onChange', 'onBlur'],
  //           initialValue: descTypeNameHandle(k.descTypeCode),
  //           rules: [
  //             {
  //               required: false,
  //               message: formatMessage({ id: 'form.common.descriptionType.placeholder' }),
  //             },
  //           ],
  //         })(<SpanFormShow.SpanSelectShow />)}
  //       </Form.Item>
  //     </Col>
  //     <Col {...colLayout}>
  //       <Form.Item {...formItemLayout} key={k.id}>
  //         {getFieldDecorator(`describe[${index}].descName`, {
  //           validateTrigger: ['onChange', 'onBlur'],
  //           initialValue: k.descName || '',
  //           rules: [
  //             {
  //               required: false,
  //               message: formatMessage({ id: 'form.common.descriptionName.placeholder' }),
  //             },
  //           ],
  //         })(<SpanFormShow.SpanInputShow />)}
  //       </Form.Item>
  //     </Col>
  //     <Col span={24}>
  //       <Form.Item {...attachLayout}>
  //         {getFieldDecorator(`describe[${index}].address`, {
  //           initialValue: k.address || '',
  //         })(
  //           <UploadFileDirect
  //             onChange={uploadFileDirectChange}
  //             uploadAccept="image/*"
  //             uploadSign={k.id}
  //             uploadIsDisable
  //           />
  //         )}
  //       </Form.Item>
  //     </Col>
  //   </Row>
  // );
  const formItem = (k, index, removeRender) => {
    return formItemInvalid(k, index, removeRender);
  };
  return (
    <DynamicFormSet
      fieldName="describe"
      initialValue={initList}
      registerField={registerField()}
      formItem={formItem}
      formItemLayoutAddBtn={{ ...attachLayout }}
      addDisabled={seeFlag}
      onSave={isShowSave && handleSave}
      {...props}
    />
  );
}

export default Form.create({
  // onValuesChange: (props, changedValues, allValues) => {
  //   const { dispatch, goodsRfSpuDescList } = props;
  //   const { keys, describe } = allValues;
  //   let arr = [...goodsRfSpuDescList];
  //   if (changedValues.describe && describe.length - keys.length === 0) {
  //     // 说明当前是item节点数据改变
  //     changedValues.describe.forEach((item, index) => {
  //       arr[index] = { ...registerField(), ...goodsRfSpuDescList[index], ...item };
  //     });
  //   }
  //   if (changedValues.describe && describe.length - keys.length > 0) {
  //     // 说明当前是row行减少（即删除）
  //     const current = changedValues.describe.filter(v => v.descTypeCode && v.descName);
  //     arr = [...goodsRfSpuDescList].filter(x =>
  //       [...current].some(
  //         y =>
  //           y.descTypeCode === x.descTypeCode &&
  //           y.descName === x.descName &&
  //           y.address === x.address
  //       )
  //     );
  //   }
  //   dispatch({
  //     // 保存的都是有效数据
  //     type: 'goodsapplybillmanage/changeGoodsRfSpuDescList',
  //     payload: [...arr],
  //   });
  // },
})(React.forwardRef(DescribeCard));
