import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Descriptions } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import EditableTagGroupModal from '@/components/EditableTagGroup/EditableTagGroupModal';
import styles from './index.less';

moment.locale('zh-cn');

/**
 * 产品信息：sku信息
 */
function SkuInfoCard(props, ref) {
  const { form, goodsTradeSpuSkuInfo } = props;

  useImperativeHandle(ref, () => ({
    form,
  }));

  const [skuInfoList, setSkuInfoList] = useState([...goodsTradeSpuSkuInfo]);

  useEffect(() => {
    setSkuInfoList([...goodsTradeSpuSkuInfo]);
  }, [goodsTradeSpuSkuInfo]);

  const formRender = (item, index) => (
    <Descriptions key={index}>
      <Descriptions.Item
        span={3}
        label={`${formatMessage({ id: 'form.common.apply.attr' })}${index + 1}`}
      >
        {`${(item.attr && item.attr.skuAttrName) || ''}(${(item.attr && item.attr.skuAttrCode) ||
          ''})`}
      </Descriptions.Item>
      <Descriptions.Item span={3}>
        <EditableTagGroupModal
          addDisabled
          value={item.tags}
          filterClosable={() => {
            return false;
          }}
        />
      </Descriptions.Item>
    </Descriptions>
  );

  return (
    <div className={styles.goodsapplybilltabBasicInfo}>
      {skuInfoList.map((v, index) => formRender(v, index))}
    </div>
  );
}

export default React.forwardRef(SkuInfoCard);
