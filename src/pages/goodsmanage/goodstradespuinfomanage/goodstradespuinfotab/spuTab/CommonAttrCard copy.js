import React, { useImperativeHandle, useState, useEffect } from 'react';
import { Form, Input, Row, Col, Select } from 'antd';
import moment from 'moment';
import uuid from 'uuid';
import { formatMessage } from 'umi/locale';
import DynamicFormSet from '../../components/DynamicFormSet';
// import SpanFormShow from '../../components/SpanFormShow';
// import styles from './index.less';

moment.locale('zh-cn');
const registerField = () => ({
  attr: { key: '', label: '' },
  attrValue: '',
});
/**
 * 产品申请单新增：产品普通属性部分组件
 */

function CommonAttrCard(props, ref) {
  const {
    // dispatch,
    form,
    // goodsAttrNameAble=[],
    // disabled,
    operationType,
    goodsTradeSpuSkuAttrListAble,
    goodsTradeSpuCommonAttr,
  } = props;
  const { getFieldDecorator } = form;
  const seeFlag = operationType === 'see'; // 查看详情
  useImperativeHandle(ref, () => ({
    form,
  }));

  const [isShowSave, setIsShowSave] = useState(false);
  const [attrListAll, setAttrListAll] = useState([]);
  const [initList, setInitList] = useState([{ id: uuid.v4(), ...registerField() }]);

  // 监听并更新属性集变化
  useEffect(() => {
    setAttrListAll([...goodsTradeSpuSkuAttrListAble]);
  }, [goodsTradeSpuSkuAttrListAble]);

  useEffect(() => {
    const arr = [...goodsTradeSpuCommonAttr].map(item => {
      const attr = {
        key: item.attrCode,
        label: item.attrName,
      };
      return { ...item, attr, attrValue: item.attrValue, id: uuid.v4() };
    });
    if (arr && arr.length > 0) {
      setInitList(arr);
    }
  }, [goodsTradeSpuCommonAttr]);

  const formItemLayout =
    operationType !== 'audit'
      ? {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
            xl: { span: 6 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
            xl: { span: 18 },
          },
        }
      : {
          labelCol: {
            xs: { span: 24 },
            sm: { span: 10 },
            xl: { span: 8 },
            xll: { span: 6 },
          },
          wrapperCol: {
            xs: { span: 24 },
            sm: { span: 14 },
            xl: { span: 16 },
            xll: { span: 18 },
          },
        };
  const colLayout =
    operationType !== 'audit'
      ? {
          xs: 24,
          sm: 12,
          xl: 8,
        }
      : {
          xs: 24,
          sm: 12,
          xll: 8,
        };
  const addBtnLayout =
    operationType === 'audit'
      ? {
          wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 18, offset: 6 },
            xl: { span: 19, offset: 4 },
            xll: { span: 19, offset: 2 },
          },
        }
      : undefined;
  // 判断是否校验设定值必填： 属性不为空必填
  const validatorSetVal = index => {
    const { attr } = form.getFieldValue('commonAttr')[index];
    if (attr && attr.key) {
      return true;
    }
    return false;
  };

  // 普通属性发生改动后，触发显示保存按钮
  const handleSaveStatus = () => {
    if (!isShowSave) {
      setIsShowSave(true);
    }
  };

  const selectChange = (index, val = {}) => {
    const commonAttr = form.getFieldValue('commonAttr');
    console.log('selectChange', index, val, commonAttr);
    commonAttr.splice(index, 1, {
      attr: { key: (val && val.key) || '', label: (val && val.label) || '' },
      attrValue: '',
    });
    form.setFieldsValue({
      commonAttr,
    });
    handleSaveStatus();
  };
  const configValChange = (index, val) => {
    console.log('configValChange', index, val);
    handleSaveStatus();
  };

  /**
   * 处理移除后函数
   * 参数 k: 移除数据的标识， record：移除的keys数据， index：移除的索引位置， current：当前移除的数据
   */
  const handleRemove = (k, record, index, current) => {
    console.log('handleRemove', k, record, index, current);
    if (current && current.attr && !current.attr.key && !current.attrValue) {
      // 移除有效数据
      handleSaveStatus();
    }
  };
  const handleSave = () => {
    console.log('保存');
    form.validateFields(err => {
      if (!err) {
        const arr = form.getFieldValue('commonAttr');
        console.log('保存数据', arr);

        setIsShowSave(false);
      }
    });
  };
  // const formItemValid = (
  //   k,
  //   index // 已生效
  // ) => (
  //   <Row key={k.id}>
  //     <Col {...colLayout}>
  //       <Form.Item
  //         {...formItemLayout}
  //         label={`${formatMessage({ id: 'form.common.apply.attr' })}${index + 1}`}
  //       >
  //         {getFieldDecorator(`commonAttr[${index}].attr`, {
  //           validateTrigger: ['onChange', 'onBlur'],
  //           initialValue: { ...k.attr },
  //           rules: [
  //             {
  //               required: false,
  //             },
  //           ],
  //         })(<SpanFormShow.SpanSelectShow />)}
  //       </Form.Item>
  //     </Col>
  //     <Col {...colLayout}>
  //       <Form.Item
  //         {...formItemLayout}
  //         label={`${formatMessage({ id: 'form.common.apply.setValue' })}${index + 1}`}
  //       >
  //         {getFieldDecorator(`commonAttr[${index}].attrValue`, {
  //           validateTrigger: ['onChange', 'onBlur'],
  //           initialValue: k.attrValue || '',
  //           rules: [
  //             {
  //               required: validatorSetVal(index),
  //               message: formatMessage({ id: 'form.goodsApplyBillManage.message.attrValue' }),
  //             },
  //           ],
  //         })(<SpanFormShow.SpanInputShow />)}
  //       </Form.Item>
  //     </Col>
  //   </Row>
  // );
  const formItemInValid = (
    k,
    index,
    removeRender // 申请单
  ) => (
    <Row key={k.id}>
      <Col {...colLayout}>
        <Form.Item
          {...formItemLayout}
          label={`${formatMessage({ id: 'form.common.apply.attr' })}${index + 1}`}
        >
          {getFieldDecorator(`commonAttr[${index}].attr`, {
            validateTrigger: ['onChange', 'onBlur'],
            initialValue: { ...k.attr },
            rules: [
              {
                required: false,
              },
            ],
          })(
            <Select
              style={{ width: '80%', marginRight: 8 }}
              allowClear
              labelInValue
              onChange={val => selectChange(index, val)}
            >
              {attrListAll.map(v => (
                <Select.Option value={v.attrCode} key={v.id}>
                  {v.attrName}
                </Select.Option>
              ))}
            </Select>
          )}
        </Form.Item>
      </Col>
      <Col {...colLayout}>
        <Form.Item
          {...formItemLayout}
          label={`${formatMessage({ id: 'form.common.apply.setValue' })}${index + 1}`}
        >
          {getFieldDecorator(`commonAttr[${index}].attrValue`, {
            validateTrigger: ['onChange', 'onBlur'],
            initialValue: k.attrValue || '',
            rules: [
              {
                required: validatorSetVal(index),
                message: formatMessage({ id: 'form.goodsApplyBillManage.message.attrValue' }),
              },
            ],
          })(
            <Input
              style={{ width: '80%', marginRight: 8 }}
              onChange={val => configValChange(index, val)}
            />
          )}
          {removeRender()}
        </Form.Item>
      </Col>
    </Row>
  );
  const formItem = (k, index, removeRender) => {
    return formItemInValid(k, index, removeRender);
    // const valid = k.rfStage === 1; // 已生效的
    // return seeFlag
    //   ? formItemValid(k, index)
    //   : formItemInValid(k, index, removeRender);
  };
  return (
    <DynamicFormSet
      fieldName="commonAttr"
      initialValue={initList}
      registerField={registerField()}
      formItem={formItem}
      addDisabled={seeFlag}
      formItemLayoutAddBtn={addBtnLayout}
      onRemove={handleRemove}
      onSave={isShowSave && handleSave}
      {...props}
    />
  );
}
export default Form.create({
  // onValuesChange: (props, changedValues, allValues) => {
  //   const { dispatch, goodsRfSpuAttrNormalList, goodsSkuAttrUsed } = props;
  //   const { keys, commonAttr } = allValues;
  //   let arr = [...goodsRfSpuAttrNormalList];
  //   if (changedValues.commonAttr && commonAttr.length - keys.length === 0) {
  //     // 说明当前是item节点数据改变
  //     changedValues.commonAttr.forEach((item, index) => {
  //       arr[index] = { ...registerField(), ...goodsRfSpuAttrNormalList[index], ...item };
  //     });
  //   }
  //   if (changedValues.commonAttr && commonAttr.length - keys.length > 0) {
  //     // 说明当前是row行减少（即删除）
  //     arr = changedValues.commonAttr.filter(v => v.attr && v.attr.key);
  //   }
  //   dispatch({
  //     // 保存的都是有效数据
  //     type: 'goodsapplybillmanage/changeGoodsRfSpuAttrNormalList',
  //     payload: [...arr],
  //   });
  //   dispatch({
  //     // 属性发生改变后，过滤可用sku属性保存起来
  //     type: 'goodsapplybillmanage/changeGoodsAttrNameAble',
  //     payload: [...arr.map(v => v.attr.key), ...goodsSkuAttrUsed.map(v => v.attr.key)],
  //   });
  // },
})(React.forwardRef(CommonAttrCard));
