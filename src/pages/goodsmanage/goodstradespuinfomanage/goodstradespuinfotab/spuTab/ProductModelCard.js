import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Descriptions } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import styles from './index.less';

moment.locale('zh-cn');

/**
 * 产品信息：产品型号
 */
function ProductModelCard(props, ref) {
  const { form, goodsTradeSpuModel } = props;

  useImperativeHandle(ref, () => ({
    form,
  }));

  const [productModelList, setProductModelList] = useState([...goodsTradeSpuModel]);

  useEffect(() => {
    setProductModelList([...goodsTradeSpuModel]);
  }, [goodsTradeSpuModel]);

  const formRender = (item, index) => (
    <Descriptions key={item.modelCode}>
      <Descriptions.Item
        span={3}
        label={`${formatMessage({ id: 'form.goodsSpuInfoUpkeepBasic.modelCode' })}${index + 1}`}
      >
        {`${item.modelName || item.modelCode || ''}`}
      </Descriptions.Item>
    </Descriptions>
  );

  return (
    <div className={styles.goodsapplybilltabBasicInfo}>
      {productModelList.map((v, index) => formRender(v, index))}
    </div>
  );
}

export default React.forwardRef(ProductModelCard);
