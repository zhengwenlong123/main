import React from 'react';
import { connect } from 'dva';
import { Card } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import BasicInfoCard from './BasicInfoCard';
import SkuInfoCard from './SkuInfoCard';
import CommonAttrCard from './CommonAttrCard';
import DescribeCard from './DescribeCard';
import styles from './index.less';

moment.locale('zh-cn');

class GoodsTradeSkuInfoManage extends React.Component {
  formObj = {
    basicInfo: null,
    skuInfo: null,
    productModel: null,
  };

  /**
   * 产品所有表单属性列表
   */
  cardList = [
    {
      key: 'basicInfo',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.basicInfo' }),
      isEdit: true,
      cardRender: () => (
        <BasicInfoCard
          wrappedComponentRef={form => this.getForm('basicInfo', form)}
          {...this.props}
        />
      ),
    },
    {
      key: 'skuInfo',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.skuInfo' }),
      cardRender: () => (
        <SkuInfoCard wrappedComponentRef={form => this.getForm('skuInfo', form)} {...this.props} />
      ),
    },
    {
      key: 'commonAttr',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.commonAttr' }),
      isEdit: true,
      cardRender: () => (
        <CommonAttrCard
          {...this.props}
          // disabled={false}
          wrappedComponentRef={form => this.getForm('commonAttr', form)}
        />
      ),
    },
    {
      key: 'describe',
      title: formatMessage({ id: 'form.goodsApplyBillManage.product.describe' }),
      isEdit: true,
      cardRender: () => (
        <DescribeCard
          {...this.props}
          wrappedComponentRef={form => this.getForm('describe', form)}
        />
      ),
    },
  ];

  getForm = (key, form) => {
    this.formObj[key] = form;
  };

  render() {
    const { operateType } = this.props;
    const titleFnc = item => {
      // const val = this.cardTitleMessage[item.key][goodsRfInfo.rfTypeCode].message;
      if (operateType === 'edit') {
        const spanRender = !item.isEdit ? (
          `${item.title}`
        ) : (
          <span className={styles.skuTypeTitle}>{item.title}(可修改)</span>
        );
        return spanRender;
      }
      return `${item.title}`;
    };
    return (
      <div className={styles.productCont}>
        {this.cardList.map(item => (
          <Card
            title={titleFnc(item)}
            key={item.key}
            bordered={false}
            headStyle={{ paddingLeft: 0, minHeight: 30 }}
          >
            {item.cardRender()}
          </Card>
        ))}
      </div>
    );
  }
}

export default connect(
  ({ goodsapplybillmanage, parmpublicparametermanage, goodstradespuinfomanage }) => ({
    goodsTradeSkuBasicInfo: goodstradespuinfomanage.goodsTradeSkuBasicInfo || {}, // 商品基本信息
    goodsTradeSkuBasicInfoValid: goodstradespuinfomanage.goodsTradeSkuBasicInfoValid || {}, // 商品基本信息（正式生效数据）
    goodsEbsCatagoryListAll: goodsapplybillmanage.goodsEbsCatagoryListAll || [], // ebs分类
    parmPublicParameterGoodsEbsSubAttr:
      parmpublicparametermanage.parmPublicParameterGoodsEbsSubAttr || [], // 子分类数据
    goodsTradeSkuAttrInfo: goodstradespuinfomanage.goodsTradeSkuAttrInfo || [], // 商品SKU信息
  })
)(GoodsTradeSkuInfoManage);
