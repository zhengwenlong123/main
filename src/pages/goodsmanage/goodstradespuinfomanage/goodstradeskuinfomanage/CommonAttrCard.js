import React from 'react';
import { Select, Input, Button, message, Table } from 'antd';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import EditableTableBasic from '@/components/EditableTable/EditableTableBasic';
import EditableTableBlock from '@/components/EditableTable/EditableTableBlock';
import styles from './index.less';

/**
 * 必填项标题处理
 * @param {*} label 标题
 */
const headerColumntitle = id => (
  <span>
    <span style={{ color: 'red' }}>*</span>
    {formatMessage({ id })}
  </span>
);
// 初始列表行数据‘
const initRowData = {
  attr: { key: '', label: '' },
  attrValue: '',
};

class CommonAttrCard extends EditableTableBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: headerColumntitle('form.goodsTradeSpuInfoManage.sku.commonAttrName'),
        dataIndex: 'attr',
        // width: 200,
        editable: true,
        rules: [
          {
            required: true,
            message: formatMessage({
              id: 'form.goodsTradeSpuInfoManage.sku.commonAttrName.placeholder',
            }),
          },
          { validator: (rule, value, callback) => this.validatorAttr(rule, value, callback) },
        ],
        render: text => <span>{text && text.label}</span>,
        renderInput: () => this.attrSelect(),
      },
      {
        title: headerColumntitle('form.goodsTradeSpuInfoManage.sku.commonAttrName'),
        dataIndex: 'attrValue',
        editable: true,
        rules: [
          {
            required: true,
            message: formatMessage({
              id: 'form.goodsTradeSpuInfoManage.sku.commonAttrName.placeholder',
            }),
          },
        ],
        renderInput: () => <Input style={{ minWidth: 100 }} allowClear />,
      },
    ];
    const validatorDispatch = list => {
      // 格式化数据
      const validList = list.map(v => ({
        ...v,
        attrCode: v.attr.key,
        attrName: v.attr.label,
        attrDesc: v.attr.label,
      }));
      return validList;
    };
    super(props, {
      tableColumns,
      validatorDispatch,
      rowKey: 'id',
      dispatchType: 'goodstradespuinfomanage/changeGoodsTradeSkuCommonAttr',
    });
    this.handled = false;
  }

  componentDidMount() {
    const { goodsTradeSkuCommonAttr } = this.props;
    this.setState({
      nativeData: [...goodsTradeSkuCommonAttr],
    });
  }

  componentWillReceiveProps(nextProps) {
    const { goodsTradeSkuCommonAttr } = this.props;
    if (goodsTradeSkuCommonAttr !== nextProps.goodsTradeSkuCommonAttr) {
      this.setState({
        nativeData: [...nextProps.goodsTradeSkuCommonAttr],
      });
    }
  }

  validatorAttr = (rule, value, callback) => {
    if (value && !value.key) {
      callback(formatMessage({ id: 'form.common.apply.attr.placeholder' }));
    } else {
      callback();
    }
  };

  /** 属性下拉框 */
  attrSelect = () => {
    const { goodsTradeSpuCommonAttr } = this.props;
    return (
      <Select style={{ width: '100%', minWidth: 80 }} labelInValue>
        {goodsTradeSpuCommonAttr.map(v => (
          <Select.Option value={v.attrCode} key={v.id}>
            {v.attrName}
          </Select.Option>
        ))}
      </Select>
    );
  };

  /**
   * 普通属性删除
   */
  handleDelete = key => {
    console.log('删除', key);
    const { dispatch } = this.props;
    dispatch({
      type: 'goodstradespuinfomanage/deleteGoodsTradeSkuCommonAttr',
      payload: [key],
      callback: () => {
        message.success(formatMessage({ id: 'form.common.delete.tips' }));
        this.onDelete(key);
      },
    });
  };

  handleSave = (index, row, list, type) => {
    console.log('保存', index, row, list, type);
    const { dispatch } = this.props;
    const { attr, attrValue, ...other } = row;
    const attrCode = attr && attr.key;
    const attrName = attr && attr.label;
    const attrDesc = attr && attr.label;
    const params = {
      attrCode,
      attrName,
      attrDesc,
      attrValue,
    };
    if (type === 'add') {
      // 新增
      dispatch({
        type: 'goodstradespuinfomanage/addGoodsTradeSkuCommonAttr',
        payload: { ...params },
        callback: res => {
          if (res !== undefined && res !== null && res.code === 0) {
            message.success(formatMessage({ id: 'form.common.save.tips' }));
            const { data } = res;
            list.splice(index, 1, { ...data, attr: { key: data.attrCode, label: data.attrName } });
            this.onSave(index, row, list);
          }
        },
      });
    }
    if (type === 'edit') {
      // 编辑
      dispatch({
        type: 'goodstradespuinfomanage/modifyGoodsTradeSkuCommonAttr',
        payload: {
          ...other,
          ...params,
        },
        callback: res => {
          if (res !== undefined && res !== null && res.code === 0) {
            message.success(formatMessage({ id: 'form.common.update.tips' }));
            const { data } = res;
            list.splice(index, 1, { ...data, attr: { key: data.attrCode, label: data.attrName } });
            this.onSave(index, row, list);
          }
        },
      });
    }
  };

  render() {
    const { operateType } = this.props;
    const { editingKey, tableColumns, rowKey, nativeData } = this.state;
    const otherButton = {
      value: 'remove',
      label: formatMessage({ id: 'button.common.delete' }),
      onClick: this.handleDelete,
    };
    return (
      <div style={{ maxWidth: 700 }}>
        {operateType === 'edit' ? (
          <span>
            <EditableTableBlock
              rowKey={rowKey}
              columns={tableColumns}
              dataList={nativeData}
              otherButton={otherButton}
              onSave={this.handleSave}
              onCanCel={this.onCanCel}
              editingKey={editingKey}
              editingKeyChange={this.editingKeyChange}
            />
            <Button
              icon="plus"
              className={styles.rowAdd}
              type="primary"
              block
              onClick={() => this.newAddRow({ ...initRowData }, nativeData)}
            >
              {formatMessage({ id: 'button.common.add' })}
            </Button>
          </span>
        ) : (
          <Table
            rowKey="id"
            className="small-margin-common-table"
            pagination={false}
            columns={tableColumns}
            dataSource={nativeData}
          />
        )}
      </div>
    );
  }
}

export default connect(({ goodstradespuinfomanage }) => ({
  goodsTradeSkuCommonAttr: goodstradespuinfomanage.goodsTradeSkuCommonAttr || [], // 商品普通属性
  goodsTradeSpuCommonAttr: goodstradespuinfomanage.goodsTradeSpuCommonAttr || [], // 商品sku属性下拉列表数据(可用)
}))(CommonAttrCard);
