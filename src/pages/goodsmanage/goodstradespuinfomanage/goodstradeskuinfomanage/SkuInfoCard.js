import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Table } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';

moment.locale('zh-cn');

/**
 * 交易产品信息管理：商品信息的sku信息
 */
function SkuInfoCard(props, ref) {
  const tableColumns = [
    {
      title: formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttr' }),
      dataIndex: 'skuAttrName',
    },
    {
      title: formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttrItemValue' }),
      dataIndex: 'skuAttrItemValue',
    },
    {
      title: formatMessage({ id: 'form.goodsSpuAttrSkuCmp.skuAttrCode' }),
      dataIndex: 'skuAttrItemCode',
    },
  ];
  const { form, goodsTradeSkuAttrInfo } = props;

  useImperativeHandle(ref, () => ({
    form,
  }));

  const [list, setList] = useState([...goodsTradeSkuAttrInfo]);

  useEffect(() => {
    setList([...goodsTradeSkuAttrInfo]);
  }, [goodsTradeSkuAttrInfo]);

  return (
    <div style={{ maxWidth: 700 }}>
      <Table
        rowKey="id"
        className="small-margin-common-table"
        pagination={false}
        columns={tableColumns}
        dataSource={list}
      />
    </div>
  );
}

export default React.forwardRef(SkuInfoCard);
