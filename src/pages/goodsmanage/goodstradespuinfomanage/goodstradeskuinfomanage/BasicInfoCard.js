import React, { useState, useEffect, useImperativeHandle } from 'react';
import { Form, Input, Descriptions, message } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import { getTreeParent } from '@/utils/utils';
import styles from './index.less';

moment.locale('zh-cn');

/**
 * 交易产品管理：产品-详情-商品-维护
 */
function BasicInfoCard(props, ref) {
  const {
    form,
    dispatch,
    operateType, // 操作类型
    goodsEbsCatagoryListAll,
    parmPublicParameterGoodsEbsSubAttr,
    goodsTradeSkuBasicInfo,
    goodsTradeSkuBasicInfoValid,
    // operationType, // 操作类型
    // goodsApplyBillDetailInfo,
    // disabled, // 是否可修改
  } = props;

  useImperativeHandle(ref, () => ({
    form,
  }));

  const [type, setType] = useState('');
  const [descError, setDescError] = useState(false);
  const [ebsCatagoryListAll, setEbsCatagoryListAll] = useState(goodsEbsCatagoryListAll);
  const [subcategory, setSubcategory] = useState(parmPublicParameterGoodsEbsSubAttr);

  // 监听并更新ebs子分类列表
  useEffect(() => {
    setEbsCatagoryListAll([...goodsEbsCatagoryListAll]);
  }, [goodsEbsCatagoryListAll]);
  // 监听并更新ebs子分类列表
  useEffect(() => {
    setSubcategory([...parmPublicParameterGoodsEbsSubAttr]);
  }, [parmPublicParameterGoodsEbsSubAttr]);
  // 监听商品基本信息变化
  useEffect(() => {
    setDescError(false);
  }, [goodsTradeSkuBasicInfo]);
  // 监听操作类型变化
  useEffect(() => {
    setType(operateType);
  }, [operateType]);

  const { getFieldDecorator } = form;
  const subcategoryLabel = goodsSkuEbs => {
    let label = '';
    if (goodsSkuEbs && goodsSkuEbs.ebsSubCatagoryCode) {
      const findVal = subcategory.find(v => v.parmCode === goodsSkuEbs.ebsSubCatagoryCode);
      label = findVal ? findVal.parmValue : '';
    }
    return label;
  };
  const ebscategoryLabel = goodsSkuEbs => {
    let ebsCatagoryLabel = '';
    if (goodsSkuEbs && goodsSkuEbs.ebsCatagoryCode) {
      const ebsCatagoryCodeArr =
        getTreeParent(
          ebsCatagoryListAll,
          goodsSkuEbs.ebsCatagoryCode,
          'ebsCatagoryCode',
          'ebsParentCode'
        ) || [];
      ebsCatagoryLabel = ebsCatagoryCodeArr.map(v => v.ebsCatagoryName).join('/');
    }
    return ebsCatagoryLabel;
  };
  const handleReset = () => {
    dispatch({
      type: 'goodstradespuinfomanage/changeGoodsTradeSkuBasicInfo',
      payload: {
        ...goodsTradeSkuBasicInfoValid,
      },
    });
  };

  const handleBlur = () => {
    dispatch({
      type: 'goodstradespuinfomanage/modifyGoodsTradeSkuBasicInfo',
      callback: res => {
        if (res !== undefined && res !== null && res.code === 0) {
          message.success('保存成功');
          setDescError(false);
        } else {
          setDescError(true);
        }
      },
    });
  };

  const formRender = () => (
    <Form layout="horizontal" labelAlign="right">
      <Descriptions column={3}>
        <Descriptions.Item
          span={3}
          label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuName' })}
        >
          {(goodsTradeSkuBasicInfo && goodsTradeSkuBasicInfo.skuName) || ''}
        </Descriptions.Item>
        <Descriptions.Item
          span={3}
          label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuCode' })}
        >
          {(goodsTradeSkuBasicInfo && goodsTradeSkuBasicInfo.skuCode) || ''}
        </Descriptions.Item>
        <Descriptions.Item
          span={3}
          label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuShortName' })}
        >
          {(goodsTradeSkuBasicInfo && goodsTradeSkuBasicInfo.skuShortName) || ''}
        </Descriptions.Item>
        <Descriptions.Item
          span={3}
          label={formatMessage({ id: 'form.goodsApplyBillManage.title.ebscategory' })}
        >
          {ebscategoryLabel(goodsTradeSkuBasicInfo && goodsTradeSkuBasicInfo.goodsSkuEbs)}
        </Descriptions.Item>
        <Descriptions.Item
          span={3}
          label={formatMessage({ id: 'form.goodsApplyBillManage.title.subcategory' })}
        >
          {subcategoryLabel(goodsTradeSkuBasicInfo && goodsTradeSkuBasicInfo.goodsSkuEbs)}
        </Descriptions.Item>
        <Descriptions.Item
          span={3}
          label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuDesc' })}
          className={styles.synopsis}
        >
          {type === 'see' && (goodsTradeSkuBasicInfo && goodsTradeSkuBasicInfo.skuDesc)}
          {type === 'edit' && (
            <Form.Item>
              {getFieldDecorator('skuDesc')(
                <Input.TextArea onBlur={handleBlur} style={{ width: '100%', maxWidth: 600 }} />
              )}
              {descError ? (
                <span className={styles.errorPrompt}>
                  保存失败：是否继续保存，恢复即将恢复修改前数据？
                  <a onClick={handleBlur}>保存</a>
                  <a onClick={handleReset}>恢复</a>
                </span>
              ) : (
                <span className={styles.blurPrompt}>鼠标光标失焦将自动保存简介内容</span>
              )}
            </Form.Item>
          )}
        </Descriptions.Item>
      </Descriptions>
    </Form>
  );

  return <div className={styles.goodsapplybilltabBasicInfo}>{formRender()}</div>;
}

export default Form.create({
  onValuesChange({ dispatch, goodsTradeSkuBasicInfo }, changedValues) {
    dispatch({
      type: 'goodstradespuinfomanage/changeGoodsTradeSkuBasicInfo',
      payload: {
        ...goodsTradeSkuBasicInfo,
        ...changedValues,
      },
    });
  },
  mapPropsToFields(props) {
    const { goodsTradeSkuBasicInfo } = props;
    return {
      skuDesc: Form.createFormField({
        value: goodsTradeSkuBasicInfo ? goodsTradeSkuBasicInfo.skuDesc : '',
      }),
    };
  },
})(React.forwardRef(BasicInfoCard));
