import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Card } from 'antd';
import SpuTab from './goodstradespuinfotab/spuTab';
import GroupTab from './goodstradespuinfotab/groupTab';
import SkuTab from './goodstradespuinfotab/skuTab';
import MaterielTab from './goodstradespuinfotab/materielTab';

moment.locale('zh-cn');
/**
 * 产品信息界面
 */
class GoodsSpuInfoDetail extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activeKey: 'spuTab',
    };
    this.contentList = spuType => ({
      spuTab: (
        <SpuTab
          onRef={val => {
            this.spuTabRef = val;
          }}
          spuType={spuType}
        />
      ),
      groupTab: (
        <GroupTab
          spuType={spuType}
          onRef={val => {
            this.groupTabRef = val;
          }}
        />
      ),
      skuTab: (
        <SkuTab
          spuType={spuType}
          onRef={val => {
            this.skuTabRef = val;
          }}
        />
      ),
      materielTab: <MaterielTab spuType={spuType} />,
    });
    this.tabList = [
      {
        key: 'spuTab',
        tab: '产品',
      },
      {
        key: 'groupTab',
        tab: '商品分组',
      },
      {
        key: 'skuTab',
        tab: '商品',
      },
      {
        key: 'materielTab',
        tab: '物料',
      },
    ];
  }

  /**
   * 默认加载数据
   */
  componentDidMount() {
    const {
      dispatch,
      location: {
        query: { spuCode = '' },
      },
    } = this.props;
    // 产品类目下拉列表的查询
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsCatagoryListAll' });
    // 产品大类下拉列表的查询
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsClassificationListAll' });
    // 产品品牌下拉列表的查询
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsbrandListAll' });
    // ebs分类下拉列表的查询
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsEbsCatagoryListAll' });
    // 属性名称（所有）
    dispatch({ type: 'goodsapplybillmanage/fetchGoodsAttrNameAll' });
    // 获取-根据公用参数类型定义代码[goods_spu_type]获取公用参数定义数据所有(产品类型)
    dispatch({
      type: 'parmpublicparametermanage/getProductType',
      payload: { parmTypeCode: 'goods_spu_type' },
    });
    // 获取-根据公用参数类型定义代码[goods_desc_type]获取公用参数定义数据所有(产品描述类型)
    dispatch({
      type: 'parmpublicparametermanage/getProductDesc',
      payload: { parmTypeCode: 'goods_desc_type' },
    });
    // 获取-根据公用参数类型定义代码[goods_unit_of_quantity]获取公用参数定义数据所有(商品数量单位类型数据)
    dispatch({
      type: 'parmpublicparametermanage/getSpuQuantityUnit',
      payload: { parmTypeCode: 'goods_unit_of_quantity' },
    });
    // 获取-根据公用参数类型定义代码[good_ebs_sub_attribute]获取公用参数定义数据所有(子分类数据)
    dispatch({
      type: 'parmpublicparametermanage/getGoodsEbsSubAttr',
      payload: { parmTypeCode: 'good_ebs_sub_attribute' },
    });
    if (spuCode) {
      dispatch({
        type: 'goodstradespuinfomanage/fetchGoodsTradeSpuInfo',
        payload: {
          spuCode,
        },
      });
    }
  }

  /**
   * tab切换回调
   */
  tabChange = key => {
    this.setState({
      activeKey: key,
    });
  };

  render() {
    const { activeKey } = this.state;
    const {
      location: {
        query: { type = 'see' },
      },
      goodsTradeSpuBasicInfoValid,
    } = this.props;
    const title = (
      <span>
        <div>{type === 'see' ? '产品详情' : '产品维护'}</div>
        <div style={{ fontSize: 14, color: '#6F6A6A', marginTop: 8 }}>
          <span style={{ marginRight: 30 }}>
            产品名称：{goodsTradeSpuBasicInfoValid.spuName || ''}
          </span>
          <span>产品代码：{goodsTradeSpuBasicInfoValid.spuCode || ''}</span>
        </div>
      </span>
    );
    return (
      <Card
        style={{ width: '100%' }}
        title={title}
        // extra={extra}
        tabList={this.tabList}
        activeTabKey={activeKey}
        onTabChange={this.tabChange}
      >
        {this.tabList.map(item => (
          <div key={item.key} style={{ display: activeKey === item.key ? 'block' : 'none' }}>
            {this.contentList(type)[item.key]}
          </div>
        ))}
      </Card>
    );
  }
}

export default connect(({ goodstradespuinfomanage }) => ({
  goodsTradeSpuBasicInfoValid: goodstradespuinfomanage.goodsTradeSpuBasicInfoValid || {}, // 产品基本信息（正式生效的数据）
}))(GoodsSpuInfoDetail);
