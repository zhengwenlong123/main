import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Form, Input, Card } from 'antd';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import styles from './index.less';

const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];

/**
 * 物料序号信息
 */
class GoodsLastNoManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      // {
      //   title: formatMessage({ id: 'form.common.createAt' }),
      //   dataIndex: 'createAt',
      //   key: 'createAt',
      //   align: 'center',
      // },
      {
        title: formatMessage({ id: 'form.common.rowno' }),
        dataIndex: 'lastNo',
        key: 'lastNo',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandCode' }),
        dataIndex: 'brandCode',
        key: 'brandCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuInfoManage.mainClassCode' }),
        dataIndex: 'mainCatgCode',
        key: 'mainCatgCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        dataIndex: 'enable',
        key: 'enable',
        align: 'center',
        render: (text, record) => <span>{status[record.enable]}</span>,
      },
    ];
    super(props, tableColumns);
    this.searchContent = {};
  }

  /**
   * 默认加载物料序号信息数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-物料序号信息数据
    dispatch({
      type: 'goodslastnomanage/paginationGoodsLastNo',
      payload: { page: 1, pageSize: 10, searchCondition: this.searchContent },
    });
  }

  /**
   * 根据条件搜索序号
   */
  onFuzzyGoodsLastNoHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'goodslastnomanage/paginationGoodsLastNo',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
    // 清空搜素框
    // setFieldsValue({
    // });
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      formItemLayout,
      tailFormItemLayout,
      tableColumns,
      tableCurrentPage,
    } = this.state;
    const { goodsLastNoList, goodsLastNoListPageSize } = this.props;
    const tablePaginationOnChangeEventDispatchType = 'goodslastnomanage/paginationGoodsLastNo';

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.common.rowno' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('lastNo')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandCode' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('brandCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsSpuInfoManage.mainClassCode' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('mainCatgCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyGoodsLastNoHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
        </div>
      );
    };

    return (
      <Card bordered={false}>
        <div>
          <MdcManageBlock
            tableTitle={formatMessage({ id: 'menu.goodsmanage.goodslastnomanage' })}
            tableColumns={tableColumns}
            tableDataSource={goodsLastNoList === undefined ? [] : goodsLastNoList}
            tableOperateAreaCmp={tableOperateAreaCmp()}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            tableTotalSize={goodsLastNoListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
            modalTitle={modalTitle}
            modalVisible={modalVisible}
            modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ goodslastnomanage }) => ({
  goodsLastNoList: goodslastnomanage.goodsLastNoList || [],
  goodsLastNoListPageSize: goodslastnomanage.goodsLastNoListPageSize || 0,
}))(Form.create()(GoodsLastNoManage));
