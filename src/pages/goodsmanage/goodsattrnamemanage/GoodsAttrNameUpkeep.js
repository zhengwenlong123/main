/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Button, Input, Checkbox, message } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
// import { verifyUnique } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 属性名称维护组件
 */
@ValidationFormHoc
class GoodsAttrNameUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      modalSearchCondition,
      validationForm,
      dispatch,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.attrEnable ? (values.attrEnable = 1) : (values.attrEnable = 0);

    if (mut === 2) {
      // 维护
      // if (values.attrCode !== mue.attrCode) {
      //   // 检验attrCode唯一性
      //   const params = { attrCode: values.attrCode };
      //   const vu = await verifyUnique('/mdc/goods/goodsattrname/verifyUnique', params);
      //   if (!vu) {
      //     message.warn(formatMessage({ id: 'form.goodsAttrNameUpkeep.attrCodeMust' }));
      //     return;
      //   }
      // }
      values.id = mue.id;
      dispatch({
        type: 'goodsattrnamemanage/updateGoodsAttrName',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    if (mut === 10) {
      // 新增
      // 检验attrCode唯一性
      // const params = { attrCode: values.attrCode };
      // const vu = await verifyUnique('/mdc/goods/goodsattrname/verifyUnique', params);
      // if (!vu) {
      //   message.warn(formatMessage({ id: 'form.goodsAttrNameUpkeep.attrCodeMust' }));
      //   return;
      // }
      dispatch({
        type: 'goodsattrnamemanage/addGoodsAttrName',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            {/* <Form.Item label={formatMessage({ id: 'form.common.code' })}>
              {getFieldDecorator('attrCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.code.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={50} />)}
            </Form.Item> */}
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('attrName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.description' })}>
              {getFieldDecorator('attrDesc')(
                <Input.TextArea disabled={mut === 1} rows={3} maxLength={250} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('attrEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" htmlType="button" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button
                type="primary"
                htmlType="button"
                onClick={this.onClickSubmit}
                loading={confirmLoading}
              >
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        // attrCode: Form.createFormField({ value: mue ? mue.attrCode : '' }),
        attrName: Form.createFormField({ value: mue ? mue.attrName : '' }),
        attrDesc: Form.createFormField({ value: mue ? mue.attrDesc : '' }),
        attrEnable: Form.createFormField({ value: mue ? mue.attrEnable === 1 : true }),
      };
    },
  })(GoodsAttrNameUpkeep)
);
