/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Select, Input, Card } from 'antd';
import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import GoodsAttrNameUpkeep from './GoodsAttrNameUpkeep';
import { serverUrl } from '@/defaultSettings';
import styles from './index.less';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 属性名称管理组件
 */
class GoodsAttrNameManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      // {
      //   title: formatMessage({ id: 'form.common.code' }),
      //   dataIndex: 'attrCode',
      //   key: 'attrCode',
      //   align: 'center',
      // },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'attrName',
        key: 'attrName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.description' }),
        dataIndex: 'attrDesc',
        key: 'attrDesc',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'attrEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.attrEnable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.goodsAttrNameManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.goodsAttrNameManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'goodsattrnamemanage/updateGoodsAttrName',
                  this.searchContent
                )
              }
            >
              {record.attrEnable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {
      attrCode: '',
      attrName: '',
      attrEnable: '',
    };
  }

  /**
   * 钩子函数
   */
  hookProcess = (type, record) => {
    if (type === 3) {
      record.attrEnable = record.attrEnable === 1 ? 0 : 1;
    } else {
      // 无处理
    }
    return record;
  };

  /**
   * 加载属性名称数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 分页-加载属性名称数据
    dispatch({
      type: 'goodsattrnamemanage/paginationGoodsAttrName',
      payload: { page: 1, pageSize: 10 },
    });
  }

  /**
   * 根据条件搜索属性
   */
  onFuzzyGoodsAttrNameHandle = () => {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'goodsattrnamemanage/paginationGoodsAttrName',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
    // 清空搜素框
    // setFieldsValue({
    // });
  };

  /**
   * 导出文件请求
   */
  onExportGoodsAttrNameFile = () => {
    const values = this.searchContent;
    const params = {
      ...values,
    };
    const formElement = document.createElement('form');
    formElement.style.display = 'display:none;';
    formElement.method = 'post';
    formElement.action = `${serverUrl}/mdc/goods/goodsattrname/export`;
    formElement.target = 'callBackTarget';
    Object.keys(params).map(item => {
      const inputElement = document.createElement('input');
      inputElement.type = 'hidden';
      inputElement.name = item;
      inputElement.value = params[item] || '';
      formElement.appendChild(inputElement);
      return item;
    });
    document.body.appendChild(formElement);
    formElement.submit();
    document.body.removeChild(formElement);
  };

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
      formItemLayout,
      tailFormItemLayout,
    } = this.state;
    const { goodsAttrNameList, goodsAttrNameListPageSize } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            {/* <Form.Item
              label={formatMessage({ id: 'form.common.code' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('attrCode')(<Input style={{ width: 120 }} />)}
            </Form.Item> */}
            <Form.Item
              label={formatMessage({ id: 'form.common.name' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('attrName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.enable' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('attrEnable')(
                <Select style={{ width: 80 }} allowClear>
                  <Select.Option value="1">
                    {formatMessage({ id: 'form.common.enabled' })}
                  </Select.Option>
                  <Select.Option value="0">
                    {formatMessage({ id: 'form.common.disabled' })}
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyGoodsAttrNameHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            icon="download"
            style={{ marginTop: 4, marginRight: 10 }}
            onClick={() => this.onExportGoodsAttrNameFile()}
          >
            {formatMessage({ id: 'button.common.export' })}
          </Button>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.goodsAttrNameManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };
    const tablePaginationOnChangeEventDispatchType = 'goodsattrnamemanage/paginationGoodsAttrName';
    return (
      <Card bordered={false}>
        <div>
          <MdcManageBlock
            tableTitle={formatMessage({ id: 'menu.goodsmanage.goodsattrnamemanage' })}
            tableColumns={tableColumns}
            tableDataSource={goodsAttrNameList}
            tableOperateAreaCmp={tableOperateAreaCmp()}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            tableTotalSize={goodsAttrNameListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
            modalTitle={modalTitle}
            modalVisible={modalVisible}
            modalContentCmp={
              <GoodsAttrNameUpkeep
                modalUpkeepType={modalUpkeepType}
                modalUpkeepEntity={modalUpkeepEntity}
                modalVisibleOnChange={this.modalVisibleOnChange}
                modalOnChangePageCurrent={this.changeCurrentPage}
                modalSearchCondition={this.searchContent}
              />
            }
            modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ goodsattrnamemanage }) => ({
  goodsAttrNameList: goodsattrnamemanage.goodsAttrNameList || [],
  goodsAttrNameListPageSize: goodsattrnamemanage.goodsAttrNameListPageSize || 0,
}))(Form.create()(GoodsAttrNameManage));
