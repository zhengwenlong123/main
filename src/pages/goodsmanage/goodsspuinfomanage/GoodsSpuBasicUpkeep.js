/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import moment from 'moment';
import {
  message,
  Button,
  Form,
  Select,
  Input,
  InputNumber,
  DatePicker,
  Checkbox,
  TreeSelect,
} from 'antd';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品基本信息维护组件
 */
@ValidationFormHoc
class GoodsSpuBasicUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      dispatch,
      validationForm,
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      modalSearchCondition,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.spuEnable ? (values.spuEnable = 1) : (values.spuEnable = 0);

    if (mut === 2) {
      // 维护
      values.id = mue.id;
      dispatch({
        type: 'goodsspuinfomanage/updateGoodsSpuInfo',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    if (mut === 10) {
      // 新增
      dispatch({
        type: 'goodsspuinfomanage/addGoodsSpuInfo',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  /**
   * 组装产品类目1级、2级、3级数据组件(树形结构/所有)
   */
  handleGoodsCatagoryWith3LevelCmp = data => {
    const { childrens } = data;
    const tempObj = {};
    tempObj.title = data.catagoryName;
    tempObj.value = data.catagoryCode;
    tempObj.key = data.catagoryCode;
    if (data.catagoryLevel === 1 || data.catagoryLevel === 2) tempObj.selectable = false;
    if (data.catagoryLevel === 3) tempObj.isLeaf = true;
    if (childrens !== undefined && childrens !== null && childrens.length > 0) {
      tempObj.children = childrens.map(item => this.handleGoodsCatagoryWith3LevelCmp(item));
    }
    return tempObj;
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
      spuBeAdoptedStatus: sbas,
      modalUpkeepEntity: mue, // 当前的渲染用的值
      goodsBrandAll: gba, // 产品品牌数据(所有)
      goodsClassificationAll: gca, // 产品大类数据(所有)
      parmPublicParameterOfGoodsSpuTypeAll: ppposta, // 产品类型数据(所有)
      goodsCatagoryWith3LevelAll: gcw3la, // 产品类目1级、2级、3级数据(树形结构/所有)
    } = this.props;

    // 产品类目树形结构数据
    const goodsCatagoryTreeData = gcw3la
      .filter(item => item.childrens.length > 0)
      .map(item => this.handleGoodsCatagoryWith3LevelCmp(item));

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.mainClassCode' })}>
              {getFieldDecorator('mainClassCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsSpuBasicUpkeep.mainClassCode.placeholder',
                    }),
                  },
                ],
              })(
                <Select disabled={mut === 1 || (mut === 2 && sbas === true)}>
                  {mue && mue.mainClassName && mue.mainClassCode ? (
                    <Select.Option key={mue.mainClassCode} value={mue.mainClassCode}>
                      {`${mue.mainClassCode}+${mue.mainClassName}`}
                    </Select.Option>
                  ) : (
                    ''
                  )}
                  {gca.map(item =>
                    !mue || item.mainClassCode !== mue.mainClassCode ? (
                      <Select.Option key={item.mainClassCode} value={item.mainClassCode}>
                        {`${item.mainClassCode}+${item.mainClassName}`}
                      </Select.Option>
                    ) : (
                      ''
                    )
                  )}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.brandCode' })}>
              {getFieldDecorator('brandCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsSpuBasicUpkeep.brandCode.placeholder',
                    }),
                  },
                ],
              })(
                <Select disabled={mut === 1 || (mut === 2 && sbas === true)}>
                  {gba.map(item => (
                    <Select.Option key={item.brandCode} value={item.brandCode}>
                      {`${item.brandCode}+${item.brandName}`}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            {/* <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuCode' })}>
              {getFieldDecorator('spuCode', {
                rules: [{ required: true, message: formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuCode.placeholder' }) }],
              })(<Input disabled={mut === 1 || (mut === 2 && sbas === true)} />)}
            </Form.Item> */}
            <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuName' })}>
              {getFieldDecorator('spuName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuName.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1 || (mut === 2 && sbas === true)} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuShortName' })}>
              {getFieldDecorator('spuShortName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsSpuBasicUpkeep.spuShortName.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1 || (mut === 2 && sbas === true)} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuTypeCode' })}>
              {getFieldDecorator('spuTypeCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuTypeCode.choice' }),
                  },
                ],
              })(
                <Select disabled={mut === 1 || (mut === 2 && sbas === true)}>
                  {ppposta.map(item => (
                    <Select.Option key={item.parmCode} value={item.parmCode}>
                      {item.parmValue}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuInfoManage.catagoryCode' })}>
              {getFieldDecorator('catagoryCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.goodsSpuInfoManage.catagoryCode.choice' }),
                  },
                ],
              })(
                <TreeSelect
                  disabled={mut === 1}
                  showCheckedStrategy="SHOW_ALL"
                  treeData={goodsCatagoryTreeData}
                />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.rowno' })}>
              {getFieldDecorator('seq')(
                <InputNumber min={1} max={10000} defaultChecked={1} disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuDesc' })}>
              {getFieldDecorator('spuDesc')(
                <Input.TextArea rows={5} disabled={mut === 1} maxLength={1000} />
              )}
              <span>*{formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuDesc-conformity' })}</span>
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuTimeToMarket' })}>
              {getFieldDecorator('spuTimeToMarket', { valuePropName: 'defaultValue' })(
                <DatePicker disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('spuEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
            {/* <Form.Item label="产品状态">
              {getFieldDecorator('spuEnable')(
                <Radio.Group disabled={mut === 1}>
                  <Radio value={1}>待发布</Radio>
                  <Radio value={2}>预售</Radio>
                  <Radio value={3}>上市</Radio>
                  <Radio value={4}>停用</Radio>
                </Radio.Group>
              )}
              </Form.Item> */}
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(
  ({
    goodsbrandmanage,
    goodscatagorymanage,
    goodsspuinfomanage,
    goodsclassificationmanage,
    parmpublicparametermanage,
  }) => ({
    spuBeAdoptedStatus: goodsspuinfomanage.spuBeAdoptedStatus || false, // 当前产品-是否已被商品引用状态
    goodsBrandAll: goodsbrandmanage.goodsBrandAll || [], // 产品品牌数据(所有)
    goodsClassificationAll: goodsclassificationmanage.goodsClassificationAll || [], // 产品大类数据(所有)
    goodsCatagoryWith3LevelAll: goodscatagorymanage.goodsCatagoryWith3LevelAll || [], // 产品类目1级、2级、3级数据(树形结构/所有)
    parmPublicParameterOfGoodsSpuTypeAll:
      parmpublicparametermanage.parmPublicParameterOfGoodsSpuTypeAll || [], // 产品类型数据(所有)(从公共参数定义接口获取数据)
  })
)(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;

      return {
        mainClassCode: Form.createFormField({ value: mue ? mue.mainClassCode : '' }),
        brandCode: Form.createFormField({ value: mue ? mue.brandCode : '' }),
        spuCode: Form.createFormField({ value: mue ? mue.spuCode : '' }),
        spuName: Form.createFormField({ value: mue ? mue.spuName : '' }),
        spuShortName: Form.createFormField({ value: mue ? mue.spuShortName : '' }),
        spuTypeCode: Form.createFormField({ value: mue ? mue.spuTypeCode : '' }),
        catagoryCode: Form.createFormField({ value: mue ? mue.catagoryCode : '' }),
        seq: Form.createFormField({ value: mue ? mue.seq : 1 }),
        spuDesc: Form.createFormField({ value: mue ? mue.spuDesc : '' }),
        spuTimeToMarket: Form.createFormField({
          value: moment(
            mue && mue.spuTimeToMarket ? mue.spuTimeToMarket : new Date(),
            'YYYY-MM-DD'
          ),
        }),
        spuEnable: Form.createFormField({ value: mue ? mue.spuEnable === 1 : true }),
      };
    },
  })(GoodsSpuBasicUpkeep)
);
