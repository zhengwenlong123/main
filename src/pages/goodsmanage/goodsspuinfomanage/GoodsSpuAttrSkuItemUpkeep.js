import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Form, Input, Checkbox, InputNumber, message } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
// import { execOneRequest } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品SKU属性项值维护组件(Table进入)
 */
@ValidationFormHoc
class GoodsSpuAttrSkuItemUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      // message.wran('操作失败');
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      dispatch,
      validationForm,
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      originGoodsSpuInfo: ogsi, // 原始产品信息数据
      // spuAttrSkuEntity: sase, // 当前被选中产品sku属性列表行数据实体对象
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.spuCode = ogsi.spuCode;
    values.skuAttrItemEnable = values.skuAttrItemEnable ? 1 : 0;

    // 验证产品SKU属性项值数据唯一性
    // if (
    //   mut === 10 ||
    //   mue.skuAttrItemCode !== values.skuAttrItemCode ||
    //   mue.skuAttrItemValue !== values.skuAttrItemValue
    // ) {
    //   const verifyResult = await execOneRequest('/mdc/goods/goodsspuattrskuitem/verifyUnique', {
    //     spuCode: values.spuCode,
    //     skuAttrCode: sase.skuAttrCode,
    //     skuAttrName: sase.skuAttrName,
    //     skuAttrItemCode: values.skuAttrItemCode,
    //     skuAttrItemValue: values.skuAttrItemValue,
    //   });
    //   if (verifyResult) {
    //     if (verifyResult.status) {
    //       // 验证通过
    //     } else {
    //       message.error(`${verifyResult.message}`);
    //       return;
    //     }
    //   } else {
    //     message.warn(formatMessage({ id: 'form.goodsSpuAttrSkuItemUpkeep.skuAttr-conformity' }));
    //     return;
    //   }
    // }

    if (mut === 2) {
      // 维护
      values.id = mue.id;
      dispatch({
        type: 'goodsspuinfomanage/updateGoodsSpuAttrSkuItem',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (mut === 10) {
      // 新增
      dispatch({
        type: 'goodsspuinfomanage/addGoodsSpuAttrSkuItem',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
    } = this.props;

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form {...formItemLayout}>
            <Form.Item
              label={formatMessage({ id: 'form.goodsSpuAttrSkuItemUpkeep.skuAttrCode' })}
              style={{ display: 'none' }}
            >
              {getFieldDecorator('skuAttrCode', { rules: [{ required: true }] })(
                <Input disabled />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuAttrSkuItemUpkeep.skuAttrName' })}>
              {getFieldDecorator('skuAttrName', { rules: [{ required: true }] })(
                <Input disabled />
              )}
            </Form.Item>
            {/* <Form.Item label={formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttrItemCode' })}>
              {getFieldDecorator('skuAttrItemCode', {
                rules: [
                  {
                    required: false,
                    message: formatMessage({
                      id: 'form.goodsSpuAttrSkuItemCmp.skuAttrItemCode.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item> */}
            <Form.Item
              label={formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttrItemValue' })}
            >
              {getFieldDecorator('skuAttrItemValue', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttrItemValue' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttrItemDesc' })}>
              {getFieldDecorator('skuAttrItemDesc')(
                <Input.TextArea rows={5} disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.rowno' })}>
              {getFieldDecorator('seq')(
                <InputNumber min={1} max={10000} defaultChecked={1} disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('skuAttrItemEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const {
        modalUpkeepEntity: mue,
        spuAttrSkuEntity: sase, // 当前被选中产品sku属性列表行数据实体对象
      } = props;
      return {
        skuAttrCode: Form.createFormField({ value: sase.skuAttrCode }),
        skuAttrName: Form.createFormField({ value: sase.skuAttrName }),
        // skuAttrItemCode: Form.createFormField({ value: mue ? mue.skuAttrItemCode : '' }),
        skuAttrItemValue: Form.createFormField({ value: mue ? mue.skuAttrItemValue : '' }),
        skuAttrItemDesc: Form.createFormField({ value: mue ? mue.skuAttrItemDesc : '' }),
        seq: Form.createFormField({ value: mue ? mue.seq : 1 }),
        skuAttrItemEnable: Form.createFormField({
          value: mue ? mue.skuAttrItemEnable === 1 : true,
        }),
      };
    },
  })(GoodsSpuAttrSkuItemUpkeep)
);
