import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Tabs } from 'antd';
import GoodsSpuInfoUpkeepBasic from './GoodsSpuInfoUpkeepBasic';
import GoodsSpuBasicUpkeep from './GoodsSpuBasicUpkeep';

/**
 * 产品信息维护组件
 */
class GoodsSpuInfoUpkeep extends React.PureComponent {
  onTabClick = key => {
    if (Number(key) === 5) {
      const { dispatch, modalUpkeepEntity: mue } = this.props;
      // 获取当前选择产品-[产品SKU属性]数据(列表/所有),供商品分组时选择从属的属性使用(需求：从属的属性来源于当前产品下的产品SKU属性表从不中选取)
      dispatch({
        type: 'goodsspuinfomanage/getSpuCondWithGoodsSpuAttrSkuList',
        payload: { spuCode: mue.spuCode },
      });
    }
  };

  render() {
    const { modalUpkeepType: mut } = this.props;

    return (
      <div>
        {mut === 10 ? (
          <GoodsSpuBasicUpkeep {...this.props} />
        ) : (
          <div>
            <Tabs tabPosition="left" onTabClick={this.onTabClick}>
              <Tabs.TabPane
                key="0"
                tab={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.spuBasic' })}
              >
                <GoodsSpuBasicUpkeep {...this.props} />
              </Tabs.TabPane>
              <Tabs.TabPane
                key="1"
                tab={formatMessage({
                  id: 'menu.goodsmanage.goodsspuinfomanage.spuCommonAttribute',
                })}
              >
                <GoodsSpuInfoUpkeepBasic {...this.props} upkeepBasicFlag={1} />
              </Tabs.TabPane>
              <Tabs.TabPane
                key="2"
                tab={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.spuSku' })}
              >
                <GoodsSpuInfoUpkeepBasic {...this.props} upkeepBasicFlag={2} />
              </Tabs.TabPane>
              <Tabs.TabPane
                key="3"
                tab={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.productType' })}
              >
                <GoodsSpuInfoUpkeepBasic {...this.props} upkeepBasicFlag={3} />
              </Tabs.TabPane>
              <Tabs.TabPane
                key="4"
                tab={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.productDesc' })}
              >
                <GoodsSpuInfoUpkeepBasic {...this.props} upkeepBasicFlag={4} />
              </Tabs.TabPane>
              <Tabs.TabPane
                key="5"
                tab={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.productGroup' })}
              >
                <GoodsSpuInfoUpkeepBasic {...this.props} upkeepBasicFlag={5} />
              </Tabs.TabPane>
            </Tabs>
          </div>
        )}
      </div>
    );
  }
}

export default connect()(GoodsSpuInfoUpkeep);
