/* eslint-disable no-nested-ternary */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Divider, message } from 'antd';
import moment from 'moment';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import GoodsSpuAttrSkuUpkeep from './GoodsSpuAttrSkuUpkeep'; // 产品SKU属性-维护组件
import GoodsSpuAttrSkuItemCmp from './GoodsSpuAttrSkuItemCmp'; // 产品SKU属性项值-维护组件

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 产品SKU属性-维护组件
 */
class GoodsSpuAttrSkuCmp extends MdcManageBasic {
  constructor(props) {
    super(props);
    // state
    this.state = {
      ...this.state,
      // Table行勾选数据对象
      selectedRowKeys: [],
      tableRowSelectionEntity: null,
    };

    // 产品SKU属性-表格列表字段定义
    this.tableColumns_GoodsSpuAttrSk = [
      // {
      //   title: formatMessage({ id: 'form.goodsSpuAttrSkuCmp.skuAttrCode' }),
      //   dataIndex: 'skuAttrCode',
      //   key: 'skuAttrCode',
      //   align: 'center',
      // },
      {
        title: formatMessage({ id: 'form.goodsSpuAttrSkuCmp.skuAttrName' }),
        dataIndex: 'skuAttrName',
        key: 'skuAttrName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'skuAttrNameEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.skuAttrNameEnable]}</span>,
      },
    ];

    // Table-表格行勾选处理
    // this.tableRowSelection = {
    //   type: 'radio',
    //   columnWidth: 30,
    //   columnTitle: '<>',
    //   selectedRowKeys,
    //   onChange: (selectedRowKeys, selectedRows) => {
    //     this.tableRowSelection.selectedRowKeys = selectedRowKeys;
    //     this.setState({ tableRowSelectionEntity: selectedRows.length > 1 ? selectedRows[1] : selectedRows[0] }); // Table勾选回调事件
    //     const { dispatch } = this.props;
    //     // 根据产品sku属性条件获取产品sku属性项值数据
    //     dispatch({
    //       type: 'goodsspuinfomanage/paginationGoodsSpuAttrSkuItem',
    //       payload: {
    //         spuCode: selectedRows[0].spuCode,
    //         skuAttrCode: selectedRows[0].skuAttrCode,
    //         skuAttrName: selectedRows[0].skuAttrName,
    //         page: 1,
    //         pageSize: 10,
    //       },
    //     });
    //   },
    // };
  }

  render() {
    const {
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableRowSelectionEntity: trse, // 当前table选中行数据实体对象
      tableCurrentPage,
      selectedRowKeys,
    } = this.state;
    const {
      goodsSpuAttrSkuList,
      goodsSpuAttrSkuListPageSize,
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
    } = this.props;
    const dispatchType = 'goodsspuinfomanage/updateGoodsSpuAttrSku'; // 停用dispatchType
    const tablePaginationOnChangeEventDispatchType = 'goodsspuinfomanage/paginationGoodsSpuAttrSku';
    const tablePaginationOnChangeEventCondition = { spuCode: mue.spuCode };
    return (
      <div>
        <MdcManageBlock
          tableTitle={`${formatMessage({ id: 'form.goodsSpuAttrSkuCmp.spu' })}：${mue.spuCode}+${
            mue.spuName
          }`}
          tableColumns={this.tableColumns_GoodsSpuAttrSk}
          tableDataSource={goodsSpuAttrSkuList}
          tableTotalSize={goodsSpuAttrSkuListPageSize}
          tableOperateAreaCmp={this._tableOperateAreaCmp(2, mut, dispatchType)}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              tablePaginationOnChangeEventCondition
            )
          }
          tableRowSelection={{
            columnWidth: 30,
            columnTitle: '<>',
            selectedRowKeys,
            onSelect: (record, selected, selectedRows) => {
              if (selected) {
                this.setState({ tableRowSelectionEntity: record });
                const { dispatch } = this.props;
                // 根据产品sku属性条件获取产品sku属性项值数据
                dispatch({
                  type: 'goodsspuinfomanage/paginationGoodsSpuAttrSkuItem',
                  payload: {
                    spuCode: record.spuCode,
                    skuAttrCode: record.skuAttrCode,
                    skuAttrName: record.skuAttrName,
                    page: 1,
                    pageSize: 10,
                  },
                });
              } else if (selectedRows.length === 0) {
                this.setState({ tableRowSelectionEntity: null });
              }
            },
            onChange: selectedRowKeysArr => {
              this.setState({
                selectedRowKeys:
                  selectedRowKeysArr.length > 1 ? [selectedRowKeysArr[1]] : selectedRowKeysArr,
              });
            }, // Table勾选回调事件
          }}
          modalVisible={modalVisible}
          modalTitle={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.spuSku' })}
          modalContentCmp={
            <GoodsSpuAttrSkuUpkeep
              originGoodsSpuInfo={mue}
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
        <GoodsSpuAttrSkuItemCmp {...this.props} spuAttrSkuEntity={trse} />
      </div>
    );
  }

  /**
   * Table-表格头部动作处理组件
   * @param {number} tableTypeFlag 当前table的标识码,
   * 1为产品普通属性-表格标识;
   * 2为产品SKU属性-表格标识;
   * 3为产品SKU属性项值-表格标识;
   * 4为产品型号定义-表格标识;
   * 5为商品分组-表格标识
   * @param {number} actionType 当前产品-动作类型, 1为详情;2为维护;3为停用/启用;10为新增;20为删除
   * @param {string} dispatchType 停用/启用-动作对应dispatch的type值
   */
  _tableOperateAreaCmp = (tableTypeFlag, actionType, dispatchType = null) => {
    const { tableRowSelectionEntity: trse } = this.state;

    // 获取当前table状态按钮文字
    const getCurrentStatus = () => {
      if (tableTypeFlag === 1 && trse && !Number.isNaN(trse.attrEnable)) {
        // 产品普通属性-状态
        return trse.attrEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 2 && trse && !Number.isNaN(trse.skuAttrNameEnable)) {
        // 产品SKU属性-状态
        return trse.skuAttrNameEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 3 && trse && !Number.isNaN(trse.skuAttrItemEnable)) {
        // 产品SKU属性项值-状态
        return trse.skuAttrItemEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 4 && trse && !Number.isNaN(trse.modelEnable)) {
        // 产品型号定义-状态
        return trse.modelEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 5 && trse && !Number.isNaN(trse.groupEnable)) {
        // 商品分组-状态
        return trse.groupEnable === 1 ? status[0] : status[1];
      }
      return formatMessage({ id: 'form.common.enabledAndDisabled' });
    };

    // 校验Table勾选tableRowSelectionEntity值是否存在
    const checkTableRowSelectionEntity = (flag = -1) => {
      if (trse === null || trse === undefined) {
        const tips =
          flag === 2
            ? formatMessage({ id: 'button.common.modify' })
            : flag === 20
            ? formatMessage({ id: 'button.common.delete' })
            : formatMessage({ id: 'button.common.handle' });
        message.warn(
          `${formatMessage({ id: 'form.goodsSpuAttrSkuCmp.pleaseSelectData' })}[${tips}]`
        );
        return;
      }
      if (flag === 1) {
        // 详情动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 2) {
        // 维护动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 20) {
        // 删除动作
        this._deleteTableRowSelection(tableTypeFlag, trse);
      }
      if (flag === 3) {
        // 停用动作
        // 设置值为0,表示停用
        Object.keys(trse)
          .filter(item =>
            ['attrEnable', 'skuAttrItemEnable', 'skuAttrNameEnable', 'modelEnable'].includes(item)
          )
          .forEach(item => {
            trse[item] = trse[item] === 0 ? 1 : 0;
          });
        this.onClickActionExecuteEvent(flag, trse, null, dispatchType);
      }
    };

    return actionType === 1 ? (
      <a type="primary" onClick={() => checkTableRowSelectionEntity(1)}>
        {formatMessage({ id: 'button.common.details' })}
      </a>
    ) : (
      <div>
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(1);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.details' })}
        </a>
        <Divider type="vertical" />
        <a type="primary" onClick={() => this.onClickActionExecuteEvent(10)}>
          {formatMessage({ id: 'button.common.add' })}
        </a>
        {trse && !trse.itemEnable ? (
          <span>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() => {
                checkTableRowSelectionEntity(2);
                this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
              }}
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a type="primary" onClick={() => checkTableRowSelectionEntity(3)}>
              {getCurrentStatus()}
            </a>
          </span>
        ) : (
          ''
        )}
        {/* <a type="primary" onClick={() => checkTableRowSelectionEntity(20)}>删除</a> */}
      </div>
    );
  };

  /**
   * Table-删除行勾选数据
   * @param {number} tableTypeFlag 当前table的标识码,
   * 1为产品普通属性-表格标识;
   * 2为产品SKU属性-表格标识;
   * 3为产品SKU属性项值-表格标识;
   * 4为产品型号定义-表格标识;
   * 5为商品分组-表格标识
   * @param {object} item 当前table选中的行数据对象
   */
  _deleteTableRowSelection = (tableTypeFlag, item) => {
    const { dispatch } = this.props;
    switch (tableTypeFlag) {
      case 1: // 删除-产品普通属性
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuAttrNormal', payload: { id: item.id } });
        break;
      case 2: // 删除-产品SKU属性
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuAttrSku', payload: { id: item.id } });
        break;
      case 3: // 删除-产品SKU属性项值
        dispatch({
          type: 'goodsspuinfomanage/deleteGoodsSpuAttrSkuItem',
          payload: { id: item.id },
        });
        break;
      case 4: // 删除-产品型号定义
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuModel', payload: { id: item.id } });
        break;
      case 6: // 删除-商品分组
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSkuGroup', payload: { id: item.id } });
        break;
      default:
        break;
    }
  };
}

export default connect(({ goodsspuinfomanage }) => ({
  spuBeAdoptedStatus: goodsspuinfomanage.spuBeAdoptedStatus || false, // 当前产品-是否已被商品引用状态
  goodsSpuAttrSkuList: goodsspuinfomanage.goodsSpuAttrSkuList || [], // 当前选择-产品SKU属性(列表/分页)
  goodsSpuAttrSkuListPageSize: goodsspuinfomanage.goodsSpuAttrSkuListPageSize || 0, // 当前选择-产品SKU属性数据总数量
}))(GoodsSpuAttrSkuCmp);
