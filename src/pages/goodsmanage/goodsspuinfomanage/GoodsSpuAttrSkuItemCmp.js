/* eslint-disable no-nested-ternary */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Divider, message } from 'antd';
import moment from 'moment';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import GoodsSpuAttrSkuItemUpkeep from './GoodsSpuAttrSkuItemUpkeep'; // 产品SKU属性项值-维护组件

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 产品SKU属性项值-维护组件
 */
class GoodsSpuAttrSkuItemCmp extends MdcManageBasic {
  constructor(props) {
    super(props);
    // state
    this.state = {
      ...this.state,
      // Table行勾选数据对象
      selectedRowKeys: [],
      tableRowSelectionEntity: null,
    };

    // Table-表格行勾选处理
    // this.tableRowSelection = {
    //   type: 'radio',
    //   columnWidth: 30,
    //   columnTitle: '<>',
    //   selectedRowKeys: [],
    //   onChange: (selectedRowKeys, selectedRows) => {
    //     this.tableRowSelection.selectedRowKeys = selectedRowKeys;
    //     this.setState({ tableRowSelectionEntity: selectedRows[0] });
    //   }, // Table勾选回调事件
    // };

    // 产品SKU属性项值-表格列表字段定义
    this.tableColumns_GoodsSpuAttrSkuItem = [
      // {
      //   title: formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttrItemCode' }),
      //   dataIndex: 'skuAttrItemCode',
      //   key: 'skuAttrItemCode',
      //   align: 'center',
      // },
      {
        title: formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttrItemValue' }),
        dataIndex: 'skuAttrItemValue',
        key: 'skuAttrItemValue',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttrItemDesc' }),
        dataIndex: 'skuAttrItemDesc',
        key: 'skuAttrItemDesc',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'skuAttrItemEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.skuAttrItemEnable]}</span>,
      },
      // { title: '序号', dataIndex: 'seq', key: 'seq', align: 'center' },
      // { title: 'SKU属性代码', dataIndex: 'skuAttrCode', key: 'skuAttrCode', align: 'center' },
      // { title: 'SKU属性名称', dataIndex: 'skuAttrName', key: 'skuAttrName', align: 'center' },
    ];
  }

  render() {
    const { modalVisible, modalUpkeepType, modalUpkeepEntity, selectedRowKeys } = this.state;
    const {
      goodsSpuAttrSkuItemList,
      goodsSpuAttrSkuItemListPageSize,
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      spuAttrSkuEntity: sase, // 当前被选中产品sku属性列表行数据实体对象
      tableLoadingSpuAttrSkuItemStatus, // 当前产品SKU属性项列表加载状态
      tableCurrentPage,
    } = this.props;
    // 验证当前是否有选中1行产品sku属性数据
    if (!sase) return null;

    const dispatchType = 'goodsspuinfomanage/updateGoodsSpuAttrSkuItem'; // 停用dispatchType
    const tablePaginationOnChangeEventDispatchType =
      'goodsspuinfomanage/paginationGoodsSpuAttrSkuItem';
    const tablePaginationOnChangeEventCondition = {
      spuCode: mue.spuCode,
      skuAttrCode: sase.skuAttrCode,
      skuAttrName: sase.skuAttrName,
    };

    return (
      <MdcManageBlock
        tableTitle={`${formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttr' })}：${
          sase.skuAttrCode
        }+${sase.skuAttrName}`}
        tableColumns={this.tableColumns_GoodsSpuAttrSkuItem}
        tableDataSource={goodsSpuAttrSkuItemList}
        tableTotalSize={goodsSpuAttrSkuItemListPageSize}
        tableLoading={tableLoadingSpuAttrSkuItemStatus}
        tableOperateAreaCmp={this._tableOperateAreaCmp(3, mut, dispatchType)}
        tableCurrentPage={tableCurrentPage}
        tablePaginationOnChangeEvent={(page, pageSize) =>
          this.tablePaginationOnChangeEvent(
            page,
            pageSize,
            tablePaginationOnChangeEventDispatchType,
            tablePaginationOnChangeEventCondition
          )
        }
        tableRowSelection={{
          columnWidth: 30,
          columnTitle: '<>',
          selectedRowKeys,
          onSelect: (record, selected, selectedRows) => {
            if (selected) {
              this.setState({ tableRowSelectionEntity: record });
            } else if (selectedRows.length === 0) {
              this.setState({ tableRowSelectionEntity: null });
            }
          },
          onChange: selectedRowKeysArr => {
            this.setState({
              selectedRowKeys:
                selectedRowKeysArr.length > 1 ? [selectedRowKeysArr[1]] : selectedRowKeysArr,
            });
          }, // Table勾选回调事件
        }}
        modalVisible={modalVisible}
        modalTitle={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.spuSkuItem' })}
        modalContentCmp={
          <GoodsSpuAttrSkuItemUpkeep
            originGoodsSpuInfo={mue}
            spuAttrSkuEntity={sase}
            modalUpkeepType={modalUpkeepType}
            modalUpkeepEntity={modalUpkeepEntity}
            modalVisibleOnChange={this.modalVisibleOnChange}
            modalOnChangePageCurrent={this.changeCurrentPage}
          />
        }
        modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
      />
    );
  }

  /**
   * Table-表格头部动作处理组件
   * @param {number} tableTypeFlag 当前table的标识码,
   * 1为产品普通属性-表格标识;
   * 2为产品SKU属性-表格标识;
   * 3为产品SKU属性项值-表格标识;
   * 4为产品型号定义-表格标识;
   * 5为商品分组-表格标识
   * @param {number} actionType 当前产品-动作类型, 1为详情;2为维护;3为停用/启用;10为新增;20为删除
   * @param {string} dispatchType 停用/启用-动作对应dispatch的type值
   */
  _tableOperateAreaCmp = (tableTypeFlag, actionType, dispatchType = null) => {
    const { tableRowSelectionEntity: trse } = this.state;

    // 获取当前table状态按钮文字
    const getCurrentStatus = () => {
      if (tableTypeFlag === 1 && trse && !Number.isNaN(trse.attrEnable)) {
        // 产品普通属性-状态
        return trse.attrEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 2 && trse && !Number.isNaN(trse.skuAttrNameEnable)) {
        // 产品SKU属性-状态
        return trse.skuAttrNameEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 3 && trse && !Number.isNaN(trse.skuAttrItemEnable)) {
        // 产品SKU属性项值-状态
        return trse.skuAttrItemEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 4 && trse && !Number.isNaN(trse.modelEnable)) {
        // 产品型号定义-状态
        return trse.modelEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 5 && trse && !Number.isNaN(trse.groupEnable)) {
        // 商品分组-状态
        return trse.groupEnable === 1 ? status[0] : status[1];
      }
      return formatMessage({ id: 'form.common.enabledAndDisabled' });
    };

    // 校验Table勾选tableRowSelectionEntity值是否存在
    const checkTableRowSelectionEntity = (flag = -1) => {
      if (trse === null || trse === undefined) {
        const tips =
          flag === 2
            ? formatMessage({ id: 'button.common.modify' })
            : flag === 20
            ? formatMessage({ id: 'button.common.delete' })
            : formatMessage({ id: 'button.common.handle' });
        message.warn(
          `${formatMessage({ id: 'form.goodsSpuAttrSkuCmp.pleaseSelectData' })}[${tips}]`
        );
        return;
      }
      if (flag === 1) {
        // 详情动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 2) {
        // 维护动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 20) {
        // 删除动作
        this._deleteTableRowSelection(tableTypeFlag, trse);
      }
      if (flag === 3) {
        // 停用动作
        // 设置值为0,表示停用
        Object.keys(trse)
          .filter(item =>
            ['attrEnable', 'skuAttrItemEnable', 'skuAttrNameEnable', 'modelEnable'].includes(item)
          )
          .forEach(item => {
            trse[item] = trse[item] === 0 ? 1 : 0;
          });
        this.onClickActionExecuteEvent(flag, trse, null, dispatchType);
      }
    };

    return actionType === 1 ? (
      <a type="primary" onClick={() => checkTableRowSelectionEntity(1)}>
        {formatMessage({ id: 'button.common.details' })}
      </a>
    ) : (
      <div>
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(1);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.details' })}
        </a>
        <Divider type="vertical" />
        <a type="primary" onClick={() => this.onClickActionExecuteEvent(10)}>
          {formatMessage({ id: 'button.common.add' })}
        </a>
        <Divider type="vertical" />
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(2);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.modify' })}
        </a>
        <Divider type="vertical" />
        <a type="primary" onClick={() => checkTableRowSelectionEntity(3)}>
          {getCurrentStatus()}
        </a>
        {/* <a type="primary" onClick={() => checkTableRowSelectionEntity(20)}>删除</a> */}
      </div>
    );
  };

  /**
   * Table-删除行勾选数据
   * @param {number} tableTypeFlag 当前table的标识码,
   * 1为产品普通属性-表格标识;
   * 2为产品SKU属性-表格标识;
   * 3为产品SKU属性项值-表格标识;
   * 4为产品型号定义-表格标识;
   * 5为商品分组-表格标识
   * @param {object} item 当前table选中的行数据对象
   */
  _deleteTableRowSelection = (tableTypeFlag, item) => {
    const { dispatch } = this.props;
    switch (tableTypeFlag) {
      case 1: // 删除-产品普通属性
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuAttrNormal', payload: { id: item.id } });
        break;
      case 2: // 删除-产品SKU属性
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuAttrSku', payload: { id: item.id } });
        break;
      case 3: // 删除-产品SKU属性项值
        dispatch({
          type: 'goodsspuinfomanage/deleteGoodsSpuAttrSkuItem',
          payload: { id: item.id },
        });
        break;
      case 4: // 删除-产品型号定义
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuModel', payload: { id: item.id } });
        break;
      case 6: // 删除-商品分组
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSkuGroup', payload: { id: item.id } });
        break;
      default:
        break;
    }
  };
}

export default connect(({ goodsspuinfomanage }) => ({
  goodsSpuAttrSkuItemList: goodsspuinfomanage.goodsSpuAttrSkuItemList || [], // 当前选择-产品SKU属性项值(列表/分页)
  goodsSpuAttrSkuItemListPageSize: goodsspuinfomanage.goodsSpuAttrSkuItemListPageSize || 0, // 当前选择-产品SKU属性项值数据总数量
  tableLoadingSpuAttrSkuItemStatus: goodsspuinfomanage.tableLoadingSpuAttrSkuItemStatus || false, // 当前产品SKU属性项列表加载状态
  spuBeAdoptedStatus: goodsspuinfomanage.spuBeAdoptedStatus || false, // 当前产品-是否已被商品引用状态
}))(GoodsSpuAttrSkuItemCmp);
