/* eslint-disable no-nested-ternary */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import moment from 'moment';
import {
  Button,
  Divider,
  Form,
  Select,
  Input,
  TreeSelect,
  Dropdown,
  Icon,
  DatePicker,
  Card,
} from 'antd';
// import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import GoodsSpuInfoUpkeep from './GoodsSpuInfoUpkeep';
import { serverUrl } from '@/defaultSettings';
import styles from './index.less';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 产品信息管理组件
 */
class GoodsSpuInfoManage extends MdcManageBasic {
  constructor(props) {
    super(props);
    this.tableColumns = [
      {
        title: formatMessage({ id: 'form.goodsSpuInfoManage.catagoryName' }),
        dataIndex: 'catagoryName',
        key: 'catagoryName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.rowno' }),
        dataIndex: 'seq',
        key: 'seq',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuCode' }),
        dataIndex: 'spuCode',
        key: 'spuCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuName' }),
        dataIndex: 'spuName',
        key: 'spuName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuShortName' }),
        dataIndex: 'spuShortName',
        key: 'spuShortName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuInfoManage.mainClassName' }),
        dataIndex: 'mainClassName',
        key: 'mainClassName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandName' }),
        dataIndex: 'brandName',
        key: 'brandName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuTypeCode' }),
        dataIndex: 'parmValue',
        key: 'parmValue',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuTimeToMarket' }),
        dataIndex: 'spuTimeToMarket',
        key: 'spuTimeToMarket',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'spuEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.spuEnable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.goodsSpuInfoManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.goodsSpuInfoManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'goodsspuinfomanage/updateGoodsSpuInfo',
                  this.searchContent
                )
              }
            >
              {record.spuEnable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    // eslint-disable-next-line
    this.searchContent = {};
    // this.overlayMenuVisible = false // 下拉菜单显示/隐藏状态
    this.state = {
      ...this.state,
      overlayMenuVisible: false,
    };
  }

  /**
   * 响应点击处理
   * 进入产品详情之前获取产品相关联的数据
   */
  hookProcess = (type, record) => {
    const { dispatch } = this.props;
    if (type === 3) {
      // 如果是启用,则改成停用,如果是停用则改成启用
      record.spuEnable = record.spuEnable === 1 ? 0 : 1; // eslint-disable-line
      return record;
    }
    if (type === 10) {
      // 新增
      // 新增暂不加载任何数据
    } else {
      // 详情/维护
      // 获取当前选择产品-[产品普通属性]数据(列表/分页)
      dispatch({
        type: 'goodsspuinfomanage/paginationGoodsSpuAttrNormal',
        payload: { spuCode: record.spuCode, page: 1, pageSize: 10 },
      });
      // 获取当前选择产品-[产品SKU属性]数据(列表/分页), 产品SKU属性项值是在选择了产品SKU属性之后再加载
      dispatch({
        type: 'goodsspuinfomanage/paginationGoodsSpuAttrSku',
        payload: { spuCode: record.spuCode, page: 1, pageSize: 10 },
      });
      // 获取当前选择产品-[产品型号定义]数据(列表/分页)
      dispatch({
        type: 'goodsspuinfomanage/paginationGoodsSpuModel',
        payload: { spuCode: record.spuCode, page: 1, pageSize: 10 },
      });
      // 获取当前选择产品-[产品描述]数据(列表/分页)
      dispatch({
        type: 'goodsspuinfomanage/paginationGoodsSpuDesc',
        payload: { spuCode: record.spuCode, page: 1, pageSize: 10 },
      });
      // 获取当前选择产品-[商品分组]数据(列表/分页)
      dispatch({
        type: 'goodsspuinfomanage/paginationGoodsSkuGroup',
        payload: { spuCode: record.spuCode, page: 1, pageSize: 10 },
      });
      // 获取当前选择产品-[产品SKU属性]数据(列表/所有),供商品分组时选择从属的属性使用(需求：从属的属性来源于当前产品下的产品SKU属性表从不中选取)
      dispatch({
        type: 'goodsspuinfomanage/getSpuCondWithGoodsSpuAttrSkuList',
        payload: { spuCode: record.spuCode },
      });
    }
    if (type === 2) {
      // 维护
      // 获取当前选择产品-[是否已被商品引用]状态
      dispatch({
        type: 'goodsspuinfomanage/getSpuBeAdoptedStatus',
        payload: { spuCode: record.spuCode },
      });
    }
    return record;
  };

  /**
   * 默认加载产品信息数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-产品信息列表
    dispatch({
      type: 'goodsspuinfomanage/paginationGoodsSpuInfo',
      payload: { page: 1, pageSize: 10 },
    });

    // 以下请求数据是实现产品操作的公共基础数据,在此统一加载维护
    // 获取-[产品品牌]数据(列表/所有)
    dispatch({ type: 'goodsbrandmanage/getGoodsBrandAll' });
    // 获取-[产品大类]数据(列表/所有)
    dispatch({ type: 'goodsclassificationmanage/getGoodsClassificationAll' });
    // 获取-[产品类目]数据(树结构/所有)(同步显示：1级、2级、3级类目，校验产品必须挂在3级类目下,最多就3层)
    dispatch({ type: 'goodscatagorymanage/getGoodsCatagoryWith3LevelAll' });
    // 获取-[产品类型/公共参数]数据(列表/所有)-公用参数(所有)(条件为名称是goods_spu_type的产品类型数据)
    dispatch({
      type:
        'parmpublicparametermanage/getParmPublicParameterAllByParmPublicParameterTypeOfGoodsSpuType',
      payload: { parmTypeCode: 'goods_spu_type' },
    });
    // 获取-[描述类型/公共参数]数据(列表/所有)-公用参数(所有)(条件为名称是goods_desc_type的产品描述数据)
    dispatch({
      type:
        'parmpublicparametermanage/getParmPublicParameterAllByParmPublicParameterTypeOfGoodsDescType',
      payload: { parmTypeCode: 'goods_desc_type' },
    });
    // 获取-[属性名称]数据(列表/所有)
    dispatch({ type: 'goodsattrnamemanage/getGoodsAttrNameAll' });
  }

  /**
   * 根据条件搜索产品
   */
  onFuzzyGoodsSpuInfoHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    const values = getFieldsValue();
    if (values.spuTimeToMarket && values.spuTimeToMarket.length > 0) {
      values.spuTimeToMarketStart = values.spuTimeToMarket[0].format();
      values.spuTimeToMarketEnd = values.spuTimeToMarket[1].format();
    }
    this.searchContent = { ...values };
    delete this.searchContent.spuTimeToMarket;
    dispatch({
      type: 'goodsspuinfomanage/paginationGoodsSpuInfo',
      payload: { page: 1, pageSize: 10, searchCondition: this.searchContent },
    });
    // 返回第一页
    this.changeCurrentPage(1);
    // 清空搜素框
    // setFieldsValue({
    // });
  }

  /**
   * 导出文件请求
   */
  onExportGoodsSpuInfoFile() {
    const values = this.searchContent;
    const params = {
      ...values,
    };
    const formElement = document.createElement('form');
    formElement.style.display = 'display:none;';
    formElement.method = 'post';
    formElement.action = `${serverUrl}/mdc/goods/goodsspuinfo/export`;
    formElement.target = 'callBackTarget';
    Object.keys(params).map(item => {
      const inputElement = document.createElement('input');
      inputElement.type = 'hidden';
      inputElement.name = item;
      inputElement.value = params[item] || '';
      formElement.appendChild(inputElement);
      return item;
    });
    document.body.appendChild(formElement);
    formElement.submit();
    document.body.removeChild(formElement);
  }

  /**
   * 组装产品类目1级、2级、3级数据组件(树形结构/所有)
   */
  handleGoodsCatagoryWith3LevelCmp = data => {
    const { childrens } = data;
    const tempObj = {};
    tempObj.title = data.catagoryName;
    tempObj.value = data.catagoryCode;
    tempObj.key = data.catagoryCode;
    if (data.catagoryLevel === 1 || data.catagoryLevel === 2) tempObj.selectable = false;
    if (data.catagoryLevel === 3) tempObj.isLeaf = true;
    if (childrens !== undefined && childrens !== null && childrens.length > 0) {
      tempObj.children = childrens.map(item => this.handleGoodsCatagoryWith3LevelCmp(item));
    }
    return tempObj;
  };

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableCurrentPage,
      formItemLayout,
      tailFormItemLayout,
      overlayMenuVisible,
    } = this.state;

    const {
      goodsSpuInfoList, // 产品管理数据列表
      goodsSpuInfoListPageSize, // 产品管理数据总数量
      tableLoadingSpuInfoStatus, // 产品管理列表数据加载状态
      goodsCatagoryWith3LevelAll: gcw3la, // 产品类目1级、2级、3级数据(树形结构/所有)
      parmPublicParameterOfGoodsSpuTypeAll: ppposta, // 产品类型数据(所有) 拓展搜索
    } = this.props;

    /**
     * 下拉操作组件
     * 模糊搜索条件输入展示组件
     */
    const dropdownOverlay = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuShortName' })}>
            {getFieldDecorator('spuShortName')(<Input />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.goodsSpuInfoManage.mainClassCode' })}>
            {getFieldDecorator('mainClassCode')(<Input />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandCode' })}>
            {getFieldDecorator('brandCode')(<Input />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuTypeCode' })}>
            {getFieldDecorator('spuTypeCode')(
              <Select style={{ width: 172 }} allowClear>
                {ppposta.map(item => (
                  <Select.Option key={item.parmCode} value={item.parmCode}>
                    {item.parmValue}
                  </Select.Option>
                ))}
              </Select>
            )}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuTimeToMarket' })}>
            {getFieldDecorator('spuTimeToMarket')(
              <DatePicker.RangePicker style={{ width: 210 }} />
            )}
          </Form.Item>
          <Form.Item>
            <Button
              onClick={() => {
                const {
                  form: { resetFields },
                } = this.props;
                resetFields([
                  'spuShortName',
                  'mainClassCode',
                  'brandCode',
                  'spuTypeCode',
                  'spuTimeToMarket',
                ]);
              }}
            >
              {formatMessage({ id: 'form.goodsSpuInfoManage.reset' })}
            </Button>
            &emsp;&emsp;
            {/* <Button onClick={() => this.setState({ overlayMenuVisible: false })}>确定</Button> */}
            {/* &emsp;&emsp; */}
          </Form.Item>
        </div>
      );
    };
    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      // 产品类目树形结构数据
      const goodsCatagoryTreeData = gcw3la
        .filter(item => item.childrens.length > 0)
        .map(item => this.handleGoodsCatagoryWith3LevelCmp(item));
      return (
        <div className={styles.OverlaySearch}>
          <Form {...formItemLayout} className={styles.OverlaySearch}>
            <Form.Item
              label={formatMessage({ id: 'form.goodsSpuInfoManage.catagoryCode' })}
              className={`${styles.OverlaySearch} ${styles.searchStyle}`}
            >
              {getFieldDecorator('catagoryCode')(
                <TreeSelect
                  style={{ width: 120 }}
                  showCheckedStrategy="SHOW_ALL"
                  treeData={goodsCatagoryTreeData}
                  allowClear
                />
              )}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuCode' })}
              className={`${styles.OverlaySearch} ${styles.searchStyle}`}
            >
              {getFieldDecorator('spuCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsSpuBasicUpkeep.spuName' })}
              className={`${styles.OverlaySearch} ${styles.searchStyle}`}
            >
              {getFieldDecorator('spuName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.goodsSpuInfoManage.spuEnable' })}
              className={`${styles.OverlaySearch} ${styles.searchStyle}`}
            >
              {getFieldDecorator('spuEnable')(
                <Select style={{ width: 80 }} allowClear>
                  <Select.Option value="1">
                    {formatMessage({ id: 'form.common.enabled' })}
                  </Select.Option>
                  <Select.Option value="0">
                    {formatMessage({ id: 'form.common.disabled' })}
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
            <Dropdown
              overlay={dropdownOverlay()}
              trigger={['click']}
              onVisibleChange={() => this.setState({ overlayMenuVisible: !overlayMenuVisible })}
              visible={overlayMenuVisible}
              overlayStyle={{ width: 520 }}
            >
              <Button style={{ marginTop: 4, marginRight: 10 }}>
                <Icon type="menu-fold" />
              </Button>
            </Dropdown>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.OverlaySearch} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyGoodsSpuInfoHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            icon="download"
            style={{ marginTop: 4, marginRight: 10 }}
            onClick={() => this.onExportGoodsSpuInfoFile()}
          >
            {formatMessage({ id: 'button.common.export' })}
          </Button>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                this.hookProcess(10, null),
                formatMessage({ id: 'button.goodsSpuInfoManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };

    const tablePaginationOnChangeEventDispatchType = 'goodsspuinfomanage/paginationGoodsSpuInfo';

    return (
      <Card bordered={false}>
        <div>
          <MdcManageBlock
            tableTitle={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage' })}
            tableColumns={this.tableColumns}
            tableDataSource={goodsSpuInfoList}
            tableOperateAreaCmp={tableOperateAreaCmp()}
            tableLoading={tableLoadingSpuInfoStatus}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={(page, pageSize) =>
              this.tablePaginationOnChangeEvent(
                page,
                pageSize,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            tableTotalSize={goodsSpuInfoListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
            modalTitle={modalTitle}
            modalVisible={modalVisible}
            modalContentCmp={
              <GoodsSpuInfoUpkeep
                modalUpkeepType={modalUpkeepType}
                modalUpkeepEntity={modalUpkeepEntity}
                modalVisibleOnChange={this.modalVisibleOnChange}
                modalOnChangePageCurrent={this.changeCurrentPage}
                modalSearchCondition={this.searchContent}
              />
            }
            modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
          />
        </div>
      </Card>
    );
  }
}

export default connect(
  ({ goodsspuinfomanage, goodscatagorymanage, parmpublicparametermanage }) => ({
    // spuBeAdoptedStatus: goodsspuinfomanage.spuBeAdoptedStatus || false, // 当前产品-是否已被商品引用状态
    goodsSpuInfoList: goodsspuinfomanage.goodsSpuInfoList || [],
    goodsSpuInfoListPageSize: goodsspuinfomanage.goodsSpuInfoListPageSize || 0,
    tableLoadingSpuInfoStatus: goodsspuinfomanage.tableLoadingSpuInfoStatus || false,
    goodsCatagoryWith3LevelAll: goodscatagorymanage.goodsCatagoryWith3LevelAll || [], // 产品类目1级、2级、3级数据(树形结构/所有)
    parmPublicParameterOfGoodsSpuTypeAll:
      parmpublicparametermanage.parmPublicParameterOfGoodsSpuTypeAll || [], // 产品类型数据(所有)(从公共参数定义接口获取数据) 拓展搜索
  })
)(Form.create()(GoodsSpuInfoManage));
