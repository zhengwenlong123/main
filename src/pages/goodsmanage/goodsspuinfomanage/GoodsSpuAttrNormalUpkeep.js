import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Form, Input, Checkbox, message, Select } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import { execOneRequest } from '@/utils/mdcutil';
import styles from './index.less';
// import { async } from 'q';

moment.locale('zh-cn');
/**
 * 产品普通属性-维护组件(Table进入)
 */
@ValidationFormHoc
class GoodsSpuAttrNormalUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      dispatch,
      validationForm,
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      goodsAttrNameAll: gana, // 属性名称数据所有(列表)
      originGoodsSpuInfo: ogsi, // 原始产品信息数据
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    values.attrEnable = values.attrEnable ? 1 : 0;
    values.spuCode = ogsi.spuCode;
    const curSelectAttrNames = gana.filter(item => item.attrCode === values.attrNormal);
    values.attrCode = curSelectAttrNames[0].attrCode;
    values.attrName = curSelectAttrNames[0].attrName;

    // 验证新数据唯一性
    if (mut === 10 || mue.attrCode !== values.attrCode || mue.attrName !== values.attrName) {
      const verifyResult = await execOneRequest('/mdc/goods/goodsspuattrnormal/verifyUnique', {
        spuCode: values.spuCode,
        attrCode: values.attrCode,
        attrName: values.attrName,
      });
      if (verifyResult) {
        if (verifyResult.status) {
          // 唯一性验证通过
        } else {
          message.error(`${verifyResult.message}`, 8);
          return;
        }
      } else {
        message.warn(
          formatMessage({ id: 'form.goodsSpuAttrNormalUpkeep.commonAttribute-conformity' })
        );
        return;
      }
    }
    this.setState({ confirmLoading: true });
    // 维护
    if (mut === 2) {
      values.id = mue.id;
      dispatch({
        type: 'goodsspuinfomanage/updateGoodsSpuAttrNormal',
        payload: values,
        callback: this.onCallback,
      });
    }
    // 新增
    if (mut === 10) {
      dispatch({
        type: 'goodsspuinfomanage/addGoodsSpuAttrNormal',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  /**
   * 普通属性选择改变回调
   */
  onChangeGoodsSkuAttrNormalHandle = () => {};

  /**
   * 普通属性搜索回调
   */
  onSearchGoodsSkuAttrNormalHandle = () => {};

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut, // 当前table选中的1行数据对象
      goodsAttrNameAll: gana, // 属性名称数据所有(列表)
    } = this.props;
    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form {...formItemLayout}>
            {/* {mut === 10 ? null : (
              <Form.Item label="属性代码">
                {getFieldDecorator('attrCode', {rules:[{required:true}]})(<Input disabled />)}
              </Form.Item>
            )}
            <Form.Item label="属性名称">
              {getFieldDecorator('attrName',{rules:[{required: true}]})(<Input disabled={mut === 1} />)}
            </Form.Item> */}
            <Form.Item label={formatMessage({ id: 'form.goodsSpuAttrNormalUpkeep.attrNormal' })}>
              {getFieldDecorator('attrNormal', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsSpuAttrNormalUpkeep.attrNormal.choice',
                    }),
                  },
                ],
              })(
                <Select
                  disabled={mut === 1}
                  allowClear
                  showSearch
                  filterOption={false}
                  onSearch={this.onSearchGoodsSkuAttrNormalHandle}
                  onChange={this.onChangeGoodsSkuAttrNormalHandle}
                >
                  {gana.map(item =>
                    item.attrEnable === 0 ? (
                      ''
                    ) : (
                      <Select.Option key={item.attrCode} value={item.attrCode} attrnameref={item}>
                        {/* {`${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrCode' })}：${
                          item.attrCode
                        } / ${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrName' })}：${
                          item.attrName
                        }`} */}
                        {`${item.attrName}`}
                      </Select.Option>
                    )
                  )}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuAttrNormalUpkeep.attrValue' })}>
              {getFieldDecorator('attrValue', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsSpuAttrNormalUpkeep.attrValue.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={200} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuAttrNormalUpkeep.attrDesc' })}>
              {getFieldDecorator('attrDesc')(
                <Input.TextArea rows={5} disabled={mut === 1} maxLength={1000} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('attrEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ goodsattrnamemanage }) => ({
  goodsAttrNameAll: goodsattrnamemanage.goodsAttrNameAll || [], // 属性名称数据所有(列表)
}))(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        attrNormal: Form.createFormField({ value: mue ? mue.attrCode : '' }),
        // attrName: Form.createFormField({ value: mue ? mue.attrName : '' }),
        attrValue: Form.createFormField({ value: mue ? mue.attrValue : '' }),
        attrDesc: Form.createFormField({ value: mue ? mue.attrDesc : '' }),
        attrEnable: Form.createFormField({ value: mue ? mue.attrEnable === 1 : true }),
      };
    },
  })(GoodsSpuAttrNormalUpkeep)
);
