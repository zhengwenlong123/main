import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Form, Checkbox, message, Select } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import { execOneRequest } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品SKU属性维护组件(Table进入)
 */
@ValidationFormHoc
class GoodsSpuAttrSkuUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      dispatch,
      validationForm,
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      goodsAttrNameAll: gana, // 属性名称数据所有(列表)
      originGoodsSpuInfo: ogsi, // 原始产品信息数据
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    values.spuCode = ogsi.spuCode;
    values.skuAttrNameEnable = values.skuAttrNameEnable ? 1 : 0;

    values.spuCode = ogsi.spuCode;
    const curSelectAttrNames = gana.filter(item => item.attrCode === values.attrSku);
    values.skuAttrCode = curSelectAttrNames[0].attrCode;
    values.skuAttrName = curSelectAttrNames[0].attrName;

    // 验证数据唯一性
    if (
      mut === 10 ||
      mue.skuAttrCode !== values.skuAttrCode ||
      mue.skuAttrName !== values.skuAttrName
    ) {
      const verifyResult = await execOneRequest('/mdc/goods/goodsspuattrsku/verifyUnique', {
        spuCode: values.spuCode,
        skuAttrCode: values.skuAttrCode,
        skuAttrName: values.skuAttrName,
      });
      if (verifyResult) {
        if (verifyResult.status) {
          // 验证通过
        } else {
          message.error(verifyResult.message, 8);
          return;
        }
      } else {
        message.warn(formatMessage({ id: 'form.goodsSpuAttrSkuUpkeep.productSkuAttr-conformity' }));
        return;
      }
    }

    this.setState({ confirmLoading: true });

    // 维护
    if (mut === 2) {
      values.id = mue.id;
      dispatch({
        type: 'goodsspuinfomanage/updateGoodsSpuAttrSku',
        payload: values,
        callback: this.onCallback,
      });
    }
    // 新增
    if (mut === 10) {
      dispatch({
        type: 'goodsspuinfomanage/addGoodsSpuAttrSku',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
      goodsAttrNameAll: gana, // 属性名称数据所有(列表)
    } = this.props;

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form {...formItemLayout}>
            {/* mut === 10 ? null : (
              <Form.Item label="代码">
                {getFieldDecorator('skuAttrCode', { rules: [{ required: true }] })(<Input disabled />)}
              </Form.Item>
            )
            <Form.Item label="名称">
              {getFieldDecorator('skuAttrName')(<Input disabled />)}
            </Form.Item> */}
            <Form.Item label={formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttr' })}>
              {getFieldDecorator('attrSku', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.goodsSpuAttrSkuItemCmp.skuAttr.choice' }),
                  },
                ],
              })(
                <Select
                  disabled={mut === 1}
                  allowClear
                  showSearch
                  filterOption={false}
                  onSearch={this.onSearchGoodsSkuAttrNormalHandle}
                  onChange={this.onChangeGoodsSkuAttrNormalHandle}
                >
                  {gana.map(item => (
                    <Select.Option key={item.attrCode} value={item.attrCode} attrnameref={item}>
                      {/* {`${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrCode' })}：${
                        item.attrCode
                      }  /  ${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrName' })}：${
                        item.attrName
                      }`} */}
                      {`${item.attrName}`}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('skuAttrNameEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ goodsattrnamemanage }) => ({
  goodsAttrNameAll: goodsattrnamemanage.goodsAttrNameAll || [], // 属性名称数据所有(列表)
}))(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        attrSku: Form.createFormField({ value: mue ? mue.skuAttrCode : '' }),
        // skuAttrName: Form.createFormField({ value: mue ? mue.skuAttrName : '' }),
        skuAttrNameEnable: Form.createFormField({
          value: mue ? mue.skuAttrNameEnable === 1 : true,
        }),
      };
    },
  })(GoodsSpuAttrSkuUpkeep)
);
