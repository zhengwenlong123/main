/* eslint-disable no-nested-ternary */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/no-redundant-should-component-update */
/* eslint-disable no-console */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Button, Input, Checkbox, Select, message, InputNumber } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import { execOneRequest } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 商品分组-维护组件
 */
@ValidationFormHoc
class GoodsSkuGroupUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      dispatch,
      validationForm,
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      originGoodsSpuInfo: ogsi, // 当前产品信息源数据
      spuCondWithGoodsSpuAttrSkuList: scwgsasl, // 该产品下产品SKU属性数据(列表/所有)
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    if (!values.subAttrCode1) {
      message.warn(formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrCode.placeholder' }));
      return;
    }
    values.groupEnable ? (values.groupEnable = 1) : (values.groupEnable = 0);
    values.spuCode = ogsi.spuCode;
    values.subAttrName1 = this.parseOnlySubAttrName(scwgsasl, values.subAttrCode1);
    values.subAttrName2 = this.parseOnlySubAttrName(scwgsasl, values.subAttrCode2);
    values.subAttrName3 = this.parseOnlySubAttrName(scwgsasl, values.subAttrCode3);

    // 验证商品分组数据唯一性
    if (mut === 10 || mue.groupCode !== values.groupCode || mue.groupName !== values.groupName) {
      const verifyResult = await execOneRequest('/mdc/goods/goodsskugroup/verifyUnique', {
        spuCode: values.spuCode,
        groupCode: values.groupCode,
        groupName: values.groupName,
      });
      if (verifyResult) {
        if (verifyResult.status) {
          // 验证通过
        } else {
          message.error(`${verifyResult.message}`);
          return;
        }
      } else {
        message.warn(
          formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrCode.skuGroup-conformity' })
        );
        return;
      }
    }
    this.setState({ confirmLoading: true });
    if (mut === 2) {
      // 维护
      values.id = mue.id;
      dispatch({
        type: 'goodsspuinfomanage/updateGoodsSkuGroup',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (mut === 10) {
      // 新增
      dispatch({
        type: 'goodsspuinfomanage/addGoodsSkuGroup',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  /**
   * 从属的属性验证是否重复
   */
  changeSubAttrCode = (name, value) => {
    const {
      form: { getFieldsValue, resetFields },
    } = this.props;
    const thatValues = getFieldsValue(['subAttrCode1', 'subAttrCode2', 'subAttrCode3']);
    for (const item of Object.keys(thatValues)) {
      if (item !== name && thatValues[item] === value) {
        resetFields(item, []);
        break;
      }
    }
  };

  /**
   * 根据从属的属性代码查找从属的属性名称值返回
   * @param {array} refer 当前选择-产品SKU属性(列表)
   * @param {string} code 待匹配从属的属性代码
   */
  parseOnlySubAttrName = (refer, code) => {
    if (refer === undefined || code === undefined) return '';
    const tempArr = refer.filter(item => item.skuAttrCode === code);
    if (tempArr === null || tempArr.length === 0) return '';
    return tempArr[0].skuAttrName;
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
      spuCondWithGoodsSpuAttrSkuList: scwgsasl, // 该产品下产品SKU属性数据(列表/所有)
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div className={`${styles.modalHandleFormScopeArea}`}>
        <Form {...formItemLayout}>
          <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.spuInfo' })}>
            {getFieldDecorator('spuInfo', { rules: [{ required: true, message: '' }] })(
              <Input disabled />
            )}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.goodsSkuInfoManage.groupCode' })}>
            {getFieldDecorator('groupCode', {
              rules: [
                {
                  required: true,
                  message: formatMessage({ id: 'form.goodsSkuInfoManage.groupCode.placeholder' }),
                },
              ],
            })(<Input disabled={mut === 1} maxLength={50} />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.goodsSkuInfoManage.groupName' })}>
            {getFieldDecorator('groupName', {
              rules: [
                {
                  required: true,
                  message: formatMessage({
                    id: 'form.goodsSkuInfoManage.skugroupName.placeholder',
                  }),
                },
              ],
            })(<Input disabled={mut === 1} maxLength={200} />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.groupShortName' })}>
            {getFieldDecorator('groupShortName', {
              rules: [
                {
                  required: true,
                  message: formatMessage({
                    id: 'form.goodsSkuGroupUpkeep.groupShortName.placeholder',
                  }),
                },
              ],
            })(<Input disabled={mut === 1} maxLength={200} />)}
          </Form.Item>
          {mut !== 1 ? (
            <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttr1' })}>
              {getFieldDecorator('subAttrCode1', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsSkuGroupUpkeep.subAttrCode.placeholder',
                    }),
                  },
                ],
              })(
                <Select
                  disabled={mut === 1}
                  onSelect={value => this.changeSubAttrCode('subAttrCode1', value)}
                >
                  {scwgsasl.map(item => (
                    <Select.Option key={item.skuAttrCode} value={item.skuAttrCode}>
                      {/* {`${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrCode' })}：${
                        item.skuAttrCode
                      } / ${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrName' })}：${
                        item.skuAttrName
                      }`} */}
                      {`${item.skuAttrName}`}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          ) : (
            <div>
              <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrCode1' })}>
                {getFieldDecorator('subAttrCode1', { rules: [{ required: true, message: '' }] })(
                  <Input disabled />
                )}
              </Form.Item>
              <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrName1' })}>
                {getFieldDecorator('subAttrName1', { rules: [{ required: true, message: '' }] })(
                  <Input disabled />
                )}
              </Form.Item>
            </div>
          )}
          {mut !== 1 ? (
            <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttr2' })}>
              {getFieldDecorator('subAttrCode2')(
                <Select
                  disabled={mut === 1}
                  onSelect={value => this.changeSubAttrCode('subAttrCode2', value)}
                >
                  {scwgsasl.map(item => (
                    <Select.Option key={item.skuAttrCode} value={item.skuAttrCode}>
                      {/* {`${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrCode' })}：${
                        item.skuAttrCode
                      } / ${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrName' })}：${
                        item.skuAttrName
                      }`} */}
                      {`${item.skuAttrName}`}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          ) : (
            <div>
              <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrCode2' })}>
                {getFieldDecorator('subAttrCode2')(<Input disabled />)}
              </Form.Item>
              <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrName2' })}>
                {getFieldDecorator('subAttrName2')(<Input disabled />)}
              </Form.Item>
            </div>
          )}
          {mut !== 1 ? (
            <Form.Item
              label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttr3' })}
              onSelect={value => this.changeSubAttrCode('subAttrCode3', value)}
              allowClear
            >
              {getFieldDecorator('subAttrCode3')(
                <Select
                  disabled={mut === 1}
                  onSelect={value => this.changeSubAttrCode('subAttrCode3', value)}
                >
                  {scwgsasl.map(item => (
                    <Select.Option key={item.skuAttrCode} value={item.skuAttrCode}>
                      {/* {`${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrCode' })}：${
                        item.skuAttrCode
                      } / ${formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrName' })}：${
                        item.skuAttrName
                      }`} */}
                      {`${item.skuAttrName}`}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
          ) : (
            <div>
              <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrCode3' })}>
                {getFieldDecorator('subAttrCode3')(<Input disabled />)}
              </Form.Item>
              <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrName3' })}>
                {getFieldDecorator('subAttrName3')(<Input disabled />)}
              </Form.Item>
            </div>
          )}
          <Form.Item label={formatMessage({ id: 'form.goodsSkuGroupUpkeep.groupDesc' })}>
            {getFieldDecorator('groupDesc')(
              <Input.TextArea disabled={mut === 1} rows={3} maxLength={1000} />
            )}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.common.rowno' })}>
            {getFieldDecorator('seq')(
              <InputNumber min={1} max={10000} defaultChecked={1} disabled={mut === 1} />
            )}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
            {getFieldDecorator('groupEnable', {
              valuePropName: 'checked',
              rules: [{ required: false }],
            })(
              <Checkbox disabled={mut === 1}>
                {formatMessage({ id: 'form.common.whetherEnabled' })}
              </Checkbox>
            )}
          </Form.Item>
        </Form>
        {mut === 1 ? null : (
          <div className={styles.modalHandleBtnArea}>
            <Button type="default" htmlType="button" onClick={() => modalVisibleOnChange(false)}>
              {formatMessage({ id: 'button.common.cancel' })}
            </Button>
            <Button
              type="primary"
              htmlType="button"
              onClick={this.onClickSubmit}
              loading={confirmLoading}
            >
              {formatMessage({ id: 'button.common.submit' })}
            </Button>
          </div>
        )}
      </div>
    );
  }
}

export default connect(({ goodsspuinfomanage }) => ({
  spuCondWithGoodsSpuAttrSkuList: goodsspuinfomanage.spuCondWithGoodsSpuAttrSkuList || [], // 该产品下产品SKU属性数据(列表/所有)
}))(
  Form.create({
    mapPropsToFields(props) {
      const {
        modalUpkeepEntity: mue,
        originGoodsSpuInfo: ogsi, // 当前产品信息源数据
      } = props;

      return {
        spuInfo: Form.createFormField({ value: `${ogsi.spuCode}+${ogsi.spuName}` }),
        groupCode: Form.createFormField({ value: mue ? mue.groupCode : '' }),
        groupName: Form.createFormField({ value: mue ? mue.groupName : '' }),
        groupShortName: Form.createFormField({ value: mue ? mue.groupShortName : '' }),
        groupDesc: Form.createFormField({ value: mue ? mue.groupDesc : '' }),
        subAttrCode1: Form.createFormField({ value: mue ? mue.subAttrCode1 : '' }),
        subAttrName1: Form.createFormField({ value: mue ? mue.subAttrName1 : '' }),
        subAttrCode2: Form.createFormField({ value: mue ? mue.subAttrCode2 : '' }),
        subAttrName2: Form.createFormField({ value: mue ? mue.subAttrName2 : '' }),
        subAttrCode3: Form.createFormField({ value: mue ? mue.subAttrCode3 : '' }),
        subAttrName3: Form.createFormField({ value: mue ? mue.subAttrName3 : '' }),
        seq: Form.createFormField({ value: mue ? mue.seq : 1 }),
        groupEnable: Form.createFormField({ value: mue ? mue.groupEnable === 1 : true }),
      };
    },
  })(GoodsSkuGroupUpkeep)
);
