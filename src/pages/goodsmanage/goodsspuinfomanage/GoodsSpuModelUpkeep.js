import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Form, Input, Checkbox, message } from 'antd';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import { execOneRequest } from '@/utils/mdcutil';
import styles from './index.less';

/**
 * 产品型号定义-维护组件(Table进入)
 */
@ValidationFormHoc
class GoodsSpuModelUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.info(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      dispatch,
      validationForm,
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      originGoodsSpuInfo: ogsi, // 原始产品信息数据
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    values.spuCode = ogsi.spuCode;
    values.modelEnable = values.modelEnable ? 1 : 0;

    // 验证数据唯一性
    if (mut === 10 || mue.modelCode !== values.modelCode || mue.modelName !== values.modelName) {
      const verifyResults = await execOneRequest('/mdc/goods/goodsspumodel/verifyUnique', {
        spuCode: values.spuCode,
        modelCode: values.modelCode,
        modelName: values.modelName,
      });
      if (verifyResults) {
        if (verifyResults.status) {
          // 验证通过
        } else {
          message.error(`${verifyResults.message}`, 8);
          return;
        }
      } else {
        message.warn(formatMessage({ id: 'form.goodsSpuModelUpkeep.spuCode-conformity' }));
        return;
      }
    }
    this.setState({ confirmLoading: true });
    if (mut === 2) {
      // 维护
      values.id = mue.id;
      dispatch({
        type: 'goodsspuinfomanage/updateGoodsSpuModel',
        payload: values,
        callback: this.onCallback,
      });
    }

    if (mut === 10) {
      // 新增
      dispatch({
        type: 'goodsspuinfomanage/addGoodsSpuModel',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
    } = this.props;

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuInfoUpkeepBasic.modelCode' })}>
              {getFieldDecorator('modelCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsSpuInfoUpkeepBasic.modelCode.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsMaterielInfoUpkeep.modelName' })}>
              {getFieldDecorator('modelName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsMaterielInfoUpkeep.modelName.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuInfoUpkeepBasic.modelShortName' })}>
              {getFieldDecorator('modelShortName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsSpuInfoUpkeepBasic.modelShortName.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsSpuModelUpkeep.modelDesc' })}>
              {getFieldDecorator('modelDesc')(<Input.TextArea rows={5} disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('modelEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        modelCode: Form.createFormField({ value: mue ? mue.modelCode : '' }),
        modelName: Form.createFormField({ value: mue ? mue.modelName : '' }),
        modelShortName: Form.createFormField({ value: mue ? mue.modelShortName : '' }),
        modelDesc: Form.createFormField({ value: mue ? mue.modelDesc : '' }),
        modelEnable: Form.createFormField({ value: mue ? mue.modelEnable === 1 : true }),
      };
    },
  })(GoodsSpuModelUpkeep)
);
