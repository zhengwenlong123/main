/* eslint-disable no-fallthrough */
/* eslint-disable consistent-return */
/* eslint-disable no-nested-ternary */
/* eslint-disable camelcase */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Divider, message } from 'antd';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import GoodsSpuAttrNormalUpkeep from './GoodsSpuAttrNormalUpkeep'; // 产品普通属性-维护组件
import GoodsSpuAttrSkuCmp from './GoodsSpuAttrSkuCmp'; // 产品SKU属性-维护组件(包含产品SKU属性项值维护)
import GoodsSpuModelUpkeep from './GoodsSpuModelUpkeep'; // 产品型号定义-维护组件
import EnclosureTableBasic from '@/components/Enclosure/EnclosureTableBasic'; // 附件描述维护统一使用此组件封装好了
import GoodsSkuGroupUpkeep from './GoodsSkuGroupUpkeep'; // 商品分组-维护组件

const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 产品信息维护[抽象类]
 */
class GoodsSpuInfoUpkeepBasic extends MdcManageBasic {
  constructor(props) {
    super(props);
    // state
    this.state = {
      ...this.state,
      selectedRowKeys: [],
      // Table行勾选数据对象
      tableRowSelectionEntity: null,
    };

    // 产品普通属性-表格列表字段定义
    this.tableColumns_GoodsSpuAttrNormal = [
      // {
      //   title: formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrCode' }),
      //   dataIndex: 'attrCode',
      //   key: 'attrCode',
      //   align: 'center',
      // },
      {
        title: formatMessage({ id: 'form.goodsSkuGroupUpkeep.skuAttrName' }),
        dataIndex: 'attrName',
        key: 'attrName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuAttrNormalUpkeep.attrValue' }),
        dataIndex: 'attrValue',
        key: 'attrValue',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'attrEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.attrEnable]}</span>,
      },
      // { title: '属性描述', dataIndex: 'attrDesc', key: 'attrDesc', align: 'center' },
    ];
    // 产品型号定义-表格列表字段定义
    this.tableColumns_GoodsSpuModel = [
      {
        title: formatMessage({ id: 'form.goodsSpuInfoUpkeepBasic.modelCode' }),
        dataIndex: 'modelCode',
        key: 'modelCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.modelName' }),
        dataIndex: 'modelName',
        key: 'modelName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSpuInfoUpkeepBasic.modelShortName' }),
        dataIndex: 'modelShortName',
        key: 'modelShortName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'modelEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.modelEnable]}</span>,
      },
      // { title: '型号描述', dataIndex: 'modelDesc', key: 'modelDesc', align: 'center' },
    ];
    // 产品描述-表格列表字段定义
    this.tableColumns_GoodsSpuDesc = [
      {
        title: formatMessage({ id: 'form.common.rowno' }),
        dataIndex: 'seq',
        key: 'seq',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsMaterielInfoUpkeep.spuName' }),
        dataIndex: 'spuName',
        key: 'spuName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.descriptionType' }),
        dataIndex: 'descTypeValue',
        key: 'descTypeValue',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.descriptionName' }),
        dataIndex: 'descName',
        key: 'descName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'descEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.descEnable]}</span>,
      },
      // { title: '存放地址', dataIndex: 'address', key: 'address', align: 'center' },
      // { title: '描述备注', dataIndex: 'descNotes', key: 'descNotes', align: 'center' },
    ];
    // 商品分组-表格列表字段定义
    this.tableColumns_GoodsSkuGroup = [
      {
        title: formatMessage({ id: 'form.goodsSkuInfoManage.groupCode' }),
        dataIndex: 'groupCode',
        key: 'groupCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuInfoManage.groupName' }),
        dataIndex: 'groupName',
        key: 'groupName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.goodsSkuGroupUpkeep.groupShortName' }),
        dataIndex: 'groupShortName',
        key: 'groupShortName',
        align: 'center',
      },
      // {
      //   title: formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrCode1' }),
      //   dataIndex: 'subAttrCode1',
      //   key: 'subAttrCode1',
      //   align: 'center',
      // },
      {
        title: formatMessage({ id: 'form.goodsSkuGroupUpkeep.subAttrName1' }),
        dataIndex: 'subAttrName1',
        key: 'subAttrName1',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'groupEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.groupEnable]}</span>,
      },
      // { title: '从属的属性代码2', dataIndex: 'subAttrCode2', key: 'subAttrCode1', align: 'center' },
      // { title: '从属的属性名称2', dataIndex: 'subAttrName2', key: 'subAttrName1', align: 'center' },
      // { title: '从属的属性代码3', dataIndex: 'subAttrCode3', key: 'subAttrCode1', align: 'center' },
      // { title: '从属的属性名称3', dataIndex: 'subAttrName3', key: 'subAttrName1', align: 'center' },
      // { title: '商品分组描述', dataIndex: 'groupDesc', key: 'groupDesc', align: 'center' },
      // { title: '序号', dataIndex: 'seq', key: 'seq', align: 'center' },
    ];
  }

  /**
   * 渲染Table组件-产品普通属性
   */
  renderGoodsSpuAttrNormalCmpTable = (mue, mut) => {
    const {
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableCurrentPage,
      selectedRowKeys,
    } = this.state;
    const { goodsSpuAttrNormalList, goodsSpuAttrNormalListPageSize } = this.props;
    const dispatchType = 'goodsspuinfomanage/updateGoodsSpuAttrNormal'; // 停用dispatchType
    const tablePaginationOnChangeEventDispatchType =
      'goodsspuinfomanage/paginationGoodsSpuAttrNormal';
    const tablePaginationOnChangeEventCondition = { spuCode: mue.spuCode };
    return (
      <MdcManageBlock
        tableTitle={`${formatMessage({ id: 'form.goodsSpuAttrSkuCmp.spu' })}：${mue.spuCode}+${
          mue.spuName
        }`}
        tableColumns={this.tableColumns_GoodsSpuAttrNormal}
        tableDataSource={goodsSpuAttrNormalList}
        tableTotalSize={goodsSpuAttrNormalListPageSize}
        tableOperateAreaCmp={this._tableOperateAreaCmp(1, mut, dispatchType)}
        tableCurrentPage={tableCurrentPage}
        tablePaginationOnChangeEvent={(page, pageSize) =>
          this.tablePaginationOnChangeEvent(
            page,
            pageSize,
            tablePaginationOnChangeEventDispatchType,
            tablePaginationOnChangeEventCondition
          )
        }
        tableRowSelection={{
          columnWidth: 30,
          columnTitle: '<>',
          selectedRowKeys,
          onSelect: (record, selected, selectedRows) => {
            if (selected) {
              this.setState({ tableRowSelectionEntity: record });
            } else if (selectedRows.length === 0) {
              this.setState({ tableRowSelectionEntity: null });
            }
          },
          onChange: selectedRowKeysArr => {
            this.setState({
              selectedRowKeys:
                selectedRowKeysArr.length > 1 ? [selectedRowKeysArr[1]] : selectedRowKeysArr,
            });
          }, // Table勾选回调事件
        }}
        modalVisible={modalVisible}
        modalTitle={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.spuCommonAttribute' })}
        modalContentCmp={
          <GoodsSpuAttrNormalUpkeep
            originGoodsSpuInfo={mue}
            modalUpkeepType={modalUpkeepType}
            modalUpkeepEntity={modalUpkeepEntity}
            modalVisibleOnChange={this.modalVisibleOnChange}
            modalOnChangePageCurrent={this.changeCurrentPage}
          />
        }
        modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
      />
    );
  };

  /**
   * 渲染Table组件-产品型号定义
   */
  renderGoodsSpuModelCmpTable = (mue, mut) => {
    const {
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableCurrentPage,
      selectedRowKeys,
    } = this.state;
    const { goodsSpuModelList, goodsSpuModelListPageSize } = this.props;
    const dispatchType = 'goodsspuinfomanage/updateGoodsSpuModel'; // 停用dispatchType
    const tablePaginationOnChangeEventDispatchType = 'goodsspuinfomanage/paginationGoodsSpuModel';
    const tablePaginationOnChangeEventCondition = { spuCode: mue.spuCode };
    return (
      <MdcManageBlock
        tableTitle={`${formatMessage({ id: 'form.goodsSpuAttrSkuCmp.spu' })}：${mue.spuCode}+${
          mue.spuName
        }`}
        tableColumns={this.tableColumns_GoodsSpuModel}
        tableDataSource={goodsSpuModelList}
        tableTotalSize={goodsSpuModelListPageSize}
        tableOperateAreaCmp={this._tableOperateAreaCmp(4, mut, dispatchType)}
        tableCurrentPage={tableCurrentPage}
        tablePaginationOnChangeEvent={(page, pageSize) =>
          this.tablePaginationOnChangeEvent(
            page,
            pageSize,
            tablePaginationOnChangeEventDispatchType,
            tablePaginationOnChangeEventCondition
          )
        }
        tableRowSelection={{
          columnWidth: 30,
          columnTitle: '<>',
          selectedRowKeys,
          onSelect: (record, selected, selectedRows) => {
            if (selected) {
              this.setState({ tableRowSelectionEntity: record });
            } else if (selectedRows.length === 0) {
              this.setState({ tableRowSelectionEntity: null });
            }
          },
          onChange: selectedRowKeysArr => {
            this.setState({
              selectedRowKeys:
                selectedRowKeysArr.length > 1 ? [selectedRowKeysArr[1]] : selectedRowKeysArr,
            });
          }, // Table勾选回调事件
        }}
        modalVisible={modalVisible}
        modalTitle={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.productType' })}
        modalContentCmp={
          <GoodsSpuModelUpkeep
            originGoodsSpuInfo={mue}
            modalUpkeepType={modalUpkeepType}
            modalUpkeepEntity={modalUpkeepEntity}
            modalVisibleOnChange={this.modalVisibleOnChange}
            modalOnChangePageCurrent={this.changeCurrentPage}
          />
        }
        modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
      />
    );
  };

  /**
   * 渲染Table组件-产品描述
   * @param {array} mue 产品描述资源数据
   * @returns 产品描述维护组件
   */
  renderGoodsSpuDescCmpTable = (mue, mut) => {
    const {
      goodsSpuDescList: gsdl, // 当前选择-产品描述(列表)
      goodsSpuDescListPageSize: gsdlps, // 当前选择-产品描述(列表)总数量
      parmPublicParameterOfGoodsDescTypeAll: pppogdta, // 描述类型数据(所有)
    } = this.props;

    const enclosureOnDeleteEventDispatchType = 'goodsspuinfomanage/deleteGoodsSpuDesc'; // 附件删除dispathc的type值
    const enclosureOnAddEventDispatchType = 'goodsspuinfomanage/addGoodsSpuDesc'; // 附件添加dispatch的type值
    const enclosureOnUpdateEventDispatchType = 'goodsspuinfomanage/updateGoodsSpuDesc'; // 附件更新dispatch的type值
    const enclosureTablePaginationOnChangeEventDispatchType =
      'goodsspuinfomanage/paginationGoodsSpuDesc'; // 产品描述列表分页dispatch的type值
    const enclosureTablePaginationOnChangeEventCondition = { spuCode: mue.spuCode }; // 附件列表分页附带的参数,是object类型

    return (
      <EnclosureTableBasic
        actionType={mut} // 附件列表-当前操作类型,来源于父级组件的定义值, 1为详情;2为修改;3为停用;10为新增;20为删除
        enclosureOperateSign="GoodsSpuDesc" // 附件当前操作标识
        enclosureTableTitle={`${formatMessage({ id: 'form.goodsSpuAttrSkuCmp.spu' })}：${
          mue.spuCode
        }+${mue.spuName}`} // 附件列表标题
        enclosureAssociatedObject={mue} // 附件关联的对象(当前操作父级对象)
        enclosureTableColumnsField={this.tableColumns_GoodsSpuDesc} // 附件列表字段定义
        enclosureTablePaginationOnChangeEventDispatchType={
          enclosureTablePaginationOnChangeEventDispatchType
        } // 附件列表分页对应dispatch的type
        enclosureTablePaginationOnChangeEventCondition={
          enclosureTablePaginationOnChangeEventCondition
        } // 附件列表分页附带的参数,是object类型
        enclosureTableDataSource={gsdl} // 附件列表源数据
        enclosureTableTotalSize={gsdlps} // 附件列表总数据量
        enclosureOnDeleteEventDispatchType={enclosureOnDeleteEventDispatchType} // 附件删除dispathc的type值
        enclosureOnAddEventDispatchType={enclosureOnAddEventDispatchType} // 附件添加dispatch的type值
        enclosureOnUpdateEventDispatchType={enclosureOnUpdateEventDispatchType} // 附件更新dispatch的type值
        enclosureDescTypeAll={pppogdta} // 附件描述类型数据所有
      />
    );
  };

  /**
   * 渲染Table组件-商品分组数据(需求:商品分组是在产品当中维护)
   */
  renderGoodsSkuGroupCmpTable = (mue, mut) => {
    const {
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableCurrentPage,
      selectedRowKeys,
    } = this.state;
    const { goodsSkuGroupList, goodsSkuGroupListPageSize } = this.props;
    const dispatchType = 'goodsspuinfomanage/updateGoodsSkuGroup'; // 停用dispatchType
    const tablePaginationOnChangeEventDispatchType = 'goodsspuinfomanage/paginationGoodsSkuGroup';
    const tablePaginationOnChangeEventCondition = { spuCode: mue.spuCode };
    return (
      <MdcManageBlock
        tableTitle={`${formatMessage({ id: 'form.goodsSpuAttrSkuCmp.spu' })}：${mue.spuCode}+${
          mue.spuName
        }`}
        tableColumns={this.tableColumns_GoodsSkuGroup}
        tableDataSource={goodsSkuGroupList}
        tableTotalSize={goodsSkuGroupListPageSize}
        tableOperateAreaCmp={this._tableOperateAreaCmp(5, mut, dispatchType)}
        tableCurrentPage={tableCurrentPage}
        tablePaginationOnChangeEvent={(page, pageSize) =>
          this.tablePaginationOnChangeEvent(
            page,
            pageSize,
            tablePaginationOnChangeEventDispatchType,
            tablePaginationOnChangeEventCondition
          )
        }
        tableRowSelection={{
          columnWidth: 30,
          columnTitle: '<>',
          selectedRowKeys,
          onSelect: (record, selected, selectedRows) => {
            if (selected) {
              this.setState({ tableRowSelectionEntity: record });
            } else if (selectedRows.length === 0) {
              this.setState({ tableRowSelectionEntity: null });
            }
          },
          onChange: selectedRowKeysArr => {
            this.setState({
              selectedRowKeys:
                selectedRowKeysArr.length > 1 ? [selectedRowKeysArr[1]] : selectedRowKeysArr,
            });
          }, // Table勾选回调事件
        }}
        modalVisible={modalVisible}
        modalTitle={formatMessage({ id: 'menu.goodsmanage.goodsspuinfomanage.productGroup' })}
        modalContentCmp={
          <GoodsSkuGroupUpkeep
            originGoodsSpuInfo={mue}
            modalUpkeepType={modalUpkeepType}
            modalUpkeepEntity={modalUpkeepEntity}
            modalVisibleOnChange={this.modalVisibleOnChange}
            modalOnChangePageCurrent={this.changeCurrentPage}
          />
        }
        modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
      />
    );
  };

  render() {
    const {
      upkeepBasicFlag,
      modalUpkeepType: mut, // 当前产品操作类型
      modalUpkeepEntity: mue, // 当前产品源数据
    } = this.props;

    let upkeepBasicCmp;
    switch (upkeepBasicFlag) {
      case 1: // 产品普通属性-维护组件
        upkeepBasicCmp = this.renderGoodsSpuAttrNormalCmpTable(mue, mut);
        break;
      case 2: // 产品SKU属性-维护组件
        upkeepBasicCmp = <GoodsSpuAttrSkuCmp {...this.props} />;
        break;
      case 3: // 产品型号定义-维护组件
        upkeepBasicCmp = this.renderGoodsSpuModelCmpTable(mue, mut);
        break;
      case 4: // 产品描述-维护组件
        upkeepBasicCmp = this.renderGoodsSpuDescCmpTable(mue, mut);
        break;
      case 5: // 商品分组管理-维护组件
        upkeepBasicCmp = this.renderGoodsSkuGroupCmpTable(mue, mut);
        break;
      default:
        upkeepBasicCmp = null;
        break;
    }
    return upkeepBasicCmp;
  }

  /**
   * Table-表格头部动作处理组件
   * @param {number} tableTypeFlag 当前table的标识码,
   * 1为产品普通属性-表格标识;
   * 2为产品SKU属性-表格标识;
   * 3为产品SKU属性项值-表格标识;
   * 4为产品型号定义-表格标识;
   * 5为商品分组-表格标识
   * @param {number} actionType 当前产品-动作类型, 1为详情;2为维护;3为停用/启用;10为新增;20为删除
   * @param {string} dispatchType 停用/启用-动作对应dispatch的type值
   */
  _tableOperateAreaCmp = (tableTypeFlag, actionType, dispatchType = null) => {
    const { tableRowSelectionEntity: trse } = this.state; // 当前table被选中1行实体对象

    // 获取当前table状态按钮文字
    const getCurrentStatus = () => {
      if (tableTypeFlag === 1 && trse && !Number.isNaN(trse.attrEnable)) {
        // 产品普通属性-状态
        return trse.attrEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 2 && trse && !Number.isNaN(trse.skuAttrNameEnable)) {
        // 产品SKU属性-状态
        return trse.skuAttrNameEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 3 && trse && !Number.isNaN(trse.skuAttrItemEnable)) {
        // 产品SKU属性项值-状态
        return trse.skuAttrItemEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 4 && trse && !Number.isNaN(trse.modelEnable)) {
        // 产品型号定义-状态
        return trse.modelEnable === 1 ? status[0] : status[1];
      }
      if (tableTypeFlag === 5 && trse && !Number.isNaN(trse.groupEnable)) {
        // 商品分组-状态
        return trse.groupEnable === 1 ? status[0] : status[1];
      }
      return formatMessage({ id: 'form.common.enabledAndDisabled' });
    };

    // 校验Table勾选tableRowSelectionEntity值是否存在
    const checkTableRowSelectionEntity = (flag = -1) => {
      if (trse === null || trse === undefined) {
        const tips =
          flag === 2
            ? formatMessage({ id: 'button.common.modify' })
            : flag === 20
            ? formatMessage({ id: 'button.common.delete' })
            : formatMessage({ id: 'button.common.handle' });
        message.warn(
          `${formatMessage({ id: 'form.goodsSpuInfoUpkeepBasic.pleaseSelectData' })}[${tips}]`
        );
        return;
      }
      if (flag === 1) {
        // 详情动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 2) {
        // 维护动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 20) {
        // 删除动作
        this._deleteTableRowSelection(tableTypeFlag, trse);
      }
      if (flag === 3) {
        // 停用动作
        // 设置值为0,表示停用
        Object.keys(trse)
          .filter(item => ['attrEnable', 'modelEnable', 'groupEnable'].includes(item))
          .forEach(item => {
            trse[item] = trse[item] === 0 ? 1 : 0;
          });
        this.onClickActionExecuteEvent(flag, trse, null, dispatchType);
      }
    };

    return actionType === 1 ? (
      <a type="primary" onClick={() => checkTableRowSelectionEntity(1)}>
        {formatMessage({ id: 'button.common.details' })}
      </a>
    ) : (
      <div>
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(1);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.details' })}
        </a>
        <Divider type="vertical" />
        <a type="primary" onClick={() => this.onClickActionExecuteEvent(10)}>
          {formatMessage({ id: 'button.common.add' })}
        </a>
        <Divider type="vertical" />
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(2);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.modify' })}
        </a>
        <Divider type="vertical" />
        <a type="primary" onClick={() => checkTableRowSelectionEntity(3)}>
          {getCurrentStatus()}
        </a>
        {/* <a type="primary" onClick={() => checkTableRowSelectionEntity(20)}>删除</a> */}
      </div>
    );
  };

  /**
   * Table-删除行勾选数据
   * @param {number} tableTypeFlag 当前table的标识码,
   * 1为产品普通属性-表格标识;
   * 2为产品SKU属性-表格标识;
   * 3为产品SKU属性项值-表格标识;
   * 4为产品型号定义-表格标识;
   * 5为商品分组-表格标识
   * @param {object} item 当前table选中的行数据对象
   */
  _deleteTableRowSelection = (tableTypeFlag, item) => {
    const { dispatch } = this.props;
    switch (tableTypeFlag) {
      case 1: // 删除-产品普通属性
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuAttrNormal', payload: { id: item.id } });
        break;
      case 2: // 删除-产品SKU属性
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuAttrSku', payload: { id: item.id } });
        break;
      case 3: // 删除-产品SKU属性项值
        dispatch({
          type: 'goodsspuinfomanage/deleteGoodsSpuAttrSkuItem',
          payload: { id: item.id },
        });
        break;
      case 4: // 删除-产品型号定义
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuModel', payload: { id: item.id } });
        break;
      case 6: // 删除-商品分组
        dispatch({ type: 'goodsspuinfomanage/deleteGoodsSkuGroup', payload: { id: item.id } });
        break;
      default:
        break;
    }
  };

  // /**
  //  * List-删除行勾选数据
  //  * @param {number} listType listType为列表类型,值仅为: 1为图片; 2为文件; 3为视频;
  //  * @param {object} item 处理数据对象
  //  */
  // _deleteListRowSelection = (listType, item) => {
  //   const { dispatch } = this.props;
  //   dispatch({ type: 'goodsspuinfomanage/deleteGoodsSpuDesc', payload: item });
  // };
}

export default connect(({ goodsspuinfomanage, parmpublicparametermanage }) => ({
  goodsSpuAttrNormalList: goodsspuinfomanage.goodsSpuAttrNormalList || [], // 当前产品-产品普通属性(列表/分页)
  goodsSpuAttrNormalListPageSize: goodsspuinfomanage.goodsSpuAttrNormalListPageSize || 0, // 当前选择-产品普通属性数据总数量
  goodsSpuModelList: goodsspuinfomanage.goodsSpuModelList || [], // 当前选择-产品型号定义(列表/分页)
  goodsSpuModelListPageSize: goodsspuinfomanage.goodsSpuModelListPageSize || 0, // 当前选择-产品型号定义数据总数量
  goodsSpuDescList: goodsspuinfomanage.goodsSpuDescList || [], // 当前选择-产品描述(列表/分页)
  goodsSpuDescListPageSize: goodsspuinfomanage.goodsSpuDescListPageSize || 0, // 当前选择-产品描述数据总数量
  goodsSkuGroupList: goodsspuinfomanage.goodsSkuGroupList || [], // 当前选择-商品分组(列表/分页)
  goodsSkuGroupListPageSize: goodsspuinfomanage.goodsSkuGroupListPageSize || 0, // 当前选择-商品分组数据总数量
  parmPublicParameterOfGoodsDescTypeAll:
    parmpublicparametermanage.parmPublicParameterOfGoodsDescTypeAll || [], // 产品描述类型数据(所有)
}))(GoodsSpuInfoUpkeepBasic);

// /**
//    * 渲染Table组件-产品SKU属性
//    */
//   renderGoodsSpuAttrSkuCmpTable = (mue, mut) => {
//     const { modalVisible, modalUpkeepType, modalUpkeepEntity } = this.state;
//     const { goodsSpuAttrSkuList, goodsSpuAttrSkuListPageSize } = this.props;
//     const dispatchType = 'goodsspuinfomanage/updateGoodsSpuAttrSku'; // 停用dispatchType
//     const tablePaginationOnChangeEventDispatchType = 'goodsspuinfomanage/paginationGoodsSpuAttrSku';
//     const tablePaginationOnChangeEventCondition = {spuCode:mue.spuCode};
//     return (
//       <MdcManageBlock
//         tableTitle={`产品：${mue.spuCode}+${mue.spuName}`}
//         tableColumns={this.tableColumns_GoodsSpuAttrSk}
//         tableDataSource={goodsSpuAttrSkuList}
//         tableTotalSize={goodsSpuAttrSkuListPageSize}
//         tableOperateAreaCmp={this._tableOperateAreaCmp(2, mut, dispatchType)}
//         tablePaginationOnChangeEvent={(page, pageSize) =>
//           this.tablePaginationOnChangeEvent(
//             page,
//             pageSize,
//             tablePaginationOnChangeEventDispatchType,
//             tablePaginationOnChangeEventCondition
//           )
//         }
//         tableRowSelection={this.tableRowSelection}
//         modalVisible={modalVisible}
//         modalTitle="产品SKU属性"
//         modalContentCmp={
//           <GoodsSpuAttrSkuUpkeep
//             originGoodsSpuInfo={mue}
//             modalUpkeepType={modalUpkeepType}
//             modalUpkeepEntity={modalUpkeepEntity}
//             modalVisibleOnChange={this.modalVisibleOnChange}
//           />
//         }
//         modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
//       />
//     );
//   };

//   /**
//    * 渲染Table组件-产品SKU属性项值
//    */
//   renderGoodsSpuAttrSkuItemCmpTable = (mue, mut) => {
//     const {
//       modalVisible,
//       modalUpkeepType,
//       modalUpkeepEntity,
//       tableRowSelectionEntity: trse, // 当前table选中行数据对象
//     } = this.state;
//     const { goodsSpuAttrSkuItemList, goodsSpuAttrSkuItemListPageSize } = this.props;
//     const dispatchType = 'goodsspuinfomanage/updateGoodsSpuAttrSkuItem'; // 停用dispatchType
//     const tablePaginationOnChangeEventDispatchType = 'goodsspuinfomanage/paginationGoodsSpuAttrSkuItem';
//     const tablePaginationOnChangeEventCondition = {spuCode:mue.spuCode};
//     if(!trse) return null;

//     return (
//       <MdcManageBlock
//         tableTitle={`SKU属性：${trse.skuAttrCode}+${trse.skuAttrName}`}
//         tableColumns={this.tableColumns_GoodsSpuAttrSkuItem}
//         tableDataSource={goodsSpuAttrSkuItemList}
//         tableTotalSize={goodsSpuAttrSkuItemListPageSize}
//         tableOperateAreaCmp={this._tableOperateAreaCmp(3, mut, dispatchType)}
//         tablePaginationOnChangeEvent={(page, pageSize) =>
//           this.tablePaginationOnChangeEvent(
//             page,
//             pageSize,
//             tablePaginationOnChangeEventDispatchType,
//             tablePaginationOnChangeEventCondition
//           )
//         }
//         tableRowSelection={this.tableRowSelection}
//         modalVisible={modalVisible}
//         modalTitle="产品SKU属性项值"
//         modalContentCmp={
//           <GoodsSpuAttrSkuItemUpkeep
//             originGoodsSpuInfo={mue}
//             modalUpkeepType={modalUpkeepType}
//             modalUpkeepEntity={modalUpkeepEntity}
//             modalVisibleOnChange={this.modalVisibleOnChange}
//           />
//         }
//         modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
//       />
//     );
//   };
