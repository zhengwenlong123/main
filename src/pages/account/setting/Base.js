import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Button, Input, Form, message, Card } from 'antd';
import { formatMessage } from 'umi/locale';
import styles from './index.less';

const formItemLayout = {
  labelCol: {
    xs: { span: 3, offset: 3 },
    sm: { span: 3, offset: 3 },
  },
  wrapperCol: {
    xs: { span: 16 },
    sm: { span: 8 },
  },
};

const formTailLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 8, offset: 6 },
};

@connect(({ loading }) => ({
  saveLoading: loading.effects['settingbase/updateAccountPassword'],
}))
@Form.create()
class setting extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      confirmDirty: false,
    };
  }

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    if (result.success) {
      message.info(formatMessage({ id: 'app.setting.saveSuccess' }));
      const { history } = this.props;
      history.push('/');
    } else {
      message.info(result.message);
    }
  };

  /**
   * 修改密码请求
   */

  handleSubmit = e => {
    if (e) e.preventDefault();
    const {
      dispatch,
      form: { validateFieldsAndScroll },
    } = this.props;
    validateFieldsAndScroll((err, values) => {
      if (err) return;
      dispatch({
        type: 'settingbase/updateAccountPassword',
        payload: values,
        callback: this.onCallback,
      });
    });
  };

  /**
   * 密码表单校验
   */
  handleConfirmBlur = e => {
    const { value } = e.target;
    const { confirmDirty } = this.state;
    this.setState({ confirmDirty: confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    const newPassword = form.getFieldValue('newPassword');
    if (value && value !== newPassword) {
      callback(formatMessage({ id: 'app.setting.pwdNotEqual' }));
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { confirmDirty } = this.state;
    const { form } = this.props;
    if (value && confirmDirty) {
      form.validateFields(['cofirm'], { force: true });
    }
    callback();
  };

  render() {
    const {
      form: { getFieldDecorator },
      saveLoading,
    } = this.props;
    return (
      <Card>
        <div className={styles.setting} onSubmit={this.handleSubmit}>
          <div className={styles.title}>{formatMessage({ id: 'app.setting.personalSetting' })}</div>
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'app.setting.oldPwd' })}>
              {getFieldDecorator('oldPassword', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.setting.pleaseInputOldPwd' }),
                  },
                ],
              })(<Input.Password style={{ width: 300 }} autoComplete="current-password" />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'app.setting.newPwd' })}>
              {getFieldDecorator('newPassword', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.setting.pleaseInputNewPwd' }),
                  },
                  {
                    validator: this.validateToNextPassword,
                  },
                ],
              })(<Input.Password style={{ width: 300 }} autoComplete="new-password" />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'app.setting.confirmPwd' })}>
              {getFieldDecorator('confirm', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.setting.pleaseInputConfirmPwd' }),
                  },
                  {
                    validator: this.compareToFirstPassword,
                  },
                ],
              })(
                <Input.Password
                  style={{ width: 300 }}
                  onBlur={this.handleConfirmBlur}
                  autoComplete="new-password"
                />
              )}
            </Form.Item>
            <Form.Item {...formTailLayout}>
              <Button type="primary" htmlType="submit" loading={saveLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </Form.Item>
          </Form>
        </div>
      </Card>
    );
  }
}

export default connect()(Form.create({})(setting));
