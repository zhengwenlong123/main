/**
 * [账户设置组件] model层
 */

import api from '@/services/index';

const { UpdateAccountPassword } = api;

export default {
  namespace: 'settingbase',
  state: {},
  reducers: {},
  effects: {
    // 更新-账户密码
    *updateAccountPassword({ payload, callback }, { call }) {
      const res = yield call(UpdateAccountPassword, payload || {});
      callback(res !== undefined && res !== null && res.code === 0, res);
    },
  },
};
