import React, { Component } from 'react';
import { connect } from 'dva';
import { Spin, notification } from 'antd';
import { formatMessage } from 'umi/locale';
import router from 'umi/router';
import { getPageQuery } from '@/utils/utils';
import { loginUrl } from '@/defaultSettings';

/**
 * 跳转登入页,这里是所有系统跳转登入后进入的第一个页面
 */
@connect(({ global, loading }) => ({
  global,
  submitting: loading.effects['global/jumplogin'],
}))
class JumpLogin extends Component {
  // 载入完成
  componentDidMount() {
    const { dispatch } = this.props;
    const { token } = getPageQuery();
    if (!token || token === '') {
      notification.error({
        message: formatMessage({ id: 'app.login.fail' }),
        description: formatMessage({ id: 'app.login.needtoken' }),
      });

      if (loginUrl.startsWith('http')) window.location = loginUrl;
      else router.push({ pathname: loginUrl });
      return;
    }
    dispatch({ type: 'global/jumplogin', payload: { token } });
  }

  render() {
    return (
      <div style={{ paddingTop: 100, textAlign: 'center' }}>
        <Spin size="large" />
      </div>
    );
  }
}

export default JumpLogin;
