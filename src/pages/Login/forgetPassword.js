/* eslint-disable prefer-const */
/* eslint-disable no-plusplus */
import React from 'react';
import { connect } from 'dva';
import { Icon, Button, Input, Form, Row, Col, message } from 'antd';
import DocumentTitle from 'react-document-title';
import { formatMessage } from 'umi/locale';
import router from 'umi/router';
import { systemName, loginUrl } from '@/defaultSettings';

import leftimg from '@/assets/image/loginleft.png';

import styles from './Login.less';

class forgetPassword extends React.Component {
  constructor() {
    super();
    this.state = {
      verificationStatus: false, // 验证码状态---是否重发
      countdown: 60, // 验证码再次发送倒计时
      countdownStr: formatMessage({ id: 'app.register.resendVerificationCode' }), // 验证码再次发送倒计时
      requestId: '',
      verificationCode: false, // 是否验证过短信验证码
      confirmDirty: false, // 校验两次密码状态
    };
  }

  /**
   * 提交验证验证码并进入下一步
   */
  handleSubmit = e => {
    if (e) e.preventDefault();
    const { requestId, verificationCode } = this.state;
    const {
      dispatch,
      form: { validateFieldsAndScroll, getFieldValue },
    } = this.props;
    const that = this;
    if (!verificationCode) {
      validateFieldsAndScroll(['mobile', 'code'], error => {
        if (error) return;
        // 验证验证码
        dispatch({
          type: 'global/verificationPhoneCode',
          payload: {
            requestId,
            vCode: getFieldValue('code'),
          },
          callback(data) {
            if (data.success === true || data.code === 200) {
              that.setState({ verificationCode: true });
            } else if (data.message) {
              message.error(data.message);
            }
          },
        });
      });
    } else {
      validateFieldsAndScroll(['password', 'confirm'], error => {
        if (error) return;
        // 提交新密码
        dispatch({
          type: 'global/modifyPassword',
          payload: {
            requestId,
            newPassword: getFieldValue('password'),
          },
          callback(data) {
            if (data.success === true || data.code === 200) {
              message.success(data.message);
              if (loginUrl.startsWith('http')) window.location = loginUrl;
              else router.push({ pathname: loginUrl });
            } else if (data.message) {
              message.error(data.message);
            }
          },
        });
      });
    }
  };

  /**
   * 验证手机号校验规则
   */
  validatePhone = (rule, value, callback) => {
    const phoneReg = /^[0-9]{11}$/;
    if (value && !phoneReg.test(value)) {
      callback(formatMessage({ id: 'app.login.pleaseInputCorrectPhone' }));
    } else {
      callback();
    }
  };

  /**
   * 获取验证码
   */
  getCode = () => {
    const { verificationStatus, requestId, countdown } = this.state;
    const {
      dispatch,
      form: { validateFields },
    } = this.props;
    const that = this;
    validateFields(['mobile'], (error, values) => {
      if (error) return;
      if (!verificationStatus) {
        that.codeTimer();
        that.setState({
          countdown: countdown - 1,
          countdownStr: `${countdown - 1}${formatMessage({
            id: 'app.login.secondReceiveCaptcha',
          })}`,
        });
        dispatch({
          type: 'global/getPhoneCode',
          payload: values,
          callback(data) {
            if (data.success || data.code === 200) {
              message.success(data.message);
              that.setState({
                verificationStatus: true,
                requestId: data.data.id,
              });
            } else {
              // message.error(data.message);
            }
          },
        });
      } else {
        that.codeTimer();
        dispatch({
          type: 'global/getPhoneCodeAgain',
          payload: { requestId },
          callback(data) {
            if (data.success === true || data.code === 200) {
              message.success(data.message);
            } else {
              // message.error(data.message);
            }
          },
        });
      }
    });
  };

  /**
   * 验证码倒计时
   */
  codeTimer = () => {
    const that = this;
    let { countdown } = this.state;
    let timer = setInterval(() => {
      that.setState({
        countdown: --countdown,
        countdownStr: `${countdown}${formatMessage({ id: 'app.login.secondReceiveCaptcha' })}`,
      });
      if (countdown === 0) {
        that.setState({
          countdown: 60,
          countdownStr: formatMessage({ id: 'app.register.resendVerificationCode' }),
        });
        clearInterval(timer);
      }
    }, 1000);
  };

  /**
   * 第一步Dom
   */
  stepOneDom = () => {
    const { countdown, countdownStr } = this.state;
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <div>
        <Form.Item>
          {getFieldDecorator('mobile', {
            rules: [
              {
                required: true,
                message: formatMessage({ id: 'app.login.pleaseInputPhone' }),
              },
              {
                validator: this.validatePhone,
              },
            ],
            initialValue: '',
          })(
            <Input
              addonBefore={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder={formatMessage({ id: 'app.login.pleaseInputPhone' })}
              size="large"
              autoComplete="mobile"
            />
          )}
        </Form.Item>
        <Form.Item>
          <Row>
            <Col span={14}>
              {getFieldDecorator('code', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.login.pleaseInputCaptcha' }),
                  },
                ],
                initialValue: '',
              })(
                <Input
                  addonBefore={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  type="code"
                  size="large"
                  placeholder={formatMessage({ id: 'app.login.pleaseInputCaptcha' })}
                  autoComplete="code"
                />
              )}
            </Col>
            <Col span={9} offset={1}>
              <Button size="large" disabled={countdown < 60} onClick={this.getCode}>
                {!(countdown < 60)
                  ? formatMessage({ id: 'app.register.get-verification-code' })
                  : countdownStr}
              </Button>
            </Col>
          </Row>
        </Form.Item>
        <Button type="primary" htmlType="submit" style={{ width: '100%' }} size="large">
          {formatMessage({ id: 'app.login.next' })}
        </Button>
      </div>
    );
  };

  /**
   * 校验两次密码
   */
  handleConfirmBlur = e => {
    const { value } = e.target;
    const { confirmDirty } = this.state;
    this.setState({ confirmDirty: confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    const password = form.getFieldValue('password');
    if (value && value !== password) {
      callback(formatMessage({ id: 'app.login.passwordNotEqual' }));
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { confirmDirty } = this.state;
    const { form } = this.props;
    if (value && confirmDirty) {
      form.validateFields(['cofirm'], { force: true });
    }
    callback();
  };

  /**
   * 第二步Dom
   */

  stepTwoDom = () => {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <div>
        <Form.Item>
          {getFieldDecorator('password', {
            rules: [
              {
                required: true,
                message: formatMessage({ id: 'app.login.passwordLessThan6' }),
                min: 6,
              },
              {
                validator: this.validateToNextPassword,
              },
            ],
            initialValue: '',
          })(
            <Input.Password
              addonBefore={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder={formatMessage({ id: 'app.login.passwordLessThan6' })}
              size="large"
              autoComplete="password"
            />
          )}
        </Form.Item>
        <Form.Item>
          {getFieldDecorator('confirm', {
            rules: [
              {
                required: true,
                message: formatMessage({ id: 'app.login.pleaseInputPassworAgain' }),
              },
              {
                validator: this.compareToFirstPassword,
              },
            ],
            initialValue: '',
          })(
            <Input.Password
              addonBefore={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              size="large"
              placeholder={formatMessage({ id: 'app.login.pleaseInputPassworAgain' })}
              autoComplete="confirm"
              onBlur={this.handleConfirmBlur}
            />
          )}
        </Form.Item>
        <Button type="primary" htmlType="submit" style={{ width: '100%' }} size="large">
          {formatMessage({ id: 'button.common.submit' })}
        </Button>
      </div>
    );
  };

  render() {
    const { verificationCode } = this.state;
    const stepOne = this.stepOneDom();
    const stepTwo = this.stepTwoDom();
    return (
      <>
        <DocumentTitle title={systemName}>
          <div className={styles.rootDiv}>
            <div className={styles.leftSide}>
              <img src={leftimg} alt="leftimg" className={styles.leftImg} />
            </div>
            <div className={styles.rightSide}>
              <div className={styles.loginDiv}>
                <div className={styles.loginDivContent}>
                  <div className={styles.logoDiv}>
                    <h1>
                      {verificationCode === false
                        ? formatMessage({ id: 'form.login.verification' })
                        : formatMessage({ id: 'app.login.resetPassword' })}
                    </h1>
                  </div>
                  <Form onSubmit={this.handleSubmit}>
                    {verificationCode === false ? stepOne : stepTwo}
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </DocumentTitle>
      </>
    );
  }
}

export default connect()(Form.create({})(forgetPassword));
