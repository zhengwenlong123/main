/* eslint-disable prefer-const */
/* eslint-disable no-plusplus */
import React from 'react';
import { connect } from 'dva';
import { Icon, Button, Input, Form, message } from 'antd';
import DocumentTitle from 'react-document-title';
import { formatMessage } from 'umi/locale';
import { systemName } from '@/defaultSettings';

import leftimg from '@/assets/image/loginleft.png';

import styles from './Login.less';

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 18 },
  },
};

const formTailLayout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 8, offset: 12 },
};

@connect(({ loading }) => ({
  saveLoading: loading.effects['settingbase/updateAccountPassword'],
}))
@Form.create()
class changePassword extends React.Component {
  constructor() {
    super();
    this.state = {
      confirmDirty: false,
    };
  }

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    if (result.success) {
      message.info(formatMessage({ id: 'app.setting.saveSuccess' }));
      const { dispatch } = this.props;
      dispatch({
        type: 'global/logout',
      });
    } else {
      message.info(result.message);
    }
  };

  /**
   * 提交新密码
   */
  handleSubmit = e => {
    if (e) e.preventDefault();
    const {
      dispatch,
      form: { validateFieldsAndScroll },
    } = this.props;
    validateFieldsAndScroll((err, values) => {
      if (err) return;
      dispatch({
        type: 'global/updateAccountPassword',
        payload: values,
        callback: this.onCallback,
      });
    });
  };

  /**
   * 密码表单校验
   */
  handleConfirmBlur = e => {
    const { value } = e.target;
    const { confirmDirty } = this.state;
    this.setState({ confirmDirty: confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    const newPassword = form.getFieldValue('newPassword');
    if (value && value !== newPassword) {
      callback(formatMessage({ id: 'app.setting.pwdNotEqual' }));
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { confirmDirty } = this.state;
    const { form } = this.props;
    if (value && confirmDirty) {
      form.validateFields(['cofirm'], { force: true });
    }
    callback();
  };

  render() {
    const {
      form: { getFieldDecorator },
      saveLoading,
    } = this.props;
    return (
      <>
        <DocumentTitle title={systemName}>
          <div className={styles.rootDiv}>
            <div className={styles.leftSide}>
              <img src={leftimg} alt="leftimg" className={styles.leftImg} />
            </div>
            <div className={styles.rightSide}>
              <div className={styles.loginDiv}>
                <div className={styles.loginDivContent} style={{ width: 400 }}>
                  <div className={styles.logoDiv}>
                    <h1>{formatMessage({ id: 'app.login.resetPassword' })}</h1>
                  </div>
                  <Form onSubmit={this.handleSubmit}>
                    <Form.Item
                      label={formatMessage({ id: 'app.setting.oldPwd' })}
                      {...formItemLayout}
                    >
                      {getFieldDecorator('oldPassword', {
                        rules: [
                          {
                            required: true,
                            message: formatMessage({ id: 'app.setting.pleaseInputOldPwd' }),
                          },
                        ],
                      })(
                        <Input.Password
                          addonBefore={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                          autoComplete="current-password"
                        />
                      )}
                    </Form.Item>
                    <Form.Item
                      label={formatMessage({ id: 'app.setting.newPwd' })}
                      {...formItemLayout}
                    >
                      {getFieldDecorator('newPassword', {
                        rules: [
                          {
                            required: true,
                            message: formatMessage({ id: 'app.setting.pleaseInputNewPwd' }),
                          },
                          {
                            validator: this.validateToNextPassword,
                          },
                        ],
                      })(
                        <Input.Password
                          addonBefore={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                          autoComplete="new-password"
                        />
                      )}
                    </Form.Item>
                    <Form.Item
                      label={formatMessage({ id: 'app.setting.confirmPwd' })}
                      {...formItemLayout}
                    >
                      {getFieldDecorator('confirm', {
                        rules: [
                          {
                            required: true,
                            message: formatMessage({ id: 'app.setting.pleaseInputConfirmPwd' }),
                          },
                          {
                            validator: this.compareToFirstPassword,
                          },
                        ],
                      })(
                        <Input.Password
                          addonBefore={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                          onBlur={this.handleConfirmBlur}
                          autoComplete="new-password"
                        />
                      )}
                    </Form.Item>
                    <Form.Item {...formTailLayout}>
                      <Button type="primary" htmlType="submit" loading={saveLoading}>
                        {formatMessage({ id: 'button.common.submit' })}
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </DocumentTitle>
      </>
    );
  }
}

export default connect()(Form.create({})(changePassword));
