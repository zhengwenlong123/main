/* eslint-disable */
import React, { Component } from 'react';
import { connect } from 'dva';
import { formatMessage, FormattedMessage } from 'umi/locale';
import Link from 'umi/link';
import { Icon, Form, Input, Button, Checkbox } from 'antd';
import DocumentTitle from 'react-document-title';
import { systemName, useAliCaptcha, disableSubLogin, loginUrl } from '@/defaultSettings';

import leftimg from '@/assets/image/loginleft.png';
import fullnamelogo from '@/assets/fullnamelogo.png';

import styles from './Login.less';

@connect(({ global, loading }) => ({
  global,
  submitting: loading.effects['global/login'],
}))
@Form.create()
class LoginPage extends Component {
  constructor(props) {
    super(props);
    if (disableSubLogin && loginUrl !== '/login') window.location = loginUrl;

    this.state = {
      token: '', // 阿里云验证码获取的token
      sessionId: '', // 阿里云验证码获得的sessionId
      sig: '', // 阿里云验证码获取的sig
      canSubmit: !useAliCaptcha, // 提交按钮是否可用
    };
    this.sc = React.createRef();
  }

  renderSmartCaptcha() {
    if (useAliCaptcha) {
      const ic = new smartCaptcha({
        renderTo: '#sc',
        width: 200,
        height: 40,
        default_txt: '点击按钮开始智能验证',
        success_txt: '验证成功',
        fail_txt: '验证失败，请在此点击按钮刷新',
        scaning_txt: '智能检测中',
        success: this.handleScCheck,
      });
      ic.init();
    }
  }

  componentDidMount() {
    this.renderSmartCaptcha();
  }

  componentDidUpdate(prevProps) {
    if (useAliCaptcha) {
      if (prevProps.location.key !== this.props.location.key) {
        const currentNode = this.sc.current;
        while (currentNode.hasChildNodes()) {
          currentNode.removeChild(currentNode.firstChild);
        }
        this.renderSmartCaptcha();
        this.setState({ canSubmit: !useAliCaptcha });
      }
    }
  }

  handleScCheck = data => {
    // console.log(NVC_Opt.token);
    // console.log(data.sessionId);
    // console.log(data.sig);
    this.setState({
      token: NVC_Opt.token,
      sessionId: data.sessionId,
      sig: data.sig,
      canSubmit: true,
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    const {
      form: { validateFieldsAndScroll },
    } = this.props;
    const { token, sessionId, sig, canSubmit } = this.state;
    // 如果不能提交就不提交
    if (!canSubmit) return;
    /* eslint-disable */
    validateFieldsAndScroll((error, values) => {
      if (error) return;
      values = { ...values, ...{ token, sessionId, sig } };
      // console.log(values);
      const { dispatch } = this.props;
      dispatch({
        type: 'global/login',
        payload: {
          ...values,
        },
      });
    });
  };

  render() {
    const {
      submitting,
      form: { getFieldDecorator },
    } = this.props;
    const { canSubmit = false } = this.state;
    return (
      <>
        <DocumentTitle title={systemName}>
          <div className={styles.rootDiv}>
            <div className={styles.leftSide}>
              <img src={leftimg} alt="leftimg" className={styles.leftImg} />
            </div>
            <div className={styles.rightSide}>
              <div className={styles.loginDiv}>
                <div className={styles.loginDivContent}>
                  <div className={styles.logoDiv}>
                    <img src={fullnamelogo} alt="logo" />
                  </div>
                  <Form onSubmit={this.handleSubmit}>
                    <Form.Item>
                      {getFieldDecorator('username', {
                        rules: [
                          {
                            required: true,
                            message: formatMessage({ id: 'validation.userName.required' }),
                          },
                        ],
                      })(
                        <Input
                          addonBefore={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                          placeholder={`${formatMessage({ id: 'app.login.userName' })}`}
                          size="large"
                          autoComplete="username"
                        />
                      )}
                    </Form.Item>
                    <Form.Item>
                      {getFieldDecorator('password', {
                        rules: [
                          {
                            required: true,
                            message: formatMessage({ id: 'validation.password.required' }),
                          },
                        ],
                      })(
                        <Input
                          addonBefore={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                          type="password"
                          size="large"
                          placeholder={`${formatMessage({ id: 'app.login.password' })}`}
                          autoComplete="password"
                        />
                      )}
                    </Form.Item>

                    {useAliCaptcha && (
                      <Form.Item>
                        <div id="sc" className={styles.sc} ref={this.sc} />
                      </Form.Item>
                    )}

                    <Form.Item>
                      {getFieldDecorator('remember', {
                        valuePropName: 'checked',
                        initialValue: true,
                      })(
                        <Checkbox size="large">
                          {formatMessage({ id: 'app.login.keeplogin' })}
                        </Checkbox>
                      )}
                      <Link style={{ float: 'right' }} to="/forgetPassword">
                        {formatMessage({ id: 'app.login.forgot-password' })}?
                      </Link>
                    </Form.Item>

                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{ width: '100%' }}
                      size="large"
                      loading={submitting}
                      disabled={!canSubmit}
                    >
                      <FormattedMessage id="app.login.login" />
                    </Button>
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </DocumentTitle>
      </>
    );
  }
}

export default LoginPage;
