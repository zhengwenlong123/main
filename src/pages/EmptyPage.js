import React, { PureComponent } from 'react';
import { formatMessage } from 'umi/locale';
import Link from 'umi/link';
import { Exception } from 'ant-design-pro';

class EmptyPage extends PureComponent {
  state = {};

  // 载入完成
  componentDidMount() {}

  render() {
    return (
      <Exception
        title={formatMessage({ id: 'app.exception.working' })}
        desc={formatMessage({ id: 'app.exception.workingdesc' })}
        linkElement={Link}
        backText={formatMessage({ id: 'app.exception.back' })}
      />
    );
  }
}

export default EmptyPage;
