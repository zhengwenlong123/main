import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Avatar, Row, Col } from 'antd';

import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './Welcome.less';

import XiQuotations from './components/XiQuotations';
import TodoList from './components/TodoList';
// import QuickLinks from './components/QuickLinks';
import TimeClock from './components/TimeClock';

@connect(({ user }) => ({
  currentUser: user.currentUser,
}))
class Workplace extends PureComponent {
  render() {
    const { currentUser, currentUserLoading } = this.props;

    const pageHeaderContent =
      currentUser && Object.keys(currentUser).length ? (
        <div className={styles.pageHeaderContent}>
          <div className={styles.avatar}>
            <Avatar size="large" src={currentUser.avatar} />
          </div>
          <div className={styles.content}>
            <div className={styles.contentTitle}>
              你好，
              {currentUser.name}
              ，祝你开心每一天！
            </div>
          </div>
        </div>
      ) : null;

    return (
      <PageHeaderWrapper
        loading={currentUserLoading}
        content={pageHeaderContent}
        extraContent={<TimeClock />}
      >
        <Row gutter={24}>
          <Col xl={16} lg={24} md={24} sm={24} xs={24}>
            <TodoList />
          </Col>
          <Col xl={8} lg={24} md={24} sm={24} xs={24}>
            <XiQuotations />
          </Col>
        </Row>
      </PageHeaderWrapper>
    );
  }
}

export default Workplace;
