import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Card, List, Icon, Modal, Form, DatePicker, Input, Typography } from 'antd';
import moment from 'moment';
import TableRender from '@/utils/tableRenders';

const localStroagePrefix = 'todolist_';

// 临时项目
@connect(({ user }) => ({
  currentUserId: user.currentUser.id,
}))
@Form.create()
class TodoList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      addModalVisible: false,
      list: [],
    };
  }

  componentDidMount() {
    this.setState({ list: this.getLocalStorage() });
  }

  getLocalStorage = () => {
    const { currentUserId } = this.props;
    if (JSON.parse(localStorage.getItem(localStroagePrefix + currentUserId)))
      return JSON.parse(localStorage.getItem(localStroagePrefix + currentUserId));
    return [];
  };

  addNewItem = obj => {
    const { currentUserId } = this.props;
    const ary = this.getLocalStorage();
    ary.push(obj);
    localStorage.setItem(localStroagePrefix + currentUserId, JSON.stringify(ary));
    this.setState({ list: this.getLocalStorage() });
  };

  showModal = e => {
    if (e) e.preventDefault();
    this.setState({ addModalVisible: true });
  };

  handleOk = () => {
    const { form } = this.props;
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      const { content, overdueDate } = values;
      this.addNewItem({
        id: this.genID(36),
        content,
        overdueDate,
        hadFinished: false,
        addDate: new Date(),
      });
      this.setState({ addModalVisible: false });
    });
  };

  handleCancel = () => {
    this.setState({ addModalVisible: false });
  };

  handleFinish = id => {
    const { currentUserId } = this.props;
    const ary = this.getLocalStorage();
    const newAry = ary.map(item => {
      const newItem = { ...item };
      if (newItem.id === id) newItem.hadFinished = true;
      return newItem;
    });
    localStorage.setItem(localStroagePrefix + currentUserId, JSON.stringify(newAry));
    this.setState({ list: this.getLocalStorage() });
  };

  handleDelete = id => {
    const { currentUserId } = this.props;
    const ary = this.getLocalStorage();
    ary.splice(ary.findIndex(item => item.id === id), 1);
    localStorage.setItem(localStroagePrefix + currentUserId, JSON.stringify(ary));
    this.setState({ list: this.getLocalStorage() });
  };

  genID = len => {
    let rdmString = '';
    while (rdmString.length < len) {
      // eslint-disable-next-line
      rdmString += ((Math.random() * 37) | 0).toString(36);
    }
    return rdmString;
  };

  renderText = item => {
    if (item.hadFinished)
      return (
        <Typography.Text delete>
          {item.content} ({TableRender.ShortDateRender(item.overdueDate)})
        </Typography.Text>
      );

    if (moment(item.overdueDate).date() < moment(new Date()).date())
      return (
        <Typography.Text type="danger">
          {item.content} ({TableRender.ShortDateRender(item.overdueDate)})
        </Typography.Text>
      );

    if (moment(item.overdueDate).date() === moment(new Date()).date())
      return (
        <Typography.Text type="warning">
          {item.content} ({TableRender.ShortDateRender(item.overdueDate)})
        </Typography.Text>
      );

    return (
      <Typography.Text>
        {item.content} ({TableRender.ShortDateRender(item.overdueDate)})
      </Typography.Text>
    );
  };

  render() {
    const { addModalVisible, list } = this.state;
    const {
      form: { getFieldDecorator },
    } = this.props;
    const dataList = list.sort((a, b) => moment(a.overdueDate) - moment(b.overdueDate));

    return (
      <>
        <Card
          title="追踪事项"
          extra={
            <a href="#" onClick={this.showModal}>
              <Icon type="plus" />
            </a>
          }
          bordered={false}
          style={{ marginBottom: 24, minHeight: 300 }}
        >
          <List
            itemLayout="horizontal"
            dataSource={dataList}
            renderItem={item => {
              const actions = [];
              if (!item.hadFinished)
                actions.push(
                  <a
                    onClick={() => {
                      this.handleFinish(item.id);
                    }}
                  >
                    <Icon type="check" />
                  </a>
                );
              actions.push(
                <a
                  onClick={() => {
                    this.handleDelete(item.id);
                  }}
                >
                  <Icon type="minus" />
                </a>
              );
              return <List.Item actions={actions}>{this.renderText(item)}</List.Item>;
            }}
          />
        </Card>

        <Modal
          title="新增追踪事项"
          visible={addModalVisible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          destroyOnClose
          maskClosable={false}
        >
          <Form.Item>
            {getFieldDecorator('content', {
              rules: [{ required: true, message: '请输入追踪事项' }],
            })(<Input.TextArea rows={4} placeholder="请输入追踪事项" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('overdueDate', {
              rules: [{ required: true, message: '请输入过期日期' }],
            })(<DatePicker placeholder="请输入过期日期" />)}
          </Form.Item>
        </Modal>
      </>
    );
  }
}

export default TodoList;
