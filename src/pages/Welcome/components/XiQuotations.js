import React from 'react';
import { Card, Icon } from 'antd';

const quotations = [
  '人民有信心，国家才有未来，国家才有力量。',
  '昨天的成功并不代表着今后能够永远成功，过去的辉煌并不意味着未来可以永远辉煌。时代是出卷人，我们是答卷人，人民是阅卷人。',
  '历史从不眷顾因循守旧、满足现状者，机遇属于勇于创新、永不自满者。',
  '信息化为中华民族代来了千载难逢的机遇。',
  '青春理想， 青春活力，青春奋斗，是中国精神和中国力量的生命力所在。',
  '理论自觉、文化自信，是一个民族进步的力量；价值先进、思想解放，是一个社会活力的来源。国家之魂，文以化之，文以铸之。',
  '生态兴则文明兴， 生态衰则文明衰。',
  '在新科技代来的新机遇面前，每个国家都有平等的发展权力。潮流来了，跟不上就会落后，就会被淘汰。',
  '中国的今天，是中国人民干出来！',
  '心中有信仰，行动有力量。',
];

// 临时用
const XiQuotations = () => {
  // eslint-disable-next-line
  const randomItem = quotations[(Math.random() * quotations.length) | 0];
  return (
    <Card
      title="习主席语录"
      bordered={false}
      extra={
        <a href="#">
          <Icon type="redo" />
        </a>
      }
      style={{ marginBottom: 24, minHeight: 200 }}
    >
      {randomItem}
    </Card>
  );
};

export default XiQuotations;
