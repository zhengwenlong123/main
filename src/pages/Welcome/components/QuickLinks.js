import React, { PureComponent } from 'react';
import { connect } from 'dva';
import { Card, Icon, Modal, Form, Input, Button } from 'antd';

const localStroagePrefix = 'quickLinks_';
// 临时项目
@connect(({ user }) => ({
  currentUserId: user.currentUser.id,
}))
@Form.create()
class QuickLinks extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      addModalVisible: false,
      list: [],
    };
  }

  componentDidMount() {
    this.setState({ list: this.getLocalStorage() });
  }

  getLocalStorage = () => {
    const { currentUserId } = this.props;
    if (JSON.parse(localStorage.getItem(localStroagePrefix + currentUserId)))
      return JSON.parse(localStorage.getItem(localStroagePrefix + currentUserId));
    return [];
  };

  addNewItem = obj => {
    const { currentUserId } = this.props;
    const ary = this.getLocalStorage();
    ary.push(obj);
    localStorage.setItem(localStroagePrefix + currentUserId, JSON.stringify(ary));
    this.setState({ list: this.getLocalStorage() });
  };

  showModal = e => {
    if (e) e.preventDefault();
    this.setState({ addModalVisible: true });
  };

  handleOk = () => {
    const { form } = this.props;
    form.validateFieldsAndScroll((err, values) => {
      if (err) return;
      this.addNewItem({
        ...values,
        id: this.genID(36),
        hadFinished: false,
        addDate: new Date(),
      });
      this.setState({ addModalVisible: false });
    });
  };

  handleCancel = () => {
    this.setState({ addModalVisible: false });
  };

  handleFinish = id => {
    const { currentUserId } = this.props;
    const ary = this.getLocalStorage();
    const newAry = ary.map(item => {
      const newItem = { ...item };
      if (newItem.id === id) newItem.hadFinished = true;
      return newItem;
    });
    localStorage.setItem(localStroagePrefix + currentUserId, JSON.stringify(newAry));
    this.setState({ list: this.getLocalStorage() });
  };

  handleDelete = id => {
    const { currentUserId } = this.props;
    const ary = this.getLocalStorage();
    ary.splice(ary.findIndex(item => item.id === id), 1);
    localStorage.setItem(localStroagePrefix + currentUserId, JSON.stringify(ary));
    this.setState({ list: this.getLocalStorage() });
  };

  genID = len => {
    let rdmString = '';
    while (rdmString.length < len) {
      // eslint-disable-next-line
      rdmString += ((Math.random() * 37) | 0).toString(36);
    }
    return rdmString;
  };

  render() {
    const { addModalVisible, list } = this.state;
    const {
      form: { getFieldDecorator },
    } = this.props;

    return (
      <>
        <Card
          title="快速链接"
          extra={
            <a href="#" onClick={this.showModal}>
              <Icon type="setting" />
            </a>
          }
          bordered={false}
          style={{ marginBottom: 24 }}
        >
          {list &&
            Array.isArray(list) &&
            list.map(item => (
              <Button
                key={item.id}
                type="primary"
                ghost
                style={{ margin: 5 }}
                href={item.websiteUrl}
              >
                {item.websiteName}
              </Button>
            ))}
        </Card>

        <Modal
          title="新增网站"
          visible={addModalVisible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          destroyOnClose
          maskClosable={false}
        >
          <Form.Item>
            {getFieldDecorator('websiteName', {
              rules: [{ required: true, message: '请输入网站名称' }],
            })(<Input placeholder="网站名称" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('websiteUrl', {
              rules: [{ required: true, message: '请输入网站地址' }],
            })(<Input placeholder="网站地址" />)}
          </Form.Item>
        </Modal>
      </>
    );
  }
}

export default QuickLinks;
