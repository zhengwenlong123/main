import React, { PureComponent } from 'react';
import moment from 'moment';
import 'moment/locale/zh-cn';
import styles from '@/pages/Welcome/Welcome.less';

class TimeClock extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      yearStr: moment().format('LL dddd'),
      timeStr: moment().format('A hh:mm:ss'),
    };
  }

  componentWillMount() {
    this.timer = setInterval(() => {
      this.setState({
        yearStr: moment().format('LL dddd'),
        timeStr: moment().format('A hh:mm:ss'),
      });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    const { yearStr, timeStr } = this.state;

    return (
      <div className={styles.extraContent}>
        <div className={styles.statItem}>
          <p>{yearStr}</p>
          <p>{timeStr}</p>
        </div>
      </div>
    );
  }
}

export default TimeClock;
