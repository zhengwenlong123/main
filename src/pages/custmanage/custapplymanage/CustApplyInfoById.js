/* eslint-disable no-lonely-if */
/* eslint-disable no-useless-escape */
/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-console */
import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Card, message, Button } from 'antd';
import router from 'umi/router'; // eslint-disable-line
import uuid from 'uuid';
import { formatMessage } from 'umi/locale';
import { isEmptyStr } from '@/utils/utils';
import { wrapTid } from '@/utils/mdcutil';
import CustApplyDetail from './CustApplyDetail';
import AuditLogTab from '../custapplyauditmanage/tabs/auditLogTab';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 客户申请单信息维护组件,以新页面访问
 */
class CustCompanyInfoUpkeep extends React.PureComponent {
  constructor(props) {
    super(props);
    const self = this;
    // 初始化数据
    self.state = {
      activeKey: 'detailTab', // tab的
      auditValues: {}, // 审核model中填写的数据
      showSaveBtn: true,
      submitFlag: false, // 是否点击了提交按钮，防止二次点击
    };
    self.tabList = [
      {
        key: 'detailTab',
        tab: '客户',
      },
      // {
      //   key: 'tianyanchaTab',
      //   tab: '天眼查',
      // },
      {
        key: 'auditLogTab',
        tab: '历史审核记录',
      },
    ];
    self.infoRef = null; // 维护组件
  }

  /**
   * 获取申请单详情，初始化传入redux中
   */
  componentDidMount() {
    const {
      dispatch,
      location: {
        query: { id = '', type = 'detail', tid = '10001' },
      },
    } = this.props;
    // 清空为默认
    dispatch({
      type: 'custapplymanage/changeCustApplyInfo',
      payload: {
        custRequestFormInfoAddDto: {},
        custRfCompRelatedCompAddDto: {
          custRfCompRelatedCompDescAddDtoList: [],
        },
        custRfCompanyInfoAddDto: {
          isCompanyCertMerge: 1,
          hasCompanyCode: 1,
          custRfCompanyDescAddDtoList: [],
        },
      }, // 客户申请单详情信息，初始化
    });
    if (type === 'detail' || type === 'edit') {
      // 详情或者维护
      if (isEmptyStr(id)) {
        this.handleToList();
      } else {
        // 根据id查询申请单详情
        dispatch({
          type: 'custapplymanage/getCustApplyInfoByRfId',
          payload: { id, tid },
        });
        // 设置申请单selectedRfId
        dispatch({
          type: 'custapplymanage/changeAuditSelectedApply',
          payload: { rfId: id, tid },
        });
        // 根据rfId查询审核历史记录
        dispatch({
          type: 'custapplymanage/getCustApplyAuditLogs',
          payload: {
            searchCondition: {
              rfId: id,
            },
          },
        });
      }
    } else if (type === 'add') {
      // 新增
      dispatch({
        type: 'custapplymanage/changeCustApplyInfo',
        payload: {
          custRequestFormInfoAddDto: {},
          custRfCompRelatedCompAddDto: {
            custRfCompRelatedCompDescAddDtoList: [],
          },
          custRfCompanyInfoAddDto: {
            isCompanyCertMerge: 1,
            hasCompanyCode: 1,
            custRfCompanyDescAddDtoList: [],
          },
        }, // 客户申请单详情信息，初始化
      });
    }
  }

  // // 监听审核的所选择的记录，改变了rfId参数值
  // componentWillReceiveProps(nextProps) {
  //   // console.log('nextProps', nextProps);
  //   const { selectedRfId, dispatch } = this.props;
  //   if (selectedRfId !== nextProps.selectedRfId) {
  //     // 根据selectedRfId查询申请单详情
  //     dispatch({
  //       type: 'custapplymanage/getCustApplyInfoByRfId',
  //       payload: nextProps.selectedRfId,
  //     });
  //     // 根据selectedRfId查询审核历史记录
  //     dispatch({
  //       type: 'custapplymanage/getCustApplyAuditLogs',
  //       payload: {
  //         searchCondition: {
  //           rfId: nextProps.selectedRfId,
  //         },
  //       },
  //     });
  //   }
  // }

  handleToList = () => {
    router.push('/custmanage/custapplymanage');
  };

  onSubmit = values => {
    const {
      dispatch,
      location: {
        query: { type = 'add' },
      },
    } = this.props;
    const { submitFlag } = this.state;
    console.log(submitFlag);
    if (submitFlag) return;
    this.setState({ submitFlag: true });
    if (type === 'add') {
      // 新增申请单
      dispatch({
        type: 'custapplymanage/addCustApplyInfo',
        payload: {
          ...values,
        },
        callback: (_type, res) => {
          if (res && res.code === 0) {
            // 成功
            this.handleToList();
            message.success(formatMessage({ id: 'form.common.save.tips' }));
            if (values.custRequestFormInfoAddDto.rfStatus === 'verify_waiting') {
              // 新增提交审核时,添加修改操作记录
              this.addLog(values, '申请单提交审核');
            }
          } else {
            message.error((res && res.message) || '新增申请单出错');
          }
          this.setState({ submitFlag: false });
        },
      });
    } else if (type === 'edit') {
      // 修改申请单
      dispatch({
        type: 'custapplymanage/updateCustApplyInfo',
        payload: {
          ...values,
        },
        callback: (_type, res) => {
          if (res && res.code === 0) {
            // 成功,添加修改操作记录
            message.success(formatMessage({ id: 'form.common.update.tips' }));
            this.addLog(values, '申请单更新');
          } else {
            message.error((res && res.message) || '申请单更新出错');
          }
          this.setState({ submitFlag: false });
        },
      });
    }
  };

  // 添加历史操作记录，维护
  addLog = (values, des) => {
    const { dispatch, currentUser, custApplyInfo } = this.props;
    const params = {
      verifyResult: values.custRequestFormInfoAddDto.rfStatus, // 申请单更新状态
      verifyDesc: des,
      verifyId: uuid(),
      rfId: values.custRequestFormInfoAddDto.rfId,
      verifyUserOrgId: wrapTid().tid,
      verifyUserOrgName: currentUser.name,
      createUcode: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUcode)
        ? custApplyInfo.custRequestFormInfoAddDto.createUcode
        : currentUser.id,
      createUname: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUname)
        ? custApplyInfo.custRequestFormInfoAddDto.createUname
        : currentUser.name,
      createUid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUid)
        ? custApplyInfo.custRequestFormInfoAddDto.createUid
        : currentUser.id,
      modifyUcode: currentUser.id,
      modifyUid: currentUser.id,
      modifyUname: currentUser.name,
      verifyUserName: currentUser.name,
      tid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.tid)
        ? custApplyInfo.custRequestFormInfoAddDto.tid
        : wrapTid().tid, // 租户id,
    };
    dispatch({
      type: 'custapplymanage/addCustApplyAudit',
      payload: params,
      callback: res => {
        if (res.code === 0) {
          // 成功
          this.handleToList();
        } else {
          message.error(res.message);
        }
      },
    });
  };

  // 调用维护组件提交
  onClickSubmit = type => {
    this.infoRef.onClickSubmit(type);
  };

  /**
   * tab切换回调
   */
  tabChange = key => {
    // const { dispatch } = this.props;
    this.setState({
      activeKey: key,
    });
  };

  render() {
    // modalUpkeepType 1详情; 2维护; 10新增
    const {
      location: {
        query: { type = 'add' },
      },
      custApplyInfo: { custRequestFormInfoAddDto: mue },
      custApplyAuditLogs,
    } = this.props;
    const { showSaveBtn = true } = this.state;
    const { activeKey } = this.state;
    let mytype = 1;
    let title = '';
    if (type === 'detail') {
      mytype = 1;
      title = '产品申请单详情';
    } else if (type === 'edit') {
      mytype = 2;
      title = '维护申请单详情';
    } else if (type === 'add') {
      mytype = 10;
      title = '新增申请单详情';
    }
    // tab内容
    const contentList = {
      detailTab: (
        <CustApplyDetail
          from="detail"
          modalUpkeepType={mytype}
          modalVisibleOnChange={this.handleToList}
          onSubmit={this.onSubmit}
          onRef={ref => {
            this.infoRef = ref;
          }}
        />
      ),
      auditLogTab: <AuditLogTab />,
    };
    // tab标签内容
    const tabList = [
      {
        key: 'detailTab',
        tab: '客户',
      },
    ];
    // 当申请单为审核通过,审核驳回,单据作废时，显示审核历史记录
    // if (
    //   mue &&
    //   (mue.rfStatus === 'verify_adopt' ||
    //     mue.rfStatus === 'verify_reject' ||
    //     mue.rfStatus === 'bill_invalid')
    // ) {
    //   tabList.push({
    //     key: 'auditLogTab',
    //     tab: '历史审核记录',
    //   });
    // }

    // 当申请单详情或维护，且有历史记录时，显示审核历史记录
    if (type !== 'add' && custApplyAuditLogs.length > 0) {
      tabList.push({
        key: 'auditLogTab',
        tab: '历史审核记录',
      });
    }

    const cardOperateAreaCmp = () => {
      return (
        <div>
          {showSaveBtn && ((mytype === 2 && mue.id) || mytype === 10) ? (
            <div>
              <Button
                type="primary"
                onClick={() => {
                  this.onClickSubmit('save');
                }}
                style={{ marginRight: '8px' }}
              >
                {formatMessage({ id: 'button.common.save' })}
              </Button>
              <Button
                type="primary"
                onClick={() => {
                  this.onClickSubmit('submitAudit');
                }}
              >
                {formatMessage({ id: 'button.custApplyManage.submitAudit' })}
              </Button>
            </div>
          ) : null}
        </div>
      );
    };

    return (
      <Card
        title={title}
        bordered={false}
        tabList={tabList}
        activeTabKey={activeKey}
        onTabChange={this.tabChange}
        extra={cardOperateAreaCmp()}
      >
        {type === 'add'
          ? tabList.map(item => (
            <div key={item.key} style={{ display: activeKey === item.key ? 'block' : 'none' }}>
              {!mue.id && contentList[item.key]}
            </div>
            ))
          : tabList.map(item => (
            <div key={item.key} style={{ display: activeKey === item.key ? 'block' : 'none' }}>
              {mue.id && contentList[item.key]}
            </div>
            ))}
      </Card>
    );
  }
}

export default connect(({ custapplymanage, user }) => ({
  custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户申请单详情信息
  currentUser: user.currentUser || {}, // 当前用户
  custApplyAuditLogs: custapplymanage.custApplyAuditLogs || {}, // 申请单审核记录
}))(CustCompanyInfoUpkeep);
