/* eslint-disable no-lonely-if */
/* eslint-disable no-useless-escape */
/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-console */
import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router'; // eslint-disable-line
// import {  isEmptyStr } from '@/utils/utils';
import CustApplyDetail from './CustApplyDetail';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 客户申请单信息维护组件,以新页面访问
 */
class CustCompanyInfoUpkeep extends React.PureComponent {
  state = {
  };

  
  /**
   * 获取申请单详情，初始化传入redux中
   */
  componentDidMount() {}


  onSubmit = (values) => {
    const { dispatch ,modalVisibleOnChange} = this.props;
    // 新增申请单
    dispatch({
      type: 'custapplymanage/addCustApplyInfo',
      payload: {
        ...values
      },
      callback: () => { 
        modalVisibleOnChange(false);
      }
    });
  }

  render() {
    // modalUpkeepType 1详情; 2维护; 10新增
    
    return (
      <div>
        <CustApplyDetail
          onSubmit={this.onSubmit}
          {...this.props}
        />
      </div>
    );
  }
}

export default connect(
  ({custapplymanage }) => ({
    custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户申请单详情信息
  })
)(CustCompanyInfoUpkeep);
