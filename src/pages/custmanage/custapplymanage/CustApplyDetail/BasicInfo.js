import React from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import uuid from 'uuid';
import { Form, Input, Select, message, Row, Col, InputNumber } from 'antd';
import {
  verifyCustApplyUnique,
  // fileRelationPathReg,
} from '@/utils/mdcutil';
import { isEmptyStr, isEmptyObj } from '@/utils/utils';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

moment.locale('zh-cn');

const colLayout = {
  xs: 24,
  sm: 24,
  lg: 12,
  xl: 12,
  xxl: 12,
};
/**
 * 客户申请单信息维护组件
 */
@ValidationFormHoc
class BasicInfo extends React.PureComponent {
  state = {
    formItemLayout: {},
    formItemLayoutSome: {},
    isShowEbs: false,
    isNotPersonal: true, // 是否是个人，默认不是，true
    companyCodeVerify: false, // 社会信用码是否重复，true：重复
    // companyEbsRule: [],
    // departEbsRule: [],
  };

  /**
   * 初始化state中的文件相关参数值
   */
  componentDidMount() {
    // 根据不同的页面引用展示不同布局
    const { onRef, from } = this.props;
    onRef(this);
    const formItemLayout =
      from === 'audit'
        ? {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 7 },
              xxl: { span: 6 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 17 },
              xxl: { span: 18 },
            },
          }
        : {
            labelCol: { xs: { span: 24 }, sm: { span: 7 }, xl: { span: 5 } },
            wrapperCol: { xs: { span: 24 }, sm: { span: 17 }, xl: { span: 19 } },
          };
    const formItemLayoutSome =
      from === 'audit'
        ? {
            labelCol: {
              xs: { span: 12 },
              sm: { span: 12 },
              lg: { span: 16 },
              xl: { span: 14 },
              xxl: { span: 12 },
            },
            wrapperCol: {
              xs: { span: 12 },
              sm: { span: 12 },
              lg: { span: 8 },
              xl: { span: 10 },
              xxl: { span: 12 },
            },
          }
        : {
            labelCol: { xs: { span: 24 }, sm: { span: 14 }, xl: { span: 10 } },
            wrapperCol: { xs: { span: 24 }, sm: { span: 10 }, xl: { span: 14 } },
          };
    const formItemLayoutSomeRight =
      from === 'audit'
        ? {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 10 },
              xxl: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 14 },
              xxl: { span: 16 },
            },
          }
        : {
            labelCol: { xs: { span: 24 }, sm: { span: 14 }, xl: { span: 10 } },
            wrapperCol: { xs: { span: 24 }, sm: { span: 10 }, xl: { span: 14 } },
          };
    this.setState({ formItemLayout, formItemLayoutSome, formItemLayoutSomeRight });
    // 初始化会计--部门设置
    const {
      custApplyInfo: { custRfCompanyInfoAddDto: mue },
    } = this.props;
    if (mue && mue.custAccountTypeCode === 'internal') {
      this.changeAccountType('internal');
    }
  }

  // 监听 custApplyInfo.custTypeCode
  componentWillReceiveProps(nextProps) {
    // console.log('nextProps', nextProps);
    const { custApplyInfo } = this.props;
    if (JSON.stringify(custApplyInfo) !== JSON.stringify(nextProps.custApplyInfo)) {
      // 根据selectedRfId查询申请单详情
      if (!isEmptyObj(nextProps.custApplyInfo.custRfCompanyInfoAddDto)) {
        if (nextProps.custApplyInfo.custRfCompanyInfoAddDto.custTypeCode === 'personal') {
          this.setState({ isNotPersonal: false });
        } else {
          this.setState({ isNotPersonal: true });
        }
        if (nextProps.custApplyInfo.custRfCompanyInfoAddDto.custAccountTypeCode === 'internal') {
          this.setState({ isShowEbs: true });
        } else {
          this.setState({ isShowEbs: false });
        }
      }
    }
  }

  /**
   * 验证客户名称唯一性
   * tid,custName必传
   */
  verifyUnquieCustName = async custNameInput => {
    const { value } = custNameInput.input;
    const { custApplyInfo, dispatch } = this.props;
    let custVerify = false;
    if (value === null || value.length === 0) return;
    const params = { custName: value };
    const res = await verifyCustApplyUnique(
      '/mdc/cust/custrfnfoentiremanage/verifyNoReviewUnique',
      params
    );
    let status = true;
    if (res && res.rfId !== custApplyInfo.custRfCompanyInfoAddDto.rfId) {
      status = false;
    }
    if (!status) {
      message.warn(formatMessage({ id: 'form.custCompanyInfoManage.custNameExist' }));
      // custNameInput.focus();
      // custNameVerify: false,// 客户名称是否重复，true：重复
      custVerify = true;
    }
    // 企业资质的企业名称和简称，同步更新
    custApplyInfo.custRfCompanyInfoAddDto.companyName = value;
    custApplyInfo.custRfCompanyInfoAddDto.custShortName = value;
    dispatch({
      type: 'custapplymanage/changeCustApplyInfo',
      payload: {
        ...custApplyInfo,
        custNameVerify: custVerify,
      },
    });
  };

  // 账户类别为内部时，会计--部门和公司必填
  changeAccountType = val => {
    // const companyEbsRule = [];
    // const departEbsRule = [];
    const { custApplyInfo } = this.props;
    let isShowEbs = false;
    if (val === 'internal') {
      isShowEbs = true;
      // 内部,必填
      // companyEbsRule.push({
      //   required: true,
      //   message: formatMessage({ id: 'form.custApplyManage.accountTitleCompanyEbs.placeholder' }),
      // });
      // departEbsRule.push({
      //   required: true,
      //   message: formatMessage({ id: 'form.custApplyManage.accountTitleDepartEbs.placeholder' }),
      // });
    } else {
      // 删除会计科目-公司，部门的值
      custApplyInfo.custRfCompanyInfoAddDto.accountTitleCompanyEbs = null;
      // eslint-disable-next-line no-param-reassign
      custApplyInfo.custRfCompanyInfoAddDto.accountTitleDepartEbs = null;
    }
    this.setState({ isShowEbs });
  };

  /**
   * 3、经营单位类型代码为 NKA、KW、KA、STORE、OTHER 时，场景校验：
   *【企业统一社会信用代码不可重复】
   */

  changeOperUnitTypeCode = async val => {
    const {
      custApplyInfo: { custRfCompanyInfoAddDto: mue },
    } = this.props;
    const whiteList = ['NKA', 'KW', 'KA', 'STORE', 'OTHER'];
    if (whiteList.includes(val)) {
      // 校验社会信用代码不可重复
      const value = mue.companyCode;
      if (value === null || value.length === 0) return;
      const params = { companyCode: value };
      let companyVerify = false;
      const res = await verifyCustApplyUnique(
        '/mdc/cust/custrfnfoentiremanage/verifyNoReviewUnique',
        params
      );
      let status = true;
      if (res && res.rfId !== mue.rfId) {
        status = false;
      }
      if (!status) {
        message.warn(formatMessage({ id: 'form.custApplyManage.companyCodeExist' }));
        // companyCodeInput.focus();
        companyVerify = true;
      }
      this.setState({
        companyCodeVerify: companyVerify,
      });
    } else {
      this.setState({
        companyCodeVerify: false,
      });
    }
  };

  // 数字的number必须通过自定义去设置规则，而不能通过自带max、min、len去设置
  handleCompanyEbsRule = (rule, value, callback) => {
    if (value && value.toString().length !== 5) {
      callback(
        formatMessage({
          id: 'form.custApplyManage.accountTitleCompanyEbs.no',
        })
      );
    }
    // Note: 必须总是返回一个 callback，否则 validateFieldsAndScroll 无法响应
    callback();
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      companyCodeVerify, // 社会信用码是否重复，true：重复
    } = this.state;
    console.log('basicInfoRef');
    if (companyCodeVerify) {
      // 社会信用码是否重复，true：重复
      message.warn(formatMessage({ id: 'form.custApplyManage.companyCodeExist' }));
      // eslint-disable-next-line no-useless-return
      return false;
    }
    return true;
  };

  render() {
    const {
      formItemLayout,
      formItemLayoutSome,
      formItemLayoutSomeRight,
      isShowEbs,
      isNotPersonal,
    } = this.state;
    const {
      modalUpkeepType: mut,
      // custApplyInfo: mue,
      form,
      custOperUnitTypeAll: couta, // 经营单位类型定义数据(所有)
      custTypeAll, // 客户性质类型(所有)
      custAccountTypeAll, // 账号类型数据(所有)
      ebsAccountType, // 账户分类名称 类型数据(所有)
      tianYCInfo,
    } = this.props;
    // if (!isShowEbs) {// 会计科目初始化
    //   form.setFieldsValue({
    //     accountTitleCompanyEbs: '',
    //     accountTitleDepartEbs:''
    //   });
    // }
    const { getFieldDecorator } = form;
    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Row>
              <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.custName' })}>
                {getFieldDecorator('custName', {
                  rules: [
                    {
                      required: true,
                      message: formatMessage({ id: 'form.custCompanyInfoManage.custNameMust' }),
                    },
                  ],
                })(
                  <Input
                    disabled={mut === 1}
                    maxLength={100}
                    // eslint-disable-next-line no-return-assign
                    ref={input => (this.custNameInput = input)}
                    onBlur={() => this.verifyUnquieCustName(this.custNameInput)}
                  />
                )}
              </Form.Item>
            </Row>
            <Row>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSome}
                  label={formatMessage({ id: 'form.custCompanyInfoManage.operUnitTypeCode' })}
                >
                  {getFieldDecorator('operUnitTypeCode', {
                    rules: [
                      {
                        required: true,
                        message: formatMessage({
                          id: 'form.custCompanyInfoManage.operUnitTypeCodeMust',
                        }),
                      },
                    ],
                  })(
                    <Select
                      disabled={mut === 1}
                      onChange={value => this.changeOperUnitTypeCode(value)}
                    >
                      {couta.map(item => (
                        <Select.Option key={item.operUnitTypeCode} value={item.operUnitTypeCode}>
                          {item.operUnitTypeName}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSomeRight}
                  label={formatMessage({ id: 'form.custApplyManage.custType' })}
                >
                  {getFieldDecorator('custTypeCode', {
                    rules: [
                      {
                        required: true,
                        message: formatMessage({
                          id: 'form.custApplyManage.custType.placeholder',
                        }),
                      },
                    ],
                  })(
                    <Select disabled={mut === 1}>
                      {custTypeAll.map(item => (
                        <Select.Option key={item.id} value={item.parmCode}>
                          {item.parmShowValue}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
                {!isEmptyObj(tianYCInfo) && isNotPersonal ? (
                  <p className={`${styles.tianYCRow} ${styles.tianYCFont}`}>
                    {formatMessage({ id: 'form.tianYCInfo.isMicroEnt' })}：
                    {tianYCInfo.isMicroEnt === 0 ? '否' : '是'}
                  </p>
                ) : null}
              </Col>
            </Row>
            <Row>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSome}
                  label={formatMessage({ id: 'form.custApplyManage.accountType' })}
                >
                  {getFieldDecorator('custAccountTypeCode', {
                    rules: [
                      {
                        required: true,
                        message: formatMessage({
                          id: 'form.custApplyManage.accountType.placeholder',
                        }),
                      },
                    ],
                  })(
                    <Select disabled={mut === 1} onChange={value => this.changeAccountType(value)}>
                      {custAccountTypeAll.map(item => (
                        <Select.Option key={item.id} value={item.parmCode}>
                          {item.parmShowValue}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSomeRight}
                  label={formatMessage({ id: 'form.custApplyManage.custAccountTypeCtagCode' })}
                >
                  {getFieldDecorator('custAccountTypeCtagCode', {
                    rules: [
                      {
                        required: true,
                        message: formatMessage({
                          id: 'form.custApplyManage.custAccountTypeCtagCode.placeholder',
                        }),
                      },
                    ],
                  })(
                    <Select disabled={mut === 1}>
                      {ebsAccountType.map(item => (
                        <Select.Option key={item.id} value={item.parmCode}>
                          {item.parmShowValue}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            {isShowEbs ? (
              <Row>
                <Col {...colLayout}>
                  <Form.Item
                    {...formItemLayoutSome}
                    label={formatMessage({ id: 'form.custApplyManage.accountTitleCompanyEbs' })}
                  >
                    {getFieldDecorator('accountTitleCompanyEbs', {
                      rules: [
                        {
                          required: true,
                          message: formatMessage({
                            id: 'form.custApplyManage.accountTitleCompanyEbs.placeholder',
                          }),
                        },
                        {
                          // max: 5,
                          // min:5,
                          // len:5,
                          // message: formatMessage({
                          //   id: 'form.custApplyManage.accountTitleCompanyEbs.no',
                          // }),
                          validator: this.handleCompanyEbsRule,
                        },
                      ],
                    })(
                      <InputNumber
                        disabled={mut === 1}
                        maxLength={5}
                        precision={0}
                        style={{ width: '100%' }}
                      />
                      // <Input disabled={mut === 1} maxLength={50} style={{ width: '100%' }} />
                    )}
                  </Form.Item>
                </Col>
                <Col {...colLayout}>
                  <Form.Item
                    {...formItemLayoutSomeRight}
                    label={formatMessage({ id: 'form.custApplyManage.accountTitleDepartEbs' })}
                  >
                    {getFieldDecorator('accountTitleDepartEbs', {
                      rules: [
                        {
                          required: true,
                          message: formatMessage({
                            id: 'form.custApplyManage.accountTitleDepartEbs.placeholder',
                          }),
                        },
                      ],
                    })(<Input disabled={mut === 1} maxLength={50} style={{ width: '100%' }} />)}
                  </Form.Item>
                </Col>
              </Row>
            ) : null}
            <Row>
              <Form.Item label={formatMessage({ id: 'form.custApplyManage.custOuId' })}>
                {getFieldDecorator('custOuId', {
                  rules: [
                    {
                      required: true,
                      message: formatMessage({ id: 'form.custApplyManage.custOuId.placeholder' }),
                    },
                  ],
                })(
                  <InputNumber
                    disabled={mut === 1}
                    maxLength={50}
                    precision={0}
                    style={{ width: '100%' }}
                  />
                )}
              </Form.Item>
            </Row>
            <Row>
              <Form.Item label={formatMessage({ id: 'form.custApplyManage.custDescription' })}>
                {getFieldDecorator('custDescription')(
                  <Input.TextArea disabled={mut === 1} rows={3} maxLength={255} />
                )}
              </Form.Item>
            </Row>
          </Form>
        </div>
      </div>
    );
  }
}

export default connect(
  ({ custoperunittypemanage, parmpublicparametermanage, custapplymanage }) => ({
    custOperUnitTypeAll: custoperunittypemanage.custOperUnitTypeAll || [], // 经营单位类型定义数据(所有)
    custTypeAll: parmpublicparametermanage.parmPublicParameterCustType || [], // 客户性质类型(所有)
    custAccountTypeAll: parmpublicparametermanage.parmPublicParameterCustAccountType || [], // 账号类型数据(所有)
    ebsAccountType: parmpublicparametermanage.parmPublicParameterEbsAccountType || [], // 账号类型数据(所有)
    custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户申请单详情信息
    tianYCInfo: custapplymanage.tianYCInfo || {}, // 天眼查信息企业基本信息
  })
)(
  Form.create({
    onValuesChange({ dispatch, custApplyInfo }, changedValues) {
      // 没有申请单id，生成rfId
      if (isEmptyStr(custApplyInfo.custRfCompanyInfoAddDto.rfId)) {
        // eslint-disable-next-line no-param-reassign
        custApplyInfo.custRfCompanyInfoAddDto.rfId = uuid();
      }
      dispatch({
        type: 'custapplymanage/changeCustApplyInfo',
        payload: {
          ...custApplyInfo,
          custRfCompanyInfoAddDto: {
            ...custApplyInfo.custRfCompanyInfoAddDto,
            ...changedValues,
          },
        },
      });
    },
    mapPropsToFields(props) {
      const {
        custApplyInfo: { custRfCompanyInfoAddDto: mue },
      } = props;
      // 注册地址
      const registerAddressArray = [];
      if (mue && mue.companyProvince) registerAddressArray.push(mue.companyProvince);
      if (mue && mue.companyCity) registerAddressArray.push(mue.companyCity);
      if (mue && mue.companyDistrict) registerAddressArray.push(mue.companyDistrict);
      return {
        custCode: Form.createFormField({ value: mue ? mue.custCode : '' }),
        custName: Form.createFormField({ value: mue ? mue.custName : '' }),
        operUnitTypeCode: Form.createFormField({ value: mue ? mue.operUnitTypeCode : '' }),
        custAccountTypeCode: Form.createFormField({ value: mue ? mue.custAccountTypeCode : '' }),
        custAccountTypeCtagCode: Form.createFormField({
          value: mue ? mue.custAccountTypeCtagCode : '',
        }),
        accountTitleCompanyEbs: Form.createFormField({
          value: mue ? mue.accountTitleCompanyEbs : '',
        }),
        accountTitleDepartEbs: Form.createFormField({
          value: mue ? mue.accountTitleDepartEbs : '',
        }),
        custTypeCode: Form.createFormField({ value: mue ? mue.custTypeCode : '' }),
        custOuId: Form.createFormField({ value: mue ? mue.custOuId : '' }),
        custDescription: Form.createFormField({ value: mue ? mue.custDescription : '' }),
      };
    },
  })(BasicInfo)
);
