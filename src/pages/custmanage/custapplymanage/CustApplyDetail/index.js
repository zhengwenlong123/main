/* eslint-disable prefer-destructuring */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-case-declarations */
import React from 'react';
import { connect } from 'dva';
import { Card, message } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import BasicInfo from './BasicInfo';
import EnterpriseQualification from './EnterpriseQualification';
import AffiliatedEnterprises from './AffiliatedEnterprises';
import LegalManInfo from './LegalManInfo';
import { isEmptyStr, isEmptyObj } from '@/utils/utils';
import { wrapTid } from '@/utils/mdcutil';
import { getAddressByRegionCode } from '@/services/custmanage/custapplymanage';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 客户申请单新增、维护的界面部分组件
 */
class CustApplyDetail extends React.PureComponent {
  state = {};

  /**
   * 产品tab下所有form表单对象
   */
  formObj = {
    basicInfo: null,
    enterpriseQualification: null,
    affiliatedEnterprises: null,
    legalManInfo: null,
  };

  /**
   * 子组件的ref
   *
   */
  basicInfoRef = null; //

  enterpriseRef = null; // 企业资质

  affiliatedRef = null; // 关联企业信息

  legalRef = null; // 法人信息

  /**
   * 产品所有表单属性列表
   */
  allCardList = [
    {
      key: 'basicInfo',
      title: '客户基本信息',
      cardRender: () => (
        <BasicInfo
          wrappedComponentRef={form => this.getForm('basicInfo', form)}
          {...{
            ...this.props,
            onRef: ref => {
              this.basicInfoRef = ref;
            },
          }}
        />
      ),
    },
    {
      key: 'enterpriseQualification',
      title: '企业资质',
      cardRender: () => (
        <EnterpriseQualification
          wrappedComponentRef={form => this.getForm('enterpriseQualification', form)}
          {...{
            ...this.props,
            onRef: ref => {
              this.enterpriseRef = ref;
            },
          }}
        />
      ),
    },
    {
      key: 'affiliatedEnterprises',
      title: '关联企业信息',
      cardRender: () => (
        <AffiliatedEnterprises
          // onRef={ref => {
          //   this.affiliatedRef = ref;
          // }}
          // {...this.props}
          wrappedComponentRef={form => this.getForm('affiliatedEnterprises', form)}
          {...{
            ...this.props,
            onRef: ref => {
              this.affiliatedRef = ref;
            },
          }}
        />
      ),
    },
    {
      key: 'legalManInfo',
      title: '法人信息',
      cardRender: () => (
        <LegalManInfo
          // onRef={ref => {
          //   this.legalRef = ref;
          // }}
          // {...this.props}
          wrappedComponentRef={form => this.getForm('legalManInfo', form)}
          {...{
            ...this.props,
            onRef: ref => {
              this.legalRef = ref;
            },
          }}
        />
      ),
    },
  ];

  cardList = this.allCardList.concat([]);

  componentDidMount() {
    const { onRef, dispatch } = this.props;
    const self = this;
    if (onRef) {
      onRef(self);
    }

    /**
     * 加载默认客户申请单选择数据
     */
    // 获取-经营单位类型定义数据(所有)
    dispatch({ type: 'custoperunittypemanage/getCustOperUnitTypeAll' });
    // 获取-客户申请单账号类型数据(所有)
    dispatch({
      type: 'parmpublicparametermanage/getCustAccountType',
      payload: { parmTypeCode: 'cust_account_type' },
    });
    // 获取-币种类型数据(所有)
    dispatch({ type: 'parmcurrencymanage/getParmCurrencyAll' });
    // 获取-客户类型数据(所有)
    dispatch({
      type: 'parmpublicparametermanage/getCustType',
      payload: { parmTypeCode: 'cust_type' },
    });
    // 获取-国别定义(所有)
    dispatch({ type: 'parmcountrymanage/getParmCountryAll' });
    // 获取-行政区划定义数据(所有)(默认获取countryCode: 'CN'的数据)
    dispatch({
      type: 'parmregionmanage/getParmRegionAllByParmRegion',
      payload: { countryCode: 'CN' },
    });
    // 获取-用户证件类型数据(所有)-公用参数(所有)(条件为代码是cust_user_card_type的用户证件类型数据)
    dispatch({
      type:
        'parmpublicparametermanage/getParmPublicParameterAllByParmPublicParameterTypeOfCustUserCardType',
      payload: { parmTypeCode: 'cust_user_card_type' },
    });
    // 获取-客户申请单客户状态数据(所有)
    dispatch({
      type: 'parmpublicparametermanage/getCustRfStatus',
      payload: { parmTypeCode: 'cust_rf_status' },
    });
    // 获取-账户分类名称(所有)
    dispatch({
      type: 'parmpublicparametermanage/getEbsAccountType',
      payload: { parmTypeCode: 'cust_account_type_ebs' },
    });
  }

  /** 组件即将卸载 */
  componentWillUnmount() {
    const { dispatch } = this.props;
    // 清空为默认
    dispatch({
      type: 'custapplymanage/changeCustApplyInfo',
      payload: {
        custRequestFormInfoAddDto: {},
        custRfCompRelatedCompAddDto: {
          custRfCompRelatedCompDescAddDtoList: [],
        },
        custRfCompanyInfoAddDto: {
          isCompanyCertMerge: 1,
          hasCompanyCode: 1,
          custRfCompanyDescAddDtoList: [],
        },
      }, // 客户申请单详情信息，初始化
    });
  }

  getForm = (key, form) => {
    this.formObj[key] = form;
  };

  onClickSubmit = async saveType => {
    const self = this;
    const { currentUser, custApplyInfo, onSubmit } = self.props;
    const flag = self.commit();
    if (!flag) {
      message.warn(formatMessage({ id: 'form.custApplyManage.auditInfoExit' }));
      return;
    }
    if (custApplyInfo.custNameVerify) {
      message.warn(formatMessage({ id: 'form.custCompanyInfoManage.custNameExist' }));
      return;
    }

    let affiliatedEnterprisesData = {
      custCode: custApplyInfo.custRfCompanyInfoAddDto.custCode,
      custRelatedCompAgencyScan: '',
      custRelatedCompCode: '',
      custRelatedCompLicenseScan: '',
      custRelatedCompLicenseSn: '',
      custRelatedCompName: '',
      custRelatedCompTaxScan: '',
      custRfCompRelatedCompDescAddDtoList: [],
      rfId: custApplyInfo.custRfCompanyInfoAddDto.rfId,
    };
    const basicInfoData = await self.basicInfoRef.onClickSubmit();
    if (!basicInfoData) return;
    const enterpriseQualificationData = await self.enterpriseRef.onClickSubmit();
    const legalManInfoData = await self.legalRef.onClickSubmit();
    if (self.cardList.length === 4) {
      affiliatedEnterprisesData = await self.affiliatedRef.onClickSubmit();
      if (isEmptyObj(affiliatedEnterprisesData)) return;
    }
    // 基本信息,企业资质信息,法人信息有错误时 返回
    if (isEmptyObj(enterpriseQualificationData)) return;
    if (isEmptyObj(legalManInfoData)) return;

    // 一些要组装的数据
    const commonPara = {
      createUid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUid)
        ? custApplyInfo.custRequestFormInfoAddDto.createUid
        : currentUser.id,
      createUcode: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUcode)
        ? custApplyInfo.custRequestFormInfoAddDto.createUcode
        : wrapTid().tid, // 租户id,
      createUname: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUname)
        ? custApplyInfo.custRequestFormInfoAddDto.createUname
        : currentUser.name,
      rfFromTid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.rfFromTid)
        ? custApplyInfo.custRequestFormInfoAddDto.rfFromTid
        : wrapTid().tid, // 租户id,
      rfName: custApplyInfo.custRfCompanyInfoAddDto.custName,
      rfRequestCreateCustNum: 1,
      rfRequestUserName: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.rfRequestUserName)
        ? custApplyInfo.custRequestFormInfoAddDto.rfRequestUserName
        : currentUser.name,
      rfRequestUserOrgId: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.rfRequestUserOrgId)
        ? custApplyInfo.custRequestFormInfoAddDto.tid
        : wrapTid().tid, // 租户id,
      rfRequestUserOrgName: !isEmptyStr(
        custApplyInfo.custRequestFormInfoAddDto.rfRequestUserOrgName
      )
        ? custApplyInfo.custRequestFormInfoAddDto.rfRequestUserOrgName
        : '主数据',
      tid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.tid)
        ? custApplyInfo.custRequestFormInfoAddDto.tid
        : wrapTid().tid, // 租户id,
    };

    // 申请单信息
    const custRequestFormInfoAddDto = {
      ...commonPara,
      rfId: custApplyInfo.custRfCompanyInfoAddDto.rfId,
      rfStatus: 'verify_waiting',
    };
    if (saveType === 'save') {
      // 保存申请单,未提交
      custRequestFormInfoAddDto.rfStatus = 'not_submitted';
    } else if (saveType === 'submitAudit') {
      // 提交审核,待审核
      custRequestFormInfoAddDto.rfStatus = 'verify_waiting';
    }

    // 基本信息,企业资质信息,法人信息
    const custRfCompanyInfoAddDto = {
      ...custApplyInfo.custRfCompanyInfoAddDto,
      ...enterpriseQualificationData, // 企业资质信息
      ...legalManInfoData, // 法人信息
      ...commonPara,
      createUid: currentUser.id,
      enable: !isEmptyStr(custApplyInfo.custRfCompanyInfoAddDto.enable)
        ? custApplyInfo.custRfCompanyInfoAddDto.enable
        : 1,
      seq: 1,
    };
    /** 注册地址，保存文字如广东省广州市，区可以不传，不传时为空 */

    // 省市存在
    if (custRfCompanyInfoAddDto.companyProvince) {
      const whiteProvinceList = ['710000000000', '810000000000', '820000000000']; // 台湾、香港、澳门只有省
      if (whiteProvinceList.includes(custRfCompanyInfoAddDto.companyProvince)) {
        const provinceAddress = await getAddressByRegionCode(
          wrapTid({ regionCode: custRfCompanyInfoAddDto.companyProvince })
        );
        custRfCompanyInfoAddDto.companyProvince = provinceAddress.data[0].regionName;
        custRfCompanyInfoAddDto.companyCity = '无';
        custRfCompanyInfoAddDto.companyDistrict = '无';
      } else if (custRfCompanyInfoAddDto.companyCity) {
        // 根据code分别查询省市名称
        const provinceAddress = await getAddressByRegionCode(
          wrapTid({ regionCode: custRfCompanyInfoAddDto.companyProvince })
        );
        const cityAddress = await getAddressByRegionCode(
          wrapTid({ regionCode: custRfCompanyInfoAddDto.companyCity })
        );
        custRfCompanyInfoAddDto.companyProvince = provinceAddress.data[0].regionName;
        custRfCompanyInfoAddDto.companyCity = cityAddress.data[0].regionName;
        // 区域存在
        if (custRfCompanyInfoAddDto.companyDistrict) {
          // 根据code查询区域名称
          const districtAddress = await getAddressByRegionCode(
            wrapTid({ regionCode: custRfCompanyInfoAddDto.companyDistrict })
          );
          custRfCompanyInfoAddDto.companyDistrict = districtAddress.data[0].regionName;
        } else {
          custRfCompanyInfoAddDto.companyDistrict = '无';
        }
      }
    } else {
      message.warn(formatMessage({ id: 'form.custCompanyInfoManage.registerAddress-conformity' }));
      return;
    }
    // 关联企业信息
    let custRfCompRelatedCompAddDto = {};
    let para = {};
    if (self.cardList.length === 4) {
      custRfCompRelatedCompAddDto = {
        ...affiliatedEnterprisesData,
        rfId: custApplyInfo.custRfCompanyInfoAddDto.rfId,
        tid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.tid)
          ? custApplyInfo.custRequestFormInfoAddDto.tid
          : wrapTid().tid, // 租户id,
        custCode: custApplyInfo.custRfCompanyInfoAddDto.custCode,
      };
      para = {
        custRequestFormInfoAddDto,
        custRfCompanyInfoAddDto,
        custRfCompRelatedCompAddDto,
      };
    } else {
      para = {
        custRequestFormInfoAddDto,
        custRfCompanyInfoAddDto,
      };
    }

    onSubmit(para);
    console.log(para);
  };

  /** 相关form校验 */
  commit = () => {
    const self = this;
    const obj = {};
    self.cardList.forEach(item => {
      const status = self.formObj[item.key].validationForm();
      if (!status) obj[item.key] = true;
      // self.formObj[item.key].form.validateFields(err => {
      //   if (err) {
      //     obj[item.key] = true;
      //   }
      // });
    });
    if (Object.keys(obj).length !== 0) {
      return false;
    }
    return true;
  };

  render() {
    const titleFnc = item => {
      return `${item.title}`;
    };
    const {
      // modalVisibleOnChange,
      modalUpkeepType: mut,
      custApplyInfo: { custRfCompanyInfoAddDto: mue },
      // showSaveBtn = true,
    } = this.props;
    if (!isEmptyObj(mue) && mue.custTypeCode === 'personal') {
      this.cardList = this.allCardList.concat([]);
    } else {
      this.cardList = this.allCardList.slice(0, 2).concat(this.allCardList.slice(3));
    }
    // const cardOperateAreaCmp = () => {
    //   return (
    //     <div>
    //       {showSaveBtn && ((mut === 2 && mue.id) || mut === 10) ? (
    //         <div>
    //           <Button
    //             type="primary"
    //             onClick={() => {
    //               this.onClickSubmit('save');
    //             }}
    //             style={{marginRight:'8px',}}
    //           >
    //             {formatMessage({ id: 'button.common.save' })}
    //           </Button>
    //           <Button
    //             type="primary"
    //             onClick={() => {
    //               this.onClickSubmit('submitAudit');
    //             }}
    //           >
    //             {formatMessage({ id: 'button.custApplyManage.submitAudit' })}
    //           </Button>
    //         </div>
    //       ) : null}
    //     </div>
    //   );
    // };
    return (
      <div className={styles.productCont}>
        {this.cardList.map(item => (
          <Card
            title={titleFnc(item)}
            key={item.key}
            bordered={false}
            headStyle={{ paddingLeft: 0, minHeight: 30 }}
          >
            {mut === 10 ? item.cardRender() : mue.id ? item.cardRender() : null}
          </Card>
        ))}
      </div>
    );
  }
}

export default connect(
  ({
    custoperunittypemanage,
    parmcountrymanage,
    parmregionmanage,
    parmpublicparametermanage,
    custapplymanage,
    user,
  }) => ({
    custOperUnitTypeAll: custoperunittypemanage.custOperUnitTypeAll || [], // 经营单位类型定义数据(所有)
    custTypeAll: parmpublicparametermanage.parmPublicParameterCustType || [], // 客户性质类型(所有)
    custAccountTypeAll: parmpublicparametermanage.parmPublicParameterCustAccountType || [], // 账号类型数据(所有)
    parmCountryAll: parmcountrymanage.parmCountryAll || [], // 国别定义(所有)
    parmRegionAll: parmregionmanage.parmRegionAll || [], // 行政区划定义数据(所有)
    parmPublicParameterOfCustUserCardTypeAll:
      parmpublicparametermanage.parmPublicParameterOfCustUserCardTypeAll, // 根据公用参数类型定义代码[cust_user_card_type]查询匹配到的公用参数定义数据[用户证件类型数据所有]
    custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户申请单详情信息
    currentUser: user.currentUser || {},
  })
)(CustApplyDetail);
