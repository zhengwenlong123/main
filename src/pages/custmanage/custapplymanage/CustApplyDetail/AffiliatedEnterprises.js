import React from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import { Form, Input, message, Row, Col } from 'antd';
import { wrapTid, fileRelationPathReg } from '@/utils/mdcutil';
import UploadSingleFile from '@/components/UploadBasic/UploadSingleFile';
import UploadSingleFiles from '@/components/UploadBasic/UploadSingleFiles'; // 上传多个文件
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import request from '@/utils/request';
import { fileUrl, accessFileUrl } from '@/defaultSettings';
import { isEmptyObj, isEmptyStr } from '@/utils/utils';
import styles from './index.less';

moment.locale('zh-cn');

/**
 * 客户申请单信息维护组件
 */
@ValidationFormHoc
class AffiliatedEnterprises extends React.PureComponent {
  state = {
    formItemLayout: {},
    custRelatedCompLicenseScan: '', // 营业执照副件扫描件,单张
    custRelatedCompAgencyScan: '', // 组织机构代码证扫描件,单张
    custRelatedCompTaxScan: '', // 税务登记证扫描件,单张
    companyOtherScan: '', // 其他证件扫描件,单张
    companyOtherScanList: [], // 其他证件扫描件地址,多张
    companyOtherScanObjList: [], // 其他证件扫描件所有信息,多张
    isNotPersonal: true, // 是否是个人，默认不是，true
  };

  /**
   * 初始化state中的文件相关参数值
   */
  componentDidMount() {
    const {
      custApplyInfo: { custRfCompRelatedCompAddDto: mue },
      onRef,
      from,
    } = this.props;
    onRef(this);
    const companyOtherScanList = []; // 其他证件扫描件,多张
    let companyOtherScanObjList = []; // 其他证件扫描件,多张
    if (mue && mue.custRfCompRelatedCompDescAddDtoList.length > 0) {
      mue.custRfCompRelatedCompDescAddDtoList.forEach(val => {
        companyOtherScanList.push(val.custPackingListAddr);
      });
      companyOtherScanObjList = [...mue.custRfCompRelatedCompDescAddDtoList];
    }
    // if (isEmptyStr(mue.isCompanyCertMerge)) mue.isCompanyCertMerge = true;
    const formItemLayout =
      from === 'audit'
        ? {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 8 },
              xxl: { span: 6 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 16 },
              xxl: { span: 18 },
            },
          }
        : {
            labelCol: { xs: { span: 24 }, sm: { span: 7 }, xl: { span: 5 } },
            wrapperCol: { xs: { span: 24 }, sm: { span: 17 }, xl: { span: 19 } },
          };
    this.setState({
      formItemLayout,
      custRelatedCompLicenseScan:
        mue && !isEmptyStr(mue.custRelatedCompLicenseScan)
          ? `${accessFileUrl}/${mue.custRelatedCompLicenseScan}`
          : '', // 营业执照副件扫描件,单张
      custRelatedCompAgencyScan:
        mue && !isEmptyStr(mue.custRelatedCompAgencyScan)
          ? `${accessFileUrl}/${mue.custRelatedCompAgencyScan}`
          : '', // 组织机构代码证扫描件,单张
      custRelatedCompTaxScan:
        mue && !isEmptyStr(mue.custRelatedCompTaxScan) ? `${accessFileUrl}/${mue.custRelatedCompTaxScan}` : '', // 税务登记证扫描件,单张
      companyOtherScanList, // 其他证件扫描件,多张
      companyOtherScanObjList,
    });
  }

  // 监听 custApplyInfo.custTypeCode
  componentWillReceiveProps(nextProps) {
    // console.log('nextProps', nextProps);
    const { custApplyInfo } = this.props;
    if (JSON.stringify(custApplyInfo) !== JSON.stringify(nextProps.custApplyInfo)) {
      // 根据selectedRfId查询申请单详情
      if (!isEmptyObj(nextProps.custApplyInfo.custRfCompanyInfoAddDto)) {
        if (nextProps.custApplyInfo.custRfCompanyInfoAddDto.custTypeCode === 'personal') {
          this.setState({ isNotPersonal: false });
        } else {
          this.setState({ isNotPersonal: true });
        }
      }
    }
  }

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      if (modalVisibleOnChange) modalVisibleOnChange(false);
    } else {
      message.info(formatMessage({ id: 'form.common.optionError' }));
    }
  };

  /**
   * 文件上传UploadFile组件回调函数
   */
  uploadFileCallback = (uploadSign, newFile) => {
    const obj = {};
    if ((newFile && newFile.type.indexOf('image') >= 0) || newFile === '') {
      obj[uploadSign] = newFile;
      this.setState(obj);
    } else if (newFile) {
      message.warn(formatMessage({ id: 'form.common.UploadTypeError' }));
    }
  };

  // 上传多幅图片的，将上传的图片存入本地的state中
  uploadFileTranscoded = async (data, type) => {
    const { companyOtherScanList, companyOtherScanObjList } = this.state;
    if (type === 1) {
      // 上传，新增图片文件对象在通过接口保存在数据库中
      const formData = new FormData();
      formData.append('file', data);
      const res = await request.post(`${fileUrl}/bootstrap/fileobject/upload/single`, {
        data: formData,
      });
      if (res) {
        companyOtherScanList.push(res.url);
        companyOtherScanObjList.push(res);
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseSnUploadError' })
        );
        return;
      }
      this.setState({
        companyOtherScanList: [...companyOtherScanList],
        companyOtherScanObjList: [...companyOtherScanObjList],
      });
    } else if (type === 2) {
      // 删除图片文件，则删除缓存list的文件，data对应为文件所在list的位置
      companyOtherScanList.splice(data, 1);
      companyOtherScanObjList.splice(data, 1); // 暂时先删除缓存中的信息
      this.setState({
        companyOtherScanList: [...companyOtherScanList],
        companyOtherScanObjList: [...companyOtherScanObjList],
      });
    }
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      custRelatedCompLicenseScan, // 营业执照副件扫描件,单张
      custRelatedCompAgencyScan, // 组织机构代码证扫描件,单张
      custRelatedCompTaxScan, // 税务登记证扫描件,单张
      companyOtherScanObjList, // 其他证件扫描件所有信息,多张
    } = this.state;
    const {
      form: { getFieldsValue },
      validationForm,
      custApplyInfo,
      currentUser,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();

    if (
      custApplyInfo.custRfCompanyInfoAddDto.isCompanyCertMerge === true ||
      custApplyInfo.custRfCompanyInfoAddDto.isCompanyCertMerge === 1
    ) {
      // 是三证合一
      // 1、营业执照注册号：三证合一后，为社会信用代码，长度18位，
      if (values.custRelatedCompCode.length !== 18) {
        message.warn(
          formatMessage({ id: 'form.custApplyManage.affiliatedCompanyCode.placeholder' })
        );
        return;
      }
    } else {
      // 不是三证合一
      // 3、营业执照注册号录入(可选),三证合一前：15位, 如果有值则必须15位,否则就不要填写数据
      // eslint-disable-next-line no-lonely-if
      if (
        values.custRelatedCompCode !== '' &&
        values.custRelatedCompCode !== null &&
        values.custRelatedCompCode !== undefined &&
        values.custRelatedCompCode.length !== 15
      ) {
        message.warn(
          formatMessage({ id: 'form.custApplyManage.affiliatedCompanyLicenseSn.placeholder' })
        );
        return;
      }
    }

    // 以上必填写数据都验证通过之后,先上传图片并取得图片地址,最后再写数据到数据库
    // 上传-营业执照副件扫描件,单张
    if (custRelatedCompLicenseScan instanceof File) {
      const formData = new FormData();
      formData.append('file', custRelatedCompLicenseScan);
      const custRelatedCompLicenseScanRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (custRelatedCompLicenseScanRes) {
        values.custRelatedCompLicenseScan = custRelatedCompLicenseScanRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseSnUploadError' })
        );
        return;
      }
      // console.log(companyLicenseScanRes);
    } else {
      values.custRelatedCompLicenseScan = custRelatedCompLicenseScan.replace(
        fileRelationPathReg,
        ''
      );
      values.custRelatedCompLicenseScan = values.custRelatedCompLicenseScan.replace(
        `${accessFileUrl}/`,
        ''
      );
    }

    // 上传-组织机构代码证扫描件,单张
    if (custRelatedCompAgencyScan instanceof File) {
      const formData = new FormData();
      formData.append('file', custRelatedCompAgencyScan);
      const custRelatedCompAgencyScanRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (custRelatedCompAgencyScanRes) {
        values.custRelatedCompAgencyScan = custRelatedCompAgencyScanRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScanResUploadError' })
        );
        return;
      }
      // console.log(companyAgencyScanRes);
    } else {
      values.custRelatedCompAgencyScan = custRelatedCompAgencyScan.replace(fileRelationPathReg, '');
      values.custRelatedCompAgencyScan = values.custRelatedCompAgencyScan.replace(
        `${accessFileUrl}/`,
        ''
      );
    }

    // 上传-税务登记证扫描件,单张
    if (custRelatedCompTaxScan instanceof File) {
      const formData = new FormData();
      formData.append('file', custRelatedCompTaxScan);
      const custRelatedCompTaxScanRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (custRelatedCompTaxScanRes) {
        values.custRelatedCompTaxScan = custRelatedCompTaxScanRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScanResUploadError' })
        );
        return;
      }
      // console.log(companyTaxScanRes);
    } else {
      values.custRelatedCompTaxScan = custRelatedCompTaxScan.replace(fileRelationPathReg, '');
      values.custRelatedCompTaxScan = values.custRelatedCompTaxScan.replace(
        `${accessFileUrl}/`,
        ''
      );
    }

    // 上传-企业其他证件正面,多张
    const tempList = [];
    const commPara = {
      createUid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUid)
        ? custApplyInfo.custRequestFormInfoAddDto.createUid
        : currentUser.id,
      createUcode: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.tid)
        ? custApplyInfo.custRequestFormInfoAddDto.tid
        : wrapTid().tid, // 租户id,
      createUname: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUname)
        ? custApplyInfo.custRequestFormInfoAddDto.createUname
        : currentUser.name,
      modifyUid: currentUser.id,
      modifyUcode: wrapTid().tid, // 租户id,
      modifyUname: currentUser.name,
      tid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.tid)
        ? custApplyInfo.custRequestFormInfoAddDto.tid
        : wrapTid().tid, // 租户id,
    };
    if (companyOtherScanObjList.length > 0) {
      companyOtherScanObjList.forEach((item, index) => {
        if (item.url) {
          // 新增
          tempList.push({
            custPackingListAddr: item.url, // 其他证件扫描件存储地址
            custPackingListCode: item.id, // 其他证件扫描件文件编码
            custPackingListName: item.originalName, // 其他证件扫描件文件名称
            custCode: custApplyInfo.custRfCompanyInfoAddDto.custCode,
            custRelatedCompCode: values.custRelatedCompCode,
            custRelatedCompName: values.custRelatedCompName,
            rfId: custApplyInfo.custRfCompanyInfoAddDto.rfId,
            tid: custApplyInfo.custRequestFormInfoAddDto.tid?custApplyInfo.custRequestFormInfoAddDto.tid:wrapTid().tid, // 租户id,
            seq: index,
            ...commPara,
          });
        } else {
          tempList.push(item);
        }
      });
    }
    values.custRfCompRelatedCompDescAddDtoList = [...tempList];
    // eslint-disable-next-line consistent-return
    // 默认三证合一
    let threeCertificatesVerify = true;
    if (!isEmptyObj(custApplyInfo) && !isEmptyObj(custApplyInfo.custRfCompanyInfoAddDto)) {
      if (!isEmptyStr(custApplyInfo.custRfCompanyInfoAddDto.isCompanyCertMerge))
        threeCertificatesVerify = custApplyInfo.custRfCompanyInfoAddDto.isCompanyCertMerge;
    }
    if (!threeCertificatesVerify) {
      /**
       * 非三证合一时，需要填写
       *
       * 3、组织机构代码证扫描件必需录入
       * 4、税务代码证扫描件必需录入
       *
       * 否则提示不需要填写此信息
       * */

      // 3、组织机构代码证扫描件必需录入(检验)
      if (
        !(custRelatedCompAgencyScan instanceof File) &&
        isEmptyStr(values.custRelatedCompAgencyScan)
      ) {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScan.placeholder' })
        );
        return;
      }
      // 4、税务代码证扫描件必需录入(检验)
      if (!(custRelatedCompTaxScan instanceof File) && isEmptyStr(values.custRelatedCompTaxScan)) {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScan.placeholder' })
        );
        return;
      }
    }
    // eslint-disable-next-line consistent-return
    return values;
  };

  render() {
    const {
      formItemLayout,
      custRelatedCompLicenseScan, // 营业执照副件扫描件,单张
      custRelatedCompAgencyScan, // 组织机构代码证扫描件,单张
      custRelatedCompTaxScan, // 税务登记证扫描件,单张
      companyOtherScan, // 其他证件扫描件,单张
      companyOtherScanList, // 其他证件扫描件地址,单张
      isNotPersonal,
    } = this.state;
    const {
      modalUpkeepType: mut,
      custApplyInfo,
      tianYCInfo,
      form: { getFieldDecorator },
    } = this.props;

    // if (custApplyInfo.custRfCompanyInfoAddDto.custTypeCode === 'personal') {
    //   this.setState({ isNotPersonal: false });
    // } else {
    //   this.setState({ isNotPersonal: true });
    // }
    // 默认三证合一
    let threeCertificatesVerify = true;
    if (!isEmptyObj(custApplyInfo) && !isEmptyObj(custApplyInfo.custRfCompanyInfoAddDto)) {
      if (!isEmptyStr(custApplyInfo.custRfCompanyInfoAddDto.isCompanyCertMerge))
        threeCertificatesVerify = custApplyInfo.custRfCompanyInfoAddDto.isCompanyCertMerge;
    }
    // 关联企业社会信用代码 校验规则
    const affiliatedCompanyCodeRule = (() => {
      let rule = [];
      if (threeCertificatesVerify) {
        // 1、三证合一，请填写社会信用代码，长度18位
        const list = [
          {
            required: true,
            message: formatMessage({ id: 'form.custCompanyInfoManage.companyCodeMust' }),
          },
          {
            min: 18,
            max: 18,
            message: formatMessage({
              id: 'form.custCompanyInfoManage.companyCode-conformity',
            }),
          },
        ]; // 身份证校验
        rule = rule.concat(list);
      } else {
        // 2、非三证合一，请填写营业执照登记号码，长度15位；
        const list = [
          {
            required: true,
            message: formatMessage({
              id: 'form.custApplyManage.companyLicenseSn.placeholder',
            }),
          },
          {
            min: 15,
            max: 15,
            message: formatMessage({
              id: 'form.custCompanyInfoManage.companyLicenseSn-conformity',
            }),
          },
        ]; // 营业执照登记号码校验
        rule = rule.concat(list);
      }

      return rule;
    })();
    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custApplyManage.affiliatedCompanyName' })}
              >
                {getFieldDecorator('custRelatedCompName', {
                  rules: [
                    {
                      required: true,
                      message: formatMessage({
                        id: 'form.custApplyManage.affiliatedCompanyName.placeholder',
                      }),
                    },
                  ],
                })(<Input disabled={mut === 1} maxLength={100} />)}
              </Form.Item>
              {!isEmptyObj(tianYCInfo) && !isNotPersonal && (
                <Row>
                  <Col {...formItemLayout.labelCol}>&nbsp;</Col>
                  <Col {...formItemLayout.wrapperCol}>
                    <p className={styles.tianYCFont}>{tianYCInfo.name}</p>
                  </Col>
                </Row>
              )}
            </Row>

            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custApplyManage.affiliatedCompanyCode' })}
              >
                {getFieldDecorator('custRelatedCompCode', {
                  rules: affiliatedCompanyCodeRule,
                })(<Input disabled={mut === 1} maxLength={100} />)}
              </Form.Item>
              {!isEmptyObj(tianYCInfo) && !isNotPersonal && (
                <Row>
                  <Col {...formItemLayout.labelCol}>&nbsp;</Col>
                  <Col {...formItemLayout.wrapperCol}>
                    <p className={styles.tianYCFont}>{tianYCInfo.creditCode}</p>
                  </Col>
                </Row>
              )}
            </Row>

            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseScan' })}
              >
                <UploadSingleFile
                  uploadFile={custRelatedCompLicenseScan}
                  uploadFileCallback={this.uploadFileCallback}
                  uploadAccept="image/*"
                  uploadSign="custRelatedCompLicenseScan"
                  uploadIsDisable={mut === 1}
                />
              </Form.Item>
            </Row>
            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScan' })}
                required={!threeCertificatesVerify}
              >
                <UploadSingleFile
                  uploadFile={custRelatedCompAgencyScan}
                  uploadFileCallback={this.uploadFileCallback}
                  uploadAccept="image/*"
                  uploadSign="custRelatedCompAgencyScan"
                  uploadIsDisable={mut === 1}
                />
              </Form.Item>
            </Row>
            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScan' })}
                required={!threeCertificatesVerify}
              >
                <UploadSingleFile
                  // uploadFile={companyTaxScan || (mue ? mue.companyTaxScan : '')}
                  uploadFile={custRelatedCompTaxScan}
                  uploadFileCallback={this.uploadFileCallback}
                  uploadAccept="image/*"
                  uploadSign="custRelatedCompTaxScan"
                  uploadIsDisable={mut === 1}
                />
              </Form.Item>
            </Row>
            <Row>
              <Form.Item label={formatMessage({ id: 'form.custApplyManage.otherCardType' })}>
                <UploadSingleFiles
                  uploadFile={companyOtherScan}
                  uploadFileList={companyOtherScanList}
                  uploadFileCallback={this.uploadFileCallback}
                  uploadFileTranscoded={this.uploadFileTranscoded}
                  uploadAccept="image/*"
                  uploadSign="companyOtherScan"
                  uploadIsDisable={mut === 1}
                />
              </Form.Item>
            </Row>
          </Form>
        </div>
      </div>
    );
  }
}

export default connect(
  ({ parmcountrymanage, parmregionmanage, parmcurrencymanage, custapplymanage, user }) => ({
    parmCountryAll: parmcountrymanage.parmCountryAll || [], // 国别定义(所有)
    parmRegionAll: parmregionmanage.parmRegionAll || [], // 行政区划定义数据(所有)
    parmCurrencyAll: parmcurrencymanage.parmCurrencyAll || [], // 币种类型
    custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户申请单详情信息
    currentUser: user.currentUser || {},
    tianYCInfo: custapplymanage.tianYCInfo || {}, // 天眼查信息企业基本信息
  })
)(
  Form.create({
    onValuesChange({ dispatch, custApplyInfo }, changedValues) {
      dispatch({
        type: 'custapplymanage/changeCustApplyInfo',
        payload: {
          ...custApplyInfo,
          custRfCompRelatedCompAddDto: {
            ...custApplyInfo.custRfCompRelatedCompAddDto,
            ...changedValues,
          },
        },
      });
    },
    mapPropsToFields(props) {
      const {
        custApplyInfo: { custRfCompRelatedCompAddDto: mue },
      } = props;
      return {
        custRelatedCompName: Form.createFormField({
          // eslint-disable-next-line no-nested-ternary
          value: !isEmptyObj(mue) ? mue.custRelatedCompName : '',
        }),
        custRelatedCompCode: Form.createFormField({
          value: !isEmptyObj(mue) ? mue.custRelatedCompCode : '_NONE_',
        }),
      };
    },
  })(AffiliatedEnterprises)
);
