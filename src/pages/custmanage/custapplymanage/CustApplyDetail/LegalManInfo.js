import React from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import { Form, Input, Select, message, Row, Col } from 'antd';
import UploadSingleFile from '@/components/UploadBasic/UploadSingleFile';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import { fileRelationPathReg } from '@/utils/mdcutil';
import { isEmptyObj } from '@/utils/utils';
import request from '@/utils/request';
import { fileUrl, accessFileUrl } from '@/defaultSettings';
import styles from './index.less';

moment.locale('zh-cn');

const colLayout = {
  xs: 24,
  sm: 24,
  lg: 12,
  xl: 12,
  xxl: 12,
};
/**
 * 客户申请单信息维护组件
 */
@ValidationFormHoc
class LegalManInfo extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 14 },
        lg: { span: 9 },
        xl: { span: 7 },
        xxl: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
        lg: { span: 15 },
        xl: { span: 17 },
        xxl: { span: 20 },
      },
    },
    legalManCardFront: '', // 企业法人证件正面,单张
    legalManCardBack: '', // 企业法人证件反面,单张
  };

  /**
   * 初始化state中的文件相关参数值
   */
  componentDidMount() {
    const {
      custApplyInfo: { custRfCompanyInfoAddDto: mue },
      onRef,
      from,
    } = this.props;
    onRef(this);
    const formItemLayout =
      from === 'audit'
        ? {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 7 },
              xxl: { span: 6 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 17 },
              xxl: { span: 18 },
            },
          }
        : {
          labelCol: { xs: { span: 24 }, sm: { span: 7 }, xl: { span: 5 } },
          wrapperCol: { xs: { span: 24 }, sm: { span: 17 }, xl: { span: 19 } },
          };
    const formItemLayoutSome =
      from === 'audit'
        ? {
            labelCol: {
              xs: { span: 12 },
              sm: { span: 12 },
              lg: { span: 16 },
              xl: { span: 14 },
              xxl: { span: 12 },
            },
            wrapperCol: {
              xs: { span: 12 },
              sm: { span: 12 },
              lg: { span: 8 },
              xl: { span: 10 },
              xxl: { span: 12 },
            },
          }
        : {
            labelCol: { xs: { span: 24 }, sm: { span: 14 }, xl: { span: 10 } },
            wrapperCol: { xs: { span: 24 }, sm: { span: 10 }, xl: { span: 14 } },
        };
        const formItemLayoutSomeRight =
        from === 'audit'
          ? {
              labelCol: {
                xs: { span: 0 },
                sm: { span: 0 },
                lg: { span: 0 },
                xl: { span: 0 },
                xxl: { span: 0 },
              },
              wrapperCol: {
                xs: { span: 24 },
                sm: { span: 24 },
                lg: { span: 24 },
                xl: { span: 24 },
                xxl: { span: 24 },
              },
            }
          : {
              labelCol: { xs: { span: 0 }, sm: { span: 0 }, xl: { span: 0 } },
              wrapperCol: { xs: { span: 24 }, sm: { span: 24 }, xl: { span: 24 } },
            };
    this.setState({
      formItemLayout,
      formItemLayoutSome,
      formItemLayoutSomeRight,
      legalManCardFront:
        mue && mue.legalManCardFront ? `${accessFileUrl}/${mue.legalManCardFront}` : '', // 企业法人证件正面,单张
      legalManCardBack:
        mue && mue.legalManCardBack ? `${accessFileUrl}/${mue.legalManCardBack}` : '', // 企业法人证件反面,单张
    });
  }

  /**
   * 文件上传UploadFile组件回调函数
   */
  uploadFileCallback = (uploadSign, newFile) => {
    const obj = {};
    if ((newFile && newFile.type.indexOf('image') >= 0) || newFile === '') {
      obj[uploadSign] = newFile;
      this.setState(obj);
    } else if (newFile) {
      message.warn(formatMessage({ id: 'form.common.UploadTypeError' }));
    }
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      legalManCardFront, // 企业法人证件正面,单张
      legalManCardBack, // 企业法人证件反面,单张
    } = this.state;
    const {
      form: { getFieldsValue },
      // custApplyInfo: mue,
      validationForm,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();

    // 上传-企业法人证件正面,单张
    if (legalManCardFront instanceof File) {
      const formData = new FormData();
      formData.append('file', legalManCardFront);
      const legalManCardFrontRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (legalManCardFrontRes) {
        values.legalManCardFront = legalManCardFrontRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.legalManCardFrontResUploadError' })
        );
        return;
      }
      // console.log(legalManCardFrontRes);
    } else {
      values.legalManCardFront = legalManCardFront.replace(fileRelationPathReg, '');
      // 替换附加路径
      values.legalManCardFront = values.legalManCardFront.replace(`${accessFileUrl}/`, '');
    }

    // 上传-企业法人证件反面,单张
    if (legalManCardBack instanceof File) {
      const formData = new FormData();
      formData.append('file', legalManCardBack);
      const legalManCardBackRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (legalManCardBackRes) {
        values.legalManCardBack = legalManCardBackRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.legalManCardBackResUploadError' })
        );
        return;
      }
      // console.log(legalManCardBackRes);
    } else {
      values.legalManCardBack = legalManCardBack.replace(fileRelationPathReg, '');
      // 替换附加路径
      values.legalManCardBack = values.legalManCardBack.replace(`${accessFileUrl}/`, '');
    }
    // eslint-disable-next-line consistent-return
    return values;
  };

  render() {
    const {
      formItemLayout,
      formItemLayoutSome,
      formItemLayoutSomeRight,
      legalManCardFront, // 企业法人证件正面,单张
      legalManCardBack, // 企业法人证件反面,单张
    } = this.state;
    const {
      modalUpkeepType: mut,
      // modalUpkeepEntity: mue,
      form: { getFieldDecorator },
      parmPublicParameterOfCustUserCardTypeAll: pppocucta, // 公用参数定义数据[用户证件类型数据所有]
      tianYCInfo,
      custApplyInfo: { custRfCompanyInfoAddDto: mue },
    } = this.props;

    // 身份证校验,证件类型为身份证时进行校验；
    const IdCardRule =
      mue.legalManCardType === 'IDCARD'
        ? [{
            pattern: new RegExp(
              /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/,
              'g'
            ),
            message: formatMessage({ id: 'form.custApplyManage.IDCard.correct.placeholder' }),
          }]
        : [];

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Row>
              <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.legalManName' })}>
                {getFieldDecorator('legalManName')(<Input disabled={mut === 1} maxLength={25} />)}
              </Form.Item>
              {!isEmptyObj(tianYCInfo) && (
                <Row>
                  <Col {...formItemLayout.labelCol}>&nbsp;</Col>
                  <Col {...formItemLayout.wrapperCol}>
                    <p className={styles.tianYCFont}>{tianYCInfo.legalPersonName}</p>
                  </Col>
                </Row>
              )}
            </Row>

            <Row gutter={{ xs: 4, sm: 8, md: 12 }}>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSome}
                  label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardNo' })}
                >
                  {getFieldDecorator('legalManCardType')(
                    <Select disabled={mut === 1}>
                      {pppocucta.map(item => (
                        <Select.Option key={item.parmCode} value={item.parmCode}>
                          {item.parmValue}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col {...colLayout}>
                <Form.Item {...formItemLayoutSomeRight}>
                  {getFieldDecorator('legalManCardNo', {
                    rules: IdCardRule,
                  })(<Input disabled={mut === 1} maxLength={25} />)}
                </Form.Item>
              </Col>
            </Row>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardFront' })}
            >
              <UploadSingleFile
                // uploadFile={legalManCardFront || (mue ? mue.legalManCardFront : '')}
                uploadFile={legalManCardFront}
                uploadFileCallback={this.uploadFileCallback}
                uploadAccept="image/*"
                uploadSign="legalManCardFront"
                uploadIsDisable={mut === 1}
              />
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardBack' })}>
              <UploadSingleFile
                // uploadFile={legalManCardBack || (mue ? mue.legalManCardBack : '')}
                uploadFile={legalManCardBack}
                uploadFileCallback={this.uploadFileCallback}
                uploadAccept="image/*"
                uploadSign="legalManCardBack"
                uploadIsDisable={mut === 1}
              />
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}

export default connect(({ parmpublicparametermanage,custapplymanage }) => ({
  parmPublicParameterOfCustUserCardTypeAll:
    parmpublicparametermanage.parmPublicParameterOfCustUserCardTypeAll, // 根据公用参数类型定义代码[cust_user_card_type]查询匹配到的公用参数定义数据[用户证件类型数据所有]
    tianYCInfo: custapplymanage.tianYCInfo || {}, // 天眼查信息企业基本信息
}))(
  Form.create({
    onValuesChange({ dispatch, custApplyInfo }, changedValues) {
      dispatch({
        type: 'custapplymanage/changeCustApplyInfo',
        payload: {
          ...custApplyInfo,
          custRfCompanyInfoAddDto: {
            ...custApplyInfo.custRfCompanyInfoAddDto,
            ...changedValues,
          },
        },
      });
    },
    mapPropsToFields(props) {
      const {
        custApplyInfo: { custRfCompanyInfoAddDto: mue },
      } = props;

      return {
        legalManName: Form.createFormField({ value: mue ? mue.legalManName : '' }),
        legalManCardType: Form.createFormField({ value: mue ? mue.legalManCardType : '' }),
        legalManCardNo: Form.createFormField({ value: mue ? mue.legalManCardNo : '' }),
        // legalManCardFront: Form.createFormField({ value: mue ? mue.legalManCardFront : '' }),
        // legalManCardBack: Form.createFormField({ value: mue ? mue.legalManCardBack : '' }),
        enable: Form.createFormField({ value: mue ? mue.enable === 1 : true }),
      };
    },
  })(LegalManInfo)
);
