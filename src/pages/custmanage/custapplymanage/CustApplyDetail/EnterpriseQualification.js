import React from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import uuid from 'uuid';
import {
  Form,
  Input,
  Checkbox,
  Select,
  TreeSelect,
  message,
  Row,
  Col,
  Cascader,
  InputNumber,
  Icon,
} from 'antd';
import {
  formatParmRegionToAntdCascaderCmp,
  formatParmRegionToAntdTreeSelectCmp,
  verifyCustApplyUnique,
  fileRelationPathReg,
  wrapTid,
} from '@/utils/mdcutil';
import UploadSingleFile from '@/components/UploadBasic/UploadSingleFile';
import UploadSingleFiles from '@/components/UploadBasic/UploadSingleFiles'; // 上传多个文件
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import request from '@/utils/request';
import { fileUrl, accessFileUrl } from '@/defaultSettings';
import { isEmptyObj, isEmptyStr } from '@/utils/utils';
import styles from './index.less';

moment.locale('zh-cn');

const colLayout = {
  xs: 24,
  sm: 24,
  lg: 12,
  xl: 12,
  xxl: 12,
};
// const formItemLayoutSome = {
//   labelCol: { xs: { span: 12 }, sm: { span: 12 }, lg: { span: 16 }, xl: { span: 14 }, xxl: { span: 12 } },
//   wrapperCol: { xs: { span: 12 }, sm: { span: 12 }, lg: { span: 8 }, xl: { span: 10 }, xxl: { span: 12 } },
// };

// const formItemLayoutIsCompanyCode = {
//   labelCol: { xs: { span: 24 }, sm: { span: 14 }, xl: { span: 14 } },
//   wrapperCol: { xs: { span: 24 }, sm: { span: 10 }, xl: { span: 10 } },
// };

/**
 * 客户申请单信息维护组件
 */
@ValidationFormHoc
class EnterpriseQualification extends React.PureComponent {
  state = {
    formItemLayout: {},
    formItemLayoutSome: {},
    companyLicenseScan: '', // 营业执照副件扫描件,单张
    companySgencyScan: '', // 组织机构代码证扫描件,单张
    companyTaxScan: '', // 税务登记证扫描件,单张
    companyOtherScan: '', // 其他证件扫描件,单张
    companyOtherScanList: [], // 其他证件扫描件地址,多张
    companyOtherScanObjList: [], // 其他证件扫描件所有信息,多张
    threeCertificatesVerify: 1, // 是否三证合一修改必填状态
    companyCodeVerify: false, // 社会信用码是否重复，true：重复
    isNotPersonal: true, // 是否是个人，默认不是，true
  };

  /**
   * 初始化
   */
  componentDidMount() {
    const { onRef } = this.props;
    onRef(this);
    // 初始化state中的图片文件相关参数值
    const {
      custApplyInfo: { custRfCompanyInfoAddDto: mue },
      from,
    } = this.props;
    const companyOtherScanList = []; // 其他证件扫描件,多张
    let companyOtherScanObjList = []; // 其他证件扫描件,多张
    if (mue && mue.custRfCompanyDescAddDtoList.length > 0) {
      mue.custRfCompanyDescAddDtoList.forEach(val => {
        companyOtherScanList.push(val.custPackingListAddr);
      });
      companyOtherScanObjList = [...mue.custRfCompanyDescAddDtoList];
    }
    const formItemLayout =
      from === 'audit'
        ? {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 7 },
              xxl: { span: 6 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 17 },
              xxl: { span: 18 },
            },
          }
        : {
            labelCol: { xs: { span: 24 }, sm: { span: 7 }, xl: { span: 5 } },
            wrapperCol: { xs: { span: 24 }, sm: { span: 17 }, xl: { span: 19 } },
          };
    const formItemLayoutSome =
      from === 'audit'
        ? {
            labelCol: {
              xs: { span: 12 },
              sm: { span: 12 },
              lg: { span: 16 },
              xl: { span: 14 },
              xxl: { span: 12 },
            },
            wrapperCol: {
              xs: { span: 12 },
              sm: { span: 12 },
              lg: { span: 8 },
              xl: { span: 10 },
              xxl: { span: 12 },
            },
          }
        : {
            labelCol: { xs: { span: 24 }, sm: { span: 14 }, xl: { span: 10 } },
            wrapperCol: { xs: { span: 24 }, sm: { span: 10 }, xl: { span: 14 } },
          };
    const formItemLayoutSomeRight =
      from === 'audit'
        ? {
            labelCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 10 },
              xxl: { span: 8 },
            },
            wrapperCol: {
              xs: { span: 24 },
              sm: { span: 24 },
              lg: { span: 12 },
              xl: { span: 14 },
              xxl: { span: 16 },
            },
          }
        : {
            labelCol: { xs: { span: 24 }, sm: { span: 14 }, xl: { span: 10 } },
            wrapperCol: { xs: { span: 24 }, sm: { span: 10 }, xl: { span: 14 } },
          };
    // 增加isNotPersonal 判断

    this.setState({
      formItemLayout,
      formItemLayoutSome,
      formItemLayoutSomeRight,
      companyLicenseScan:
        mue && !isEmptyStr(mue.companyLicenseScan)
          ? `${accessFileUrl}/${mue.companyLicenseScan}`
          : '', // 营业执照副件扫描件,单张
      companySgencyScan:
        mue && !isEmptyStr(mue.companySgencyScan)
          ? `${accessFileUrl}/${mue.companySgencyScan}`
          : '', // 组织机构代码证扫描件,单张
      companyTaxScan:
        mue && !isEmptyStr(mue.companyTaxScan) ? `${accessFileUrl}/${mue.companyTaxScan}` : '', // 税务登记证扫描件,单张
      companyOtherScanList, // 其他证件扫描件,多张
      companyOtherScanObjList,
      threeCertificatesVerify: mue && mue.isCompanyCertMerge ? mue.isCompanyCertMerge : 1,
      isNotPersonal: !(mue && mue.custTypeCode === 'personal'),
    });
  }

  // 监听 custApplyInfo.custTypeCode
  componentWillReceiveProps(nextProps) {
    // console.log('nextProps', nextProps);
    const { custApplyInfo } = this.props;
    if (JSON.stringify(custApplyInfo) !== JSON.stringify(nextProps.custApplyInfo)) {
      // 根据selectedRfId查询申请单详情
      if (!isEmptyObj(nextProps.custApplyInfo.custRfCompanyInfoAddDto)) {
        if (nextProps.custApplyInfo.custRfCompanyInfoAddDto.custTypeCode === 'personal') {
          this.setState({ isNotPersonal: false });
        } else if (
          nextProps.custApplyInfo.custRfCompanyInfoAddDto.custTypeCode === 'enterprises' ||
          nextProps.custApplyInfo.custRfCompanyInfoAddDto.custTypeCode === 'business'
        ) {
          this.setState({ isNotPersonal: true });
        }
      }
    }
  }

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      if (modalVisibleOnChange) modalVisibleOnChange(false);
    } else {
      message.info(formatMessage({ id: 'form.common.optionError' }));
    }
  };

  /**
   * 文件上传UploadFile组件回调函数
   */
  uploadFileCallback = (uploadSign, newFile) => {
    const obj = {};
    if ((newFile && newFile.type.indexOf('image') >= 0) || newFile === '') {
      obj[uploadSign] = newFile;
      this.setState(obj);
    } else if (newFile) {
      message.warn(formatMessage({ id: 'form.common.UploadTypeError' }));
    }
  };

  // 上传多幅图片的，将上传的图片存入本地的state中
  uploadFileTranscoded = async (data, type) => {
    const { companyOtherScanList, companyOtherScanObjList } = this.state;
    if (type === 1) {
      // 上传，新增图片文件对象在通过接口保存在数据库中
      const formData = new FormData();
      formData.append('file', data);
      const res = await request.post(`${fileUrl}/bootstrap/fileobject/upload/single`, {
        data: formData,
      });
      if (res) {
        companyOtherScanList.push(res.url);
        companyOtherScanObjList.push(res);
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseSnUploadError' })
        );
        return;
      }
      this.setState({
        companyOtherScanList: [...companyOtherScanList],
        companyOtherScanObjList: [...companyOtherScanObjList],
      });
    } else if (type === 2) {
      // 删除图片文件，则删除缓存list的文件，data对应为文件所在list的位置
      companyOtherScanList.splice(data, 1);
      companyOtherScanObjList.splice(data, 1); // 暂时先删除缓存中的信息
      this.setState({
        companyOtherScanList: [...companyOtherScanList],
        companyOtherScanObjList: [...companyOtherScanObjList],
      });
    }
  };

  /**
   * 三证合一修改必填状态
   */
  threeCertificates = e => {
    // 当客户类型为-企事业单位、工商个体，且非三证合一时，需要填写，否则提示不需要填写此信息
    const { custApplyInfo } = this.props;
    if (custApplyInfo.custRfCompanyInfoAddDto.custTypeCode !== 'personal' && e.target.checked) {
      this.setState({ threeCertificatesVerify: 1 });
    } else {
      this.setState({ threeCertificatesVerify: 0 });
    }
  };

  /**
   *格式化金额，按千分位加逗号，例如：10，000，000
   */

  // numFormat = inputObj => {
  //   const { value } = inputObj.input;
  //   const { form } = this.props;
  //   const val =
  //     value.toString().indexOf('.') !== -1
  //       ? value.toLocaleString()
  //       : value.toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
  //   form.setFieldsValue({
  //     custRegisteredCapital: val,
  //   });
  // };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      companyLicenseScan, // 营业执照副件扫描件,单张
      companySgencyScan, // 组织机构代码证扫描件,单张
      companyTaxScan, // 税务登记证扫描件,单张
      companyOtherScanObjList, // 其他证件扫描件所有信息,多张
      companyCodeVerify, // 社会信用码是否重复，true：重复
      isNotPersonal,
    } = this.state;
    const {
      form: { getFieldsValue },
      validationForm,
      custApplyInfo,
      currentUser,
    } = this.props;

    if (companyCodeVerify) {
      // 社会信用码是否重复，true：重复
      message.warn(formatMessage({ id: 'form.custApplyManage.companyCodeExist' }));
      return;
    }
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();

    // 国别编码，缺省值为"CN"
    // eslint-disable-next-line no-unused-expressions
    values.companyCountryCode ? null : (values.companyCountryCode = 'CN');
    // eslint-disable-next-line no-unused-expressions
    // values.hasCompanyCode ? (values.hasCompanyCode = 1) : (values.hasCompanyCode = 0);
    values.hasCompanyCode = 1;
    // eslint-disable-next-line no-unused-expressions
    values.isCompanyCertMerge ? (values.isCompanyCertMerge = 1) : (values.isCompanyCertMerge = 0);

    // if (values.hasCompanyCode === 0) {
    //   // 1、 0-无对应公司(即无社会统一信用代码,无对应信用代码时企业代码填写"_NONE_"）
    //   values.companyCode = '_NONE_';
    //   // 2、无对应信用代码时企业名称填写客户名称
    //   values.companyName = values.custName;
    // }
    // else {
    //   // 1-有对应公司；缺省为1
    //   // 社会统一信用代码(校验)
    //   // eslint-disable-next-line no-lonely-if
    //   if (isNotPersonal && values.companyCode.length !== 18) {
    //     message.warn(formatMessage({ id: 'form.custCompanyInfoManage.companyCode-conformity' }));
    //     return;
    //   }
    // }

    // if (values.isCompanyCertMerge !== 1 && isNotPersonal) {
    //   // 是三证合一
    //   // 1、营业执照注册号：三证合一后，为社会信用代码，长度18位，
    //   values.companyLicenseSn = values.companyCode;
    // }
    values.companyLicenseSn = values.companyCode;
    // 以上必填写数据都验证通过之后,先上传图片并取得图片地址,最后再写数据到数据库
    // 上传-营业执照副件扫描件,单张
    if (companyLicenseScan instanceof File) {
      const formData = new FormData();
      formData.append('file', companyLicenseScan);
      const companyLicenseScanRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (companyLicenseScanRes) {
        values.companyLicenseScan = companyLicenseScanRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseSnUploadError' })
        );
        return;
      }
      // console.log(companyLicenseScanRes);
    } else {
      values.companyLicenseScan = companyLicenseScan.replace(fileRelationPathReg, '');
      // 替换附加路径
      values.companyLicenseScan = values.companyLicenseScan.replace(`${accessFileUrl}/`, '');
    }

    // 上传-组织机构代码证扫描件,单张
    if (companySgencyScan instanceof File) {
      const formData = new FormData();
      formData.append('file', companySgencyScan);
      const companySgencyScanRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (companySgencyScanRes) {
        values.companySgencyScan = companySgencyScanRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScanResUploadError' })
        );
        return;
      }
      // console.log(companySgencyScanRes);
    } else {
      values.companySgencyScan = companySgencyScan.replace(fileRelationPathReg, '');
      // 替换附加路径
      values.companySgencyScan = values.companySgencyScan.replace(`${accessFileUrl}/`, '');
    }

    // 上传-税务登记证扫描件,单张
    if (companyTaxScan instanceof File) {
      const formData = new FormData();
      formData.append('file', companyTaxScan);
      const companyTaxScanRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (companyTaxScanRes) {
        values.companyTaxScan = companyTaxScanRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScanResUploadError' })
        );
        return;
      }
      // console.log(companyTaxScanRes);
    } else {
      values.companyTaxScan = companyTaxScan.replace(fileRelationPathReg, '');
      // 替换附加路径
      values.companyTaxScan = values.companyTaxScan.replace(`${accessFileUrl}/`, '');
    }

    // 上传-企业其他证件正面,多张
    const tempList = [];
    const commPara = {
      createUid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUid)
        ? custApplyInfo.custRequestFormInfoAddDto.createUid
        : currentUser.id,
      createUcode: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.tid)
        ? custApplyInfo.custRequestFormInfoAddDto.tid
        : wrapTid().tid, // 租户id,
      createUname: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.createUname)
        ? custApplyInfo.custRequestFormInfoAddDto.createUname
        : currentUser.name,
      modifyUid: currentUser.id,
      modifyUcode: wrapTid().tid, // 租户id,
      modifyUname: currentUser.name,
      tid: !isEmptyStr(custApplyInfo.custRequestFormInfoAddDto.tid)
        ? custApplyInfo.custRequestFormInfoAddDto.tid
        : wrapTid().tid, // 租户id,
    };
    if (companyOtherScanObjList.length > 0) {
      companyOtherScanObjList.forEach((item, index) => {
        if (item.url) {
          // 新增
          tempList.push({
            custPackingListAddr: item.url, // 其他证件扫描件存储地址
            custPackingListCode: item.id, // 其他证件扫描件文件编码
            custPackingListName: item.originalName, // 其他证件扫描件文件名称
            custCode: custApplyInfo.custRfCompanyInfoAddDto.custCode,
            rfId: custApplyInfo.custRfCompanyInfoAddDto.rfId,
            tid: custApplyInfo.custRequestFormInfoAddDto.tid
              ? custApplyInfo.custRequestFormInfoAddDto.tid
              : wrapTid().tid, // 租户id,
            seq: index,
            ...commPara,
          });
        } else {
          tempList.push(item);
        }
      });
    }
    values.custRfCompanyDescAddDtoList = [...tempList];
    // 验证，经营范围前端是非必填的，数据库是必填的，若为空默认为空字符
    // if (isEmptyStr(values.companyBusiness)) values.companyBusiness = " ";

    if (values.isCompanyCertMerge !== 1 && isNotPersonal) {
      /**
       * 当客户类型为-企事业单位、工商个体，且非三证合一时，需要填写
       *
       * 1、组织机构代码必需录入
       * 2、税务代码必需录入
       * 3、组织机构代码证扫描件必需录入
       * 4、税务代码证扫描件必需录入
       *
       * 否则提示不需要填写此信息
       * */

      // 1、组织机构代码必需录入(检验)
      if (isEmptyStr(values.companySgencySn)) {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyAgencySn-conformity' })
        );
        return;
      }
      // 2、税务代码必需录入(检验)
      if (isEmptyStr(values.companyTaxSn)) {
        message.warn(formatMessage({ id: 'form.custCompanyInfoManage.companyTaxSn-conformity' }));
        return;
      }
      // 3、组织机构代码证扫描件必需录入(检验)
      if (!(companySgencyScan instanceof File) && isEmptyStr(values.companySgencyScan)) {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScan.placeholder' })
        );
        return;
      }
      // 4、税务代码证扫描件必需录入(检验)
      if (!(companyTaxScan instanceof File) && isEmptyStr(values.companyTaxScan)) {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScan.placeholder' })
        );
        return;
      }
      // 5、营业执照注册号录入(可选),三证合一前：15位, 如果有值则必须15位,否则就不要填写数据
      if (
        values.companyLicenseSn !== '' &&
        values.companyLicenseSn !== null &&
        values.companyLicenseSn !== undefined &&
        values.companyLicenseSn.length !== 15
      ) {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseSn-conformity' })
        );
        return;
      }
    }
    // eslint-disable-next-line consistent-return
    return values;
  };

  /**
   * 验证客户名称唯一性
   * tid,custName必传
   */
  verifyUnquieCustName = async custNameInput => {
    const {
      custApplyInfo: { custRfCompanyInfoAddDto: mue },
    } = this.props;
    const { value } = custNameInput.input;
    if (value === null || value.length === 0) return;
    const params = { custName: value };
    const res = await verifyCustApplyUnique(
      '/mdc/cust/custrfnfoentiremanage/verifyNoReviewUnique',
      params
    );
    let status = true;
    if (res && res.rfId !== mue.rfId) {
      status = false;
    }
    if (!status) {
      message.warn(formatMessage({ id: 'form.custCompanyInfoManage.custNameExist' }));
      // custNameInput.focus();
    }
  };

  /**
   * 修改注册地址执行的操作
   */
  changeRegisterAddress = value => {
    // console.log(value)
    const {
      dispatch,
      form: { setFieldsValue },
    } = this.props;
    dispatch({
      type: 'custcompanyinfomanage/getParmRegionByReginCodesUsing',
      payload: { reginCodeList: value },
      callback: res => {
        setFieldsValue({ companyRegionCode: res.data.regionCode });
      },
    });
  };

  // // 操作其他证件图片上传
  // delOtherScan = index => {
  //   const { companyOtherScanList } = this.state;
  //   companyOtherScanList.splice(index, 1);
  //   this.setState({
  //     companyOtherScanList: [...companyOtherScanList],
  //   });
  // };

  // addOtherScan = () => {
  //   // console.log(this);
  //   const { companyOtherScanList } = this.state;
  //   companyOtherScanList.push({
  //     uploadFile: {}, // 上传文件所在
  //     custPackingListAddr: '', // 其他证件扫描件存储地址
  //     custPackingListCode: '', // 其他证件扫描件文件编码
  //     custPackingListName: '', // 其他证件扫描件文件名称
  //   });
  //   this.setState({
  //     companyOtherScanList: [...companyOtherScanList],
  //   });
  // };

  render() {
    const {
      formItemLayout,
      formItemLayoutSome,
      formItemLayoutSomeRight,
      companyLicenseScan, // 营业执照副件扫描件,单张
      companySgencyScan, // 组织机构代码证扫描件,单张
      companyTaxScan, // 税务登记证扫描件,单张
      companyOtherScan, // 其他证件扫描件,单张
      companyOtherScanList, // 其他证件扫描件,多张
      threeCertificatesVerify, // 三证合一必填状态判断
      isNotPersonal,
    } = this.state;
    const {
      modalUpkeepType: mut,
      form: { getFieldDecorator },
      parmCountryAll: pca, // 国别定义(所有)
      parmRegionAll: pra, // 行政区划定义数据(所有)
      parmCurrencyAll: currencyType, // 币种类型
      custApplyInfo: { custRfCompanyInfoAddDto: mue },
      tianYCInfo,
    } = this.props;

    // 企业社会信用代码 校验规则
    const companyCodeRule = (() => {
      let rule = [];
      if (!isEmptyObj(mue)) {
        // if (mue.hasCompanyCode) {
        // 使用社会信用统一码
        if (mue.custTypeCode === 'personal') {
          // 客户类型为个人时，需要填写身份证
          const list = [
            {
              required: true,
              message: formatMessage({ id: 'form.custApplyManage.IDCard.placeholder' }),
            },
            {
              pattern: new RegExp(
                /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/,
                'g'
              ),
              message: formatMessage({ id: 'form.custApplyManage.IDCard.correct.placeholder' }),
            },
          ]; // 身份证校验
          rule = rule.concat(list);
        } else {
          // 客户类型为非个人时：
          // eslint-disable-next-line no-lonely-if
          if (mue.isCompanyCertMerge) {
            // 1、三证合一，请填写社会信用代码，长度18位
            const list = [
              {
                required: true,
                message: formatMessage({ id: 'form.custCompanyInfoManage.companyCodeMust' }),
              },
              {
                min: 18,
                max: 18,
                message: formatMessage({
                  id: 'form.custApplyManage.companyCode.placeholder',
                }),
              },
            ];
            rule = rule.concat(list);
          } else {
            // 2、非三证合一，请填写营业执照登记号码，长度15位；
            const list = [
              {
                required: true,
                message: formatMessage({
                  id: 'form.custApplyManage.companyLicenseSn.placeholder',
                }),
              },
              {
                min: 15,
                max: 15,
                message: formatMessage({
                  id: 'form.custApplyManage.companyCode.companyLicenseSn',
                }),
              },
            ]; // 营业执照登记号码校验
            rule = rule.concat(list);
          }
        }
        // } else {
        //   // 不使用社会信用统一码
        //   const list = [
        //     {
        //       required: false,
        //     },
        //   ]; // 营业执照登记号码校验
        //   rule = rule.concat(list);
        // }
      }

      return rule;
    })();
    // 组织机构代码 校验规则
    const companyAgencySnRule = (() => {
      if (!isEmptyObj(mue)) {
        return isNotPersonal && mue.isCompanyCertMerge === 0
          ? [
              {
                required: true,
                message: formatMessage({ id: 'form.custApplyManage.companyAgencySn.placeholder' }),
              },
            ]
          : '';
      }
      return '';
    })();

    // 税务代码 校验规则
    const companyTaxSnRule = (() => {
      if (!isEmptyObj(mue)) {
        return isNotPersonal && mue.isCompanyCertMerge === 0
          ? [
              {
                required: true,
                message: formatMessage({ id: 'form.custApplyManage.companyTaxSn.placeholder' }),
              },
            ]
          : '';
      }
      return '';
    })();

    /**
     * 验证社会信息代码唯一性
     * companyCode必传
     * 规则：
     * 1、客户类型-企事业单位、工商个体场景校验：
     *【三证合一：社会信用代码，长度18位】
     *
     *2、客户类型，针对客户类型为个人场景校验：
     *【社会信用代码为身份证号码，格式必须有效正确】
     *
     *3、经营单位类型代码为 NKA、KW、KA、STORE、OTHER 时，场景校验：
     *【企业统一社会信用代码不可重复】
     */
    const verifyUnquieCompanyCode = async companyCodeInput => {
      // console.log(companyCodeInput);
      const whiteList = ['NKA', 'KW', 'KA', 'STORE', 'OTHER'];
      if (whiteList.includes(mue.operUnitTypeCode)) {
        // 校验社会信用代码不可重复
        const { value } = companyCodeInput.input;
        if (value === null || value.length === 0) return;
        const params = { companyCode: value };
        let companyVerify = false;
        const res = await verifyCustApplyUnique(
          '/mdc/cust/custrfnfoentiremanage/verifyNoReviewUnique',
          params
        );
        let status = true;
        if (res && res.rfId !== mue.rfId) {
          status = false;
        }
        if (!status) {
          message.warn(formatMessage({ id: 'form.custApplyManage.companyCodeExist' }));
          // companyCodeInput.focus();
          companyVerify = true;
        }
        this.setState({
          companyCodeVerify: companyVerify,
        });
      }
    };

    // 经营单位类型代码改变时，触发
    // eslint-disable-next-line no-unused-vars
    (() => {
      const whiteList = ['NKA', 'KW', 'KA', 'STORE', 'OTHER'];
      if (!whiteList.includes(mue.operUnitTypeCode)) {
        this.setState({
          companyCodeVerify: false,
        });
      }
      // verifyUnquieCompanyCode(this.companyCodeInput);
    })();

    // // 其他图片上传
    // const otherScanList = () => {
    //   const children = [];
    //   companyOtherScanList.forEach((item, index) => {
    //     children.push(
    //       <Col span={2}>
    //         <Row gutter={8}>
    //           <Col span={18}>
    //             <UploadSingleFile
    //               uploadFile={item.custPackingListAddr}
    //               uploadFileCallback={this.uploadFileCallback}
    //               uploadAccept="image/*"
    //               uploadSign={`companyOtherScan${index}`}
    //               uploadIsDisable={mut === 1}
    //             />
    //           </Col>
    //           <Col span={6}>
    //             <Icon
    //               type="minus-circle"
    //               className={styles.minusPos}
    //               onClick={() => {
    //                 this.delOtherScan(index);
    //               }}
    //             />
    //           </Col>
    //         </Row>
    //       </Col>
    //     );
    //   });
    //   return children;
    // };

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Row>
              <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.companyName' })}>
                {getFieldDecorator('companyName', {
                  rules: [
                    {
                      required: true,
                      message: formatMessage({ id: 'form.custCompanyInfoManage.companyNameMust' }),
                    },
                  ],
                })(<Input disabled={mut === 1} maxLength={100} />)}
              </Form.Item>
              {!isEmptyObj(tianYCInfo) && isNotPersonal && (
                <Row>
                  <Col {...formItemLayout.labelCol}>&nbsp;</Col>
                  <Col {...formItemLayout.wrapperCol}>
                    <p className={styles.tianYCFont}>{tianYCInfo.name}</p>
                  </Col>
                </Row>
              )}
            </Row>

            <Row>
              <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.custShortName' })}>
                {getFieldDecorator('custShortName')(<Input disabled={mut === 1} maxLength={50} />)}
              </Form.Item>
            </Row>

            <Row gutter={{ xs: 4, sm: 8, md: 12 }}>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSome}
                  label={formatMessage({ id: 'form.custApplyManage.registeredCapital' })}
                >
                  {getFieldDecorator('custRegisteredCapital')(
                    <InputNumber
                      disabled={mut === 1}
                      maxLength={20}
                      formatter={value => ` ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      parser={value => value.replace(/\$\s?|(,*)/g, '')}
                      min={1}
                      precision={0}
                      style={{ width: '100%' }}
                    />
                    // <Input
                    //   disabled={mut === 1}
                    //   maxLength={50}
                    //   // eslint-disable-next-line no-return-assign
                    //   ref={input => (this.custCapitalInput = input)}
                    //   onBlur={() => this.numFormat(this.custCapitalInput)}
                    // />
                  )}
                </Form.Item>
                {!isEmptyObj(tianYCInfo) && isNotPersonal && (
                  <Row>
                    <Col {...formItemLayoutSome.labelCol}>&nbsp;</Col>
                    <Col {...formItemLayoutSome.wrapperCol}>
                      <p className={styles.tianYCFont}>{tianYCInfo.regCapital}</p>
                    </Col>
                  </Row>
                )}
              </Col>
              <Col {...colLayout}>
                <Form.Item {...formItemLayoutSomeRight}>
                  {getFieldDecorator('custCurrencyCode')(
                    <Select disabled={mut === 1}>
                      {currencyType.map(item => (
                        <Select.Option key={item.currencyCode} value={item.currencyCode}>
                          {item.currencyName}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row>
              {/* <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSome}
                  label={formatMessage({ id: 'form.custApplyManage.isCompanyCode' })}
                >
                  {getFieldDecorator('hasCompanyCode', {
                    valuePropName: 'checked',
                    rules: [
                      {
                        required: true,
                        message: formatMessage({
                          id: 'form.custApplyManage.isCompanyCode.placeholder',
                        }),
                      },
                    ],
                  })(<Checkbox disabled />)}
                </Form.Item>
              </Col> */}
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSome}
                  label={formatMessage({
                    id: 'form.custCompanyInfoManage.isCompanyCertMergeMust',
                  })}
                >
                  {getFieldDecorator('isCompanyCertMerge', {
                    valuePropName: 'checked',
                    rules: [{ required: false }],
                  })(<Checkbox disabled={mut === 1} onChange={this.threeCertificates} />)}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.companyCode' })}>
                {getFieldDecorator('companyCode', {
                  rules: companyCodeRule,
                })(
                  <Input
                    disabled={mut === 1}
                    maxLength={18}
                    // eslint-disable-next-line no-return-assign
                    ref={input => (this.companyCodeInput = input)}
                    onBlur={() => verifyUnquieCompanyCode(this.companyCodeInput)}
                  />
                )}
              </Form.Item>
              {!isEmptyObj(tianYCInfo) && isNotPersonal && (
                <Row>
                  <Col {...formItemLayout.labelCol}>&nbsp;</Col>
                  <Col {...formItemLayout.wrapperCol}>
                    <p className={styles.tianYCFont}>{tianYCInfo.creditCode}</p>
                  </Col>
                </Row>
              )}
            </Row>

            <Row>
              <Col {...formItemLayout.labelCol}>&nbsp;</Col>
              <Col {...formItemLayout.wrapperCol}>
                <p>
                  <Icon type="exclamation-circle" />
                  &nbsp;
                  <span>
                    客户性质-普通企业&微小企业，需要填写社会信用代码；客户性质-个人，需填写身份证号
                  </span>
                </p>
                <p>
                  三证合一前，请填写营业执照登记号码，长度15位；三证合一后，请填写社会信用代码，长度18位
                </p>
              </Col>
            </Row>

            <Row>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSome}
                  label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencySn' })}
                >
                  {getFieldDecorator('companySgencySn', { rules: companyAgencySnRule })(
                    <Input disabled={mut === 1} maxLength={10} />
                  )}
                </Form.Item>
                {!isEmptyObj(tianYCInfo) && isNotPersonal && (
                  <Row>
                    <Col {...formItemLayoutSome.labelCol}>&nbsp;</Col>
                    <Col {...formItemLayoutSome.wrapperCol}>
                      <p className={styles.tianYCFont}>{tianYCInfo.orgNumber}</p>
                    </Col>
                  </Row>
                )}
              </Col>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSomeRight}
                  label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxSn' })}
                >
                  {getFieldDecorator('companyTaxSn', { rules: companyTaxSnRule })(
                    <Input disabled={mut === 1} maxLength={20} />
                  )}
                </Form.Item>
                {!isEmptyObj(tianYCInfo) && isNotPersonal && (
                  <Row>
                    <Col {...formItemLayoutSomeRight.labelCol}>&nbsp;</Col>
                    <Col {...formItemLayoutSomeRight.wrapperCol}>
                      <p className={styles.tianYCFont}>{tianYCInfo.taxNumber}</p>
                    </Col>
                  </Row>
                )}
              </Col>
            </Row>

            <Row>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSome}
                  label={formatMessage({ id: 'form.custCompanyInfoManage.companyCountryCode' })}
                >
                  {getFieldDecorator('companyCountryCode', {
                    rules: [
                      {
                        required: true,
                        message: formatMessage({
                          id: 'form.custCompanyInfoManage.companyCountryCodeMust',
                        }),
                      },
                    ],
                  })(
                    <Select disabled={mut === 1}>
                      {pca.map(item => (
                        <Select.Option key={item.countryCode} value={item.countryCode}>
                          {item.countryName}
                        </Select.Option>
                      ))}
                    </Select>
                  )}
                </Form.Item>
              </Col>
              <Col {...colLayout}>
                <Form.Item
                  {...formItemLayoutSomeRight}
                  label={formatMessage({ id: 'form.custCompanyInfoManage.companyRegionCode' })}
                >
                  {getFieldDecorator('companyRegionCode', {
                    rules: [
                      {
                        required: true,
                        message: formatMessage({
                          id: 'form.custCompanyInfoManage.companyRegionCodeMust',
                        }),
                      },
                    ],
                  })(<TreeSelect disabled treeData={formatParmRegionToAntdTreeSelectCmp(pra)} />)}
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.registerAddress' })}
                required
              >
                {/* 包含：注册地址-省/市/区,初始值例如: ['zhejiang', 'hangzhou', 'xihu'] */}
                {getFieldDecorator('registerAddress', {
                  rules: [
                    {
                      required: true,
                      message: formatMessage({
                        id: 'form.custCompanyInfoManage.registerAddress.placeholder',
                      }),
                    },
                  ],
                })(
                  <Cascader
                    disabled={mut === 1}
                    options={formatParmRegionToAntdCascaderCmp(pra)}
                    placeholder={formatMessage({ id: 'form.common.pleaseSelect' })}
                    changeOnSelect
                    onChange={this.changeRegisterAddress}
                  />
                )}
              </Form.Item>
            </Row>
            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyAddressDetail' })}
              >
                {getFieldDecorator('companyAddressDetail', {
                  rules: [
                    {
                      required: true,
                      message: formatMessage({
                        id: 'form.custCompanyInfoManage.companyAddressDetailMust',
                      }),
                    },
                  ],
                })(<Input disabled={mut === 1} maxLength={100} />)}
              </Form.Item>
              {!isEmptyObj(tianYCInfo) && isNotPersonal && (
                <Row>
                  <Col {...formItemLayout.labelCol}>&nbsp;</Col>
                  <Col {...formItemLayout.wrapperCol}>
                    <p className={styles.tianYCFont}>{tianYCInfo.regLocation}</p>
                  </Col>
                </Row>
              )}
            </Row>

            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyBusiness' })}
              >
                {getFieldDecorator('companyBusiness', {
                  rules: [
                    {
                      required: true,
                      message: formatMessage({
                        id: 'form.custCompanyInfoManage.companyBusinessMust',
                      }),
                    },
                  ],
                })(<Input.TextArea disabled={mut === 1} rows={3} maxLength={255} />)}
              </Form.Item>
              {!isEmptyObj(tianYCInfo) && isNotPersonal && (
                <Row>
                  <Col {...formItemLayout.labelCol}>&nbsp;</Col>
                  <Col {...formItemLayout.wrapperCol}>
                    <p className={styles.tianYCFont}>{tianYCInfo.businessScope}</p>
                  </Col>
                </Row>
              )}
            </Row>

            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseScan' })}
              >
                <UploadSingleFile
                  uploadFile={companyLicenseScan}
                  uploadFileCallback={this.uploadFileCallback}
                  uploadAccept="image/*"
                  uploadSign="companyLicenseScan"
                  uploadIsDisable={mut === 1}
                />
              </Form.Item>
            </Row>
            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScan' })}
                required={isNotPersonal && !threeCertificatesVerify}
              >
                <UploadSingleFile
                  uploadFile={companySgencyScan}
                  uploadFileCallback={this.uploadFileCallback}
                  uploadAccept="image/*"
                  uploadSign="companySgencyScan"
                  uploadIsDisable={mut === 1}
                />
              </Form.Item>
            </Row>
            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScan' })}
                required={isNotPersonal && !threeCertificatesVerify}
              >
                <UploadSingleFile
                  // uploadFile={companyTaxScan || (mue ? mue.companyTaxScan : '')}
                  uploadFile={companyTaxScan}
                  uploadFileCallback={this.uploadFileCallback}
                  uploadAccept="image/*"
                  uploadSign="companyTaxScan"
                  uploadIsDisable={mut === 1}
                />
              </Form.Item>
            </Row>
            <Row>
              {/* <Form.Item label={formatMessage({ id: 'form.custApplyManage.otherCardType' })}>
                {otherScanList()}
                <Icon type="plus-circle" onClick={this.addOtherScan} />
              </Form.Item> */}
              <Form.Item label={formatMessage({ id: 'form.custApplyManage.otherCardType' })}>
                <UploadSingleFiles
                  uploadFile={companyOtherScan}
                  uploadFileList={companyOtherScanList}
                  uploadFileCallback={this.uploadFileCallback}
                  uploadFileTranscoded={this.uploadFileTranscoded}
                  uploadAccept="image/*"
                  uploadSign="companyOtherScan"
                  uploadIsDisable={mut === 1}
                />
              </Form.Item>
            </Row>
          </Form>
        </div>
      </div>
    );
  }
}

export default connect(
  ({ parmcountrymanage, parmregionmanage, parmcurrencymanage, custapplymanage, user }) => ({
    parmCountryAll: parmcountrymanage.parmCountryAll || [], // 国别定义(所有)
    parmRegionAll: parmregionmanage.parmRegionAll || [], // 行政区划定义数据(所有)
    parmCurrencyAll: parmcurrencymanage.parmCurrencyAll || [], // 币种类型
    custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户申请单详情信息
    currentUser: user.currentUser || {},
    tianYCInfo: custapplymanage.tianYCInfo || {}, // 天眼查信息企业基本信息
  })
)(
  Form.create({
    onValuesChange({ dispatch, custApplyInfo }, changedValues) {
      const values = { ...changedValues };
      // 没有custCode，生成custCode
      if (isEmptyStr(custApplyInfo.custRfCompanyInfoAddDto.custCode)) {
        // eslint-disable-next-line no-param-reassign
        custApplyInfo.custRfCompanyInfoAddDto.custCode = uuid.v4();
      }
      // 注册地址(省/市/区)
      if (changedValues.registerAddress && changedValues.registerAddress.length > 0) {
        [
          values.companyProvince,
          values.companyCity,
          values.companyDistrict,
        ] = changedValues.registerAddress; // 设置注册地址-省市区
      }
      // 三证合一和信用码
      if ('isCompanyCertMerge' in changedValues) {
        // eslint-disable-next-line no-unused-expressions
        changedValues.isCompanyCertMerge
          ? (values.isCompanyCertMerge = 1)
          : (values.isCompanyCertMerge = 0);
      }
      if ('hasCompanyCode' in changedValues) {
        // eslint-disable-next-line no-unused-expressions
        changedValues.hasCompanyCode ? (values.hasCompanyCode = 1) : (values.hasCompanyCode = 0);
      }

      dispatch({
        type: 'custapplymanage/changeCustApplyInfo',
        payload: {
          ...custApplyInfo,
          custRfCompanyInfoAddDto: {
            ...custApplyInfo.custRfCompanyInfoAddDto,
            ...values,
          },
        },
      });
    },
    mapPropsToFields(props) {
      const {
        custApplyInfo: { custRfCompanyInfoAddDto: mue },
      } = props;
      // 注册地址
      const registerAddressArray = [];
      if (mue && mue.companyProvince) registerAddressArray.push(mue.companyProvince);
      if (mue && mue.companyCity) registerAddressArray.push(mue.companyCity);
      if (mue && mue.companyDistrict) registerAddressArray.push(mue.companyDistrict);
      return {
        companyName: Form.createFormField({
          // eslint-disable-next-line no-nested-ternary
          value: !isEmptyObj(mue) ? mue.companyName : '',
        }),
        custShortName: Form.createFormField({
          // eslint-disable-next-line no-nested-ternary
          value: !isEmptyObj(mue) ? mue.custShortName : '',
        }),
        custRegisteredCapital: Form.createFormField({
          value: !isEmptyObj(mue) ? mue.custRegisteredCapital : '',
        }),
        custCurrencyCode: Form.createFormField({
          // eslint-disable-next-line no-nested-ternary
          value: !isEmptyObj(mue) ? (mue.custCurrencyCode ? mue.custCurrencyCode : 'CNY') : 'CNY',
        }),
        hasCompanyCode: Form.createFormField({
          // eslint-disable-next-line no-nested-ternary
          value: !isEmptyObj(mue)
            ? !isEmptyStr(mue.hasCompanyCode)
              ? mue.hasCompanyCode === 1
              : true
            : true,
        }),
        isCompanyCertMerge: Form.createFormField({
          // eslint-disable-next-line no-nested-ternary
          value: !isEmptyObj(mue)
            ? !isEmptyStr(mue.isCompanyCertMerge)
              ? mue.isCompanyCertMerge === 1
              : true
            : true,
        }),
        companyCode: Form.createFormField({ value: !isEmptyObj(mue) ? mue.companyCode : '_NONE_' }),
        companySgencySn: Form.createFormField({
          value: !isEmptyObj(mue) ? mue.companySgencySn : '',
        }),
        companyTaxSn: Form.createFormField({ value: !isEmptyObj(mue) ? mue.companyTaxSn : '' }),
        companyCountryCode: Form.createFormField({
          // eslint-disable-next-line no-nested-ternary
          value: !isEmptyObj(mue)
            ? !isEmptyStr(mue.companyCountryCode)
              ? mue.companyCountryCode
              : 'CN'
            : 'CN',
        }),
        companyRegionCode: Form.createFormField({
          value: !isEmptyObj(mue) ? mue.companyRegionCode : '',
        }),
        registerAddress: Form.createFormField({ value: mue ? registerAddressArray : [] }),
        companyAddressDetail: Form.createFormField({
          value: !isEmptyObj(mue) ? mue.companyAddressDetail : '',
        }),
        companyBusiness: Form.createFormField({
          value: !isEmptyObj(mue) ? mue.companyBusiness : '',
        }),
        companyLicenseSn: Form.createFormField({
          value: !isEmptyObj(mue) ? mue.companyLicenseSn : '',
        }),
      };
    },
  })(EnterpriseQualification)
);
