/* eslint-disable no-nested-ternary */
/* eslint-disable no-case-declarations */
import React, { Fragment } from 'react';
import { connect } from 'dva';
import { Descriptions, Divider, message } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import UploadSingleFile from '@/components/UploadBasic/UploadSingleFile';
import UploadSingleFiles from '@/components/UploadBasic/UploadSingleFiles'; // 上传多个文件
// import { isEmptyStr, isEmptyObj } from '@/utils/utils';
// import { wrapTid } from '@/utils/mdcutil';
import { accessFileUrl } from '@/defaultSettings';
import styles from './index.less';

moment.locale('zh-cn');
const uploadIsDisable = true;
/**
 * 客户信息的界面部分组件
 */

class CustApplyDetailInfo extends React.PureComponent {
  state = {
    companyLicenseScan: '', // 营业执照副件扫描件,单张
    companySgencyScan: '', // 组织机构代码证扫描件,单张
    companyTaxScan: '', // 税务登记证扫描件,单张
    companyOtherScan: '', // 其他证件扫描件,单张
    companyOtherScanList: [], // 其他证件扫描件地址,多张

    custRelatedCompLicenseScan: '', // 营业执照副件扫描件,单张
    custRelatedCompAgencyScan: '', // 组织机构代码证扫描件,单张
    custRelatedCompTaxScan: '', // 税务登记证扫描件,单张
    relatedCompanyOtherScan: '', // 其他证件扫描件,单张
    relatedCompanyOtherScanList: [], // 其他证件扫描件地址,多张

    legalManCardFront: '', // 企业法人证件正面,单张
    legalManCardBack: '', // 企业法人证件反面,单张
  };

  componentDidMount() {
    // 初始化
    const {
      custApplyInfo: {
        custRfCompRelatedCompAddDto: relatInfo,
        custRfCompanyInfoAddDto: companyInfo,
      },
    } = this.props;

    // 查询行政地区
    // const reginCodeList = [companyInfo.companyCity, companyInfo.companyDistrict];
    // dispatch({
    //   type: 'custcompanyinfomanage/getParmRegionByReginCodesUsing',
    //   payload: { reginCodeList },
    //   callback: res => {
    //     this.setState({
    //       reginName: res.data.regionFullName,
    //     });
    //   },
    // });
    // 企业资质的图片
    const companyOtherScanList = []; // 其他证件扫描件,多张
    if (
      companyInfo &&
      companyInfo.custRfCompanyDescAddDtoList &&
      companyInfo.custRfCompanyDescAddDtoList.length > 0
    ) {
      companyInfo.custRfCompanyDescAddDtoList.forEach(val => {
        companyOtherScanList.push(val.custPackingListAddr);
      });
    }

    // 相关企业的图片
    const relatedCompanyOtherScanList = []; // 其他证件扫描件,多张
    if (
      relatInfo &&
      relatInfo.custRfCompRelatedCompDescAddDtoList &&
      relatInfo.custRfCompRelatedCompDescAddDtoList.length > 0
    ) {
      relatInfo.custRfCompRelatedCompDescAddDtoList.forEach(val => {
        relatedCompanyOtherScanList.push(val.custPackingListAddr);
      });
    }

    this.setState({
      companyLicenseScan:
        companyInfo && companyInfo.companyLicenseScan
          ? `${accessFileUrl}/${companyInfo.companyLicenseScan}`
          : '', // 营业执照副件扫描件,单张
      companySgencyScan:
        companyInfo && companyInfo.companySgencyScan
          ? `${accessFileUrl}/${companyInfo.companySgencyScan}`
          : '', // 组织机构代码证扫描件,单张
      companyTaxScan:
        companyInfo && companyInfo.companyTaxScan
          ? `${accessFileUrl}/${companyInfo.companyTaxScan}`
          : '', // 税务登记证扫描件,单张
      companyOtherScanList, // 其他证件扫描件,多张

      custRelatedCompLicenseScan:
        relatInfo && relatInfo.custRelatedCompLicenseScan
          ? `${accessFileUrl}/${relatInfo.custRelatedCompLicenseScan}`
          : '', // 营业执照副件扫描件,单张
      custRelatedCompAgencyScan:
        relatInfo && relatInfo.custRelatedCompAgencyScan
          ? `${accessFileUrl}/${relatInfo.custRelatedCompAgencyScan}`
          : '', // 组织机构代码证扫描件,单张
      custRelatedCompTaxScan:
        relatInfo && relatInfo.custRelatedCompTaxScan
          ? `${accessFileUrl}/${relatInfo.custRelatedCompTaxScan}`
          : '', // 税务登记证扫描件,单张
      relatedCompanyOtherScanList, // 其他证件扫描件,多张

      legalManCardFront:
        companyInfo && companyInfo.legalManCardFront
          ? `${accessFileUrl}/${companyInfo.legalManCardFront}`
          : '', // 企业法人证件正面,单张
      legalManCardBack:
        companyInfo && companyInfo.legalManCardBack
          ? `${accessFileUrl}/${companyInfo.legalManCardBack}`
          : '', // 企业法人证件反面,单张
    });
  }

  redStar = node => (
    <span>
      <span className={styles.redStar}>*</span>
      {node}
    </span>
  );

  /**
   * 文件上传UploadFile组件回调函数
   */
  uploadFileCallback = (uploadSign, newFile) => {
    const obj = {};
    if ((newFile && newFile.type.indexOf('image') >= 0) || newFile === '') {
      obj[uploadSign] = newFile;
      this.setState(obj);
    } else if (newFile) {
      message.warn(formatMessage({ id: 'form.common.UploadTypeError' }));
    }
  };

  render() {
    const {
      custApplyInfo: {
        custRfCompRelatedCompAddDto: relatInfo,
        custRequestFormInfoAddDto: formInfo,
        custRfCompanyInfoAddDto: companyInfo,
      },
    } = this.props;
    const {
      // 企业资质
      companyLicenseScan, // 营业执照副件扫描件,单张
      companySgencyScan, // 组织机构代码证扫描件,单张
      companyTaxScan, // 税务登记证扫描件,单张
      companyOtherScan, // 其他证件扫描件,单张
      companyOtherScanList, // 其他证件扫描件,多张
      // 相关企业图片
      custRelatedCompLicenseScan, // 营业执照副件扫描件,单张
      custRelatedCompAgencyScan, // 组织机构代码证扫描件,单张
      custRelatedCompTaxScan, // 税务登记证扫描件,单张
      relatedCompanyOtherScan, // 其他证件扫描件,单张
      relatedCompanyOtherScanList, // 其他证件扫描件地址,单张

      legalManCardFront, // 企业法人证件正面,单张
      legalManCardBack, // 企业法人证件反面,单张
      // reginName, // 行政区域
    } = this.state;
    // 判断会计科目-公司是否必填
    const hasEbs = companyInfo.custAccountTypeCode === 'internal';
    // const companyEbsText = () => {
    //   if (companyInfo.custAccountTypeCode === 'internal') {
    //     return this.redStar(formatMessage({ id: 'form.custApplyManage.accountTitleCompanyEbs' }));
    //   }
    //   return formatMessage({ id: 'form.custApplyManage.accountTitleCompanyEbs' });
    // };
    // const departEbsText = () => {
    //   if (companyInfo.custAccountTypeCode === 'internal') {
    //     return this.redStar(formatMessage({ id: 'form.custApplyManage.accountTitleDepartEbs' }));
    //   }
    //   return formatMessage({ id: 'form.custApplyManage.accountTitleDepartEbs' });
    // };

    //
    const registerAddress=companyInfo?(companyInfo.companyDistrictShowName==='无'?`${companyInfo.companyProvinceShowName}/${companyInfo.companyCityShowName}`:`${companyInfo.companyProvinceShowName}/${companyInfo.companyCityShowName}/${companyInfo.companyDistrictShowName}`):''
    return (
      <div className={styles.productCont}>
        <Descriptions
          title={formatMessage({ id: 'form.custInfoManage.basicInfo' })}
          column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        >
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custCompanyInfoManage.custName' }))}
            span={2}
          >
            {companyInfo.custName}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custInfoManage.custCode' }))}
          >
            {formInfo.rfStatus === 'verify_adopt' ? companyInfo.custCode : ''}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(
              formatMessage({ id: 'form.custCompanyInfoManage.operUnitTypeCode' })
            )}
          >
            {companyInfo.operUnitTypeShowName}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custApplyManage.accountType' }))}
          >
            {companyInfo.custAccountTypeShowName}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custApplyManage.custType' }))}
          >
            {companyInfo.custTypeShowName}
          </Descriptions.Item>

          <Descriptions.Item
            label={formatMessage({ id: 'form.custApplyManage.custAccountTypeCtagCode' })}
          >
            {companyInfo.custAccountTypeCtagShowName}
          </Descriptions.Item>
          {hasEbs && (
            <Descriptions.Item
              label={this.redStar(
                formatMessage({ id: 'form.custApplyManage.accountTitleCompanyEbs' })
              )}
            >
              {companyInfo.accountTitleCompanyEbs}
            </Descriptions.Item>
          )}
          {hasEbs && (
            <Descriptions.Item
              label={this.redStar(
                formatMessage({ id: 'form.custApplyManage.accountTitleDepartEbs' })
              )}
            >
              {companyInfo.accountTitleDepartEbs}
            </Descriptions.Item>
          )}

          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custApplyManage.custOuId' }))}
            span={3}
          >
            {companyInfo.custOuId}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custApplyManage.custDescription' })}
            span={3}
          >
            {companyInfo.custDescription}
          </Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions
          title={formatMessage({ id: 'form.custInfoManage.companyInfo' })}
          column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        >
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custCompanyInfoManage.companyName' }))}
            span={3}
          >
            {companyInfo.companyName}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.custShortName' })}
          >
            {companyInfo.custShortName}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custApplyManage.registeredCapital' })}
            span={2}
          >
            {companyInfo.custRegisteredCapital}
            &nbsp;
            {companyInfo.custCurrencyCode}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custCompanyInfoManage.companyCode' }))}
          >
            {companyInfo.companyCode}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencySn' })}
          >
            {companyInfo.companySgencySn ? companyInfo.companySgencySn : '无'}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxSn' })}
          >
            {companyInfo.companyTaxSn ? companyInfo.companyTaxSn : '无'}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyCountryCode' })}
          >
            {companyInfo && companyInfo.companyCountryShowName}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyRegionCode' })}
            span={2}
          >
            {companyInfo && companyInfo.companyRegionShowName}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(
              formatMessage({ id: 'form.custCompanyInfoManage.registerAddress' })
            )}
          >
            {registerAddress}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(
              formatMessage({ id: 'form.custCompanyInfoManage.companyAddressDetail' })
            )}
          >
            {companyInfo && companyInfo.companyAddressDetail}
          </Descriptions.Item>{' '}
          <Descriptions.Item
            label={this.redStar(
              formatMessage({ id: 'form.custCompanyInfoManage.companyBusiness' })
            )}
            span={3}
          >
            {companyInfo && companyInfo.companyBusiness}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions
          className={styles.descripRow}
          column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        >
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseScan' })}
          >
            {' '}
            <UploadSingleFile
              uploadFile={companyLicenseScan}
              uploadAccept="image/*"
              uploadSign="companyLicenseScan"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScan' })}
          >
            <UploadSingleFile
              uploadFile={companySgencyScan}
              uploadAccept="image/*"
              uploadSign="companySgencyScan"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScan' })}
          >
            <UploadSingleFile
              uploadFile={companyTaxScan}
              uploadAccept="image/*"
              uploadSign="companyTaxScan"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>

          <Descriptions.Item
            label={formatMessage({ id: 'form.custApplyManage.otherCardType' })}
            span={3}
          >
            <UploadSingleFiles
              uploadFile={companyOtherScan}
              uploadFileList={companyOtherScanList}
              uploadAccept="image/*"
              uploadSign="companyOtherScan"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
        </Descriptions>
        <Divider />
        {relatInfo && relatInfo.id ? (
          <Fragment>
            <Descriptions
              title={formatMessage({ id: 'form.custInfoManage.affiliatedCompanyInfo' })}
              column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 2 }}
            >
              <Descriptions.Item
                label={this.redStar(
                  formatMessage({ id: 'form.custApplyManage.affiliatedCompanyName' })
                )}
              >
                {relatInfo.custRelatedCompName ? relatInfo.custRelatedCompName : '无'}
              </Descriptions.Item>
              <Descriptions.Item
                label={this.redStar(
                  formatMessage({ id: 'form.custApplyManage.affiliatedCompanyCode' })
                )}
              >
                {relatInfo.custRelatedCompCode ? relatInfo.custRelatedCompCode : '无'}
              </Descriptions.Item>
            </Descriptions>
            <Descriptions
              className={styles.descripRow}
              column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
            >
              <Descriptions.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseScan' })}
              >
                <UploadSingleFile
                  uploadFile={custRelatedCompLicenseScan}
                  uploadAccept="image/*"
                  uploadSign="custRelatedCompLicenseScan"
                  uploadIsDisable={uploadIsDisable}
                  // bodyStyle={{ width: '100px', height: '100px' }}
                  uploadFileCallback={this.uploadFileCallback}
                />
              </Descriptions.Item>
              <Descriptions.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScan' })}
              >
                <UploadSingleFile
                  uploadFile={custRelatedCompAgencyScan}
                  uploadAccept="image/*"
                  uploadSign="custRelatedCompAgencyScan"
                  uploadIsDisable={uploadIsDisable}
                  // bodyStyle={{ width: '100px', height: '100px' }}
                  uploadFileCallback={this.uploadFileCallback}
                />
              </Descriptions.Item>
              <Descriptions.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScan' })}
              >
                <UploadSingleFile
                  uploadFile={custRelatedCompTaxScan}
                  uploadAccept="image/*"
                  uploadSign="custRelatedCompTaxScan"
                  uploadIsDisable={uploadIsDisable}
                  // bodyStyle={{ width: '100px', height: '100px' }}
                  uploadFileCallback={this.uploadFileCallback}
                />
              </Descriptions.Item>

              <Descriptions.Item
                label={formatMessage({ id: 'form.custApplyManage.otherCardType' })}
                span={3}
              >
                <UploadSingleFiles
                  uploadFile={relatedCompanyOtherScan}
                  uploadFileList={relatedCompanyOtherScanList}
                  uploadAccept="image/*"
                  uploadSign="relatedCompanyOtherScan"
                  uploadIsDisable={uploadIsDisable}
                  uploadFileCallback={this.uploadFileCallback}
                  // bodyStyle={{ width: '100px', height: '100px' }}
                />
              </Descriptions.Item>
            </Descriptions>
          </Fragment>
        ) : null}
        <Descriptions
          title={formatMessage({ id: 'form.custInfoManage.legalManInfo' })}
          column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 2 }}
        >
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.legalManName' })}
          >
            {companyInfo.legalManName ? companyInfo.legalManName : '无'}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardNo' })}
          >
            {companyInfo.legalManCardNo
              ? `${companyInfo.legalManCardTypeShowName}： ${companyInfo.legalManCardNo}`
              : '无'}

            {/* {renderCardType()} {companyInfo.legalManCardNo} */}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions
          className={styles.descripRow}
          column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 1 }}
        >
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardFront' })}
          >
            <UploadSingleFile
              uploadFile={legalManCardFront}
              uploadAccept="image/*"
              uploadSign="legalManCardFront"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardBack' })}
          >
            <UploadSingleFile
              uploadFile={legalManCardBack}
              uploadAccept="image/*"
              uploadSign="legalManCardBack"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
        </Descriptions>
      </div>
    );
  }
}

export default connect(({ custapplymanage }) => ({
  custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户详情信息
}))(CustApplyDetailInfo);
