/* eslint-disable no-lonely-if */
/* eslint-disable no-useless-escape */
/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-console */
import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Card } from 'antd';
import router from 'umi/router'; // eslint-disable-line
import { isEmptyStr } from '@/utils/utils';
import CustApplyDetailInfo from './CustApplyDetailInfo';
import AuditLogTab from '../../custapplyauditmanage/tabs/auditLogTab';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 客户申请单信息维护组件,以新页面访问
 */
class CustCompanyInfoUpkeep extends React.PureComponent {
  constructor(props) {
    super(props);
    const self = this;
    // 初始化数据
    self.state = {
      activeKey: 'detailTab', // tab的
      auditValues: {}, // 审核model中填写的数据
    };
    self.tabList = [
      {
        key: 'detailTab',
        tab: '客户',
      },
      {
        key: 'auditLogTab',
        tab: '历史审核记录',
      },
    ];
  }

  /**
   * 获取申请单详情，初始化传入redux中
   */
  componentDidMount() {
    const {
      dispatch,
      location: {
        query: { id = '', from = 'apply',tid='10001' },
      },
    } = this.props;
    // 清空为默认
    dispatch({
      type: 'custapplymanage/changeCustApplyInfo',
      payload: {
        custRequestFormInfoAddDto: {},
        custRfCompRelatedCompAddDto: {
          custRfCompRelatedCompDescAddDtoList: [],
        },
        custRfCompanyInfoAddDto: {
          isCompanyCertMerge: 1,
          hasCompanyCode: 1,
          custRfCompanyDescAddDtoList: [],
        },
      }, // 客户申请单详情信息，初始化
    });
    // 详情
    if (isEmptyStr(id)) {
      if (from === 'apply') {
        // 申请单详情
        router.push('/custmanage/custapplymanage');
      } else if (from === 'audit') {
        // 审核详情
        router.push('/custmanage/custapplyauditmanage');
      }
    } else {
      // 根据id查询申请单详情
      dispatch({
        type: 'custapplymanage/getCustApplyInfoByRfId',
        payload: {id,tid},
      });
      // 根据rfId查询审核历史记录
      dispatch({
        type: 'custapplymanage/getCustApplyAuditLogs',
        payload: {
          searchCondition: {
            rfId: id,
          },
        },
      });
    }
  }

  /**
   * tab切换回调
   */
  tabChange = key => {
    // const { dispatch } = this.props;
    this.setState({
      activeKey: key,
    });
  };

  render() {
    const {
      custApplyInfo: { custRequestFormInfoAddDto: mue },
      custApplyAuditLogs,
    } = this.props;
    const { activeKey } = this.state;
    // tab内容
    const contentList = {
      detailTab: <CustApplyDetailInfo />,
      auditLogTab: <AuditLogTab />,
    };
    // tab标签内容
    const tabList = [
      {
        key: 'detailTab',
        tab: '详情',
      },
    ];
    // 当申请单为审核通过,审核驳回,单据作废时，显示审核历史记录
    // if (
    //   mue &&
    //   (mue.rfStatus === 'verify_adopt' ||
    //     mue.rfStatus === 'verify_reject' ||
    //     mue.rfStatus === 'bill_invalid')
    // ) {
    //   tabList.push({
    //     key: 'auditLogTab',
    //     tab: '历史审核记录',
    //   });
    // }

    // 当申请单详情或维护，且有历史记录时，显示审核历史记录
    if (custApplyAuditLogs.length > 0) {
      tabList.push({
        key: 'auditLogTab',
        tab: '历史审核记录',
      });
    }

    return (
      <Card
        title="客户申请单详情"
        bordered={false}
        tabList={tabList}
        activeTabKey={activeKey}
        onTabChange={this.tabChange}
      >
        {tabList.map(item => (
          <div key={item.key} style={{ display: activeKey === item.key ? 'block' : 'none' }}>
            {mue && mue.id ? contentList[item.key] : null}
          </div>
        ))}
      </Card>
    );
  }
}

export default connect(({ custapplymanage }) => ({
  custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户申请单详情信息
  custApplyAuditLogs: custapplymanage.custApplyAuditLogs || {}, // 申请单审核记录
}))(CustCompanyInfoUpkeep);
