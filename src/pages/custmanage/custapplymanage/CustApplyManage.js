/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Select, Input, Card, Modal } from 'antd';
import moment from 'moment';
// eslint-disable-next-line import/no-extraneous-dependencies
import router from 'umi/router';
import uuid from 'uuid';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import CustApplyUpkeep from './CustApplyUpkeep';
// import { formatDate } from '@/utils/utils';
import { wrapTid } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 企业信息管理组件
 */
class CustApplyManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.goodsApplyBillManage.applyName' }),
        dataIndex: 'rfName',
        key: 'rfName',
        align: 'center',
        width: 120,
      },
      {
        title: formatMessage({ id: 'form.custApplyManage.custNum' }),
        dataIndex: 'rfRequestCreateCustNum',
        key: 'rfRequestCreateCustNum',
        align: 'center',
        width: 70,
      },
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.operUnitTypeCode' }),
        key: 'operUnitTypeCode',
        dataIndex: 'operUnitTypeCode',
        align: 'center',
        width: 100,
        // render: (text, record) => (
        //   <span>{this.custOperUnitTypeTxt(record, 'operUnitTypeCode')}</span>
        // ),
      },
      {
        title: formatMessage({ id: 'form.custApplyManage.accountType' }),
        key: 'custAccountTypeCode',
        align: 'center',
        dataIndex: 'custAccountTypeCode',
        width: 70,
      },
      {
        title: formatMessage({ id: 'form.common.apply.people' }),
        key: 'rfRequestUserName',
        align: 'center',
        dataIndex: 'rfRequestUserName',
        width: 120,
      },
      {
        title: formatMessage({ id: 'form.common.apply.organization' }),
        key: 'rfRequestUserOrgName',
        dataIndex: 'rfRequestUserOrgName',
        align: 'center',
        width: 70,
      },
      {
        title: formatMessage({ id: 'form.common.apply.status' }),
        key: 'rfStatus',
        align: 'center',
        width: 70,
        // dataIndex: 'rfStatusShowName',
        render: (text, record) => <span>{this.renderStatus(record, 'rfStatus')}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.apply.time' }),
        key: 'createAt',
        dataIndex: 'createAt',
        align: 'center',
        width: 100,
        // render: (text, record) => (
        //   <span>{formatDate(new Date(record.createAt), 'yyyy-MM-dd hh:mm:ss')}</span>
        // ),
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        width: 130,
        render: (text, record) => (
          <span>
            <a
              type="primary"
              size="small"
              onClick={() => {
                this.hookProcess(1, record);
              }}
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            {/* 审核驳回和未提交 开放维护功能 */}
            {(record.rfStatus === 'verify_reject' || record.rfStatus === 'not_submitted') && (
              <span>
                <Divider type="vertical" />
                <a
                  type="primary"
                  size="small"
                  onClick={() => {
                    this.hookProcess(2, record);
                  }}
                >
                  {formatMessage({ id: 'button.common.modify' })}
                </a>
              </span>
            )}
            {/* 待审核 开放撤销功能 */}
            {record.rfStatus === 'verify_waiting' && (
              <span>
                <Divider type="vertical" />
                <a type="primary" size="small" onClick={() => this.hookProcess(3, record)}>
                  {formatMessage({ id: 'button.custApplyManage.revoke' })}
                </a>
              </span>
            )}
            {/* 审核未提交 开放删除功能 */}
            {record.rfStatus === 'not_submitted' && (
              <span>
                <Divider type="vertical" />
                <a
                  type="primary"
                  size="small"
                  onClick={() => {
                    this.hookProcess(4, record);
                  }}
                >
                  {formatMessage({ id: 'button.common.delete' })}
                </a>
              </span>
            )}
            {/* 申请驳回 开放作废功能 */}
            {record.rfStatus === 'verify_reject' && (
              <span>
                <Divider type="vertical" />
                <a type="primary" size="small" onClick={() => this.hookProcess(5, record)}>
                  {formatMessage({ id: 'button.custApplyManage.void' })}
                </a>
              </span>
            )}
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {
      tid: wrapTid().tid, // 租户id,
      createUid: props.currentUser.id, // 当前用户id，只能查询自己的提交的申请单
    };
  }

  /**
   * 响应点击处理函数
   */
  hookProcess = (type, record) => {
    // console.log('查询列表', type, record);
    // const { dispatch } = this.props;
    const val = { ...record };
    const self = this;
    // const values = this.searchContent;
    if (type === 1) {
      // 详情
      router.push({
        pathname: '/custmanage/custapplymanage/detail',
        query: {
          id: record.rfId,
          from: 'apply',
        },
      });
    } else if (type === 2) {
      // 维护
      // console.log('/goodsmanage/goodsapplybillmanage/detail?id=1321', record);
      router.push({
        pathname: '/custmanage/custapplymanage/info',
        query: {
          id: record.rfId,
          tid:record.tid,
          type: 'edit',
        },
      });
    } else if (type === 3) {
      // 撤销
      Modal.confirm({
        title: formatMessage({ id: 'form.custApplyAuditManage.revoke.title' }),
        content: formatMessage({ id: 'form.custApplyAuditManage.revoke.content' }),
        okText: formatMessage({ id: 'form.common.sure' }),
        okType: 'danger',
        cancelText: formatMessage({ id: 'form.common.cancel' }),
        onOk() {
          self.revokeApply(record);
        },
        onCancel() {
          // console.log('Cancel');
        },
      });
    } else if (type === 4) {
      // 删除
      Modal.confirm({
        title: formatMessage({ id: 'form.custApplyAuditManage.delete.title' }),
        content: formatMessage({ id: 'form.custApplyAuditManage.delete.content' }),
        okText: formatMessage({ id: 'form.common.sure' }),
        okType: 'danger',
        cancelText: formatMessage({ id: 'form.common.cancel' }),
        onOk() {
          self.deleteInfo(record);
        },
        onCancel() {
          // console.log('Cancel');
        },
      });
    } else if (type === 5) {
      // 作废
      Modal.confirm({
        title: formatMessage({ id: 'form.custApplyAuditManage.void.title' }),
        content: formatMessage({ id: 'form.custApplyAuditManage.void.content' }),
        okText: formatMessage({ id: 'form.common.sure' }),
        okType: 'danger',
        cancelText: formatMessage({ id: 'form.common.cancel' }),
        onOk() {
          self.voidInfo(record);
        },
        onCancel() {
          // console.log('Cancel');
        },
      });
    }
    return val;
  };

  // 删除申请单
  deleteInfo = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'custapplymanage/deleteCustApplyInfo',
      payload: record.id,
      callback: () => {
        this.onFuzzyCustCompanyInfoHandle();
      },
    });
  };

  // 撤销申请单
  revokeApply = record => {
    const changeVal = {
      rfId: record.rfId,
      id: record.id,
      rfStatus: 'verify_reject', // 撤销变为审核驳回状态
      verifyDesc: '自行撤销', // 记录描述
    };
    this.changeApply(changeVal);
  };

  // 作废申请单
  voidInfo = record => {
    const changeVal = {
      rfId: record.rfId,
      id: record.id,
      rfStatus: 'bill_invalid', // 作废
      verifyDesc: '申请单作废', // 记录描述
    };
    this.changeApply(changeVal);
  };

  // 修改申请单状态：1、修改申请单状态，2、增加操作日志
  changeApply = applyInfo => {
    const { currentUser, dispatch } = this.props;
    const para = {
      id: applyInfo.id,
      modifyAt: new Date(),
      createUcode: currentUser.id,
      createUname: currentUser.name,
      createUid:currentUser.id,
      modifyUcode: currentUser.id,
      modifyUid: currentUser.id,
      modifyUname: currentUser.name,
      rfStatus: applyInfo.rfStatus,
    };
    // 更新-客户申请单(单个记录)
    dispatch({
      type: 'custapplymanage/updateCustApplyInfoRfStatus',
      payload: para,
      callback: () => {
        this.onFuzzyCustCompanyInfoHandle();
      },
    });
    // 添加历史操作记录，撤销
    const params = {
      verifyResult: applyInfo.rfStatus, // 撤销变为审核驳回状态
      verifyDesc: applyInfo.verifyDesc,
      verifyId: uuid(),
      rfId: applyInfo.rfId,
      verifyUserOrgId: wrapTid().tid,
      verifyUserOrgName: currentUser.name,
      createUcode: currentUser.id,
      createUname: currentUser.name,
      createUid:currentUser.id,
      modifyUcode: currentUser.id,
      modifyUid: currentUser.id,
      modifyUname: currentUser.name,
      verifyUserName: currentUser.name,
      tid:wrapTid().tid,
    };
    dispatch({
      type: 'custapplymanage/addCustApplyAudit',
      payload: params,
    });
  };

  /**
   * 加载默认企业信息数据
   */
  componentDidMount() {
    const { dispatch } = this.props;

    // 获取-客户申请列表(列表)
    dispatch({
      type: 'custapplymanage/paginationCustApplyList',
      payload: {
        page: 1,
        pageSize: 10,
        searchCondition: this.searchContent,
      },
    });
    // 获取-经营单位类型定义数据(所有)
    dispatch({ type: 'custoperunittypemanage/getCustOperUnitTypeAll' });
    // 获取-客户申请单客户状态数据(所有)
    dispatch({
      type: 'parmpublicparametermanage/getCustRfStatus',
      payload: { parmTypeCode: 'cust_rf_status' },
    });
  }

  /**
   * 经营单位类型
   */
  custOperUnitTypeTxt = (record, field) => {
    const { custOperUnitTypeAll } = this.props;

    const findVal = custOperUnitTypeAll.filter(v => {
      return v.operUnitTypeCode === record[field];
    });
    return findVal.length > 0 ? findVal[0].operUnitTypeName : '';
  };

  /**
   * 申请单数据的状态
   */
  renderStatus = (record, field) => {
    const { parmPublicParameterCustRfStatus: custRfStatus = [] } = this.props;
    const findVal = custRfStatus.find(v => v.parmCode === record[field]);
    return findVal ? findVal.parmValue : '';
  };

  /**
   * 根据条件搜索品牌
   */
  onFuzzyCustCompanyInfoHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    const values = getFieldsValue();
    this.searchContent = { ...this.searchContent, ...values };
    dispatch({
      type: 'custapplymanage/paginationCustApplyList',
      payload: { page: 1, pageSize: 10, searchCondition: this.searchContent },
    });
    // 返回第一页
    this.changeCurrentPage(1);
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
      formItemLayout,
      tailFormItemLayout,
    } = this.state;
    const { custApplyList, custApplyListPageSize } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
        custOperUnitTypeAll: couta, // 经营单位类型定义数据(所有)
        // eslint-disable-next-line react/no-this-in-sfc
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.goodsApplyBillManage.applyName' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('rfName')(<Input maxLength={99} style={{ width: 300 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.operUnitTypeCode' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('operUnitTypeCode')(
                <Select style={{ width: 150 }} allowClear>
                  {couta.map(item => (
                    <Select.Option key={item.operUnitTypeCode} value={item.operUnitTypeCode}>
                      {item.operUnitTypeName}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyCustCompanyInfoHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() => {
              // eslint-disable-next-line react/no-this-in-sfc
              router.push({
                pathname: '/custmanage/custapplymanage/info',
                query: {
                  id: '',
                  type: 'add',
                },
              });
            }}
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };
    const tablePaginationOnChangeEventDispatchType = 'custapplymanage/paginationCustApplyList';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'menu.custmanage.custapplymanage' })}
          tableColumns={tableColumns}
          tableDataSource={custApplyList}
          tableOperateAreaCmp={tableOperateAreaCmp()}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              { searchCondition: this.searchContent }
            )
          }
          tableTotalSize={custApplyListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <CustApplyUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
              modalSearchCondition={this.searchContent}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(
  ({ custapplymanage, custoperunittypemanage, parmpublicparametermanage, user }) => ({
    custApplyList: custapplymanage.custApplyList,
    custApplyListPageSize: custapplymanage.custApplyListPageSize,
    custOperUnitTypeAll: custoperunittypemanage.custOperUnitTypeAll || [], // 经营单位类型定义数据(所有)
    parmPublicParameterCustRfStatus: parmpublicparametermanage.parmPublicParameterCustRfStatus, // 客户申请单客户状态数据(所有)
    currentUser: user.currentUser || {}, // 当前用户
  })
)(Form.create()(CustApplyManage));
