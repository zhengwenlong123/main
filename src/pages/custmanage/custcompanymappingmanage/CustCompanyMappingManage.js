/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Select, Input, Card } from 'antd';
import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import CustCompanyMappingUpkeep from './CustCompanyMappingUpkeep';
import { trim } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 企业编码对照管理组件
 */
class CustCompanyMappingManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      // { title: '客户代码', dataIndex: 'custCode', key: 'companyCode', align: 'center' },
      {
        title: formatMessage({ id: 'form.custCompanyMappingManage.mappingSysCode' }),
        dataIndex: 'mappingSysCode',
        key: 'mappingSysCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custCompanyMappingManage.mappingSysName' }),
        dataIndex: 'mappingSysName',
        key: 'mappingSysName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custCompanyMappingManage.companyName' }),
        dataIndex: 'custCodeVO.companyName',
        key: 'custCodeVO.companyName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custCompanyMappingManage.mappingCustCode' }),
        dataIndex: 'mappingCustCode',
        key: 'mappingCustCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custCompanyMappingManage.mappingCustName' }),
        dataIndex: 'mappingCustName',
        key: 'mappingCustName',
        align: 'center',
      },
      // { title: '对照客户简称', dataIndex: 'mappingCustShortName', key: 'mappingCustShortName', align: 'center' },
      // { title: '对照客户描述', dataIndex: 'mappingCustDescription', key: 'mappingCustDescription', align: 'center' },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'enable',
        align: 'center',
        render: (text, record) => <span>{status[record.enable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  record,
                  formatMessage({ id: 'button.custCompanyMappingManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  record,
                  formatMessage({ id: 'button.custCompanyMappingManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'custcompanymappingmanage/updateCustCompanyMappingEnable',
                  this.searchContent
                )
              }
            >
              {record.enable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {};
  }

  /**
   * 响应点击处理函数
   */
  hookProcess = (type, record) => {
    if (type === 3) {
      record.enable = record.enable === 1 ? 0 : 1; // 设置记录状态为无效
    } else {
      // 无处理
    }
    return record;
  };

  /**
   * 加载默认企业编码对照数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-企业编码数据(列表)
    dispatch({ type: 'custcompanymappingmanage/getCustCompanyMapping' });
    // 获取-企业信息数据(所有)
    dispatch({ type: 'custcompanyinfomanage/getCustCompanyInfoAll' });
    // 获取-客户编码对照数据(所有)-公用参数(所有)(条件为代码是cust_sys_code_mapping的客户系统编码对照数据)
    dispatch({
      type:
        'parmpublicparametermanage/getParmPublicParameterAllByParmPublicParameterTypeOfCustSysCodeMapping',
      payload: { parmTypeCode: 'cust_sys_code_mapping' },
    });
  }

  /**
   * 根据条件搜索品牌
   */
  onFuzzyCustCompanyMappingHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    // eslint-disable-next-line
    for (const key in values) {
      if (values[key]) {
        values[key] = trim(values[key]);
      }
    }
    dispatch({
      type: 'custcompanymappingmanage/paginationCustCompanyMapping',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
    } = this.state;
    const {
      custCompanyMappingList,
      custCompanyMappingListPageSize,
      formItemLayout,
      tailFormItemLayout,
    } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.companyName' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('custName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.mappingSysCode' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('mappingSysCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.mappingCustName' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('mappingCustName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.enable' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('enable')(
                <Select style={{ width: 80 }} allowClear>
                  <Select.Option value="1">
                    {formatMessage({ id: 'form.common.enabled' })}
                  </Select.Option>
                  <Select.Option value="0">
                    {formatMessage({ id: 'form.common.disabled' })}
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyCustCompanyMappingHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.custCompanyMappingManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };

    const tablePaginationOnChangeEventDispatchType =
      'custcompanymappingmanage/paginationCustCompanyMapping';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'menu.custmanage.custcompanymappingmanage' })}
          tableColumns={tableColumns}
          tableDataSource={custCompanyMappingList}
          tableOperateAreaCmp={tableOperateAreaCmp()}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              { searchCondition: this.searchContent }
            )
          }
          tableTotalSize={custCompanyMappingListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <CustCompanyMappingUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
              modalSearchCondition={this.searchContent}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ custcompanymappingmanage }) => ({
  custCompanyMappingList: custcompanymappingmanage.custCompanyMappingList,
  custCompanyMappingListPageSize: custcompanymappingmanage.custCompanyMappingListPageSize,
}))(Form.create()(CustCompanyMappingManage));
