/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Button, Input, Checkbox, Select, message } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 企业编码对照维护组件
 */
@ValidationFormHoc
class CustCompanyMappingUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 去除空格
   */
  // eslint-disable-next-line no-unused-vars
  trim = value => value.replace(/(^\s*) | (\s*$)/g, '');

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      dispatch,
      validationForm,
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      modalOnChangePageCurrent,
      modalSearchCondition,
      parmPublicParameterOfCustSysCodeMappingTypeAll: pppocscmta, // 系统编码对照下拉数据源
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    values.enable ? (values.enable = 1) : (values.enable = 0);
    const newMappings = pppocscmta.filter(item => item.parmCode === values.mappingSysCode);
    if (newMappings === null || newMappings.length === 0) {
      message.warn(
        formatMessage({ id: 'form.custCompanyMappingManage.mappingSysName-conformity' })
      );
      return;
    }
    this.setState({ confirmLoading: true });
    values.mappingSysName = newMappings[0].parmValue;
    values.mappingSysFieldCode = this.trim(values.mappingSysFieldCode);
    values.mappingSysFieldName = this.trim(values.mappingSysFieldName);
    values.mappingCustCode = this.trim(values.mappingCustCode);
    values.mappingCustName = this.trim(values.mappingCustName);
    values.mappingCustShortName = this.trim(values.mappingCustShortName);

    switch (mut) {
      case 2: // 维护
        values.id = mue.id;
        dispatch({
          type: 'custcompanymappingmanage/updateCustCompanyMapping',
          payload: values,
          callback: this.onCallback,
          searchCondition: modalSearchCondition,
        });
        break;
      case 10: // 新增
        dispatch({
          type: 'custcompanymappingmanage/addCustCompanyMapping',
          payload: values,
          callback: this.onCallback,
          searchCondition: modalSearchCondition,
        });
        break;
      default:
        break;
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      modalUpkeepType: mut,
      form: { getFieldDecorator },
      custCompanyInfoAll: ccia, // 企业信息数据(所有)
      parmPublicParameterOfCustSysCodeMappingTypeAll: pppocscmta, // 系统编码对照下拉数据源
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.custCompanyMappingManage.custName' })}>
              {getFieldDecorator('custCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.custCompanyMappingManage.custNameMust' }),
                  },
                ],
              })(
                <Select disabled={mut === 1}>
                  {ccia.map(item => (
                    <Select.Option key={item.custCode} value={item.custCode}>
                      {item.custName}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.mappingSysName' })}
            >
              {getFieldDecorator('mappingSysCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyMappingManage.mappingSysNameMust',
                    }),
                  },
                ],
              })(
                <Select disabled={mut === 1}>
                  {pppocscmta.map(item => (
                    <Select.Option key={item.parmCode} value={item.parmCode}>
                      {item.parmValue}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.mappingSysFieldCode' })}
            >
              {getFieldDecorator('mappingSysFieldCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyMappingManage.mappingSysFieldCodeMust',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={50} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.mappingSysFieldName' })}
            >
              {getFieldDecorator('mappingSysFieldName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyMappingManage.mappingSysFieldNameMust',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.mappingCustCode' })}
            >
              {getFieldDecorator('mappingCustCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyMappingManage.mappingCustCodeMust',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={50} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.mappingCustName' })}
            >
              {getFieldDecorator('mappingCustName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyMappingManage.mappingCustNameMust',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.mappingCustShortName' })}
            >
              {getFieldDecorator('mappingCustShortName')(
                <Input disabled={mut === 1} maxLength={50} />
              )}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyMappingManage.mappingCustDescription' })}
            >
              {getFieldDecorator('mappingCustDescription')(
                <Input.TextArea disabled={mut === 1} rows={3} maxLength={1000} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('enable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ custcompanyinfomanage, parmpublicparametermanage }) => ({
  custCompanyInfoAll: custcompanyinfomanage.custCompanyInfoAll || [], // 企业信息数据(所有)
  parmPublicParameterOfCustSysCodeMappingTypeAll:
    parmpublicparametermanage.parmPublicParameterOfCustSysCodeMappingTypeAll || [], // 公用参数定义(根据公用参数类型定义名称[cust_sys_code_mapping]查询到匹配的公用参数定义数据)
}))(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        custCode: Form.createFormField({ value: mue ? mue.custCode : '' }),
        mappingSysCode: Form.createFormField({ value: mue ? mue.mappingSysCode : '' }),
        mappingSysFieldCode: Form.createFormField({ value: mue ? mue.mappingSysFieldCode : '' }),
        mappingSysFieldName: Form.createFormField({ value: mue ? mue.mappingSysFieldName : '' }),
        // mappingSysName: Form.createFormField({ value: mue ? mue.mappingSysName : '' }),
        mappingCustCode: Form.createFormField({ value: mue ? mue.mappingCustCode : '' }),
        mappingCustName: Form.createFormField({ value: mue ? mue.mappingCustName : '' }),
        mappingCustShortName: Form.createFormField({ value: mue ? mue.mappingCustShortName : '' }),
        mappingCustDescription: Form.createFormField({
          value: mue ? mue.mappingCustDescription : '',
        }),
        enable: Form.createFormField({ value: mue ? mue.enable === 1 : true }),
      };
    },
  })(CustCompanyMappingUpkeep)
);
