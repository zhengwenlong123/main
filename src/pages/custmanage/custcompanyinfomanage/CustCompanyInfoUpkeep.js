/* eslint-disable no-lonely-if */
/* eslint-disable no-useless-escape */
/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-console */
import React from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';
import { Form, Button, Input, Checkbox, Select, TreeSelect, message, Cascader } from 'antd';
import moment from 'moment';
import {
  formatParmRegionToAntdCascaderCmp,
  formatParmRegionToAntdTreeSelectCmp,
  verifyUnique,
  fileRelationPathReg,
} from '@/utils/mdcutil';
import UploadSingleFile from '@/components/UploadBasic/UploadSingleFile';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import request from '@/utils/request';
import { fileUrl, accessFileUrl } from '@/defaultSettings';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 企业信息维护组件
 */
@ValidationFormHoc
class CustCompanyInfoUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    threeCertificatesVerify: false, // 是否三证合一修改必填状态
    companyLicenseScan: '', // 营业执照副件扫描件,单张
    companyAgencyScan: '', // 组织机构代码证扫描件,单张
    companyTaxScan: '', // 税务登记证扫描件,单张
    legalManCardFront: '', // 企业法人证件正面,单张
    legalManCardBack: '', // 企业法人证件反面,单张
    confirmLoading: false, // 提交等待
  };

  /**
   * 初始化state中的文件相关参数值
   */
  componentDidMount() {
    const { modalUpkeepEntity: mue } = this.props;
    this.setState({
      companyLicenseScan:
        mue && mue.companyLicenseScan ? `${accessFileUrl}/${mue.companyLicenseScan}` : '', // 营业执照副件扫描件,单张
      companyAgencyScan:
        mue && mue.companyAgencyScan ? `${accessFileUrl}/${mue.companyAgencyScan}` : '', // 组织机构代码证扫描件,单张
      companyTaxScan: mue && mue.companyTaxScan ? `${accessFileUrl}/${mue.companyTaxScan}` : '', // 税务登记证扫描件,单张
      legalManCardFront:
        mue && mue.legalManCardFront ? `${accessFileUrl}/${mue.legalManCardFront}` : '', // 企业法人证件正面,单张
      legalManCardBack:
        mue && mue.legalManCardBack ? `${accessFileUrl}/${mue.legalManCardBack}` : '', // 企业法人证件反面,单张
      threeCertificatesVerify: mue && mue.isCompanyCertMerge ? mue.isCompanyCertMerge : 0,
    });
  }

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.info(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      companyLicenseScan, // 营业执照副件扫描件,单张
      companyAgencyScan, // 组织机构代码证扫描件,单张
      companyTaxScan, // 税务登记证扫描件,单张
      legalManCardFront, // 企业法人证件正面,单张
      legalManCardBack, // 企业法人证件反面,单张
    } = this.state;
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
      modalSearchCondition,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();

    // 判断名称是否重复
    // const params = { custName: values.custName };
    // const custNameStatus = await verifyUnique('/mdc/cust/custcompanyinfo/verifyUnique', params);
    // if (!custNameStatus && mut === 10) {
    //   message.warn(formatMessage({ id: 'form.custCompanyInfoManage.custNameExist' }));
    //   return;
    // }

    values.enable ? (values.enable = 1) : (values.enable = 0);
    values.hasCompanyCode ? (values.hasCompanyCode = 1) : (values.hasCompanyCode = 0);
    values.isCompanyCertMerge ? (values.isCompanyCertMerge = 1) : (values.isCompanyCertMerge = 0);

    // 注册地址(省/市/区)
    if (values.registerAddress.length > 0) {
      values.companyProvince = values.registerAddress[0]; // 设置注册地址-省
    }
    if (values.registerAddress.length > 1) {
      values.companyCity = values.registerAddress[1]; // 设置注册地址-市
    }
    if (values.registerAddress.length > 2) {
      values.companyDistrict = values.registerAddress[2]; // 设置注册地址-区
    }

    // 国别编码，缺省值为"CN"
    values.companyCountryCode ? null : (values.companyCountryCode = 'CN');

    if (values.hasCompanyCode === 0) {
      // 1、 0-无对应公司(即无社会统一信用代码,无对应信用代码时企业代码填写"_NONE_"）
      values.companyCode = '_NONE_';
      // 2、无对应信用代码时企业名称填写客户名称
      values.companyName = values.custName;
    } else {
      // 1-有对应公司；缺省为1
      // 社会统一信用代码(校验)
      if (values.companyCode.length !== 18) {
        message.warn(formatMessage({ id: 'form.custCompanyInfoManage.companyCode-conformity' }));
        return;
      }
    }

    if (values.registerAddress === '') {
      // 验证注册地址是否为空
      message.warn(formatMessage({ id: 'form.custCompanyInfoManage.registerAddress-conformity' }));
      return;
    }

    if (values.isCompanyCertMerge === 1) {
      // 是三证合一
      // 1、营业执照注册号：三证合一后，为社会信用代码，长度18位，
      values.companyLicenseSn = values.companyCode;
    } else {
      // 不是三证合一
      // 1、组织机构代码必需录入(检验)
      if (
        values.companyAgencySn === null ||
        values.companyAgencySn === undefined ||
        values.companyAgencySn === ''
      ) {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyAgencySn-conformity' })
        );
        return;
      }
      // 2、税务代码必需录入(检验)
      if (
        values.companyTaxSn === null ||
        values.companyTaxSn === undefined ||
        values.companyTaxSn === ''
      ) {
        message.warn(formatMessage({ id: 'form.custCompanyInfoManage.companyTaxSn-conformity' }));
        return;
      }
      // 3、营业执照注册号录入(可选),三证合一前：15位, 如果有值则必须15位,否则就不要填写数据
      if (
        values.companyLicenseSn !== '' &&
        values.companyLicenseSn !== null &&
        values.companyLicenseSn !== undefined &&
        values.companyLicenseSn.length !== 15
      ) {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseSn-conformity' })
        );
        return;
      }
    }

    // 以上必填写数据都验证通过之后,先上传图片并取得图片地址,最后再写数据到数据库
    // 上传-营业执照副件扫描件,单张
    if (companyLicenseScan instanceof File) {
      const formData = new FormData();
      formData.append('file', companyLicenseScan);
      const companyLicenseScanRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (companyLicenseScanRes) {
        values.companyLicenseScan = companyLicenseScanRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseSnUploadError' })
        );
        return;
      }
      // console.log(companyLicenseScanRes);
    } else {
      values.companyLicenseScan = companyLicenseScan.replace(fileRelationPathReg, '');
    }

    // 修改提交状态
    this.setState({ confirmLoading: true });

    // 上传-组织机构代码证扫描件,单张
    if (companyAgencyScan instanceof File) {
      const formData = new FormData();
      formData.append('file', companyAgencyScan);
      const companyAgencyScanRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (companyAgencyScanRes) {
        values.companyAgencyScan = companyAgencyScanRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScanResUploadError' })
        );
        return;
      }
      // console.log(companyAgencyScanRes);
    } else {
      values.companyAgencyScan = companyAgencyScan.replace(fileRelationPathReg, '');
    }

    // 上传-税务登记证扫描件,单张
    if (companyTaxScan instanceof File) {
      const formData = new FormData();
      formData.append('file', companyTaxScan);
      const companyTaxScanRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (companyTaxScanRes) {
        values.companyTaxScan = companyTaxScanRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScanResUploadError' })
        );
        return;
      }
      // console.log(companyTaxScanRes);
    } else {
      values.companyTaxScan = companyTaxScan.replace(fileRelationPathReg, '');
    }

    // 上传-企业法人证件正面,单张
    if (legalManCardFront instanceof File) {
      const formData = new FormData();
      formData.append('file', legalManCardFront);
      const legalManCardFrontRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (legalManCardFrontRes) {
        values.legalManCardFront = legalManCardFrontRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.legalManCardFrontResUploadError' })
        );
        return;
      }
      // console.log(legalManCardFrontRes);
    } else {
      values.legalManCardFront = legalManCardFront.replace(fileRelationPathReg, '');
    }

    // 上传-企业法人证件反面,单张
    if (legalManCardBack instanceof File) {
      const formData = new FormData();
      formData.append('file', legalManCardBack);
      const legalManCardBackRes = await request.post(
        `${fileUrl}/bootstrap/fileobject/upload/single`,
        { data: formData }
      );
      if (legalManCardBackRes) {
        values.legalManCardBack = legalManCardBackRes.url;
      } else {
        message.warn(
          formatMessage({ id: 'form.custCompanyInfoManage.legalManCardBackResUploadError' })
        );
        return;
      }
      // console.log(legalManCardBackRes);
    } else {
      values.legalManCardBack = legalManCardBack.replace(fileRelationPathReg, '');
    }

    switch (mut) {
      case 2: // 维护
        values.id = mue.id;
        dispatch({
          type: 'custcompanyinfomanage/updateCustCompanyInfo',
          payload: values,
          callback: this.onCallback,
          searchCondition: modalSearchCondition,
        });
        break;
      case 10: // 新增
        dispatch({
          type: 'custcompanyinfomanage/addCustCompanyInfo',
          payload: values,
          callback: this.onCallback,
          searchCondition: modalSearchCondition,
        });
        break;
      default:
        break;
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  /**
   * 社会信用代码切换选中
   */
  // companyCodeChange = e => {
  //   const { form: {setFieldsValue} } = this.props
  //   if (e.target.checked) {
  //     setFieldsValue({
  //       companyCode: ''
  //     })
  //   } else {
  //     setFieldsValue({
  //       companyCode: '_NONE_'
  //     })
  //   }
  // }

  /**
   * 三证合一修改必填状态
   */
  threeCertificates = e => {
    this.setState({ threeCertificatesVerify: e.target.checked ? 1 : 0 });
  };

  /**
   * 文件上传UploadFile组件回调函数
   */
  uploadFileCallback = (uploadSign, newFile) => {
    const obj = {};
    if ((newFile && newFile.type.indexOf('image') >= 0) || newFile === '') {
      obj[uploadSign] = newFile;
      this.setState(obj);
    } else if (newFile) {
      message.warn(formatMessage({ id: 'form.common.UploadTypeError' }));
    }
  };

  /**
   * 验证客户名称唯一性
   * tid,custName必传
   */
  verifyUnquieCustName = async custNameInput => {
    const value = custNameInput.input.value;
    if (value === null || value.length === 0) return;
    const params = { custName: value };
    const status = await verifyUnique('/mdc/cust/custcompanyinfo/verifyUnique', params);
    if (!status) {
      message.warn(formatMessage({ id: 'form.custCompanyInfoManage.custNameExist' }));
      // custNameInput.focus();
    }
  };

  /**
   * 修改注册地址执行的操作
   */
  changeRegisterAddress = value => {
    // console.log(value)
    const {
      dispatch,
      form: { setFieldsValue },
    } = this.props;
    dispatch({
      type: 'custcompanyinfomanage/getParmRegionByReginCodesUsing',
      payload: { reginCodeList: value },
      callback: res => {
        setFieldsValue({ companyRegionCode: res.data.regionCode });
      },
    });
  };

  render() {
    const {
      formItemLayout,
      threeCertificatesVerify, // 三证合一必填状态判断
      companyLicenseScan, // 营业执照副件扫描件,单张
      companyAgencyScan, // 组织机构代码证扫描件,单张
      companyTaxScan, // 税务登记证扫描件,单张
      legalManCardFront, // 企业法人证件正面,单张
      legalManCardBack, // 企业法人证件反面,单张
      confirmLoading,
    } = this.state;
    const {
      modalVisibleOnChange,
      modalUpkeepType: mut,
      // modalUpkeepEntity: mue,
      form: { getFieldDecorator },
      custOperUnitTypeAll: couta, // 经营单位类型定义数据(所有)
      parmCountryAll: pca, // 国别定义(所有)
      parmRegionAll: pra, // 行政区划定义数据(所有)
      parmPublicParameterOfCustUserCardTypeAll: pppocucta, // 公用参数定义数据[用户证件类型数据所有]
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            {mut === 10 ? null : (
              <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.custCode' })}>
                {getFieldDecorator('custCode')(<Input disabled maxLength={25} />)}
              </Form.Item>
            )}
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.custName' })}>
              {getFieldDecorator('custName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.custCompanyInfoManage.custNameMust' }),
                  },
                ],
              })(
                <Input
                  disabled={mut === 1}
                  maxLength={100}
                  ref={input => (this.custNameInput = input)}
                  // onBlur={() => this.verifyUnquieCustName(this.custNameInput)}
                />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.operUnitTypeCode' })}>
              {getFieldDecorator('operUnitTypeCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyInfoManage.operUnitTypeCodeMust',
                    }),
                  },
                ],
              })(
                <Select disabled={mut === 1}>
                  {couta.map(item => (
                    <Select.Option key={item.operUnitTypeCode} value={item.operUnitTypeCode}>
                      {item.operUnitTypeName}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.custShortName' })}>
              {getFieldDecorator('custShortName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.custCompanyInfoManage.custShortNameMust' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={50} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.custDescription' })}>
              {getFieldDecorator('custDescription')(
                <Input.TextArea disabled={mut === 1} rows={3} maxLength={255} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.hasCompanyCode' })}>
              {getFieldDecorator('hasCompanyCode', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.custCompanyInfoManage.hasCompanyCodeMust' })}
                </Checkbox>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.companyCode' })}>
              {getFieldDecorator('companyCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.custCompanyInfoManage.companyCodeMust' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={18} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.companyName' })}>
              {getFieldDecorator('companyName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.custCompanyInfoManage.companyNameMust' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyCountryCode' })}
            >
              {getFieldDecorator('companyCountryCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyInfoManage.companyCountryCodeMust',
                    }),
                  },
                ],
              })(
                <Select disabled={mut === 1}>
                  {pca.map(item => (
                    <Select.Option key={item.countryCode} value={item.countryCode}>
                      {item.countryName}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyRegionCode' })}
            >
              {getFieldDecorator('companyRegionCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyInfoManage.companyRegionCodeMust',
                    }),
                  },
                ],
              })(<TreeSelect disabled treeData={formatParmRegionToAntdTreeSelectCmp(pra)} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.registerAddress' })}
              required
            >
              {/* 包含：注册地址-省/市/区,初始值例如: ['zhejiang', 'hangzhou', 'xihu'] */}
              {getFieldDecorator('registerAddress')(
                <Cascader
                  disabled={mut === 1}
                  options={formatParmRegionToAntdCascaderCmp(pra)}
                  placeholder={formatMessage({ id: 'form.common.pleaseSelect' })}
                  changeOnSelect
                  onChange={this.changeRegisterAddress}
                />
              )}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyAddressDetail' })}
            >
              {getFieldDecorator('companyAddressDetail', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyInfoManage.companyAddressDetailMust',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.companyBusiness' })}>
              {getFieldDecorator('companyBusiness', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.custCompanyInfoManage.companyBusinessMust',
                    }),
                  },
                ],
              })(<Input.TextArea disabled={mut === 1} rows={3} maxLength={1000} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.isCompanyCertMerge' })}
            >
              {getFieldDecorator('isCompanyCertMerge', { valuePropName: 'checked' })(
                <Checkbox disabled={mut === 1} onChange={this.threeCertificates}>
                  {formatMessage({ id: 'form.custCompanyInfoManage.isCompanyCertMergeMust' })}
                </Checkbox>
              )}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseSn' })}
              // style={{ display: !threeCertificatesVerify ? 'block' : 'none' }}
              style={{ display: 'none' }}
            >
              {getFieldDecorator('companyLicenseSn')(<Input disabled={mut === 1} maxLength={18} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencySn' })}
              required={!threeCertificatesVerify}
            >
              {getFieldDecorator('companyAgencySn')(<Input disabled={mut === 1} maxLength={10} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxSn' })}
              required={!threeCertificatesVerify}
            >
              {getFieldDecorator('companyTaxSn')(<Input disabled={mut === 1} maxLength={20} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseScan' })}
            >
              <UploadSingleFile
                uploadFile={companyLicenseScan}
                uploadFileCallback={this.uploadFileCallback}
                uploadAccept="image/*"
                uploadSign="companyLicenseScan"
                uploadIsDisable={mut === 1}
              />
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScan' })}
            >
              <UploadSingleFile
                uploadFile={companyAgencyScan}
                uploadFileCallback={this.uploadFileCallback}
                uploadAccept="image/*"
                uploadSign="companyAgencyScan"
                uploadIsDisable={mut === 1}
              />
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScan' })}>
              <UploadSingleFile
                // uploadFile={companyTaxScan || (mue ? mue.companyTaxScan : '')}
                uploadFile={companyTaxScan}
                uploadFileCallback={this.uploadFileCallback}
                uploadAccept="image/*"
                uploadSign="companyTaxScan"
                uploadIsDisable={mut === 1}
              />
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.legalManName' })}>
              {getFieldDecorator('legalManName')(<Input disabled={mut === 1} maxLength={25} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardType' })}>
              {getFieldDecorator('legalManCardType')(
                <Select disabled={mut === 1}>
                  {pppocucta.map(item => (
                    <Select.Option key={item.parmCode} value={item.parmCode}>
                      {item.parmValue}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardNo' })}>
              {getFieldDecorator('legalManCardNo')(<Input disabled={mut === 1} maxLength={25} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardFront' })}
            >
              <UploadSingleFile
                // uploadFile={legalManCardFront || (mue ? mue.legalManCardFront : '')}
                uploadFile={legalManCardFront}
                uploadFileCallback={this.uploadFileCallback}
                uploadAccept="image/*"
                uploadSign="legalManCardFront"
                uploadIsDisable={mut === 1}
              />
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardBack' })}>
              <UploadSingleFile
                // uploadFile={legalManCardBack || (mue ? mue.legalManCardBack : '')}
                uploadFile={legalManCardBack}
                uploadFileCallback={this.uploadFileCallback}
                uploadAccept="image/*"
                uploadSign="legalManCardBack"
                uploadIsDisable={mut === 1}
              />
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.custCompanyInfoManage.showEnable' })}>
              {getFieldDecorator('enable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(
  ({ custoperunittypemanage, parmcountrymanage, parmregionmanage, parmpublicparametermanage }) => ({
    custOperUnitTypeAll: custoperunittypemanage.custOperUnitTypeAll || [], // 经营单位类型定义数据(所有)
    parmCountryAll: parmcountrymanage.parmCountryAll || [], // 国别定义(所有)
    parmRegionAll: parmregionmanage.parmRegionAll || [], // 行政区划定义数据(所有)
    parmPublicParameterOfCustUserCardTypeAll:
      parmpublicparametermanage.parmPublicParameterOfCustUserCardTypeAll, // 根据公用参数类型定义代码[cust_user_card_type]查询匹配到的公用参数定义数据[用户证件类型数据所有]
  })
)(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      // 注册地址
      const registerAddressArray = [];
      if (mue && mue.companyProvince) registerAddressArray.push(mue.companyProvince);
      if (mue && mue.companyCity) registerAddressArray.push(mue.companyCity);
      if (mue && mue.companyDistrict) registerAddressArray.push(mue.companyDistrict);

      return {
        custCode: Form.createFormField({ value: mue ? mue.custCode : '' }),
        custName: Form.createFormField({ value: mue ? mue.custName : '' }),
        operUnitTypeCode: Form.createFormField({ value: mue ? mue.operUnitTypeCode : '' }),
        custShortName: Form.createFormField({ value: mue ? mue.custShortName : '' }),
        custDescription: Form.createFormField({ value: mue ? mue.custDescription : '' }),
        hasCompanyCode: Form.createFormField({ value: mue ? mue.hasCompanyCode === 1 : true }),
        companyCode: Form.createFormField({ value: mue ? mue.companyCode : '_NONE_' }),
        companyName: Form.createFormField({ value: mue ? mue.companyName : '' }),
        companyCountryCode: Form.createFormField({ value: mue ? mue.companyCountryCode : 'CN' }),
        companyRegionCode: Form.createFormField({ value: mue ? mue.companyRegionCode : '' }),
        registerAddress: Form.createFormField({ value: mue ? registerAddressArray : '' }),
        companyAddressDetail: Form.createFormField({ value: mue ? mue.companyAddressDetail : '' }),
        companyBusiness: Form.createFormField({ value: mue ? mue.companyBusiness : '' }),
        isCompanyCertMerge: Form.createFormField({
          value: mue ? mue.isCompanyCertMerge === 1 : false,
        }),
        companyLicenseSn: Form.createFormField({ value: mue ? mue.companyLicenseSn : '' }),
        companyAgencySn: Form.createFormField({ value: mue ? mue.companyAgencySn : '' }),
        companyTaxSn: Form.createFormField({ value: mue ? mue.companyTaxSn : '' }),
        // companyLicenseScan: Form.createFormField({ value: mue ? mue.companyLicenseScan : '' }),
        // companyAgencyScan: Form.createFormField({ value: mue ? mue.companyAgencyScan : '' }),
        // companyTaxScan: Form.createFormField({ value: mue ? mue.companyTaxScan : '' }),
        legalManName: Form.createFormField({ value: mue ? mue.legalManName : '' }),
        legalManCardType: Form.createFormField({ value: mue ? mue.legalManCardType : '' }),
        legalManCardNo: Form.createFormField({ value: mue ? mue.legalManCardNo : '' }),
        // legalManCardFront: Form.createFormField({ value: mue ? mue.legalManCardFront : '' }),
        // legalManCardBack: Form.createFormField({ value: mue ? mue.legalManCardBack : '' }),
        enable: Form.createFormField({ value: mue ? mue.enable === 1 : true }),
      };
    },
  })(CustCompanyInfoUpkeep)
);
