/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Select, Input, Card } from 'antd';
import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import CustCompanyInfoUpkeep from './CustCompanyInfoUpkeep';
import styles from './index.less';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 企业信息管理组件
 */
class CustCompanyInfoManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.custCode' }),
        dataIndex: 'custCode',
        key: 'custCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.custName' }),
        dataIndex: 'custName',
        key: 'custName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.companyName' }),
        dataIndex: 'companyName',
        key: 'companyName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.showEnable' }),
        key: 'showEnable',
        align: 'center',
        render: (text, record) => <span>{status[record.enable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.custCompanyInfoManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.custCompanyInfoManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'custcompanyinfomanage/updateCustCompanyInfoEnable',
                  this.searchContent
                )
              }
            >
              {record.enable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {};
  }

  /**
   * 响应点击处理函数
   */
  hookProcess = (type, record) => {
    if (type === 3) {
      record.enable = record.enable === 1 ? 0 : 1; // 设置当前记录状态无效/有效
    } else {
      // 获取-当前选择-相关数据
    }
    return record;
  };

  /**
   * 加载默认企业信息数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-企业信息数据(列表)
    dispatch({ type: 'custcompanyinfomanage/getCustCompanyInfo' });
    // 获取-经营单位类型定义数据(所有)
    dispatch({ type: 'custoperunittypemanage/getCustOperUnitTypeAll' });
    // 获取-国别定义(所有)
    dispatch({ type: 'parmcountrymanage/getParmCountryAll' });
    // 获取-行政区划定义数据(所有)(默认获取countryCode: 'CN'的数据)
    dispatch({
      type: 'parmregionmanage/getParmRegionAllByParmRegion',
      payload: { countryCode: 'CN' },
    });
    // 获取-用户证件类型数据(所有)-公用参数(所有)(条件为代码是cust_user_card_type的用户证件类型数据)
    dispatch({
      type:
        'parmpublicparametermanage/getParmPublicParameterAllByParmPublicParameterTypeOfCustUserCardType',
      payload: { parmTypeCode: 'cust_user_card_type' },
    });
  }

  /**
   * 根据条件搜索品牌
   */
  onFuzzyCustCompanyInfoHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'custcompanyinfomanage/paginationCustCompanyInfo',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
      formItemLayout,
      tailFormItemLayout,
    } = this.state;
    const { custCompanyInfoList, custCompanyInfoListPageSize } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.custCode' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('custCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.custName' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('custName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.showEnable' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('enable')(
                <Select style={{ width: 80 }} allowClear>
                  <Select.Option value="1">
                    {formatMessage({ id: 'form.common.enabled' })}
                  </Select.Option>
                  <Select.Option value="0">
                    {formatMessage({ id: 'form.common.disabled' })}
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyCustCompanyInfoHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.custCompanyInfoManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };
    const tablePaginationOnChangeEventDispatchType =
      'custcompanyinfomanage/paginationCustCompanyInfo';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'menu.custmanage.custcompanyinfomanage' })}
          tableColumns={tableColumns}
          tableDataSource={custCompanyInfoList}
          tableOperateAreaCmp={tableOperateAreaCmp()}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              { searchCondition: this.searchContent }
            )
          }
          tableTotalSize={custCompanyInfoListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <CustCompanyInfoUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
              modalSearchCondition={this.searchContent}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ custcompanyinfomanage }) => ({
  custCompanyInfoList: custcompanyinfomanage.custCompanyInfoList,
  custCompanyInfoListPageSize: custcompanyinfomanage.custCompanyInfoListPageSize,
}))(Form.create()(CustCompanyInfoManage));
