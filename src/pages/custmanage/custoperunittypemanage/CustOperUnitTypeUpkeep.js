/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Button, Input, Checkbox, message } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 经营单位类型定义维护组件
 */
@ValidationFormHoc
class CustOperUnitTypeUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.info(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
      modalSearchCondition,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.enable ? (values.enable = 1) : (values.enable = 0);

    switch (mut) {
      case 2: // 维护
        values.id = mue.id;
        dispatch({
          type: 'custoperunittypemanage/updateCustOperUnitType',
          payload: values,
          callback: this.onCallback,
          searchCondition: modalSearchCondition,
        });
        break;
      case 10: // 新增
        dispatch({
          type: 'custoperunittypemanage/addCustOperUnitType',
          payload: values,
          callback: this.onCallback,
          searchCondition: modalSearchCondition,
        });
        break;
      default:
        break;
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      modalUpkeepType: mut,
      form: { getFieldDecorator },
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            {/* {mut === 10 ? null : ( */}
            <Form.Item label={formatMessage({ id: 'form.common.code' })}>
              {getFieldDecorator('operUnitTypeCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.code.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={50} />)}
            </Form.Item>
            {/* )} */}
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('operUnitTypeName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={25} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.description' })}>
              {getFieldDecorator('operUnitTypeDesc')(
                <Input.TextArea disabled={mut === 1} rows={3} maxLength={250} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('enable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" htmlType="button" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button
                type="primary"
                htmlType="button"
                onClick={this.onClickSubmit}
                loading={confirmLoading}
              >
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        operUnitTypeCode: Form.createFormField({ value: mue ? mue.operUnitTypeCode : '' }),
        operUnitTypeName: Form.createFormField({ value: mue ? mue.operUnitTypeName : '' }),
        operUnitTypeDesc: Form.createFormField({ value: mue ? mue.operUnitTypeDesc : '' }),
        enable: Form.createFormField({ value: mue ? mue.enable === 1 : true }),
      };
    },
  })(CustOperUnitTypeUpkeep)
);
