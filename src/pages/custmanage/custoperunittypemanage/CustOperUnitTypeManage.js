/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Input, Select, Card } from 'antd';
import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import CustOperUnitTypeUpkeep from './CustOperUnitTypeUpkeep';
import styles from './index.less';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 经营单位类型定义管理组件
 */
class CustOperUnitTypeManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'operUnitTypeCode',
        key: 'operUnitTypeCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'operUnitTypeName',
        key: 'operUnitTypeName',
        align: 'center',
      },
      // { title: '描述', dataIndex: 'operUnitTypeDesc', key: 'operUnitTypeDesc', align: 'center' },
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'enable',
        align: 'center',
        render: (text, record) => <span>{status[record.enable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  record,
                  formatMessage({ id: 'button.custOperUnitTypeManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  record,
                  formatMessage({ id: 'button.custOperUnitTypeManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'custoperunittypemanage/updateCustOperUnitTypeEnable',
                  this.searchContent
                )
              }
            >
              {record.enable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {};
  }

  /**
   * 点击响应函数
   */
  hookProcess = (type, record) => {
    if (type === 3) {
      record.enable = record.enable === 1 ? 0 : 1; // 设置记录状态为无效
    } else {
      // 无处理
    }
    return record;
  };

  /**
   * 加载默认经营单位类型定义数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-经营单位类型定义数据(列表)
    dispatch({ type: 'custoperunittypemanage/getCustOperUnitType' });
  }

  /**
   * 根据条件搜索品牌
   */
  onFuzzyCustOperUnitTypeHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'custoperunittypemanage/paginationCustOperUnitType',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
      formItemLayout,
      tailFormItemLayout,
    } = this.state;
    const { custOperUnitTypeList, custOperUnitTypeListPageSize } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.common.code' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('operUnitTypeCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.name' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('operUnitTypeName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.enable' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('enable')(
                <Select style={{ width: 80 }} allowClear>
                  <Select.Option value="1">
                    {formatMessage({ id: 'form.common.enabled' })}
                  </Select.Option>
                  <Select.Option value="0">
                    {formatMessage({ id: 'form.common.disabled' })}
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyCustOperUnitTypeHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.custOperUnitTypeManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };
    const tablePaginationOnChangeEventDispatchType =
      'custoperunittypemanage/paginationCustOperUnitType';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'menu.custmanage.custoperunittypemanage' })}
          tableColumns={tableColumns}
          tableDataSource={custOperUnitTypeList}
          tableOperateAreaCmp={tableOperateAreaCmp()}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              { searchCondition: this.searchContent }
            )
          }
          tableTotalSize={custOperUnitTypeListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <CustOperUnitTypeUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
              modalSearchCondition={this.searchContent}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ custoperunittypemanage }) => ({
  custOperUnitTypeList: custoperunittypemanage.custOperUnitTypeList || [],
  custOperUnitTypeListPageSize: custoperunittypemanage.custOperUnitTypeListPageSize || 0,
}))(Form.create()(CustOperUnitTypeManage));
