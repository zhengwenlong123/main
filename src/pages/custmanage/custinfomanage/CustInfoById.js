/* eslint-disable no-lonely-if */
/* eslint-disable no-useless-escape */
/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-console */
import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Card, Button, message } from 'antd';
import { formatMessage } from 'umi/locale';
import router from 'umi/router'; // eslint-disable-line
import { isEmptyStr, isEmptyObj } from '@/utils/utils';
import CustInfoDetail from './CustInfoDetail';
import TianYanChaTab from '../custapplyauditmanage/tabs/tianYanChaTab';
// import AuditLogTab from '../custapplyauditmanage/tabs/auditLogTab';
// import styles from './index.less';

moment.locale('zh-cn');
/**
 * 客户申请单信息维护组件,以新页面访问
 */
class CustCompanyInfoUpkeep extends React.PureComponent {
  constructor(props) {
    super(props);
    const self = this;
    // 初始化数据
    self.state = {
      activeKey: 'detailTab', // tab的
      auditValues: {}, // 审核model中填写的数据
      myTabList: [
        {
          key: 'detailTab',
          tab: '客户信息',
        },
      ], // tab数据
    };
    // 默认的一些数据取值
    // self.tabList = [
    //   {
    //     key: 'detailTab',
    //     tab: '客户信息',
    //   },
    //   {
    //     key: 'tianyanchaTab',
    //     tab: '天眼查',
    //   },
    // ];
  }

  /**
   * 获取申请单详情，初始化传入redux中
   */
  componentDidMount() {
    this.getInfo();
  }

  /** 组件即将卸载 */
  componentWillUnmount() {
    const { dispatch } = this.props;
    // 清空客户详情
    dispatch({
      type: 'custapplymanage/changeCustInfo',
      payload: {},
    });
    // 清空天眼查
    dispatch({
      type: 'custapplymanage/changeTianYCInfo',
      payload: {},
    });
    // 企业天眼查经营异常列表
    dispatch({
      type: 'custapplymanage/changeTianYCAbnormalList',
      payload: { content: [] },
    });
    // 客户天眼查企业风险
    dispatch({
      type: 'custapplymanage/changeTianYCRisk',
      payload: [],
    });
  }

  // // 监听审核的所选择的记录，改变了rfId参数值
  // componentWillReceiveProps(nextProps) {
  //   // console.log('nextProps', nextProps);
  //   const { selectedRfId, dispatch } = this.props;
  //   if (selectedRfId !== nextProps.selectedRfId) {
  //     // 根据selectedRfId查询申请单详情
  //     dispatch({
  //       type: 'custapplymanage/getCustApplyInfoByRfId',
  //       payload: nextProps.selectedRfId,
  //     });
  //     // 根据selectedRfId查询审核历史记录
  //     dispatch({
  //       type: 'custapplymanage/getCustApplyAuditLogs',
  //       payload: {
  //         searchCondition: {
  //           rfId: nextProps.selectedRfId,
  //         },
  //       },
  //     });
  //   }
  // }

  getInfo = () => {
    const {
      dispatch,
      location: {
        query: { id = '' },
      },
    } = this.props;
    // 详情
    if (isEmptyStr(id)) {
      this.handleToList();
    } else {
      // 根据id查询申请单详情
      dispatch({
        type: 'custinfomanage/getCustApplyInfoById',
        payload: id,
      });
    }
  };

  handleToList = () => {
    router.push('/custmanage/custinfomanage');
  };

  /**
   * tab切换回调
   */
  tabChange = key => {
    // const { dispatch } = this.props;
    this.setState({
      activeKey: key,
    });
  };

  clickTianYC = () => {
    const { custInfoDetail, dispatch } = this.props;
    // const { myTabList } = this.state;
    if (
      !isEmptyObj(custInfoDetail.custCompanyInfoMergeVO) &&
      custInfoDetail.custCompanyInfoMergeVO.companyCode
    ) {
      let companyCode = '';
      if (custInfoDetail.custCompRelatedCompDto && custInfoDetail.custCompRelatedCompDto.id) {
        // 2、客户性质-个人：关联企业社会信用统一代码
        companyCode = custInfoDetail.custCompRelatedCompDto.custRelatedCompCode;
      } else {
        // 1、客户性质 - 企事业单位、工商个体→社会信用统一代码
        companyCode = custInfoDetail.custCompanyInfoMergeVO.companyCode;
      }
      /**
       * 获取-根据客户信用码获取天眼查企业信息:
       * 1、客户性质 - 企事业单位、工商个体→社会信用统一代码
       * 2、客户性质-个人：关联企业社会信用统一代码
       */
      dispatch({
        type: 'custapplymanage/getTianYCInfoByCompanyCode',
        payload: {
          code: companyCode,
          // code: '91120116749118895P',
          sourceType: 1,
        },
        callback: res => {
          if (res && res.id) {
            const tabList = [
              {
                key: 'detailTab',
                tab: '客户信息',
              },
              {
                key: 'tianyanchaTab',
                tab: '天眼查',
              },
            ];
            this.setState({ myTabList: tabList });
          } else {
            // 天眼查没有结果
            message.error(formatMessage({ id: 'form.tianYCInfo.nodata' }));
          }
        },
      });
    }
  };

  render() {
    const { activeKey, myTabList } = this.state;
    const { custInfoDetail } = this.props;
    // tab内容,判断相关生成内容
    let cardOperateAreaCmp = null;
    if (
      !isEmptyObj(custInfoDetail.custCompanyInfoMergeVO) &&
      custInfoDetail.custCompanyInfoMergeVO.companyCode
    ) {
      cardOperateAreaCmp = (
        <Button
          type="primary"
          onClick={() => {
            this.clickTianYC();
          }}
        >
          {formatMessage({ id: 'button.custApplyAudit.tianyc' })}
        </Button>
      );
    } else {
      cardOperateAreaCmp = (
        <Button type="primary" disabled>
          {formatMessage({ id: 'button.custApplyAudit.tianyc' })}
        </Button>
      );
    }
    const contentList = {
      detailTab: !isEmptyObj(custInfoDetail.custCompanyInfoMergeVO) ? (
        <CustInfoDetail getInfo={this.getInfo} />
      ) : null,
      // eslint-disable-next-line no-nested-ternary
      tianyanchaTab: !isEmptyObj(custInfoDetail.custCompanyInfoMergeVO) ? (
        custInfoDetail.custCompRelatedCompDto && custInfoDetail.custCompRelatedCompDto.id ? (
          <TianYanChaTab companyCode={custInfoDetail.custCompRelatedCompDto.custRelatedCompCode} />
        ) : (
          <TianYanChaTab companyCode={custInfoDetail.custCompanyInfoMergeVO.companyCode} />
        )
      ) : null,
    };
    return (
      <Card
        title={formatMessage({ id: 'form.custInfoManage.title' })}
        bordered={false}
        extra={cardOperateAreaCmp}
        tabList={myTabList}
        activeTabKey={activeKey}
        onTabChange={this.tabChange}
      >
        {!isEmptyObj(custInfoDetail.custCompanyInfoMergeVO) &&
          myTabList.map(item => (
            <div key={item.key} style={{ display: activeKey === item.key ? 'block' : 'none' }}>
              {contentList[item.key]}
            </div>
          ))}
      </Card>
    );
  }
}

export default connect(({ custinfomanage }) => ({
  custInfoDetail: custinfomanage.custInfoDetail || {}, // 客户详情信息
}))(CustCompanyInfoUpkeep);
