/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Select, Input, Card, Modal, Row } from 'antd';
import moment from 'moment';
// eslint-disable-next-line import/no-extraneous-dependencies
import router from 'umi/router';
import uuid from 'uuid';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
// import { formatDate } from '@/utils/utils';
import { wrapTid } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 企业信息管理组件
 */
class CustInfoManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.operUnitTypeCode' }),
        key: 'operUnitTypeCode',
        dataIndex: 'operUnitTypeCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custApplyManage.accountType' }),
        key: 'custAccountTypeCode',
        align: 'center',
        dataIndex: 'custAccountTypeCode',
        width: 80,
      },
      {
        title: formatMessage({ id: 'form.custApplyManage.custType' }),
        dataIndex: 'custTypeCode',
        key: 'custTypeCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.custCode' }),
        dataIndex: 'custCode',
        key: 'custCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.custName' }),
        dataIndex: 'custName',
        key: 'custName',
        align: 'center',
        width: 180,
      },
      {
        title: formatMessage({ id: 'form.custInfoManage.companyCode' }),
        key: 'companyCode',
        align: 'center',
        dataIndex: 'companyCode',
      },
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.companyRegionCode' }),
        key: 'companyRegionCode',
        align: 'center',
        dataIndex: 'companyRegionCode',
        // render: (text, record) => (
        //   <span>{formatDate(new Date(record.createAt), 'yyyy-MM-dd hh:mm:ss')}</span>
        // ),
      },
      {
        title: formatMessage({ id: 'form.custInfoManage.enable' }),
        key: 'enable',
        align: 'center',
        // dataIndex: 'rfRequestUserName',
        render: (text, record) => <span>{record.enable === 1 ? '有效' : '无效'}</span>,
      },
      // {
      //   title: formatMessage({ id: 'form.common.apply.time' }),
      //   key: 'createAt',
      //   align: 'center',
      //   render: (text, record) => (
      //     <span>{formatDate(new Date(record.createAt), 'yyyy-MM-dd hh:mm:ss')}</span>
      //   ),
      // },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        width: 100,
        render: (text, record) => (
          <span>
            <a
              type="primary"
              size="small"
              onClick={() => {
                this.hookProcess(1, record);
              }}
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            {/* 有效/无效 */}
            {
              <span>
                <Divider type="vertical" />
                <a
                  type="primary"
                  size="small"
                  onClick={() => {
                    this.hookProcess(2, record);
                  }}
                >
                  {record.enable === 1
                    ? formatMessage({ id: 'form.common.disabled' })
                    : formatMessage({ id: 'form.common.enabled' })}
                </a>
              </span>
            }
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {
      
    };
    this.stateList = [
      {
        id: uuid(),
        value: 0,
        txt: '无效',
      },
      {
        id: uuid(),
        value: 1,
        txt: '有效',
      },
    ];
  }

  /**
   * 响应点击处理函数
   */
  hookProcess = (type, record) => {
    // console.log('查询列表', type, record);
    // const { dispatch } = this.props;
    const val = { ...record };
    // const values = this.searchContent;
    if (type === 1) {
      // 详情
      router.push({
        pathname: '/custmanage/custinfomanage/detail',
        query: {
          id: record.id,
        },
      });
    } else if (type === 2) {
      // 有效/无效
      const self = this;
      const title =
        record.enable === 1
          ? formatMessage({ id: 'form.custInfoManage.disable.title' })
          : formatMessage({ id: 'form.custInfoManage.enable.title' });

      const content =
        record.enable === 1
          ? formatMessage({ id: 'form.custInfoManage.disable.content' })
          : formatMessage({ id: 'form.custInfoManage.enable.content' });

      Modal.confirm({
        title,
        content,
        okText: formatMessage({ id: 'form.common.sure' }),
        okType: 'danger',
        cancelText: formatMessage({ id: 'form.common.cancel' }),
        onOk() {
          self.enableStatus(record);
        },
        onCancel() {
          // console.log('Cancel');
        },
      });
    }
    return val;
  };

  /**
   * 加载默认企业信息数据
   */
  componentDidMount() {
    const { dispatch } = this.props;

    // 获取-客户信息列表(列表)
    dispatch({
      type: 'custinfomanage/paginationCustInfoList',
      payload: {
        page: 1,
        pageSize: 10,
        searchCondition: this.searchContent,
      },
    });
    // 获取-经营单位类型定义数据(所有)
    dispatch({ type: 'custoperunittypemanage/getCustOperUnitTypeAll' });
    // 获取-客户申请单账号类型数据(所有)
    dispatch({
      type: 'parmpublicparametermanage/getCustAccountType',
      payload: { parmTypeCode: 'cust_account_type' },
    });
    // 获取-客户类型数据(所有)
    dispatch({
      type: 'parmpublicparametermanage/getCustType',
      payload: { parmTypeCode: 'cust_type' },
    });
  }

  // 申请单 有效/无效
  enableStatus = record => {
    const { dispatch } = this.props;
    const para = {
      id: record.id,
      enable: record.enable === 1 ? 0 : 1,
    };
    // 获取-客户申请列表(列表)
    dispatch({
      type: 'custinfomanage/updateCustApplyInfoRfStatus',
      payload: para,
      callback: () => {
        this.onFuzzyCustCompanyInfoHandle();
      },
    });
  };

  /**
   * 经营单位类型
   */
  custOperUnitTypeTxt = (record, field) => {
    const { custOperUnitTypeAll } = this.props;

    const findVal = custOperUnitTypeAll.filter(v => {
      return v.operUnitTypeCode === record[field];
    });
    return findVal.length > 0 ? findVal[0].operUnitTypeName : '';
  };

  /**
   * 数据审核的状态
   */
  applyBillField = (record, field) => {
    const {
      parmpublicparametermanage: {
        parmPublicParameterGoodsRfStatus,
        parmPublicParameterApplyBillTypeAll,
      },
    } = this.props;
    const obj = {
      rfStatus: parmPublicParameterGoodsRfStatus,
      rfTypeCode: parmPublicParameterApplyBillTypeAll,
    };
    const findVal = obj[field].find(v => v.parmCode === record[field]);
    return findVal ? findVal.parmValue : '';
  };

  /**
   * 根据条件搜索品牌
   */
  onFuzzyCustCompanyInfoHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    const values = getFieldsValue();
    this.searchContent = { ...this.searchContent, ...values };
    dispatch({
      type: 'custinfomanage/paginationCustInfoList',
      payload: { page: 1, pageSize: 10, searchCondition: this.searchContent },
    });
    // 返回第一页
    this.changeCurrentPage(1);
  }

  render() {
    const { tableColumns, tableCurrentPage, formItemLayout, tailFormItemLayout } = this.state;
    const { custInfoList, custInfoListPageSize } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
        custOperUnitTypeAll: couta, // 经营单位类型定义数据(所有)
        custTypeAll, // 客户性质类型(所有)
        custAccountTypeAll, // 账号类型数据(所有)
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custInfoManage.operUnitTypeCode' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('operUnitTypeCode')(
                  <Select style={{ width: 120 }} allowClear>
                    {couta.map(item => (
                      <Select.Option key={item.operUnitTypeCode} value={item.operUnitTypeCode}>
                        {item.operUnitTypeName}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.custCode' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('custCode')(<Input maxLength={100} style={{ width: 120 }} />)}
              </Form.Item>
            </Row>

            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custApplyManage.accountType' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('custAccountTypeCode')(
                  <Select style={{ width: 120 }} allowClear>
                    {custAccountTypeAll.map(item => (
                      <Select.Option key={item.id} value={item.parmCode}>
                        {item.parmShowValue}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>

              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.custName' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('custName')(<Input maxLength={100} style={{ width: 120 }} />)}
              </Form.Item>
            </Row>

            <Row>
              <Form.Item
                label={formatMessage({ id: 'form.custApplyManage.custType' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('custTypeCode')(
                  <Select style={{ width: 120 }} allowClear>
                    {custTypeAll.map(item => (
                      <Select.Option key={item.id} value={item.parmCode}>
                        {item.parmShowValue}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
              <Form.Item
                label={formatMessage({ id: 'form.custInfoManage.enable' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('enable')(
                  <Select style={{ width: 120 }} allowClear>
                    {this.stateList.map(item => (
                      <Select.Option key={item.id} value={item.value}>
                        {item.txt}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
            </Row>

            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyCustCompanyInfoHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
        </div>
      );
    };
    const tablePaginationOnChangeEventDispatchType = 'custinfomanage/paginationCustInfoList';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'menu.custmanage.custinfomanage' })}
          tableColumns={tableColumns}
          tableDataSource={custInfoList}
          tableOperateAreaCmp={tableOperateAreaCmp()}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              { searchCondition: this.searchContent }
            )
          }
          tableTotalSize={custInfoListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
        />
      </Card>
    );
  }
}

export default connect(
  ({ custinfomanage, custoperunittypemanage, user, parmpublicparametermanage }) => ({
    custInfoList: custinfomanage.custInfoList,
    custInfoListPageSize: custinfomanage.custInfoListPageSize,
    custOperUnitTypeAll: custoperunittypemanage.custOperUnitTypeAll || [], // 经营单位类型定义数据(所有)
    custTypeAll: parmpublicparametermanage.parmPublicParameterCustType || [], // 客户性质类型(所有)
    custAccountTypeAll: parmpublicparametermanage.parmPublicParameterCustAccountType || [], // 账号类型数据(所有)
    currentUser: user.currentUser || {}, // 当前用户
  })
)(Form.create()(CustInfoManage));
