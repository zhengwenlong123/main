/* eslint-disable no-nested-ternary */
/* eslint-disable no-case-declarations */
import React, { Fragment } from 'react';
import { connect } from 'dva';
import { Descriptions, Switch, Divider, message, Button, Modal, Form, Input } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import UploadSingleFile from '@/components/UploadBasic/UploadSingleFile';
import UploadSingleFiles from '@/components/UploadBasic/UploadSingleFiles'; // 上传多个文件
import { isEmptyStr, isEmptyObj, formatDate } from '@/utils/utils';
import { wrapTid } from '@/utils/mdcutil';
import { accessFileUrl } from '@/defaultSettings';
import styles from './index.less';

moment.locale('zh-cn');
const uploadIsDisable = true;
const formItemLayout = {
  labelCol: { xs: { span: 24 }, sm: { span: 7 }, xl: { span: 5 } },
  wrapperCol: { xs: { span: 24 }, sm: { span: 17 }, xl: { span: 19 } },
};
/**
 * 客户信息的界面部分组件
 */

class CustInfoDetail extends React.PureComponent {
  state = {
    companyLicenseScan: '', // 营业执照副件扫描件,单张
    companyAgencyScan: '', // 组织机构代码证扫描件,单张
    companyTaxScan: '', // 税务登记证扫描件,单张
    companyOtherScan: '', // 其他证件扫描件,单张
    companyOtherScanList: [], // 其他证件扫描件地址,多张

    custRelatedCompLicenseScan: '', // 营业执照副件扫描件,单张
    custRelatedCompAgencyScan: '', // 组织机构代码证扫描件,单张
    custRelatedCompTaxScan: '', // 税务登记证扫描件,单张
    relatedCompanyOtherScan: '', // 其他证件扫描件,单张
    relatedCompanyOtherScanList: [], // 其他证件扫描件地址,多张

    legalManCardFront: '', // 企业法人证件正面,单张
    legalManCardBack: '', // 企业法人证件反面,单张
    isNotPersonal: true, // 是否是个人，默认不是，true
    custOuVisible: false,
  };

  componentWillReceiveProps(nextProps) {
    // 初始化
    const {
      custInfoDetail: { custCompRelatedCompDto: relatInfo, custCompanyInfoMergeVO: companyInfo },
      // eslint-disable-next-line no-unused-vars
      dispatch,
    } = nextProps;

    // 查询行政地区
    // const reginCodeList = [companyInfo.companyCity, companyInfo.companyDistrict];
    // dispatch({
    //   type: 'custcompanyinfomanage/getParmRegionByReginCodesUsing',
    //   payload: { reginCodeList },
    //   callback: res => {
    //     this.setState({
    //       reginName: res.data.regionFullName,
    //     });
    //   },
    // });
    // 企业资质的图片
    const companyOtherScanList = []; // 其他证件扫描件,多张
    if (
      companyInfo &&
      companyInfo.custCompanyDescDtoList &&
      companyInfo.custCompanyDescDtoList.length > 0
    ) {
      companyInfo.custCompanyDescDtoList.forEach(val => {
        companyOtherScanList.push(val.custPackingListAddr);
      });
    }

    // 相关企业的图片
    const relatedCompanyOtherScanList = []; // 其他证件扫描件,多张
    if (
      relatInfo &&
      relatInfo.custCompRelatedCompDescDtoList &&
      relatInfo.custCompRelatedCompDescDtoList.length > 0
    ) {
      relatInfo.custCompRelatedCompDescDtoList.forEach(val => {
        relatedCompanyOtherScanList.push(val.custPackingListAddr);
      });
    }

    this.setState({
      companyLicenseScan:
        companyInfo && companyInfo.companyLicenseScan
          ? `${accessFileUrl}/${companyInfo.companyLicenseScan}`
          : '', // 营业执照副件扫描件,单张
      companyAgencyScan:
        companyInfo && companyInfo.companyAgencyScan
          ? `${accessFileUrl}/${companyInfo.companyAgencyScan}`
          : '', // 组织机构代码证扫描件,单张
      companyTaxScan:
        companyInfo && companyInfo.companyTaxScan
          ? `${accessFileUrl}/${companyInfo.companyTaxScan}`
          : '', // 税务登记证扫描件,单张
      companyOtherScanList, // 其他证件扫描件,多张

      custRelatedCompLicenseScan:
        relatInfo && relatInfo.custRelatedCompLicenseScan
          ? `${accessFileUrl}/${relatInfo.custRelatedCompLicenseScan}`
          : '', // 营业执照副件扫描件,单张
      custRelatedCompAgencyScan:
        relatInfo && relatInfo.custRelatedCompAgencyScan
          ? `${accessFileUrl}/${relatInfo.custRelatedCompAgencyScan}`
          : '', // 组织机构代码证扫描件,单张
      custRelatedCompTaxScan:
        relatInfo && relatInfo.custRelatedCompTaxScan
          ? `${accessFileUrl}/${relatInfo.custRelatedCompTaxScan}`
          : '', // 税务登记证扫描件,单张
      relatedCompanyOtherScanList, // 其他证件扫描件,多张

      legalManCardFront:
        companyInfo && companyInfo.legalManCardFront
          ? `${accessFileUrl}/${companyInfo.legalManCardFront}`
          : '', // 企业法人证件正面,单张
      legalManCardBack:
        companyInfo && companyInfo.legalManCardBack
          ? `${accessFileUrl}/${companyInfo.legalManCardBack}`
          : '', // 企业法人证件反面,单张
    });
  }

  redStar = node => (
    <span>
      <span className={styles.redStar}>*</span>
      {node}
    </span>
  );

  /**
   * 文件上传UploadFile组件回调函数
   */
  uploadFileCallback = (uploadSign, newFile) => {
    const obj = {};
    if ((newFile && newFile.type.indexOf('image') >= 0) || newFile === '') {
      obj[uploadSign] = newFile;
      this.setState(obj);
    } else if (newFile) {
      message.warn(formatMessage({ id: 'form.common.UploadTypeError' }));
    }
  };

  // 申请单 有效/无效
  enableStatus = checked => {
    const { custInfoDetail, getInfo, dispatch } = this.props;
    const para = {
      id: custInfoDetail.custCompanyInfoMergeVO.id,
      enable: checked ? 1 : 0,
    };
    // 更新-客户信息 有效/无效
    dispatch({
      type: 'custinfomanage/updateCustApplyInfoRfStatus',
      payload: para,
      callback: res => {
        console.log(res);
        // 查询详情
        getInfo();
      },
    });
  };

  showModal = () => {
    this.setState({
      custOuVisible: true,
    });
  };

  /**
   * 表单提交点击事件,获取表单填写值
   */
  modalSubmit = async () => {
    const {
      custInfoDetail,
      currentUser,
      form: { getFieldsValue },
      dispatch,
      getInfo,
    } = this.props;
    const values = getFieldsValue();
    if (isEmptyStr(custInfoDetail.custCompanyInfoMergeVO.custOuId)) {
      // 新增
      const para = {
        createUname: currentUser.name,
        createUid: currentUser.id,
        custCode: custInfoDetail.custCompanyInfoMergeVO.custCode,
        custOuId: values.custOuId,
        tid: custInfoDetail.custCompanyInfoMergeVO.tid
          ? custInfoDetail.custCompanyInfoMergeVO.tid
          : wrapTid().tid, // 租户id,
      };
      // 新增客户实体ID
      dispatch({
        type: 'custinfomanage/addCustApplyInfOuId',
        payload: {
          ...para,
        },
        callback: res => {
          console.log(res);
          this.handleCancel();
          getInfo();
        },
      });
    } else {
      // 更新
      const para = {
        modifyUname: currentUser.name,
        modifyUid: currentUser.id,
        custCode: custInfoDetail.custCompanyInfoMergeVO.custCode,
        custOuId: values.custOuId,
        tid: custInfoDetail.custCompanyInfoMergeVO.tid
          ? custInfoDetail.custCompanyInfoMergeVO.tid
          : wrapTid().tid, // 租户id,
      };
      // 更新客户实体ID
      dispatch({
        type: 'custinfomanage/updateCustApplyInfOuId',
        payload: {
          ...para,
        },
        callback: res => {
          console.log(res);
          this.handleCancel();
          getInfo();
        },
      });
    }
  };

  handleCancel = () => {
    this.setState({
      custOuVisible: false,
    });
  };

  render() {
    const {
      form: { getFieldDecorator },
      custInfoDetail,
      tianYCInfo,
    } = this.props;
    const {
      // 企业资质
      companyLicenseScan, // 营业执照副件扫描件,单张
      companyAgencyScan, // 组织机构代码证扫描件,单张
      companyTaxScan, // 税务登记证扫描件,单张
      companyOtherScan, // 其他证件扫描件,单张
      companyOtherScanList, // 其他证件扫描件,多张
      // 相关企业图片
      custRelatedCompLicenseScan, // 营业执照副件扫描件,单张
      custRelatedCompAgencyScan, // 组织机构代码证扫描件,单张
      custRelatedCompTaxScan, // 税务登记证扫描件,单张
      relatedCompanyOtherScan, // 其他证件扫描件,单张
      relatedCompanyOtherScanList, // 其他证件扫描件地址,单张

      legalManCardFront, // 企业法人证件正面,单张
      legalManCardBack, // 企业法人证件反面,单张
      // reginName, // 行政区域
      isNotPersonal,
      custOuVisible,
    } = this.state;
    if (custInfoDetail.custCompanyInfoMergeVO.custTypeCode === '个人') {
      this.setState({ isNotPersonal: false });
    } else {
      this.setState({ isNotPersonal: true });
    }
    const optionBtn = isEmptyStr(custInfoDetail.custCompanyInfoMergeVO.custOuId) ? (
      <Button type="link" onClick={() => this.showModal()}>
        {formatMessage({ id: 'button.common.add' })}
      </Button>
    ) : (
      <span>
        {custInfoDetail.custCompanyInfoMergeVO.custOuId}
        <Button type="link" onClick={() => this.showModal()}>
          {formatMessage({ id: 'button.common.modify' })}
        </Button>
      </span>
    );
    const custOuTitle = isEmptyStr(custInfoDetail.custCompanyInfoMergeVO.custOuId) ? (
      <span>
        {formatMessage({ id: 'button.common.add' })}
        {formatMessage({ id: 'form.custInfoManage.unityId' })}
      </span>
    ) : (
      <span>
        {formatMessage({ id: 'button.common.modify' })}
        {formatMessage({ id: 'form.custInfoManage.unityId' })}
      </span>
      );
        // 判断会计科目-公司是否必填
        const hasEbs = custInfoDetail.custCompanyInfoMergeVO.custAccountTypeCode === '内部';
    return (
      <div className={styles.productCont}>
        <Descriptions
          title={formatMessage({ id: 'form.custInfoManage.basicInfo' })}
          bordered
          column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
          size="small"
        >
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custCompanyInfoManage.custName' }))}
            span={2}
          >
            {custInfoDetail.custCompanyInfoMergeVO.custName}
          </Descriptions.Item>
          <Descriptions.Item label={this.redStar(formatMessage({ id: 'form.common.createAt' }))}>
            {formatDate(
              new Date(custInfoDetail.custCompanyInfoMergeVO.createAt),
              'yyyy-MM-dd hh:mm:ss'
            )}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(
              formatMessage({ id: 'form.custCompanyInfoManage.operUnitTypeCode' })
            )}
          >
            {custInfoDetail.custCompanyInfoMergeVO.operUnitTypeCode}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custApplyManage.custType' }))}
          >
            <p>{custInfoDetail.custCompanyInfoMergeVO.custTypeCode}</p>
            {!isEmptyObj(tianYCInfo) && isNotPersonal ? (
              <p className={styles.tianYCFont}>
                {formatMessage({ id: 'form.tianYCInfo.isMicroEnt' })}：
                {tianYCInfo.isMicroEnt === 0 ? '否' : '是'}
              </p>
            ) : null}
          </Descriptions.Item>
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custApplyManage.accountType' }))}
          >
            {custInfoDetail.custCompanyInfoMergeVO.custAccountTypeCode}
          </Descriptions.Item>

          <Descriptions.Item
            label={formatMessage({ id: 'form.custApplyManage.custAccountTypeCtagCode' })}
          >
            {custInfoDetail.custCompanyInfoMergeVO.custAccountTypeCtagShowName}
          </Descriptions.Item>
          {hasEbs && (
            <Descriptions.Item
              label={this.redStar(
                formatMessage({ id: 'form.custApplyManage.accountTitleCompanyEbs' })
              )}
            >
              {custInfoDetail.custCompanyInfoMergeVO.accountTitleCompanyEbs}
            </Descriptions.Item>
          )}

          {hasEbs && (
            <Descriptions.Item
              label={this.redStar(
                formatMessage({ id: 'form.custApplyManage.accountTitleDepartEbs' })
              )}
            >
              {custInfoDetail.custCompanyInfoMergeVO.accountTitleDepartEbs}
            </Descriptions.Item>
          )}

          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custInfoManage.custCode' }))}
          >
            {custInfoDetail.custCompanyInfoMergeVO.custCode}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custInfoManage.enableStatus' })}
            span={1}
          >
            <Switch
              checkedChildren="生效"
              unCheckedChildren="无效"
              checked={custInfoDetail.custCompanyInfoMergeVO.enable === 1}
              onClick={checked => {
                this.enableStatus(checked);
              }}
            />
          </Descriptions.Item>
          <Descriptions.Item label={formatMessage({ id: 'form.custInfoManage.unityId' })}>
            {optionBtn}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custApplyManage.custDescription' })}
            span={3}
          >
            {custInfoDetail.custCompanyInfoMergeVO.custDescription}
          </Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions
          title={formatMessage({ id: 'form.custInfoManage.companyInfo' })}
          bordered
          column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
          size="small"
        >
          <Descriptions.Item
            label={this.redStar(formatMessage({ id: 'form.custCompanyInfoManage.companyName' }))}
            span={3}
          >
            <p>{custInfoDetail.custCompanyInfoMergeVO.companyName}</p>
            {!isEmptyObj(tianYCInfo) && isNotPersonal && (
              <p className={styles.tianYCFont}>{tianYCInfo.name}</p>
            )}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.custShortName' })}
          >
            {custInfoDetail.custCompanyInfoMergeVO.custShortName}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custApplyManage.registeredCapital' })}
            span={2}
          >
            <p>
              {custInfoDetail.custCompanyInfoMergeVO.custRegisteredCapital}
              &nbsp;
              {custInfoDetail.custCompanyInfoMergeVO.custCurrencyCode}
            </p>
            {!isEmptyObj(tianYCInfo) && isNotPersonal && (
              <p className={styles.tianYCFont}>
                {tianYCInfo.regCapital}
                {/* &nbsp;
                {tianYCInfo.regCapitalCurrency} */}
              </p>
            )}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyCode' })}
          >
            <p>{custInfoDetail.custCompanyInfoMergeVO.companyCode}</p>
            {!isEmptyObj(tianYCInfo) && isNotPersonal && (
              <p className={styles.tianYCFont}>{tianYCInfo.creditCode}</p>
            )}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencySn' })}
          >
            <p>
              {custInfoDetail.custCompanyInfoMergeVO.companyAgencySn
                ? custInfoDetail.custCompanyInfoMergeVO.companyAgencySn
                : '无'}
            </p>
            {!isEmptyObj(tianYCInfo) && isNotPersonal && (
              <p className={styles.tianYCFont}>{tianYCInfo.orgNumber}</p>
            )}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxSn' })}
          >
            <p>
              {custInfoDetail.custCompanyInfoMergeVO.companyTaxSn
                ? custInfoDetail.custCompanyInfoMergeVO.companyTaxSn
                : '无'}
            </p>
            {!isEmptyObj(tianYCInfo) && isNotPersonal && (
              <p className={styles.tianYCFont}>{tianYCInfo.taxNumber}</p>
            )}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyCountryCode' })}
          >
            {custInfoDetail.custCompanyInfoMergeVO &&
              custInfoDetail.custCompanyInfoMergeVO.companyCountryCode}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyRegionCode' })}
            span={2}
          >
            {custInfoDetail.custCompanyInfoMergeVO &&
              custInfoDetail.custCompanyInfoMergeVO.companyRegionCode}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyAddressDetail' })}
            span={3}
          >
            <p>{custInfoDetail.custCompanyInfoMergeVO.companyAddressDetail}</p>
            {!isEmptyObj(tianYCInfo) && isNotPersonal && (
              <p className={styles.tianYCFont}>{tianYCInfo.regLocation}</p>
            )}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyBusiness' })}
            span={3}
          >
            <p>{custInfoDetail.custCompanyInfoMergeVO.companyBusiness}</p>
            {!isEmptyObj(tianYCInfo) && isNotPersonal && (
              <p className={styles.tianYCFont}>{tianYCInfo.businessScope}</p>
            )}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions
          className={styles.descripRow}
          column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
          size="small"
        >
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseScan' })}
          >
            {' '}
            <UploadSingleFile
              uploadFile={companyLicenseScan}
              uploadAccept="image/*"
              uploadSign="companyLicenseScan"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScan' })}
          >
            <UploadSingleFile
              uploadFile={companyAgencyScan}
              uploadAccept="image/*"
              uploadSign="companyAgencyScan"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScan' })}
          >
            <UploadSingleFile
              uploadFile={companyTaxScan}
              uploadAccept="image/*"
              uploadSign="companyTaxScan"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>

          <Descriptions.Item
            label={formatMessage({ id: 'form.custApplyManage.otherCardType' })}
            span={3}
          >
            <UploadSingleFiles
              uploadFile={companyOtherScan}
              uploadFileList={companyOtherScanList}
              uploadAccept="image/*"
              uploadSign="companyOtherScan"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
        </Descriptions>
        <Divider />
        {custInfoDetail.custCompRelatedCompDto && custInfoDetail.custCompRelatedCompDto.id ? (
          <Fragment>
            <Descriptions
              title={formatMessage({ id: 'form.custInfoManage.affiliatedCompanyInfo' })}
              bordered
              column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 2 }}
              size="small"
            >
              <Descriptions.Item
                label={this.redStar(
                  formatMessage({ id: 'form.custApplyManage.affiliatedCompanyName' })
                )}
              >
                <p>
                  {custInfoDetail.custCompRelatedCompDto.custRelatedCompName
                    ? custInfoDetail.custCompRelatedCompDto.custRelatedCompName
                    : '无'}
                </p>
                {!isEmptyObj(tianYCInfo) && !isNotPersonal && (
                  <p className={styles.tianYCFont}>{tianYCInfo.name}</p>
                )}
              </Descriptions.Item>
              <Descriptions.Item
                label={this.redStar(
                  formatMessage({ id: 'form.custApplyManage.affiliatedCompanyCode' })
                )}
              >
                {custInfoDetail.custCompRelatedCompDto.custRelatedCompCode
                  ? custInfoDetail.custCompRelatedCompDto.custRelatedCompCode
                  : '无'}
                {!isEmptyObj(tianYCInfo) && !isNotPersonal && (
                  <p className={styles.tianYCFont}>{tianYCInfo.creditCode}</p>
                )}
              </Descriptions.Item>
            </Descriptions>
            <Descriptions
              className={styles.descripRow}
              column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
            >
              <Descriptions.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyLicenseScan' })}
              >
                <UploadSingleFile
                  uploadFile={custRelatedCompLicenseScan}
                  uploadAccept="image/*"
                  uploadSign="custRelatedCompLicenseScan"
                  uploadIsDisable={uploadIsDisable}
                  // bodyStyle={{ width: '100px', height: '100px' }}
                  uploadFileCallback={this.uploadFileCallback}
                />
              </Descriptions.Item>
              <Descriptions.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencyScan' })}
              >
                <UploadSingleFile
                  uploadFile={custRelatedCompAgencyScan}
                  uploadAccept="image/*"
                  uploadSign="custRelatedCompAgencyScan"
                  uploadIsDisable={uploadIsDisable}
                  // bodyStyle={{ width: '100px', height: '100px' }}
                  uploadFileCallback={this.uploadFileCallback}
                />
              </Descriptions.Item>
              <Descriptions.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.companyTaxScan' })}
              >
                <UploadSingleFile
                  uploadFile={custRelatedCompTaxScan}
                  uploadAccept="image/*"
                  uploadSign="custRelatedCompTaxScan"
                  uploadIsDisable={uploadIsDisable}
                  // bodyStyle={{ width: '100px', height: '100px' }}
                  uploadFileCallback={this.uploadFileCallback}
                />
              </Descriptions.Item>

              <Descriptions.Item
                label={formatMessage({ id: 'form.custApplyManage.otherCardType' })}
                span={3}
              >
                <UploadSingleFiles
                  uploadFile={relatedCompanyOtherScan}
                  uploadFileList={relatedCompanyOtherScanList}
                  uploadAccept="image/*"
                  uploadSign="relatedCompanyOtherScan"
                  uploadIsDisable={uploadIsDisable}
                  uploadFileCallback={this.uploadFileCallback}
                  // bodyStyle={{ width: '100px', height: '100px' }}
                />
              </Descriptions.Item>
            </Descriptions>
          </Fragment>
        ) : null}

        <Divider />
        <Descriptions
          title={formatMessage({ id: 'form.custInfoManage.legalManInfo' })}
          bordered
          column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 2 }}
          size="small"
        >
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.legalManName' })}
          >
            <p>
              {custInfoDetail.custCompanyInfoMergeVO.legalManName
                ? custInfoDetail.custCompanyInfoMergeVO.legalManName
                : '无'}
            </p>
            {!isEmptyObj(tianYCInfo) && (
              <p className={styles.tianYCFont}>{tianYCInfo.legalPersonName}</p>
            )}
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardNo' })}
          >
            {custInfoDetail.custCompanyInfoMergeVO.legalManCardNo
              ? `${custInfoDetail.custCompanyInfoMergeVO.legalManCardType}： ${custInfoDetail.custCompanyInfoMergeVO.legalManCardNo}`
              : '无'}

            {/* {renderCardType()} {custInfoDetail.custCompanyInfoMergeVO.legalManCardNo} */}
          </Descriptions.Item>
        </Descriptions>
        <Descriptions
          className={styles.descripRow}
          column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 1 }}
          size="small"
        >
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardFront' })}
          >
            <UploadSingleFile
              uploadFile={legalManCardFront}
              uploadAccept="image/*"
              uploadSign="legalManCardFront"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
          <Descriptions.Item
            label={formatMessage({ id: 'form.custCompanyInfoManage.legalManCardBack' })}
          >
            <UploadSingleFile
              uploadFile={legalManCardBack}
              uploadAccept="image/*"
              uploadSign="legalManCardBack"
              uploadIsDisable={uploadIsDisable}
              // bodyStyle={{ width: '100px', height: '100px' }}
              uploadFileCallback={this.uploadFileCallback}
            />
          </Descriptions.Item>
        </Descriptions>
        <Modal
          title={custOuTitle}
          visible={custOuVisible}
          onOk={this.modalSubmit}
          onCancel={this.handleCancel}
        >
          <div>
            <Form layout="horizontal" labelAlign="right" {...formItemLayout}>
              <Form.Item label={formatMessage({ id: 'form.custInfoManage.unityId' })}>
                {getFieldDecorator('custOuId', {
                  rules: [
                    {
                      required: true,
                      message: formatMessage({ id: 'form.custInfoManage.unityId.placeholder' }),
                    },
                  ],
                })(<Input maxLength={30} style={{ width: '100%' }} />)}
              </Form.Item>
            </Form>
          </div>
        </Modal>
      </div>
    );
  }
}

export default connect(({ custinfomanage, custapplymanage, user }) => ({
  custInfoDetail: custinfomanage.custInfoDetail || {}, // 客户详情信息
  tianYCInfo: custapplymanage.tianYCInfo || {}, // 天眼查信息企业基本信息
  currentUser: user.currentUser || {},
}))(
  Form.create({
    mapPropsToFields(props) {
      const {
        custInfoDetail: { custCompanyInfoMergeVO: mue },
      } = props;
      return {
        custOuId: Form.createFormField({ value: mue ? mue.custOuId : '' }),
      };
    },
  })(CustInfoDetail)
);
