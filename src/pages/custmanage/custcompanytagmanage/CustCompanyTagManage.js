/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Input, Select, Card } from 'antd';
import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import CustCompanyTagUpkeep from './CustCompanyTagUpkeep';
import styles from './index.less';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 企业标签管理组件
 */
class CustCompanyTagManage extends MdcManageBasic {
  constructor(props) {
    super(props);

    // 企业标签类型列表字段定义
    this.tableColumns_CustCompanyTagType = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'companyTagTypeCode',
        key: 'companyTagTypeCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'companyTagTypeName',
        key: 'companyTagTypeName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.shortName' }),
        dataIndex: 'companyTagTypeShortName',
        key: 'companyTagTypeShortName',
        align: 'center',
      },
      // { title: '描述', dataIndex: 'companyTagTypeDesc', key: 'companyTagTypeDesc', align: 'center'},
      {
        title: formatMessage({ id: 'form.common.enable' }),
        key: 'enable',
        align: 'center',
        render: (text, record) => <span>{status[record.enable]}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.custCompanyTagManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.custCompanyTagManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'custcompanytagmanage/updateCustCompanyTagTypeEnable',
                  this.searchContent
                )
              }
            >
              {record.enable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
  }

  /**
   * 响应点击处理
   */
  hookProcess = (type, record) => {
    const { dispatch } = this.props;
    if (type === 3) {
      record.enable = record.enable === 1 ? 0 : 1;
    } else {
      // 获取-当前选择企业标签类型内企业标签值定义数据(所有)
      dispatch({ type: 'custcompanytagmanage/getCustCompanyTagDefineAll', payload: record });
    }
    return record;
  };

  /**
   * 加载默认企业标签类型数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({ type: 'custcompanytagmanage/getCustCompanyTagType' });
  }

  /**
   * 根据条件搜索品牌
   */
  onFuzzyCompanyTagHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'custcompanytagmanage/paginationCustCompanyTagType',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableCurrentPage,
    } = this.state;
    const {
      custCompanyTagTypeList,
      custCompanyTagTypeListPageSize,
      formItemLayout,
      tailFormItemLayout,
    } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.common.code' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('companyTagTypeCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.name' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('companyTagTypeName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.enable' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('enable')(
                <Select style={{ width: 80 }} allowClear>
                  <Select.Option value="1">
                    {formatMessage({ id: 'form.common.enabled' })}
                  </Select.Option>
                  <Select.Option value="0">
                    {formatMessage({ id: 'form.common.disabled' })}
                  </Select.Option>
                </Select>
              )}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyCompanyTagHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.custCompanyTagManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };

    const tablePaginationOnChangeEventDispatchType =
      'custcompanytagmanage/paginationCustCompanyTagType';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'menu.custmanage.custcompanytagmanage' })}
          tableColumns={this.tableColumns_CustCompanyTagType}
          tableDataSource={custCompanyTagTypeList}
          tableOperateAreaCmp={tableOperateAreaCmp()}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              { searchCondition: this.searchContent }
            )
          }
          tableTotalSize={custCompanyTagTypeListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <CustCompanyTagUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
              modalSearchCondition={this.searchContent}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ custcompanytagmanage }) => ({
  custCompanyTagTypeList: custcompanytagmanage.custCompanyTagTypeList || [],
  custCompanyTagTypeListPageSize: custcompanytagmanage.custCompanyTagTypeListPageSize || 0,
}))(Form.create()(CustCompanyTagManage));
