/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Button, Input, Checkbox, message } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 企业标签值定义列表项-维护组件
 */
@ValidationFormHoc
class CustCompanyTagTableItemUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      validationForm,
      dispatch,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.enable ? (values.enable = 1) : (values.enable = 0);

    // 维护
    if (mut === 2) {
      values.id = mue.id;
      dispatch({
        type: 'custcompanytagmanage/updateCustCompanyTagDefine',
        payload: values,
        callback: this.onCallback,
      });
    }
    // 新增
    if (mut === 10) {
      dispatch({
        type: 'custcompanytagmanage/addCustCompanyTagDefine',
        payload: values,
        callback: this.onCallback,
      });
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalUpkeepType: mut,
      modalVisibleOnChange,
      form: { getFieldDecorator },
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyTagTableItemUpkeep.companyTagTypeCode' })}
            >
              {getFieldDecorator('companyTagTypeCode', { rules: [{ required: true }] })(
                <Input disabled />
              )}
            </Form.Item>
            {mut === 10 ? null : (
              <Form.Item label={formatMessage({ id: 'form.common.code' })}>
                {getFieldDecorator('companyTagTypeItemCode', { rules: [{ required: true }] })(
                  <Input disabled />
                )}
              </Form.Item>
            )}
            <Form.Item label={formatMessage({ id: 'form.common.value' })}>
              {getFieldDecorator('companyTagTypeItemValue', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.custCompanyTagTable.companyvalueMust' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.custCompanyTagTable.companyTagTypeItemDesc' })}
            >
              {getFieldDecorator('companyTagTypeItemDesc')(
                <Input disabled={mut === 1} maxLength={50} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('enable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.whetherEnabled' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue, companyTagTypeCode } = props;

      return {
        companyTagTypeCode: Form.createFormField({
          value: mue ? mue.companyTagTypeCode : companyTagTypeCode || '',
        }),
        companyTagTypeItemCode: Form.createFormField({
          value: mue ? mue.companyTagTypeItemCode : '',
        }),
        companyTagTypeItemValue: Form.createFormField({
          value: mue ? mue.companyTagTypeItemValue : '',
        }),
        companyTagTypeItemDesc: Form.createFormField({
          value: mue ? mue.companyTagTypeItemDesc : '',
        }),
        enable: Form.createFormField({ value: mue ? mue.enable === 1 : true }),
      };
    },
  })(CustCompanyTagTableItemUpkeep)
);
