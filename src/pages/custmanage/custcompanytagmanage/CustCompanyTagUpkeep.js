/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import moment from 'moment';
import { Tabs } from 'antd';
import CustCompanyTagTypeUpkeep from './CustCompanyTagTypeUpkeep';
import CustCompanyTagTableUpkeep from './CustCompanyTagTableUpkeep';

moment.locale('zh-cn');
/**
 * 企业标签管理维护组件
 */
class CustCompanyTagUpkeep extends React.PureComponent {
  render() {
    const { modalUpkeepType: mut, modalUpkeepEntity: mue } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        {mut === 10 ? (
          <CustCompanyTagTypeUpkeep {...this.props} />
        ) : (
          <Tabs tabPosition="left">
            <Tabs.TabPane
              key="0"
              tab={formatMessage({ id: 'menu.custmanage.custcompanytagmanage.tagType' })}
            >
              <CustCompanyTagTypeUpkeep {...this.props} />
            </Tabs.TabPane>
            <Tabs.TabPane
              key="1"
              tab={formatMessage({ id: 'menu.custmanage.custcompanytagmanage.tagTable' })}
            >
              <CustCompanyTagTableUpkeep
                companyTagTypeCode={mue.companyTagTypeCode}
                actionType={mut}
              />
            </Tabs.TabPane>
          </Tabs>
        )}
      </div>
    );
  }
}

export default connect()(CustCompanyTagUpkeep);
