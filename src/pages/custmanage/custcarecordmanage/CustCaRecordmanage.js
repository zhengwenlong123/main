import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Card } from 'antd';
import moment from 'moment';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import CustCaRecordUpkeep from './CustCaRecordUpkeep';

moment.locale('zh-cn');
const status = [
  formatMessage({ id: 'form.common.disabled' }),
  formatMessage({ id: 'form.common.enabled' }),
];
/**
 * 客户CA记录管理组件
 */
class CustCaRecordManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'id',
        key: 'id',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'name',
        key: 'name',
        align: 'center',
        // render: (text, record) => <span>{record.enable === 1 ? '有效' : '失效'}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  this.hookProcess(1, record),
                  formatMessage({ id: 'button.custCaRecordManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.custCaRecordManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'custcarecordmanage/updateCustCompanyInfoEnable'
                )
              }
            >
              {record.enable === 1 ? status[0] : status[1]}
            </a>
          </span>
        ),
      },
    ];
    super(props, tableColumns);
  }

  /**
   * 加载默认客户CA记录数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-客户CA记录数据(列表)
    dispatch({ type: 'custcarecordmanage/getCustCaRecord' });
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
    } = this.state;
    const { custCaRecordList, custCaRecordListPageSize } = this.props;
    const tableOperateAreaCmp = (
      <Button
        type="primary"
        onClick={() =>
          this.onClickActionExecuteEvent(
            10,
            null,
            formatMessage({ id: 'button.custCaRecordManage.add' })
          )
        }
      >
        {formatMessage({ id: 'button.common.add' })}
      </Button>
    );
    const tablePaginationOnChangeEventDispatchType =
      'custcarecordmanage/paginationCustCaRecordManage';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'menu.custmanage.custcarecordmanage' })}
          tableColumns={tableColumns}
          tableDataSource={custCaRecordList}
          tableOperateAreaCmp={tableOperateAreaCmp}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType
            )
          }
          tableTotalSize={custCaRecordListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <CustCaRecordUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ custcarecordmanage }) => ({
  custCaRecordList: custcarecordmanage.custCaRecordList,
  custCaRecordListPageSize: custcarecordmanage.custCaRecordListPageSize,
}))(CustCaRecordManage);
