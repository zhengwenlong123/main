import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Button, Input, message } from 'antd';
import moment from 'moment';
// import {
//   formatParmRegionToAntdCascaderCmp,
//   formatParmRegionToAntdTreeSelectCmp,
//   verifyUnique,
//   fileRelationPathReg,
// } from '@/utils/mdcutil';
// import UploadSingleFile from '@/components/UploadBasic/UploadSingleFile';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
// import request from '@/utils/request';
// import { fileUrl, accessFileUrl } from '@/defaultSettings';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 客户CA记录维护组件
 */
@ValidationFormHoc
class CustCaRecordUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, ms: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, ms: { span: 20 } },
    },
  };

  /**
   * 初始化state中的文件相关参数值
   */
  componentDidMount() {
    // const { modalUpKeepEntity: mue } = this.propss
  }

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.info(formatMessage({ id: 'form.common.optionError' }));
    }
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = async () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      validationForm,
      dispatch,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();

    values.enable ? (values.enable = 1) : (values.enable = 0);

    switch (mut) {
      case 2: // 维护
        values.id = mue.id;
        dispatch({
          type: 'custcarecordmanage/updateCustCaRecord',
          payload: values,
          callback: this.onCallback,
        });
        break;
      case 10: // 新增
        dispatch({
          type: 'custcarecordmanage/addCustCaRecord',
          payload: values,
          callback: this.onCallback,
        });
        break;
      default:
        break;
    }
  };

  render() {
    const { formItemLayout } = this.state;
    const {
      modalVisibleOnChange,
      modalUpkeepType: mut,
      // modalUpkeepEntity: mue,
      form: { getFieldDecorator },
    } = this.props;

    // mut 1详情； 2维护； 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('custName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={100} />)}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(() => ({}))(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        custName: Form.createFormField({ value: mue ? mue.custName : '' }),
      };
    },
  })(CustCaRecordUpkeep)
);
