/* eslint-disable no-nested-ternary */
/* eslint-disable no-case-declarations */
import React from 'react';
import { connect } from 'dva';
import { Collapse, Icon, Empty } from 'antd';
import moment from 'moment';
// import { formatMessage } from 'umi/locale';
import { isEmptyStr } from '@/utils/utils';
// import { wrapTid } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
const { Panel } = Collapse;
// const customPanelStyle = {
//   background: '#f7f7f7',
//   borderRadius: 4,
//   marginBottom: 24,
//   border: 0,
//   overflow: 'hidden',
// };
/**
 * 企业天眼查经营异常列表界面部分组件
 */

class CompanyRisk extends React.PureComponent {
  state = {};

  componentDidMount() {
    // 初始化
  }

  // 去掉string中的所有html标签
  prettierTitle = str => str.replace(/<[^>]+>/g, '');

  // 渲染每一条数据
  renderItem = obj => {
    // 一级数据个数为count，二级是total，三级是riskCount，通过这个可以区分各级数据
    let myTitle = '';
    // let dataStage = 1; // 数据等级，默认一级
    if (!isEmptyStr(obj.count)) {
      // 一级分类数据
      // eslint-disable-next-line no-param-reassign
      obj.name = this.prettierTitle(obj.name); // 去掉string中的所有html标签
      if (obj.type === 1) {
        myTitle = (
          <h3 className={`${styles.redFont} ${styles.ristTitle}`}>{`${obj.name}(${obj.count})`}</h3>
        );
      } else {
        myTitle = <h3>{`${obj.name}(${obj.count})`}</h3>;
      }
    } else if (!isEmptyStr(obj.total)) {
      // 二级分类数据
      // dataStage = 2;
      // eslint-disable-next-line no-param-reassign
      obj.title = this.prettierTitle(obj.title); // 去掉string中的所有html标签
      let tagElement = '';
      if (obj.tag === '高风险信息') {
        tagElement = <span className={`${styles.pading2px} ${styles.redBorder}`}>{obj.tag}</span>;
      } else if (obj.tag === '警示信息') {
        tagElement = (
          <span className={`${styles.pading2px} ${styles.orangeBorder}`}>{obj.tag}</span>
        );
      } else if (obj.tag === '提示信息') {
        tagElement = <span className={`${styles.pading2px} ${styles.khakiBorder}`}>{obj.tag}</span>;
      }
      myTitle = (
        <span>
          <h3 className={`${styles.ristTitle}`}>{`${obj.title}(${obj.total})`}</h3>
          {tagElement}
        </span>
      );
    } else if (!isEmptyStr(obj.riskCount)) {
      // 三级分类数据
      // dataStage = 3;
      // eslint-disable-next-line no-param-reassign
      obj.title = this.prettierTitle(obj.title); // 去掉string中的所有html标签
      const title3 = obj.title.split('该公司');
      myTitle = (
        <span className={`${styles.pading8px}`}>
          <span className={styleMedia.blackFont}>该公司</span>
          <span className={`${styles.redFont}`}>{`${title3[1]}(${obj.riskCount})`}</span>
        </span>
      );
    }
    const myKey = obj.id ? obj.id : obj.title ? obj.title : obj.type;
    if (obj.list && obj.list.length > 0) {
      // 包含子数组数据
      return (
        <Panel header={myTitle} key={myKey}>
          <Collapse>{obj.list.map(item => this.renderItem(item))}</Collapse>
        </Panel>
      );
    }
    if (obj.count === 0) {
      // 不包含子数组数据
      return <Panel header={myTitle} key={myKey} />;
    }
    return <p className={`${styles.pading2px} ${styles.riskItem}`}> {myTitle} </p>;
  };

  render() {
    const { tianYCRisk } = this.props;
    return (
      <div>
        {tianYCRisk.length > 0 ? (
          <Collapse
            bordered={false}
            expandIcon={({ isActive }) => <Icon type="caret-right" rotate={isActive ? 90 : 0} />}
          >
            {tianYCRisk.map(item => this.renderItem(item))}
          </Collapse>
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        )}
      </div>
    );
  }
}

export default connect(({ custapplymanage }) => ({
  tianYCRisk: custapplymanage.tianYCRisk || [], // 客户天眼查企业风险
}))(CompanyRisk);
