/* eslint-disable no-nested-ternary */
/* eslint-disable no-case-declarations */
import React from 'react';
import { connect } from 'dva';
import { Card } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
// import { isEmptyStr, isEmptyObj } from '@/utils/utils';
// import { wrapTid } from '@/utils/mdcutil';
import CompanyInfo from './companyInfo';
import CompanyRisk from './companyRisk';
import AbnormalList from './abnormalList';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 客户信息的界面部分组件
 */

class TianYCTab extends React.PureComponent {
  componentDidMount() {
    // 初始化
    const {
      // custApplyInfo: { custRfCompanyInfoAddDto: companyInfo },
      companyCode,
      dispatch,
    } = this.props;
    // 获取-根据客户信用码获取天眼查企业信息
    dispatch({
      type: 'custapplymanage/getTianYCInfoByCompanyCode',
      payload: {
        code: companyCode,
        // code: '91120116749118895P',
        sourceType: 1,
      },
      callback: res => {
        // 获取-企业天眼查经营异常列表
        if (res && res.id) {
          // 获取-企业天眼查经营异常列表
          dispatch({
            type: 'custapplymanage/getTianYCAbnormalListById',
            payload: {
              id: res.id,
              // id: '123456',
              sourceType: 1,
              pageNum: 1,
              pageSize: 10,
            },
          });
          // 获取-客户天眼查企业风险
          dispatch({
            type: 'custapplymanage/getTianYCRiskById',
            payload: {
              id: res.id,
              sourceType: 1,
            },
          });
        }
      },
    });
  }

  componentWillReceiveProps(nextProps) {
    // console.log('nextProps', nextProps);
    const {
      // custApplyInfo: { custRfCompanyInfoAddDto: companyInfo },
      companyCode,
      dispatch,
    } = this.props;
    if (companyCode !== nextProps.companyCode) {
      if (nextProps.companyCode) {
        // 获取-根据客户信用码获取天眼查企业信息
        dispatch({
          type: 'custapplymanage/getTianYCInfoByCompanyCode',
          payload: {
            code: nextProps.companyCode,
            // code: '91120116749118895P',
            sourceType: 1,
          },
          callback: res => {
            // 获取-企业天眼查经营异常列表
            if (res && res.id) {
              // 获取-企业天眼查经营异常列表
              dispatch({
                type: 'custapplymanage/getTianYCAbnormalListById',
                payload: {
                  id: res.id,
                  // id: '123456',
                  sourceType: 1,
                  pageNum: 1,
                  pageSize: 10,
                },
              });
              // 获取-客户天眼查企业风险
              dispatch({
                type: 'custapplymanage/getTianYCRiskById',
                payload: {
                  id: res.id,
                  sourceType: 1,
                },
              });
            }
          },
        });
      }
    }
  }

  render() {
    return (
      <div className={styles.productCont}>
        <Card
          title={formatMessage({ id: 'form.tianYCInfo.companyInfo' })}
          key="companyInfo"
          bordered={false}
          headStyle={{ paddingLeft: 0, minHeight: 30 }}
        >
          <CompanyInfo />
        </Card>
        <Card
          title={formatMessage({ id: 'form.tianYCInfo.companyAbnormal' })}
          key="companyAbnormal"
          bordered={false}
          headStyle={{ paddingLeft: 0, minHeight: 30 }}
        >
          <AbnormalList />
        </Card>
        <Card
          title={formatMessage({ id: 'form.tianYCInfo.companyRisk' })}
          key="companyRisk"
          bordered={false}
          headStyle={{ paddingLeft: 0, minHeight: 30 }}
        >
          <CompanyRisk />
        </Card>
      </div>
    );
  }
}

export default connect(({ custapplymanage }) => ({
  custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户申请单详情信息
}))(TianYCTab);
