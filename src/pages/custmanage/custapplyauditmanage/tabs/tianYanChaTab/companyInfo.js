/* eslint-disable no-nested-ternary */
/* eslint-disable no-case-declarations */
import React from 'react';
import { connect } from 'dva';
import { Descriptions, Empty } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
// import { isEmptyStr, isEmptyObj } from '@/utils/utils';
// import { wrapTid } from '@/utils/mdcutil';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 天眼查的企业基本信息界面部分组件
 */

class CompanyInfo extends React.PureComponent {
  state = {};

  componentDidMount() {
    // 初始化
  }

  render() {
    const { tianYCInfo } = this.props;
    return (
      <div className={styles.productCont}>
        {tianYCInfo.id ? (
          <Descriptions column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 1 }} size="small">
            <Descriptions.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyName' })}
            >
              {tianYCInfo.name}
            </Descriptions.Item>
            <Descriptions.Item label={formatMessage({ id: 'form.tianYCInfo.percentileScore' })}>
              {tianYCInfo.percentileScore}（万分制）
            </Descriptions.Item>
            <Descriptions.Item label={formatMessage({ id: 'form.tianYCInfo.legalPersonName' })}>
              {tianYCInfo.legalPersonName}
            </Descriptions.Item>
            <Descriptions.Item
              label={formatMessage({ id: 'form.custApplyManage.registeredCapital' })}
            >
              {tianYCInfo.regCapital}
              {tianYCInfo.regCapitalCurrency}
            </Descriptions.Item>
            <Descriptions.Item label={formatMessage({ id: 'form.common.enable' })}>
              {tianYCInfo.regStatus}
            </Descriptions.Item>
            <Descriptions.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyCode' })}
            >
              {tianYCInfo.creditCode}
            </Descriptions.Item>
            <Descriptions.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyAgencySn' })}
            >
              {tianYCInfo.orgNumber}
            </Descriptions.Item>
            <Descriptions.Item label={formatMessage({ id: 'form.tianYCInfo.taxNumber' })}>
              {tianYCInfo.taxNumber}
            </Descriptions.Item>
            <Descriptions.Item label={formatMessage({ id: 'form.tianYCInfo.toTime' })}>
              {tianYCInfo.toTime}
            </Descriptions.Item>
            <Descriptions.Item label={formatMessage({ id: 'form.tianYCInfo.regInstitute' })}>
              {tianYCInfo.regInstitute}
            </Descriptions.Item>

            <Descriptions.Item label={formatMessage({ id: 'form.tianYCInfo.approvedTime' })}>
              {tianYCInfo.approvedTime}
            </Descriptions.Item>
            <Descriptions.Item label={formatMessage({ id: 'form.tianYCInfo.estiblishTime' })}>
              {tianYCInfo.estiblishTime}
            </Descriptions.Item>
            <Descriptions.Item label={formatMessage({ id: 'form.tianYCInfo.regLocation' })}>
              {tianYCInfo.regLocation}
            </Descriptions.Item>
            <Descriptions.Item label={formatMessage({ id: 'form.tianYCInfo.isMicroEnt' })}>
              {tianYCInfo.isMicroEnt === 0 ? '否' : '是'}
            </Descriptions.Item>

            <Descriptions.Item
              label={formatMessage({ id: 'form.custCompanyInfoManage.companyBusiness' })}
              span={2}
            >
              {tianYCInfo.businessScope}
            </Descriptions.Item>
          </Descriptions>
        ) : (
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        )}
      </div>
    );
  }
}

export default connect(({ custapplymanage }) => ({
  tianYCInfo: custapplymanage.tianYCInfo || {}, // 天眼查信息企业基本信息
}))(CompanyInfo);
