/* eslint-disable no-nested-ternary */
/* eslint-disable no-case-declarations */
import React from 'react';
import { connect } from 'dva';
// import { Table } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
// import { isEmptyStr, isEmptyObj } from '@/utils/utils';
// import { wrapTid } from '@/utils/mdcutil';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 企业天眼查经营异常列表界面部分组件
 */

class AbnormalList extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.rowno' }),
        key: 'rowkey',
        align: 'center',
        width: 10,
        render: (text, record, index) => <span>{index}</span>,
      },
      {
        title: formatMessage({ id: 'form.tianYCInfo.putDate' }),
        dataIndex: 'putDate',
        key: 'putDate',
        align: 'center',
        width: 60,
      },
      {
        title: formatMessage({ id: 'form.tianYCInfo.putReason' }),
        dataIndex: 'putReason',
        key: 'putReason',
        align: 'center',
        width: 120,
      },
      {
        title: formatMessage({ id: 'form.tianYCInfo.putDepartment' }),
        dataIndex: 'putDepartment',
        key: 'putDepartment',
        align: 'center',
        width: 100,
      },
      {
        title: formatMessage({ id: 'form.tianYCInfo.removeDate' }),
        dataIndex: 'removeDate',
        key: 'removeDate',
        align: 'center',
        width: 60,
      },
      {
        title: formatMessage({ id: 'form.tianYCInfo.removeReason' }),
        dataIndex: 'removeReason',
        key: 'removeReason',
        align: 'center',
        width: 120,
      },
      {
        title: formatMessage({ id: 'form.tianYCInfo.removeDepartment' }),
        dataIndex: 'removeDepartment',
        key: 'removeDepartment',
        align: 'center',
        width: 100,
      },
    ];
    super(props, tableColumns);
  }

  componentDidMount() {
    // 初始化
  }

  render() {
    const { tableColumns, tableCurrentPage } = this.state;
    const { tianYCAbnormalList ,tianYCAbnormalListPageSize} = this.props;
    const tablePaginationOnChangeEventDispatchType = 'custapplymanage/paginationCustApplyAuditList';
    return (
      <div className={styles.productCont}>
        <h5>经营异常例数：{tianYCAbnormalListPageSize}</h5> 
        <MdcManageBlock
          tableColumns={tableColumns}
          tableDataSource={tianYCAbnormalList}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              { searchCondition: {} }
            )
          }
          tableTotalSize={tianYCAbnormalListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
        />
      </div>
    );
  }
}

export default connect(({ custapplymanage }) => ({
  tianYCAbnormalList: custapplymanage.tianYCAbnormalList || {}, // 企业天眼查经营异常列表
  tianYCAbnormalListPageSize:custapplymanage.tianYCAbnormalListPageSize||0,// 企业天眼查经营异常列表总数量
}))(AbnormalList);
