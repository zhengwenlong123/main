import React from 'react';
import { connect } from 'dva';
import { Card, Table } from 'antd';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import { formatDate } from '@/utils/utils';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品申请单审核记录部分组件
 */

class AuditTab extends React.PureComponent {
  tableColumns = [
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.account' }),
      dataIndex: 'verifyUserName',
      key: 'verifyUserName',
      align: 'center',
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.company' }),
      dataIndex: 'verifyUserOrgName',
      key: 'verifyUserOrgName',
      align: 'center',
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.optionTime' }),
      // dataIndex: 'createAt',
      key: 'createAt',
      align: 'center',
      render: (text, record) => <span>{formatDate(new Date(record.createAt), 'yyyy-MM-dd hh:mm:ss')}</span>,
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.optionResult' }),
      dataIndex: 'verifyResult',
      key: 'verifyResult',
      align: 'center',
      render: (text, record) => <span>{this.renderStatus(record.verifyResult)}</span>,
    },
    {
      title: formatMessage({ id: 'form.goodsApplyBillAudit.optionDesc' }),
      dataIndex: 'verifyDesc',
      key: 'verifyDesc',
      align: 'center',
    },
  ];

  /**
   * 默认加载审核历史数据
   */
  componentDidMount() {
    // const { dispatch } = this.props;
    // // 获取-根据公用参数类型定义代码[goods_rf_type]获取公用参数定义数据所有(产品申请单状态数据)
    // dispatch({
    //   type: 'parmpublicparametermanage/getGoodsRfStatus',
    //   payload: { parmTypeCode: 'goods_rf_status' },
    // });

  }

    /** 组件即将卸载 */
    componentWillUnmount() {
      const { dispatch } = this.props;
      // 清空数据
      dispatch({
        type: 'custapplymanage/changeCustApplyAuditListAll',
        payload: {}, // 客户申请单详情信息，初始化
      });
    }


  // 渲染申请单的状态
  renderStatus(rfStatus) {
    const {
      parmpublicparametermanage: { parmPublicParameterCustRfStatus = [] },
    } = this.props;
    let result = '';
    parmPublicParameterCustRfStatus.forEach(val => {
      if (val.parmCode === rfStatus) result = val.parmShowValue;
    });
    return result;
  }

  render() {
    const { custApplyAuditLogs } = this.props;
    return (
      <div className={styles.productCont}>
        <Card
          title={formatMessage({ id: 'form.goodsApplyBillAudit.log' })}
          bordered={false}
          headStyle={{ paddingLeft: 0, minHeight: 30 }}
        >
          <Table
            rowKey={record => record.id}
            dataSource={custApplyAuditLogs}
            columns={this.tableColumns}
          />
          ;
        </Card>
      </div>
    );
  }
}

export default connect(
  ({ parmpublicparametermanage, custapplymanage }) => ({
    parmpublicparametermanage,
    custApplyAuditLogs: custapplymanage.custApplyAuditLogs || {}, // 申请单审核记录
  })
)(AuditTab);
