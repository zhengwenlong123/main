/* eslint-disable react/no-string-refs */
/* eslint-disable no-return-assign */
/* eslint-disable no-unused-expressions */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
// import moment from 'moment';
import { Button, Form, Input, Select } from 'antd';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 产品品牌基本信息维护组件
 */
@ValidationFormHoc
class GoodsApplyBillAuditModal extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
  };



  /**
   * 表单提交点击事件,获取表单填写值
   */
  modalSubmit = async () => {
    const {
      commit,
      form: { getFieldsValue },
      validationForm,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    commit(values);
  };

  render() {
    const { formItemLayout } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      parmPublicParameterCustRfStatus,
      
    } = this.props;
    const auditTypeList = parmPublicParameterCustRfStatus.filter(
      v => v.parmCode === 'verify_adopt' || v.parmCode === 'verify_reject' || v.parmCode ==="bill_invalid"
    );

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form layout="horizontal" labelAlign="right" {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.goodsApplyBillAudit.result' })}>
              {getFieldDecorator('verifyResult', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.goodsApplyBillAudit.result.placeholder',
                    }),
                  },
                ],
              })(
                // eslint-disable-next-line react/jsx-no-duplicate-props
                <Select style={{ width: 80 }} allowClear style={{ width: '50%' }}>
                  {auditTypeList.map(v => (
                    <Select.Option value={v.parmCode} key={v.id}>
                      {v.parmShowValue}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.goodsApplyBillAudit.desc' })}>
              {getFieldDecorator('verifyDesc')(<Input.TextArea rows={5} maxLength={255} />)}
            </Form.Item>
          </Form>
        </div>
        <div>
          <div className={styles.modalHandleBtnArea}>
            <Button type="default" onClick={() => modalVisibleOnChange(false)}>
              {formatMessage({ id: 'button.common.cancel' })}
            </Button>
            <Button type="primary" onClick={this.modalSubmit}>
              {formatMessage({ id: 'form.goodsApplyBillAudit.audit' })}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({ user }) => ({
  currentUser: user.currentUser || {},
}))(
  Form.create({
    mapPropsToFields() {
      const mue = {};
      return {
        verifyDesc: Form.createFormField({ value: mue ? mue.verifyDesc : '' }),
        verifyResult: Form.createFormField({ value: mue ? mue.verifyResult : '' }),
      };
    },
  })(GoodsApplyBillAuditModal)
);
