/* eslint-disable no-lonely-if */
/* eslint-disable no-useless-escape */
/* eslint-disable no-return-assign */
/* eslint-disable no-param-reassign */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-console */
import React from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { formatMessage } from 'umi/locale';
import { Card, Layout, Button, message } from 'antd';
import router from 'umi/router'; // eslint-disable-line
import uuid from 'uuid';
import { wrapTid } from '@/utils/mdcutil';
import { isEmptyStr, isEmptyObj } from '@/utils/utils';
import CustApplyDetail from '../custapplymanage/CustApplyDetail';
import CustApplyAuditList from './CustApplyAuditList';
import TianYanChaTab from './tabs/tianYanChaTab';
import AuditLogTab from './tabs/auditLogTab';
import AuditModal from './AuditModal';
import ModalBasic from '@/components/ModalBasic/ModalBasic';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
// import styles from './index.less';

moment.locale('zh-cn');

const { Content, Sider } = Layout;
/**
 * 客户申请单信息维护组件,以新页面访问
 */
class CustApplyBatchAudit extends MdcManageBasic {
  constructor(props) {
    super(props);
    const self = this;
    // 初始化数据
    self.state = {
      activeKey: 'detailTab', // tab的
      auditValues: {}, // 审核model中填写的数据
      myTabList: [
        {
          key: 'detailTab',
          tab: '客户',
        },
        {
          key: 'auditLogTab',
          tab: '历史审核记录',
        },
      ],
    };
    self.detailRef = null; // 子组件的ref
  }

  /**
   * 获取申请单详情，初始化传入redux中
   */
  componentDidMount() {
    const {
      dispatch,
      location: {
        query: { id = '', type = '', tid = 10001 },
      },
    } = this.props;
    // 清空为默认
    dispatch({
      type: 'custapplymanage/changeCustApplyInfo',
      payload: {
        custRequestFormInfoAddDto: {},
        custRfCompRelatedCompAddDto: {
          custRfCompRelatedCompDescAddDtoList: [],
        },
        custRfCompanyInfoAddDto: {
          isCompanyCertMerge: 1,
          hasCompanyCode: 1,
          custRfCompanyDescAddDtoList: [],
        },
      }, // 客户申请单详情信息，初始化
    });

    /**
     * 审核
     *
     * */
    let cb = () => {}; // 查询审核列表的回调函数
    if (type === 'sigleAudit') {
      // 单个审核
      if (isEmptyStr(id)) {
        this.handleToList();
      } else {
        // 根据id查询申请单详情
        dispatch({
          type: 'custapplymanage/getCustApplyInfoByRfId',
          payload: { id, tid },
        });
        // 设置选中申请单selectedApply
        dispatch({
          type: 'custapplymanage/changeAuditSelectedApply',
          payload: { rfId: id, tid },
        });
      }
    } else {
      // 批量审核
      cb = res => {
        // 批量审核，默认选中第一项
        dispatch({
          type: 'custapplymanage/changeAuditSelectedApply',
          payload: res.content[0],
        });
      };
    }
    // 获取-客户申请列表(列表)
    dispatch({
      type: 'custapplymanage/paginationCustApplyAuditList',
      payload: {
        page: 1,
        pageSize: 20,
        searchCondition: {
          rfStatus: "'verify_waiting'",
        },
      },
      callback: cb,
    });

    /**
     * 加载默认企业信息数据
     */
    // 获取-客户申请单客户状态数据(所有)
    dispatch({
      type: 'parmpublicparametermanage/getCustRfStatus',
      payload: { parmTypeCode: 'cust_rf_status' },
    });
  }

  // 监听审核的所选择的记录，改变了rfId参数值
  componentWillReceiveProps(nextProps) {
    // console.log('nextProps', nextProps);
    const { selectedApply, dispatch } = this.props;
    if (selectedApply.rfId !== nextProps.selectedApply.rfId) {
      // 根据selectedApply.rfId查询申请单详情
      if (nextProps.selectedApply.rfId) {
        dispatch({
          type: 'custapplymanage/getCustApplyInfoByRfId',
          payload: { id: nextProps.selectedApply.rfId, tid: nextProps.selectedApply.tid },
        });
        // 根据selectedApply.rfId查询审核历史记录
        dispatch({
          type: 'custapplymanage/getCustApplyAuditLogs',
          payload: {
            searchCondition: {
              rfId: nextProps.selectedApply.rfId,
            },
          },
        });
        // 初始化天眼查数据，都为空
        // 清空天眼查
        dispatch({
          type: 'custapplymanage/changeTianYCInfo',
          payload: {},
        });
        // 企业天眼查经营异常列表
        dispatch({
          type: 'custapplymanage/changeTianYCAbnormalList',
          payload: { content: [] },
        });
        // 客户天眼查企业风险
        dispatch({
          type: 'custapplymanage/changeTianYCRisk',
          payload: [],
        });
      }
    }
  }

  /** 组件即将卸载 */
  componentWillUnmount() {
    const { dispatch } = this.props;
    // 清空审核历史记录
    dispatch({
      type: 'custapplymanage/changeCustApplyAuditLogs',
      payload: { content: [] },
    });
    // 清空selectedApply
    dispatch({
      type: 'custapplymanage/changeAuditSelectedApply',
      payload: {},
    });
    // 初始化天眼查数据，都为空
    // 清空天眼查
    dispatch({
      type: 'custapplymanage/changeTianYCInfo',
      payload: {},
    });
    // 企业天眼查经营异常列表
    dispatch({
      type: 'custapplymanage/changeTianYCAbnormalList',
      payload: { content: [] },
    });
    // 客户天眼查企业风险
    dispatch({
      type: 'custapplymanage/changeTianYCRisk',
      payload: [],
    });
  }

  /**
   * tab切换回调
   */
  tabChange = key => {
    // const { dispatch } = this.props;
    this.setState({
      activeKey: key,
    });
  };

  handleToList = () => {
    router.push('/custmanage/custapplyauditmanage');
  };

  // 打开审核弹框
  openModel = () => {
    // 首先判断申请单数据是否正确
    const flag = this.detailRef.commit();
    if (flag) {
      this.modalVisibleOnChange(true);
    } else {
      message.warn(formatMessage({ id: 'form.custApplyManage.auditInfoExit' }));
    }
  };

  // 弹出审核框的审核按钮点击事件,
  onAuditSubmit = values => {
    const self = this;
    self.setState({
      auditValues: values,
    });
    self.detailRef.onClickSubmit(); // 调用客户信息的点击事件,先保存申请单，再审核
  };

  /**
   * 更新产品申请单相关
   */
  // 客户信息修改
  custInfoSubmit = values => {
    const { custApplyInfo,dispatch } = this.props;
    // 审核时候会有外部数据，这时候要注意，不能修改过来的一些基本数据，如：申请人的信息
    values.custRequestFormInfoAddDto.createUid = custApplyInfo.custRequestFormInfoAddDto.createUid;
    values.custRequestFormInfoAddDto.createUcode = custApplyInfo.custRequestFormInfoAddDto.createUcode;
    values.custRequestFormInfoAddDto.createUname = custApplyInfo.custRequestFormInfoAddDto.createUname;

    values.custRequestFormInfoAddDto.rfRequestUserName = custApplyInfo.custRequestFormInfoAddDto.rfRequestUserName;
    values.custRequestFormInfoAddDto.rfRequestUserOrgId = custApplyInfo.custRequestFormInfoAddDto.rfRequestUserOrgId;
    values.custRequestFormInfoAddDto.rfRequestUserOrgName = custApplyInfo.custRequestFormInfoAddDto.rfRequestUserOrgName;

    values.custRequestFormInfoAddDto.rfFromTid = custApplyInfo.custRequestFormInfoAddDto.rfFromTid;
    values.custRequestFormInfoAddDto.tid = custApplyInfo.custRequestFormInfoAddDto.tid;
    values.custRequestFormInfoAddDto.rfId = custApplyInfo.custRequestFormInfoAddDto.rfId;

    // 修改申请单
    dispatch({
      type: 'custapplymanage/updateCustApplyInfo',
      payload: {
        ...values,
      },
      callback: (_type, res) => {
        if (res.code === 0) {
          // 成功
          this.custInfoSubmitCb();
        } else {
          message.error(res.message);
        }
      },
    });
  };

  // 客户申请单信息更新成功后回调
  custInfoSubmitCb = () => {
    const { auditValues } = this.state;
    // 审核客户申请单
    this.auditSubmit(auditValues);
  };

  /**
   * 提交审核相关
   */
  // 客户申请单审核接口
  auditSubmit = values => {
    // console.log(values);
    const { selectedApply, currentUser, custApplyInfo, dispatch } = this.props;
    const params = {
      ...values,
      verifyId: uuid(),
      rfId: selectedApply.rfId,
      verifyUserOrgId: wrapTid().tid,
      verifyUserOrgName: currentUser.name,
      createUcode: currentUser.id,
      createUname: currentUser.name,
      createUid:currentUser.id,
      modifyUcode: currentUser.id,
      modifyUid: currentUser.id,
      modifyUname: currentUser.name,
      verifyUserName: currentUser.name,
      tid: custApplyInfo.custRequestFormInfoAddDto.tid
        ? custApplyInfo.custRequestFormInfoAddDto.tid
        : wrapTid().tid, // 租户id,
    };
    dispatch({
      type: 'custapplymanage/addCustApplyAudit',
      payload: params,
      callback: this.auditCallback,
    });
  };

  // 审核完成后回调
  auditCallback = result => {
    const { dispatch } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      this.modalVisibleOnChange(false);
      // this.handleToList();
      // // 查询客户申请审核列表(列表
      // 初始化重置rfId为空，避免当前审核数据的rfId和审核后列表第一条数据rfId相同
      dispatch({
        type: 'custapplymanage/changeAuditSelectedApply',
        payload: {},
      });
      dispatch({
        type: 'custapplymanage/paginationCustApplyAuditList',
        payload: {
          page: 1,
          pageSize: 20,
          searchCondition: {
            rfStatus: "'verify_waiting'",
          },
        },
        callback: res => {
          // 审核成功后，默认选中第一项
          dispatch({
            type: 'custapplymanage/changeAuditSelectedApply',
            payload: res.content[0],
          });
        },
      });
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
  };

  clickTianYC = () => {
    const { custApplyInfo, dispatch } = this.props;
    // const { myTabList } = this.state;
    if (
      !isEmptyObj(custApplyInfo.custRfCompanyInfoAddDto) &&
      custApplyInfo.custRfCompanyInfoAddDto.companyCode
    ) {
      let companyCode = '';
      if (
        custApplyInfo.custRfCompRelatedCompAddDto &&
        custApplyInfo.custRfCompRelatedCompAddDto.id
      ) {
        // 2、客户性质-个人：关联企业社会信用统一代码
        companyCode = custApplyInfo.custRfCompRelatedCompAddDto.custRelatedCompCode;
      } else {
        // 1、客户性质 - 企事业单位、工商个体→社会信用统一代码
        companyCode = custApplyInfo.custRfCompanyInfoAddDto.companyCode;
      }
      /**
       * 获取-根据客户信用码获取天眼查企业信息:
       * 1、客户性质 - 企事业单位、工商个体→社会信用统一代码
       * 2、客户性质-个人：关联企业社会信用统一代码
       */
      dispatch({
        type: 'custapplymanage/getTianYCInfoByCompanyCode',
        payload: {
          code: companyCode,
          // code: '91120116749118895P',
          sourceType: 1,
        },
        callback: res => {
          if (res && res.id) {
            const tabList = [
              {
                key: 'detailTab',
                tab: '客户信息',
              },
              {
                key: 'tianyanchaTab',
                tab: '天眼查',
              },
              {
                key: 'auditLogTab',
                tab: '历史审核记录',
              },
            ];
            this.setState({ myTabList: tabList });
          } else {
            // 天眼查没有结果
            message.error(formatMessage({ id: 'form.tianYCInfo.nodata' }));
          }
        },
      });
    }
  };

  render() {
    // modalUpkeepType 1详情; 2维护; 10新增,审核时候只能看详情
    const {
      custApplyInfo: {
        custRequestFormInfoAddDto: custFormInfo = {},
        custRfCompanyInfoAddDto: custCompanyInfo = {},
        custRfCompRelatedCompAddDto: custRelatedCompanyInfo = {},
      },
      parmpublicparametermanage: { parmPublicParameterCustRfStatus = [] },
      custApplyAuditList,
    } = this.props;
    const { activeKey, modalVisible, myTabList } = this.state;
    const modalContentCmp = (
      <AuditModal
        modalUpkeepEntity={custFormInfo}
        modalVisibleOnChange={this.modalVisibleOnChange}
        parmPublicParameterCustRfStatus={parmPublicParameterCustRfStatus}
        commit={this.onAuditSubmit}
      />
    );
    const extra = () => {
      let tianYCbtn = null;
      if (!isEmptyObj(custCompanyInfo) && custCompanyInfo.companyCode) {
        tianYCbtn = (
          <Button
            type="primary"
            onClick={() => {
              this.clickTianYC();
            }}
          >
            {formatMessage({ id: 'button.custApplyAudit.tianyc' })}
          </Button>
        );
      } else {
        tianYCbtn = (
          <Button type="primary" disabled>
            {formatMessage({ id: 'button.custApplyAudit.tianyc' })}
          </Button>
        );
      }
      return (
        <div>
          {tianYCbtn}
          <Button type="primary" style={{ marginLeft: '10px' }} onClick={() => this.openModel()}>
            {formatMessage({ id: 'form.goodsApplyBillAudit.audit' })}
          </Button>
        </div>
      ); // 审核按钮
    };

    const contentList = {
      detailTab: (
        <CustApplyDetail
          from="audit"
          modalUpkeepType={2}
          modalVisibleOnChange={this.handleToList}
          onSubmit={this.custInfoSubmit}
          showSaveBtn={false}
          onRef={ref => {
            this.detailRef = ref;
          }}
        />
      ),
      // eslint-disable-next-line no-nested-ternary
      tianyanchaTab: !isEmptyObj(custCompanyInfo) ? (
        custRelatedCompanyInfo && custRelatedCompanyInfo.id ? (
          <TianYanChaTab companyCode={custRelatedCompanyInfo.custRelatedCompCode} />
        ) : (
          <TianYanChaTab companyCode={custCompanyInfo.companyCode} />
        )
      ) : null,
      auditLogTab: <AuditLogTab />,
    };
    return (
      <Card
        title="客户申请单-审核"
        extra={extra()}
        tabList={myTabList}
        activeTabKey={activeKey}
        onTabChange={this.tabChange}
        bordered={false}
        bodyStyle={{ padding: '12px' }}
      >
        <Layout>
          <Layout style={{ paddingRight: '2px', overflow: 'hidden' }}>
            <Content style={{ background: '#fff', paddingRight: '10px' }}>
              {custFormInfo.id &&
                myTabList.map(item => (
                  <div
                    key={item.key}
                    style={{ display: activeKey === item.key ? 'block' : 'none' }}
                  >
                    {contentList[item.key]}
                  </div>
                ))}
            </Content>
          </Layout>
          <Sider
            style={{
              overflow: 'auto',
              background: '#fff',
              paddingLeft: '10px',
            }}
            width={360}
          >
            {custApplyAuditList.length > 0 ? <CustApplyAuditList /> : null}
          </Sider>
        </Layout>
        <ModalBasic
          modalTitle={formatMessage({ id: 'form.goodsApplyBillAudit.title' })}
          modalContentCmp={modalContentCmp}
          modalWidth={800}
          modalVisible={modalVisible}
          modalBodyStyle={{ minHeight: 500 }}
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ custapplymanage, user, parmpublicparametermanage }) => ({
  custApplyInfo: custapplymanage.custApplyInfo || {}, // 客户申请单详情信息
  selectedApply: custapplymanage.selectedApply || {}, // 审核选择的客户申请单
  custApplyAuditList: custapplymanage.custApplyAuditList,
  currentUser: user.currentUser || {},
  parmpublicparametermanage, // 公共参数
}))(CustApplyBatchAudit);
