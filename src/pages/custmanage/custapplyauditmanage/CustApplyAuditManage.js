/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Button, Divider, Form, Select, Input, Card, Row } from 'antd';
import moment from 'moment';
// eslint-disable-next-line import/no-extraneous-dependencies
import router from 'umi/router';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
// import { formatDate } from '@/utils/utils';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 企业信息管理组件
 */
class CustApplyAuditManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.goodsApplyBillManage.applyName' }),
        dataIndex: 'rfName',
        key: 'rfName',
        align: 'center',
        width: 120,
      },
      {
        title: formatMessage({ id: 'form.custApplyManage.custNum' }),
        dataIndex: 'rfRequestCreateCustNum',
        key: 'rfRequestCreateCustNum',
        align: 'center',
        width: 80,
      },
      {
        title: formatMessage({ id: 'form.custCompanyInfoManage.operUnitTypeCode' }),
        key: 'operUnitTypeCode',
        align: 'center',
        width: 120,
        dataIndex: 'operUnitTypeCode',
        // render: (text, record) => (
        //   <span>{this.custOperUnitTypeTxt(record, 'operUnitTypeCode')}</span>
        // ),
      },
      {
        title: formatMessage({ id: 'form.custApplyManage.accountType' }),
        key: 'custAccountTypeCode',
        align: 'center',
        dataIndex: 'custAccountTypeCode',
        width: 80,
      },
      {
        title: formatMessage({ id: 'form.common.apply.people' }),
        key: 'rfRequestUserName',
        align: 'center',
        dataIndex: 'rfRequestUserName',
        width: 150,
      },
      {
        title: formatMessage({ id: 'form.common.apply.organization' }),
        key: 'rfRequestUserOrgName',
        dataIndex: 'rfRequestUserOrgName',
        align: 'center',
        width: 80,
      },
      {
        title: formatMessage({ id: 'form.common.apply.status' }),
        key: 'rfStatus',
        align: 'center',
        // dataIndex: 'rfStatusShowName',
        width: 80,
        render: (text, record) => <span>{this.renderStatus(record, 'rfStatus')}</span>,
      },
      {
        title: formatMessage({ id: 'form.common.apply.time' }),
        key: 'createAt',
        align: 'center',
        dataIndex: 'createAt',
        width: 100,
        // render: (text, record) => (
        //   <span>{formatDate(new Date(record.createAt), 'yyyy-MM-dd hh:mm:ss')}</span>
        // ),
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        width: 100,
        render: (text, record) => (
          <span>
            <a
              type="primary"
              size="small"
              onClick={() => {
                this.hookProcess(1, record);
              }}
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            {/* verify_adopt审核通过和审核驳回状态不开放审核功能，即只有审核等待状态才能审核 */}
            {(record.rfStatus === 'verify_waiting') && (
              <span>
                <Divider type="vertical" />
                <a
                  type="primary"
                  size="small"
                  onClick={() => {
                    this.hookProcess(2, record);
                  }}
                >
                  {formatMessage({ id: 'form.goodsApplyBillAudit.audit' })}
                </a>
              </span>
            )}
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {};
  }

  /**
   * 响应点击处理函数
   */
  hookProcess = (type, record) => {
    // console.log('查询列表', type, record);
    // const { dispatch } = this.props;
    const val = { ...record };
    // const values = this.searchContent;
    if (type === 1) {
      // 详情
      router.push({
        pathname: '/custmanage/custapplyauditmanage/detail',
        query: {
          id: record.rfId,
          tid: record.tid ? record.tid : '10001',
          type: 'detail',
        },
      });
    }
    if (type === 2) {
      // 单页面审核
      router.push({
        pathname: '/custmanage/custapplyauditmanage/batchaudit',
        query: {
          id: record.rfId,
          tid:record.tid,
          type: 'sigleAudit',
        },
      });
    }
    return val;
  };

  /**
   * 加载默认企业信息数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-客户审核申请列表(列表)
    dispatch({
      type: 'custapplymanage/paginationCustApplyAuditList',
      payload: { page: 1, pageSize: 10, searchCondition: {} },
    });
    // 获取-经营单位类型定义数据(所有)
    dispatch({ type: 'custoperunittypemanage/getCustOperUnitTypeAll' });
    // 获取-客户申请单客户状态数据(所有)
    dispatch({
      type: 'parmpublicparametermanage/getCustRfStatus',
      payload: { parmTypeCode: 'cust_rf_status' },
    });
  }

  /** 组件即将卸载 */
  componentWillUnmount() {
    const { dispatch } = this.props;
    // 清空为默认
    dispatch({
      type: 'custapplymanage/changeCustApplyAuditListAll',
      payload: {}, // 清空客户申请单审核列表，初始化
    });
  }

  /**
   * 经营单位类型
   */
  custOperUnitTypeTxt = (record, field) => {
    const { custOperUnitTypeAll } = this.props;

    const findVal = custOperUnitTypeAll.filter(v => {
      return v.operUnitTypeCode === record[field];
    });
    return findVal.length > 0 ? findVal[0].operUnitTypeName : '';
  };

  /**
   * 申请单数据的状态
   */
  renderStatus = (record, field) => {
    const {
      parmpublicparametermanage: { parmPublicParameterCustRfStatus = [] },
    } = this.props;
    const findVal = parmPublicParameterCustRfStatus.find(v => v.parmCode === record[field]);
    return findVal ? findVal.parmValue : '';
  };

  /**
   * 根据条件搜索品牌
   */
  onFuzzyCustCompanyInfoHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    if (values.rfStatus) {
      values.rfStatus = `'${values.rfStatus}'`;
    }
    // const values = {};
    dispatch({
      type: 'custapplymanage/paginationCustApplyAuditList',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
  }

  render() {
    const { tableColumns, tableCurrentPage, formItemLayout, tailFormItemLayout } = this.state;
    const { custApplyAuditList, custApplyAuditListPageSize } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
        custOperUnitTypeAll: couta, // 经营单位类型定义数据(所有)
        parmpublicparametermanage: { parmPublicParameterCustRfStatus = [] },
        // eslint-disable-next-line react/no-this-in-sfc
      } = this.props;
      const custRfStatusList = parmPublicParameterCustRfStatus.filter(
        v => v.parmCode !== 'not_submitted'
      );
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form
            {...formItemLayout}
            className={styles.dropdownOverlayArea}
            labelAlign="right"
            labelCol={{ span: 10 }}
          >
            <Row style={{ width: '260px' }}>
              <Form.Item
                label={formatMessage({ id: 'form.goodsApplyBillManage.applyName' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('rfName')(<Input maxLength={100} style={{ width: 150 }} />)}
              </Form.Item>
              <Form.Item
                label={formatMessage({ id: 'form.custCompanyInfoManage.operUnitTypeCode' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('operUnitTypeCode')(
                  <Select style={{ width: 150 }} allowClear>
                    {couta.map(item => (
                      <Select.Option key={item.operUnitTypeCode} value={item.operUnitTypeCode}>
                        {item.operUnitTypeName}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
            </Row>
            <Row style={{ width: '260px' }}>
              <Form.Item
                label={formatMessage({ id: 'form.common.enable' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('rfStatus')(
                  <Select style={{ width: 150 }} allowClear>
                    {custRfStatusList.map(v => (
                      <Select.Option value={v.parmCode} key={v.id}>
                        {v.parmShowValue}
                      </Select.Option>
                    ))}
                  </Select>
                )}
              </Form.Item>
              <Form.Item
                label={formatMessage({ id: 'form.common.apply.organization' })}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                {getFieldDecorator('rfRequestUserOrgName')(<Input maxLength={100} style={{ width: 150 }} />)}
              </Form.Item>
            </Row>
            <Row>
              <Form.Item
                {...tailFormItemLayout}
                className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
              >
                <Button onClick={() => this.onFuzzyCustCompanyInfoHandle()}>
                  {formatMessage({ id: 'button.common.query' })}
                </Button>
              </Form.Item>
            </Row>
          </Form>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() => {
              // eslint-disable-next-line react/no-this-in-sfc
              router.push({
                pathname: '/custmanage/custapplyauditmanage/batchaudit',
              });
            }}
          >
            {formatMessage({ id: 'button.custApplyAudit.batchAudit' })}
          </Button>
        </div>
      );
    };
    const tablePaginationOnChangeEventDispatchType = 'custapplymanage/paginationCustApplyAuditList';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'menu.custmanage.custapplymanage' })}
          tableColumns={tableColumns}
          tableDataSource={custApplyAuditList}
          tableOperateAreaCmp={tableOperateAreaCmp()}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              { searchCondition: this.searchContent }
            )
          }
          tableTotalSize={custApplyAuditListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
        />
      </Card>
    );
  }
}

export default connect(
  ({ custapplymanage, custoperunittypemanage, parmpublicparametermanage }) => ({
    custApplyAuditList: custapplymanage.custApplyAuditList,
    custApplyAuditListPageSize: custapplymanage.custApplyAuditListPageSize,
    custOperUnitTypeAll: custoperunittypemanage.custOperUnitTypeAll || [], // 经营单位类型定义数据(所有)
    parmpublicparametermanage, // 公共参数
  })
)(Form.create()(CustApplyAuditManage));
