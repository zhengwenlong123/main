/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-param-reassign */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { connect } from 'dva';
import { Form, Card } from 'antd';
import moment from 'moment';
// eslint-disable-next-line import/no-extraneous-dependencies
// import router from 'umi/router';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
// import { formatDate } from '@/utils/utils';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 客户申请单审核管理组件
 * 客户申请单为审核列表
 */
class CustApplyAuditList extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.goodsApplyBillManage.applyName' }),
        dataIndex: 'rfName',
        key: 'rfName',
        align: 'center',
        width: 150,
      },
      {
        title: formatMessage({ id: 'form.common.apply.organization' }),
        key: 'rfRequestUserOrgName',
        dataIndex: 'rfRequestUserOrgName',
        align: 'center',
        width: 90,
      },
      {
        title: formatMessage({ id: 'form.common.apply.time' }),
        key: 'createAt',
        align: 'center',
        dataIndex: 'createAt',
        width: 100,
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
  }

  /**
   * 加载默认没有审核的客户申请单数据
   */
  componentDidMount() {
    // const {
    //   custApplyAuditList,
    //   dispatch,
    //   location: {
    //     query: { type = '' },
    //   },
    // } = this.props;
    // // 清空SelectedRfId
    // dispatch({
    //   type: 'custapplymanage/changeAuditSelectedRfId',
    //   payload: '',
    // });
    // // 获取-客户申请列表(列表)
    // dispatch({
    //   type: 'custapplymanage/paginationCustApplyAuditList',
    //   payload: { page: 1, pageSize: 10, searchCondition: this.searchContent },
    // });
    // if (custApplyAuditList.length > 0) {
    //   // 审核列表记录存在
    //   custApplyAuditList.forEach(v => {
    //     if (!v.id) {
    //       v.id = v.rfId;
    //     }
    //   });
    //   // 选中第一项
    //   // const selectedRowKeys = [custApplyAuditList[0].id];
    //   // this.setState({ selectedRowKeys });
    //   // 改变selectedRfId，再根据selectedRfId查询申请单详情
    //   if (type !== 'sigleAudit') {
    //     // 批量审核，默认选中第一项
    //     dispatch({
    //       type: 'custapplymanage/changeAuditSelectedRfId',
    //       payload: custApplyAuditList[0].rfId,
    //     });
    //   }
    // }
  }

  // 监听审核客户申请单数据，初始化改变了rfId参数值
  // componentWillReceiveProps(nextProps) {
  //   console.log('nextProps', nextProps);
  //   const {
  //     custApplyAuditList,
  //     location: {
  //       query: { type = '' },
  //     },
  //     dispatch,
  //   } = this.props;
  //   if (JSON.stringify(custApplyAuditList) !== JSON.stringify(nextProps.custApplyAuditList)) {
  //     if (nextProps.custApplyAuditList.length > 0) {
  //       // 初始化改变了rfId参数值为列表第一项
  //       // custApplyAuditList加上id，用于table组件的key
  //       nextProps.custApplyAuditList.forEach(v => {
  //         if (!v.id) {
  //           v.id = v.rfId;
  //         }
  //       });
  //       // 选中第一项
  //       // const selectedRowKeys = [nextProps.custApplyAuditList[0].id];
  //       // this.setState({ selectedRowKeys });
  //       // 根据查询申请单详情
  //       if (type !== 'sigleAudit') {
  //         // 批量审核，默认选中第一项
  //         dispatch({
  //           type: 'custapplymanage/changeAuditSelectedApply',
  //           payload: nextProps.custApplyAuditList[0],
  //         });
  //       }
  //     }
  //   }
  // }

  searchContent = {
    rfStatus: "'verify_waiting'",
  };

  onSelectChange = record => {
    const { dispatch } = this.props;
    // 设置选中申请单
    dispatch({
      type: 'custapplymanage/changeAuditSelectedApply',
      payload: record,
    });
  };

  render() {
    const { tableColumns, tableCurrentPage } = this.state;
    const { custApplyAuditList, custApplyAuditListPageSize, selectedApply } = this.props;
    const tablePaginationOnChangeEventDispatchType = 'custapplymanage/paginationCustApplyAuditList';
    // 选择某行事件
    return (
      <Card
        title={formatMessage({ id: 'menu.custmanage.custapplymanage' })}
        bodyStyle={{ padding: '10px' }}
      >
        <div className={styles.custApplyAuditTable}>
          <MdcManageBlock
            tablePageSize={20}
            tableColumns={tableColumns}
            tableDataSource={custApplyAuditList}
            tableCurrentPage={tableCurrentPage}
            tablePaginationOnChangeEvent={page =>
              this.tablePaginationOnChangeEvent(
                page,
                20,
                tablePaginationOnChangeEventDispatchType,
                { searchCondition: this.searchContent }
              )
            }
            rowClassName={record => {
              if (record.rfId === selectedApply.rfId) {
                return styles.rowActive;
              }
              return '';
            }}
            onRow={record => {
              return {
                onClick: () => {
                  this.onSelectChange(record);
                },
              };
            }}
            tableTotalSize={custApplyAuditListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          />
        </div>
      </Card>
    );
  }
}

export default connect(({ custapplymanage }) => ({
  custApplyAuditList: custapplymanage.custApplyAuditList,
  custApplyAuditListPageSize: custapplymanage.custApplyAuditListPageSize,
  selectedApply: custapplymanage.selectedApply,
}))(Form.create()(CustApplyAuditList));
