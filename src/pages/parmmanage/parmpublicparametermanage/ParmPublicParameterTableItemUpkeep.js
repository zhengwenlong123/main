import React from 'react';
import { connect } from 'dva';
import { Button, Form, Input, InputNumber, message } from 'antd';
import { formatMessage } from 'umi/locale';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

/**
 * 公用参数定义列表项-维护组件
 */
@ValidationFormHoc
class ParmPublicParameterTableItemUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    tailFormItemLayout: {
      wrapperCol: { xs: { span: 24, offset: 0 }, sm: { span: 24, offset: 0 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();

    // 维护
    if (mut === 2) {
      values.id = mue.id;
      dispatch({
        type: 'parmpublicparametermanage/updateParmPublicParameter',
        payload: values,
        callback: this.onCallback,
      });
    }
    // 新增
    if (mut === 10) {
      dispatch({
        type: 'parmpublicparametermanage/addParmPublicParameter',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, tailFormItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      modalUpkeepType: mut,
      form: { getFieldDecorator },
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
        <Form {...formItemLayout}>
          <Form.Item
            label={formatMessage({ id: 'form.parmPublicParameterTableItemUpkeep.parmTypeCode' })}
          >
            {getFieldDecorator('parmTypeCode', { rules: [{ required: true }] })(<Input disabled />)}
          </Form.Item>
          <Form.Item
            label={formatMessage({ id: 'form.parmPublicParameterTableItemUpkeep.parmParentCode' })}
          >
            {getFieldDecorator('parmParentCode')(<Input disabled={mut === 1} />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.common.code' })}>
            {getFieldDecorator('parmCode', {
              rules: [
                { required: true, message: formatMessage({ id: 'form.common.code.placeholder' }) },
              ],
            })(<Input disabled={mut === 1} />)}
          </Form.Item>
          <Form.Item
            label={formatMessage({ id: 'form.parmPublicParameterTableItemUpkeep.parmValue' })}
          >
            {getFieldDecorator('parmValue', {
              rules: [
                {
                  required: true,
                  message: formatMessage({
                    id: 'form.parmPublicParameterTableItemUpkeep.parmValue.placeholder',
                  }),
                },
              ],
            })(<Input disabled={mut === 1} />)}
          </Form.Item>
          <Form.Item
            label={formatMessage({ id: 'form.parmPublicParameterTableItemUpkeep.parmShowValue' })}
          >
            {getFieldDecorator('parmShowValue', {
              rules: [
                {
                  required: true,
                  message: formatMessage({
                    id: 'form.parmPublicParameterTableItemUpkeep.parmShowValue.placeholder',
                  }),
                },
              ],
            })(<Input disabled={mut === 1} />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.parmPublicParameterTableItemUpkeep.seq' })}>
            {getFieldDecorator('seq', {
              rules: [
                {
                  required: true,
                  message: formatMessage({
                    id: 'form.parmPublicParameterTableItemUpkeep.seq.placeholder',
                  }),
                },
              ],
            })(<InputNumber min={1} max={10000} defaultChecked={1} disabled={mut === 1} />)}
          </Form.Item>
          <Form.Item
            label={formatMessage({ id: 'form.parmPublicParameterTableItemUpkeep.parmDesc' })}
          >
            {getFieldDecorator('parmDesc')(<Input.TextArea disabled={mut === 1} rows={3} />)}
          </Form.Item>
          {mut === 1 ? null : (
            <Form.Item {...tailFormItemLayout}>
              <div className={styles.modalHandleBtnArea}>
                <Button
                  type="default"
                  htmlType="button"
                  onClick={() => modalVisibleOnChange(false)}
                >
                  {formatMessage({ id: 'button.common.cancel' })}
                </Button>
                <Button
                  type="primary"
                  htmlType="button"
                  onClick={this.onClickSubmit}
                  loading={confirmLoading}
                >
                  {formatMessage({ id: 'button.common.submit' })}
                </Button>
              </div>
            </Form.Item>
          )}
        </Form>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue, parmTypeCode } = props;

      return {
        parmTypeCode: Form.createFormField({ value: mue ? mue.parmTypeCode : parmTypeCode || '' }),
        parmCode: Form.createFormField({ value: mue ? mue.parmCode : '' }),
        parmParentCode: Form.createFormField({ value: mue ? mue.parmParentCode : '' }),
        parmValue: Form.createFormField({ value: mue ? mue.parmValue : '' }),
        parmShowValue: Form.createFormField({ value: mue ? mue.parmShowValue : '' }),
        seq: Form.createFormField({ value: mue ? mue.seq : 1 }),
        parmDesc: Form.createFormField({ value: mue ? mue.parmDesc : '' }),
      };
    },
  })(ParmPublicParameterTableItemUpkeep)
);
