import React from 'react';
import { connect } from 'dva';
import { Tabs } from 'antd';
import { formatMessage } from 'umi/locale';
import ParmPublicParameterTypeUpkeep from './ParmPublicParameterTypeUpkeep';
import ParmPublicParameterTableUpkeep from './ParmPublicParameterTableUpkeep';

/**
 * 公用参数管理维护Modal对话框内容部分组件
 */
class ParmPublicParameterUpkeep extends React.PureComponent {
  render() {
    const { modalUpkeepType: mut, modalUpkeepEntity: mue } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        {mut === 10 ? (
          <ParmPublicParameterTypeUpkeep {...this.props} />
        ) : (
          <Tabs tabPosition="left">
            <Tabs.TabPane
              key="0"
              tab={formatMessage({ id: 'form.parmPublicParameterManage.parmType' })}
            >
              <ParmPublicParameterTypeUpkeep {...this.props} />
            </Tabs.TabPane>
            <Tabs.TabPane
              key="1"
              tab={formatMessage({ id: 'form.parmPublicParameterManage.parm' })}
            >
              <ParmPublicParameterTableUpkeep parmTypeCode={mue.parmTypeCode} actionType={mut} />
            </Tabs.TabPane>
          </Tabs>
        )}
      </div>
    );
  }
}

export default connect()(ParmPublicParameterUpkeep);
