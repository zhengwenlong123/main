/* eslint-disable no-unused-expressions */
import React from 'react';
import { connect } from 'dva';
// import moment from 'moment';
import { Button, Form, Input, message } from 'antd';
import { formatMessage } from 'umi/locale';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

/**
 * 公用参数类型定义-维护组件
 */
@ValidationFormHoc
class ParmPublicParameterTypeUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      form: { getFieldsValue },
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
      modalSearchCondition,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();

    // 新增
    if (mut === 10) {
      dispatch({
        type: 'parmpublicparametermanage/addParmPublicParameterType',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    // 维护
    if (mut === 2) {
      values.id = mue.id;
      dispatch({
        type: 'parmpublicparametermanage/updateParmPublicParameterType',
        payload: values,
        callback: this.onCallback,
        searchCondition: modalSearchCondition,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
    } = this.props;

    return (
      <div>
        <div className={styles.modalHandleFormScopeArea}>
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.common.code' })}>
              {getFieldDecorator('parmTypeCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.code.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('parmTypeName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.parmPublicParameterTypeUpkeep.parmTypeDesc' })}
            >
              {getFieldDecorator('parmTypeDesc')(<Input.TextArea disabled={mut === 1} rows={3} />)}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        parmTypeCode: Form.createFormField({ value: mue ? mue.parmTypeCode : '' }),
        parmTypeName: Form.createFormField({ value: mue ? mue.parmTypeName : '' }),
        parmTypeDesc: Form.createFormField({ value: mue ? mue.parmTypeDesc : '' }),
      };
    },
  })(ParmPublicParameterTypeUpkeep)
);
