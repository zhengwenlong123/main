/* eslint-disable no-nested-ternary */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Divider, message } from 'antd';
import { formatMessage } from 'umi/locale';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import ParmPublicParameterTableItemUpkeep from './ParmPublicParameterTableItemUpkeep';

/**
 * 公用参数定义列表维护
 */
class ParmPublicParameterTableUpkeep extends MdcManageBasic {
  constructor(props) {
    super(props);
    // state
    this.state = {
      ...this.state,
      // Table行勾选数据对象
      selectedRowKeys: [],
      tableRowSelectionEntity: null,
    };
    // Table-表格行勾选处理
    // this.tableRowSelection = {
    //   type: 'radio',
    //   columnWidth: 30,
    //   columnTitle: '<>',
    //   selectedRowKeys: [],
    //   onChange: (selectedRowKeys, selectedRows) => {
    //     this.tableRowSelection.selectedRowKeys = selectedRowKeys;
    //     this.setState({ tableRowSelectionEntity: selectedRows[0] });
    //   }, // Table勾选回调事件
    // };
    // 表格列表字段定义
    this.tableColumns = [
      {
        title: formatMessage({ id: 'form.parmPublicParameterManage.parmTypeCode' }),
        dataIndex: 'parmTypeCode',
        key: 'parmTypeCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'parmCode',
        key: 'parmCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.parmPublicParameterManage.parmValue' }),
        dataIndex: 'parmValue',
        key: 'parmValue',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.parmPublicParameterManage.parmShowValue' }),
        dataIndex: 'parmShowValue',
        key: 'parmShowValue',
        align: 'center',
      },
    ];
  }

  /**
   * 渲染组件-公共参数定义
   */
  render() {
    const {
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableCurrentPage,
      selectedRowKeys,
    } = this.state;
    const {
      parmTypeCode, // 公用参数类型定义代码
      currentSelectParmPublicParameter: cspp, // 当前选择[公用参数类型定义]下[公用参数定义]列表
      currentSelectParmPublicParameterPageSize: cspppps, // 当前选择[公用参数类型定义]下[公用参数定义]列表-总数量
    } = this.props;

    const tablePaginationOnChangeEventDispatchType =
      'parmpublicparametermanage/paginationParmPublicParameter'; // 分页dispatch的type
    const tablePaginationOnChangeEventCondition = { parmTypeCode }; // 分页派发附加条件

    return (
      <MdcManageBlock
        tableColumns={this.tableColumns}
        tableDataSource={cspp}
        tableOperateAreaCmp={this._tableOperateAreaCmp()}
        tableRowSelection={{
          columnWidth: 30,
          columnTitle: '<>',
          selectedRowKeys,
          onSelect: (record, selected, selectedRows) => {
            if (selected) {
              this.setState({ tableRowSelectionEntity: record });
            } else if (selectedRows.length === 0) {
              this.setState({ tableRowSelectionEntity: null });
            }
          },
          onChange: selectedRowKeysArr => {
            this.setState({
              selectedRowKeys:
                selectedRowKeysArr.length > 1 ? [selectedRowKeysArr[1]] : selectedRowKeysArr,
            });
          }, // Table勾选回调事件
        }}
        tableTitle={formatMessage({ id: 'form.parmPublicParameterTableUpkeep.tableTitle' })}
        tableCurrentPage={tableCurrentPage}
        tablePaginationOnChangeEvent={(page, pageSize) =>
          this.tablePaginationOnChangeEvent(
            page,
            pageSize,
            tablePaginationOnChangeEventDispatchType,
            tablePaginationOnChangeEventCondition // 分页附加条件
          )
        }
        tableTotalSize={cspppps} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
        modalVisible={modalVisible}
        modalTitle={formatMessage({ id: 'form.parmPublicParameterTableUpkeep.modalTitle' })}
        modalContentCmp={
          <ParmPublicParameterTableItemUpkeep
            parmTypeCode={parmTypeCode} // 在新增时使用(公用参数类型定义代码)
            modalUpkeepType={modalUpkeepType}
            modalUpkeepEntity={modalUpkeepEntity}
            modalVisibleOnChange={this.modalVisibleOnChange}
            modalOnChangePageCurrent={this.changeCurrentPage}
          />
        }
        modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
      />
    );
  }

  // Table-表格头部动作处理组件
  _tableOperateAreaCmp = () => {
    const { tableRowSelectionEntity: trse } = this.state;
    const { actionType } = this.props; // 公用参数类型定义的动作类型值,1为详情;2为维护;10为新增

    // 校验Table勾选tableRowSelectionEntity值是否存在
    const checkTableRowSelectionEntity = (flag = -1) => {
      if (trse == null || trse === undefined) {
        const tips =
          flag === 2
            ? formatMessage({ id: 'button.common.modify' })
            : flag === 20
            ? formatMessage({ id: 'button.common.delete' })
            : formatMessage({ id: 'button.common.handle' });
        message.warn(
          `${formatMessage({
            id: 'form.parmPublicParameterTableUpkeep.tableRowSelectionEntity',
          })}[${tips}]`
        );
        return;
      }
      if (flag === 1) {
        // 详情动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 2) {
        // 维护动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 20) {
        // 删除动作
        this._deleteTableRowSelection(trse);
      }
    };

    return actionType === 1 ? (
      <a type="primary" onClick={() => checkTableRowSelectionEntity(1)}>
        {formatMessage({ id: 'button.common.details' })}
      </a>
    ) : (
      <div>
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(1);
            // this.tableRowSelection.selectedRowKeys = [];
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.details' })}
        </a>
        <Divider type="vertical" />
        <a type="primary" onClick={() => this.onClickActionExecuteEvent(10)}>
          {formatMessage({ id: 'button.common.add' })}
        </a>
        <Divider type="vertical" />
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(2);
            // this.tableRowSelection.selectedRowKeys = [];
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.modify' })}
        </a>
        <Divider type="vertical" />
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(20);
            // this.tableRowSelection.selectedRowKeys = [];
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.delete' })}
        </a>
      </div>
    );
  };

  /**
   * Table-删除行勾选数据
   * @param {object} item 处理数据对象
   * 删除1条公用参数定义数据
   */
  _deleteTableRowSelection = item => {
    const { dispatch } = this.props;
    // 删除-公用参数定义数据
    dispatch({
      type: 'parmpublicparametermanage/deleteParmPublicParameter',
      payload: item,
      callback: this.onCallbackTips,
    });
  };
}

ParmPublicParameterTableUpkeep.propTypes = {
  parmTypeCode: PropTypes.string.isRequired, // 公用参数类型定义代码,为新增公用参数定义时使用
  actionType: PropTypes.number.isRequired, // 公用参数类型定义动作类型(同modalUpkeepType绑定一致)
};

export default connect(({ parmpublicparametermanage }) => ({
  currentSelectParmPublicParameter: parmpublicparametermanage.currentSelectParmPublicParameter, // 公用参数定义列表
  currentSelectParmPublicParameterPageSize:
    parmpublicparametermanage.currentSelectParmPublicParameterPageSize, // 公用参数定义列表-总数量
}))(ParmPublicParameterTableUpkeep);
