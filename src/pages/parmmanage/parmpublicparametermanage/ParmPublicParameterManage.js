import React from 'react';
import { connect } from 'dva';
import { Button, Card, Divider, Form, Input } from 'antd';
import { formatMessage } from 'umi/locale';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import ParmPublicParameterUpkeep from './ParmPublicParameterUpkeep';
import styles from './index.less';

/**
 * 公用参数管理组件(含:公用参数类型定义,公用参数定义)
 */
class ParmPublicParameterManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'parmTypeCode',
        key: 'parmTypeCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'parmTypeName',
        key: 'parmTypeName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.parmPublicParameterManage.parmTypeDesc' }),
        dataIndex: 'parmTypeDesc',
        key: 'parmTypeDesc',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(1, this.hookProcess(1, record), '公用参数详情')
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() => {
                this.onClickActionExecuteEvent(
                  2,
                  this.hookProcess(2, record),
                  formatMessage({ id: 'button.parmPublicParameterManage.modify' }),
                  null,
                  this.searchContent
                );
              }}
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            {/* <Divider type="vertical" /> */}
          </span>
        ),
      },
    ];
    super(props, tableColumns);
    // eslint-disable-next-line
    this.searchContent = {};
  }

  /**
   * 按公用参数类型定义条件获取公用参数定义数据
   * @param type 公用参数类型定义动作代码
   * @param record 公用参数类型定义对象
   */
  hookProcess = (type, record) => {
    const { dispatch } = this.props;
    // 获取-当前选择-公用参数定义数据(列表-分页方式)
    dispatch({
      type: 'parmpublicparametermanage/paginationParmPublicParameter',
      payload: { ...record, page: 1, pageSize: 10 },
    });
    return record;
  };

  /**
   * 默认加载公用参数数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({ type: 'parmpublicparametermanage/getParmPublicParameterType' });
  }

  /**
   * 根据条件搜索品牌
   */
  onFuzzyParmPublicParameterHandle() {
    const {
      form: { getFieldsValue },
      dispatch,
    } = this.props;
    this.searchContent = getFieldsValue();
    const values = this.searchContent;
    dispatch({
      type: 'parmpublicparametermanage/paginationParmPublicParameterType',
      payload: { page: 1, pageSize: 10, searchCondition: values },
    });
    // 返回第一页
    this.changeCurrentPage(1);
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
    } = this.state;
    const {
      parmPublicParameterTypeList,
      parmPublicParameterTypeListPageSize,
      formItemLayout,
      tailFormItemLayout,
    } = this.props;

    const tableOperateAreaCmp = () => {
      const {
        form: { getFieldDecorator },
      } = this.props;
      return (
        <div className={styles.dropdownOverlayArea}>
          <Form {...formItemLayout} className={styles.dropdownOverlayArea}>
            <Form.Item
              label={formatMessage({ id: 'form.common.code' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('parmTypeCode')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              label={formatMessage({ id: 'form.common.name' })}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              {getFieldDecorator('parmTypeName')(<Input style={{ width: 120 }} />)}
            </Form.Item>
            <Form.Item
              {...tailFormItemLayout}
              className={`${styles.dropdownOverlayArea} ${styles.searchStyle}`}
            >
              <Button onClick={() => this.onFuzzyParmPublicParameterHandle()}>
                {formatMessage({ id: 'button.common.query' })}
              </Button>
            </Form.Item>
          </Form>
          <Button
            type="primary"
            style={{ marginTop: 4 }}
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.parmPublicParameterManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      );
    };

    const tablePaginationOnChangeEventDispatchType =
      'parmpublicparametermanage/paginationParmPublicParameterType';

    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'form.parmPublicParameterManage.tableTitle' })}
          tableColumns={tableColumns}
          tableDataSource={parmPublicParameterTypeList || []}
          tableOperateAreaCmp={tableOperateAreaCmp()}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType,
              { searchCondition: this.searchContent }
            )
          }
          tableTotalSize={parmPublicParameterTypeListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <ParmPublicParameterUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
              modalSearchCondition={this.searchContent}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ parmpublicparametermanage }) => ({
  parmPublicParameterTypeList: parmpublicparametermanage.parmPublicParameterTypeList, // 公用参数类型定义列表
  parmPublicParameterTypeListPageSize:
    parmpublicparametermanage.parmPublicParameterTypeListPageSize, // 公用参数类型定义总数量
}))(Form.create()(ParmPublicParameterManage));
