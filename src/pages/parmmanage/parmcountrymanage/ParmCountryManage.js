import React from 'react';
import { connect } from 'dva';
import { Button, Card, Divider, Select } from 'antd';
import { formatMessage } from 'umi/locale';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import ParmCountryUpkeep from './ParmCountryUpkeep';
import { worldContinentDefault } from '@/utils/mdcutil';
import styles from './index.less';

/**
 * 国别定义管理组件
 */
class ParmCountryManage extends MdcManageBasic {
  constructor(props) {
    super(props);

    // 国别定义列表字段定义
    this.tableColumns_ParmCountry = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'countryCode',
        key: 'countryCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'countryName',
        key: 'countryName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.ParmCountryManage.countryNameEn' }),
        dataIndex: 'countryNameEn',
        key: 'countryNameEn',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.ParmCountryManage.continentName' }),
        dataIndex: 'continentName',
        key: 'continentName',
        align: 'center',
      },
      // { title: '描述', dataIndex: 'countryDesc', key: 'countryDesc', align: 'center' },
      {
        title: formatMessage({ id: 'form.ParmCountryManage.currency' }),
        dataIndex: 'currency',
        key: 'currency',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.ParmCountryManage.currencySymbol' }),
        dataIndex: 'currencySymbol',
        key: 'currencySymbol',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() => this.onClickActionExecuteEvent(1, record, '国别定义详情')}
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() => this.onClickActionExecuteEvent(2, record, '国别定义维护')}
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            {/* <Divider type="vertical" /> */}
            {/* <a type="primary" onClick={() => this.onClickActionExecuteEvent(3, record, null, 'parmcountrymanage/updateParmCountryEnable')}>停用</a> */}
          </span>
        ),
      },
    ];
  }

  /**
   * 默认加载国别定义数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-默认所属洲下国别定义数据(列表)
    dispatch({ type: 'parmcountrymanage/getParmCountryByContinentName' });
    // 获取-币别定义数据(所有)
    dispatch({ type: 'parmcurrencymanage/getParmCurrencyAll' });
  }

  /**
   * 在此生命周期方法内做清理数据操作
   */
  componentWillUnmount() {
    const { dispatch } = this.props;
    // 在该组件移除挂载时需要清空-国别定义列表数据(因为需要用户再次选择所属洲时再查询设置parmCountryList数据)
    dispatch({
      type: 'parmcountrymanage/changeParmCountryList',
      payload: { content: [], total: 0 },
    });
  }

  /**
   * 当[所属洲]选择改变时回调函数
   */
  onSelectParmCountryCallback = value => {
    const { dispatch } = this.props;
    // 更新-当前选择-国别定义-所属洲名称
    dispatch({
      type: 'parmcountrymanage/changecurrentSelectParmCountryOfContinent',
      payload: value,
    });
    // 获取-根据所属洲名称-获取该国别定义数据(列表)
    dispatch({ type: 'parmcountrymanage/getParmCountry', payload: { continentName: value } });
    // 重置当前页面页码数
    this.changeCurrentPage(1);
  };

  /**
   * 渲染列表组件头部操作区划的组件
   */
  renderTableOperateAreaCmp = () => {
    const { currentSelectParmCountryOfContinent: cspcoc } = this.props; // 当前选择-国别定义-所属洲(默认为亚洲)
    const defaultContinent = worldContinentDefault();

    return (
      <div className={styles.parmContryTableOperateAreaCmp}>
        <div>
          <span>{formatMessage({ id: 'form.ParmCountryManage.continentName' })}:&nbsp;&nbsp;</span>
          <Select
            style={{ width: 150 }}
            value={cspcoc} // 当前选择-国别定义-所属洲名称
            placeholder={formatMessage({ id: 'form.ParmCountryManage.continentName.placeholder' })}
            onSelect={this.onSelectParmCountryCallback}
          >
            {defaultContinent.map(item => (
              <Select.Option key={item.code} value={item.name} extra={item}>
                {item.name}
              </Select.Option>
            ))}
          </Select>
        </div>
        <Button
          type="primary"
          onClick={() => this.onClickActionExecuteEvent(10, null, '国别定义新增')}
        >
          {formatMessage({ id: 'button.common.add' })}
        </Button>
      </div>
    );
  };

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableCurrentPage,
    } = this.state;
    const {
      parmCountryList,
      parmCountryListPageSize,
      currentSelectParmCountryOfContinent: cspcoc, // 当前选择-国别定义-所属洲(默认为亚洲)
    } = this.props;

    const tableOperateAreaCmp = this.renderTableOperateAreaCmp();
    const tablePaginationOnChangeEventDispatchType = 'parmcountrymanage/paginationParmCountry';
    const tablePaginationOnChangeEventCondition = { continentName: cspcoc }; // 分页派发附加条件

    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'form.ParmCountryManage.tableTitle' })}
          tableColumns={this.tableColumns_ParmCountry}
          tableDataSource={parmCountryList === undefined ? [] : parmCountryList}
          tableOperateAreaCmp={tableOperateAreaCmp}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType, // 分页dispatch的type
              tablePaginationOnChangeEventCondition // 分页附加条件
            )
          }
          tableTotalSize={parmCountryListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <ParmCountryUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ parmcountrymanage }) => ({
  parmCountryList: parmcountrymanage.parmCountryList,
  parmCountryListPageSize: parmcountrymanage.parmCountryListPageSize,
  currentSelectParmCountryOfContinent: parmcountrymanage.currentSelectParmCountryOfContinent, // 当前选择-国别定义-所属洲(默认为亚洲)
}))(ParmCountryManage);
