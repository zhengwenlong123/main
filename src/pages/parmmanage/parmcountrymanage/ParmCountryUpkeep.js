/* eslint-disable no-unused-expressions */
import React from 'react';
import { connect } from 'dva';
import { formatMessage } from 'umi/locale';

import { Button, Form, Input, message, Select } from 'antd';
import { worldContinentDefault } from '@/utils/mdcutil';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

/**
 * 国别定义维护组件
 */
@ValidationFormHoc
class ParmCountryUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      currentSelectParmCountryOfContinent: cspcoc, // 当前选择-国别定义-所属洲(默认为亚洲)
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.currentSelectContinent = cspcoc; // 设置-当前选择-所属洲名称

    if (mut === 2) {
      // 维护
      values.id = mue.id;
      dispatch({
        type: 'parmcountrymanage/updateParmCountry',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (mut === 10) {
      // 新增
      dispatch({
        type: 'parmcountrymanage/addParmCountry',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
      parmCurrencyAll: pca, // 币别数据(所有)
    } = this.props;

    const defaultContinent = worldContinentDefault();

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.common.code' })}>
              {getFieldDecorator('countryCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.code.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('countryName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.ParmCountryManage.countryNameEn' })}>
              {getFieldDecorator('countryNameEn', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.ParmCountryManage.continentName' })}>
              {getFieldDecorator('continentName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.ParmCountryManage.continentName.placeholder',
                    }),
                  },
                ],
              })(
                <Select disabled={mut === 1}>
                  {defaultContinent.map(item => (
                    <Select.Option key={item.code} value={item.name}>
                      {item.name}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.ParmCountryManage.countryDesc' })}>
              {getFieldDecorator('countryDesc', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.ParmCountryManage.countryDesc.placeholder',
                    }),
                  },
                ],
              })(<Input.TextArea disabled={mut === 1} rows={3} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.ParmCountryManage.currency' })}>
              {getFieldDecorator('currency')(
                <Select disabled={mut === 1}>
                  {pca.map(item => (
                    <Select.Option key={item.currencyCode} value={item.currencyCode}>
                      {item.currencyName}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.ParmCountryManage.currencySymbol' })}>
              {getFieldDecorator('currencySymbol')(<Input disabled={mut === 1} />)}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" htmlType="button" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button
                type="primary"
                htmlType="button"
                onClick={this.onClickSubmit}
                loading={confirmLoading}
              >
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ parmcountrymanage, parmcurrencymanage }) => ({
  parmCurrencyAll: parmcurrencymanage.parmCurrencyAll || [], // 币别数据(所有)
  currentSelectParmCountryOfContinent: parmcountrymanage.currentSelectParmCountryOfContinent, // 当前选择-国别定义-所属洲(默认为亚洲)
}))(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        countryCode: Form.createFormField({ value: mue ? mue.countryCode : '' }),
        countryName: Form.createFormField({ value: mue ? mue.countryName : '' }),
        countryNameEn: Form.createFormField({ value: mue ? mue.countryNameEn : '' }),
        continentName: Form.createFormField({ value: mue ? mue.continentName : '' }),
        countryDesc: Form.createFormField({ value: mue ? mue.countryDesc : '' }),
        currency: Form.createFormField({ value: mue ? mue.currency : '' }), // 对应币别代码
        currencySymbol: Form.createFormField({ value: mue ? mue.currencySymbol : '' }),
      };
    },
  })(ParmCountryUpkeep)
);
