/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-expressions */
import React from 'react';
import { connect } from 'dva';
import { Button, Checkbox, Form, Input, InputNumber, message } from 'antd';
import { formatMessage } from 'umi/locale';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

/**
 * 行政区划定义维护组件
 */
@ValidationFormHoc
class ParmRegionUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      currentSelectParmRegionCascader: csprc, // 当前选择-级联行政区划定义对象
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
    } = this.props;
    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();
    values.showEnable ? (values.showEnable = 1) : (values.showEnable = 0);

    if (mut === 2) {
      // 维护
      values.id = mue.id;
      dispatch({
        type: 'parmregionmanage/updateParmRegion',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (mut === 10) {
      // 新增
      values.parentRegionCode = csprc.regionCode;
      values.countryCode = csprc.countryCode;
      values.regionLevel = Number(csprc.regionLevel) + 1;
      dispatch({
        type: 'parmregionmanage/addParmRegion',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      modalUpkeepType: mut,
      form: { getFieldDecorator },
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div>
        <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
          <Form {...formItemLayout}>
            {mut === 10 ? null : (
              <Form.Item label={formatMessage({ id: 'form.ParmRegionManage.parentRegionCode' })}>
                {getFieldDecorator('parentRegionCode')(<Input disabled />)}
              </Form.Item>
            )}
            {mut === 10 ? null : (
              <Form.Item label={formatMessage({ id: 'form.ParmRegionManage.countryCode' })}>
                {getFieldDecorator('countryCode', { rules: [{ required: true }] })(
                  <Input disabled />
                )}
              </Form.Item>
            )}
            <Form.Item label={formatMessage({ id: 'form.common.code' })}>
              {getFieldDecorator('regionCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.code.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('regionName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.ParmRegionManage.regionShortName' })}>
              {getFieldDecorator('regionShortName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({
                      id: 'form.ParmRegionManage.regionShortName.placeholder',
                    }),
                  },
                ],
              })(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.ParmRegionManage.regionFullName' })}>
              {getFieldDecorator('regionFullName')(<Input disabled={mut === 1} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.ParmRegionManage.regionLevel' })}>
              {getFieldDecorator('regionLevel', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.ParmRegionManage.regionLevel.placeholder' }),
                  },
                ],
              })(<InputNumber min={1} max={10000} defaultChecked={1} disabled />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.ParmRegionManage.showEnable' })}>
              {getFieldDecorator('showEnable', { valuePropName: 'checked' })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'button.ParmRegionManage.show' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" htmlType="button" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button
                type="primary"
                htmlType="button"
                onClick={this.onClickSubmit}
                loading={confirmLoading}
              >
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default connect(({ parmregionmanage }) => ({
  currentSelectParmRegionCascader: parmregionmanage.currentSelectParmRegionCascader || {}, // 当前选择-级联行政区划定义(对象)
}))(
  Form.create({
    mapPropsToFields(props) {
      const {
        modalUpkeepEntity: mue,
        modalUpkeepType: mut,
        currentSelectParmRegionCascader: csprc, // 当前选择-级联行政区划定义(对象)
      } = props;

      return {
        parentRegionCode: Form.createFormField({ value: mue ? mue.parentRegionCode : '' }),
        countryCode: Form.createFormField({ value: mue ? mue.countryCode : '' }),
        regionCode: Form.createFormField({ value: mue ? mue.regionCode : '' }),
        regionName: Form.createFormField({ value: mue ? mue.regionName : '' }),
        regionShortName: Form.createFormField({ value: mue ? mue.regionShortName : '' }),
        regionFullName: Form.createFormField({ value: mue ? mue.regionFullName : '' }),
        regionLevel: Form.createFormField({
          value:
            mut === 10
              ? Number(csprc.regionLevel) + 1
              : mue
              ? mue.regionLevel
              : Number(csprc.regionLevel) - 1,
        }),
        showEnable: Form.createFormField({ value: mue ? mue.showEnable === 1 : true }),
      };
    },
  })(ParmRegionUpkeep)
);
