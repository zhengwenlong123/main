/* eslint-disable no-param-reassign */
import React from 'react';
import { connect } from 'dva';
import { Button, Card, Cascader, Divider, Select } from 'antd';
import { formatMessage } from 'umi/locale';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import ParmRegionUpkeep from './ParmRegionUpkeep';
import { formatParmRegionToAntdCascaderCmp } from '@/utils/mdcutil';
import styles from './index.less';

/**
 * 行政区划定义管理组件
 */
class ParmRegionManage extends MdcManageBasic {
  constructor(props) {
    super(props);

    // 行政区划定义列名
    this.tableColumns_ParmRegion = [
      // { title: '国别代码', dataIndex: 'countryCode', key: 'countryCode', align: 'center' },
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'regionCode',
        key: 'regionCode',
        align: 'center',
      },
      // { title: '级别', dataIndex: 'regionLevel', key: 'regionLevel', align: 'center' },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'regionName',
        key: 'regionName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.ParmRegionManage.regionFullName' }),
        dataIndex: 'regionFullName',
        key: 'regionFullName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.ParmRegionManage.showEnable' }),
        key: 'showEnable',
        align: 'center',
        render: (text, record) => (
          <span>
            {record.showEnable === 1
              ? formatMessage({ id: 'button.ParmRegionManage.show.true' })
              : formatMessage({ id: 'button.ParmRegionManage.show.false' })}
          </span>
        ),
      },
      // { title: '父类参数代码', dataIndex: 'parentRegionCode', key: 'parentRegionCode', align: 'center'},
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  1,
                  record,
                  formatMessage({ id: 'button.ParmRegionManage.details' })
                )
              }
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  2,
                  record,
                  formatMessage({ id: 'button.ParmRegionManage.mod' })
                )
              }
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() =>
                this.onClickActionExecuteEvent(
                  3,
                  this.hookProcess(3, record),
                  null,
                  'parmregionmanage/updateParmRegionEnable'
                )
              }
            >
              {record.showEnable === 1
                ? formatMessage({ id: 'button.ParmRegionManage.show.false' })
                : formatMessage({ id: 'button.ParmRegionManage.show.true' })}
            </a>
          </span>
        ),
      },
    ];
  }

  /**
   * 点击响应函数
   */
  hookProcess = (type, record) => {
    // const {dispatch} = this.props;
    if (type === 3) {
      record.showEnable = record.showEnable === 1 ? 0 : 1; // 设置是否显示状态值
    } else {
      // 未处理
    }
    return record;
  };

  /**
   * 默认加载行政区划定义数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    // 获取-行政区划下-按国别代码regionLevel为1-行政区划数据(所有)
    dispatch({ type: 'parmregionmanage/getParmRegionOfCountryCodeAll' });
  }

  /**
   * 在此生命周期方法内做清理数据操作
   */
  componentWillUnmount() {
    const { dispatch } = this.props;
    // 在该组件移除挂载时需要清空-行政区划列表数据(因为需要用户再次选择行政区划时再查询设置parmRegionList数据)
    dispatch({
      type: 'parmregionmanage/changeCurrentSelectParmRegionCascaderCmpValue',
      payload: [],
    });
    dispatch({ type: 'parmregionmanage/changeParmRegionList', payload: { content: [], total: 0 } });
  }

  /**
   * 当[国家]选择改变时回调函数
   */
  onSelectParmRegionCallback = (value, ops) => {
    const { dispatch } = this.props;
    const { extra } = ops.props;
    // 更新-当前选择-级联行政区划定义的值(数组,仅为Cascader组件展示值使用)
    dispatch({
      type: 'parmregionmanage/changeCurrentSelectParmRegionCascaderCmpValue',
      payload: [],
    });
    // 更新-当前选择-行政区划-国家(对象)
    dispatch({ type: 'parmregionmanage/changeCurrentSelectParmRegionOfCountry', payload: extra });
    // 更新-当前选择-级联行政区划定义对象
    dispatch({ type: 'parmregionmanage/changeCurrentSelectParmRegionCascader', payload: extra });
    // 获取-根据国家代码-获取该国家下的-行政区划数据(所有)
    dispatch({ type: 'parmregionmanage/getParmRegionAllByParmRegion', payload: extra });
    // 当前页面回归第一页
    this.changeCurrentPage(1);
  };

  /**
   * 当[行政区划]改变时回调函数
   * @param {array} valueArr 当前选择-行政区划数据-区划代码(数组(逐级)
   * @param {array} opsArr 当前选择-行政区划数据对象(数组(逐级))
   */
  onChangeParmRegionCascaderCallback = (valueArr, opsArr) => {
    if (opsArr === undefined) return;
    const {
      currentSelectParmRegionOfCountry: csproc, // 当前选择-行政区划-国家(对象)
      dispatch,
    } = this.props;
    let pr = {}; // 当前选择-行政区划实际对象
    if (opsArr.length === 0) {
      // 当点击了Cascader框的清除按钮后的处理,对应到国家级别
      pr = csproc;
    } else {
      pr = opsArr[opsArr.length - 1].extra;
    }
    // 更新-当前选择-级联行政区划定义的值(数组,仅为Cascader组件展示值使用)
    dispatch({
      type: 'parmregionmanage/changeCurrentSelectParmRegionCascaderCmpValue',
      payload: valueArr,
    });
    // 更新-当前选择-级联行政区划定义对象
    dispatch({ type: 'parmregionmanage/changeCurrentSelectParmRegionCascader', payload: pr });
    // 获取-根据行政区划获取子行政区划数据(列表)(分页)
    dispatch({
      type: 'parmregionmanage/getParmRegion',
      payload: {
        countryCode: pr.countryCode,
        parentRegionCode: pr.regionCode,
        regionLevel: Number(pr.regionLevel) + 1,
      },
    });
    // 当前页面回归第一页
    this.changeCurrentPage(1);
  };

  /**
   * 渲染列表组件头部操作区划的组件
   */
  renderTableOperateAreaCmp = () => {
    const {
      currentSelectParmRegionCascaderCmpValue: csprccv, // 当前选择-级联行政区划定义的值(数组,仅为Cascader组件展示值使用)
      currentSelectParmRegionOfCountry: csproc, // 当前选择-行政区划-国家(对象)
      parmRegionOfCountryCodeAll: procca, // 行政区划定义数据内-按国别代码regionLevel为1-数据(所有)
      parmRegionAll: pra, // 当前国别下行政区划数据(所有)(树相结构)
    } = this.props;

    return (
      <div>
        <div className={styles.parmRegionTableOperateAreaCmp}>
          <div>
            <span>{formatMessage({ id: 'form.ParmRegionManage.country' })}:&nbsp;&nbsp;</span>
            <Select
              style={{ width: 150 }}
              value={csproc.regionCode} // 当前选择-行政区划定义-国家代码,默认为中国(对象)-国家代码值
              placeholder={formatMessage({ id: 'form.ParmRegionManage.pleaseSelect' })}
              onSelect={this.onSelectParmRegionCallback}
            >
              {procca.map(item => (
                <Select.Option key={item.regionCode} value={item.regionCode} extra={item}>
                  {item.regionName}
                </Select.Option>
              ))}
            </Select>
          </div>
          <div>
            <span>{formatMessage({ id: 'form.ParmRegionManage.region' })}:&nbsp;&nbsp;</span>
            <Cascader
              style={{ width: '40vw' }}
              options={formatParmRegionToAntdCascaderCmp(pra)}
              onChange={this.onChangeParmRegionCascaderCallback}
              notFoundContent={formatMessage({
                id: 'form.ParmRegionManage.message.notFoundContent',
              })}
              placeholder={formatMessage({ id: 'form.ParmRegionManage.pleaseSelect' })}
              expandTrigger="hover"
              value={csprccv}
              changeOnSelect
            />
          </div>
          <Button
            type="primary"
            onClick={() =>
              this.onClickActionExecuteEvent(
                10,
                null,
                formatMessage({ id: 'button.ParmRegionManage.add' })
              )
            }
          >
            {formatMessage({ id: 'button.common.add' })}
          </Button>
        </div>
      </div>
    );
  };

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableCurrentPage,
    } = this.state;
    const {
      parmRegionList,
      parmRegionListPageSize,
      currentSelectParmRegionCascader: csprc, // 当前选择-级联行政区划定义对象
      parmRegionTableLoading: prtl, // 加载行政区划定义列表状态
    } = this.props;

    const tableOperateAreaCmp = this.renderTableOperateAreaCmp();
    const tablePaginationOnChangeEventDispatchType = 'parmregionmanage/paginationParmRegion';
    const tablePaginationOnChangeEventCondition = {
      countryCode: csprc.countryCode,
      parentRegionCode: csprc.regionCode,
      regionLevel: Number(csprc.regionLevel) + 1,
    }; // 分页派发附加条件

    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'form.ParmRegionManage.tableTitle' })}
          tableColumns={this.tableColumns_ParmRegion}
          tableDataSource={parmRegionList}
          tableLoading={prtl}
          tableOperateAreaCmp={tableOperateAreaCmp}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType, // 分页dispatch的type
              tablePaginationOnChangeEventCondition // 分页附加条件
            )
          }
          tableTotalSize={parmRegionListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <ParmRegionUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ parmregionmanage }) => ({
  parmRegionList: parmregionmanage.parmRegionList, // 行政区划定义列表
  parmRegionListPageSize: parmregionmanage.parmRegionListPageSize, // 行政区划定义列表总数据
  parmRegionOfCountryCodeAll: parmregionmanage.parmRegionOfCountryCodeAll || [], // 行政区划定义数据内按-国别代码regionLevel为1-数据(所有)
  parmRegionAll: parmregionmanage.parmRegionAll, // 行政区划定义数据(所有,存储树形结构)
  currentSelectParmRegionCascader: parmregionmanage.currentSelectParmRegionCascader, // 当前选择-级联行政区划定义对象
  currentSelectParmRegionOfCountry: parmregionmanage.currentSelectParmRegionOfCountry, // 当前选择-行政区划-国家(对象)
  currentSelectParmRegionCascaderCmpValue: parmregionmanage.currentSelectParmRegionCascaderCmpValue, // 当前选择-级联行政区划定义的值(数组,仅为Cascader组件展示值使用)
  parmRegionTableLoading: parmregionmanage.parmRegionTableLoading, // 加载行政区划定义列表状态
}))(ParmRegionManage);
