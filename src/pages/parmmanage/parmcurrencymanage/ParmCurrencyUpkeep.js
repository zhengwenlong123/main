/* eslint-disable no-unused-expressions */
import React from 'react';
import { connect } from 'dva';
import { Button, Form, Input, message } from 'antd';
import { formatMessage } from 'umi/locale';
import moment from 'moment';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import styles from './index.less';

moment.locale('zh-cn');
/**
 * 币别定义维护组件
 */
@ValidationFormHoc
class ParmCurrencyUpkeep extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    tailFormItemLayout: {
      wrapperCol: { xs: { span: 24, offset: 0 }, sm: { span: 24, offset: 0 } },
    },
    confirmLoading: false, // 提交等待
  };

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const {
      form: { getFieldsValue },
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      validationForm,
      dispatch,
      modalOnChangePageCurrent,
    } = this.props;

    const status = validationForm();
    if (!status) return;
    this.setState({ confirmLoading: true });
    const values = getFieldsValue();

    if (mut === 2) {
      // 维护
      values.id = mue.id;
      dispatch({
        type: 'parmcurrencymanage/updateParmCurrency',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (mut === 10) {
      // 新增
      dispatch({
        type: 'parmcurrencymanage/addParmCurrency',
        payload: values,
        callback: this.onCallback,
      });
    }
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  render() {
    const { formItemLayout, tailFormItemLayout, confirmLoading } = this.state;
    const {
      modalVisibleOnChange,
      modalUpkeepType: mut,
      form: { getFieldDecorator },
    } = this.props;

    // mut 1详情; 2维护; 10新增
    return (
      <div className={`${styles.contentModal} ${styles.modalHandleFormScopeArea}`}>
        <Form {...formItemLayout}>
          <Form.Item label={formatMessage({ id: 'form.common.code' })}>
            {getFieldDecorator('currencyCode', {
              rules: [
                {
                  required: true,
                  message: formatMessage({ id: 'form.common.code.placeholder' }),
                  whitespace: true,
                },
              ],
            })(<Input disabled={mut === 1} />)}
          </Form.Item>
          <Form.Item label={formatMessage({ id: 'form.common.name' })}>
            {getFieldDecorator('currencyName', {
              rules: [
                {
                  required: true,
                  message: formatMessage({ id: 'form.common.name.placeholder' }),
                  whitespace: true,
                },
              ],
            })(<Input disabled={mut === 1} />)}
          </Form.Item>
          {mut === 1 ? null : (
            <Form.Item {...tailFormItemLayout}>
              <div className={styles.modalHandleBtnArea}>
                <Button
                  type="default"
                  htmlType="button"
                  onClick={() => modalVisibleOnChange(false)}
                >
                  {formatMessage({ id: 'button.common.cancel' })}
                </Button>
                <Button
                  type="primary"
                  htmlType="button"
                  onClick={this.onClickSubmit}
                  loading={confirmLoading}
                >
                  {formatMessage({ id: 'button.common.submit' })}
                </Button>
              </div>
            </Form.Item>
          )}
        </Form>
      </div>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const { modalUpkeepEntity: mue } = props;
      return {
        currencyCode: Form.createFormField({ value: mue ? mue.currencyCode : '' }),
        currencyName: Form.createFormField({ value: mue ? mue.currencyName : '' }),
      };
    },
  })(ParmCurrencyUpkeep)
);
