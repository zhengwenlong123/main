import React from 'react';
import { connect } from 'dva';
import { Button, Card, Divider } from 'antd';
import { formatMessage } from 'umi/locale';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import ParmCurrencyUpkeep from './ParmCurrencyUpkeep';

/**
 * 币别定义管理组件
 */
class ParmCurrencyManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.common.code' }),
        dataIndex: 'currencyCode',
        key: 'currencyCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.name' }),
        dataIndex: 'currencyName',
        key: 'currencyName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <span>
            <a
              type="primary"
              onClick={() => this.onClickActionExecuteEvent(1, record, '币别定义详情')}
            >
              {formatMessage({ id: 'button.common.details' })}
            </a>
            <Divider type="vertical" />
            <a
              type="primary"
              onClick={() => this.onClickActionExecuteEvent(2, record, '币别定义维护')}
            >
              {formatMessage({ id: 'button.common.modify' })}
            </a>
            {/* <Divider type="vertical" /> */}
            {/* <a type="primary" onClick={() =>this.onClickActionExecuteEvent(3,record,null,'parmregionmanage/updateParmCurrencyEnable')}>停用</a> */}
          </span>
        ),
      },
    ];
    super(props, tableColumns);
  }

  /**
   * 默认加载币别定义数据
   */
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({ type: 'parmcurrencymanage/getParmCurrency' });
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
      tableCurrentPage,
    } = this.state;
    const { parmCurrencyList, parmCurrencyListPageSize } = this.props;

    const tableOperateAreaCmp = (
      <Button
        type="primary"
        onClick={() => this.onClickActionExecuteEvent(10, null, '币别定义新增')}
      >
        {formatMessage({ id: 'button.common.add' })}
      </Button>
    );
    const tablePaginationOnChangeEventDispatchType = 'parmcurrencymanage/paginationParmCurrency';
    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'form.ParmCurrencyManage.tableTitle' })}
          tableColumns={tableColumns}
          tableDataSource={parmCurrencyList}
          tableOperateAreaCmp={tableOperateAreaCmp}
          tableCurrentPage={tableCurrentPage}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType
            )
          }
          tableTotalSize={parmCurrencyListPageSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <ParmCurrencyUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
              modalOnChangePageCurrent={this.changeCurrentPage}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(({ parmcurrencymanage }) => ({
  parmCurrencyList: parmcurrencymanage.parmCurrencyList,
  parmCurrencyListPageSize: parmcurrencymanage.parmCurrencyListPageSize,
}))(ParmCurrencyManage);
