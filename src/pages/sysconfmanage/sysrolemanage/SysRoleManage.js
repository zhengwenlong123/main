/* eslint-disable no-param-reassign,no-undef */
import React from 'react';
import { connect } from 'dva';
import { Card } from 'antd';
import { formatMessage } from 'umi/locale';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import SysRoleUpkeep from './SysRoleUpkeep';

/**
 * 系统角色管理组件
 */
class SysRoleManage extends MdcManageBasic {
  constructor(props) {
    const tableColumns = [
      {
        title: formatMessage({ id: 'form.sysRoleManage.attrName' }),
        dataIndex: 'attrName',
        key: 'attrName',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.sysRoleManage.attrCode' }),
        dataIndex: 'attrCode',
        key: 'attrCode',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.sysRoleManage.attrDesc' }),
        dataIndex: 'attrDesc',
        key: 'attrDesc',
        align: 'center',
      },
      {
        title: formatMessage({ id: 'form.common.options' }),
        key: 'action',
        align: 'center',
        render: (text, record) => (
          <a type="primary" onClick={() => this.allocateAuthorityEvent(record)}>
            {formatMessage({ id: 'form.sysRoleManage.promise' })}
          </a>
        ),
      },
    ];
    super(props, tableColumns);
  }

  /**
   *
   */
  allocateAuthorityEvent = () => {
    // 处理点击了分配权限按钮动作
  };

  /**
   * 加载默系统角色数据
   */
  componentDidMount() {
    // const { dispatch } = this.props;
    // 获取当前所有系统角色数据
    // dispatch({type: ''});
  }

  render() {
    const {
      modalTitle,
      modalVisible,
      modalUpkeepType,
      modalUpkeepEntity,
      tableColumns,
    } = this.state;

    return (
      <Card bordered={false}>
        <MdcManageBlock
          tableTitle={formatMessage({ id: 'form.sysRoleManage.tableTitle' })}
          tableColumns={tableColumns}
          tableDataSource={[]}
          tableOperateAreaCmp={tableOperateAreaCmp}
          tablePaginationOnChangeEvent={(page, pageSize) =>
            this.tablePaginationOnChangeEvent(
              page,
              pageSize,
              tablePaginationOnChangeEventDispatchType
            )
          }
          tableTotalSize={0} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
          modalTitle={modalTitle}
          modalVisible={modalVisible}
          modalContentCmp={
            <SysRoleUpkeep
              modalUpkeepType={modalUpkeepType}
              modalUpkeepEntity={modalUpkeepEntity}
              modalVisibleOnChange={this.modalVisibleOnChange}
            />
          }
          modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
        />
      </Card>
    );
  }
}

export default connect(() => ({}))(SysRoleManage);
