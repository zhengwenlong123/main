import { encodeBase64Content } from '@/utils/utils';
import AppGlobal from '@/utils/globalVariable';

// use localStorage to store the authority info, which might be sent from server in actual project.
/**
 * 权限string
 *
 * @param {string} str
 */
export function getAuthority(str) {
  // 这里得到权限的语句

  const authorityString =
    // typeof str === 'undefined' ? decodeBase64Content(localStorage.getItem('N-B2B-authority')) : str;
    typeof str === 'undefined' ? AppGlobal.authorityArray.array : str;

  let authority;
  try {
    authority = JSON.parse(authorityString);
  } catch (e) {
    authority = authorityString;
  }
  if (typeof authority === 'string') {
    return [authority];
  }
  return authority;
}

/**
 * 设置权限
 * @param {Array} authority 权限数组
 */
export function setAuthority(authority) {
  // 获取权限数组, 如果是单行string,就变成数组
  const proAuthority = typeof authority === 'string' ? [authority] : authority;
  // 将权限数组 存入localStorage
  return localStorage.setItem('N-B2B-authority', encodeBase64Content(JSON.stringify(proAuthority)));
}
