import { Authorized as RenderAuthorized } from 'ant-design-pro';
import { getAuthority } from './authority';
// import { getCode } from '@/utils/utils';
// import api from '@/services/index';

// const { AccountResource } = api;

let Authorized = RenderAuthorized(getAuthority()); // eslint-disable-line

// if (getAuthority().length === 0 && window.location.pathname !== '/login') {
//   AccountResource().then(data => {
//     Authorized = RenderAuthorized(getCode(data));
//   });
// }

// Reload the rights component
const reloadAuthorized = () => {
  Authorized = RenderAuthorized(getAuthority());
};

export { reloadAuthorized };
export default Authorized;
