import 'moment/locale/zh-cn';
import TableRender from './tableRenders';

/**
 * 常用表格render
 */
export default class Compute {
  // 计算数量
  static computeNum = (list, numName) => {
    if (!list && !numName) return 0;
    return TableRender.NumberRender(
      list.reduce((prev, next) => prev + (next[numName] || 0), 0) || 0
    );
  };

  // 计算金额
  static computeMoney = (list, numName, priceName) => {
    if (!list && !numName) return 0;
    return TableRender.CurrentRender(
      list.reduce((prev, next) => prev + (next[numName] || 0) * next[priceName], 0) || 0
    );
  };
}
