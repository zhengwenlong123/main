/* eslint-disable react/no-array-index-key */

import React, { Fragment } from 'react';
import { Divider, Badge } from 'antd';
import moment from 'moment';
import 'moment/locale/zh-cn';
import Link from 'umi/link';
import { ORDER_STATUS, AUDIT_STATUS, SHIPPING_STATUS } from '@/utils/const';

moment.locale('zh-cn');

/**
 * 常用表格render
 */
export default class TableRender {
  // 以YYYY-MM-DD格式返回日期
  static ShortDateRender = text => {
    if (!text) return '';
    return moment(text).format('YYYY-MM-DD');
  };

  // 给数字增加千分位
  static NumberRender = text => {
    if (!text) return '';
    return (text || 0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,');
  };

  // 除了增加千分位.也增加货币符号
  static CurrentRender = text => {
    if (!text) return '';
    return `￥${TableRender.NumberRender(text)}`;
  };

  // 生成一个link
  static LinkRender = (text, url) => <Link to={url}>{text}</Link>;

  // 传入一个links数组
  static LinkArrayRender = ary => {
    const lenth = ary ? ary.length : 0;
    return (
      <Fragment>
        {lenth > 0 &&
          ary.map((item, index) =>
            index !== ary.length - 1 ? (
              <Fragment key={index}>
                {item} <Divider type="vertical" />
              </Fragment>
            ) : (
              <Fragment key={index}>{item}</Fragment>
            )
          )}
      </Fragment>
    );
  };

  /**
   * 根据状态显示不同颜色的状态说明
   * @param orderStatusCode - 订单状态
   * @param orderPayStatusCode - 支付状态
   * @param orderShippingStatusCode - 发运状态
   * @param orderAuditStatusCode - 审核状态
   * @param text -
   * @constructor
   */
  static ColoredStatusRender = (
    orderStatusCode,
    orderPayStatusCode,
    orderShippingStatusCode,
    orderAuditStatusCode,
    text
  ) => {
    let color = 'lime';

    // #BFBFBF ●待提交
    if (orderStatusCode === ORDER_STATUS.drafted) color = '#BFBFBF';

    // #0E77D1 ●已创建(业务待审核)●已创建(商务待审核)●已创建(待财务审核)
    if (
      orderStatusCode === ORDER_STATUS.created &&
      [
        AUDIT_STATUS.draftedAuditing,
        AUDIT_STATUS.bizAuditPass,
        AUDIT_STATUS.commerceAuditPass,
      ].includes(orderAuditStatusCode)
    )
      color = '#0E77D1';

    // #FFBF00 ●已创建(业务不通过)●已创建(商务不通过)●已创建(财务不通过)
    if (
      orderStatusCode === ORDER_STATUS.created &&
      [
        AUDIT_STATUS.bizAuditFailed,
        AUDIT_STATUS.commerceAuditFailed,
        AUDIT_STATUS.financeAuditFailed,
      ].includes(orderAuditStatusCode)
    )
      color = '#FFBF00';

    // #FF0000 ●已创建(业务已作废)●已创建(商务已作废)●已创建(财务已作废)
    if (
      orderStatusCode === ORDER_STATUS.created &&
      [
        AUDIT_STATUS.bizAuditCancel,
        AUDIT_STATUS.commerceAuditCancel,
        AUDIT_STATUS.financeAuditCancel,
      ].includes(orderAuditStatusCode)
    )
      color = '#FF0000';

    // #00A854 ●已激活(未发货)●已激活(发货中)●已激活(部分发货)●已激活(已发货)
    if (
      orderStatusCode === ORDER_STATUS.created &&
      [
        SHIPPING_STATUS.Unshipped,
        SHIPPING_STATUS.Shipping,
        SHIPPING_STATUS.PartialShipment,
        SHIPPING_STATUS.ShippingCompleted,
      ].includes(orderShippingStatusCode)
    )
      color = '#00A854';

    // #939393 ●已完成
    if (orderStatusCode === ORDER_STATUS.completed) color = '#939393';

    // #CC0000 ●已取消●已中止●已退单
    if (
      [ORDER_STATUS.cancelled, ORDER_STATUS.terminated, ORDER_STATUS.returned].includes(
        orderStatusCode
      )
    )
      color = '#CC0000';

    return <Badge color={color} text={text} />;
  };

  /**
   * 根据状态显示不同颜色的状态说明
   */
  static ColoredStatusDefaultRender = record =>
    this.ColoredStatusRender(
      record.orderStatusCode,
      record.orderPayStatusCode,
      record.orderShippingStatusCode,
      record.orderAuditStatusCode,
      record.orderStatusDesc
    );
}
