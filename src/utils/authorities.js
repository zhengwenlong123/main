/**
 * @enum {string}
 * @desc 权限代码
 */
module.exports = {
  goods_manage: '4001', // 4001	商品管理
  goods_brand_manage: '4001001', // 4001001	品牌管理
  goods_classification_manage: '4001002', // 4001002	产品大类管理
  goods_category_manage: '4001003', // 4001003	产品类目管理
  goods_attr_name_manage: '4001004', // 4001004	属性名称管理
  goods_spu_info_manage: '4001005', // 4001005	产品信息管理
  goods_sku_info_manage: '4001006', // 4001006	商品信息管理
  goods_tag_manage: '4001007', // 4001007	标签管理
  goods_last_no_manage: '4001008', // 4001008	物料序号信息
  goods_apply_bill_manage: '4001009', // 4001009 产品申请单管理
  goods_apply_bill_audit: '4001010', // 4001010 产品申请单审核
  goods_ebs_category: '4001011', // 4001011 ebs分类
  goods_trade_info_manage: '4001012', // 4001012 交易产品信息管理

  cust_manage: '4002', // 4002	客户管理
  cust_company_info_manage: '4002001', // 4002001	企业信息管理
  cust_company_tag_manage: '4002002', // 4002002	企业标签管理
  cust_company_mapping_manage: '4002003', // 4002003	企业编码对照管理
  cust_oper_unit_type_manage: '4002004', // 4002004	经营单位类型定义
  cust_apply_bill_manage: '4002005', // 4002005 客户申请单
  cust_apply_bill_audit: '4002006', // 4002006 客户申请单审核
  cust_info_manage: '4002007', // 4002007 客户信息管理

  parm_manage: '4003', // 4003	参数管理
  parm_public_parameter_manage: '4003001', // 4003001	公用参数管理
  parm_country_manage: '4003002', // 4003002	国别定义管理
  parm_region_manage: '4003003', // 4003003	行政区划定义管理
  parm_currency_manage: '4003004', // 4003004	币别定义管理
};
