/* eslint-disable import/prefer-default-export */
import { formatMessage } from 'umi/locale';

export const FORM_LAYOUT = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 24 },
    md: { span: 6 },
    xl: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 24 },
    md: { span: 18 },
    xl: { span: 18 },
  },
};

/**
 * 订单状态
 */
export const ORDER_STATUS = {
  // 待提交
  drafted: '1',
  // 已创建
  created: '2',
  // 已激活
  activated: '3',
  // 发货中
  shipping: '4',
  // 已发货
  shipped: '5',
  // 已完成
  completed: '6',
  // 已取消
  cancelled: '7',
  // 已中止
  terminated: '8',
  // 已退单
  returned: '9',
};

/**
 * 支付状态
 */
export const PAYMENT_STATUS = {
  // 未支付
  unpaid: '1',
  // 部分支付
  partiallyPaid: '2',
  // 已支付
  paid: '3',
};

/**
 * 发货状态
 */
export const SHIPPING_STATUS = {
  // 等待备货waitingStock；发货中shipping；发货中止shipTerminated；发货完毕shipped；不能履行cannotPerform；
  // 好像DB状态和需求不符.暂时不修改
  /**
   * 未发运
   */
  Unshipped: '1',
  /**
   * 发货中
   */
  Shipping: '2',
  /**
   * 部分发货
   */
  PartialShipment: '3',
  /**
   * 发货中止
   */
  ShippingSuspension: '4',
  /**
   * 发货完毕
   */
  ShippingCompleted: '5',
  /**
   * 未完成订单
   */
  UncompletedOrders: '6',
};

/**
 * 审核状态
 */
export const AUDIT_STATUS = {
  // 待提交审核draftedAuditing,
  // 业务未通过bizFailed,
  // 业务已审核bizPassed,
  // 商务未通过commerceFailed,
  // 商务已审核commercePassed,
  // 财务未通过financeFailed,
  // 财务已审核financePassed,
  // 审核已完成completed,
  // 订单已作废cancelled,
  // 待提交审核
  draftedAuditing: '0',
  // 业务审核 通过
  bizAuditPass: '1',
  // 业务审核 不通过
  bizAuditFailed: '2',
  // 业务审核 作废
  bizAuditCancel: '3',
  // 商务审核 通过
  commerceAuditPass: '4',
  // 商务审核 不通过
  commerceAuditFailed: '5',
  // 商务审核 作废
  commerceAuditCancel: '6',
  // 财务审核 通过
  financeAuditPass: '7',
  // 财务审核 不通过
  financeAuditFailed: '8',
  // 财务审核 作废
  financeAuditCancel: '9',
};

/**
 * 产品申请单类型
 */
export const GOODS_APPLY_TYPE = {
  spu: '1',
  sku: '2',
  materiel: '3',
};

/** 产品状态（spu、sku、materiel） */
export const SPU_ENABLE = {
  enabled: { value: 1, label: formatMessage({ id: 'form.common.enabled' }) }, // 有效
  disabled: { value: 0, label: formatMessage({ id: 'form.common.disabled' }) }, // 无效
};
