import { compare } from '@/utils/utils';

const goodsTradeSpuInfoToDva = (info, dataSource = {}) => {
  // 处理的初始化数据
  const initVal = {
    goodsTradeSpuBasicInfo: {}, // 产品基本信息
    goodsTradeSpuBasicInfoValid: {}, // 产品基本信息（正式生效数据）
    goodsTradeSpuSkuInfo: [], // 产品SKU信息
    goodsTradeSpuCommonAttr: [], // 产品普通属性
    goodsTradeSpuModel: [], // 产品型号
    goodsTradeSpuDesc: [], // 产品描述
    goodsTradeSpuSKuGroupAttr: {}, // 产品商品分组从属属性
    goodsTradeSpuSkuGroupList: [], // 商品分组
    goodsTradeSpuSkuList: [], // 商品列表
    goodsTradeSpuMaterielList: [], // 物料数据
    goodsTradeSpuSkuAttrListAble: [], // 产品sku属性下拉列表数据（可用）
  };
  if (!info) return { ...initVal };
  const {
    goodsSpuInfo, // 展现产品信息的值对象
    goodsSkuInfoList, // 展现商品（单品）信息加强的值对象
    goodsSkuGroupList = [], // 展现商品（单品）分组加强的值对象
    goodsMaterielInfoList, // 展现商品（单品）物料信息加强的值对象
  } = info;

  const {
    // goodsCatagoryListAll = [], // 产品类目树结构数据
    goodsSpuSkuAttrListAll = [], // 产品sku属性下拉列表数据（所有）
  } = dataSource;

  const {
    goodsSpuAttrNormalList = [], // 产品普通属性
    goodsSpuAttrSkuList = [], // 产品sku信息
    goodsSpuDescList = [], // 产品描述
    goodsSpuModelList = [], // 产品型号
    goodsSpuGroupAttrSku = {}, // 产品商品分组从属属性
    ...rest // 产品基本信息
  } = goodsSpuInfo || {};

  /** 产品基本信息 */
  const goodsTradeSpuBasicInfo = { ...rest };
  // if (goodsTradeSpuBasicInfo && goodsTradeSpuBasicInfo.catagoryCode) {
  //   // 产品类目编码存在

  //   const catagoryCodeArr =
  //     getTreeParent(
  //       goodsCatagoryListAll,
  //       goodsTradeSpuBasicInfo.catagoryCode,
  //       'catagoryCode',
  //       'parentCode'
  //     ) || [];
  //   goodsTradeSpuBasicInfo.catagoryCode = catagoryCodeArr.map(v => v.catagoryCode) || [];
  //   goodsTradeSpuBasicInfo.catagoryName = catagoryCodeArr.map(v => v.catagoryName).join('/');
  // }

  /** 产品sku信息 */
  const goodsTradeSpuSkuInfo =
    (goodsSpuAttrSkuList &&
      goodsSpuAttrSkuList.map(v => {
        const { goodsSpuAttrSkuItemList = [], ...attrInfo } = v;
        const tags =
          (goodsSpuAttrSkuItemList &&
            goodsSpuAttrSkuItemList.map(i => ({
              ...i,
              code: i.skuAttrItemCode,
              name: i.skuAttrItemValue,
            }))) ||
          [];
        const attr = { ...attrInfo, key: attrInfo.skuAttrCode, label: attrInfo.skuAttrName };
        return { attr, tags };
      })) ||
    [];

  /** 产品普通属性 */
  const goodsTradeSpuCommonAttr =
    (goodsSpuAttrNormalList &&
      goodsSpuAttrNormalList.map(item => ({
        ...item,
        attr: {
          key: item.attrCode,
          label: item.attrName,
        },
      }))) ||
    [];

  /** 产品描述 */
  const goodsTradeSpuDesc = (goodsSpuDescList && goodsSpuDescList.sort(compare('seq'))) || [];

  /** sku属性下拉列表数据(可用：代表除去sku信息中的属性和已在普通属性中使用的属性数据) */
  const usedSkuAttr = (goodsSpuAttrSkuList && goodsSpuAttrSkuList.map(v => v.skuAttrCode)) || [];
  const usedCommonAttr =
    (goodsSpuAttrNormalList && goodsSpuAttrNormalList.map(v => v.attrCode)) || [];
  const goodsTradeSpuSkuAttrListAble = [...goodsSpuSkuAttrListAll].filter(
    v => !usedSkuAttr.includes(v.attrCode) && !usedCommonAttr.includes(v.attrCode)
  );

  /** 除sku信息中所定义外的sku属性 */
  const goodsTradeSpuSkuAttrListOutSku = [...goodsSpuSkuAttrListAll].filter(
    v => !usedSkuAttr.includes(v.attrCode)
  );

  /** 商品分组 */
  const goodsTradeSpuSkuGroupList = [...goodsSkuGroupList].map(v => {
    const { goodsSkuGroupSkuItemList = [], ...other } = v;
    const obj = {};
    goodsSkuGroupSkuItemList.forEach(item => {
      obj[item.skuAttrCode] = { code: item.skuAttrItemCode, name: item.skuAttrItemName };
    });
    return {
      ...other,
      subAttrCode: v.subAttrCode1 || '',
      subAttrName: v.subAttrName1 || '',
      ...obj,
    };
  });

  return {
    ...initVal,
    goodsTradeSpuBasicInfo, // 产品基本信息
    goodsTradeSpuBasicInfoValid: { ...goodsTradeSpuBasicInfo }, // 产品基本信息（正式生效数据）
    goodsTradeSpuSkuInfo, // 产品sku信息
    goodsTradeSpuCommonAttr, // 产品普通属性
    goodsTradeSpuModel: [...goodsSpuModelList], // 产品型号
    goodsTradeSpuDesc, // 产品描述
    goodsTradeSpuSKuGroupAttr: { ...goodsSpuGroupAttrSku }, // 产品商品分组从属属性
    goodsTradeSpuSkuGroupList, // 商品分组
    goodsTradeSpuSkuAttrListAble: [...goodsTradeSpuSkuAttrListAble], // 产品sku属性下拉列表数据（可用）
    goodsTradeSpuSkuAttrListOutSku, // 产品sku属性下拉列表数据（仅除去sku信息定义的sku属性之前）
    goodsTradeSpuSkuList: [...goodsSkuInfoList], // 商品数据列表
    goodsTradeSpuMaterielList: [...goodsMaterielInfoList], // 物料数据
  };
};

/**
 * 商品详情获取的数据，处理后存入dva
 * @param {*} info 需要处理的数据
 */
const goodsTradeSkuInfoToDva = info => {
  // 处理的初始化数据
  const initVal = {
    goodsTradeSkuBasicInfo: {}, // 商品基本信息
    goodsTradeSkuBasicInfoValid: {}, // 商品基本信息（正式生效数据）
    goodsTradeSkuAttrInfo: [], // 商品SKU信息
    goodsTradeSkuCommonAttr: [], // 商品普通属性
    goodsTradeSkuDesc: [], // 产品描述
  };
  if (!info) return { ...initVal };
  const {
    goodsSkuAttrNormalList = [], // 产品普通属性
    goodsSkuAttrSkuList = [], // 商品sku信息
    goodsSkuDescList = [], // 产品描述
    ...rest // 产品基本信息
  } = info;

  /** 产品普通属性 */
  const goodsTradeSkuCommonAttr =
    (goodsSkuAttrNormalList &&
      goodsSkuAttrNormalList.map(item => ({
        ...item,
        attr: {
          key: item.attrCode,
          label: item.attrName,
        },
      }))) ||
    [];

  /** 产品描述 */
  const goodsTradeSkuDesc = (goodsSkuDescList && goodsSkuDescList.sort(compare('seq'))) || [];

  return {
    ...initVal,
    goodsTradeSkuBasicInfo: { ...rest }, // 商品基本信息
    goodsTradeSkuBasicInfoValid: { ...rest }, // 商品基本信息（正式生效数据）
    goodsTradeSkuAttrInfo: [...goodsSkuAttrSkuList], // 商品SKU信息
    goodsTradeSkuCommonAttr, // 商品普通属性
    goodsTradeSkuDesc, // 产品描述
  };
};

export { goodsTradeSpuInfoToDva, goodsTradeSkuInfoToDva };
