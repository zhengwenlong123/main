/* eslint-disable no-bitwise */
import { parse, stringify } from 'qs';

/**
 * 将当前URL中?之后的参数解析成一个对象
 */
export function getPageQuery() {
  return parse(window.location.href.split('?')[1]);
}

/**
 * 将一个对象转成?a=xxx&b=xxx&c=xxx格式贴在path之后生成url
 * @param {string} path url
 * @param {object} query 参数对象
 */
export function getQueryPath(path = '', query = {}) {
  const search = stringify(query);
  if (search.length) {
    return `${path}?${search}`;
  }
  return path;
}

/* eslint no-useless-escape:0 import/prefer-default-export:0 */
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;

/**
 * 是否是url
 * @param {string} path urlstring
 */
export function isUrl(path) {
  return reg.test(path);
}

/**
 * 数组（对象型数组）去重
 * @param {*} arr 需要去重的对象数组
 * @param {*} field 重复的标志字段
 */
export function uniqueArr(arr, field) {
  const res = new Map();
  return arr.filter(a => !res.has(a[field]) && res.set(a[field], 1));
}

/**
 * base64加密
 * @param {string} commonContent
 */
export function encodeBase64Content(commonContent) {
  if (!commonContent || commonContent === '') return '';
  const base64Content = Buffer.from(commonContent).toString('base64');
  return base64Content;
}

/**
 * base64解密
 * @param {string} base64Content
 */
export function decodeBase64Content(base64Content) {
  if (!base64Content || base64Content === '') return '';
  let commonContent = base64Content.replace(/\s/g, '+');
  commonContent = Buffer.from(commonContent, 'base64').toString();
  return commonContent;
}

/**
 * 得到服务器返回的resource,取得当中的code拼成字串
 */
export function getCode(resource) {
  if (!resource) return [];

  const rtn = [];
  const getCodeAndSub = data => {
    data.forEach(item => {
      const { code } = item;
      rtn.push(code);
      if (item.children) {
        getCodeAndSub(item.children);
      }
    });
  };
  getCodeAndSub(resource);
  return rtn;
}

/**
 * 下载文件流成文件提示的公共方法
 * @param fileName 需要显示的文件名(带扩展名)
 * @param blob 文件流
 * @param filetype 文件类型,默认excel
 */
export const DownloadAttachment = (fileName, blob, filetype = 'application/vnd.ms-excel') => {
  if (blob) {
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(new Blob([blob], { type: filetype }), fileName);
    } else {
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = fileName;
      // 此写法兼容可火狐浏览器
      document.body.appendChild(link);
      const evt = document.createEvent('MouseEvents');
      evt.initEvent('click', false, false);
      link.dispatchEvent(evt);
      document.body.removeChild(link);
    }
  }
};

/* 将时间戳转换为事件
 * DateFormat
 * format YYYY/yyyy/YY/yy year
 * MM/M month
 * dd/DD/d/D day
 * hh/HH/h/H hour
 * mm/m minute
 * ss/SS/s/S second
 * 例如：formatDate(new Date( 1571328545000), 'yyyy-MM-dd hh:mm:ss')，输出为："2019-10-18 00:09:05"
 */
export function formatDate(date, formatStr) {
  if (formatStr == null) return null;

  let str = formatStr;
  const Week = ['日', '一', '二', '三', '四', '五', '六'];
  const month = date.getMonth() + 1;

  str = str.replace(/yyyy|YYYY/, date.getFullYear());
  str = str.replace(
    /yy|YY/,
    date.getYear() % 100 > 9 ? (date.getYear() % 100).toString() : `0${date.getYear() % 100}`
  );

  str = str.replace(/MM/, month > 9 ? month : `0${month}`);
  str = str.replace(/M/g, month);

  str = str.replace(/w|W/g, Week[date.getDay()]);

  str = str.replace(/dd|DD/, date.getDate() > 9 ? date.getDate().toString() : `0${date.getDate()}`);
  str = str.replace(/d|D/g, date.getDate());

  str = str.replace(
    /hh|HH/,
    date.getHours() > 9 ? date.getHours().toString() : `0${date.getHours()}`
  );
  str = str.replace(/h|H/g, date.getHours());
  str = str.replace(
    /mm/,
    date.getMinutes() > 9 ? date.getMinutes().toString() : `0${date.getMinutes()}`
  );
  str = str.replace(/m/g, date.getMinutes());

  str = str.replace(
    /ss|SS/,
    date.getSeconds() > 9 ? date.getSeconds().toString() : `0${date.getSeconds()}`
  );
  str = str.replace(/s|S/g, date.getSeconds());

  str = str.replace(/t/g, date.getHours() < 12 ? 'am' : 'pm');
  str = str.replace(/T/g, date.getHours() < 12 ? 'AM' : 'PM');

  return str;
}

/**
 * 通过传入当前节点的ID，查询出所有的父级节点
 * @param {array} dataSource  传入数据(数据为树型结构)
 * @param {string} nodeIdVal 当前节点ID值
 * @param {string} id 节点标志字段
 * @param {string} parentId 当前节点的父id标志字段
 */
export function getTreeParent(dataSource, nodeIdVal, id, parentId) {
  if (!nodeIdVal) return [];
  let arrRes = [];
  if (dataSource.length === 0) {
    if (nodeIdVal) {
      arrRes.unshift(dataSource);
    }
    return arrRes;
  }
  const rev = (data, nodeId) => {
    for (let i = 0, { length } = data; i < length; i += 1) {
      const node = data[i];
      if (node[id] === nodeId) {
        arrRes.unshift(node);
        rev(dataSource, node[parentId]);
        break;
      } else if (node.childrens) {
        rev(node.childrens, nodeId);
      }
    }
    return arrRes;
  };
  arrRes = rev(dataSource, nodeIdVal);
  return arrRes;
}

/**
 * 判断obj是否为空
 */
export function isEmptyObj(obj) {
  return obj === null || obj === undefined || JSON.stringify(obj) === '{}';
}

/**
 * 判断string是否为空
 */
export function isEmptyStr(str) {
  return str === null || str === undefined || str === '';
}

/* 产品申请单详情接口获取的物料数据进行格式化
 * @param {array} list 未格式化的物料数据
 */
export function formatMaterielData(list) {
  if (!list) return [];
  if (Array.isArray(list) && list.length === 0) return [];
  let materielList = [];
  list.forEach(v => {
    const { skuName = '' } = v.goodsRfSkuInfo || {};
    const arr = v.goodsRfMaterielInfoList.map(item => {
      const { goodsRfMaterielEbs, ...other } = item;
      const { manufactureMaterielCode = '', customizedMachineType = '' } = goodsRfMaterielEbs || {};
      return { ...other, manufactureMaterielCode, customizedMachineType, skuName };
    });
    materielList = materielList.concat(arr);
  });
  return materielList;
}

/**
 *sort方法根据数组中对象的某一个属性值进行排序
 * @param {*} property 对象的某个属性
 * example： arr.sort(compare('age'))
 */
export function compare(property) {
  return (a, b) => {
    const value1 = a[property];
    const value2 = b[property];
    return value1 - value2;
  };
}
