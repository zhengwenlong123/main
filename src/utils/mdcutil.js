/* eslint-disable no-param-reassign */
/* eslint-disable no-useless-escape */
import request from '@/utils/request';
import { serverUrl } from '@/defaultSettings';

/* eslint-disable func-names */
/* eslint-disable compat/compat */

/**
 * 判断obj是空
 * @param {any} obj
 * @return {boolean} true is empty,otherwise false
 */
export function isEmpty(obj) {
  let res = true;
  switch (typeof obj) {
    case 'string':
      if (obj.length > 0) res = false;
      break;
    case 'object':
      if (obj instanceof Array && obj.length() > 0) res = false;
      // eslint-disable-next-line compat/compat
      if (obj instanceof Object && Object.values().length() > 0) res = false;
      break;
    case 'number':
      res = false;
      break;
    case 'boolean':
      res = false;
      break;
    default:
      break;
  }
  return res;
}

/**
 * 判断obj非空
 * @param {object} obj
 */
export function isNotEmpty(obj) {
  let res = true;
  switch (typeof obj) {
    case 'string':
      if (obj === null || (obj === undefined && obj.length <= 0)) res = false;
      break;
    case 'object':
      if (obj instanceof Array && obj.length() <= 0) res = false;
      if (obj instanceof Object && Object.values().length() <= 0) res = false;
      break;
    default:
      break;
  }

  return res;
}

/**
 * 调用后台接口请求结果处理
 * 返回数据结构为：{content:[],total:0}
 */
export function requestResult(res) {
  let result = {};
  if (res !== undefined && res != null && Object.values(res).length > 0 && res.code === 0) {
    result = res.data;
  }
  return result;
}

/**
 * 请求接口传参数统一在此处包裹tid值
 * 包裹tid参数
 * @param {object} req
 */
export function wrapTid(req) {
  if (req === null || req === undefined) req = {};
  req.rid = 123456;
  req.tid = 10001;
  req.createUid = 1;
  req.modifyUid = 1;
  return req;
}

/**
 * 从obj当中获取所有id,并返回isArray值
 * obj当中可包含childrens子对象
 * @param {object} obj
 * @returns {array} id数组字符串,使用逗号分隔开
 */
export function filterIds(obj) {
  const isArray = [];
  isArray.push(obj.id);
  const childs = obj.childrens || null;
  if (childs === undefined || childs === null || childs.length === 0) return isArray.toString();

  const tempFilter = data => {
    isArray.push(data.id);
    const { childrens } = data;
    if (childrens !== undefined && childrens !== null && childrens.length > 0) {
      childrens.forEach(element => {
        tempFilter(element);
      });
    }
  };

  tempFilter(childs);
  return isArray.toString();
}

/**
 * 示例: [ {"countryCode":"CN","id":"CN_820000000000","childrens":[ {"childrens":[ ],"countryCode":"CN","id":"CN_820000000000"]},{}]
 * 1:国家；2:省/直辖市；3市；4区/县;5乡镇;6村
 * @param {string} regionData 源始数据
 * @warning 默认会移除掉regionLevel为1的值
 */
export function parseRegionDataHandle(regionData) {
  let origin;
  if (Array.isArray(regionData)) {
    origin = regionData;
  } else {
    origin = JSON.parse(regionData);
  }
  // 取出regionLevel为1数据
  const regionLevelOneArray = origin.filter(item => Number(item.regionLevel) === 1); // 解析国家
  if (
    regionLevelOneArray === undefined ||
    regionLevelOneArray === null ||
    regionLevelOneArray.length === 0
  )
    return [];

  const parseChildRegionHandle = (data, level) => {
    if (data === undefined || data === null) return;
    const tempArray = origin.filter(
      item => Number(item.regionLevel) === level && item.parentRegionCode === data.regionCode
    );
    if (tempArray.length !== 0) tempArray.forEach(item => parseChildRegionHandle(item, level + 1));
    // eslint-disable-next-line no-param-reassign
    data.childrens = tempArray;
  };

  parseChildRegionHandle(regionLevelOneArray[0], 2); // 解析省/直辖市
  return regionLevelOneArray[0].childrens.filter(item => item);
}

/**
 * 格式化数据，组装成antd下Cascader组件使用的数据格式
 */
export function formatParmRegionToAntdCascaderCmp(data) {
  const process = itemChild => {
    const tempArr = [];
    itemChild.map(item => {
      const { childrens, regionCode, regionName } = item;
      const tempObj = {};
      tempObj.label = regionName;
      tempObj.key = regionCode;
      tempObj.value = regionCode;
      tempObj.extra = item;
      if (childrens !== undefined && childrens.length > 0) {
        tempObj.children = process(childrens);
      }
      tempArr.push(tempObj);
      return null;
    });
    return tempArr;
  };
  return process(data);
}

/**
 * 格式化数据，组装成antd下TreeSelect组件使用的数据格式
 */
export function formatParmRegionToAntdTreeSelectCmp(data) {
  const process = itemChild => {
    const tempArr = [];
    itemChild.map(item => {
      const { childrens, regionCode, regionName } = item;
      const tempObj = {};
      tempObj.title = regionName;
      tempObj.key = regionCode;
      tempObj.value = regionCode;
      tempObj.disabled = !item.showEnable;
      tempObj.isLeaf = !item.showEnable;
      tempObj.extra = item;
      if (childrens !== undefined && childrens.length > 0) {
        tempObj.children = process(childrens);
      }
      tempArr.push(tempObj);
      return null;
    });
    return tempArr;
  };
  return process(data);
}

/**
 * 世界所属洲
 */
export function worldContinentDefault() {
  return [
    { code: 1, name: '亚洲' },
    { code: 2, name: '欧洲' },
    { code: 3, name: '非洲' },
    { code: 4, name: '大洋洲' },
    { code: 5, name: '北美洲' },
    { code: 6, name: '南美洲' },
  ];
}

/**
 * 获取文件的base64数据
 * @param {file} file
 */
export function getFileBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

/**
 * 验证客户申请单数据唯一性,验证单条数据，查询临时表中的数据，即包含申请单所有状态的数据,
 * 返回数据用于除去当前申请单的数据
 * @param {string} requestLink
 * @param {object} params
 */
export async function verifyCustApplyUnique(requestLink, params) {
  const res = await request.post(`${serverUrl}${requestLink}`, { data: params });
  if (res === undefined) return false; // 验证失败
  return requestResult(res);
}

/**
 * 验证数据唯一性,只能对验证单条数据
 * @param {string} requestLink
 * @param {object} params
 */
export async function verifyUnique(requestLink, params) {
  const res = await request.post(`${serverUrl}${requestLink}`, { data: params });
  if (res === undefined) return false; // 验证失败
  const result = requestResult(res);
  if (Object.keys(result).length > 0 && result.id !== null) {
    return false;
  }
  return true;
}

/**
 * 执行一次请求操作
 * @param {string} requestLink
 * @param {object} params
 * @returns 请求结果数据
 */
export async function execOneRequest(requestLink, params) {
  const res = await request.post(`${serverUrl}${requestLink}`, { data: wrapTid(params) });
  const result = requestResult(res);
  if (Object.keys(result).length === 0) return null;
  return result;
}

/**
 * 匹配附件绝对地址的相对路径
 */
const fileRelationPathReg = /http\:\/\/[A-Za-z\.\:\d]+\//g;
export { fileRelationPathReg };

/**
 * 验证inpt的value为数字,否则清空
 * @param {Input} input 组件
 */
export async function verifyInputTypeNumber(cmp) {
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(cmp.input.value)) {
    cmp.input.value = '';
  } else {
    // 验证通过,无处理
  }
}

/**
 * 去出字符串两边的空格
 */
export function trim(value) {
  if (!value) return '';
  return value.replace(/(^\s*) | (\s*$)/g, '');
}
