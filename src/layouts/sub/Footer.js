import React, { Fragment } from 'react';
import { Layout, Icon } from 'antd';
import { GlobalFooter } from 'ant-design-pro';
import imgPolic from '@/assets/image/police.png';
import imgLicense from '@/assets/image/business_license.png';

const { Footer } = Layout;
const FooterView = () => (
  <Footer style={{ padding: 0 }}>
    <GlobalFooter
      links={[
        {
          key: 'police',
          title: (
            <Fragment>
              <img src={imgPolic} alt="police" />
              鄂公网安备 42010202000791号
            </Fragment>
          ),
          href: 'http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=42010202000791',
          blankTarget: true,
        },
        {
          key: '经营证照',
          title: '经营证照',
          href: imgLicense,
          blankTarget: true,
        },
      ]}
      copyright={
        <Fragment>
          <Icon type="copyright" /> 2015-2019 中国邮电器材中南有限公司 经营许可证编号:
          鄂B1.B2-20150119 | 鄂ICP备15005688号
        </Fragment>
      }
    />
  </Footer>
);
export default FooterView;
