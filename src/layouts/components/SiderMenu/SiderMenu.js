import React, { PureComponent, Suspense } from 'react';
import { Layout, Switch } from 'antd';
import { formatMessage } from 'umi/locale';
import classNames from 'classnames';
import Link from 'umi/link';
import styles from './index.less';
import PageLoading from '../PageLoading';
import { getDefaultCollapsedSubMenus } from './SiderMenuUtils';
import { defaultPage } from '@/defaultSettings';

const BaseMenu = React.lazy(() => import('./BaseMenu'));
const { Sider } = Layout;

export default class SiderMenu extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      openKeys: getDefaultCollapsedSubMenus(props),
    };
  }

  static getDerivedStateFromProps(props, state) {
    const { pathname } = state;
    if (props.location.pathname !== pathname) {
      return {
        pathname: props.location.pathname,
        openKeys: getDefaultCollapsedSubMenus(props),
      };
    }
    return null;
  }

  isMainMenu = key => {
    const { menuData } = this.props;
    return menuData.some(item => {
      if (key) {
        return item.key === key || item.path === key;
      }
      return false;
    });
  };

  handleOpenChange = openKeys => {
    const moreThanOne = openKeys.filter(openKey => this.isMainMenu(openKey)).length > 1;
    this.setState({
      openKeys: moreThanOne ? [openKeys.pop()] : [...openKeys],
    });
  };

  render() {
    const { logo, collapsed, onCollapse, fixSiderbar, theme, changeThemeColor } = this.props;
    const { openKeys } = this.state;
    const defaultProps = collapsed ? {} : { openKeys };

    const siderClassName = classNames(styles.sider, {
      [styles.fixSiderbar]: fixSiderbar,
      [styles.light]: theme === 'light',
    });
    return (
      <Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        breakpoint="lg"
        onCollapse={onCollapse}
        width={256}
        theme={theme}
        className={siderClassName}
      >
        <div className={styles.logo} id="logo">
          <Link to={defaultPage}>
            <img src={logo} alt="logo" />
            {/* <h1>{systemName}</h1> */}
          </Link>
        </div>
        <Suspense fallback={<PageLoading />}>
          <BaseMenu
            {...this.props}
            mode="inline"
            handleOpenChange={this.handleOpenChange}
            onOpenChange={this.handleOpenChange}
            style={{ padding: '16px 0', width: '100%' }}
            {...defaultProps}
          />
          <div className={styles.setTheme}>
            {!collapsed && (
              <span
                style={{
                  color: theme === 'dark' ? '#fff' : 'rgba(0, 0, 0, 0.65)',
                  marginLeft: '24px',
                }}
              >
                {formatMessage({ id: 'app.setting.themeSet' })}
              </span>
            )}
            <Switch
              checked={theme === 'light'}
              className={styles.isAntdSwitch}
              style={{ backgroundColor: '#1890ff', marginLeft: '10px' }}
              onChange={changeThemeColor}
              checkedChildren={formatMessage({ id: 'app.setting.light' })}
              unCheckedChildren={formatMessage({ id: 'app.setting.dark' })}
            />
          </div>
        </Suspense>
      </Sider>
    );
  }
}
