import React, { PureComponent } from 'react';
import { FormattedMessage, formatMessage } from 'umi/locale';
import { Spin, Menu, Icon, Avatar, Tooltip } from 'antd';
// import HeaderSearch from '../HeaderSearch';
import HeaderDropdown from '../HeaderDropdown';
// import SelectLang from '../SelectLang';
import styles from './index.less';

import { alibabaFontIcont } from '@/defaultSettings';

const IconFont = Icon.createFromIconfontCN({
  scriptUrl: alibabaFontIcont,
});

/**
 * 这里是上方header的右边白色部分
 * 包含了搜索, 帮助, 切换系统, 消息, 当前登入者, 登出, 选择语言
 */
export default class GlobalHeaderRight extends PureComponent {
  // 获取 可用平台列表的panel
  getCanUseAppsData() {
    const { canUseApps = [] } = this.props;
    if (canUseApps.length === 0) {
      return {};
    }

    const count = canUseApps && Array.isArray(canUseApps) ? canUseApps.length : 0;
    const newApps = [];
    canUseApps.forEach((item, index) => {
      if (!item) return;
      const {
        appVO: { id, name, code, imageUrl, url, visible },
      } = item;
      if (!visible) return;

      const cls =
        index === count - 1
          ? { display: 'table' }
          : { display: 'table', borderBottom: '1px solid #e8e8e8' };

      newApps.push(
        <Menu.Item key={id} code={code} url={url} style={cls}>
          <IconFont
            type={imageUrl || 'iconxitongguanli'}
            style={{
              fontSize: 20,
              display: 'table-cell',
              verticalAlign: 'middle',
              width: 22,
              height: 30,
              paddingRight: 12,
            }}
          />
          <span style={{ display: 'table-cell', verticalAlign: 'middle' }}>{name}</span>
        </Menu.Item>
      );
    });

    return (
      <Menu className={styles.menu} onClick={this.handlerChangeApp} style={{ width: 160 }}>
        {newApps}
      </Menu>
    );
  }

  // 点击 切换平台中的某一个子系统 跳转到该系统中
  handlerChangeApp = ({
    item: {
      props: { code, url },
    },
  }) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'global/getAppTokenAndRedirect',
      payload: { code, url },
    });
  };

  render() {
    const { currentUser, onMenuClick, theme, canUseApps = [] } = this.props;
    // 点头像弹出的菜单
    const menu = (
      <Menu className={styles.menu} selectedKeys={[]} onClick={onMenuClick}>
        <Menu.Item key="userinfo">
          <Icon type="setting" />
          <FormattedMessage id="menu.account.settings" defaultMessage="account settings" />
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="logout">
          <Icon type="logout" />
          <FormattedMessage id="menu.account.logout" defaultMessage="logout" />
        </Menu.Item>
      </Menu>
    );

    const changeAppContent = this.getCanUseAppsData();

    let className = styles.right;
    if (theme === 'dark') {
      className = `${styles.right}  ${styles.dark}`;
    }
    return (
      <div className={className}>
        {/* 搜索 */}
        {/* <HeaderSearch
          className={`${styles.action} ${styles.search}`}
          placeholder={formatMessage({ id: 'component.globalHeader.search' })}
          dataSource={[
            formatMessage({ id: 'component.globalHeader.search.example1' }),
            formatMessage({ id: 'component.globalHeader.search.example2' }),
            formatMessage({ id: 'component.globalHeader.search.example3' }),
          ]}
          onSearch={value => {
            console.log('input', value); // eslint-disable-line
          }}
          onPressEnter={value => {
            console.log('enter', value); // eslint-disable-line
          }}
        /> */}
        {/* 帮助 */}
        {/* <Tooltip title={formatMessage({ id: 'component.globalHeader.help' })}>
          <a
            target="_blank"
            href="http://www.ptaczn.com/"
            rel="noopener noreferrer"
            className={styles.action}
          >
            <Icon type="question-circle-o" />
          </a>
        </Tooltip> */}
        {/* 切换系统 */}
        {canUseApps.length > 0 && (
          <Tooltip title={formatMessage({ id: 'component.globalHeader.changeapp' })}>
            <HeaderDropdown overlay={changeAppContent} trigger={['click']}>
              <a className={styles.action}>
                <IconFont type="iconqiehuanxitong" />
              </a>
            </HeaderDropdown>
          </Tooltip>
        )}

        {/* 人名 */}
        {currentUser.name ? (
          <HeaderDropdown overlay={menu}>
            <span className={`${styles.action} ${styles.account}`}>
              <Avatar
                size="small"
                className={styles.avatar}
                src={currentUser.avatar}
                alt="avatar"
              />
              <span className={styles.name}>{currentUser.name}</span>
            </span>
          </HeaderDropdown>
        ) : (
          <Spin size="small" style={{ marginLeft: 8, marginRight: 8 }} />
        )}
        {/* 选择语言 */}
        {/* <SelectLang className={styles.action} /> */}
      </div>
    );
  }
}
