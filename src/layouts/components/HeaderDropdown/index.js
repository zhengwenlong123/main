import React, { PureComponent } from 'react';
import { Dropdown } from 'antd';
import classNames from 'classnames';

export default class HeaderDropdown extends PureComponent {
  render() {
    const { overlayClassName, ...props } = this.props;
    return <Dropdown overlayClassName={classNames(overlayClassName)} {...props} />;
  }
}
