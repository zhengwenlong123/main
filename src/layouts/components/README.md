---
公共组件说明
这里是所有框架页面所需的控件.单独放这里以免和上面页面使用的搞混
---
###BreadcrumbView
最上面的面包屑.会自动根据地址显示

### GlobalHeader
从ant-desgin-pro来的. BasicLayout上方菜单.包含左边LOGO区和右边白色区域
(不可删除)

### HeaderDropdown
从ant-desgin-pro来的. 上方head区域的下拉.选择语言,选择系统,人菜单都有用到.
(不可删除)

### HeaderSearch
从ant-desgin-pro来的. 头部搜索按钮.点了会出现搜索输入框.目前没有实现具体逻辑.只是留着好看

### PageLoading
从ant-desgin-pro来的. 页面载入的全局转圈圈.
(不可删除)

### SelectLang
从ant-desgin-pro来的. 头部语言选择
(不可删除)

### SiderMenu
从ant-desgin-pro来的. 侧边菜单
(不可删除)

### TopNavHeader
从ant-desgin-pro来的. 在手机模式下的header
(不可删除)
