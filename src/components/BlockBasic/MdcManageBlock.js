/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import TableBlock from '@/components/TableBasic/TableBlock';
import ModalBasic from '@/components/ModalBasic/ModalBasic';

/**
 * 主数据管理Table+Modal对话框维护区块组件
 */
const MdcManageBlock = props => (
  <div>
    <TableBlock {...props} />
    <ModalBasic {...props} />
  </div>
);

/**
 * 定义MdcManageBlock组件props属性数据类型格式
 */
MdcManageBlock.propTypes = {
  tableColumns: PropTypes.array.isRequired, // 表格列表字段
  tableDataSource: PropTypes.array.isRequired, // 表格数据源
  tableTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.element]), // 表格标题
  tableOperateAreaCmp: PropTypes.element, // 表格操作区域组件
  tableCurrentPage: PropTypes.number, // 表格分页当前页
  tablePaginationOnChangeEvent: PropTypes.func, // 表格分页onChange事件函数
  tableTotalSize: PropTypes.number, // 表格数据总数
  tableRowSelection: PropTypes.object, // 表格列表勾选处理对象
  tableLoading: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]), // 表格列表加载状态
  hasTableHeader: PropTypes.bool, // 表格头部是否存在
  hasTableFooter: PropTypes.bool, // 表格尾部是否存在
  modalTitle: PropTypes.string, // 对话框标题
  modalContentCmp: PropTypes.element, // 对话框内容组件
  modalVisible: PropTypes.bool, // 对话框显示/关闭状态
  modalOnCancelEvent: PropTypes.func, // 对话框关闭事件函数
  modalWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]), // 对话框宽度值
  modalBodyStyle: PropTypes.object, // 对话框自身样式
  modalStyle: PropTypes.object, // 对话框展示样式
  hasModal: PropTypes.bool, // 对话框是否存在
};

export default MdcManageBlock;
