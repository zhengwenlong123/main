/* eslint-disable no-throw-literal */
/* eslint-disable react/no-unused-state */
import React from 'react';
import { formatMessage } from 'umi/locale';
import { isEmpty } from '@/utils/mdcutil';
import { message, Modal } from 'antd'; // eslint-disable-line

/**
 * 主数据管理-基础公共代码类组件
 */
class MdcManageBasic extends React.PureComponent {
  constructor(props, tableColumns = []) {
    super(props);
    this.state = {
      modalVisible: false, // Modal对话框显示/关闭状态
      modalTitle: '', // Modal对话框标题
      modalUpkeepType: 0, // Modal对话框操作类型,默认0; 1详情; 2修改; 10新增
      modalUpkeepEntity: null, // Modal对话框操作实体
      tableColumns, // 表格字段定义
      tableTotalSize: 0, // 表格数据总数量; 仅测试使用,在生产环节废弃该值
      tableCurrentPage: 1, // 表格分页当前页面
    };
  }

  /**
   * 对话框[visible]更改事件
   */
  modalVisibleOnChange = type => {
    this.setState({ modalVisible: type });
  };

  /**
   * 表格[分页]按钮事件
   * @param {number} page 当前第几页,默认值0
   * @param {number} pageSize 每页数量,默认值10
   * @param {string} dispatchType 动作派发类型
   * @param {object} condition 动作派发条件
   */
  tablePaginationOnChangeEvent = (page = 0, pageSize = 10, dispatchType, condition = {}) => {
    // eslint-disable-next-line no-throw-literal
    if (isEmpty(dispatchType))
      // eslint-disable-next-line no-throw-literal
      throw 'MdcManageBasic:调用tablePaginationOnChangeEvent方法时未定义dispatchType值,处理错误';
    const { dispatch } = this.props;
    dispatch({ type: dispatchType, payload: { page, pageSize, ...condition } });
    this.changeCurrentPage(page);
  };

  /**
   * 修改后当前分页修改为指定页面
   */
  changeCurrentPage = currentPage => {
    this.setState({ tableCurrentPage: currentPage });
  };

  /**
   * Table表格操作点击事件
   * @param {number} type  对话框操作类型,默认0; 1详情; 2修改; 10新增;
   * @param {object} record 当前行数据对象实体
   * @param {string} title 对话框标题
   * @param {string} dispatchType 派发类型值
   */
  onClickActionExecuteEvent = (
    type,
    record = null,
    title = null,
    dispatchType = null,
    searchCondition = null
  ) => {
    switch (type) {
      case 1: // 详情
      case 2: // 修改
      case 10: // 新增
        this.setState({
          modalVisible: true,
          modalTitle: title,
          modalUpkeepType: type,
          modalUpkeepEntity: record,
        });
        break;
      case 3: // 停用/启用
        Modal.confirm({
          title: formatMessage({ id: 'form.common.prompt' }),
          content: formatMessage({ id: 'form.common.confirm.question' }),
          okText: formatMessage({ id: 'form.common.sure' }),
          cancelText: formatMessage({ id: 'form.common.cancel' }),
          onOk: () => {
            // 直接调用后台接口处理
            const { dispatch } = this.props;
            // eslint-disable-next-line
            const dispatchObj = {
              type: dispatchType,
              payload: record,
              callback: this.onCallbackTips,
            };
            if (searchCondition) {
              dispatchObj.searchCondition = searchCondition;
            }
            dispatch(dispatchObj);
            this.changeCurrentPage(1);
          },
          onCancel: () => {
            // 无处理
          },
        });
        break;
      case 20: // 删除
        Modal.confirm({
          title: formatMessage({ id: 'form.common.prompt' }),
          content: formatMessage({ id: 'form.common.cancel.question' }),
          okText: formatMessage({ id: 'form.common.sure' }),
          cancelText: formatMessage({ id: 'form.common.cancel' }),
          onOk: () => {
            // 直接调用后台接口处理
            const { dispatch } = this.props;
            dispatch({ type: dispatchType, payload: record, callback: this.onCallbackTips });
            this.changeCurrentPage(1);
          },
          onCancel: () => {
            // 无处理
          },
        });
        break;
      default:
        throw `MdcManageBasic:${formatMessage({ id: 'form.common.MdcManageBasic.defaultError' })}`;
    }
  };

  /**
   * 请求回调提示
   */
  onCallbackTips = result => {
    if (result) {
      message.info(formatMessage({ id: 'form.common.optionSuccess' }));
    } else {
      message.warn(formatMessage({ id: 'form.common.optionError' }));
    }
  };

  /**
   * 过滤商品主数据资源
   * 对商品主数据-资源数据过滤操作
   * @param {string} descTypeCode 资源类型代码
   * @param {array} data 资源数据列表
   * @returns 按条件过滤后新资源数据列表
   */
  filterGoodsDesc = (descTypeCode, data = []) =>
    data.filter(item => item.descTypeCode === descTypeCode);
}

export default MdcManageBasic;
