import React, { Fragment } from 'react';
import { Table, Form, Divider } from 'antd';
import { formatMessage } from 'umi/locale';

const FormItem = Form.Item;
const EditableContext = React.createContext();

const EditableCell = props => {
  const {
    editing,
    dataIndex,
    title,
    // inputType,
    record,
    index,
    renderInput,
    validate,
    validateTitle,
    initialValueType,
    init,
    rulesVal,
    ...restProps
  } = props;

  /** 表单检验可有俩种办法定义
   * 一、自定义，采用rules字段，格式完全依照form的rules格式
   * 二、默认定义，{required， message},通过字段validate（boolean）,
   *    validateTitle(string/字段中文名称)
   */
  const rules = rulesVal || [
    {
      required: !!validate,
      message: `${formatMessage({ id: 'form.common.input' })}${validateTitle}`,
    },
  ];

  return (
    <EditableContext.Consumer>
      {form => {
        const { getFieldDecorator } = form;
        return (
          <td {...restProps}>
            {editing ? (
              <FormItem style={{ margin: 0 }}>
                {getFieldDecorator(dataIndex, {
                  rules,
                  initialValue: initialValueType ? init(record) : record[dataIndex],
                })(renderInput(form))}
              </FormItem>
            ) : (
              restProps.children
            )}
          </td>
        );
      }}
    </EditableContext.Consumer>
  );
};

class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [...props.columns, this.operationColumn()],
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      columns: [...nextProps.columns, this.operationColumn()],
    });
  }

  operation = (form, record) => {
    const { rowKey } = this.props;
    if (record.add) {
      return (
        <a style={{ marginRight: 8 }} onClick={() => this.save(form, record[rowKey], 'add')}>
          {formatMessage({ id: 'button.common.addition' })}
        </a>
      );
    }
    return (
      <a onClick={() => this.save(form, record[rowKey], 'edit')} style={{ marginRight: 8 }}>
        {formatMessage({ id: 'button.common.save' })}
      </a>
    );
  };

  operationColumn = () => {
    const { rowKey, otherButton = null, otherBtnRender = null, dataList } = this.props;
    return {
      title: formatMessage({ id: 'form.common.options' }),
      width: 95,
      dataIndex: rowKey,
      render: (text, record) => {
        const editable = this.isEditing(record);
        const disabled = this.isDisabled();
        return (
          <div>
            {editable ? (
              <span>
                <EditableContext.Consumer>
                  {form => this.operation(form, record)}
                </EditableContext.Consumer>
                <a onClick={() => this.cancel(record[rowKey], record.add)}>
                  {formatMessage({ id: 'button.common.cancel' })}
                </a>
              </span>
            ) : (
              <Fragment>
                <a disabled={disabled} onClick={() => this.edit(record[rowKey])}>
                  {formatMessage({ id: 'button.common.edit' })}
                </a>

                {(otherButton || otherBtnRender) && <Divider type="vertical" />}
                {otherButton && (
                  <a
                    disabled={disabled}
                    onClick={() => otherButton.onClick(record[rowKey], dataList, otherButton.value)}
                  >
                    {otherButton && otherButton.label}
                  </a>
                )}
                {otherBtnRender && otherBtnRender(record)}
              </Fragment>
            )}
          </div>
        );
      },
    };
  };

  editingKeySet = (val = '') => {
    const { editingKeyChange } = this.props;
    editingKeyChange(val);
  };

  isEditing = record => {
    const { rowKey, editingKey } = this.props;
    return record[rowKey] === editingKey;
  };

  isDisabled = () => {
    const { editingKey } = this.props;
    return editingKey !== '';
  };

  cancel = (key, add) => {
    const { dataList, onCanCel, rowKey } = this.props;
    const newData = [...dataList];
    const index = newData.findIndex(item => key === item[rowKey]);
    if (index > -1 && add) {
      newData.splice(index, 1);
      onCanCel(newData);
    } else {
      onCanCel(dataList);
    }
    this.editingKeySet();
  };

  save(form, key, type) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const { dataList, onSave, rowKey } = this.props;
      const newData = [...dataList];
      const index = newData.findIndex(item => key === item[rowKey]);
      if (type === 'add') {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        delete newData[index].add;
        onSave(index, row, newData, type);
      }
      if (index > -1 && type === 'edit') {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        onSave(index, newData[index], newData, type);
      } else if (type === 'edit') {
        newData.push(row);
        onSave(index, row, newData, type);
      }
    });
  }

  edit(key) {
    this.editingKeySet(key);
  }

  render() {
    const components = {
      body: {
        cell: EditableCell,
      },
    };
    const { columns = [] } = this.state;

    const columnsVal = columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          renderInput: col.renderInput,
          initialValueType: col.initialValueType,
          init: col.init,
          rulesVal: col.rules,
          validate: col.validate,
          validateTitle: col.validateTitle,
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    const {
      form,
      headTitle,
      dataList,
      rowKey,
      loading = false,
      bordered = false,
      scroll,
    } = this.props;
    return (
      <EditableContext.Provider value={form}>
        <Table
          rowKey={rowKey || 'key'}
          title={headTitle}
          bordered={bordered}
          loading={loading}
          components={components}
          dataSource={dataList}
          columns={columnsVal}
          rowClassName="editable-row"
          className="ediable-table-block small-margin-common-table"
          pagination={false}
          scroll={scroll}
          // {...other}
          // footer={() => this.footerRender()}
        />
      </EditableContext.Provider>
    );
  }
}

const EditableTableBlock = Form.create()(EditableTable);

export default EditableTableBlock;
