/* eslint-disable react/no-unused-state */
import React from 'react';
import { message } from 'antd';
import uuid from 'uuid';

/**
 * 主数据管理-可编辑表格公共代码类组件
 */
class EditableTableBasic extends React.PureComponent {
  constructor(props, childProps) {
    super(props);
    const { tableColumns = [], rowKey = 'id', dispatchType = null, validatorDispatch } = childProps;
    this.state = {
      tableColumns, // 表格字段定义
      dispatchType, // 派发类型
      rowKey, // 列标识
      editingKey: '',
      otherButton: {},
      nativeData: [], // 存于本地列表数据
    };
    this.validatorDispatchFnc = validatorDispatch || null; // 当使用dipatch保存数据到redux，可做校验只保存有效数据，但要有本地数据的保存
  }

  /**
   * 更新列表数值
   * @param {array} list  即将要更新的数据
   */
  updateData = list => {
    const { onChange, dispatch } = this.props;
    const { dispatchType } = this.state;
    if (onChange) {
      // 可以采用外层父组件onChange更新值到本地
      onChange(list);
    } else {
      // 可直接更新数据到本地
      this.setState({
        nativeData: list,
      });
    }
    if (dispatchType) {
      // 将列表更新到redux
      const payload = this.validatorDispatchFnc ? this.validatorDispatchFnc(list) : list;
      dispatch({
        type: dispatchType,
        payload,
      });
    }
  };

  /**
   * 添加一行事件
   * @param {object} initRow  初始化数据
   * @param {array} dataList 当前列表所有数据
   */
  newAddRow = (initRow = null, dataList = []) => {
    const { editingKey, rowKey } = this.state;
    if (editingKey) {
      message.warn('已存在编辑行，关闭该编辑行后才可以添加');
      return;
    }
    const newData = dataList.map(item => ({ ...item }));
    const row = { ...initRow, add: true };
    row[rowKey] = uuid.v4();
    newData.push({ ...row });
    this.setState({ editingKey: row[rowKey] }, () => {
      this.updateData(newData);
    });
  };

  /**
   * 保存函数
   * @param {number} index  当前操作的行位置
   * @param {array} row 当前操作的行数据
   * @param {array} list 即将保存更新的所有列表数据
   */
  onSave = (index, row, list) => {
    this.setState({ editingKey: '' }, () => {
      this.updateData(list);
    });
  };

  /**
   * 取消函数
   * @param {array} list 即将取消更新的所有列表数据
   */
  onCanCel = list => {
    this.setState({ editingKey: '' });
    this.updateData(list);
  };

  /**
   * 删除函数
   * @param {string} key 即将删除的行标识key
   * @param {array} dataList 列表所有数据
   */
  onDelete = key => {
    const { nativeData, rowKey } = this.state;
    const list = nativeData.filter(v => v[`${rowKey}`] !== key);
    this.updateData(list);
  };

  /**
   * 列表编辑切换状态函数
   * @param {string} key 行标识key
   */
  editingKeyChange = key => {
    this.setState({ editingKey: key });
  };
}

export default EditableTableBasic;
