import React, { PureComponent, Fragment } from 'react';
import { Table, Alert, Pagination } from 'antd';
import { formatMessage } from 'umi/locale';
import { cloneDeep, compact } from 'lodash';
import styles from './index.less';

// 得到需要统计字段的total列表.预设0
function initBlueTotalList(columns) {
  const totalList = [];
  columns.forEach(column => {
    if (column.needTotal) {
      totalList.push({ ...column, total: 0 });
    }
  });
  return totalList;
}

// sortedInfo : {
//   column: {title: "采购数", dataIndex: "purchaseNum", needSum: true, align: "right", sorter: ƒ, …}
//   columnKey: "purchaseNum"
//   field: "purchaseNum"
//   order: "descend"
// }
// props

class TableAndPaging extends PureComponent {
  constructor(props) {
    super(props);
    const { columns } = props; // 外部传入的表格字段
    this.state = {
      selectedRowKeys: [], // 选中的row
      needTotalList: initBlueTotalList(columns), // 需要蓝色统计total的字段列表

      filteredInfo: null,
      sortedInfo: null,
    };
  }

  componentDidMount() {}

  // componentWillReceiveProps(newProps) {
  //   if(this.props.pagination !== newProps.pagination)
  //     console.log(this.props.pagination,newProps.pagination);
  // }

  // 得到表格最末下方总计行数据
  getSumRow = () => {
    const { dataSource, isTotalText, rowKey, columns } = this.props;
    const allColumns = columns; // 所有字段
    const needSumColumns = [];

    cloneDeep(allColumns).forEach(item => {
      if (item.needSum) needSumColumns.push({ ...item });
      if (item.children && Array.isArray(item.children) && item.children.length > 0) {
        item.children.forEach(child => {
          if (child.needSum) needSumColumns.push({ ...child });
        });
      }
    }); // 需要Count的字段

    if (!Array.isArray(dataSource) || dataSource.length <= 0) return null;
    if (needSumColumns.length <= 0) return null;
    const obj = {}; // 总计行

    const showTotalItem = cloneDeep(allColumns).filter(item => item.needSumText)[0];
    if (!showTotalItem) {
      const showTotal = cloneDeep(allColumns).map((itm, indx) => {
        if (itm.needSum) {
          return indx;
        }
        return '';
      });
      const showTotalDataIndex = allColumns[compact(showTotal)[0] - 1].dataIndex;
      obj[showTotalDataIndex] = isTotalText || formatMessage({ id: 'form.common.total' });
    } else {
      obj[showTotalItem.dataIndex] =
        showTotalItem.TotalText || isTotalText || formatMessage({ id: 'form.common.total' });
    }
    needSumColumns.forEach(item => {
      obj[item.dataIndex] = dataSource.reduce(
        (prev, next) => prev * 1 + (next[item.dataIndex] || 0) * 1,
        0
      ); // 采购数量
    });
    obj.isSumRow = true;
    obj.edit = 'no';
    obj.CustomTableRowNo = 'no';
    if (rowKey) {
      obj[rowKey] = '';
    } else {
      obj.id = '';
    }
    return obj;
  };

  /**
   * 清空选择
   */
  cleanSelectedKeys = () => {
    // 清空当前选择
    this.handleRowSelectChange([], []);
  };

  /**
   * 当某一行被选中后需要统计的字段自动加总
   * @param selectedRowKeys 选中行key
   * @param selectedRows 选中的多行资料
   */
  handleRowSelectChange = (selectedRowKeys, selectedRows) => {
    // 选中行
    let { needTotalList } = this.state;
    needTotalList = needTotalList.map(item => ({
      ...item,
      total: selectedRows.reduce((sum, val) => sum + parseFloat(val[item.dataIndex], 10), 0),
    }));
    const { onSelectRow } = this.props;
    if (onSelectRow) {
      onSelectRow(selectedRows);
    }

    this.setState({ selectedRowKeys, needTotalList });
  };

  /**
   * 当表格页码/pagesize/排序/过滤改变时.触发onChange事件
   * @param _pagination
   * @param filters
   * @param sorter
   */
  handleTableChange = (_pagination, filters, sorter) => {
    const { onChange, pagination } = this.props;
    // 点击页数
    if (onChange) {
      onChange(pagination, filters, sorter);
    }
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  handlePageNumChange = (current, pageSize) => {
    const { filteredInfo, sortedInfo } = this.state;
    const { pagination } = this.props;
    pagination.current = current;
    pagination.pageSize = pageSize;

    const { onChange } = this.props;
    if (onChange) {
      onChange(pagination, filteredInfo, sortedInfo);
    }
  };

  handlePageSizeChange = (current, pageSize) => {
    const { filteredInfo, sortedInfo } = this.state;
    const { pagination } = this.props;
    pagination.current = current;
    pagination.pageSize = pageSize;
    // 点击页数
    const { onChange } = this.props;
    if (onChange) {
      onChange(pagination, filteredInfo, sortedInfo);
    }
  };

  getData = () => {
    const {
      showSumRow, // 是否显示最下方统计行
      sumRowTop, // sumRow是否显示在上方
      dataSource,
    } = this.props;

    // 如果没有数据返回空
    if (!Array.isArray(dataSource) || dataSource.length <= 0) return [];

    // 如果取不到sumRow就不显示
    const sumRow = this.getSumRow();
    if (!sumRow) return dataSource;

    // 如果要显示sumRow
    if (showSumRow) {
      // 如果sumRow在头部
      if (sumRowTop) return [sumRow, ...dataSource];
      // 默认sumRow在底部
      return [...dataSource, sumRow];
    }
    // 不显示sumRow
    return dataSource;
  };

  render() {
    const {
      selectedRowKeys, // 选中表格行
      needTotalList, // 需要蓝色统计的列表
    } = this.state;

    const {
      showTotalList, // 是否显示蓝色total列
      sumRowTop, // sumRow是否显示在上方
      pagination,
      onSelectRow, // 选择行的回调
      columns,
      ...restProps
    } = this.props;

    // 如果有分页就加上必要的
    const paginationProps = pagination
      ? {
          showSizeChanger: true,
          showQuickJumper: true,
          ...pagination,
        }
      : false;

    // 如果有选择回调,就带上选择属性
    const rowSelection = onSelectRow
      ? {
          selectedRowKeys,
          onChange: this.handleRowSelectChange, // 需要拦截处理勾选时间以总计勾选值
          getCheckboxProps: record => ({
            // 总计行不显示checkbox
            disabled: record.disabled,
            style: record.CustomTableRowNo === 'no' ? { display: 'none' } : {},
          }),
        }
      : null;

    // 根据是否有下方总计列加上总计
    const newDataSource = this.getData();

    // 如果有排序.设置不排序统计行
    const newColumns = columns.map(item => {
      const newItem = { ...item };
      if (newItem.sorter && typeof newItem.sorter === 'function')
        newItem.sorter = (a, b) => (a.isSumRow || b.isSumRow ? sumRowTop : item.sorter(a, b));
      return newItem;
    });

    return (
      <Fragment>
        {/* 如果需要显示统计, 且需要统计的字段大于0 才显示 */}
        {showTotalList && needTotalList.length > 0 && (
          <div style={{ marginBottom: 10 }}>
            <Alert
              message={
                <Fragment>
                  {formatMessage({ id: 'form.common.selected' })}{' '}
                  <a style={{ fontWeight: 600 }}>{selectedRowKeys.length}</a> &nbsp;&nbsp;
                  {needTotalList.map(item => (
                    <span style={{ marginLeft: 8 }} key={item.dataIndex}>
                      {item.title}
                      {formatMessage({ id: 'form.common.total' })}&nbsp;
                      <span style={{ fontWeight: 600 }}>
                        {item.render ? item.render(item.total) : item.total}
                      </span>
                    </span>
                  ))}
                  <a onClick={this.cleanSelectedKeys} style={{ marginLeft: 24 }}>
                    {formatMessage({ id: 'form.common.clear' })}
                  </a>
                </Fragment>
              }
              type="info"
              showIcon
            />
          </div>
        )}

        <Table
          {...restProps}
          columns={newColumns}
          dataSource={newDataSource} // 可能会加总计列.,所以必须重写
          pagination={false} // 可能加了必须的翻页属性,所以必须重写
          rowSelection={rowSelection} // 可能带了选择事件,所以必须重写
          onChange={this.handleTableChange}
        />
        {paginationProps && (
          <Pagination
            {...paginationProps}
            showTotal={total =>
              total
                ? `${formatMessage({ id: 'form.common.tableTotal1' })}${total}${formatMessage({
                    id: 'form.common.tableTotal2',
                  })}`
                : ''
            }
            className={styles.custPaging}
            onChange={this.handlePageNumChange}
            onShowSizeChange={this.handlePageSizeChange}
            style={{ float: 'right', margin: '16px 0' }}
          />
        )}
      </Fragment>
    );
  }
}

export default TableAndPaging;
