import React, { PureComponent, Fragment } from 'react';
import { Button, Icon, Drawer, Checkbox, Divider, message } from 'antd';
import { formatMessage } from 'umi/locale';
import styles from './index.less';

const localStroagePrefix = 'CustomTable_'; // localstorage中的前缀.

class SettingDrawer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      DrawerVisible: false, // 侧边栏是否展开
      checkAll: false, // 全选
      indeterminate: '', // 全选icon
      checkboxList: [], // 选项列表
      checkedList: [], // 勾选的列表
    };
  }

  componentDidMount() {
    const { allColumns = [] } = this.props;

    // 获得checkbox列表列表
    const checkboxList = allColumns.map(item => ({ label: item.title, value: item.dataIndex }));
    // 获得储存的排除字段列表
    const savedRemovedColumns = this.getLocalStorage(); // 通过localstorage里读取出来需要去掉的字段
    // 得到勾选的列表
    const checkedList = [];
    allColumns.forEach(item => {
      if (!savedRemovedColumns.includes(item.dataIndex)) {
        checkedList.push(item.dataIndex);
      }
    });
    this.setState({
      checkboxList,
      checkedList,
    });
  }

  /**
   * 获取指定tabel被选中的checkList, 如果不存在就存次
   */
  getLocalStorage = () => {
    const { tabelId } = this.props;
    // 得到localstorage中要求被移除的字段
    if (JSON.parse(localStorage.getItem(localStroagePrefix + tabelId))) {
      return JSON.parse(localStorage.getItem(localStroagePrefix + tabelId));
    }
    return [];
  };

  /**
   * 显示右边的字段选择抽屉
   * */
  openDrawer = () => {
    // 打开弹框获取LocalStorage里checkedList
    const { checkedList, DrawerVisible, checkboxList } = this.state;
    this.setState({
      DrawerVisible: !DrawerVisible,
      checkedList,
      checkAll: this.getLocalStorage().length === 0, // 是否选中所有
      indeterminate: this.getLocalStorage().length > checkboxList.length,
    });
  };

  /**
   * 当右边抽屉中的checkbox选项被选中时,触发更改选中list事件
   * */
  checkOneChange = checkedList => {
    // 更新checkedList
    const { checkboxList } = this.state;
    this.setState({
      checkedList,
      indeterminate: !!checkedList.length && checkedList.length < checkboxList.length,
      checkAll: checkedList.length === checkboxList.length,
    });
  };

  /**
   * 当右边抽屉中的checkbox全选事件
   * */
  checkAllChange = e => {
    // 更新checkedList
    const { checkboxList } = this.state;
    const arr = [];
    checkboxList.forEach(item => arr.push(item.value));
    this.setState({
      checkedList: e.target.checked ? arr : [],
      indeterminate: false,
      checkAll: e.target.checked,
    });
  };

  /**
   * 储存右边选择的字段列表到localstorage里
   * */
  submitChange = () => {
    // 确认之后存checkedList
    const { tabelId, onChange } = this.props;
    const { checkedList, checkboxList } = this.state;
    const list = checkboxList.filter(item => checkedList.indexOf(item.value) === -1);
    const newList = [];
    list.forEach(item => newList.push(item.value));
    localStorage.setItem(localStroagePrefix + tabelId, JSON.stringify(newList));
    this.setState({ DrawerVisible: false });
    if (checkedList.length === 0) {
      message.info('请至少选择一项');
      this.openDrawer();
    }
    if (onChange) {
      onChange();
    }
  };

  render() {
    const { DrawerVisible, indeterminate, checkboxList, checkAll, checkedList } = this.state;
    const { tabelTitle } = this.props;

    return (
      <Fragment>
        {/* 显示右边抽屉的按钮 */}
        <Button
          htmlType="button"
          type="primary"
          style={{
            borderRadius: '4px 0 0 4px',
            zIndex: 1001,
            position: 'fixed',
            top: 300,
            right: DrawerVisible ? 256 : 0,
          }}
          onClick={() => {
            this.openDrawer();
          }}
        >
          <Icon type={DrawerVisible ? 'close' : 'setting'} />
        </Button>
        {/* 右边抽屉 */}
        <Drawer
          title={tabelTitle}
          placement="right"
          destroyOnClose
          visible={DrawerVisible}
          onClose={() => {
            this.setState({ DrawerVisible: false });
          }}
        >
          <Checkbox indeterminate={indeterminate} checked={checkAll} onChange={this.checkAllChange}>
            {formatMessage({ id: 'form.common.all' })}
          </Checkbox>
          <Divider style={{ margin: '10px 0' }} />
          <Checkbox.Group
            className={styles.drawerCard}
            options={checkboxList}
            value={checkedList}
            onChange={this.checkOneChange}
          />
          <footer style={{ marginTop: 20 }}>
            <Button
              htmlType="button"
              type="primary"
              style={{ marginRight: 20 }}
              onClick={this.submitChange}
            >
              {formatMessage({ id: 'form.common.sure' })}
            </Button>
            <Button
              htmlType="button"
              style={{ marginRight: 20 }}
              onClick={() => this.setState({ DrawerVisible: false })}
            >
              {formatMessage({ id: 'form.common.cancel' })}
            </Button>
          </footer>
        </Drawer>
      </Fragment>
    );
  }
}
export default SettingDrawer;
