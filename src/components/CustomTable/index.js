import React, { PureComponent, Fragment } from 'react';
import { formatMessage } from 'umi/locale';
import SettingDrawer from './SettingDrawer';
import TableAndPaging from './TableAndPaging';

const localStroagePrefix = 'CustomTable_'; // localstorage中的前缀.

/**
 * 自定义表格.功能如下
 * 1.可以自动编号
 * 2.可以自定义表格字段隐藏显示
 * 3.可以自动在下方总计行
 * 4.表格上方蓝色条显示勾选的统计信息
 *
 * // Ant-design 表格相关属性
 * columns        // [], 总共有哪些字段(见ant.design)
 * pagination     // {}或false, 默认不分页(见ant.design)
 * dataSource     // [], 实际表格数据(见ant.design)
 * rowKey         // string, 行key(见ant.design)
 * // 自定义功能相关
 * showCustom     // true/false, 是否可以自定义表格
 * tabelId        // string, 表格ID, 决定了你localstorage里的ID, 如果不带就不会触发自定义表格
 * tabelTitle     // string. 表格名称, 可以不填
 * showRowNo      // true/false, 是否显示行号
 * showSumRow     // true/false, 是否显示表格下方总计行
 * showTotalList  // true/false, 是否显示表格上方蓝色统计列,如果true,每次勾选会有蓝色统计.如果不需要统计可以设置为false隐藏
 * isTotalText    // string, 蓝色合计行文字,为空自带
 * // 回调事件
 * onSelectRow,   // function(), 选中行的回调function! 注意是function!
 * onChange,      // function(), 翻页控件的回调function! 注意是funcion!
 *
 * // columns字段属性
 * needTotal bool,需要表格上方蓝色统计的字段
 * needSum  bool,需要表格下方总计行的字段(原属性needCount)
 * needSumText bool,表格下方总计行的显示总计的字段(原属性needTotalText)
 *
 * // 旧版本废弃属性
 * hasBordered ,    // 显示边框(废弃.这个可以直接调用)
 * noPagination,    // 不显示分页(废弃.现在这个直接根据pagination判断)
 * noRowSelection,  // 不显示选择checkbox(废弃.现在通过onSelectRow属性判断)
 */
class CustomTable extends PureComponent {
  // 默认属性, 这里只列本控件所需的, 其他的可见antd的table组件
  static defaultProps = {
    // Ant-design 表格相关属性
    columns: [], // [], 总共有哪些字段(见ant.design)
    pagination: false, // 默认不分页
    dataSource: [], // [], 实际表格数据
    rowKey: 'id', // string, 行key
    // 自定义功能相关
    showCustom: false, // 是否可以自定义表格
    tabelId: '', // string, 表格ID, 决定了你localstorage里的ID, 如果不带就不会触发自定义表格
    tabelTitle: '', // string. 表格名称, 可以不填
    showRowNo: false, // 是否显示行号
    showSumRow: false, // 是否显示表格下方总计行
    sumRowTop: false, // 总计行是否在上方
    showTotalList: false, // 表格上方是否显示蓝色统计行
    isTotalText: '', // string, 蓝色合计行文字,为空自带
    // 回调事件
    onSelectRow: null, // function(), 选中行的回调function! 注意是function!
    onChange: null, // function(), 翻页控件的回调function! 注意是funcion!
  };

  constructor(props) {
    super(props);

    // 通过showRowNo确定字段里是否需要显示序号
    const { columns, showRowNo } = props;
    const allColumns = this.getAllColumns(columns, showRowNo);

    this.state = {
      allColumns, // 全部字段列表, 和props的columns区别是否加了序号
      filteredColumns: [], // 过滤后的字段列表
    };
  }

  componentDidMount() {
    this.getFilteredColumns();
  }

  /**
   * 根据LocalStorage checkList筛选columns
   */
  getFilteredColumns = () => {
    const { showCustom } = this.props; // 是否显示自定义表格
    const { allColumns } = this.state;
    const savedRemovedColumns = this.getLocalStorage(); // 通过localstorage里读取出来需要去掉的字段
    const filteredColumns = [...allColumns]; // 过滤后的字段列表

    // 如果自定义表格, 去除掉localstorage里存在的.
    if (showCustom) {
      allColumns.forEach(itm => {
        savedRemovedColumns.forEach(item => {
          if (itm.dataIndex === item) {
            filteredColumns.splice(filteredColumns.findIndex(value => value.dataIndex === item), 1);
          }
        });
      });
    }
    this.setState({
      filteredColumns,
    });
  };

  /**
   * 取得所有的字段列表. 如果有显示行号bool, 就会自动加上行号字段
   * @returns {*} 取得不经过过滤的所有字段列表.包含行号
   */
  getAllColumns = (columns, showRowNo) =>
    showRowNo
      ? [
          {
            title: formatMessage({ id: 'form.common.rowno' }),
            dataIndex: 'CustomTableRowNo',
            width: 60,
            align: 'left',
            // no 的是总计行 不显示行号
            render: (text, row, index) => <span>{row.isSumRow ? '' : index + 1}</span>,
          },
          ...columns,
        ]
      : columns;

  /**
   * 获取指定tabel被选中的checkList, 如果不存在就存次
   */
  getLocalStorage = () => {
    const { tabelId, showCustom } = this.props;
    // 如果不需要自定表格就直接返回所有字段
    if (!showCustom) return [];
    // 得到localstorage中要求被移除的字段
    if (JSON.parse(localStorage.getItem(localStroagePrefix + tabelId))) {
      return JSON.parse(localStorage.getItem(localStroagePrefix + tabelId));
    }
    return [];
  };

  render() {
    const {
      allColumns, // 所有字段
      filteredColumns, // 过滤后需要显示的字段列表
    } = this.state;
    const {
      tabelId, // 外部传入的表格ID
      tabelTitle, // (可选)外部传入的表格名字
      showCustom = true,
      ...restProps
    } = this.props;

    return (
      <Fragment>
        <TableAndPaging {...restProps} columns={filteredColumns} />

        {/* 是否显示自定义表格 */}
        {tabelId && showCustom && allColumns.length > 0 && (
          <SettingDrawer
            tabelTitle={tabelTitle}
            tabelId={tabelId}
            allColumns={allColumns}
            onChange={this.getFilteredColumns} // 修改后回调
          />
        )}
      </Fragment>
    );
  }
}

export default CustomTable;
