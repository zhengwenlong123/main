---
公共组件说明

---

### Charts
从ant-desgin-pro来的. 图形组件demo
(如不需要可以删除)

### DescriptionList
从ant-desgin-pro来的. 用于快速显示字段文字
(有用,勿删)

### EditableLinkGroup
从ant-desgin-pro来的. 首页demo用的, 现在已没有使用
(如不需要可以删除)

### PageHeaderWrapper
从ant-desgin-pro来的. 每个具体页面区域上方的菜单层级路径和名称
(不可删除)

### Result
从ant-desgin-pro来的. 典型结果页面
(不可删除)

### StandardTable
从ant-desgin-pro来的. 一个在基本表格封装了一层统计的提示.
