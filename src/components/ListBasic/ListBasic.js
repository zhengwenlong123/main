import React from 'react';
import PropTypes from 'prop-types';
import { Avatar, Divider, List } from 'antd';
import { formatMessage } from 'umi/locale';
import ModalBasic from '@/components/ModalBasic/ModalBasic';
import styles from './index.less';

/**
 * antd List基础配置抽取维护组件
 * 1.集成了Modal组件
 * 2.listType值仅为: 1为图片; 2为文件; 3为视频;
 */
const ListBasic = props => {
  const { listType, listDataSource, listRowSelectionEvent, listPagination, listHeaderCmp } = props;

  // List元数据渲染组件
  const renderListItemMetaCmp = item => (
    <List.Item.Meta
      className={styles.listPicCmpChildItem}
      avatar={
        <Avatar
          src={item.address}
          alt={formatMessage({ id: 'form.common.picture' })}
          shape="square"
        />
      }
      title={item.descName}
      description={item.descNotes}
    />
  );

  // List操作动作渲染组件
  const renderListActionCmp = item => (
    <div style={{ display: 'flex', width: '150px' }}>
      <div>
        <a type="primary" onClick={() => listRowSelectionEvent(listType, 10, item)}>
          {formatMessage({ id: 'button.common.add' })}
        </a>
        <Divider type="vertical" />
      </div>
      <div>
        <a type="primary" onClick={() => listRowSelectionEvent(listType, 2, item)}>
          {formatMessage({ id: 'button.common.edit' })}
        </a>
        <Divider type="vertical" />
      </div>
      <div>
        <a type="primary" onClick={() => listRowSelectionEvent(listType, 20, item)}>
          {formatMessage({ id: 'button.common.delete' })}
        </a>
      </div>
    </div>
  );

  // List每项item渲染组件
  const renderListItemCmp = item => {
    let cmp = null;
    switch (listType) {
      case 1: // 图片
        cmp = (
          <div className={styles.listItemCmp}>
            <div className={styles.listItemCmpChild}>
              {renderListItemMetaCmp(item)}
              <div className={styles.listPicCmpChildItem}>{item.descEnable}</div>
            </div>
          </div>
        );
        break;
      case 2: // 文件
        cmp = (
          <div className={styles.listItemCmp}>
            <div className={styles.listItemCmpChild}>
              <List.Item.Meta title={item.descName} description={item.address} />
              <div>{item.descEnable}</div>
            </div>
          </div>
        );
        break;
      case 3: // 视频
        cmp = (
          <div className={styles.listItemCmp}>
            <div className={styles.listItemCmpChild}>
              <List.Item.Meta title={item.descName} description={item.address} />
              <div>{item.descEnable}</div>
            </div>
          </div>
        );
        break;
      default:
        break;
    }
    return cmp;
  };

  return (
    <div>
      <List
        header={listHeaderCmp}
        itemLayout="horizontal" // horizontal or vertical
        dataSource={listDataSource}
        pagination={listPagination}
        size="small"
        renderItem={item => (
          <List.Item key={item.id} extra={renderListActionCmp(item)}>
            {renderListItemCmp(item)}
          </List.Item>
        )}
      />
      <ModalBasic {...props} />
    </div>
  );
};

/**
 * List组件抽象
 * listType 非空
 * listDataSource 非空
 */
ListBasic.propTypes = {
  listType: PropTypes.oneOf([1, 2, 3]).isRequired, // list类型,1为图片; 2为文件; 3为视频;
  listDataSource: PropTypes.array.isRequired, // list数据源
  listRowSelectionEvent: PropTypes.func.isRequired, // list行选择函数
  listHeaderCmp: PropTypes.element, // list头部展示组件
  listActions: PropTypes.arrayOf([PropTypes.element]), // list动作按钮组件
  listPagination: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]), // list分页配置
};

ListBasic.defaultProps = {
  listHeaderCmp: null,
  listActions: null,
  listPagination: false,
};

export default ListBasic;
