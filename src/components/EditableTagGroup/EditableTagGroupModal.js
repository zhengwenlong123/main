import React, { useState, useEffect } from 'react';
import { Tag, Tooltip, Icon, Modal, Form, Input } from 'antd';
import { formatMessage } from 'umi/locale';

function EditableTagGroupModal(props) {
  const {
    value: propsValue,
    onChange,
    form,
    modalTitle,
    filterClosable,
    tagColor,
    addDisabled,
    onAddable,
    onCloseAble,
  } = props;
  const { getFieldDecorator } = form;
  const [tags, setTags] = useState([]);
  const [visibleModal, setVisibleModal] = useState(false);

  // 监听并更新属性集变化
  useEffect(() => {
    setTags([...propsValue]);
  }, [propsValue]);

  const handleAdd = () => {
    const addAble = onAddable
      ? onAddable(flag => {
          // flag返回值是true，代表可以进行添加操作
          if (flag) setVisibleModal(true);
        })
      : true;
    if (!addAble) return;
    setVisibleModal(true);
  };

  const handleVisibleModal = flag => {
    if (flag) {
      form.validateFields((err, values) => {
        if (!err) {
          const next = tags.length > 0 ? tags[tags.length - 1].seq : 0;
          const list = [...propsValue, { ...values, seq: next + 1 }];
          setTags([...tags, { ...values, seq: next + 1 }]);
          onChange(list);
          setVisibleModal(false);
        }
      });
    } else {
      setVisibleModal(false);
    }
  };
  // 校验名称是否存在
  const compareName = (rule, value, callback) => {
    const findVal = tags.findIndex(v => v.name === value);
    if (value && findVal !== -1) {
      callback(formatMessage({ id: 'form.goodsApplyBillManage.message.skuAttrName' }));
    } else {
      callback();
    }
  };
  // 校验编码是否存在
  const compareCode = (rule, value, callback) => {
    const findVal = tags.findIndex(v => v.code === value);
    if (value && findVal !== -1) {
      callback(formatMessage({ id: 'form.goodsApplyBillManage.message.skuAttrCode' }));
    } else {
      callback();
    }
  };

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 5 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 },
    },
  };

  const close = tag => {
    const list = propsValue.filter(v => v.code !== tag.code);
    onChange(list);
  };
  const handleClose = tag => {
    const closeAble = onCloseAble
      ? onCloseAble(flag => {
          if (flag) close(tag);
        })
      : true;
    if (!closeAble) return;
    close(tag);
  };

  return (
    <div>
      {tags.map(tag => {
        const isLongTag = tag.name.length > 20;
        const tagElem = (
          <Tag
            key={tag.code}
            color={tagColor && tagColor(tag)}
            closable={filterClosable(tag)}
            onClose={() => handleClose(tag)}
          >
            {isLongTag ? `${tag.name.slice(0, 20)}...` : tag.name}({tag.code})
          </Tag>
        );
        return isLongTag ? (
          <Tooltip title={tag.name} key={tag.name}>
            {tagElem}
          </Tooltip>
        ) : (
          tagElem
        );
      })}
      {!addDisabled && (
        <Tag onClick={() => handleAdd()} style={{ background: '#fff', borderStyle: 'dashed' }}>
          <Icon type="plus" /> {formatMessage({ id: 'button.common.add' })}
        </Tag>
      )}
      {visibleModal && (
        <Modal
          title={modalTitle}
          width={400}
          visible={visibleModal}
          onOk={() => handleVisibleModal(true)}
          onCancel={() => handleVisibleModal(false)}
        >
          <Form {...formItemLayout}>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('name', {
                validateTrigger: ['onChange', 'onBlur'],
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                  { validator: compareName },
                ],
              })(<Input />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.codeVal' })}>
              {getFieldDecorator('code', {
                validateTrigger: ['onChange', 'onBlur'],
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.codeVal.placeholder' }),
                  },
                  { validator: compareCode },
                ],
              })(<Input />)}
            </Form.Item>
          </Form>
        </Modal>
      )}
    </div>
  );
}

export default Form.create()(EditableTagGroupModal);
