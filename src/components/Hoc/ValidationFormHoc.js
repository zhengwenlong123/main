import React from 'react';

/**
 * 验证antd的form表单必填写值是否填写完整
 * @param {object} WrappedComponent
 */
const ValidationFormHoc = WrappedComponent =>
  class ValidationForm extends React.PureComponent {
    /**
     * 验证form必填值是否填写完整
     */
    validationForm = () => {
      const {
        form: { validateFields },
      } = this.props;
      let status = false;
      if (!validateFields) return status;
      validateFields(errors => {
        status = !errors;
      });
      return status;
    };

    render() {
      return <WrappedComponent {...this.props} validationForm={this.validationForm} />;
    }
  };

export default ValidationFormHoc;
