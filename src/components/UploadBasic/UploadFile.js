import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, Upload } from 'antd';
import { formatMessage } from 'umi/locale';

/**
 * 上传文件-组件,回调当前上传的文件列表
 */
class UploadFile extends React.PureComponent {
  // state = {
  //   fileList: [], // 已经上传的文件列表
  // }

  /**
   * UploadFile组件上传属性
   */
  propsUploadFile = () => {
    // const { fileList } = this.state;
    const { uploadAccept, uploadFileCallback, uploadSign } = this.props;
    return {
      // 支持文件的上传类型
      accept: uploadAccept,
      // 已经上传的文件列表
      fileList: null,
      // 点击移除文件时的回调
      onRemove: file => {
        // this.setState(prevState => {
        //   const index = prevState.fileList.indexOf(file);
        //   const newFileList = prevState.fileList.slice();
        //   newFileList.splice(index,1);
        //   return {
        //     fileList: newFileList,
        //   }
        // });
        uploadFileCallback('remove', uploadSign, file);
        return true; // 点击移除文件时的回调，返回值为 false 时不移除
      },
      // 上传文件之前的钩子
      beforeUpload: file => {
        // let newFileList = [];
        // if(uploadMulti){ // 支持多文件上传
        //   newFileList = [...fl, file];
        // }else{ // 支持单文件上传,所以state当中fileList只保存1个file,直接存入新file
        //   newFileList = [file];
        // }
        // this.setState({fileList: newFileList});
        uploadFileCallback('add', uploadSign, file);
        return false; // 参数为上传的文件，若返回 false 则停止上传
      },
    };
  };

  render() {
    const { uploadLabel } = this.props;
    return (
      <Upload {...this.propsUploadFile()}>
        <Button>
          <Icon type="upload" />
          {uploadLabel}
        </Button>
      </Upload>
    );
  }
}

UploadFile.propTypes = {
  uploadSign: PropTypes.string, // 上传文件标记
  uploadAccept: PropTypes.string, // 上传文件类型,
  uploadLabel: PropTypes.string, // 上传按钮文字
  uploadFileCallback: PropTypes.func.isRequired, // 当前操作上传的文件回调函数
};

UploadFile.defaultProps = {
  uploadSign: 'file', // 上传文件标记
  uploadAccept: 'image/*,video/*,audio/*,.pdf,.doc,.docx,.xls,.xlsx', // 支持文件上传的类型
  uploadLabel: formatMessage({ id: 'form.common.attachment.pleaseSelect' }),
};

export default UploadFile;
