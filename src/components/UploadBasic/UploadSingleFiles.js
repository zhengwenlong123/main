/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-param-reassign */
import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Modal, Upload } from 'antd';
import { formatMessage } from 'umi/locale';
import { accessFileUrl } from '@/defaultSettings';
import styles from './index.less';

/**
 * 多上传文件-组件,回调当前上传的文件列表
 */
class UploadSingleFile extends React.PureComponent {
  state = {
    previewVisible: false, // 图片预览Modal层的显示/隐藏状态控制
    previewFile: '', // 等上传图片预览数据
    previewFileState: '', // 是否正在转换base64
    // previewMaskStatus: false, // 图片预览遮罩层状态,用于控制删除按钮display的状态
  };

  // 通过FileReader使文件转base64
  getFileBase64 = file =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });

  // 设置预览图片状态
  previewPic = (visible, index) => {
    const { uploadFileList } = this.props;
    this.setState({
      previewVisible: visible,
      previewFile: `${accessFileUrl}/${uploadFileList[index]}`,
    });
  };

  // file转换成base64,并设置state的previewFile值
  convertFile2Base64 = async file => {
    const { uploadFileTranscoded } = this.props;
    if (!file.url && !file.preview) {
      file.preview = await this.getFileBase64(file);
    }
    this.setState({ previewFileState: 0 }); // 清除上传状态否则循环上传
    uploadFileTranscoded(file, 1); // 上传文件
  };

  // 清除图片
  onDeleteFile = index => {
    const { uploadFileCallback, uploadSign, uploadFileTranscoded } = this.props;
    uploadFileTranscoded(index, 2);
    // uploadFileList.splice(index, 1)
    uploadFileCallback(uploadSign, '');
  };

  // 判断文件格式显示对应的标签
  judgeFileFormat = (previewFile, index) => {
    const { uploadAccept } = this.props;
    let htmlStr;
    if (uploadAccept === 'image/*') {
      htmlStr = (
        <img
          alt={formatMessage({ id: 'form.common.attachment.preview' })}
          src={previewFile}
          onClick={() => this.previewPic(true, index)}
        />
      );
    } else if (uploadAccept === 'text/html') {
      htmlStr = (
        <a href={previewFile} download className={styles.previewFile}>
          <Icon type="paper-clip" style={{ fontSize: 24 }} />
        </a>
      );
    } else {
      htmlStr = (
        <a href={previewFile} download className={styles.previewFile}>
          <Icon type="download" style={{ fontSize: 24 }} />
        </a>
      );
    }
    return htmlStr;
  };

  render() {
    const { previewVisible, previewFile, previewFileState } = this.state;
    const {
      uploadSign,
      uploadAccept,
      uploadFileCallback,
      uploadFile,
      uploadIsDisable,
      uploadFileList,
    } = this.props;

    if (uploadFile instanceof Object && previewFileState) {
      this.convertFile2Base64(uploadFile);
    }

    return (
      <div className={styles.uploadSingleFilesList}>
        {/* 上传多个文件后的显示的DOM元素 */}
        {uploadFileList.length !== '' &&
          uploadFileList.map((item, key) => (
            <div
              className={`${styles.uploadSingleFileBtn} ${styles.uploadSingFileDistance}`}
              key={item}
            >
              {this.judgeFileFormat(`${accessFileUrl}/${item}`, key)}
              {uploadIsDisable ? null : (
                <Icon
                  className={styles.deleteFile}
                  type="delete"
                  onClick={() => this.onDeleteFile(key)}
                />
              )}
            </div>
          ))}
        {/* 上传框 */}
        <Upload
          className={styles.uploadPic}
          disabled={uploadIsDisable}
          fileList={false}
          accept={uploadAccept}
          beforeUpload={file => {
            // this.setState({ previewFileState: 1 }); // 开启上传解析
            this.setState({
              previewFileState: uploadFileCallback(uploadSign, file) === false ? 0 : 1,
            });
            // uploadFileCallback(uploadSign, file); // 将文件列表返回
            return false; // 参数为上传的文件，若返回 false 则停止上传
          }}
        >
          {uploadFileList.length < 10 ? (
            <div className={`${styles.uploadSingleFileBtn} ${styles.uploadSingFileDistance}`}>
              <Icon type="plus" />
            </div>
          ) : null}
        </Upload>
        {/* 预览图片弹框 */}
        <Modal visible={previewVisible} footer={null} onCancel={() => this.previewPic(false)}>
          <img
            alt={formatMessage({ id: 'form.common.attachment.preview' })}
            style={{ width: '100%' }}
            src={previewFile}
          />
        </Modal>
      </div>
    );
  }
}

UploadSingleFile.propTypes = {
  uploadSign: PropTypes.string, // 上传文件标记
  uploadAccept: PropTypes.string, // 上传文件类型,
  uploadFile: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // 上传的文件对象
  uploadFileList: PropTypes.array, // 上传的文件地址列表
  uploadFileCallback: PropTypes.func.isRequired, // 当前操作上传的文件回调函数
  uploadIsDisable: PropTypes.bool, // 上传操作使能,默认为true, false表示只能看,不能选择文件和删除操作
};

UploadSingleFile.defaultProps = {
  uploadSign: 'file', // 上传文件标记
  uploadAccept: 'image/*,video/*,audio/*,text/html,.pdf,.doc,.docx,.xls,.xlsx', // 支持文件上传的类型
  uploadFile: '', // 上传的文件对象
  uploadFileList: '', // 上传的文件地址列表
  uploadIsDisable: false, // 上传操作禁用,默认为false, true表示启用,不能选择文件和删除操作,只能查看
};

export default UploadSingleFile;
