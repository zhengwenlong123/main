/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-param-reassign */
import React from 'react';
// import PropTypes from 'prop-types';
import { message } from 'antd';
// import { formatMessage } from 'umi/locale';
import request from '@/utils/request';
import { fileUrl, accessFileUrl } from '@/defaultSettings';
import UploadSingleFile from './UploadSingleFile';
// import styles from './index.less';

/**
 * 单上传文件-组件, 直接上传文件到数据库
 */
class UploadSingleFileDirect extends React.PureComponent {
  state = {
    uploadAccept: '',
  };

  componentDidMount() {
    const { value } = this.props;
    let uploadAccept = '';
    if (value && /\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/.test(value)) {
      uploadAccept = 'image/*';
    } else if (value && /\.(htm|html)$/.test(value)) {
      uploadAccept = 'text/html';
    } else {
      uploadAccept = '';
    }
    this.setState({
      uploadAccept,
    });
  }

  // 判断文件类型
  handleFileType = type => {
    let uploadAccept = '';
    if (type.indexOf('image/') !== -1) {
      uploadAccept = 'image/*';
    } else if (type.indexOf('text/html') !== -1) {
      uploadAccept = 'text/html';
    } else {
      uploadAccept = '';
    }
    this.setState({
      uploadAccept,
    });
  };

  /**
   * 文件上传UploadFile组件回调函数
   */
  uploadFileCallback = async (uploadSign, newFile) => {
    console.log('上传文件回调', uploadSign, newFile);
    const { onChange } = this.props;
    if (!newFile) {
      // 文件不存在，代表的是移除图片
      onChange('', uploadSign);
      return;
    }
    const isLt5M = newFile.size / 1024 / 1024 < 5;
    if (!isLt5M) {
      message.error('附件大小只支持5M以内!');
      return;
    }
    this.handleFileType(newFile.type || '');
    const formData = new FormData();
    formData.append('file', newFile);
    const singleFileDirect = await request.post(`${fileUrl}/bootstrap/fileobject/upload/single`, {
      data: formData,
    });
    console.log('singleFileDirect', singleFileDirect);
    if (singleFileDirect) {
      const { url } = singleFileDirect;
      // const val = url.replace(fileRelationPathReg, '');
      const fileUrlCurrent = url ? `${accessFileUrl}/${url}` : '';

      onChange(fileUrlCurrent, uploadSign);
    } else {
      message.error('附件上传失败');
    }
  };

  render() {
    const { uploadAccept } = this.state;
    const { value = '', ...other } = this.props;
    return (
      <div>
        <UploadSingleFile
          uploadAccept={uploadAccept}
          uploadFile={value}
          uploadFileCallback={this.uploadFileCallback}
          {...other}
          // uploadIsDisable={mut === 1}
        />
      </div>
    );
  }
}

export default UploadSingleFileDirect;
