/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-param-reassign */
import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Modal, Upload } from 'antd';
import { formatMessage } from 'umi/locale';
import styles from './index.less';

/**
 * 单上传文件-组件,回调当前上传的文件列表
 */
class UploadSingleFile extends React.PureComponent {
  state = {
    previewVisible: false, // 图片预览Modal层的显示/隐藏状态控制
    previewImage: '', // 等上传图片预览数据,base64格式
    // previewMaskStatus: false, // 图片预览遮罩层状态,用于控制删除按钮display的状态
  };

  // 通过FileReader使文件转base64
  getFileBase64 = file =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });

  // 设置预览图片状态
  previewPic = visible => {
    this.setState({ previewVisible: visible });
  };

  // file转换成base64,并设置state的previewImage值
  convertFile2Base64 = async file => {
    if (!file.url && !file.preview) {
      file.preview = await this.getFileBase64(file);
    }
    this.setState({ previewImage: file.url || file.preview });
  };

  // 清除图片
  onDeleteFile = () => {
    const { uploadFileCallback, uploadSign } = this.props;
    this.setState({ previewImage: '' });
    uploadFileCallback(uploadSign, '');
  };

  // 判断文件格式显示对应的标签
  judgeFileFormat = (previewImage, uploadFile) => {
    const { uploadAccept } = this.props;
    let htmlStr;
    if (uploadAccept === 'image/*') {
      htmlStr = (
        <img
          alt={formatMessage({ id: 'form.common.attachment.preview' })}
          src={previewImage || uploadFile}
          onClick={() => this.previewPic(true)}
        />
      );
    } else if (uploadAccept === 'text/html') {
      htmlStr = (
        <a href={previewImage || uploadFile} download className={styles.previewFile}>
          <Icon type="paper-clip" style={{ fontSize: 24 }} />
        </a>
      );
    } else {
      htmlStr = (
        <a href={previewImage || uploadFile} download className={styles.previewFile}>
          <Icon type="download" style={{ fontSize: 24 }} />
        </a>
      );
    }
    return htmlStr;
  };

  render() {
    const { previewVisible, previewImage } = this.state;
    const {
      uploadSign,
      uploadAccept,
      uploadFileCallback,
      uploadFile,
      uploadIsDisable,
      bodyStyle,
    } = this.props;

    if (uploadFile instanceof Object && !previewImage) {
      this.convertFile2Base64(uploadFile);
    }

    return (
      <div>
        <div className={styles.uploadSingleFileBtn} style={bodyStyle}>
          {uploadFile ? (
            <div>
              {this.judgeFileFormat(previewImage, uploadFile)}
              {uploadIsDisable ? null : (
                <Icon
                  className={styles.deletePic}
                  type="delete"
                  onClick={() => this.onDeleteFile()}
                />
              )}
            </div>
          ) : (
            <Upload
              className={styles.uploadPic}
              disabled={uploadIsDisable}
              fileList={null}
              accept={uploadAccept}
              beforeUpload={file => {
                uploadFileCallback(uploadSign, file);
                return false; // 参数为上传的文件，若返回 false 则停止上传
              }}
            >
              <Icon type="plus" />
            </Upload>
          )}
        </div>
        <Modal visible={previewVisible} footer={null} onCancel={() => this.previewPic(false)}>
          <img
            alt={formatMessage({ id: 'form.common.attachment.preview' })}
            style={{ width: '100%' }}
            src={previewImage || uploadFile}
          />
        </Modal>
      </div>
    );
  }
}

UploadSingleFile.propTypes = {
  uploadSign: PropTypes.string, // 上传文件标记
  uploadAccept: PropTypes.string, // 上传文件类型,
  uploadFile: PropTypes.oneOfType([PropTypes.object, PropTypes.string]), // 上传的文件对象
  uploadFileCallback: PropTypes.func.isRequired, // 当前操作上传的文件回调函数
  uploadIsDisable: PropTypes.bool, // 上传操作使能,默认为true, false表示只能看,不能选择文件和删除操作
  bodyStyle: PropTypes.object, // 上传文件容器的样式
};

UploadSingleFile.defaultProps = {
  uploadSign: 'file', // 上传文件标记
  uploadAccept: 'image/*,video/*,audio/*,text/html,.pdf,.doc,.docx,.xls,.xlsx', // 支持文件上传的类型
  uploadFile: '', // 上传的文件对象
  uploadIsDisable: false, // 上传操作禁用,默认为false, true表示启用,不能选择文件和删除操作,只能查看
  bodyStyle: {}, // 上传文件容器的样式
};

export default UploadSingleFile;
