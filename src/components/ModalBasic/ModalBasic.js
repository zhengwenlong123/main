import React from 'react';
import PropTypes from 'prop-types';
import { Modal } from 'antd';

/**
 * antd Modal基础配置抽取组件
 * 1.集成了title
 * 2.内容部分通过children传入
 */

const ModalBasic = props => {
  const {
    modalTitle,
    modalContentCmp,
    modalVisible,
    modalOnCancelEvent,
    modalWidth,
    modalBodyStyle,
    modalStyle,
    hasModal,
    hasMask,
  } = props;

  return hasModal ? (
    <div>
      <Modal
        title={modalTitle}
        visible={modalVisible}
        conten
        centered
        destroyOnClose
        footer={null}
        closable
        maskClosable={false}
        mask={hasMask}
        style={modalStyle}
        bodyStyle={modalBodyStyle}
        width={modalWidth}
        onCancel={modalOnCancelEvent}
      >
        {modalContentCmp}
      </Modal>
    </div>
  ) : null;
};

/**
 * 定义ModalBasic组件props属性数据类型格式
 */
ModalBasic.propTypes = {
  modalTitle: PropTypes.string, // 对话框标题
  modalContentCmp: PropTypes.element, // 对话框内容组件
  modalVisible: PropTypes.bool, // 对话框显示/关闭状态
  modalOnCancelEvent: PropTypes.func, // 对话框关闭事件函数
  modalWidth: PropTypes.oneOfType([PropTypes.string, PropTypes.number]), // 对话框宽度值
  modalBodyStyle: PropTypes.object, // 对话框自身样式
  modalStyle: PropTypes.object, // 对话框展示样式
  hasModal: PropTypes.bool, // 对话框是否存在
  hasMask: PropTypes.bool, // 对话框是否展示遮罩
};

/**
 * 定义ModalBasic组件props属性默认值
 */
ModalBasic.defaultProps = {
  modalTitle: null,
  modalContentCmp: null,
  modalVisible: false,
  modalOnCancelEvent: null,
  modalWidth: '80vw',
  modalBodyStyle: { maxHeight: '86vh', minHeight: '86vh', overflowY: 'auto' },
  modalStyle: {},
  hasModal: true,
  hasMask: true,
};

export default ModalBasic;
