/* eslint-disable no-nested-ternary */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'dva';
import { Divider, message } from 'antd';
import { formatMessage } from 'umi/locale';
import MdcManageBasic from '@/components/BlockBasic/MdcManageBasic';
import MdcManageBlock from '@/components/BlockBasic/MdcManageBlock';
import EnclosureTableItem from './EnclosureTableItem';

/**
 * 附件描述列表-维护组件
 */
class EnclosureTableBasic extends MdcManageBasic {
  constructor(props) {
    super(props);
    // state
    this.state = {
      ...this.state,
      // Table行勾选数据对象
      selectedRowKeys: [],
      tableRowSelectionEntity: null,
    };
    // Table-表格行勾选处理
    // this.tableRowSelection = {
    //   columnWidth: 30,
    //   columnTitle: '<>',
    //   selectedRowKeys: [],
    //   onChange: (selectedRowKeys, selectedRows) => {
    //     this.tableRowSelection.selectedRowKeys = selectedRowKeys;
    //     this.setState({ tableRowSelectionEntity: selectedRows[0] });
    //   }, // Table勾选回调事件
    // };
  }

  /**
   * 渲染组件-品牌描述(列表)
   */
  render() {
    const {
      modalVisible,
      tableCurrentPage, // 当前选中的页面
      modalUpkeepType: mut, // 当前组件操作的类型
      modalUpkeepEntity: mue, // 当前组件操作的实体对象
      selectedRowKeys,
    } = this.state;
    const {
      enclosureAssociatedObject, // 附件关联的对象(当前操作父级对象)
      enclosureTablePaginationOnChangeEventDispatchType, // 附件列表分页对应dispatch的type
      enclosureTablePaginationOnChangeEventCondition, // 附件列表分页附带的参数,是object类型
      enclosureTableTitle, // 附件列表标题
      enclosureTableColumnsField, // 附件列表字段定义
      enclosureTableDataSource, // 附件列表源数据
      enclosureTableTotalSize, // 附件列表总数据量
    } = this.props;

    return (
      <MdcManageBlock
        tableColumns={enclosureTableColumnsField}
        tableDataSource={enclosureTableDataSource}
        tableOperateAreaCmp={this._tableOperateAreaCmp()}
        tableRowSelection={{
          columnWidth: 30,
          columnTitle: '<>',
          selectedRowKeys,
          onSelect: (record, selected, selectedRows) => {
            if (selected) {
              this.setState({ tableRowSelectionEntity: record });
            } else if (selectedRows.length === 0) {
              this.setState({ tableRowSelectionEntity: null });
            }
          },
          onChange: selectedRowKeysArr => {
            this.setState({
              selectedRowKeys:
                selectedRowKeysArr.length > 1 ? [selectedRowKeysArr[1]] : selectedRowKeysArr,
            });
          }, // Table勾选回调事件
        }}
        tableTitle={enclosureTableTitle}
        tableCurrentPage={tableCurrentPage}
        tablePaginationOnChangeEvent={(page, pageSize) =>
          this.tablePaginationOnChangeEvent(
            page,
            pageSize,
            enclosureTablePaginationOnChangeEventDispatchType,
            enclosureTablePaginationOnChangeEventCondition || null // 分页附加条件
          )
        }
        tableTotalSize={enclosureTableTotalSize} // table 总共数据条数,这里随机写100,生产环境应按照实际查询结果值为准
        modalVisible={modalVisible}
        modalTitle={enclosureTableTitle}
        modalContentCmp={
          <EnclosureTableItem
            enclosureAssociatedObject={enclosureAssociatedObject}
            modalUpkeepType={mut}
            modalUpkeepEntity={mue}
            modalVisibleOnChange={this.modalVisibleOnChange}
            {...this.props}
            modalOnChangePageCurrent={this.changeCurrentPage}
          />
        }
        modalOnCancelEvent={() => this.modalVisibleOnChange(false)}
      />
    );
  }

  // Table-表格头部动作处理组件
  _tableOperateAreaCmp = () => {
    const { tableRowSelectionEntity: trse } = this.state;
    const { actionType } = this.props; // 附件列表-当前操作类型,来源于父级组件的定义值, 1为详情;2为修改;3为停用;10为新增;20为删除

    // 校验Table勾选tableRowSelectionEntity值是否存在
    const checkTableRowSelectionEntity = (flag = -1) => {
      if (trse == null || trse === undefined) {
        const tips =
          flag === 2
            ? { id: 'message.confirm.modify' }
            : flag === 20
            ? { id: 'message.confirm.delete' }
            : { id: 'message.confirm.handle' };
        message.warn(`${formatMessage({ id: 'message.confirm.prefix' })}[${formatMessage(tips)}]`);
        return;
      }
      if (flag === 1) {
        // 详情动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 2) {
        // 维护动作
        this.onClickActionExecuteEvent(flag, trse);
      }
      if (flag === 20) {
        // 删除动作
        this._deleteTableRowSelection(trse);
      }
    };

    return actionType === 1 ? (
      <a type="primary" onClick={() => checkTableRowSelectionEntity(1)}>
        {formatMessage({ id: 'button.common.details' })}
      </a>
    ) : (
      <div>
        <a type="primary" onClick={() => checkTableRowSelectionEntity(1)}>
          {formatMessage({ id: 'button.common.details' })}
        </a>
        <Divider type="vertical" />
        <a type="primary" onClick={() => this.onClickActionExecuteEvent(10)}>
          {formatMessage({ id: 'button.common.add' })}
        </a>
        <Divider type="vertical" />
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(2);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.modify' })}
        </a>
        <Divider type="vertical" />
        <a
          type="primary"
          onClick={() => {
            checkTableRowSelectionEntity(20);
            this.setState({ tableRowSelectionEntity: null, selectedRowKeys: [] });
          }}
        >
          {formatMessage({ id: 'button.common.delete' })}
        </a>
      </div>
    );
  };

  /**
   * Table-删除行勾选数据
   * @param {object} item 处理数据对象
   * 删除1条附件数据
   */
  _deleteTableRowSelection = item => {
    const {
      enclosureOnDeleteEventDispatchType, // 附件删除dispathc的type值
      dispatch,
    } = this.props;
    this.changeCurrentPage(1);
    // 删除-描述数据
    dispatch({
      type: enclosureOnDeleteEventDispatchType,
      payload: item,
      callback: this.onCallbackTips,
    });
  };
}

EnclosureTableBasic.propTypes = {
  actionType: PropTypes.number.isRequired, // 附件列表-当前操作类型,来源于父级组件的定义值, 1为详情;2为修改;3为停用;10为新增;20为删除
  enclosureOperateSign: PropTypes.oneOf(['BrandDesc', 'GoodsSpuDesc', 'GoodsSkuDesc']).isRequired, // 附件当前操作标识
  enclosureTableTitle: PropTypes.string.isRequired, // 附件列表标题
  enclosureAssociatedObject: PropTypes.object.isRequired, // 附件关联的对象(当前操作父级对象)
  enclosureTableColumnsField: PropTypes.array.isRequired, // 附件列表字段定义
  enclosureTablePaginationOnChangeEventDispatchType: PropTypes.string.isRequired, // 附件列表分页对应dispatch的type
  enclosureTablePaginationOnChangeEventCondition: PropTypes.object, // 附件列表分页附带的参数,是object类型
  enclosureTableDataSource: PropTypes.array.isRequired, // 附件列表源数据
  enclosureTableTotalSize: PropTypes.number.isRequired, // 附件列表总数据量
  enclosureOnDeleteEventDispatchType: PropTypes.string.isRequired, // 附件删除dispathc的type值
  enclosureOnAddEventDispatchType: PropTypes.string.isRequired, // 附件添加dispatch的type值
  enclosureOnUpdateEventDispatchType: PropTypes.string.isRequired, // 附件更新dispatch的type值
  enclosureDescTypeAll: PropTypes.array.isRequired, // 附件描述类型数据所有(对应附件的类型有哪些,提供下拉选择操作的基础数据)
};

export default connect()(EnclosureTableBasic);
