/* eslint-disable no-restricted-syntax */
import React from 'react';
import { connect } from 'dva';
import { Button, Checkbox, Form, Input, InputNumber, message, Select, Spin } from 'antd';
import { formatMessage } from 'umi/locale';
import ValidationFormHoc from '@/components/Hoc/ValidationFormHoc';
import UploadSingleFile from '@/components/UploadBasic/UploadSingleFiles';
import { enclosureUploadPromise } from '@/services/enclosuremanage';
import styles from './index.less';

/**
 * 附件描述列表项-维护组件
 */
@ValidationFormHoc
class EnclosureTableItem extends React.PureComponent {
  state = {
    formItemLayout: {
      labelCol: { xs: { span: 24 }, sm: { span: 4 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 20 } },
    },
    address: '', // 品牌描述文件,单张
    filesList: [], // 上传文件,多个
    uploadFileType: '*', // 品牌描述文件上传类型
    loading: false,
    confirmLoading: false, // 提交等待
  };

  /**
   * 初始化state中的文件相关参数值
   */
  componentDidMount() {
    const { modalUpkeepEntity: mue } = this.props;
    this.setState({
      filesList: mue && mue.address ? mue.address.split(',') : [],
      uploadFileType: mue && mue.descTypeCode ? this.changeUploadFileType(mue.descTypeCode) : '*',
    });
  }

  /**
   * 表单提交请求回调
   */
  onCallback = (type, result) => {
    const { modalVisibleOnChange } = this.props;
    if (result) {
      message.info(formatMessage({ id: 'message.common.optionSuccess' }));
      modalVisibleOnChange(false);
    } else {
      message.warn(formatMessage({ id: 'message.common.optionError' }));
    }
    this.setState({ confirmLoading: false });
  };

  /**
   * 表单提交点击事件
   */
  onClickSubmit = () => {
    const { filesList } = this.state; // 品牌描述文件,单张
    const {
      form: { getFieldsValue, setFieldsValue },
      modalOnChangePageCurrent,
      validationForm,
      modalUpkeepType: mut,
      modalUpkeepEntity: mue,
      enclosureOnAddEventDispatchType, // 附件添加dispatch的type值
      enclosureOnUpdateEventDispatchType, // 附件更新dispatch的type值
      dispatch,
    } = this.props;

    // 为form表单内地附件地址设置值
    setFieldsValue({ address: '' });

    const status = validationForm();
    if (!status) return;
    const values = getFieldsValue();
    values.descEnable = values.descEnable ? 1 : 0;
    values.address = filesList.join(',');
    if (filesList.length > 0) {
      // 已选择附件
    } else {
      message.error(formatMessage({ id: 'message.attachment.notSelect' }), 5);
      return;
    }
    this.setState({ confirmLoading: true });
    if (mut === 2) {
      // 维护
      values.id = mue.id;
      dispatch({
        type: enclosureOnUpdateEventDispatchType,
        payload: values,
        callback: this.onCallback,
      });
    }

    if (mut === 10) {
      // 新增
      dispatch({
        type: enclosureOnAddEventDispatchType,
        payload: values,
        callback: this.onCallback,
      });
    }

    // 提交数据
    // enclosureUploadPromise(address)
    //   .then(url => {
    //     values.address = url;
    //     if (mut === 2) {
    //       // 维护
    //       values.id = mue.id;
    //       dispatch({
    //         type: enclosureOnUpdateEventDispatchType,
    //         payload: values,
    //         callback: this.onCallback,
    //       });
    //     }

    //     if (mut === 10) {
    //       // 新增
    //       dispatch({
    //         type: enclosureOnAddEventDispatchType,
    //         payload: values,
    //         callback: this.onCallback,
    //       });
    //     }
    //   })
    //   .catch(() => {
    //     message.warn('附件处理失败');
    //   });
    if (modalOnChangePageCurrent) {
      modalOnChangePageCurrent(1);
    }
  };

  // 文件类型的长传字段处理
  changeUploadFileType = value => {
    let type;
    switch (value) {
      case 'MMAP':
      case 'THUMBNAIL':
      case 'ALBUM':
        type = 'image/*';
        break;
      case 'DETAIL':
        type = 'image/*';
        break;
      case 'VIDEO':
        type = 'video/*';
        break;
      case 'WEBPAGE':
        type = 'text/html';
        break;
      case 'DOCUMENT':
        type = '.pdf,.doc,.docx,.xls,.xlsx';
        break;
      case 'OTHER':
        type = '*';
        break;
      default:
        break;
    }
    return type;
  };

  // 更改描述类型后清空上传组件
  onChangeUploadFile = value => {
    this.setState({ filesList: [], uploadFileType: this.changeUploadFileType(value) });
  };

  /**
   * 文件上传UploadFile组件回调函数
   * 把数据暂存address然后传回去异步上传
   */
  uploadFileCallback = (uploadSign, newFile) => {
    const obj = {};
    // 验证文件格式是否有误
    const {
      form: { getFieldValue },
    } = this.props;
    const uploadFileType = getFieldValue('descTypeCode');
    const FileType = this.changeUploadFileType(uploadFileType);
    const documentType = [
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // xlsx
      'application/vnd.ms-excel', // xls
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document', // docx
      'application/msword', // doc
      'application/pdf', // pdf
    ];
    let flag = false;
    if (newFile === '') {
      // 文件设置为空
      flag = true;
    } else if (
      newFile.type.indexOf('image') >= 0 &&
      (FileType === 'image/*' || uploadFileType === 'DETAIL')
    ) {
      // 文件设置为图片
      flag = true;
    } else if (newFile.type.indexOf('video') >= 0 && uploadFileType === 'VIDEO') {
      // 文件设置为视频
      flag = true;
    } else if (newFile.type === 'text/html' && uploadFileType === 'WEBPAGE') {
      // 文件设置为网页
      flag = true;
    } else if (newFile.type.indexOf('application') >= 0 && uploadFileType === 'DOCUMENT') {
      // 文件设置为文档
      for (const item of documentType) {
        if (item === newFile.type) {
          flag = true;
          break;
        }
      }
    } else if (uploadFileType === 'OTHER' || uploadFileType === '') {
      flag = true;
    }
    if (flag) {
      obj[uploadSign] = newFile;
      this.setState(obj);
      return true;
    }
    if (newFile) {
      message.warn(formatMessage({ id: 'message.attachment.formatError' }));
    }
    return false;
  };

  /**
   * 文件转码成功后执行的上传函数uploads调用
   */
  executeUploadFile = (file, type) => {
    const { filesList } = this.state;
    const arr = filesList.slice();
    if (type === 1) {
      this.setState({ loading: true });
      // 提交文件 type是1的时候 file是文件
      enclosureUploadPromise(file)
        .then(url => {
          arr.push(url);
          this.setState({ filesList: arr });
          this.setState({ loading: false });
        })
        .catch(() => {
          message.warn(formatMessage({ id: 'message.attachment.handleError' }));
          this.setState({ loading: false });
        });
    } else if (type === 2) {
      // 删除文件 type是2的时候 file是索引
      arr.splice(file, 1);
      this.setState({ filesList: arr });
    }
  };

  render() {
    const {
      formItemLayout,
      address, // 附件描述文件,单张
      filesList, // 上传完成的文件列表
      uploadFileType, // 附件类型 单个
      loading,
      confirmLoading,
    } = this.state;
    const {
      modalVisibleOnChange,
      form: { getFieldDecorator },
      modalUpkeepType: mut,
      enclosureDescTypeAll: edta, // 附件类型数据(所有)
      enclosureOperateSign: eos, // 值限定为：'BrandDesc', 'GoodsSpuDesc', 'GoodsSkuDesc'
    } = this.props;

    return (
      <Spin tip={formatMessage({ id: 'message.attachment.uploading' })} spinning={loading}>
        <div>
          <Form {...formItemLayout}>
            {eos === 'BrandDesc' ? (
              <Form.Item
                label={formatMessage({ id: 'form.goodsBrandBasicUpkeep.brandCode' })}
                style={{ display: 'none' }}
              >
                {getFieldDecorator('brandCode', { rules: [{ required: true }] })(
                  <Input className={styles.inputTextStyle} type="hidden" disabled maxLength={50} />
                )}
              </Form.Item>
            ) : null}
            {eos === 'GoodsSpuDesc' || eos === 'GoodsSkuDesc' ? (
              <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.spuCode' })}>
                {getFieldDecorator('spuCode', { rules: [{ required: true }] })(
                  <Input className={styles.inputTextStyle} disabled maxLength={50} />
                )}
              </Form.Item>
            ) : null}
            {eos === 'GoodsSkuDesc' ? (
              <Form.Item label={formatMessage({ id: 'form.goodsSkuBasicUpkeep.skuCode' })}>
                {getFieldDecorator('skuCode', { rules: [{ required: true }] })(
                  <Input className={styles.inputTextStyle} disabled maxLength={50} />
                )}
              </Form.Item>
            ) : null}
            <Form.Item label={formatMessage({ id: 'form.common.descriptionType' })}>
              {getFieldDecorator('descTypeCode', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.descriptionType.placeholder' }),
                  },
                ],
              })(
                <Select disabled={mut === 1} onSelect={this.onChangeUploadFile}>
                  {edta.map(item => (
                    <Select.Option key={item.parmCode} value={item.parmCode}>
                      {item.parmValue}
                    </Select.Option>
                  ))}
                </Select>
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.attachment' })}>
              {getFieldDecorator('enclosure', { rules: [{ required: true, message: '' }] })(
                <UploadSingleFile
                  uploadFile={address}
                  uploadFileList={filesList}
                  uploadFileCallback={this.uploadFileCallback}
                  uploadFileTranscoded={this.executeUploadFile}
                  uploadAccept={uploadFileType}
                  uploadSign="address"
                  uploadIsDisable={mut === 1}
                />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.name' })}>
              {getFieldDecorator('descName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'form.common.name.placeholder' }),
                  },
                ],
              })(<Input disabled={mut === 1} maxLength={25} />)}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.rowno' })}>
              {getFieldDecorator('seq')(
                <InputNumber min={1} max={10000} defaultChecked={1} disabled={mut === 1} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.remark' })}>
              {getFieldDecorator('descNotes')(
                <Input.TextArea rows={5} disabled={mut === 1} maxLength={250} />
              )}
            </Form.Item>
            <Form.Item label={formatMessage({ id: 'form.common.enable' })}>
              {getFieldDecorator('descEnable', {
                valuePropName: 'checked',
                rules: [{ required: false }],
              })(
                <Checkbox disabled={mut === 1}>
                  {formatMessage({ id: 'form.common.enable.placeholder' })}
                </Checkbox>
              )}
            </Form.Item>
          </Form>
        </div>
        <div>
          {mut === 1 ? null : (
            <div className={styles.modalHandleBtnArea}>
              <Button type="default" onClick={() => modalVisibleOnChange(false)}>
                {formatMessage({ id: 'button.common.cancel' })}
              </Button>
              <Button type="primary" onClick={this.onClickSubmit} loading={confirmLoading}>
                {formatMessage({ id: 'button.common.submit' })}
              </Button>
            </div>
          )}
        </div>
      </Spin>
    );
  }
}

export default connect()(
  Form.create({
    mapPropsToFields(props) {
      const {
        modalUpkeepEntity: mue,
        enclosureAssociatedObject: eao, // 附件关联的对象(当前操作父级对象)
        enclosureOperateSign: eos, // 值限定为：'BrandDesc', 'GoodsSpuDesc', 'GoodsSkuDesc'
      } = props;

      const tempObj = {};
      if (eos === 'BrandDesc')
        tempObj.brandCode = Form.createFormField({
          value: mue ? mue.brandCode : eao.brandCode || '',
        });
      if (eos === 'GoodsSpuDesc' || eos === 'GoodsSkuDesc')
        tempObj.spuCode = Form.createFormField({ value: mue ? mue.spuCode : eao.spuCode || '' });
      if (eos === 'GoodsSkuDesc')
        tempObj.skuCode = Form.createFormField({ value: mue ? mue.skuCode : eao.skuCode || '' });

      return Object.assign(
        {
          descTypeCode: Form.createFormField({ value: mue ? mue.descTypeCode : '' }),
          descName: Form.createFormField({ value: mue ? mue.descName : '' }),
          descNotes: Form.createFormField({ value: mue ? mue.descNotes : '' }),
          seq: Form.createFormField({ value: mue ? mue.seq : 1 }),
          descEnable: Form.createFormField({ value: mue ? mue.descEnable === 1 : true }),
          enclosure: Form.createFormField({ value: 'enclosure' }),
        },
        tempObj
      );
    },
  })(EnclosureTableItem)
);
