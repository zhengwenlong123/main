import React from 'react';
import moment from 'moment';
import { DatePicker } from 'antd';
import PropTypes from 'prop-types';

/**
 * 规范日期传值组件，传入和传出一致
 */
class DatePickerStandard extends React.Component {
  handleChange = (date, dateString) => {
    const { onChange } = this.props;
    onChange(dateString);
  };

  conversionObject = () => {
    const { value, formateVal } = this.props;
    return {
      dateVal: value ? moment(value, formateVal) : null,
    };
  };

  render() {
    const { dateVal } = this.conversionObject();
    return (
      <div>
        <DatePicker value={dateVal} onChange={this.handleChange} />
      </div>
    );
  }
}
// const DatePickerStandard = (props) => {
//   const { value, onChange, formateVal } = props;

//   const handleChange = (date) => {
//     onChange(moment(date).format(formateVal));
//   }

//   return (
//     <DatePicker
//       value={value ? moment(value, formateVal) : null}
//       onChange={handleChange}
//     />
//   );
// }

DatePickerStandard.propTypes = {
  value: PropTypes.string, // 时间字符串
  formateVal: PropTypes.string, // 时间格式 '2019-10-01'
  // onChange: PropTypes.func.isRequired, // 时间变化回调函数
};

DatePickerStandard.defaultProps = {
  value: null, // 时间字符串
  formateVal: 'YYYY-MM-DD', // 支持文件上传的类型
};

export default DatePickerStandard;
