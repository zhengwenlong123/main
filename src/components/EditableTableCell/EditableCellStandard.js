import React from 'react';
import { Table } from 'antd';
import { EditableFormRow, EditableCell } from './EditableForm';

class EditableCellStandard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSave = (row, error, currentError) => {
    const { dataSource, onSave, rowKey = 'key' } = this.props;
    const newData = [...dataSource];
    const index = newData.findIndex(item => row[rowKey] === item[rowKey]);
    const item = newData[index];
    newData.splice(index, 1, {
      ...item,
      ...row,
    });
    onSave(newData, error, currentError);
  };

  render() {
    const { columns, dataSource, extra, loading = false, ...other } = this.props;
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };
    const currentColumns = columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          editable: col.editable,
          dataIndex: col.dataIndex,
          title: col.title,
          rules: col.rules || [],
          formItem: col.formItem,
          handleSave: this.handleSave,
        }),
      };
    });
    return (
      <div>
        <div style={{ marginBottom: 16, width: '100%' }}>{extra}</div>
        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          loading={loading}
          dataSource={dataSource}
          columns={currentColumns}
          {...other}
        />
      </div>
    );
  }
}

export default EditableCellStandard;
