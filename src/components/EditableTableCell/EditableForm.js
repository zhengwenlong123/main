import React from 'react';
import { Form } from 'antd';

const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
  state = {
    // editing: false,
  };

  componentDidMount() {
    if (!this.form) return;
    this.form.validateFields(() => {});
  }

  toggleEdit = () => {
    const { editing } = this.state;
    this.setState({ editing: !editing }, () => {
      if (!editing) {
        this.input.focus();
      }
    });
  };

  save = e => {
    const { record, handleSave } = this.props;
    this.form.validateFields((error, values) => {
      handleSave({ ...record, ...values }, error, e);
    });
  };

  // formRender = () => {
  //   const { iptType } = this.props;
  //   if (iptType === 'InputNumber') {
  //     return (
  //       <InputNumber
  //         min={0}
  //         precision={0}
  //         ref={node => {
  //           this.input = node;
  //         }}
  //         onPressEnter={this.save}
  //         onBlur={this.save}
  //       />
  //     );
  //   }
  //   return (
  //     <Input
  //       ref={node => {
  //         this.input = node;
  //       }}
  //       onPressEnter={this.save}
  //       onBlur={this.save}
  //     />
  //   );
  // };

  renderCell = form => {
    this.form = form;
    const { dataIndex, record, rules = null, formItem } = this.props;
    return (
      <Form.Item style={{ margin: 0 }}>
        {form.getFieldDecorator(dataIndex, {
          rules,
          initialValue: record[dataIndex],
        })(
          formItem &&
            formItem(
              node => {
                this.input = node;
              }, // 返回dom节点
              e => {
                this.save(e);
              } // 返回事件对象
            )
        )}
      </Form.Item>
    );
  };

  render() {
    const {
      editable,
      dataIndex,
      title,
      record,
      index,
      handleSave,
      children,
      rules,
      formItem,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editable ? (
          <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
        ) : (
          children
        )}
      </td>
    );
  }
}

export { EditableFormRow, EditableCell };
