import React from 'react';
import PropTypes from 'prop-types';
import { Pagination } from 'antd';
import styles from './index.less';

/**
 * 为Table 添加分页组件
 * 注：当只有一页时隐藏分页器
 */
const TableFooter = props => {
  const {
    tableTotalSize,
    tablePaginationOnChangeEvent,
    hasTableFooter,
    tableCurrentPage,
    tablePageSize = 10,
  } = props;
  return hasTableFooter ? (
    <div className={styles.footer}>
      <Pagination
        current={tableCurrentPage}
        defaultPageSize={10}
        pageSize={tablePageSize}
        hideOnSinglePage
        total={tableTotalSize}
        onChange={tablePaginationOnChangeEvent}
      />
    </div>
  ) : null;
};

/**
 * 定义TableFooter组件props属性数据类型格式
 */
TableFooter.propTypes = {
  tablePaginationOnChangeEvent: PropTypes.func, // 表格分页onChange事件函数
  tableTotalSize: PropTypes.number, // 表格数据总数
  hasTableFooter: PropTypes.bool, // 表格尾部是否存在
  tableCurrentPage: PropTypes.number, // 表格当前页数
};

/**
 * 定义TableFooter组件props属性默认值
 */
TableFooter.defaultProps = {
  tablePaginationOnChangeEvent: null,
  tableTotalSize: null,
  hasTableFooter: true,
  tableCurrentPage: null,
};

export default TableFooter;
