import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';
import TableHeader from './TableHeader';
import TableFooter from './TableFooter';

/**
 * 基于antd Table组件进行扩展的表格区块组件
 * 1.扩展Table头部区块组件,标题，操作按钮
 * 2.扩展Table尾部区块组件
 * 3.扩展Table不带勾选功能
 */
const TableBlock = props => {
  const {
    tableDataSource,
    tableColumns,
    tableRowSelection,
    tableLoading,
    hideHeader,
    onRow,
    rowClassName,
  } = props;

  // 暂未使用 Table 组件属性中的pagination参数
  // const {tableTotal, onChangeTablePage} = props;
  // const pagination = {
  //   position: 'bottom',
  //   defaultPageSize: 5,
  //   pageSize: 5, // 每页条数
  //   hideOnSinglePage: true, // 只有一页时是否隐藏分页器
  //   total: tableTotal, // 数据总数
  //   onChange: onChangeTablePage, // 页码改变的回调，参数是改变后的页码及每页条数
  // }

  return (
    <div>
      {!hideHeader && <TableHeader {...props} />}
      <Table
        rowKey={record => record.id}
        rowSelection={tableRowSelection}
        dataSource={tableDataSource}
        columns={tableColumns}
        loading={tableLoading}
        pagination={false}
        bordered
        size="small"
        rowClassName={rowClassName}
        onRow={onRow}
      />
      <TableFooter {...props} />
    </div>
  );
};

/**
 * 定义TableBlock组件props属性数据类型格式
 */
TableBlock.propTypes = {
  tableColumns: PropTypes.array.isRequired, // 表格列表字段
  tableDataSource: PropTypes.array.isRequired, // 表格数据源
  tableRowSelection: PropTypes.object, // 表格列表勾选处理对象
  tableLoading: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]), // 表格列表加载状态
  hideHeader: PropTypes.bool, // 隐藏表头
  rowClassName: PropTypes.oneOfType([PropTypes.func, PropTypes.string]), // 表格行的类名
  onRow: PropTypes.func, // 行属性
};

/**
 * 定义TableBlock组件props属性默认值
 */
TableBlock.defaultProps = {
  tableRowSelection: null,
  tableLoading: false,
  hideHeader: false,
  rowClassName: '',
  onRow: null,
};

export default TableBlock;
