import React from 'react';
import PropTypes from 'prop-types';
import styles from './index.less';

/**
 * 为Table添加头部视图
 */
const TableHeader = props => {
  const { tableTitle, tableOperateAreaCmp, hasTableHeader } = props;
  let titleCmp = null;
  if (typeof tableTitle === 'string') {
    titleCmp = <span className={styles.blockTitle}>{tableTitle}</span>; // 当字符串使用
  }
  if (typeof tableTitle === 'object') {
    titleCmp = tableTitle; // 当组件使用
  }
  return hasTableHeader ? (
    <div className={`${styles.header} table-header`}>
      <div>{titleCmp}</div>
      <div>{tableOperateAreaCmp}</div>
    </div>
  ) : null;
};

/**
 * 定义TableHeader组件props属性数据类型格式
 */
TableHeader.propTypes = {
  tableTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.element]), // 表格标题
  tableOperateAreaCmp: PropTypes.element, // 表格操作区域组件
  hasTableHeader: PropTypes.bool, // 表格头部是否存在
};

/**
 * 定义TableHeader组件props属性默认值
 */
TableHeader.defaultProps = {
  tableTitle: null,
  tableOperateAreaCmp: null,
  hasTableHeader: true,
};

export default TableHeader;
