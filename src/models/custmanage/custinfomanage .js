/**
 * [企业信息管理] model层
 */

import {
  getCustInfoList, // 获取-客户信息列表
  getCustInfoById, // 获取-客户信息详情
  updateCustInfoRfStatus, // 更新-客户状态
  addCustOuId, // 新增-客户实体ID
  updateCustOuId, // 更新-客户实体ID
} from '@/services/custmanage/custinfomanage';
import { requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'custinfomanage',
  state: {
    //
    custInfoDetail: {}, // 客户详情信息
    // 客户相关
    custInfoList: [], // 客户申请列表单数据(所有)
    custInfoListPageSize: 0, // 企业信息列表总数量
  },
  reducers: {
    // 更新-客户数据(所有)
    changeCustInfoListAll(state, { payload }) {
      return {
        ...state,
        custInfoList: payload.content || [],
        custInfoListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-客户详情信息
    changeCustInfo(state, { payload }) {
      return {
        ...state,
        custInfoDetail: payload || {},
      };
    },
  },
  effects: {
    // 获取-客户列表(列表/分页)
    *paginationCustInfoList({ payload }, { call, put }) {
      const res = yield call(
        getCustInfoList,
        {
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        }
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustInfoListAll', payload: result });
    },
    // 获取-客户信息详情
    *getCustApplyInfoById({ payload, callback = null }, { call, put }) {
      const res = yield call(getCustInfoById, payload);
      const result = requestResult(res);
      if (callback) callback(result);
      yield put({ type: 'changeCustInfo', payload: result });
    },
    // 更新-客户信息状态
    *updateCustApplyInfoRfStatus({ payload, callback }, { call }) {
      const res = yield call(updateCustInfoRfStatus, payload);
      if (callback) callback(res);
    },
    // 新增-客户实体ID
    *addCustApplyInfOuId({ payload, callback }, { call }) {
      const res = yield call(addCustOuId, payload);
      if (callback) callback(res);
    },
    // 更新-客户实体ID
    *updateCustApplyInfOuId({ payload, callback }, { call }) {
      const res = yield call(updateCustOuId, payload);
      if (callback) callback(res);
    },
  },
};
