/**
 * [企业信息管理] model层
 */

import {
  getCustApplyList, // 获取-客户申请单信息列表
  getCustApplyInfoByRfId, // 获取-客户申请单信息详情
  addCustApplyInfo, // 新增-客户申请单信息
  updateCustApplyInfo, // 更新-客户申请单信息
  deleteCustApplyInfo, // 删除-客户申请单信息
  updateCustApplyInfoRfStatus, // 更新-客户申请单状态
  auditCustApplyInfo, // 审核-客户申请单信息
  getCustApplyAuditList, // 获取-客户申请单审核信息列表
  getCustApplyAuditLogs, // 获取-某个客户申请单审核历史列表
  getTianYCInfoByCompanyCode, // 获取-通过社会信用码查询客户申请单天眼查信息
  getTianYCAbnormalList, // 获取-根据企业id条件查询经营异常列表
  getTianYCRisk, // 获取-根据企业id条件查询企业风险
} from '@/services/custmanage/custapplymanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';
import { isEmptyObj } from '@/utils/utils';

export default {
  namespace: 'custapplymanage',
  state: {
    // CustApplyDetail组件中的客户申请单详情，只用于组件内部
    custApplyInfo: {
      // 客户申请单详情信息
      custRequestFormInfoAddDto: {},
      custRfCompRelatedCompAddDto: {
        custRfCompRelatedCompDescAddDtoList: [],
      },
      custRfCompanyInfoAddDto: {
        isCompanyCertMerge: 1,
        hasCompanyCode: 1,
        custRfCompanyDescAddDtoList: [],
      },
    },
    // 客户申请单相关
    custApplyList: [], // 客户申请列表单数据(所有)
    custApplyListPageSize: 0, // 企业信息列表总数量
    singleCustApplyInfo: {}, // 单个申请单信息
    tianYCInfo: {}, // 企业天眼查信息
    tianYCAbnormalList: [], // 企业天眼查经营异常列表
    tianYCAbnormalListPageSize: 0, // 企业天眼查经营异常列表总数量
    tianYCRisk: [], // 企业天眼查企业风险
    // 客户申请单审核相关
    selectedApply: '', // 选择的申请单信息，主要包含rfid和tid
    custApplyAuditList: [], // 客户申请单审核列表数据(所有)
    custApplyAuditListPageSize: 0, // 客户申请单审核列表总数量
    custApplyAuditLogs: [], // 某个客户申请单审核历史列表
  },
  reducers: {
    // 更新-客户申请单数据(所有)
    changeCustApplyListAll(state, { payload }) {
      return {
        ...state,
        custApplyList: payload.content || [],
        custApplyListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-客户申请单详情信息
    changeCustApplyInfo(state, { payload }) {
      return {
        ...state,
        custApplyInfo: payload || {},
      };
    },
    // 更新-客户天眼查详情信息
    changeTianYCInfo(state, { payload }) {
      return {
        ...state,
        tianYCInfo: payload || {},
      };
    },
    // 更新-企业天眼查经营异常列表
    changeTianYCAbnormalList(state, { payload }) {
      return {
        ...state,
        tianYCAbnormalList: payload.content || [],
        tianYCAbnormalListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-客户天眼查企业风险
    changeTianYCRisk(state, { payload }) {
      return {
        ...state,
        tianYCRisk: payload || [],
      };
    },
    // 更新-客户申请单审核选择的申请单rfid
    changeAuditSelectedApply(state, { payload }) {
      return {
        ...state,
        selectedApply: payload || {},
      };
    },
    // 更新-客户申请单审核列表数据(所有)
    changeCustApplyAuditListAll(state, { payload }) {
      return {
        ...state,
        custApplyAuditList: payload.content || [],
        custApplyAuditListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-客户申请单审核历史列表数据(所有)
    changeCustApplyAuditLogs(state, { payload }) {
      return {
        ...state,
        custApplyAuditLogs: payload.content || [],
      };
    },
  },
  effects: {
    // 获取-客户申请单列表(列表/分页)
    *paginationCustApplyList({ payload }, { call, put }) {
      const res = yield call(
        getCustApplyList,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustApplyListAll', payload: result });
    },
    // 新增-客户申请单列表
    *addCustApplyInfo({ payload, callback }, { call }) {
      const res = yield call(addCustApplyInfo, payload);
      callback(10, res);
    },
    // 获取-客户申请单信息详情
    *getCustApplyInfoByRfId({ payload, callback = null }, { call, put }) {
      const res = yield call(getCustApplyInfoByRfId, payload);
      if (callback) callback(1, res);
      const result = requestResult(res);
      // 格式化数据
      const addRes = {
        custRequestFormInfoAddDto: result.custRequestFormInfoDto,
        custRfCompRelatedCompAddDto: {
          ...result.custRfCompRelatedCompDto,
          custRfCompRelatedCompDescAddDtoList: result.custRfCompRelatedCompDto
            ? result.custRfCompRelatedCompDto.custRfCompRelatedCompDescDtoList
            : [],
        },
        custRfCompanyInfoAddDto: {
          ...result.custRfCompanyInfoDto,
          custRfCompanyDescAddDtoList: result.custRfCompanyInfoDto.custRfCompanyDescDtoList,
        },
      };
      // 删除dto
      if (
        addRes.custRfCompRelatedCompAddDto &&
        addRes.custRfCompRelatedCompAddDto.custRfCompRelatedCompDescDtoList
      ) {
        delete addRes.custRfCompRelatedCompAddDto.custRfCompRelatedCompDescDtoList;
      }
      if (addRes.custRfCompanyInfoAddDto.custRfCompanyDescDtoList) {
        delete addRes.custRfCompanyInfoAddDto.custRfCompanyDescDtoList;
      }

      yield put({ type: 'changeCustApplyInfo', payload: addRes });
    },
    // 删除-经营单位类型定义
    *deleteCustApplyInfo({ payload, callback }, { call }) {
      const res = yield call(deleteCustApplyInfo, payload);
      if (callback) callback(res);
    },
    // 更新-客户申请单信息
    *updateCustApplyInfo({ payload, callback }, { call }) {
      const res = yield call(updateCustApplyInfo, payload);
      callback(2, res);
    },
    // 更新-客户申请单信息状态
    *updateCustApplyInfoRfStatus({ payload, callback }, { call }) {
      const res = yield call(updateCustApplyInfoRfStatus, payload);
      if (callback) callback(res);
    },
    // 获取-根据客户申请单的社会统一信用码获取天眼查信息
    *getTianYCInfoByCompanyCode({ payload, callback }, { call, put }) {
      const res = yield call(getTianYCInfoByCompanyCode, payload);
      const result = requestResult(res);
      if (callback) callback(result);
      yield put({ type: 'changeTianYCInfo', payload: result });
    },
    // 获取-企业天眼查经营异常列表
    *getTianYCAbnormalListById({ payload, callback }, { call, put }) {
      const res = yield call(getTianYCAbnormalList, payload);
      let result = requestResult(res);
      if (callback) callback(result);
      if (isEmptyObj(result)) {
        result = { content: [] };
      }
      yield put({ type: 'changeTianYCAbnormalList', payload: result });
    },
    // 获取-客户天眼查企业风险
    *getTianYCRiskById({ payload, callback }, { call, put }) {
      const res = yield call(getTianYCRisk, payload);
      const result = requestResult(res);
      if (callback) callback(result);
      yield put({ type: 'changeTianYCRisk', payload: result });
    },
    /**
     * 申请单审核相关
     */

    // 新增申请单审核记录
    *addCustApplyAudit({ payload, callback }, { call }) {
      const res = yield call(auditCustApplyInfo, payload);
      if (callback) callback(res);
    },
    // 获取-客户申请单审核列表(列表/分页)
    *paginationCustApplyAuditList({ payload, callback }, { call, put }) {
      const res = yield call(getCustApplyAuditList, {
        limit: payload.pageSize,
        page: payload.page - 1,
        searchCondition: payload.searchCondition,
        sortCondition: '[{"property":"createAt","direction":"DESC"}]',
      });
      const result = requestResult(res);
      if (callback) callback(result);
      yield put({ type: 'changeCustApplyAuditListAll', payload: result });
    },
    // 获取-根据客户申请单的rfid获取审核记录列表(列表)
    *getCustApplyAuditLogs({ payload }, { call, put }) {
      const res = yield call(getCustApplyAuditLogs, {
        limit: 100,
        page: 0,
        searchCondition: payload.searchCondition,
        sortCondition: '[{"property":"createAt","direction":"DESC"}]',
      });
      const result = requestResult(res);
      yield put({ type: 'changeCustApplyAuditLogs', payload: result });
    },
  },
};
