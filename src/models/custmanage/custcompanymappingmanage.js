/**
 * [企业编码对照管理] model层
 */

import {
  getCustCompanyMapping,
  addCustCompanyMapping,
  updateCustCompanyMapping,
  updateCustCompanyMappingEnable,
  paginationCustCompanyMapping,
} from '@/services/custmanage/custcompanymappingmanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'custcompanymappingmanage',
  state: {
    custCompanyMappingList: [], // 企业编码对照列表
    custCompanyMappingListPageSize: 0, // 企业编码对照列表总数量
  },
  reducers: {
    // 更新-企业编码对照(列表)
    changeCustCompanyMappingList(state, { payload }) {
      return {
        ...state,
        custCompanyMappingList: payload.content || [],
        custCompanyMappingListPageSize: Number(payload.total || 0),
      };
    },
  },
  effects: {
    // 获取-企业编码对照(列表)
    *getCustCompanyMapping(_, { call, put }) {
      const res = yield call(
        getCustCompanyMapping,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: {},
          sortCondition: '[{"property":"create_at", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustCompanyMappingList', payload: result });
    },
    // 新增-企业编码对照
    *addCustCompanyMapping({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addCustCompanyMapping, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyMapping',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-企业编码对照
    *updateCustCompanyMapping({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateCustCompanyMapping, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyMapping',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-企业编码对照状态(使用/停用)
    *updateCustCompanyMappingEnable({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateCustCompanyMappingEnable, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyMapping',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 分页-企业编码对照(表格)
    *paginationCustCompanyMapping({ payload }, { call, put }) {
      const res = yield call(
        paginationCustCompanyMapping,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"create_at", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustCompanyMappingList', payload: result });
    },
  },
};
