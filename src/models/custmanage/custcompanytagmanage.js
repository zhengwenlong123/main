/**
 * [企业标签管理] model层
 */

import {
  // 企业标签类型
  getCustCompanyTagType,
  addCustCompanyTagType,
  updateCustCompanyTagType,
  updateCustCompanyTagTypeEnable,
  deleteCustCompanyTagType,
  paginationCustCompanyTagType,

  // 企业标签值定义
  getCustCompanyTagDefineAll,
  addCustCompanyTagDefine,
  updateCustCompanyTagDefine,
  updateCustCompanyTagDefineEnable,
  deleteCustCompanyTagDefine,
} from '@/services/custmanage/custcompanytagmanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'custcompanytagmanage',
  state: {
    custCompanyTagTypeList: [], // 企业标签类型列表
    custCompanyTagTypeListPageSize: 0, // 企业标签类型列表总数量
    currentSelectCustCompanyTagDefine: [], // 当前选择-企业标签值定义数据(所有)
  },
  reducers: {
    // 更新-企业标签类型(列表)
    changeCustCompanyTagTypeList(state, { payload }) {
      return {
        ...state,
        custCompanyTagTypeList: payload.content || [],
        custCompanyTagTypeListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-当前选择企业标签值定义数据(所有)
    changeCurrentSelectCustCompanyTagDefine(state, { payload }) {
      return {
        ...state,
        currentSelectCustCompanyTagDefine: payload || [],
      };
    },
  },
  effects: {
    // 获取-企业标签类型(列表)
    *getCustCompanyTagType(_, { call, put }) {
      const res = yield call(
        getCustCompanyTagType,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: {},
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustCompanyTagTypeList', payload: result });
    },
    // 新增-企业标签类型
    *addCustCompanyTagType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addCustCompanyTagType, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyTagType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-企业标签类型
    *updateCustCompanyTagType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateCustCompanyTagType, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyTagType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-企业标签类型状态(使用/停用)
    *updateCustCompanyTagTypeEnable({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateCustCompanyTagTypeEnable, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyTagType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 删除-企业标签类型
    *deleteCustCompanyTagType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(deleteCustCompanyTagType, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyTagType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 分页-企业标签类型(表格)
    *paginationCustCompanyTagType({ payload }, { call, put }) {
      const res = yield call(
        paginationCustCompanyTagType,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustCompanyTagTypeList', payload: result });
    },

    // 获取-企业标签值定义(所有,按企业标签类型条件查询)
    *getCustCompanyTagDefineAll(
      {
        payload: { companyTagTypeCode },
      },
      { call, put }
    ) {
      const res = yield call(getCustCompanyTagDefineAll, wrapTid({ companyTagTypeCode }));
      const result = requestResult(res);
      yield put({ type: 'changeCurrentSelectCustCompanyTagDefine', payload: result });
    },
    // 新增-企业标签值定义
    *addCustCompanyTagDefine({ payload, callback }, { call, put }) {
      const res = yield call(addCustCompanyTagDefine, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'getCustCompanyTagDefineAll',
        payload: { companyTagTypeCode: payload.companyTagTypeCode },
      });
    },
    // 更新-企业标签值定义
    *updateCustCompanyTagDefine({ payload, callback }, { call, put }) {
      const res = yield call(updateCustCompanyTagDefine, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'getCustCompanyTagDefineAll',
        payload: { companyTagTypeCode: payload.companyTagTypeCode },
      });
    },
    // 更新-企业标签值定义状态(使用/停用)
    *updateCustCompanyTagDefineEnable({ payload, callback }, { call, put }) {
      const res = yield call(updateCustCompanyTagDefineEnable, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'getCustCompanyTagDefineAll',
        payload: { companyTagTypeCode: payload.companyTagTypeCode },
      });
    },
    // 删除-企业标签值定义
    *deleteCustCompanyTagDefine({ payload, callback }, { call, put }) {
      const res = yield call(deleteCustCompanyTagDefine, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'getCustCompanyTagDefineAll',
        payload: { companyTagTypeCode: payload.companyTagTypeCode },
      });
    },
  },
};
