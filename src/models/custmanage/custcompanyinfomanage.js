/**
 * [企业信息管理] model层
 */

import {
  getCustCompanyInfo,
  getCustCompanyInfoAll,
  addCustCompanyInfo,
  updateCustCompanyInfo,
  updateCustCompanyInfoEnable,
  paginationCustCompanyInfo,
  getParmRegionByReginCodesUsing,
} from '@/services/custmanage/custcompanyinfomanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'custcompanyinfomanage',
  state: {
    custCompanyInfoList: [], // 企业信息列表
    custCompanyInfoListPageSize: 0, // 企业信息列表总数量
    custCompanyInfoAll: [], // 企业信息数据(所有)
  },
  reducers: {
    // 更新-企业信息(列表)
    changeCustCompanyInfoList(state, { payload }) {
      return {
        ...state,
        custCompanyInfoList: payload.content || [],
        custCompanyInfoListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-企业信息数据(所有)
    changeCustCompanyInfoAll(state, { payload }) {
      return {
        ...state,
        custCompanyInfoAll: payload || [],
      };
    },
  },
  effects: {
    // 获取-企业信息(列表)
    *getCustCompanyInfo(_, { call, put }) {
      const res = yield call(
        getCustCompanyInfo,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: {},
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustCompanyInfoList', payload: result });
    },
    // 获取-企业信息(所有)
    *getCustCompanyInfoAll(_, { call, put }) {
      const res = yield call(
        getCustCompanyInfoAll,
        wrapTid({
          enable: 1,
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustCompanyInfoAll', payload: result });
    },
    // 新增-企业信息
    *addCustCompanyInfo({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addCustCompanyInfo, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyInfo',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-企业信息
    *updateCustCompanyInfo({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateCustCompanyInfo, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyInfo',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-企业信息状态(使用/停用)
    *updateCustCompanyInfoEnable({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateCustCompanyInfoEnable, payload);
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustCompanyInfo',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 分页-企业信息(表格)
    *paginationCustCompanyInfo({ payload }, { call, put }) {
      const res = yield call(
        paginationCustCompanyInfo,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustCompanyInfoList', payload: result });
    },
    // 获取行政区号
    *getParmRegionByReginCodesUsing({ payload, callback }, { call }) {
      const res = yield call(getParmRegionByReginCodesUsing, wrapTid(payload));
      callback(res);
    },
  },
};
