/**
 * [客户CA记录管理组件] model层
 */

import { getCustCaRecord, addCustCaRecord } from '@/services/custmanage/custcarecordmanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'custcarecordmanage',
  state: {
    custCaRecordList: [], // 客户CA记录列表
    custCaRecordListPageSize: 0, // 客户CA记录列表总数量
    custCaRecordAll: [], // 客户CA记录数据(所有)
  },
  reducers: {
    // 更新-客户CA记录（列表）
    changeCustCaRecord(state, { payload }) {
      return {
        ...state,
        custCaRecordList: payload.content || [],
        custCaRecordListPageSize: Number(payload.total || 0),
      };
    },
  },
  effects: {
    // 获取-客户CA记录列表
    *getCustCaRecord(_, { call, put }) {
      const res = yield call(
        getCustCaRecord,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: {},
          sortCondition: '[{"property":"id", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustCaRecord', payload: result });
    },
    // 新增-客户CA记录
    *addCustCaRecord({ payload, callback }, { call, put }) {
      const res = yield call(addCustCaRecord, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({ type: 'addCustCaRecord' });
    },
  },
};
