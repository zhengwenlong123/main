/**
 * [经营单位类型定义管理] model层
 */

import {
  getCustOperUnitType,
  getCustOperUnitTypeAll,
  addCustOperUnitType,
  updateCustOperUnitType,
  updateCustOperUnitTypeEnable,
  paginationCustOperUnitType,
  deleteCustOperUnitType,
} from '@/services/custmanage/custoperunittypemanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'custoperunittypemanage',
  state: {
    custOperUnitTypeList: [], // 经营单位类型定义列表
    custOperUnitTypeListPageSize: 0, // 经营单位类型定义列表总数据
    custOperUnitTypeAll: [], // 经营单位类型定义数据(所有)
  },
  reducers: {
    // 更新-经营单位类型定义(列表)
    changeCustOperUnitTypeList(state, { payload }) {
      return {
        ...state,
        custOperUnitTypeList: payload.content || [],
        custOperUnitTypeListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-经营单位类型定义(所有)
    changeCustOperUnitTypeAll(state, { payload }) {
      return {
        ...state,
        custOperUnitTypeAll: payload || [],
      };
    },
  },
  effects: {
    // 获取-经营单位类型定义(列表)
    *getCustOperUnitType(_, { call, put }) {
      const res = yield call(
        getCustOperUnitType,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: {},
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustOperUnitTypeList', payload: result });
    },
    // 获取-经营单位类型定义(所有)
    *getCustOperUnitTypeAll(_, { call, put }) {
      const res = yield call(
        getCustOperUnitTypeAll,
        wrapTid({
          enable: 1,
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustOperUnitTypeAll', payload: result });
    },
    // 新增-经营单位类型定义
    *addCustOperUnitType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addCustOperUnitType, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustOperUnitType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-经营单位类型定义
    *updateCustOperUnitType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateCustOperUnitType, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustOperUnitType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-经营单位类型定义状态(使用/停用)
    *updateCustOperUnitTypeEnable({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateCustOperUnitTypeEnable, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustOperUnitType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 删除-经营单位类型定义
    *deleteCustOperUnitType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(deleteCustOperUnitType, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationCustOperUnitType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 分页-经营单位类型定义(表格)
    *paginationCustOperUnitType({ payload }, { call, put }) {
      const res = yield call(
        paginationCustOperUnitType,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCustOperUnitTypeList', payload: result });
    },
  },
};
