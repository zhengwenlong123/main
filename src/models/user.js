export default {
  namespace: 'user',

  state: {
    list: [],
    currentUser: {},
  },

  effects: {},

  reducers: {
    // 储存当前用户信息
    saveCurrentUser(state, action) {
      return {
        ...state,
        currentUser: action.payload || {},
      };
    },
  },
};
