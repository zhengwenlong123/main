/**
 * [行政区划定义管理] model层
 */

import {
  getParmRegion,
  getParmRegionAllByParmRegion,
  addParmRegion,
  updateParmRegion,
  updateParmRegionEnable,
  paginationParmRegion,
  getParmRegionOfCountryCodeAll,
} from '@/services/parmmanage/parmregionmanage';
import { wrapTid, requestResult, parseRegionDataHandle } from '@/utils/mdcutil';

export default {
  namespace: 'parmregionmanage',
  state: {
    parmRegionList: [], // 行政区划定义(列表)
    parmRegionListPageSize: 0, // 行政区划定义列表总数据
    parmRegionAll: [], // 行政区划定义数据(所有,存储树形结构)
    parmRegionOfCountryCodeAll: [], // 行政区划定义数据内按-国别代码regionLevel为1-数据(列表所有)
    currentSelectParmRegionCascader: {}, // 当前选择-级联行政区划定义(对象)
    currentSelectParmRegionOfCountry: {}, // 当前选择-行政区划-国家(对象)
    currentSelectParmRegionCascaderCmpValue: [], // 当前选择-级联行政区划定义的值(数组,仅为Cascader组件展示值使用)
    parmRegionTableLoading: false, // 加载行政区划定义列表(状态)
  },
  reducers: {
    // 更新-行政区划定义(列表)
    changeParmRegionList(state, { payload }) {
      return {
        ...state,
        parmRegionList: payload.content || [],
        parmRegionListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-行政区划定义(所有)(树形)
    changeParmRegionAll(state, { payload }) {
      return {
        ...state,
        parmRegionAll: payload || [],
      };
    },
    // 更新-行政区划定义数据内按-国别代码regionLevel为1-数据(所有)
    changeParmRegionOfCountryCodeAll(state, { payload }) {
      return {
        ...state,
        parmRegionOfCountryCodeAll: payload || [],
      };
    },
    // 更新-当前选择-级联行政区划定义对象
    changeCurrentSelectParmRegionCascader(state, { payload }) {
      return {
        ...state,
        currentSelectParmRegionCascader: payload || {},
      };
    },
    // 更新-加载行政区划定义列表状态
    changeParmRegionTableLoading(state, { payload }) {
      return {
        ...state,
        parmRegionTableLoading: payload || false,
      };
    },
    // 更新-当前选择-行政区划-国家(对象)
    changeCurrentSelectParmRegionOfCountry(state, { payload }) {
      return {
        ...state,
        currentSelectParmRegionOfCountry: payload || {},
      };
    },
    // 更新-当前选择-级联行政区划定义的值(数组,仅为Cascader组件展示值使用)
    changeCurrentSelectParmRegionCascaderCmpValue(state, { payload }) {
      return {
        ...state,
        currentSelectParmRegionCascaderCmpValue: payload || [],
      };
    },
  },
  effects: {
    // 获取-行政区划定义(列表)
    *getParmRegion({ payload }, { call, put }) {
      // 第1步：设置加table加载状态为true
      yield put({ type: 'changeParmRegionTableLoading', payload: true });
      // 第2步: 获取行政区划定义分页数据
      const res = yield call(
        getParmRegion,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: { ...payload },
          sortCondition: '[{"property":"id", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeParmRegionList', payload: result });
      // 第3步: 设置加载table加载状态为false
      yield put({ type: 'changeParmRegionTableLoading', payload: false });
    },
    // 获取-行政区划定义(所有)(根据行政区划条件)
    *getParmRegionAllByParmRegion({ payload }, { call, put }) {
      const { countryCode, regionCode, regionLevel } = payload;
      const res = yield call(getParmRegionAllByParmRegion, wrapTid({ countryCode }));
      const result = requestResult(res);

      // 从数据库获取的数据为列表结构->现在转换成树形结构->存储到parmRegionList
      yield put({ type: 'changeParmRegionAll', payload: parseRegionDataHandle(result) });
      // 在此处默认获取regionLevel为2的数据(列表分页)
      yield put({
        type: 'getParmRegion',
        payload: {
          countryCode,
          parentRegionCode: regionCode,
          regionLevel: Number(regionLevel) + 1,
        },
      });
    },
    // 新增-行政区划定义
    *addParmRegion({ payload, callback }, { call, put }) {
      // 第1步：添加数据
      const res = yield call(addParmRegion, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);

      // 第2步：获取-第1页数据
      const { countryCode, parentRegionCode, regionLevel } = payload;
      yield put({ type: 'getParmRegion', payload: { countryCode, parentRegionCode, regionLevel } });

      // 第3步：重新获取行政区划级联选择的所有数据
      const resTmp = yield call(getParmRegionAllByParmRegion, wrapTid({ countryCode }));
      const result = requestResult(resTmp);
      // 从数据库获取的数据为列表结构->现在转换成树形结构->存储到parmRegionList
      yield put({ type: 'changeParmRegionAll', payload: parseRegionDataHandle(result) });
    },
    // 更新-行政区划定义
    *updateParmRegion({ payload, callback }, { call, put }) {
      // 第1步：更新数据
      const res = yield call(updateParmRegion, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);

      // 第2步：获取-第1页数据
      const { countryCode, parentRegionCode, regionLevel } = payload;
      yield put({ type: 'getParmRegion', payload: { countryCode, parentRegionCode, regionLevel } });

      // 第3步：重新获取行政区划级联选择的所有数据
      const resTmp = yield call(getParmRegionAllByParmRegion, wrapTid({ countryCode }));
      const result = requestResult(resTmp);
      // 从数据库获取的数据为列表结构->现在转换成树形结构->存储到parmRegionList
      yield put({ type: 'changeParmRegionAll', payload: parseRegionDataHandle(result) });
    },
    // 更新-行政区划定义状态(使用/停用)
    *updateParmRegionEnable({ payload, callback }, { call, put }) {
      const res = yield call(updateParmRegionEnable, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);

      // 获取-第1页数据
      const { countryCode, parentRegionCode, regionLevel } = payload;
      yield put({ type: 'getParmRegion', payload: { countryCode, parentRegionCode, regionLevel } });
    },
    // 分页-行政区划定义(表格)
    *paginationParmRegion({ payload }, { call, put }) {
      const { countryCode, parentRegionCode, regionLevel } = payload;
      const res = yield call(
        paginationParmRegion,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: { countryCode, parentRegionCode, regionLevel },
          sortCondition: '[{"property":"id", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeParmRegionList', payload: result });
    },
    // 获取-行政区划内按国别代码regionLevel为1数据(所有)
    *getParmRegionOfCountryCodeAll(_, { call, put }) {
      const res = yield call(getParmRegionOfCountryCodeAll, wrapTid({}));
      const result = requestResult(res);
      yield put({ type: 'changeParmRegionOfCountryCodeAll', payload: result });

      // 注意: 根据默认国家代码(中国),查询行政区划定义数据(所有)
      // 后期国际化,根据不同国家需要设置不同的默认值(也可以从后台接口中获取默认国家代码参数)
      const pr = result.filter(item => item.regionCode === '000000000000');

      if (pr && pr.length > 0) {
        // 更新-默认当前选择-行政区划-国家(对象)
        yield put({ type: 'changeCurrentSelectParmRegionOfCountry', payload: pr[0] });
        // 获取-默认国家代码下的-行政区划数据(所有)
        yield put({ type: 'getParmRegionAllByParmRegion', payload: pr[0] });
        // 设置-当前选择-级联行政区划定义对象(把当前选择的国家做为默认数据)
        yield put({ type: 'changeCurrentSelectParmRegionCascader', payload: pr[0] });
      }
    },
  },
};
