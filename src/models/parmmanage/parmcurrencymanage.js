/* eslint-disable no-param-reassign */
/**
 * [币别管理] model层
 */

import {
  getParmCurrency,
  getParmCurrencyAll,
  addParmCurrency,
  updateParmCurrency,
  paginationParmCurrency,
} from '@/services/parmmanage/parmcurrencymanage';
import { requestResult, wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'parmcurrencymanage',
  state: {
    parmCurrencyList: [], // 币别定义列表
    parmCurrencyListPageSize: 0, // 币别定义列表总数量
    parmCurrencyAll: [], // 币别定义数据(所有)
  },
  reducers: {
    // 更新-币别定义(列表)
    changegParmCurrencyList(state, { payload }) {
      return {
        ...state,
        parmCurrencyList: payload.content || [],
        parmCurrencyListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-币别定义数据(所有)
    changeParmCurrencyAll(state, { payload }) {
      return {
        ...state,
        parmCurrencyAll: payload,
      };
    },
  },
  effects: {
    // 获取-币别定义(列表)
    *getParmCurrency(_, { call, put }) {
      const res = yield call(getParmCurrency, {
        limit: 10,
        page: 0,
        searchCondition: {},
        sortCondition: '[{"property":"id", "direction":"DESC"}]',
      });
      const result = requestResult(res);
      yield put({ type: 'changegParmCurrencyList', payload: result });
    },
    // 获取-币别定义(所有)
    *getParmCurrencyAll(_, { call, put }) {
      const res = yield call(getParmCurrencyAll, wrapTid({}));
      const result = requestResult(res);
      yield put({ type: 'changeParmCurrencyAll', payload: result });
    },
    // 新增-币别定义
    *addParmCurrency({ payload, callback }, { call, put }) {
      const res = yield call(addParmCurrency, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({ type: 'getParmCurrency' });
    },
    // 更新-币别定义
    *updateParmCurrency({ payload, callback }, { call, put }) {
      const res = yield call(updateParmCurrency, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({ type: 'getParmCurrency' });
    },
    // 分页-币别定义(表格)
    *paginationParmCurrency({ payload }, { call, put }) {
      const res = yield call(paginationParmCurrency, {
        limit: payload.pageSize,
        page: payload.page - 1,
        searchCondition: {},
        sortCondition: '[{"property":"id", "direction":"DESC"}]',
      });
      const result = requestResult(res);
      yield put({ type: 'changegParmCurrencyList', payload: result });
    },
  },
};
