/**
 * [国别定义管理] model层
 */

import {
  getParmCountry,
  getParmCountryAll,
  addParmCountry,
  updateParmCountry,
  updateParmCountryEnable,
  paginationParmCountry,
} from '@/services/parmmanage/parmcountrymanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'parmcountrymanage',
  state: {
    parmCountryList: [], // 国别定义列表
    parmCountryListPageSize: 0, // 国别定义列表总数据
    parmCountryAll: [], // 国别定义(所有)
    currentSelectParmCountryOfContinent: '亚洲', // 当前选择-国别定义-所属洲(默认为亚洲)
  },
  reducers: {
    // 更新-国别定义(列表)
    changeParmCountryList(state, { payload }) {
      return {
        ...state,
        parmCountryList: payload.content || [],
        parmCountryListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-国别定义(所有)
    changeParmCountryAll(state, { payload }) {
      return {
        ...state,
        parmCountryAll: payload || [],
      };
    },
    // 更新-当前选择-国别定义-所属洲(默认为亚洲)
    changecurrentSelectParmCountryOfContinent(state, { payload }) {
      return {
        ...state,
        currentSelectParmCountryOfContinent: payload || '亚洲',
      };
    },
  },
  effects: {
    // 获取-国别定义(列表)
    *getParmCountry({ payload }, { call, put }) {
      const { continentName } = payload;
      const res = yield call(
        getParmCountry,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: { continentName },
          sortCondition: '[{"property":"id", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeParmCountryList', payload: result });
    },
    // 获取-国别定义(所有)
    *getParmCountryAll(_, { call, put }) {
      const res = yield call(getParmCountryAll, wrapTid({}));
      const result = requestResult(res);
      yield put({ type: 'changeParmCountryAll', payload: result });
    },
    // 新增-国别定义
    *addParmCountry({ payload, callback }, { call, put }) {
      const res = yield call(addParmCountry, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);

      // 获取-当前选择国别定义列表
      const { currentSelectContinent } = payload;
      yield put({ type: 'getParmCountry', payload: { continentName: currentSelectContinent } });
    },
    // 更新-国别定义
    *updateParmCountry({ payload, callback }, { call, put }) {
      const res = yield call(updateParmCountry, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);

      // 获取-当前选择国别定义列表
      const { currentSelectContinent } = payload;
      yield put({ type: 'getParmCountry', payload: { continentName: currentSelectContinent } });
    },
    // 更新-国别定义状态(使用/停用)
    *updateParmCountryEnable({ payload, callback }, { call, put }) {
      const res = yield call(updateParmCountryEnable, payload);
      callback(res !== undefined && res !== null && res.code === 0);

      // 获取-当前选择国别定义列表
      const { currentSelectContinent } = payload;
      yield put({ type: 'getParmCountry', payload: { continentName: currentSelectContinent } });
    },
    // 分页-国别定义(表格)
    *paginationParmCountry({ payload }, { call, put }) {
      const { continentName } = payload;
      const res = yield call(
        paginationParmCountry,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: { continentName },
          sortCondition: '[{"property":"id", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeParmCountryList', payload: result });
    },

    // 获取-默认所属洲-国别定义数据(所有)
    *getParmCountryByContinentName(_, { put }) {
      const defaultContinentName = '亚洲';
      // 设置-默认所属洲名称
      yield put({
        type: 'changecurrentSelectParmCountryOfContinent',
        payload: defaultContinentName,
      });
      // 获取-当前选择所属洲-国别定义数据(列表)
      yield put({ type: 'getParmCountry', payload: { continentName: defaultContinentName } });
    },
  },
};
