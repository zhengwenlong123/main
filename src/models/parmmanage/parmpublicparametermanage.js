/**
 * [公用参数管理] model层
 */

import {
  getParmPublicParameterType,
  addParmPublicParameterType,
  updateParmPublicParameterType,
  deleteParmPublicParameterType,
  paginationParmPublicParameterType,
  getParmPublicParameterAllByParmPublicParameterType,
  addParmPublicParameter,
  updateParmPublicParameter,
  deleteParmPublicParameter,
  paginationParmPublicParameter,
  findByParamType,
} from '@/services/parmmanage/parmpublicparametermanage';
import { requestResult, wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'parmpublicparametermanage',
  state: {
    parmPublicParameterTypeList: [], // 公用参数类型定义列表
    parmPublicParameterTypeListPageSize: 0, // 公用参数类型定义总数量
    parmPublicParameterOfGoodsSpuTypeAll: [], // 根据公用参数类型定义代码[goods_spu_type]查询匹配到的公用参数定义数据[产品类型数据所有]
    parmPublicParameterOfGoodsDescTypeAll: [], // 根据公用参数类型定义代码[goods_desc_type]查询匹配到的公用参数定义数据[描述类型数据所有]
    parmPublicParameterOfGoodsUnitOfQuantityAll: [], // 根据公用参数类型定义代码[goods_unit_of_quantity]查询匹配到的公用参数定义数据[商品(单品)数量单位数据所有]
    parmPublicParameterOfCustUserCardTypeAll: [], // 根据公用参数类型定义代码[cust_user_card_type]查询匹配到的公用参数定义数据[用户证件类型数据所有]
    parmPublicParameterOfCustSysCodeMappingTypeAll: [], // 根据公用参数类型定义代码[cust_sys_code_mapping]查询匹配到的公用参数定义数据[客户系统代码所有]
    currentSelectParmPublicParameter: [], // 当前选择-公用参数列表(根据公用参数类型定义代码[code]查询匹配到的公用参数定义数据)
    currentSelectParmPublicParameterPageSize: 0, // 当前选择-公用参数列表(根据公用参数类型定义代码[code]查询匹配到的公用参数定义数据)-总数量
    parmPublicParameterApplyBillTypeAll: [], // 获取-公用参数定义(根据公用参数类型定义[goods_rf_type]查询公用参数定义所有数据)(产品申请单类型数据所有)
    parmPublicParameterProductType: [], // 获取-公用参数定义(根据公用参数类型定义[goods_spu_type]查询公用参数定义所有数据)(产品类型数据)
    parmPublicParameterProductDesc: [], // 获取-公用参数定义(根据公用参数类型定义[goods_desc_type]查询公用参数定义所有数据)(产品描述类型数据)
    parmPublicParameterQuantityUnit: [], // 获取-公用参数定义(根据公用参数类型定义[goods_unit_of_quantity]查询公用参数定义所有数据)(商品数量单位类型数据)
    parmPublicParameterGoodsRfStatus: [], // 获取-公用参数定义(根据公用参数类型定义[goods_rf_status]查询公用参数定义所有数据)(申请单状态类型数据)
    parmPublicParameterGoodsEbsSubAttr: [], // 获取-公用参数定义(根据公用参数类型定义[good_ebs_sub_attribute]查询公用参数定义所有数据)(子分类数据)
    parmPublicParameterCustAccountType: [], // 获取-公用参数定义(根据公用参数类型定义[cust_account_type]查询公用参数定义所有数据)(客户申请单账号类型数据)
    parmPublicParameterCustType: [], // 获取-公用参数定义(根据公用参数类型定义[cust_type]查询公用参数定义所有数据)(客户申请单客户性质类型数据)
    parmPublicParameterCustRfStatus: [], // 获取-公用参数定义(根据公用参数类型定义[cust_rf_status]查询公用参数定义所有数据)(客户申请单客户状态数据)
    parmPublicParameterEbsAccountType: [], // 获取-公用参数定义(根据公用参数类型定义[cust_account_type_ebs]查询公用参数定义所有数据)(客户申请单账户分类名称数据)
  },
  reducers: {
    // 更新-公用参数类型定义(列表)
    changeParmPublicParameterTypeList(state, { payload }) {
      return {
        ...state,
        parmPublicParameterTypeList: payload.content || [],
        parmPublicParameterTypeListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-公用参数定义(当前选择)
    changeCurrentSelectParmPublicParameter(state, { payload }) {
      return {
        ...state,
        currentSelectParmPublicParameter: payload.content || [],
        currentSelectParmPublicParameterPageSize: Number(payload.total || 0),
      };
    },
    // 更新-公用参数定义(根据公用参数类型定义名称[goods_spu_type]查询到匹配的公用参数定义数据)
    changeParmPublicParameterOfGoodsSpuTypeAll(state, { payload }) {
      return {
        ...state,
        parmPublicParameterOfGoodsSpuTypeAll: payload || [],
      };
    },
    // 更新-公用参数定义(根据公用参数类型定义名称[goods_desc_type]查询到匹配的公用参数定义数据)
    changeParmPublicParameterOfGoodsDescTypeAll(state, { payload }) {
      return {
        ...state,
        parmPublicParameterOfGoodsDescTypeAll: payload || [],
      };
    },
    // 更新-公用参数定义(根据公用参数类型定义名称[goods_unit_of_quantity]查询到匹配的公用参数定义数据)
    changeParmPublicParameterOfGoodsUnitOfQuantityAll(state, { payload }) {
      return {
        ...state,
        parmPublicParameterOfGoodsUnitOfQuantityAll: payload || [],
      };
    },
    // 更新-公用参数定义(根据公用参数类型定义名称[cust_user_card_type]查询到匹配的公用参数定义数据)
    changeParmPublicParameterOfCustUserCardTypeAll(state, { payload }) {
      return {
        ...state,
        parmPublicParameterOfCustUserCardTypeAll: payload || [],
      };
    },
    // 更新-公用参数定义(根据公用参数类型定义名称[cust_sys_code_mapping]查询到匹配的公用参数定义数据)
    changeParmPublicParameterOfCustSysCodeMappingTypeAll(state, { payload }) {
      return {
        ...state,
        parmPublicParameterOfCustSysCodeMappingTypeAll: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_rf_type]查询公用参数定义所有数据)(产品申请单类型数据所有)
    changeParmPublicParameterApplyBillTypeAll(state, { payload }) {
      return {
        ...state,
        parmPublicParameterApplyBillTypeAll: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_spu_type]查询公用参数定义所有数据)(产品类型数据)
    changeParmPublicParameterProductType(state, { payload }) {
      return {
        ...state,
        parmPublicParameterProductType: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_desc_type]查询公用参数定义所有数据)(产品描述类型数据)
    changeParmPublicParameterProductDesc(state, { payload }) {
      return {
        ...state,
        parmPublicParameterProductDesc: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_unit_of_quantity]查询公用参数定义所有数据)(商品数量单位类型数据)
    changeParmPublicParameterQuantityUnit(state, { payload }) {
      return {
        ...state,
        parmPublicParameterQuantityUnit: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_rf_status]查询公用参数定义所有数据)(申请单状态类型数据)
    changeParmPublicParameterGoodsRfStatus(state, { payload }) {
      return {
        ...state,
        parmPublicParameterGoodsRfStatus: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[good_ebs_sub_attribute]查询公用参数定义所有数据)(子分类数据)
    changeParmPublicParameterGoodsEbsSubAttr(state, { payload }) {
      return {
        ...state,
        parmPublicParameterGoodsEbsSubAttr: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[cust_account_type]查询公用参数定义所有数据)(客户申请单账号类型数据)
    changeParmPublicParameterCustAccountType(state, { payload }) {
      return {
        ...state,
        parmPublicParameterCustAccountType: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[cust_type]查询公用参数定义所有数据)(客户申请单客户类型数据)
    changeParmPublicParameterCustType(state, { payload }) {
      return {
        ...state,
        parmPublicParameterCustType: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[cust_rf_status]查询公用参数定义所有数据)(客户申请单客户状态数据)
    changeParmPublicParameterCustRfStatus(state, { payload }) {
      return {
        ...state,
        parmPublicParameterCustRfStatus: payload || [],
      };
    },
    // 获取-公用参数定义(根据公用参数类型定义[cust_account_type_ebs]查询公用参数定义所有数据)(客户申请单账户分类名称数据)
    changeParmPublicParameterEbsAccountType(state, { payload }) {
      return {
        ...state,
        parmPublicParameterEbsAccountType: payload || [],
      };
    },
  },
  effects: {
    // 获取- 公用参数类型定义(列表)
    *getParmPublicParameterType(_, { call, put }) {
      const res = yield call(
        getParmPublicParameterType,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: {},
          sortCondition: '[{"property":"id", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeParmPublicParameterTypeList', payload: result });
    },
    // 新增-公用参数类型定义
    *addParmPublicParameterType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addParmPublicParameterType, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationParmPublicParameterType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-公用参数类型定义
    *updateParmPublicParameterType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateParmPublicParameterType, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationParmPublicParameterType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 删除-公用参数类型定义
    *deleteParmPublicParameterType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(deleteParmPublicParameterType, payload);
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationParmPublicParameterType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 分页-公用参数类型定义(表格)
    *paginationParmPublicParameterType({ payload }, { call, put }) {
      const res = yield call(paginationParmPublicParameterType, {
        limit: payload.pageSize,
        page: payload.page - 1,
        searchCondition: payload.searchCondition,
        sortCondition: '[{"property":"id", "direction":"DESC"}]',
      });
      const result = requestResult(res);
      yield put({ type: 'changeParmPublicParameterTypeList', payload: result });
    },

    // 分页-公用参数定义(表格)
    *paginationParmPublicParameter({ payload }, { call, put }) {
      const res = yield call(
        paginationParmPublicParameter,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: { parmTypeCode: payload.parmTypeCode },
          sortCondition: '[{"property":"seq", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCurrentSelectParmPublicParameter', payload: result });
    },

    // 获取-公用参数定义(通过公用参数类型定义的代码[goods_spu_type]查询公用参数定义数据)(产品类型数据所有)
    *getParmPublicParameterAllByParmPublicParameterTypeOfGoodsSpuType({ payload }, { call, put }) {
      const res = yield call(getParmPublicParameterAllByParmPublicParameterType, wrapTid(payload));
      const result = requestResult(res);
      yield put({ type: 'changeParmPublicParameterOfGoodsSpuTypeAll', payload: result });
    },
    // 获取-公用参数定义(通过公用参数类型定义的代码[goods_desc_type]查询公用参数定义数据)(描述类型数据所有)
    *getParmPublicParameterAllByParmPublicParameterTypeOfGoodsDescType({ payload }, { call, put }) {
      const res = yield call(getParmPublicParameterAllByParmPublicParameterType, wrapTid(payload));
      const result = requestResult(res);
      yield put({ type: 'changeParmPublicParameterOfGoodsDescTypeAll', payload: result });
    },
    // 获取-公用参数定义(通过公用参数类型定义的代码[goods_unit_of_quantity]查询公用参数定义数据)(商品(单品)信息数量单位数据所有)
    *getParmPublicParameterAllByParmPublicParameterTypeOfGoodsUnitOfQuantity(
      { payload },
      { call, put }
    ) {
      const res = yield call(getParmPublicParameterAllByParmPublicParameterType, wrapTid(payload));
      const result = requestResult(res);
      yield put({ type: 'changeParmPublicParameterOfGoodsUnitOfQuantityAll', payload: result });
    },
    // 获取-公用参数定义(通过公用参数类型定义的代码[cust_user_card_type]查询公用参数定义数据)(用户证件类型数据所有)
    *getParmPublicParameterAllByParmPublicParameterTypeOfCustUserCardType(
      { payload },
      { call, put }
    ) {
      const res = yield call(getParmPublicParameterAllByParmPublicParameterType, wrapTid(payload));
      const result = requestResult(res);
      yield put({ type: 'changeParmPublicParameterOfCustUserCardTypeAll', payload: result });
    },
    // 获取-公用参数定义(通过公用参数类型定义的代码[cust_sys_code_mapping]查询公用参数定义数据)(客户系统代码对照所有)
    *getParmPublicParameterAllByParmPublicParameterTypeOfCustSysCodeMapping(
      { payload },
      { call, put }
    ) {
      const res = yield call(getParmPublicParameterAllByParmPublicParameterType, wrapTid(payload));
      const result = requestResult(res);
      yield put({ type: 'changeParmPublicParameterOfCustSysCodeMappingTypeAll', payload: result });
    },
    // 新增-公用参数定义
    *addParmPublicParameter({ payload, callback }, { call, put }) {
      const res = yield call(addParmPublicParameter, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);

      // 更新公用参数定义列表
      yield put({
        type: 'paginationParmPublicParameter',
        payload: { parmTypeCode: payload.parmTypeCode, page: 1, pageSize: 10 },
      });
    },
    // 更新-公用参数定义
    *updateParmPublicParameter({ payload, callback }, { call, put }) {
      const res = yield call(updateParmPublicParameter, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);

      // 更新公用参数定义列表
      yield put({
        type: 'paginationParmPublicParameter',
        payload: { parmTypeCode: payload.parmTypeCode, page: 1, pageSize: 10 },
      });
    },
    // 删除-公用参数定义
    *deleteParmPublicParameter({ payload, callback }, { call, put }) {
      const res = yield call(deleteParmPublicParameter, payload.id);
      callback(res !== undefined && res !== null && res.code === 0);

      // 更新公用参数定义列表
      yield put({
        type: 'paginationParmPublicParameter',
        payload: { parmTypeCode: payload.parmTypeCode, page: 1, pageSize: 10 },
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_rf_type]查询公用参数定义所有数据)(产品申请单类型数据所有)
    *getApplyBillType({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterApplyBillTypeAll',
        payload: result,
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_spu_type]查询公用参数定义所有数据)(产品类型数据)
    *getProductType({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterProductType',
        payload: result,
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_desc_type]查询公用参数定义所有数据)(产品描述类型数据)
    *getProductDesc({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterProductDesc',
        payload: result,
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_unit_of_quantity]查询公用参数定义所有数据)(商品数量单位类型数据)
    *getSpuQuantityUnit({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterQuantityUnit',
        payload: result,
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[goods_rf_status]查询公用参数定义所有数据)(申请单状态类型数据)
    *getGoodsRfStatus({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterGoodsRfStatus',
        payload: result,
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[good_ebs_sub_attribute]查询公用参数定义所有数据)(子分类数据)
    *getGoodsEbsSubAttr({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterGoodsEbsSubAttr',
        payload: result,
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[cust_account_type]查询公用参数定义所有数据)(客户申请单账号类型数据)
    *getCustAccountType({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterCustAccountType',
        payload: result,
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[cust_type]查询公用参数定义所有数据)(客户申请单客户类型数据)
    *getCustType({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterCustType',
        payload: result,
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[cust_rf_status]查询公用参数定义所有数据)(客户申请单客户状态数据)
    *getCustRfStatus({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterCustRfStatus',
        payload: result,
      });
    },
    // 获取-公用参数定义(根据公用参数类型定义[cust_account_type_ebs]查询公用参数定义所有数据)(客户申请单账户分类名称数据)
    *getEbsAccountType({ payload }, { call, put }) {
      const res = yield call(findByParamType, wrapTid({ parmTypeCode: payload.parmTypeCode }));
      const result = requestResult(res);
      yield put({
        type: 'changeParmPublicParameterEbsAccountType',
        payload: result,
      });
    },
  },
};
