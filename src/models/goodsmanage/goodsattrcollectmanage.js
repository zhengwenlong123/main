/**
 * [属性集管理] model层
 */

import {
  getGoodsAttrCollect,
  getGoodsAttrCollectAll,
  addGoodsAttrCollect,
  updateGoodsAttrCollect,
  updateGoodsAttrCollectEnable,
  paginationGoodsAttrCollect,
} from '@/services/goodsmanage/goodsattrcollectmanage';
import { requestResult, wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'goodsattrcollectmanage',
  state: {
    goodsAttrCollectList: [], // 属性集列表
    goodsAttrCollectListPageSize: 0, // 属性集列表总数量
    goodsAttrCollectAll: [], // 属性集所有数据
  },
  reducers: {
    // 更新-属性集(所有)
    changeGoodsAttrCollectAll(state, { payload }) {
      return {
        ...state,
        goodsAttrCollectAll: payload,
      };
    },
    // 更新-属性集(列表)
    changeGoodsAttrCollectList(state, { payload }) {
      return {
        ...state,
        goodsAttrCollectList: payload.content,
        goodsAttrCollectListPageSize: Number(payload.total || 0),
      };
    },
  },
  effects: {
    // 获取-属性集(列表)
    *getGoodsAttrCollect(_, { call, put }) {
      const res = yield call(
        getGoodsAttrCollect,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: {},
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsAttrCollectList', payload: result });
    },
    // 获取-属性集(所有)
    *getGoodsAttrCollectAll(_, { call, put }) {
      const res = yield call(
        getGoodsAttrCollectAll,
        wrapTid({
          attrCollectionEnable: 1,
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsAttrCollectAll', payload: result });
    },
    // 新增-属性集
    *addGoodsAttrCollect({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsAttrCollect, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({ type: 'getGoodsAttrCollect' });
    },
    // 更新-属性集
    *updateGoodsAttrCollect({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsAttrCollect, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({ type: 'getGoodsAttrCollect' });
    },
    // 更新-属性集状态(使用/停用)
    *updateGoodsAttrCollectEnable({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsAttrCollectEnable, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({ type: 'getGoodsAttrCollect' });
    },
    // 分页-属性集(表格)
    *paginationGoodsAttrCollect({ payload }, { call, put }) {
      const res = yield call(
        paginationGoodsAttrCollect,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: {},
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsAttrCollectList', payload: result });
    },
  },
};
