/* eslint-disable no-param-reassign */
/* eslint-disable no-console */
/**
 * [商品信息管理] model层
 */

import {
  // 获取-产品信息接口
  getSpuListBySpu,
  // 获取-商品分组接口
  getCurrentSpuCondOfGoodsSkuGroupList,
  // 获取-产品型号接口
  getCurrentSpuCondOfGoodsSpuModelList,
  // 获取-产品普通属性接口
  getCurrentSpuCondOfGoodsSpuAttrNormalList,
  // 获取-产品sku属性接口以及产品sku属性项值接口
  getCurrentSpuCondOfGoodsSpuAttrSkuListWithChildAttrSkuItemList,
  // 获取-根据产品sku属性条件获取到产品sku属性项值数据接口
  getGoodsSpuAttrSkuItemListByGoodsSpuAttrSku,

  // 获取-商品基本信息
  getLatestGoodsSkuInfo,
  paginationGoodsSkuInfo,
  addGoodsSkuInfo,
  addGoodsSkuAllInfo,
  updateGoodsSkuInfo,
  // 获取-商品sku属性数据
  getCurrentSelectGoodsSkuAttrSku,
  addGoodsSkuAttrSku,
  // updateGoodsSkuAttrSku,
  // 获取-商品普通属性数据
  getCurrentSelectGoodsSkuAttrNormal,
  addGoodsSkuAttrNormal,
  updateGoodsSkuAttrNormal,

  // 商品(单品)商品描述
  paginationGoodsSkuDesc,
  addGoodsSkuDesc,
  updateGoodsSkuDesc,
  deleteGoodsSkuDesc,

  // 商品(单品)商品物料
  paginationGoodsMaterielInfo,
  addGoodsMaterielInfo,
  updateGoodsMaterielInfo,
} from '@/services/goodsmanage/goodsskuinfomanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'goodsskuinfomanage',
  state: {
    tableLoadingStatus: false, // 商品列表数据加载状态
    goodsSkuInfoList: [], // 商品信息列表
    goodsSkuInfoListPageSize: 0, // 商品信息列表总数量
    currentCondSelectEffectiveSpu: {}, // 当前条件选择的有效产品信息数据(对象)
    currentCondSearchEffectiveSpuList: [], // 当前条件查询到的有效产品信息数据(列表)
    currentSpuCondOfGoodsSkuGroupList: [], // 当前产品条件-对应的商品分组数据(列表)
    currentSpuCondOfGoodsSpuModelList: [], // 当前产品条件-对应的产品型号数据(列表)
    goodsSkuAttrSkuList: [], // 商品sku属性数据(列表)
    goodsSkuAttrSkuUnderSkuGroupList: [], // 基于商品分组已关联商品的商品sku属性数据(列表)
    goodsSkuAttrNormalList: [], // 商品普通属性数据(列表)
    goodsSkuDescList: [], // 商品描述数据(列表/分页)
    goodsSkuDescListPageSize: 0, // 商品描述数据总数量
    goodsMaterielInfoList: [], // 商品物料数据(列表/分页)
    goodsMaterielInfoListPageSize: 0, // 商品物料数据总数量
    // 以下变量在新增商品时使用
    currentInputSkuName: '', // 当前新增商品时输入的商品名称
    currentInputSkuShortName: '', // 当前新增商品时输入的商品简称
    currentInputSkuDesc: '', // 当前新增商品时输入的简介
    currentSelectDefaultModelCode: '', // 当前新增商品时选择的缺省产品型号代码
    currentSelectUnitOfQuantityCode: '', // 当前新增商品时选择的数量单位代码
    currentSelectSeq: 1, // 当前新增商品时选择的序号
    currentSelectGoodsSkuGroup: '', // 当前新增商品时选择的商品分组数据(对象)
  },
  reducers: {
    // 更新-商品管理列表数据加载状态
    changeTableLoadingStatus(state, { payload }) {
      return {
        ...state,
        tableLoadingStatus: payload || false,
      };
    },
    // 更新-当前条件[查询/选择]到的有效产品信息数据
    changeCurrentCondEffectiveSpuData(state, { payload }) {
      const { spu, spuList } = payload;
      const result = { ...state };
      if (spu instanceof Object) result.currentCondSelectEffectiveSpu = spu;
      if (spuList instanceof Array) result.currentCondSearchEffectiveSpuList = spuList;
      return result;
    },
    // 更新-商品信息数据列表
    changeGoodsSkuInfoList(state, { payload }) {
      return {
        ...state,
        goodsSkuInfoList: payload.content,
        goodsSkuInfoListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-当前产品条件-对应的商品分组数据(列表/所有)
    changeCurrentSpuCondOfGoodsSkuGroupList(state, { payload }) {
      return {
        ...state,
        currentSpuCondOfGoodsSkuGroupList: payload || [],
      };
    },
    // 更新-当前选择的商品分组数据(对象)
    changeCurrentSelectGoodsSkuGroup(state, { payload }) {
      return {
        ...state,
        currentSelectGoodsSkuGroup: payload,
      };
    },
    // 更新-当前产品条件-对应产品型号数据(列表/所有)
    changeCurrentSpuCondOfGoodsSpuModelList(state, { payload }) {
      return {
        ...state,
        currentSpuCondOfGoodsSpuModelList: payload || [],
      };
    },
    // 更新-商品sku属性数据(列表)
    changeGoodsSkuAttrSkuList(state, { payload }) {
      return {
        ...state,
        goodsSkuAttrSkuList: payload || [],
      };
    },
    // 更新-基于商品分组已关联商品的商品sku属性数据(列表)
    changeGoodsSkuAttrSkuUnderSkuGroupList(state, { payload }) {
      return {
        ...state,
        goodsSkuAttrSkuUnderSkuGroupList: payload,
      };
    },
    // 更新-商品普通属性数据(列表)
    changeGoodsSkuAttrNormalList(state, { payload }) {
      return {
        ...state,
        goodsSkuAttrNormalList: payload || [],
      };
    },
    // 更新-商品描述数据(列表/分页)
    changeGoodsSkuDescList(state, { payload }) {
      return {
        ...state,
        goodsSkuDescList: payload.content || [],
        goodsSkuDescListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-商品物料数据(列表/分页)
    changeGoodsMaterielInfoList(state, { payload }) {
      return {
        ...state,
        goodsMaterielInfoList: payload.content || [],
        goodsMaterielInfoListPageSize: Number(payload.total || 0),
      };
    },
  },
  effects: {
    // 获取-首次默认商品(单品)列表数据
    *getDefaultFirstGoodsSkuInfo(_, { call, put }) {
      // 第1步：查询所有产品信息数据-供页面下拉选择使用
      yield put({ type: 'getGoodsSpuInfoListByGoodsSpuInfo', payload: {} }); // 默认加载所有,所以这里payload传{}即可

      // 第2步：获取最新1条添加的商品信息数据值
      const res = yield call(getLatestGoodsSkuInfo, wrapTid({}));
      const result = requestResult(res);
      const { spuCode } = result;
      if (spuCode === null || spuCode === undefined) return;

      // 第3步：设置当前默认选择产品信息数据对象
      const cuurSpuList = yield call(getSpuListBySpu, wrapTid({ spuCode })); // 默认加载当前自动选择的产品信息对象
      const resultCuurSpuList = requestResult(cuurSpuList);
      yield put({
        type: 'changeCurrentCondEffectiveSpuData',
        payload: { spu: resultCuurSpuList[0] },
      });

      // 第4步: 设置当前默认选中的产品信息分组列表
      const cuurSkuGroupList = yield call(
        getCurrentSpuCondOfGoodsSkuGroupList,
        wrapTid({ spuCode: resultCuurSpuList[0].spuCode })
      );
      const resultCuurSkuGroupList = requestResult(cuurSkuGroupList);
      yield put({
        type: 'changeCurrentSpuCondOfGoodsSkuGroupList',
        payload: resultCuurSkuGroupList,
      });

      // 第5步：根据商品内产品代码获取产品对应商品信息数据(列表/分页)
      yield put({ type: 'paginationGoodsSkuInfo', payload: { page: 1, pageSize: 10, spuCode } });
    },
    // 获取-根据产品条件/模糊查询产品信息数据(列表/所有符合条件的数据)
    *getGoodsSpuInfoListByGoodsSpuInfo({ payload }, { call, put }) {
      const res = yield call(getSpuListBySpu, wrapTid(payload));
      const result = requestResult(res);
      yield put({ type: 'changeCurrentCondEffectiveSpuData', payload: { spuList: result } });
    },
    // 获取-商品(单品)信息数据(列表/分页)【产品代码spuCode必须有值】
    *paginationGoodsSkuInfo({ payload }, { call, put }) {
      const { page, pageSize, spuCode } = payload;
      if (!page || !pageSize) {
        // console.log('[分页查询商品信息数据时,查询page, pageSize必须有值才可以查询]');
        return;
      }
      // 设置商品管理列表处于加载中状态
      yield put({ type: 'changeTableLoadingStatus', payload: true });

      const res = yield call(
        paginationGoodsSkuInfo,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { spuCode, ...payload.searchCondition },
          sortCondition: '[{"property":"create_at","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSkuInfoList', payload: result });
      // 设置商品管理列表处于加载完成状态
      yield put({ type: 'changeTableLoadingStatus', payload: false });
    },
    // 新增-当前选择产品-商品信息(基本信息/普通属性/sku属性)
    *addGoodsSkuInfo({ payload, callback }, { call, put }) {
      const {
        spuCode,
        skuName,
        skuShortName,
        skuDesc,
        groupCode,
        defaultModelCode,
        seq,
        unitOfQuantityCode,
        unitOfQuantityName,
        skuEnable,
        goodsSkuAttrSku,
        goodsSkuAttrNormal,
      } = payload;

      // 第1步：添加商品基本信息数据
      const res = yield call(
        addGoodsSkuInfo,
        wrapTid({
          spuCode,
          skuName,
          skuShortName,
          skuDesc,
          groupCode,
          defaultModelCode,
          seq,
          unitOfQuantityCode,
          unitOfQuantityName,
          skuEnable,
        })
      );
      if (res === null || res === undefined || res.code !== 0) {
        callback(10, false); // 新建商品基本信息数据失败
        return;
      }

      // 第2步：添加商品sku属性数据
      yield call(
        addGoodsSkuAttrSku,
        goodsSkuAttrSku.map(item => {
          item.skuCode = res.data.skuCode;
          return wrapTid(item);
        })
      );
      // 第3步：添加商品普通属性数据
      yield call(
        addGoodsSkuAttrNormal,
        goodsSkuAttrNormal.map(item => {
          item.skuCode = res.data.skuCode;
          return wrapTid(item);
        })
      );

      callback(10, true);
      yield put({ type: 'paginationGoodsSkuInfo', payload: { spuCode, pageSize: 10, page: 1 } });

      // 第4步：添加默认1条商品物料数据(商品+缺省的产品型号)
      // yield call(
      //   addGoodsMaterielInfo,
      //   wrapTid({
      //     materielName: res.data.skuName,
      //     materielShortName: res.data.skuShortName,
      //     materielDesc: res.data.skuDesc,
      //     spuCode: res.data.spuCode,
      //     skuCode: res.data.skuCode,
      //     groupCode: res.data.groupCode,
      //     modelCode: res.data.defaultModelCode || '',
      //     seq: 1,
      //     materielEnable: 1,
      //   })
      // );
    },
    *addGoodsSkuAllInfo({ payload, callback }, { call, put }) {
      const {
        spuCode,
        skuName,
        skuShortName,
        skuDesc,
        groupCode,
        defaultModelCode,
        seq,
        unitOfQuantityCode,
        unitOfQuantityName,
        skuEnable,
        goodsSkuAttrSku,
        goodsSkuAttrNormal,
      } = payload;
      const res = yield call(
        addGoodsSkuAllInfo,
        wrapTid({
          spuCode,
          skuName,
          skuShortName,
          skuDesc,
          groupCode,
          defaultModelCode,
          seq,
          unitOfQuantityCode,
          unitOfQuantityName,
          skuEnable,
          gsasADList: goodsSkuAttrSku,
          gsanADList: goodsSkuAttrNormal,
        })
      );
      if (res === null || res === undefined || res.code !== 0) {
        callback(10, false); // 新建商品基本信息数据失败
        return;
      }
      callback(10, true);
      yield put({ type: 'paginationGoodsSkuInfo', payload: { spuCode, pageSize: 10, page: 1 } });
    },
    // 更新-当前选择产品-商品信息(基本信息/普通属性/sku属性)
    *updateGoodsSkuInfo({ payload, callback }, { call, put }) {
      const {
        id,
        spuCode,
        skuCode,
        skuName,
        skuShortName,
        skuDesc,
        groupCode,
        defaultModelCode,
        seq,
        unitOfQuantityCode,
        unitOfQuantityName,
        skuEnable,
        goodsSkuAttrNormal,
      } = payload;

      // 更新商品基本信息数据
      const res = yield call(
        updateGoodsSkuInfo,
        wrapTid({
          id,
          spuCode,
          skuCode,
          skuName,
          skuShortName,
          skuDesc,
          groupCode,
          defaultModelCode,
          seq,
          unitOfQuantityCode,
          unitOfQuantityName,
          skuEnable,
        })
      );
      if (res === null || res === undefined || res.code !== 0) {
        callback(0, false); // 更新商品基本信息数据失败
        return;
      }

      // 更新商品sku属性数据,在新商品创建完成后商品的sku属性不可修改,因此不存在对商品sku属性更新的处理
      // if(goodsSkuAttrSku) yield call(updateGoodsSkuAttrSku, goodsSkuAttrSku.map(item=>wrapTid(item)));

      // 更新商品普通属性数据
      if (goodsSkuAttrNormal)
        yield call(updateGoodsSkuAttrNormal, goodsSkuAttrNormal.map(item => wrapTid(item)));

      callback(2, true);
      yield put({ type: 'paginationGoodsSkuInfo', payload: { spuCode, pageSize: 10, page: 1 } });
    },
    *updateGoodsSkuInfoEnable({ payload, callback }, { call, put }) {
      const {
        id,
        spuCode,
        skuCode,
        skuName,
        skuShortName,
        skuDesc,
        groupCode,
        defaultModelCode,
        seq,
        unitOfQuantityCode,
        unitOfQuantityName,
        skuEnable,
      } = payload;
      const res = yield call(
        updateGoodsSkuInfo,
        wrapTid({
          id,
          spuCode,
          skuCode,
          skuName,
          skuShortName,
          skuDesc,
          groupCode,
          defaultModelCode,
          seq,
          unitOfQuantityCode,
          unitOfQuantityName,
          skuEnable: skuEnable === 1 ? 0 : 1,
        })
      );
      if (res === null || res === undefined || res.code !== 0) {
        callback(0, false); // 更新商品基本信息数据失败
        return;
      }
      callback(2, true);
      yield put({ type: 'paginationGoodsSkuInfo', payload: { spuCode, pageSize: 10, page: 1 } });
    },
    // 获取-当前选择的产品-对应商品分组数据(列表所有)
    *getCurrentSpuCondOfGoodsSkuGroupList({ payload }, { call, put }) {
      const res = yield call(getCurrentSpuCondOfGoodsSkuGroupList, wrapTid(payload));
      const result = requestResult(res);
      yield put({ type: 'changeCurrentSpuCondOfGoodsSkuGroupList', payload: result });
    },
    // 获取-当前选择的产品-对应产品型号数据(列表所有)
    *getCurrentSpuCondOfGoodsSpuModelList({ payload }, { call, put }) {
      const res = yield call(getCurrentSpuCondOfGoodsSpuModelList, wrapTid(payload));
      const result = requestResult(res);
      yield put({ type: 'changeCurrentSpuCondOfGoodsSpuModelList', payload: result });
    },
    // 获取-当前选择的产品-对应产品普通属性数据(列表所有)
    *getCurrentSpuCondOfGoodsSpuAttrNormalList({ payload }, { call, put }) {
      const res = yield call(getCurrentSpuCondOfGoodsSpuAttrNormalList, wrapTid(payload));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSkuAttrNormalList', payload: result });
    },
    // 获取-当前选择的产品-对应产品sku属性数据(列表所有)及关联sku属性项值数据(列表所有)
    *getCurrentSpuCondOfGoodsSpuAttrSkuListWithChildAttrSkuItemList({ payload }, { call, put }) {
      const res = yield call(
        getCurrentSpuCondOfGoodsSpuAttrSkuListWithChildAttrSkuItemList,
        wrapTid(payload)
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSkuAttrSkuList', payload: result });
    },

    // 获取-当前选择的商品-商品(单品)[SKU属性]数据(列表所有)
    *getCurrentSelectGoodsSkuAttrSku(
      {
        payload: { spuCode, skuCode },
      },
      { call, put }
    ) {
      const res = yield call(getCurrentSelectGoodsSkuAttrSku, wrapTid({ spuCode, skuCode }));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSkuAttrSkuList', payload: result });
    },
    // 获取-当前选择的商品-商品(单品)[普通属性]数据(列表所有)
    *getCurrentSelectGoodsSkuAttrNormal(
      {
        payload: { spuCode, skuCode },
      },
      { call, put }
    ) {
      const res = yield call(getCurrentSelectGoodsSkuAttrNormal, wrapTid({ spuCode, skuCode }));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSkuAttrNormalList', payload: result });
    },

    // 获取-当前选择的商品-商品(单品)[描述]数据(列表所有/分页)
    *paginationGoodsSkuDesc(
      {
        payload: { page, pageSize, spuCode, skuCode },
      },
      { call, put }
    ) {
      const res = yield call(
        paginationGoodsSkuDesc,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { spuCode, skuCode },
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSkuDescList', payload: result });
    },
    // 新增-当前选择商品-商品描述
    *addGoodsSkuDesc({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsSkuDesc, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      const { spuCode, skuCode } = payload;
      yield put({
        type: 'paginationGoodsSkuDesc',
        payload: { spuCode, skuCode, pageSize: 10, page: 1 },
      });
    },
    // 更新-当前选择商品-商品描述
    *updateGoodsSkuDesc({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsSkuDesc, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      const { spuCode, skuCode } = payload;
      yield put({
        type: 'paginationGoodsSkuDesc',
        payload: { spuCode, skuCode, pageSize: 10, page: 1 },
      });
    },
    // 删除-当前选择商品-商品描述
    *deleteGoodsSkuDesc({ payload, callback }, { call, put }) {
      const res = yield call(deleteGoodsSkuDesc, wrapTid([payload.id]));
      callback(res !== undefined && res !== null && res.code === 0);
      const { spuCode, skuCode } = payload;
      yield put({
        type: 'paginationGoodsSkuDesc',
        payload: { spuCode, skuCode, pageSize: 10, page: 1 },
      });
    },
    // 获取-当前选择商品-商品物料(列表/分页)
    *paginationGoodsMaterielInfo({ payload }, { call, put }) {
      const { spuCode, skuCode, page, pageSize } = payload;
      const res = yield call(
        paginationGoodsMaterielInfo,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { spuCode, skuCode },
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsMaterielInfoList', payload: result });
    },
    // 新增-当前选择商品-商品物料
    *addGoodsMaterielInfo({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsMaterielInfo, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      const { spuCode, skuCode } = payload;
      yield put({
        type: 'paginationGoodsMaterielInfo',
        payload: { spuCode, skuCode, page: 1, pageSize: 10 },
      });
    },
    // 更新-当前选择商品-商品物料
    *updateGoodsMaterielInfo({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsMaterielInfo, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      const { spuCode, skuCode } = payload;
      yield put({
        type: 'paginationGoodsMaterielInfo',
        payload: { spuCode, skuCode, page: 1, pageSize: 10 },
      });
    },
    // 验证及处理-当前选择商品分组是否已存在关联的商品
    *underSkuGroupVerifyAndProcess({ payload }, { call, put }) {
      const {
        skuGroup: {
          spuCode,
          groupCode,
          subAttrCode1,
          subAttrName1,
          subAttrCode2,
          subAttrName2,
          subAttrCode3,
          subAttrName3,
        },
      } = payload;
      // 根据groupCode查询GoodsSkuInfo数据
      const goodsSkuInfoRes = yield call(
        paginationGoodsSkuInfo,
        wrapTid({
          limit: 2000,
          page: 0,
          searchCondition: { groupCode, spuCode },
          // sortCondition: '[{"property":"groupCode","direction":"DESC"}]',
        })
      );
      const goodsSkuInfoResult = requestResult(goodsSkuInfoRes);
      if (Object.keys(goodsSkuInfoResult).length === 0 || goodsSkuInfoResult.content.length === 0) {
        // 该商品分组无关联任何商品,不做其它处理
        // console.log('当前商品分组无关联任何商品,不做其它处理');
      } else {
        // 该商品分组已存在关联商品
        const basicSkus = goodsSkuInfoResult.content.filter(item => item.skuEnable === 1);
        // 取第1条数据当作默认新增商品sku属性数据来源
        const { skuCode } = basicSkus[basicSkus.length - 1];
        // 第1步：根据当前选择的商品-查询该商品的sku属性数据(所有)
        const goodsSkuAttrSkuRes = yield call(
          getCurrentSelectGoodsSkuAttrSku,
          wrapTid({ spuCode, skuCode })
        );
        const goodsSkuAttrSkuResultOrigin = requestResult(goodsSkuAttrSkuRes);
        // 在此处需要[过滤掉]默认指定的商品sku属性与商品分组内从属的属性重复数据
        const goodsSkuAttrSkuResult = goodsSkuAttrSkuResultOrigin.filter(
          item =>
            ![`${subAttrCode1}`, `${subAttrCode2}`, `${subAttrCode3}`].includes(item.skuAttrCode)
        );
        // 第2步：根据当前选择的商品分组-查询商品分组内从属的属性(也就是产品sku属性,共3组)对应到[产品sku属性项值]数据
        if (subAttrCode1 && subAttrName1) {
          const subRes1 = yield call(
            getGoodsSpuAttrSkuItemListByGoodsSpuAttrSku,
            wrapTid({ spuCode, skuAttrCode: subAttrCode1, skuAttrName: subAttrName1 })
          );
          const subSkuItemResult1 = requestResult(subRes1);
          // 组装新增商品sku属性数据,若存在sku属性项值下拉选取操作时,则key名称对应skuAttrItemList,此key名称由后台接口统一定义供前端界面使用
          goodsSkuAttrSkuResult.push({
            spuCode,
            skuCode,
            skuAttrCode: subAttrCode1,
            skuAttrName: subAttrName1,
            skuAttrItemList: subSkuItemResult1,
            skuAttrDesc: '',
            skuAttrEnable: 1,
          });
        }
        if (subAttrCode2 && subAttrName2) {
          const subRes2 = yield call(
            getGoodsSpuAttrSkuItemListByGoodsSpuAttrSku,
            wrapTid({ spuCode, skuAttrCode: subAttrCode2, skuAttrName: subAttrName2 })
          );
          const subSkuItemResult2 = requestResult(subRes2);
          // 组装新增商品sku属性数据,若存在sku属性项值下拉选取操作时,则key名称对应skuAttrItemList,此key名称由后台接口统一定义供前端界面使用
          goodsSkuAttrSkuResult.push({
            spuCode,
            skuCode,
            skuAttrCode: subAttrCode2,
            skuAttrName: subAttrName2,
            skuAttrItemList: subSkuItemResult2,
            skuAttrDesc: '',
            skuAttrEnable: 1,
          });
        }
        if (subAttrCode3 && subAttrName3) {
          const subRes3 = yield call(
            getGoodsSpuAttrSkuItemListByGoodsSpuAttrSku,
            wrapTid({ spuCode, skuAttrCode: subAttrCode3, skuAttrName: subAttrName3 })
          );
          const subSkuItemResult3 = requestResult(subRes3);
          // 组装新增商品sku属性数据,若存在sku属性项值下拉选取操作时,则key名称对应skuAttrItemList,此key名称由后台接口统一定义供前端界面使用
          goodsSkuAttrSkuResult.push({
            spuCode,
            skuCode,
            skuAttrCode: subAttrCode3,
            skuAttrName: subAttrName3,
            skuAttrItemList: subSkuItemResult3,
            skuAttrDesc: '',
            skuAttrEnable: 1,
          });
        }

        // 更新-当前商品分组-对应指定商品sku属性数据
        yield put({
          type: 'changeGoodsSkuAttrSkuUnderSkuGroupList',
          payload: goodsSkuAttrSkuResult,
        });
      }
    },
  },
};
