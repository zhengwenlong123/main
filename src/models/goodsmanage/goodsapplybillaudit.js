/**
 * [产品申请单审核] model层
 * 统一定义在model层中的callback的type值:1为增;2为删除;3更新;4为查询
 */

import {
  getGoodsApplyBillAuditList, // 获取申请单审核列表
  addGoodsApplyAudit, // 新增申请单审核记录
  getGoodsApplyAuditList, // 获取某个申请单申请单审核记录
} from '@/services/goodsmanage/goodsapplybillaudit';
import {
  getGoodsApplyBillList, // 根据条件查询产品申请单信息列表
} from '@/services/goodsmanage/goodsapplybillmanage';
import { requestResult, wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'goodsapplybillaudit',
  state: {
    goodAuditInfo: {}, // 申请单审核信息
    selectGoodsApplyInfo: {}, // 选择的申请单信息
    goodsApplyAuditList: {}, // 某个申请单申请单审核记录
    goodsApplyBillAuditList: [], // 申请单审核列表
    goodsApplyBillAuditListPageSize: 0, // 申请单审核列表总数量
    goodsApplyBillAwaitAuditList: [], // 申请单待审核列表
    goodsApplyBillAwaitAuditListPageSize: 0, // 申请单待审核列表总数量
  },
  reducers: {
    // 更新-产品申请单审核列表数据
    changeGoodsApplyBillAuditList(state, { payload }) {
      return {
        ...state,
        goodsApplyBillAuditList: payload.content || [],
        goodsApplyBillAuditListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-产品申请单待审核列表数据
    changeGoodsApplyBillAwaitAuditList(state, { payload }) {
      return {
        ...state,
        goodsApplyBillAwaitAuditList: payload.content || [],
        goodsApplyBillAwaitAuditListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-申请单审核信息
    changeGoodsAuditInfo(state, { payload }) {
      return {
        ...state,
        goodAuditInfo: payload || {},
      };
    },
    // 更新-选择的申请单信息
    changeSelectGoodsApplyInfo(state, { payload }) {
      return {
        ...state,
        selectGoodsApplyInfo: payload || {},
      };
    },
    // 更新 - 某个申请单申请单审核记录
    changeGoodsApplyAuditList(state, { payload }) {
      return {
        ...state,
        goodsApplyAuditList: payload || {},
      };
    },
  },
  effects: {
    // 获取-产品申请单审核列表(列表/分页)
    *paginationGoodsApplyBillAudit({ payload }, { call, put }) {
      const res = yield call(getGoodsApplyBillAuditList, {
        limit: payload.pageSize,
        page: payload.page - 1,
        searchCondition: { ...payload.searchCondition, tid: wrapTid().tid },
        sortCondition: '[{"property":"createAt","direction":"DESC"}]',
      });
      const result = requestResult(res);
      yield put({ type: 'changeGoodsApplyBillAuditList', payload: result });
    },
    // 获取-产品申请单待审核列表(列表/分页)
    *paginationGoodsApplyBillAwaitAudit({ payload }, { call, put }) {
      const res = yield call(getGoodsApplyBillList, {
        limit: payload.pageSize,
        page: payload.page - 1,
        searchCondition: { ...payload.searchCondition, tid: wrapTid().tid },
        sortCondition: '[{"property":"createAt","direction":"DESC"}]',
      });
      const result = requestResult(res);
      yield put({ type: 'changeGoodsApplyBillAwaitAuditList', payload: result });
    },
    // 新增申请单审核记录
    *addApplyAudit({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addGoodsApplyAudit, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'goodsapplybillmanage/paginationGoodsApplyBill',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 获取-某个申请单申请单审核记录
    *fetchGoodsApplyAuditList({ payload }, { call, put }) {
      const res = yield call(
        getGoodsApplyAuditList,
        wrapTid({
          ...payload,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsApplyAuditList', payload: result });
    },
  },
};
