/**
 * [物料序号信息] model层
 */

import paginationGoodsLastNo from '@/services/goodsmanage/goodslastnomanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'goodslastnomanage',
  state: {
    goodsLastNoList: [], // 商品序号控制列表
    goodsLastNoListPageSize: 0, // 商品序号控制列表总数量
  },
  reducers: {
    changeGoodsLastNoList(state, { payload }) {
      return {
        ...state,
        goodsLastNoList: payload.content,
        goodsLastNoListPageSize: Number(payload.total || 0),
      };
    },
  },
  effects: {
    *paginationGoodsLastNo({ payload }, { call, put }) {
      const res = yield call(
        paginationGoodsLastNo,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: { noType: 'MAT', ...payload.searchCondition },
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsLastNoList', payload: result });
    },
  },
};
