/* eslint-disable no-param-reassign */
/**
 * [属性名称管理] model层
 */

import {
  paginationGoodsAttrName,
  addGoodsAttrName,
  updateGoodsAttrName,
  getGoodsAttrNameAll,
} from '@/services/goodsmanage/goodsattrnamemanage';
import { requestResult, wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'goodsattrnamemanage',
  state: {
    goodsAttrNameList: [], // 属性名称列表
    goodsAttrNameListPageSize: 0, // 属性名称列表数量
    goodsAttrNameAll: [], // 属性名称数据所有(列表)
  },
  reducers: {
    // 更新-属性名称(列表)
    changeGoodsAttrNameList(state, { payload }) {
      return {
        ...state,
        goodsAttrNameList: payload.content,
        goodsAttrNameListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-属性名称数据所有
    changeGoodsAttrNameAll(state, { payload }) {
      return {
        ...state,
        goodsAttrNameAll: payload || [],
      };
    },
  },
  effects: {
    // 获取-属性名称(列表/分页)
    *paginationGoodsAttrName({ payload }, { call, put }) {
      const res = yield call(
        paginationGoodsAttrName,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsAttrNameList', payload: result });
    },
    // 新增-属性名称
    *addGoodsAttrName({ payload, callback, searchCondition = null }, { call, put }) {
      // 第1步：新增属性名称数据
      const res = yield call(addGoodsAttrName, wrapTid(payload)); // 添加属性名称数据
      callback(10, res !== undefined && res !== null && res.code === 0);
      // 第2步：重新获取属性名称数据
      yield put({
        type: 'paginationGoodsAttrName',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-属性名称
    *updateGoodsAttrName({ payload, callback, searchCondition = null }, { call, put }) {
      // 第1步：更新属性名称数据
      const res = yield call(updateGoodsAttrName, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      // 第3步：重新获取属性名称数据
      yield put({
        type: 'paginationGoodsAttrName',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },

    // 获取-属性名称数据所有(列表)
    *getGoodsAttrNameAll(_, { call, put }) {
      const res = yield call(getGoodsAttrNameAll, wrapTid({}));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsAttrNameAll', payload: result });
    },
  },
};
