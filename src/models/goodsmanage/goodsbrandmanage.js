/* eslint-disable no-unused-expressions */
/* eslint-disable no-param-reassign */
/**
 * [品牌管理] model层
 * 统一定义在model层中的callback的type值:1为增;2为删除;3更新;4为查询
 */

import {
  // 产品品牌
  getGoodsBrandAll,
  addGoodsBrand,
  updateGoodsBrand,
  updateGoodsBrandEnable,
  paginationGoodsBrand,

  // 产品品牌描述
  getCurrentSelectGoodsBrandDescByBrandCode,
  addGoodsBrandDesc,
  updateGoodsBrandDesc,
  deleteGoodsBrandDesc,
} from '@/services/goodsmanage/goodsbrandmanage';
import { requestResult, wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'goodsbrandmanage',
  state: {
    goodsBrandList: [], // 品牌列表
    goodsBrandListPageSize: 0, // 品牌列表总数量
    goodsBrandAll: [], // 品牌所有数据
    currentSelectGoodsBrandDesc: [], // 当前选择-产品品牌下品牌描述数据(列表)
    currentSelectGoodsBrandDescPageSize: 0, // 当前选择-产品品牌下品牌描述数据(列表)总数量
  },
  reducers: {
    // 更新-产品品牌列表数据
    changeGoodsBrandList(state, { payload }) {
      return {
        ...state,
        goodsBrandList: payload.content || [],
        goodsBrandListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-产品品牌数据(所有)
    changeGoodsBrandAll(state, { payload }) {
      return {
        ...state,
        goodsBrandAll: payload || [],
      };
    },
    // 更新-当前选择-产品品牌描述数据(列表)
    changeCurrentSelectGoodsBrandDesc(state, { payload }) {
      return {
        ...state,
        currentSelectGoodsBrandDesc: payload.content || [],
        currentSelectGoodsBrandDescPageSize: Number(payload.total || 0),
      };
    },
  },
  effects: {
    // 获取-产品品牌(列表/所有)
    *getGoodsBrandAll(_, { call, put }) {
      const res = yield call(getGoodsBrandAll, wrapTid({ brandEnable: 1 }));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsBrandAll', payload: result });
    },
    // 获取-产品品牌(列表/分页)
    *paginationGoodsBrand({ payload }, { call, put }) {
      const res = yield call(
        paginationGoodsBrand,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsBrandList', payload: result });
    },
    // 新增-产品品牌
    *addGoodsBrand({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addGoodsBrand, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsBrand',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-产品品牌
    *updateGoodsBrand({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateGoodsBrand, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsBrand',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-产品品牌状态(使用/停用)
    *updateGoodsBrandEnable({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateGoodsBrandEnable, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsBrand',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },

    // 获取-根据产品品牌获取-品牌描述数据(列表/分页)
    *paginationGoodsBrandDescByGoodsBrand({ payload }, { call, put }) {
      const { page, pageSize, brandCode } = payload;
      const res = yield call(
        getCurrentSelectGoodsBrandDescByBrandCode,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { brandCode },
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeCurrentSelectGoodsBrandDesc', payload: result });
    },
    // 新增-产品品牌描述
    *addGoodsBrandDesc({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsBrandDesc, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsBrandDescByGoodsBrand',
        payload: { brandCode: payload.brandCode, page: 1, pageSize: 10 },
      });
    },
    // 更新-产品品牌描述
    *updateGoodsBrandDesc({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsBrandDesc, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsBrandDescByGoodsBrand',
        payload: { brandCode: payload.brandCode, page: 1, pageSize: 10 },
      });
    },
    // 删除-产品品牌描述
    *deleteGoodsBrandDesc({ payload, callback }, { call, put }) {
      const res = yield call(deleteGoodsBrandDesc, payload.id);
      callback(20, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsBrandDescByGoodsBrand',
        payload: { brandCode: payload.brandCode, page: 1, pageSize: 10 },
      });
    },
  },
};
