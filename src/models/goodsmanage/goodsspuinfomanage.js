/**
 * [产品信息管理] model层
 */

import {
  // 产品信息
  paginationGoodsSpuInfo,
  addGoodsSpuInfo,
  updateGoodsSpuInfo,
  getSpuBeAdoptedStatus,
  // 产品普通属性
  paginationGoodsSpuAttrNormal,
  addGoodsSpuAttrNormal,
  updateGoodsSpuAttrNormal,
  // 产品SKU属性
  paginationGoodsSpuAttrSku,
  addGoodsSpuAttrSku,
  updateGoodsSpuAttrSku,
  getSpuCondWithGoodsSpuAttrSkuList,
  // 产品SKU属性项值
  paginationGoodsSpuAttrSkuItem,
  addGoodsSpuAttrSkuItem,
  updateGoodsSpuAttrSkuItem,
  // 产品描述
  paginationGoodsSpuDesc,
  addGoodsSpuDesc,
  updateGoodsSpuDesc,
  deleteGoodsSpuDesc,
  // 产品型号定义
  paginationGoodsSpuModel,
  addGoodsSpuModel,
  updateGoodsSpuModel,
  // 商品分组
  paginationGoodsSkuGroup,
  addGoodsSkuGroup,
  updateGoodsSkuGroup,
} from '@/services/goodsmanage/goodsspuinfomanage';
import { requestResult, wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'goodsspuinfomanage',
  state: {
    tableLoadingSpuInfoStatus: false, // 产品管理列表加载状态
    tableLoadingSpuAttrSkuItemStatus: false, // 产品SKU属性项管理列表加载状态
    goodsSpuInfoList: [], // 产品信息列表
    goodsSpuInfoListPageSize: 0, // 产品信息列表总数量
    goodsSpuAttrNormalList: [], // 产品普通属性数据(列表)
    goodsSpuAttrNormalListPageSize: 0, // 产品普通属性数据总数量
    goodsSpuAttrSkuList: [], // 产品SKU属性数据(列表)
    goodsSpuAttrSkuListPageSize: 0, // 产品SKU属性数据总数量
    goodsSpuAttrSkuItemList: [], // 产品SKU属性项值数据(列表)
    goodsSpuAttrSkuItemListPageSize: 0, // 产品SKU属性项值数据总数量
    goodsSpuModelList: [], // 产品型号定义数据(列表)
    goodsSpuModelListPageSize: 0, // 产品型号定义数据总数量
    goodsSpuDescList: [], // 产品描述数据(列表)
    goodsSpuDescListPageSize: 0, // 产品描述数据总数量
    goodsSkuGroupList: [], // 商品分组数据(列表)
    goodsSkuGroupListPageSize: 0, // 商品分组数据总数量
    spuCondWithGoodsSpuAttrSkuList: [], // 该产品下产品SKU属性数据(列表/所有)
    spuBeAdoptedStatus: false, // 该产品是否已被商品引用状态, false表示未被引用, true表示已被引用
  },
  reducers: {
    // 更新-产品管理列表加载状态
    changeTableLoadingSpuInfoStatus(state, { payload }) {
      return {
        ...state,
        tableLoadingSpuInfoStatus: payload || false,
      };
    },
    // 更新-产品SKU属性项管理列表加载状态
    changeTableLoadingSpuAttrSkuItemStatus(state, { payload }) {
      return {
        ...state,
        tableLoadingSpuAttrSkuItemStatus: payload || false,
      };
    },
    // 更新-产品信息列表
    changeGoodsSpuInfoList(state, { payload }) {
      return {
        ...state,
        goodsSpuInfoList: payload.content || [],
        goodsSpuInfoListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-产品普通属性(列表)
    changeGoodsSpuAttrNormalList(state, { payload }) {
      return {
        ...state,
        goodsSpuAttrNormalList: payload.content || [],
        goodsSpuAttrNormalListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-产品SKU属性(列表)
    changeGoodsSpuAttrSkuList(state, { payload }) {
      return {
        ...state,
        goodsSpuAttrSkuList: payload.content || [],
        goodsSpuAttrSkuListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-产品SKU属性项值(列表)
    changeGoodsSpuAttrSkuItemList(state, { payload }) {
      return {
        ...state,
        goodsSpuAttrSkuItemList: payload.content || [],
        goodsSpuAttrSkuItemListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-产品型号定义(列表)
    changeGoodsSpuModelList(state, { payload }) {
      return {
        ...state,
        goodsSpuModelList: payload.content || [],
        goodsSpuModelListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-产品描述(列表)
    changeGoodsSpuDescList(state, { payload }) {
      return {
        ...state,
        goodsSpuDescList: payload.content || [],
        goodsSpuDescListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-商品分组(列表)
    changeGoodsSkuGroupList(state, { payload }) {
      return {
        ...state,
        goodsSkuGroupList: payload.content || [],
        goodsSkuGroupListPageSize: Number(payload.total || 0),
      };
    },

    // 更新-该产品下产品SKU属性数据(列表/所有)
    changeSpuCondWithGoodsSpuAttrSkuList(state, { payload }) {
      return {
        ...state,
        spuCondWithGoodsSpuAttrSkuList: payload || [],
      };
    },
    // 更新-该产品是否已被商品引用状态
    changespuBeAdoptedStatus(state, { payload }) {
      return {
        ...state,
        spuBeAdoptedStatus: payload || false,
      };
    },
  },
  effects: {
    // 获取-产品信息(列表/分页)
    *paginationGoodsSpuInfo({ payload }, { call, put }) {
      // 设置当前产品管理列表加载状态为加载中
      yield put({ type: 'changeTableLoadingSpuInfoStatus', payload: true });
      // console.log(payload.searchCondition);
      const res = yield call(
        paginationGoodsSpuInfo,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSpuInfoList', payload: result });
      // 设置当前产品管理列表加载状态为加载完成
      yield put({ type: 'changeTableLoadingSpuInfoStatus', payload: false });
    },
    // 新增-产品信息
    *addGoodsSpuInfo({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addGoodsSpuInfo, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuInfo',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-产品信息
    *updateGoodsSpuInfo({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateGoodsSpuInfo, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuInfo',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },

    // 获取-产品普通属性(列表/分页)
    *paginationGoodsSpuAttrNormal(
      {
        payload: { spuCode, page, pageSize },
      },
      { call, put }
    ) {
      const res = yield call(
        paginationGoodsSpuAttrNormal,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { spuCode },
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSpuAttrNormalList', payload: result });
    },
    // 新增-产品普通属性
    *addGoodsSpuAttrNormal({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsSpuAttrNormal, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuAttrNormal',
        payload: { spuCode: payload.spuCode, page: 1, pageSize: 10 },
      });
    },
    // 更新-产品普通属性
    *updateGoodsSpuAttrNormal({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsSpuAttrNormal, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuAttrNormal',
        payload: { spuCode: payload.spuCode, page: 1, pageSize: 10 },
      });
    },

    // 获取-产品SKU属性(列表/分页)
    *paginationGoodsSpuAttrSku(
      {
        payload: { spuCode, page, pageSize },
      },
      { call, put }
    ) {
      const res = yield call(
        paginationGoodsSpuAttrSku,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { spuCode },
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSpuAttrSkuList', payload: result });
    },
    // 新增-产品SKU属性
    *addGoodsSpuAttrSku({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsSpuAttrSku, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuAttrSku',
        payload: { spuCode: payload.spuCode, page: 1, pageSize: 10 },
      });
    },
    // 更新-产品SKU属性
    *updateGoodsSpuAttrSku({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsSpuAttrSku, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuAttrSku',
        payload: { spuCode: payload.spuCode, page: 1, pageSize: 10 },
      });
    },

    // 获取-产品SKU属性项值(列表/分页)
    *paginationGoodsSpuAttrSkuItem(
      {
        payload: { spuCode, skuAttrCode, skuAttrName, page, pageSize },
      },
      { call, put }
    ) {
      // 设置加载进行中状态
      yield put({ type: 'changeTableLoadingSpuAttrSkuItemStatus', payload: true });
      const res = yield call(
        paginationGoodsSpuAttrSkuItem,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { spuCode, skuAttrCode, skuAttrName },
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSpuAttrSkuItemList', payload: result });
      // 设置加载完成状态
      yield put({ type: 'changeTableLoadingSpuAttrSkuItemStatus', payload: false });
    },
    // 新增-产品SKU属性项值
    *addGoodsSpuAttrSkuItem({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsSpuAttrSkuItem, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuAttrSkuItem',
        payload: {
          spuCode: payload.spuCode,
          skuAttrCode: payload.skuAttrCode,
          skuAttrName: payload.skuAttrName,
          page: 1,
          pageSize: 10,
        },
      });
    },
    // 更新-产品SKU属性项值
    *updateGoodsSpuAttrSkuItem({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsSpuAttrSkuItem, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuAttrSkuItem',
        payload: {
          spuCode: payload.spuCode,
          skuAttrCode: payload.skuAttrCode,
          skuAttrName: payload.skuAttrName,
          page: 1,
          pageSize: 10,
        },
      });
    },

    // 获取-产品型号定义(列表/分页)
    *paginationGoodsSpuModel(
      {
        payload: { spuCode, page, pageSize },
      },
      { call, put }
    ) {
      const res = yield call(
        paginationGoodsSpuModel,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { spuCode },
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSpuModelList', payload: result });
    },
    // 新增-产品型号定义
    *addGoodsSpuModel({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsSpuModel, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuModel',
        payload: { spuCode: payload.spuCode, page: 1, pageSize: 10 },
      });
    },
    // 更新-产品型号定义
    *updateGoodsSpuModel({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsSpuModel, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuModel',
        payload: { spuCode: payload.spuCode, page: 1, pageSize: 10 },
      });
    },

    // 获取-产品描述(列表/分页)
    *paginationGoodsSpuDesc(
      {
        payload: { spuCode, page, pageSize },
      },
      { call, put }
    ) {
      const res = yield call(
        paginationGoodsSpuDesc,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { spuCode },
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSpuDescList', payload: result });
    },
    // 新增-产品描述
    *addGoodsSpuDesc({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsSpuDesc, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuDesc',
        payload: { spuCode: payload.spuCode, pageSize: 10, page: 1 },
      });
    },
    // 更新-产品描述
    *updateGoodsSpuDesc({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsSpuDesc, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuDesc',
        payload: { spuCode: payload.spuCode, pageSize: 10, page: 1 },
      });
    },
    // 删除-当前选择-产品描述
    *deleteGoodsSpuDesc({ payload, callback }, { call, put }) {
      const res = yield call(deleteGoodsSpuDesc, payload.id);
      callback(20, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSpuDesc',
        payload: { spuCode: payload.spuCode, pageSize: 10, page: 1 },
      });
    },

    // 获取-商品分组(列表/分页)
    *paginationGoodsSkuGroup(
      {
        payload: { spuCode, page, pageSize },
      },
      { call, put }
    ) {
      const res = yield call(
        paginationGoodsSkuGroup,
        wrapTid({
          limit: pageSize,
          page: page - 1,
          searchCondition: { spuCode },
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsSkuGroupList', payload: result });
    },
    // 新增-商品分组
    *addGoodsSkuGroup({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsSkuGroup, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSkuGroup',
        payload: { spuCode: payload.spuCode, page: 1, pageSize: 10 },
      });
    },
    // 更新-商品分组
    *updateGoodsSkuGroup({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsSkuGroup, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsSkuGroup',
        payload: { spuCode: payload.spuCode, page: 1, pageSize: 10 },
      });
    },

    // 获取-该产品下产品SKU属性数据(列表/所有)
    *getSpuCondWithGoodsSpuAttrSkuList(
      {
        payload: { spuCode },
      },
      { call, put }
    ) {
      const res = yield call(getSpuCondWithGoodsSpuAttrSkuList, wrapTid({ spuCode }));
      const result = requestResult(res);
      yield put({ type: 'changeSpuCondWithGoodsSpuAttrSkuList', payload: result });
    },
    // 获取-该产品是否已被商品引用状态
    *getSpuBeAdoptedStatus(
      {
        payload: { spuCode },
      },
      { call, put }
    ) {
      const res = yield call(getSpuBeAdoptedStatus, wrapTid({ spuCode }));
      const result = requestResult(res);
      yield put({ type: 'changespuBeAdoptedStatus', payload: result.status });
    },
  },
};
