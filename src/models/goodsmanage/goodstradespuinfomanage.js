/**
 * [交易产品信息管理] model层
 */
import { formatMessage } from 'umi/locale';
import { message } from 'antd';
import { goodsTradeSpuInfoToDva, goodsTradeSkuInfoToDva } from '@/utils/goodsTradeSpuInfoToDva';
import {
  paginationGoodsTradeSpuInfo,
  getGoodsTradeSpuInfo,
  updateGoodsTradeSpuInfo,
  addGoodsTradeSpuCommonAttr,
  modifyGoodsTradeSpuCommonAttr,
  deleteGoodsTradeSpuCommonAttr,
  addGoodsTradeSpuDesc,
  modifyGoodsTradeSpuDesc,
  deleteGoodsTradeSpuDesc,
  // 商品接口
  getGoodsSkuInfoEnhance,
  updateGoodsTradeSkuInfo,
  addGoodsTradeSkuCommonAttr,
  modifyGoodsTradeSkuCommonAttr,
  deleteGoodsTradeSkuCommonAttr,
  addGoodsTradeSkuDesc,
  modifyGoodsTradeSkuDesc,
  deleteGoodsTradeSkuDesc,
  modifyGoodsMaterielInfo, // 修改物料
} from '@/services/goodsmanage/goodstradespuinfomanage';
import { requestResult, wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'goodstradespuinfomanage',
  state: {
    goodsTradeSpuInfoList: [], // 产品信息列表
    goodsTradeSpuInfoListPageSize: 0, // 产品信息列表总数量
    goodsTradeSpuInfo: {}, // 产品信息
    goodsTradeSpuBasicInfo: {}, // 产品基本信息
    goodsTradeSpuBasicInfoValid: {}, // 产品基本信息（正式生效数据）
    goodsTradeSpuSkuInfo: [], // 产品SKU信息
    goodsTradeSpuCommonAttr: [], // 产品普通属性
    goodsTradeSpuModel: [], // 产品型号
    goodsTradeSpuDesc: [], // 产品描述
    goodsTradeSpuSkuGroupList: [], // 商品分组
    goodsTradeSpuSkuList: [], // 商品列表
    goodsTradeSpuMaterielList: [], // 物料数据
    goodsTradeSpuSKuGroupAttr: {}, // 产品商品分组从属属性
    goodsTradeSpuSkuAttrListAble: [], // 可用的属性列表数据
    goodsTradeSpuSkuAttrListOutSku: [], // 产品sku属性下拉列表数据（仅除去sku信息定义的sku属性之前）
    goodsTradeSkuInfo: {}, // 商品详情（当前）
    goodsTradeSkuBasicInfo: {}, // 商品基本信息
    goodsTradeSkuBasicInfoValid: {}, // 商品基本信息（正式生效数据）
    goodsTradeSkuAttrInfo: [], // 商品SKU信息
    goodsTradeSkuCommonAttr: [], // 商品普通属性
    goodsTradeSkuDesc: [], // 产品描述
  },
  reducers: {
    // 更新-产品信息(列表/分页)
    changeGoodsTradeSpuInfoList(state, { payload }) {
      return {
        ...state,
        goodsTradeSpuInfoList: payload.content || [],
        goodsTradeSpuInfoListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-产品信息
    changeGoodsTradeSpuInfo(state, { payload, dataSource }) {
      const val = goodsTradeSpuInfoToDva(payload, dataSource);
      return {
        ...state,
        goodsTradeSpuInfo: payload || {},
        ...val,
      };
    },
    // 更新-产品基本信息
    changeGoodsTradeSpuBasicInfo(state, { payload }) {
      return {
        ...state,
        goodsTradeSpuBasicInfo: payload || {},
      };
    },
    // 更新-产品普通属性
    changeGoodsTradeSpuCommonAttr(state, { payload }) {
      const { goodsTradeSpuSkuAttrListOutSku } = state;
      const usedCommonAttr = (payload && payload.map(v => v.attr && v.attr.key)) || [];
      const goodsTradeSpuSkuAttrListAble = [...goodsTradeSpuSkuAttrListOutSku].filter(
        v => !usedCommonAttr.includes(v.attrCode)
      );
      return {
        ...state,
        goodsTradeSpuCommonAttr: payload || {},
        goodsTradeSpuSkuAttrListAble,
      };
    },
    // 更新-产品描述
    changeGoodsTradeSpuDesc(state, { payload }) {
      return {
        ...state,
        goodsTradeSpuDesc: payload || {},
      };
    },
    // 更新-商品详情
    changeGoodsTradeSkuInfo(state, { payload }) {
      const val = goodsTradeSkuInfoToDva(payload);
      return {
        ...state,
        goodsTradeSkuInfo: payload || {},
        ...val,
      };
    },
    // 更新-商品基本信息
    changeGoodsTradeSkuBasicInfo(state, { payload }) {
      return {
        ...state,
        goodsTradeSkuBasicInfo: payload || {},
      };
    },
    // 更新-商品列表
    changeGoodsTradeSpuSkuList(state, { payload }) {
      return {
        ...state,
        goodsTradeSpuSkuList: payload || {},
      };
    },
  },
  effects: {
    // 获取-产品信息(列表/分页)
    *paginationGoodsTradeSpuInfo({ payload }, { call, put }) {
      const res = yield call(
        paginationGoodsTradeSpuInfo,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsTradeSpuInfoList', payload: result });
    },
    // 更新-产品信息
    *updateGoodsSpuInfo({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateGoodsTradeSpuInfo, wrapTid(payload));
      if (res !== undefined && res !== null && res.code === 0) {
        callback(3, true);
        yield put({
          type: 'paginationGoodsTradeSpuInfo',
          payload: { page: 1, pageSize: 10, searchCondition },
        });
      } else {
        callback(3, false);
      }
    },
    // 获取-产品信息（详情）
    *fetchGoodsTradeSpuInfo({ payload }, { call, put, select }) {
      const res = yield call(getGoodsTradeSpuInfo, { ...payload, tid: wrapTid().tid });
      const result = requestResult(res);
      const goodsCatagoryListAll = yield select(
        // 产品类目
        state => state.goodsapplybillmanage.goodsCatagoryListAll || []
      );
      const goodsSpuSkuAttrListAll = yield select(
        // sku属性下来列表数据（所有）
        state => state.goodsapplybillmanage.goodsAttrNameAll || []
      );
      yield put({
        type: 'changeGoodsTradeSpuInfo',
        payload: result,
        dataSource: { goodsCatagoryListAll, goodsSpuSkuAttrListAll },
      });
    },
    // 修改-产品基本信息
    *modifyGoodsTradeSpuBasicInfo({ payload, callback }, { call, select }) {
      const goodsTradeSpuBasicInfo = yield select(
        state => state.goodstradespuinfomanage.goodsTradeSpuBasicInfo
      );
      const res = yield call(updateGoodsTradeSpuInfo, {
        ...goodsTradeSpuBasicInfo,
        ...payload,
        tid: wrapTid().tid,
      });
      callback(res);
    },
    // 新增-产品普通属性
    *addGoodsTradeSpuCommonAttr({ payload, callback }, { call, select }) {
      const goodsTradeSpuBasicInfo = yield select(
        state => state.goodstradespuinfomanage.goodsTradeSpuBasicInfo
      );
      const { attrCode, attrName, attrDesc, attrValue } = payload;
      const res = yield call(addGoodsTradeSpuCommonAttr, {
        attrCode,
        attrName,
        attrDesc,
        attrValue,
        spuCode: goodsTradeSpuBasicInfo.spuCode, // 产品代码
        attrEnable: 1, // 有效值
        tid: wrapTid().tid,
      });
      callback(res);
    },
    // 修改-产品普通属性
    *modifyGoodsTradeSpuCommonAttr({ payload, callback }, { call }) {
      const res = yield call(modifyGoodsTradeSpuCommonAttr, {
        ...payload,
        tid: wrapTid().tid,
      });
      callback(res);
    },
    // 删除-产品普通属性
    *deleteGoodsTradeSpuCommonAttr({ payload, callback }, { call }) {
      const idArray = payload.join(',');
      const res = yield call(deleteGoodsTradeSpuCommonAttr, idArray);
      if (res !== undefined && res !== null && res.code === 0) {
        callback(res);
      }
    },
    // 新增-产品描述
    *addGoodsTradeSpuDesc({ payload, callback }, { call, select }) {
      const goodsTradeSpuBasicInfo = yield select(
        state => state.goodstradespuinfomanage.goodsTradeSpuBasicInfo
      );
      const res = yield call(
        addGoodsTradeSpuDesc,
        wrapTid({
          ...payload,
          descNotes: payload.descName, // 描述备注
          spuCode: goodsTradeSpuBasicInfo.spuCode, // 产品代码
          descEnable: 1, // 有效值
        })
      );
      callback(res);
    },
    // 修改-产品描述
    *modifyGoodsTradeSpuDesc({ payload, callback }, { call }) {
      const res = yield call(
        modifyGoodsTradeSpuDesc,
        wrapTid({
          ...payload,
          descNotes: payload.descName,
        })
      );
      callback(res);
    },
    // 删除-产品描述
    *deleteGoodsTradeSpuDesc({ payload, callback }, { call }) {
      const idArray = payload.join(',');
      const res = yield call(deleteGoodsTradeSpuDesc, idArray);
      if (res !== undefined && res !== null && res.code === 0) {
        callback(res);
      }
    },

    /**
     * 商品相关接口
     */
    // 获取-商品详情
    *getGoodsSkuInfoEnhance({ payload }, { call, put }) {
      const res = yield call(getGoodsSkuInfoEnhance, { ...payload, tid: wrapTid().tid });
      const result = requestResult(res);
      yield put({ type: 'changeGoodsTradeSkuInfo', payload: result });
    },
    // 修改-产品基本信息
    *modifyGoodsTradeSkuBasicInfo({ payload, callback }, { call, select }) {
      const goodsTradeSkuBasicInfo = yield select(
        state => state.goodstradespuinfomanage.goodsTradeSkuBasicInfo
      );
      const res = yield call(updateGoodsTradeSkuInfo, {
        ...goodsTradeSkuBasicInfo,
        ...payload,
        tid: wrapTid().tid,
      });
      callback(res);
    },
    // 新增-产品普通属性
    *addGoodsTradeSkuCommonAttr({ payload, callback }, { call, select }) {
      const goodsTradeSkuBasicInfo = yield select(
        state => state.goodstradespuinfomanage.goodsTradeSkuBasicInfo
      );
      const goodsTradeSpuBasicInfo = yield select(
        state => state.goodstradespuinfomanage.goodsTradeSpuBasicInfo
      );
      const { attrCode, attrName, attrDesc, attrValue } = payload;
      const res = yield call(addGoodsTradeSkuCommonAttr, {
        attrCode,
        attrName,
        attrDesc,
        attrValue,
        skuCode: goodsTradeSkuBasicInfo.skuCode, // 商品代码
        spuCode: goodsTradeSpuBasicInfo.spuCode, // 产品代码
        attrEnable: 1, // 有效值
        attrRefType: 0, // 属性引用类型
        tid: wrapTid().tid,
      });
      callback(res);
    },
    // 修改-产品普通属性
    *modifyGoodsTradeSkuCommonAttr({ payload, callback }, { call }) {
      const res = yield call(modifyGoodsTradeSkuCommonAttr, {
        ...payload,
        tid: wrapTid().tid,
      });
      callback(res);
    },
    // 删除-产品普通属性
    *deleteGoodsTradeSkuCommonAttr({ payload, callback }, { call }) {
      const idArray = payload.join(',');
      const res = yield call(deleteGoodsTradeSkuCommonAttr, idArray);
      if (res !== undefined && res !== null && res.code === 0) {
        callback(res);
      }
    },
    // 新增-商品描述
    *addGoodsTradeSkuDesc({ payload, callback }, { call, select }) {
      const goodsTradeSpuBasicInfo = yield select(
        state => state.goodstradespuinfomanage.goodsTradeSpuBasicInfo
      );
      const goodsTradeSkuBasicInfo = yield select(
        state => state.goodstradespuinfomanage.goodsTradeSkuBasicInfo
      );
      const res = yield call(
        addGoodsTradeSkuDesc,
        wrapTid({
          ...payload,
          descNotes: payload.descName, // 描述备注
          spuCode: goodsTradeSpuBasicInfo.spuCode, // 产品代码
          skuCode: goodsTradeSkuBasicInfo.skuCode, // 商品代码
          descEnable: 1, // 有效值
        })
      );
      callback(res);
    },
    // 修改-商品状态
    *modifyGoodsTradeSkuStatus({ payload }, { call, select, put }) {
      const goodsTradeSpuSkuList = yield select(
        state => state.goodstradespuinfomanage.goodsTradeSpuSkuList
      );
      const res = yield call(
        updateGoodsTradeSkuInfo,
        wrapTid({
          ...payload,
        })
      );
      if (res !== undefined && res !== null && res.code === 0) {
        message.success('操作成功');
        const findIndex = goodsTradeSpuSkuList.findIndex(v => v.skuCode === payload.skuCode);
        yield put({
          type: 'changeGoodsTradeSpuSkuList',
          payload: goodsTradeSpuSkuList.splice(findIndex, 1, { ...res.data }),
        });
      } else {
        message.error((res && res.message) || formatMessage({ id: 'form.common.optionError' }));
      }
    },
    // 修改-商品描述
    *modifyGoodsTradeSkuDesc({ payload, callback }, { call }) {
      const res = yield call(
        modifyGoodsTradeSkuDesc,
        wrapTid({
          ...payload,
          descNotes: payload.descName,
        })
      );
      callback(res);
    },
    // 删除-商品描述
    *deleteGoodsTradeSkuDesc({ payload, callback }, { call }) {
      const idArray = payload.join(',');
      const res = yield call(deleteGoodsTradeSkuDesc, idArray);
      if (res !== undefined && res !== null && res.code === 0) {
        callback(res);
      }
    },
    // 修改-物料
    *modifyGoodsMaterielInfo({ payload, callback }, { call }) {
      const res = yield call(modifyGoodsMaterielInfo, {
        ...payload,
        tid: wrapTid().tid,
      });
      callback(res);
    },
  },
};
