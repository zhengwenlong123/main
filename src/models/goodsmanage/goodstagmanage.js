/**
 * [标签管理] model层
 */

import {
  // 标签类型
  getGoodsTagType,
  addGoodsTagType,
  updateGoodsTagType,
  updateGoodsTagTypeEnable,
  paginationGoodsTagType,
  deleteGoodsTagType,

  // 标签值定义
  getCurrentSelectGoodsTagDefineAll,
  addGoodsTagDefine,
  updateGoodsTagDefine,
  deleteGoodsTagDefine,
} from '@/services/goodsmanage/goodstagmanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'goodstagmanage',
  state: {
    goodsTagTypeList: [], // 标签类型列表
    goodsTagTypeListPageSize: 0, // 标签类型列表总数量
    currentSelectGoodsTagDefineAll: [], // 当前选择-标签类型下标签值定义数据(所有)
  },
  reducers: {
    // 更新-标签类型(列表)
    changeGoodsTagTypeList(state, { payload }) {
      return {
        ...state,
        goodsTagTypeList: payload.content,
        goodsTagTypeListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-当前选择-标签类型下标签值定义数据(所有)
    changeCurrentSelectGoodsTagDefineAll(state, { payload }) {
      return {
        ...state,
        currentSelectGoodsTagDefineAll: payload || [],
      };
    },
  },
  effects: {
    // 获取-标签类型(列表)
    *getGoodsTagType(_, { call, put }) {
      const res = yield call(
        getGoodsTagType,
        wrapTid({
          limit: 10,
          page: 0,
          searchCondition: {},
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsTagTypeList', payload: result });
    },
    // 新增-标签类型
    *addGoodsTagType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addGoodsTagType, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsTagType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-标签类型
    *updateGoodsTagType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateGoodsTagType, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsTagType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-标签类型状态(使用/停用)
    *updateGoodsTagTypeEnable({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateGoodsTagTypeEnable, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsTagType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 删除-标签类型
    *deleteGoodsTagType({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(deleteGoodsTagType, wrapTid(payload));
      callback(20, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsTagType',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 分页-标签类型(表格)
    *paginationGoodsTagType({ payload }, { call, put }) {
      const res = yield call(
        paginationGoodsTagType,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsTagTypeList', payload: result });
    },

    // 获取-当前选择-标签类型下标签值定义数据(所有)
    *getCurrentSelectGoodsTagDefineAll(
      {
        payload: { tagTypeCode },
      },
      { call, put }
    ) {
      const res = yield call(getCurrentSelectGoodsTagDefineAll, wrapTid({ tagTypeCode }));
      const result = requestResult(res);
      yield put({ type: 'changeCurrentSelectGoodsTagDefineAll', payload: result });
    },
    // 新增-标签值定义
    *addGoodsTagDefine({ payload, callback }, { call, put }) {
      const res = yield call(addGoodsTagDefine, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'getCurrentSelectGoodsTagDefineAll',
        payload: { tagTypeCode: payload.tagTypeCode },
      });
    },
    // 更新-标签值定义
    *updateGoodsTagDefine({ payload, callback }, { call, put }) {
      const res = yield call(updateGoodsTagDefine, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'getCurrentSelectGoodsTagDefineAll',
        payload: { tagTypeCode: payload.tagTypeCode },
      });
    },
    // 删除-标签值定义
    *deleteGoodsTagDefine({ payload, callback }, { call, put }) {
      const res = yield call(deleteGoodsTagDefine, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({ type: 'getCurrentSelectGoodsTagDefineAll', payload: payload.tagTypeCode });
    },
  },
};
