/* eslint-disable no-unused-vars */
/* eslint-disable no-lone-blocks */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-param-reassign */
/**
 * [产品类目管理] model层
 */

import {
  getCurrentSelectCatagoryParentNode,
  getCurrentSelectCatagoryChildNode,
  getGoodsCatagoryWith3LevelAll,
  addGoodsCatagory,
  updateGoodsCatagory,
  deleteGoodsCatagory,
  // getGoodsCatagoryAttrCollectionByGoodsCategory,
  // addGoodsCatagoryAttrCollectionByGoodsCategory,
  // updateGoodsCatagoryAttrCollectionByGoodsCategory,
  // deleteGoodsCatagoryAttrCollectionByGoodsCategory,
  updateGoodsCatagoryEnable,
} from '@/services/goodsmanage/goodscatagorymanage';
import { wrapTid, requestResult } from '@/utils/mdcutil';
import { cloneDeep } from 'lodash'; // eslint-disable-line

export default {
  namespace: 'goodscatagorymanage',
  state: {
    goodsCatagoryRootNode: [], // 产品类目-根节点数据(列表所有)
    currentSelectCatagory: null, // 当前选择-类目对象
    currentSelectParentCatagory: null, // 当前选择-类目父对象
    goodsCatagoryWith3LevelAll: [], // 产品类目1级、2级、3级数据(树形结构/所有)
    // currentSelectCatagoryAttrCollect: null, // 当前选择-类目属性集对象, 需求：每个类目只可以有一个属性集
  },
  reducers: {
    // 更新-产品类目-根节点数据所有
    changeGoodsCatagoryRootNode(state, { payload }) {
      return {
        ...state,
        goodsCatagoryRootNode: payload || [],
      };
    },
    // 更新-产品类目数据-(本地计算添加更新)
    changeLocalCalculationGoodsCatagory(state, { payload }) {
      const { newObj, newDataArray } = payload;
      const prevOldValue = cloneDeep(state.goodsCatagoryRootNode);
      const findObj = data =>
        data.forEach(item => {
          if (item.catagoryCode === newObj.parentCode) {
            // 找到了对应节点, 直接更新节点即可
            item.childrens = newDataArray;
          } else if (item.childrens && item.childrens.length > 0) {
            findObj(item.childrens);
          }
        });
      findObj(prevOldValue);

      return {
        ...state,
        goodsCatagoryRootNode: prevOldValue,
      };
    },
    // 更新-当前选择类目数据
    changeCurrentSelectCatagory(
      state,
      {
        payload: { payload, parentPayload },
      }
    ) {
      return {
        ...state,
        currentSelectCatagory: payload || null,
        currentSelectParentCatagory: parentPayload || null,
      };
    },
    // 更新-产品类目1级、2级、3级数据(树形结构/所有)
    changeGoodsCatagoryWith3LevelAll(state, { payload }) {
      return {
        ...state,
        goodsCatagoryWith3LevelAll: payload || [],
      };
    },
    // 更新-当前选择类目属性集数据,
    // 需求：每个类目只可以有一个属性集
    // changeCurrentSelectCatagoryAttrCollect(state, { payload }) {
    //   return {
    //     ...state,
    //     currentSelectCatagoryAttrCollect: payload,
    //   };
    // },
  },
  effects: {
    // 产品类目-根节点数据所有(仅catagoryLevel为0,且parentCode为_ROOT_数据)
    *getGoodsCatagoryRootNode(_, { call, put }) {
      // 加载大类
      const res = yield call(
        getCurrentSelectCatagoryChildNode,
        wrapTid({ parentCode: '_ROOT_', catagoryLevel: 0 })
      );
      const result = requestResult(res);
      // 加载一级分类
      const res2 = yield call(
        getCurrentSelectCatagoryChildNode,
        wrapTid({
          parentCode: result[0].catagoryCode,
          catagoryLevel: 1,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result2 = requestResult(res2);
      // 将一级分类数据导入大类
      result[0].childrens = result2 || [];
      yield put({ type: 'changeGoodsCatagoryRootNode', payload: result });
      yield put({
        type: 'changeCurrentSelectCatagory',
        payload: { payload: result[0], parentPayload: null },
      });
    },
    // 获取-产品类目1级、2级、3级数据(树形结构/所有),需求：其中产品必须挂在3级类目上(需要做校验)
    *getGoodsCatagoryWith3LevelAll(_, { call, put }) {
      const res = yield call(getGoodsCatagoryWith3LevelAll, wrapTid({}));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsCatagoryWith3LevelAll', payload: result });
    },
    // 新增-产品类目
    *addGoodsCatagory(
      {
        payload: { params, parentCatagory, callback },
      },
      { call, put }
    ) {
      // 第1步：新增产品类目数据入库
      const res = yield call(addGoodsCatagory, wrapTid(params));
      const resNewAdd = requestResult(res);
      callback(2, res !== undefined && res !== null && res.code === 0);

      {
        /* // 第2步：新增产品类目-对应属性集数据入库
      const {goodsCatagoryAttrCollectionObj:{attrCollectionCode}} = params; // goodsCatagoryAttrCollectionObj是类目属性集对象
      if(attrCollectionCode){
        // 存在表示有选择值则新增类目属性集对象
        yield call(addGoodsCatagoryAttrCollectionByGoodsCategory, wrapTid({catagoryCode:resNewAdd.catagoryCode, attrCollectionCode}));

        // $此处特别说明:因新增了类目对象,在页面左边树结构上需要对当前选择类目对象及当前选择类目父对象设置最新值,所以会执行下面的程序$
        // 1、本次新增因库中数据新增,所以也要更新本地-当前选择类目对象值及当前类目父级对象值-此处对应到父级节点
        const resParentNode = yield call(getCurrentSelectCatagoryParentNode, wrapTid({catagoryCode: parentCatagory.parentCode})); // 获取父级类目对象
        yield put({type: 'changeCurrentSelectCatagory', payload: {payload: parentCatagory, parentPayload: resParentNode.data}});
        // 2、本次新增因库中数据新增,所以也要更新本地-当前选择类目对应属性集对象值-此处对应到父级节点
        yield put({type: 'getGoodsCatagoryAttrCollectionByGoodsCategory', payload: {catagoryCode: parentCatagory.catagoryCode}});
      } */
      }

      // 第3步：获取当前节点下子节点列表数据,去更新state当中的goodsCatagoryRootNode值
      if (!res) return;
      const results = requestResult(res);
      const res2 = yield call(
        getCurrentSelectCatagoryChildNode,
        wrapTid({
          parentCode: results.parentCode,
          catagoryLevel: results.catagoryLevel,
        })
      );
      const results2 = requestResult(res2);
      yield put({
        type: 'changeLocalCalculationGoodsCatagory',
        payload: { newObj: results, newDataArray: results2 },
      });
      yield put({
        type: 'changeCurrentSelectCatagory',
        payload: { newObj: results, newDataArray: results2 },
      });
    },
    // 更新-产品类目
    *updateGoodsCatagory(
      {
        payload: { params, callback },
      },
      { call, put }
    ) {
      // 第1步：更新产品类目数据入库
      const res = yield call(updateGoodsCatagory, params);
      callback(1, res !== undefined && res !== null && res.code === 0);

      {
        /* // 第2步：更新产品类目-对应属性集数据入库
      const {goodsCatagoryAttrCollectionObj:{id, catagoryCode, attrCollectionCode}} = params; // goodsCatagoryAttrCollectionObj是类目属性集对象
      if(id && catagoryCode && attrCollectionCode){
        // 更新-类目属性集对象
        const resUpdate = yield call(updateGoodsCatagoryAttrCollectionByGoodsCategory, wrapTid({id, catagoryCode, attrCollectionCode}));
        // 本次更新因库中数据改变,所以也要更新本地-当前选择类目对应属性集对象值
        yield put({type: 'changeCurrentSelectCatagoryAttrCollect', payload: resUpdate.data});
      }else if(id && (!attrCollectionCode || !catagoryCode)){
        // 删除-类目属性集对象
        yield call(deleteGoodsCatagoryAttrCollectionByGoodsCategory, id);
        // 本次更新因数据不完整删除了库的数据,所有也要更新本地-当前选择类目对应属性集对象值为null
        yield put({type: 'changeCurrentSelectCatagoryAttrCollect', payload: null});
      }else{
        // 新增-类目属性集对象
        const resAdd = yield call(addGoodsCatagoryAttrCollectionByGoodsCategory, wrapTid({catagoryCode, attrCollectionCode}));
        // 本次更新因库中数据新增,所以也要更新本地-当前选择类目对应属性集对象值
        const tempResult = requestResult(resAdd);
        yield put({type: 'changeCurrentSelectCatagoryAttrCollect', payload: tempResult});
      } */
      }

      // 第3步：获取当前节点下子节点列表数据,去更新state当中的goodsCatagoryRootNode值
      const results = requestResult(res);
      const res2 = yield call(
        getCurrentSelectCatagoryChildNode,
        wrapTid({
          parentCode: results.parentCode,
          catagoryLevel: results.catagoryLevel,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const results2 = requestResult(res2);
      yield put({
        type: 'changeLocalCalculationGoodsCatagory',
        payload: {
          payload: results,
          parentPayload: { parentCode: params.parentCode, parentName: params.parentName },
        },
      });
    },
    // 删除-产品类目(含子目录)
    *deleteGoodsCatagory(
      {
        payload: { params, callback },
      },
      { call, put }
    ) {
      // 第1步：删除产品类目数据
      const res = yield call(deleteGoodsCatagory, params.id);
      callback(3, res !== undefined && res !== null && res.code === 0);

      {
        /* // 第2步：删除产品类目-对应属性集数据
      const {goodsCatagoryAttrCollectionObj} = params; // goodsCatagoryAttrCollectionObj是类目属性集对象
      if(goodsCatagoryAttrCollectionObj){
        // 存在表示有值则删除类目属性集对象
        yield call(deleteGoodsCatagoryAttrCollectionByGoodsCategory, goodsCatagoryAttrCollectionObj.id);
        // 本次更新因数据不完整删除了库的数据,所有也要更新本地-当前选择类目对应属性集对象值为null
        yield put({type: 'changeCurrentSelectCatagoryAttrCollect', payload: null});
      } */
      }

      // 第3步：获取当前节点下子节点列表数据,去更新state当中的goodsCatagoryRootNode值
      if (!res) return;
      const res2 = yield call(
        getCurrentSelectCatagoryChildNode,
        wrapTid({
          parentCode: params.parentCode,
          catagoryLevel: params.catagoryLevel,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const results = requestResult(res2);
      yield put({
        type: 'changeLocalCalculationGoodsCatagory',
        payload: { newObj: params, newDataArray: results },
      });
    },
    // 获取-当前选择产品类目-对应属性集数据,需求：每个类目只可以有一个属性集
    // *getGoodsCatagoryAttrCollectionByGoodsCategory({ payload }, { call, put }) {
    //   const res = yield call(getGoodsCatagoryAttrCollectionByGoodsCategory, wrapTid(payload));
    //   const result = requestResult(res);
    //   // 需求：每个类目只可以有一个属性集
    //   let goodsCatagoryAttrCollection = null;
    //   if(result instanceof Array){ // 目前接口返回的数据形式是数组结构
    //     if(result.length > 0) goodsCatagoryAttrCollection = result[0];
    //   }
    //   yield put({ type: 'changeCurrentSelectCatagoryAttrCollect', payload: goodsCatagoryAttrCollection });
    // },

    // 更新-产品类目状态(使用/停用)
    *updateGoodsCatagoryEnable({ payload }, { call }) {
      yield call(updateGoodsCatagoryEnable, payload);
    },
  },
};
