/**
 * [产品大类管理] model层
 */

import {
  getGoodsClassificationAll,
  paginationGoodsClassification,
  addGoodsClassification,
  updateGoodsClassification,
  updatedGoodsClassificationStatus,
} from '@/services/goodsmanage/goodsclassificationmanage';
import { requestResult, wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'goodsclassificationmanage',
  state: {
    goodsClassificationList: [], // 产品大类列表
    goodsClassificationListPageSize: 0, // 产品大类列表总数量
    goodsClassificationAll: [], // 产品大类所有数据
  },
  reducers: {
    // 更新-产品大类(所有)
    changeGoodsClassificationAll(state, { payload }) {
      return {
        ...state,
        goodsClassificationAll: payload,
      };
    },
    // 更新-产品大类(列表)
    changeGoodsClassificationList(state, { payload }) {
      return {
        ...state,
        goodsClassificationList: payload.content,
        goodsClassificationListPageSize: Number(payload.total || 0),
      };
    },
  },
  effects: {
    // 获取-产品大类(所有)
    *getGoodsClassificationAll(_, { call, put }) {
      const res = yield call(getGoodsClassificationAll, wrapTid({ enable: 1 }));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsClassificationAll', payload: result });
    },
    // 获取-产品大类(列表/分页)
    *paginationGoodsClassification({ payload }, { call, put }) {
      const res = yield call(
        paginationGoodsClassification,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt", "direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsClassificationList', payload: result });
    },
    // 新增-产品大类
    *addGoodsClassification({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(addGoodsClassification, wrapTid(payload));
      callback(10, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsClassification',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    // 更新-产品大类
    *updateGoodsClassification({ payload, callback, searchCondition = null }, { call, put }) {
      const res = yield call(updateGoodsClassification, wrapTid(payload));
      callback(2, res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsClassification',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
    *updatedGoodsClassificationStatus({ payload, callback, searchCondition }, { call, put }) {
      const res = yield call(updatedGoodsClassificationStatus, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({
        type: 'paginationGoodsClassification',
        payload: { page: 1, pageSize: 10, searchCondition },
      });
    },
  },
};
