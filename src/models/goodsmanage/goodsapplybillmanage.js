/**
 * [产品申请单管理] model层
 * 统一定义在model层中的callback的type值:1为增;2为删除;3更新;4为查询
 */
import moment from 'moment';
import uuid from 'uuid';
import {
  addGoodsApplyBill, // 新增申请单
  getGoodsApplyBill, // 获取申请单信息
  editGoodsApplyBill, // 修改申请单信息
  getGoodsApplyBillList, // 根据条件查询产品申请单信息列表
  getGoodsSpuInfoAll, // 获取已生效产品所有数据
  getGoodsCatagoryListAll, // 获取产品类目所有数据
  getGoodsClassificationListAll, // 获取产品大类所有数据
  getGoodsbrandListAll, // 产品品牌
  getGoodsAttrNameAll, // 属性名称（所有）
  verifyUniqueBySpuName, // 验证产品名称数据唯一性
  getGoodsebscatagoryListAll, // 获取ebs分类
  deleteGoodsRfInfo, // 删除申请单
} from '@/services/goodsmanage/goodsapplybillmanage';
import { addGoodsApplyAudit } from '@/services/goodsmanage/goodsapplybillaudit';
import { requestResult, wrapTid } from '@/utils/mdcutil';
// import applyBillJson from '@/utils/applyBill.json';
import { getTreeParent, formatMaterielData } from '@/utils/utils';

export default {
  namespace: 'goodsapplybillmanage',
  state: {
    goodsApplyBillList: [], // 申请单列表
    goodsApplyBillListPageSize: 0, // 申请单列表总数量
    goodsApplyBillDetailInfo: {}, // 申请单详细信息
    goodsRfInfo: {}, // 申请单选择类型信息
    validProductAll: [], // 系统已经生效的所有产品数据
    goodsCatagoryListAll: [], // 产品类目
    goodsClassificationListAll: [], // 产品类目
    goodsbrandListAll: [], // 产品品牌
    goodsAttrNameAll: [], // 属性名称（所有）
    goodsAttrNameAble: [], // 属性名称 （可用的）
    goodsRfSpuInfo: {}, // 申请单产品基本信息
    goodsSkuAttrUsed: [], // sku属性(sku信息中已设置好的属性内容)
    goodsSkuAttrUsedChangeId: uuid.v4(), // 标志goodsSkuAttrUsed发生改变的时候都会给一个唯一的值
    goodsSkuAttrUsedChangeClear: uuid.v4(), // sku属性更改需要清空商品、物料标记
    goodsRfSpuAttrNormalList: [], // 申请单产品普通属性
    goodsRfSpuDescList: [], // 申请单产品描述数据
    goodsSpuModellist: [], // 产品型号数据
    goodsSpuModelChangeClear: uuid.v4(), // 产品型号发生改变需要清空相关的商品及物料数据标志
    goodsGrouplist: [], // 商品分组列表总数据
    goodsSkulist: [], // 商品列表数据(新建)
    goodsSkulistValid: [], // 商品列表数据(已生效)
    goodsMaterielList: [], // 物料列表数据
    goodsRfSpuSubAttrInfo: {}, // 商品分组从属属性设定值
    goodsRfGroupList: [], // 商品分组列表数据(新)
    goodsRfGroupListValid: [], // 商品分组列表数据(已生效)
    goodsEbsCatagoryListAll: [], // ebs分类（所有）
  },
  reducers: {
    // 清除-页面离开的时候初始化state
    clearModel(state) {
      return {
        ...state,
        goodsApplyBillDetailInfo: {}, // 申请单详细信息
        goodsRfInfo: {}, // 申请单选择类型信息
        goodsRfSpuInfo: {}, // 申请单产品基本信息
        goodsSkuAttrUsed: [], // sku属性(sku信息中已设置好的属性内容)
        goodsRfSpuSubAttrInfo: {}, // 商品分组从属属性设定值
        goodsSkuAttrUsedChangeId: uuid.v4(), // 标志goodsSkuAttrUsed发生改变的时候都会给一个唯一的值
        goodsSkuAttrUsedChangeClear: uuid.v4(), // sku属性更改需要清空商品、物料标记
        goodsSpuModellist: [], // 产品型号数据
        goodsRfSpuAttrNormalList: [], // 申请单产品普通属性
        goodsRfSpuDescList: [], // 申请单产品描述数据
        goodsGrouplist: [], // 商品分组列表总数据
        goodsSkulist: [], // 商品列表数据(新建)
        goodsSkulistValid: [], // 商品列表数据(已生效)
        goodsMaterielList: [], // 物料列表数据
        goodsRfGroupList: [], // 商品分组列表数据(新)
        goodsRfGroupListValid: [], // 商品分组列表数据(已生效)
      };
    },
    // 更新-产品申请单列表数据
    changeGoodsApplyBillList(state, { payload }) {
      return {
        ...state,
        goodsApplyBillList: payload.content || [],
        goodsApplyBillListPageSize: Number(payload.total || 0),
      };
    },
    // 更新-正式系统产品信息列表(所有)
    changeValidProductAll(state, { payload }) {
      return {
        ...state,
        validProductAll: payload || [],
      };
    },
    // 更新 - 申请单信息（选择类型）
    changeGoodsRFInfo(state, { payload: goodsRfInfo }) {
      return {
        ...state,
        goodsRfInfo,
      };
    },
    // 更新 - 申请单详细信息
    changeGoodsRFDetailInfo(state, { payload: goodsApplyBillDetailInfo }) {
      return {
        ...state,
        goodsApplyBillDetailInfo,
      };
    },
    // 更新 - 申请单信息中产品类目信息
    changeGoodsCatagoryList(state, { payload }) {
      if (Array.isArray(payload) && payload.length > 0) {
        const { childrens } = payload[0]; // 去除main_catagory类目名称顶层
        return {
          ...state,
          goodsCatagoryListAll: [...childrens],
        };
      }
      return {
        ...state,
        goodsCatagoryListAll: [],
      };
    },
    // 更新 - 申请单信息中产品大类
    changeGoodsClassificationList(state, { payload: goodsClassificationListAll }) {
      return {
        ...state,
        goodsClassificationListAll,
      };
    },
    // 更新 - 申请单信息中产品品牌
    changeGoodsbrandList(state, { payload: goodsbrandListAll }) {
      return {
        ...state,
        goodsbrandListAll,
      };
    },
    // 更新 - 申请单信息中ebs分类（所有）
    changeGoodsEbsCatagoryListAll(state, { payload }) {
      if (Array.isArray(payload) && payload.length > 0) {
        const { childrens } = payload[0]; // 去除main_catagory类目名称顶层
        return {
          ...state,
          goodsEbsCatagoryListAll: [...childrens],
        };
      }
      return {
        ...state,
        goodsEbsCatagoryListAll: [],
      };
    },
    // 更新 - 申请单信息中属性名称（所有）
    changeGoodsAttrNameAllList(state, { payload: goodsAttrNameAll }) {
      return {
        ...state,
        goodsAttrNameAll,
        goodsAttrNameAble: goodsAttrNameAll,
      };
    },
    // 更新 - 申请单信息中属性名称（可用的）
    changeGoodsAttrNameAble(state, { payload }) {
      const { goodsAttrNameAll } = state;
      const arr = goodsAttrNameAll.filter(v => !payload.includes(v.attrCode));
      return {
        ...state,
        goodsAttrNameAble: arr,
      };
    },
    // 更新 - spu产品基本信息
    changeGoodsRfSpuInfo(state, { payload: goodsRfSpuInfo }) {
      return {
        ...state,
        goodsRfSpuInfo,
      };
    },
    // 更新 - spu产品普通属性
    changeGoodsRfSpuAttrNormalList(state, { payload: goodsRfSpuAttrNormalList }) {
      return {
        ...state,
        goodsRfSpuAttrNormalList,
      };
    },
    // 更新 - 商品分组从属属性设定值
    changeGoodsRfSpuSubAttrInfo(state, { payload: goodsRfSpuSubAttrInfo }) {
      return {
        ...state,
        goodsRfSpuSubAttrInfo,
      };
    },
    // 更新 - spu产品描述数据
    changeGoodsRfSpuDescList(state, { payload: goodsRfSpuDescList }) {
      return {
        ...state,
        goodsRfSpuDescList,
      };
    },
    // 更新 - sku属性(sku信息中已设置好的属性内容)
    changeGoodsSkuAttrUsed(state, { payload: goodsSkuAttrUsed }) {
      return {
        ...state,
        goodsSkuAttrUsed,
        goodsSkuAttrUsedChangeId: uuid.v4(),
      };
    },
    // 更新 - sku属性更改需要清空商品、物料标记
    changeGoodsSkuAttrUsedChangeClear(state) {
      return {
        ...state,
        goodsSkuAttrUsedChangeClear: uuid.v4(),
      };
    },
    // 更新 - 商品分组列表总数据
    changeGoodsGrouplist(state, { payload: goodsGrouplist }) {
      return {
        ...state,
        goodsGrouplist,
      };
    },
    // 更新 - 商品分组列表总数据(新)
    changeGoodsRfGroupList(state, { payload }) {
      const goodsRfGroupList = payload.filter(v => v.rfStage !== 1);
      return {
        ...state,
        goodsRfGroupList,
      };
    },
    // 更新 - 商品分组列表总数据(已生效)
    changeGoodsRfGroupListValid(state, { payload = [] }) {
      const goodsRfGroupListValid = payload.filter(v => v.rfStage === 1);
      return {
        ...state,
        goodsRfGroupListValid,
      };
    },
    // 更新 - 商品列表数据(新建)
    changeGoodsSkulist(state, { payload }) {
      return {
        ...state,
        goodsSkulist: payload || [],
      };
    },
    // 更新 - 商品列表数据(已生效)
    changeGoodsSkulistValid(state, { payload }) {
      return {
        ...state,
        goodsSkulistValid: payload || [],
      };
    },
    // 更新 - 产品型号数据
    changeGoodsSpuModellist(state, { payload: goodsSpuModellist }) {
      return {
        ...state,
        goodsSpuModellist,
      };
    },
    // 更新 - 产品型号数据更改需要清空相关商品、物料标记
    changeGoodsSpuModelChangeClear(state) {
      return {
        ...state,
        goodsSpuModelChangeClear: uuid.v4(),
      };
    },
    // 更新 - 物料列表数据
    changeGoodsMaterielList(state, { payload }) {
      return {
        ...state,
        goodsMaterielList: payload || [],
      };
    },
    // 切换-物料tab做相关处理： 1、如果当前物料已有值，则保持当前值渲染 2、如果当前物料为空数组，则进行计算匹配出物料默认值
    toMaterielTab(state) {
      const { goodsMaterielList, goodsSpuModellist, goodsSkulist } = state;
      if (goodsMaterielList.length !== 0) {
        // 如果当前物料已有值，则保持当前值渲染
        return { ...state };
      }
      const arr = [];
      goodsSkulist.forEach(item => {
        const modelCode = item.defaultModelCode || ''; // 产品型号：用商品默认型号
        const obj = {
          number: goodsSpuModellist.length || 1, // 当前商品下物料组合数：产品型号 + 商品名称
          skuName: item.skuName, // 商品名称
          list: [
            {
              modelCode, // 产品型号
              materielName: `${item.skuName} ${modelCode}`,
              series: '', // 系列
              materielEnable: 'default', // 物料状态
            },
          ],
        };
        arr.push(obj);
      });
      return {
        ...state,
        goodsMaterielList: arr,
      };
    },
  },
  effects: {
    // 新增申请单
    *addApplyBill({ payload, callback }, { call }) {
      const res = yield call(addGoodsApplyBill, { ...payload });
      if (res !== undefined && res !== null && res.code === 0) {
        callback();
      }
    },
    // 新增申请单
    *editApplyBill({ payload, callback }, { call }) {
      const res = yield call(editGoodsApplyBill, { ...payload });
      if (res !== undefined && res !== null && res.code === 0) {
        callback();
      }
    },
    // 查询申请单信息(此次直接处理：申请单信息、产品基本信息、商品分组)
    *fetchApplyBillInfo({ payload }, { call, put, select }) {
      const { rfId } = payload;
      const res = yield call(getGoodsApplyBill, { tid: wrapTid().tid, ...payload });
      const result = requestResult(res);
      const {
        goodsRfInfo = {},
        goodsRfSpuInfo = {},
        goodsRfSkuGroupList = [],
        goodsRfMaterielInfoWithSkuNameGroupList = [],
      } = result;
      let catagoryCodeVal = [];
      const goodsCatagoryListAll = yield select(
        state => state.goodsapplybillmanage.goodsCatagoryListAll
      );
      if (goodsRfSpuInfo && goodsRfSpuInfo.catagoryCode) {
        const catagoryCodeArr =
          getTreeParent(
            goodsCatagoryListAll,
            goodsRfSpuInfo.catagoryCode,
            'catagoryCode',
            'parentCode'
          ) || [];
        catagoryCodeVal = catagoryCodeArr.map(v => v.catagoryCode) || [];
      }
      yield put({ type: 'changeGoodsRFDetailInfo', payload: result });
      yield put({
        type: 'changeGoodsRfSpuInfo',
        payload: {
          ...goodsRfSpuInfo,
          catagoryCode: catagoryCodeVal, // 产品类目
          spuTimeToMarket:
            goodsRfSpuInfo && goodsRfSpuInfo.spuTimeToMarket // 上市时间
              ? moment(goodsRfSpuInfo.spuTimeToMarket, 'YYYY-MM-DD')
              : null,
        },
      });
      if (rfId) {
        yield put({ type: 'changeGoodsRFInfo', payload: { ...goodsRfInfo } });
      }
      // 商品分组数据的处理
      const groupList = [...goodsRfSkuGroupList].map(v => {
        const { goodsRfSkuGroupSkuItemVOList, ...other } = v;
        const obj = {};
        goodsRfSkuGroupSkuItemVOList.forEach(item => {
          obj[item.skuAttrCode] = { code: item.skuAttrItemCode, name: item.skuAttrItemName };
        });
        return {
          ...other,
          subAttrCode: v.subAttrCode1 || '',
          subAttrName: v.subAttrName1 || '',
          ...obj,
        };
      });
      yield put({ type: 'changeGoodsRfGroupList', payload: [...groupList] });
      yield put({ type: 'changeGoodsRfGroupListValid', payload: [...groupList] });
      // 物料数据处理
      const goodsMaterielList = formatMaterielData(goodsRfMaterielInfoWithSkuNameGroupList);
      yield put({ type: 'changeGoodsMaterielList', payload: [...goodsMaterielList] });
    },
    // 获取-产品申请单列表(列表/分页)
    *paginationGoodsApplyBill({ payload }, { call, put }) {
      const res = yield call(
        getGoodsApplyBillList,
        wrapTid({
          limit: payload.pageSize,
          page: payload.page - 1,
          searchCondition: payload.searchCondition,
          sortCondition: '[{"property":"createAt","direction":"DESC"}]',
        })
      );
      const result = requestResult(res);
      yield put({ type: 'changeGoodsApplyBillList', payload: result });
    },
    // 获取-正式系统产品信息列表(所有)
    *fetchGoodsSpuInfoAll({ payload }, { call, put }) {
      const res = yield call(getGoodsSpuInfoAll, wrapTid({ ...payload }));
      const result = requestResult(res);
      yield put({ type: 'changeValidProductAll', payload: result });
    },
    // 获取-产品类目(所有)
    *fetchGoodsCatagoryListAll({ payload }, { call, put }) {
      const res = yield call(getGoodsCatagoryListAll, wrapTid({ ...payload }));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsCatagoryList', payload: result });
    },
    // 获取-产品大类(所有)
    *fetchGoodsClassificationListAll({ payload }, { call, put }) {
      const res = yield call(getGoodsClassificationListAll, wrapTid({ ...payload }));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsClassificationList', payload: result });
    },
    // 获取-ebs分类(所有)
    *fetchGoodsEbsCatagoryListAll({ payload }, { call, put }) {
      const res = yield call(getGoodsebscatagoryListAll, { ...payload, tid: wrapTid().tid });
      const result = requestResult(res);
      yield put({ type: 'changeGoodsEbsCatagoryListAll', payload: result });
    },
    // 获取-产品品牌(所有)
    *fetchGoodsbrandListAll({ payload }, { call, put }) {
      const res = yield call(getGoodsbrandListAll, wrapTid({ ...payload }));
      const result = requestResult(res);
      yield put({ type: 'changeGoodsbrandList', payload: result });
    },
    // 获取-属性名称(所有)
    *fetchGoodsAttrNameAll(_, { call, put }) {
      const { tid } = wrapTid({});
      const res = yield call(getGoodsAttrNameAll, tid);
      const result = requestResult(res);
      yield put({ type: 'changeGoodsAttrNameAllList', payload: result });
    },
    // 验证产品名称数据唯一性
    *testUniqueBySpuName({ payload, callback }, { call }) {
      const res = yield call(verifyUniqueBySpuName, wrapTid({ ...payload }));
      callback(res);
    },
    // 申请单作废/撤销
    *applyBillNullify({ payload, callback }, { call }) {
      const res = yield call(addGoodsApplyAudit, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
    },
    // 申请单删除
    *deleteApplyBill({ payload, callback }, { call }) {
      const res = yield call(deleteGoodsRfInfo, payload);
      callback(res !== undefined && res !== null && res.code === 0);
    },
  },
};
