/**
 * [系统角色管理] model层
 */

// import { paginationSysRole } from '@/services/sysconfmanage/sysrolemanage';
// import { requestResult } from '@/utils/mdcutil';

export default {
  namespace: 'sysrolemanage',
  state: {
    sysRoleList: [], // 系统角色数据列表
    sysRoleListPagiSize: 0, // 系统角色数据总数量
  },
  reducers: {
    // 更新-系统角色数据列表
    changeSysRoleList(state, { payload }) {
      return {
        ...state,
        sysRoleList: payload.content || [],
        sysRoleListPagiSize: Number(payload.total || 0),
      };
    },
  },
  effects: {
    // 分页获取系统角色数据
    // *paginationSysRole({ payload }, { call, put }) {
    //   const res = yield call(paginationSysRole, payload);
    //   const result = requestResult(res);
    //   yield put({ type: 'changeSysRoleList', payload: result });
    // },
  },
};
