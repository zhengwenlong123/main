/**
 * [账户设置组件] model层
 */

import { updateAccountPassword2 } from '@/services/account/center';
import { wrapTid } from '@/utils/mdcutil';

export default {
  namespace: 'center',
  state: {
    passwordChange2: false,
  },
  reducers: {
    // 更细-密码
    changeSettingPassword(state, { payload }) {
      return {
        ...state,
        passwordChange: payload || true,
      };
    },
  },
  effects: {
    // 更新-账户密码
    *updateAccountPassword({ payload, callback }, { call, put }) {
      const res = yield call(updateAccountPassword2, wrapTid(payload));
      callback(res !== undefined && res !== null && res.code === 0);
      yield put({ type: 'changeSettingPassword' });
    },
  },
};
