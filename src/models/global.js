import router from 'umi/router';
// import { setAuthority } from '@/utils/authority';
import { getPageQuery, getQueryPath } from '@/utils/utils';
import { reloadAuthorized } from '@/utils/Authorized';
import { defaultPage, loginUrl } from '@/defaultSettings';
import AppGlobal from '@/utils/globalVariable';
import api from '@/services/index';
import defaultAvatar from '@/assets/image/defaultAvatar.png';

const {
  AccountLogin,
  AccountResource,
  CanUseApps,
  GetAppToken,
  TokenLogin,
  CurrentAccount,
  AccountLogout,
  GetPhoneCode,
  GetPhoneCodeAgain,
  VerificationPhoneCode,
  ModifyPassword,
  UpdateAccountPassword,
} = api;

const redirectToLogin = () => {
  // 如果要登出之前的页面本身就是Login, 就不加入到redirect中
  // const urlParams = new URL(window.location.href);
  // const search =
  //   urlParams &&
  //   !notAuthPages.includes(urlParams.pathname) &&
  //   urlParams.pathname !== '/' &&
  //   urlParams.pathname !== ''
  //     ? { redirect: window.location.href }
  //     : null;
  // 将当前地址作为login的?redirect=xxx参数, 这样登入后可以直接跳到这个站点
  if (loginUrl.startsWith('http')) window.location = loginUrl;
  else router.push({ pathname: loginUrl });
  // router.push({ pathname: loginUrl, query: search });
};

export default {
  namespace: 'global',

  state: {
    collapsed: false, // header中个人头像下方的菜单是否显示
    status: undefined,
  },

  effects: {
    // 正常从窗口登入
    *login({ payload }, { call, put }) {
      const response = yield call(AccountLogin, payload);
      yield put({ type: 'redirect', payload: response });
    },

    // 从其他系统跳转登入
    *jumplogin({ payload }, { call, put }) {
      const response = yield call(TokenLogin, payload);
      yield put({ type: 'redirect', payload: response });
    },

    // 登入后的获取权限及跳转
    *redirect({ payload }) {
      if (payload) {
        const urlParams = new URL(window.location.href);
        // 获得url参数中redirect的值
        const params = getPageQuery();
        let { redirect } = params;

        // 如果参数中存在redirect 则跳转到 目的页面
        if (redirect) {
          const redirectUrlParams = new URL(redirect);
          if (redirectUrlParams.origin === urlParams.origin) {
            // 如果目标站点和当前站点是同一个就用router挑战
            redirect = redirect.substr(urlParams.origin.length);
            if (redirect.match(/^\/.*#/)) {
              // 去掉#锚点或者urlrewrite记录
              redirect = redirect.substr(redirect.indexOf('#') + 1);
            }
          } else {
            // 非同个站点直接跳转
            window.location.href = redirect;
            return;
          }
        }
        yield 1;
        // 跳转到目标redirect页面,如果不存在就跳到welcome
        router.replace(redirect || defaultPage);
      }
    },

    // 登出
    *logout(_, { put, call }) {
      // 将当前账号权限设置为登出
      yield call(AccountLogout);
      yield put({
        type: 'changeLoginStatus',
        payload: {
          status: false,
          currentAuthority: 'guest',
        },
      });
      reloadAuthorized();
      redirectToLogin();
    },
    *getCurrentUser({ payload, callback }, { call, put }) {
      const response = yield call(CurrentAccount);
      if (!response) {
        redirectToLogin();
        if (callback) callback(false);
        return;
      }
      const { totalCount, unreadCount, avatar, needResetPwd } = response;
      yield put({
        type: 'user/saveCurrentUser',
        payload: {
          ...response,
          totalCount: totalCount || 0, // 因为现在没有,所以默认0
          unreadCount: unreadCount || 0, // 因为现在没有,所以默认0
          avatar: avatar || defaultAvatar, // 因为现在没有,所以默认头像
        },
      });
      if (needResetPwd) {
        router.push({ pathname: '/changepassword' });
        return;
      }
      yield put({ type: 'getUserResource', payload });
      yield put({ type: 'getapps' });
      if (callback) callback(true);
    },

    // 取得当前人能用的App平台列表
    *getapps({ payload }, { call, put }) {
      const response = yield call(CanUseApps, payload);
      if (!response) return;
      const { usableAppList } = response;
      yield put({
        type: 'saveCanUseApps',
        payload: usableAppList,
      });
    },

    *getUserResource({ payload }, { call, put }) {
      // 如果登入成功 获得菜单
      const response2 = yield call(AccountResource);

      // 如果登入成功 获得菜单  将菜单写入全局变量
      AppGlobal.authorityArray.array = response2;

      // 将菜单写入权限
      yield put({
        type: 'changeLoginStatus',
        payload: { currentAuthority: response2 },
      });
      // 重载权限
      reloadAuthorized();
      yield put({ type: 'menu/getMenuData', payload });
    },

    // 通过系统code得到跳转token,然后跳转到指定系统
    *getAppTokenAndRedirect({ payload }, { call }) {
      const { code, url } = payload;
      // 通过系统code得到跳转token
      const token = yield call(GetAppToken, { appCode: code });
      // 跳转 到 jump.html?token=xxxx
      window.location.href = getQueryPath(url, { token });
    },

    // 将当前页面,和parms保存下来,然后跳转
    *keepListAndRedirect({ payload }) {
      const { parms = {}, redirectTo = '' } = payload;
      const urlParams = new URL(window.location.href);
      // urlParams.pathname: "/purchase/list"
      sessionStorage.setItem('list_url', urlParams.pathname);
      sessionStorage.setItem('list_params', JSON.stringify(parms));
      sessionStorage.setItem('list_to', redirectTo);
      yield 1;
      router.push(redirectTo);
    },
    // 跳回列表页
    *backToList({ payload }) {
      const { defaultUrl } = payload;
      const listUrl = sessionStorage.getItem('list_url');
      yield 1;
      router.push(`${listUrl || defaultUrl}?isback=1`);
    },
    // 取得储存的列表页参数
    *getBackParams({ callback }) {
      const listParms = JSON.parse(sessionStorage.getItem('list_params'));
      yield 1; // 只是用来不让报错
      if (callback) callback(listParms);
    },
    // 手机获取重置密码验证码
    *getPhoneCode({ payload, callback }, { call }) {
      const response = yield call(GetPhoneCode, payload);
      if (callback) callback(response);
    },

    // 手机重新获取重置密码验证码
    *getPhoneCodeAgain({ payload, callback }, { call }) {
      const response = yield call(GetPhoneCodeAgain, payload);
      if (callback) callback(response);
    },

    // 验证验证码是否有效
    *verificationPhoneCode({ payload, callback }, { call }) {
      const response = yield call(VerificationPhoneCode, payload);
      if (callback) callback(response);
    },

    // 重置密码
    *modifyPassword({ payload, callback }, { call }) {
      const response = yield call(ModifyPassword, payload);
      if (callback) callback(response);
    },

    // 更新-账户密码
    *updateAccountPassword({ payload, callback }, { call }) {
      const res = yield call(UpdateAccountPassword, payload || {});
      callback(res !== undefined && res !== null && res.code === 0, res);
    },
  },

  reducers: {
    // 更改 登入状态
    changeLoginStatus(state, { payload }) {
      // setAuthority(payload.currentAuthority);
      return {
        ...state,
        status: payload.status,
        type: payload.type,
      };
    },

    // 储存 当前登入人员可用平台列表
    saveCanUseApps(state, { payload }) {
      return {
        ...state,
        canUseApps: payload,
      };
    },

    //* 这个是头像的菜单是否展开
    changeLayoutCollapsed(state, { payload }) {
      return {
        ...state,
        collapsed: payload,
      };
    },
  },

  subscriptions: {
    setup({ history }) {
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      return history.listen(({ pathname, search }) => {
        // 这里是订阅了history当页面改编时将页面访问的地址发送到GoogleAnalytics, 目前本系统似乎没有使用GA
        if (typeof window.ga !== 'undefined') {
          window.ga('send', 'pageview', pathname + search);
        }
      });
    },
  },
};
