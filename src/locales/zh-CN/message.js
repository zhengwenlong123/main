export default {
  'message.export.success': '导出成功',
  'message.delete.success': '删除成功',
  'message.active.success': '激活成功',
  'message.active.scyn.success': '激活并同步成功',
  'message.save.success': '储存成功',
  'message.Shipment.success': '请求发运成功',
  'message.choose.one': '请至少选择一项',
  'message.delete.it': '删除后不可恢复,继续吗?',
  'message.active.it': '确定要激活吗?',
  'message.shipment.it': '确定要发运吗?',

  'message.common.optionSuccess': '操作成功',
  'message.common.optionError': '操作失败',

  'message.confirm.prefix': '请选择数据后再',
  'message.confirm.delete': '删除',
  'message.confirm.handle': '处理',
  'message.confirm.modify': '维护',

  'message.attachment.notSelect': '附件未选择,请处理',
  'message.attachment.formatError': '文件格式有误请重新上传',
  'message.attachment.handleError': '附件处理失败',
  'message.attachment.uploading': '上传中...',

  'message.datasource.null': '暂无数据',
};
