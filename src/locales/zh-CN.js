import globalHeader from './zh-CN/globalHeader';
import menu from './zh-CN/menu';
import settings from './zh-CN/settings';
import exception from './zh-CN/exception';
import login from './zh-CN/login';
import form from './zh-CN/form';
import button from './zh-CN/button';
import message from './zh-CN/message';

export default {
  'navBar.lang': '语言',
  'layout.user.link.help': '帮助',
  'layout.user.link.privacy': '隐私',
  'layout.user.link.terms': '条款',
  'app.home.introduce': '介绍',
  ...globalHeader,
  ...menu,
  ...settings,
  ...exception,
  ...login,
  ...form,
  ...button,
  ...message,
};
