export default {
  'menu.welcome': 'Welcome',
  'menu.more-blocks': 'More Blocks',
  'menu.home': 'Home',

  'menu.account.center': 'Account Center',
  'menu.account.settings': 'Account Settings',
  'menu.account.trigger': 'Trigger Error',
  'menu.account.logout': 'Logout',

  'menu.exception': 'Exception',
  'menu.exception.not-permission': '403',
  'menu.exception.not-find': '404',
  'menu.exception.server-error': '500',
  'menu.exception.trigger': 'Trigger',

  'menu.sysconfmanage': 'SysConfManage',
  'menu.sysconfmanage.sysrolemanage': 'SysRoleManage',
  'menu.sysconfmanage.sysaccountmanage': 'SysAccountManage',

  'menu.custmanage': 'CustManage',
  'menu.custmanage.custoperunittypemanage': 'CustOperUnitTypeManage',
  'menu.custmanage.custcompanyinfomanage': 'CustCompanyInfoManage',
  'menu.custmanage.custcompanytagmanage': 'CustCompanyTagManage',
  'menu.custmanage.custcompanymappingmanage': 'CustCompanyMappingManage',

  'menu.parmmanage': 'ParmManage',
  'menu.parmmanage.parmpublicparametermanage': 'ParmPublicParameterManage',
  'menu.parmmanage.parmcountrymanage': 'ParmCountryManage',
  'menu.parmmanage.parmregionmanage': 'ParmRegionManage',
  'menu.parmmanage.parmcurrencymanage': 'ParmCurrencyManage',

  'menu.goodsmanage': 'GoodsManage',
  'menu.goodsmanage.goodsbrandmanage': 'BrandManage',
  'menu.goodsmanage.goodsclassificationmanage': 'ClassificationManage',
  'menu.goodsmanage.goodscatagorymanage': 'CatagoryManage',
  'menu.goodsmanage.goodsattrnamemanage': 'AttrNameManage',
  'menu.goodsmanage.goodsattrcollectmanage': 'AttrCollectManage',
  'menu.goodsmanage.goodsspuinfomanage': 'SpuInfoMange',
  'menu.goodsmanage.goodsskuinfomanage': 'SkuInfoManage',
  'menu.goodsmanage.goodsmaterielmanage': 'MaterielManage',
  'menu.goodsmanage.goodsapplybillmanage': 'ApplyBillManage',
  'menu.goodsmanage.goodstagmanage': 'TagManage',
};
